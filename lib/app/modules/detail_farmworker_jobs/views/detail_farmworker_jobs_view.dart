import 'package:agritech/app/modules/farm_worker_home_page/controllers/farm_worker_controller.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/detail_farmworker_jobs_controller.dart';

class DetailFarmworkerJobsView extends GetView<DetailFarmworkerJobsController> {
  const DetailFarmworkerJobsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
   return Scaffold(
      appBar:AppBar(
        title: Text(
          'detail_jobs'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: ()async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(right: 20,left: 20),
        child: Obx(() =>controller.apiJobIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: Get.height * 0.35,
                    width: Get.width,
                    decoration: BoxDecoration(
                        color: colorAsh,
                        borderRadius: BorderRadius.circular(13)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: controller.jobByIdModel.value.result?.image != null
                          ? Image.network(
                              controller.jobByIdModel.value.result!.image!,
                              height: Get.height * 0.35,
                              width: Get.width,
                              fit: BoxFit.cover,
                            )
                          : Image.asset(
                              defaultImageUrl,
                              height: Get.height * 0.35,
                              width: Get.width,
                              fit: BoxFit.cover,
                            ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${'job_at'.tr} ${controller.jobByIdModel.value.result?.village ?? ''}',
                            style: TextStyles.kTSFS20W700
                                .copyWith(color: colorDetails),
                          ),
                          Text(
                            '${controller.jobByIdModel.value.result?.mandal ?? ''}, ${controller.jobByIdModel.value.result?.district ?? ''}',
                            style: TextStyles.kTSDS14W500
                                .copyWith(color: colorDetails),
                          ),
                        ],
                      ),
                      Flexible(
                        child: Obx(
                          () => InkWell(
                            onTap: () async {
                              if (controller.isFarmworkerFavourite.value ==
                                  true) {
                                await Get.find<HomeController>().updateSavedUser(false, id: controller.deleteId.value ,errorMessage: "job_unsaved_successfully".tr);
                                await controller.getAllSavedJobs(userId: controller.id.value);
                                controller.isFarmworkerFavourite.value = false;
                              } else {
                                await controller
                                    .saveFarmworkerJobAsFavourite();
                                await controller.getAllSavedJobs(userId: controller.id.value);
                                controller.isFarmworkerFavourite.value = true;
                              }
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(11),
                                color: controller.isFarmworkerFavourite.value
                                    ? colorBlue.withOpacity(0.5)
                                    : colorSave,
                              ),
                              child: Icon(
                                controller.isFarmworkerFavourite.value
                                    ? Icons.bookmark
                                    : Icons.bookmark_border_outlined,
                                color: colorBlack.withOpacity(0.5),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          CircleAvatar(
                            radius: 26,
                            backgroundColor: colorAsh,
                            child: ClipOval(
                              child:
                              Image.network(
                      controller.jobByIdModel.value.result?.jobOwnerDetails?.imageUrl.toString() ??
                          defaultImageUrl,
                      height: Get.height * 0.1,
                      width: Get.width*0.2,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.1,
                          width: Get.width*0.2,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                              
                            ),
                          ),
                          const SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                controller.jobByIdModel.value.result?.jobOwnerDetails?.firstName
                                        ?.toString() ??
                                    'Unknown User',
                                style: TextStyles.kTSDS14W700
                                    .copyWith(color: colorPic),
                              ),
                              const SizedBox(height: 5),
                              Text(
                                controller.jobByIdModel.value.result?.jobOwnerDetails?.address
                                        ?.toString() ??
                                    'Unknown Address',
                                style: TextStyles.kTSWFS10W700
                                    .copyWith(color: colorDetails),
                              )
                            ],
                          ),
                        ],
                      ),controller.statusForLandlord.value=="" ?
                      InkWell(
                        onTap: () async{
                          await controller.sentRequestByFarmworker(
                              id: controller
                                      .jobByIdModel.value.result?.landLordId ??
                                  controller
                                      .jobByIdModel.value.result?.farmerId,
                              role: controller.jobByIdModel.value.result
                                          ?.landLordId ==
                                      null
                                  ? "Farmer"
                                  : "Landlord",
                              jobId:controller.jobByIdModel.value.result
                                          ?.id );
                          await controller.sentRequestByFarmworkerFetch(
                              id: controller
                                      .jobByIdModel.value.result?.landLordId ??
                                  controller
                                      .jobByIdModel.value.result?.farmerId,
                              role: controller.jobByIdModel.value.result
                                          ?.landLordId ==
                                      null
                                  ? "Farmer"
                                  : "Landlord", );
                        },
                        child: Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(11),
                              color: primary),
                          child: Text(
                            'interest'.tr,
                            style: TextStyles.kTSFS16W700
                                .copyWith(color: colorWhite),
                          ),
                        ),
                      ):Text(controller.statusForLandlord.value,style: TextStyles.kTSFS18W600.copyWith(color:controller.statusForLandlord.value=='pending'?Color.fromARGB(255, 227, 154, 44): primary),),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Text(
                    controller.jobByIdModel.value.result?.description ??
                        'no_description_available'.tr,
                    style: TextStyles.kTSCF12W500,
                  ),
                  const SizedBox(height: 20),
                  Text(
                    'suggested_jobs'.tr,
                    style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                  ),
                  const SizedBox(height: 10),
                  SizedBox(
                    height: Get.height * 0.08,
                    width: Get.width,
                    child: Obx(() {
                      final jobs = Get.find<FarmWorkerHomePageController>()
                              .filterJobsByModel
                              .value
                              .result ??
                          [];

                      final filteredJobs = jobs
                          .where((job) =>
                              job.job?.id.toString() != controller.jobId.value)
                          .toList();

                      return ListView.separated(
                        itemCount: filteredJobs.length,
                        scrollDirection: Axis.horizontal,
                        separatorBuilder: (context, index) =>
                            const SizedBox(width: 10),
                        itemBuilder: (context, index) {
                          final jobImage = filteredJobs[index].job?.image;

                          return InkWell(
                            onTap: () async {
                              controller.jobId.value =
                                  filteredJobs[index].job?.id.toString() ?? '';
                              await controller.getIdJobs(id: controller.jobId.value);
                              await controller.sentRequestByFarmworkerFetch(
                              id: controller
                                      .jobByIdModel.value.result?.landLordId ??
                                  controller
                                      .jobByIdModel.value.result?.farmerId,
                              role: controller.jobByIdModel.value.result
                                          ?.landLordId ==
                                      null
                                  ? "Farmer"
                                  : "Landlord", );
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: colorGrey,
                                borderRadius: BorderRadius.circular(11),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(11),
                                child: jobImage != null
                                    ? Image.network(
                                        jobImage,
                                        height: Get.height * 0.09,
                                        width: Get.width * 0.2,
                                        fit: BoxFit.cover,
                                      )
                                    : Image.asset(
                                        defaultImageUrl,
                                        height: Get.height * 0.09,
                                        width: Get.width * 0.2,
                                        fit: BoxFit.cover,
                                      ),
                              ),
                            ),
                          );
                        },
                      );
                    }),
                  )
                ],
              ),
            )),
      ),
    );
  }
}
