// To parse this JSON data, do
//
//     final sendLeaseInterestNotificationByFarmerModel = sendLeaseInterestNotificationByFarmerModelFromJson(jsonString);

import 'dart:convert';

SendLeaseInterestNotificationByFarmerModel sendLeaseInterestNotificationByFarmerModelFromJson(String str) => SendLeaseInterestNotificationByFarmerModel.fromJson(json.decode(str));

String sendLeaseInterestNotificationByFarmerModelToJson(SendLeaseInterestNotificationByFarmerModel data) => json.encode(data.toJson());

class SendLeaseInterestNotificationByFarmerModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    SendLeaseInterestNotificationByFarmerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SendLeaseInterestNotificationByFarmerModel.fromJson(Map<String, dynamic> json) => SendLeaseInterestNotificationByFarmerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    Notification? notification;
    dynamic name;

    Result({
        this.notification,
        this.name,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        notification: json["notification"] == null ? null : Notification.fromJson(json["notification"]),
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "notification": notification?.toJson(),
        "name": name,
    };
}

class Notification {
    bool? isRead;
    int? id;
    dynamic message;
    dynamic from;
    dynamic to;
    dynamic imageUrl;
    dynamic name;
    int? landId;
    dynamic statusForFarmer;
    bool? flag;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic statusForLandlord;
    dynamic jobId;
    dynamic statusForFarmworker;

    Notification({
        this.isRead,
        this.id,
        this.message,
        this.from,
        this.to,
        this.imageUrl,
        this.name,
        this.landId,
        this.statusForFarmer,
        this.flag,
        this.updatedAt,
        this.createdAt,
        this.statusForLandlord,
        this.jobId,
        this.statusForFarmworker,
    });

    factory Notification.fromJson(Map<String, dynamic> json) => Notification(
        isRead: json["is_read"],
        id: json["id"],
        message: json["message"],
        from: json["from"],
        to: json["to"],
        imageUrl: json["image_url"],
        name: json["name"],
        landId: json["landId"],
        statusForFarmer: json["status_for_farmer"],
        flag: json["flag"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        statusForLandlord: json["status_for_landlord"],
        jobId: json["jobId"],
        statusForFarmworker: json["status_for_farmworker"],
    );

    Map<String, dynamic> toJson() => {
        "is_read": isRead,
        "id": id,
        "message": message,
        "from": from,
        "to": to,
        "image_url": imageUrl,
        "name": name,
        "landId": landId,
        "status_for_farmer": statusForFarmer,
        "flag": flag,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "status_for_landlord": statusForLandlord,
        "jobId": jobId,
        "status_for_farmworker": statusForFarmworker,
    };
}
