// To parse this JSON data, do
//
//     final intiateCropPaymentModel = intiateCropPaymentModelFromJson(jsonString);

import 'dart:convert';

IntiateCropPaymentModel intiateCropPaymentModelFromJson(String str) => IntiateCropPaymentModel.fromJson(json.decode(str));

String intiateCropPaymentModelToJson(IntiateCropPaymentModel data) => json.encode(data.toJson());

class IntiateCropPaymentModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    IntiateCropPaymentModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory IntiateCropPaymentModel.fromJson(Map<String, dynamic> json) => IntiateCropPaymentModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    dynamic orderId;
    dynamic amount;
    dynamic paymentId;
    dynamic paymentStatus;

    Result({
        this.orderId,
        this.amount,
        this.paymentId,
        this.paymentStatus,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        orderId: json["orderId"],
        amount: json["amount"],
        paymentId: json["paymentId"],
        paymentStatus: json["paymentStatus"],
    );

    Map<String, dynamic> toJson() => {
        "orderId": orderId,
        "amount": amount,
        "paymentId": paymentId,
        "paymentStatus": paymentStatus,
    };
}
