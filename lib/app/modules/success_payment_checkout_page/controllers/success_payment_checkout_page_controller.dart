import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../details_of_landlord_utilities/data/create_utility_payment_model.dart';
import '../../details_of_landlord_utilities/services/details_of_landlord_utilities_repo_impl.dart';

class SuccessPaymentCheckoutPageController extends BaseController {

  Rx<CreateUtilityPaymentModel> getUtilityPaymentByIdModel = Rx(CreateUtilityPaymentModel());

  var paymentUtilityId="".obs;

  @override
  void onInit() async{
    super.onInit();
    if(Get.arguments!=null){
      await getUtilityPayment(createdUtillityId: Get.arguments["id"]);
    }
  }

  //get Utility Payment By Id  API
  Future<void> getUtilityPayment({createdUtillityId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailsOfLandlordUtilitiesRepoImpl(dioClient).getUtilityPayment(id: createdUtillityId);
      print("===================== get utility payment by id ==============");
      print(result);
      result.fold((model) {
        getUtilityPaymentByIdModel.value = model;
        paymentUtilityId.value = getUtilityPaymentByIdModel.value.result!.id.toString();
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
