import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../core/utils/widget_utils/buttons/custom_button.dart';
import '../../../routes/app_pages.dart';
import '../../home/controllers/home_controller.dart';
import '../controllers/profile_page_controller.dart';

class ProfilePageView extends GetView<ProfilePageController> {
  const ProfilePageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void showPicker(BuildContext context) {
      showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  leading: const Icon(Icons.photo_library),
                  title: Text('gallery'.tr),
                  onTap: () {
                    controller.pickImage(ImageSource.gallery);
                    Navigator.of(context).pop();
                  },
                ),
                ListTile(
                  leading: const Icon(Icons.photo_camera),
                  title: Text('camera'.tr),
                  onTap: () {
                    controller.pickImage(ImageSource.camera);
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        },
      );
    }

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            flex: 3,
            child: Stack(
              children: [
                // Logo at the bottom of the stack
                SizedBox(
                    width: Get.width,
                    child: Image.asset(profileLogo, fit: BoxFit.cover)),

                // Profile title text
                Padding(
                  padding:const EdgeInsets.only(bottom: 210, top: 20),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      'profile'.tr,
                      style: TextStyles.kTSFS26W400,
                    ),
                  ),
                ),

                // Profile image
                Padding(
                  padding: const EdgeInsets.only(top: 80),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Obx(() {
                          final String profilePath =
                              controller.profilePath.value;
                          final String profileImage =
                              Get.find<HomeController>().profileImage.value;

                          ImageProvider? imageProvider;

                          if (profilePath.isNotEmpty &&
                              File(profilePath).existsSync()) {
                            // Valid local file
                            imageProvider = FileImage(File(profilePath));
                          } else if (profileImage.isNotEmpty &&
                              Uri.tryParse(profileImage)?.hasAbsolutePath ==
                                  true) {
                            // Valid network image
                            imageProvider = NetworkImage(profileImage);
                          } else {
                            // Fallback to default asset image
                            imageProvider = AssetImage(farmer);
                          }

                          return CircleAvatar(
                            backgroundColor: inActiveDotColor,
                            radius: 55,
                            backgroundImage: imageProvider,
                          );
                        }),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            flex: 4,
            fit: FlexFit.tight,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(31),
                    topRight: Radius.circular(31)),
                color: colorGhostWhite,
                boxShadow: [
                  BoxShadow(
                    color: colorBlack.withOpacity(0.2),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: const Offset(0, -3),
                  ),
                ],
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    // Show My Details, Saved, Settings, Customer Support for Farm Worker only
                    Obx(() {
                      if (Get.find<HomeController>().role.value ==
                          'Farm Worker') {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.EDIT_PROFILE);
                              },
                              child: Text('my_details'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.SAVED);
                              },
                              child: Text('saved'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.SETTINGS);
                              },
                              child: Text('settings'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.CUSTOMER_SUPPORT);
                              },
                              child: Text('customer_support'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            //SizedBox(height: Get.height/4,)
                          ],
                        );
                      } else if (Get.find<HomeController>().role.value ==
                          'Utiliser') {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.EDIT_PROFILE);
                              },
                              child: Text('my_details'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.MY_INSTRUMENTS);
                              },
                              child: Text('my_instruments'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.SETTINGS);
                              },
                              child: Text('settings'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.CUSTOMER_SUPPORT);
                              },
                              child: Text('customer_support'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            //SizedBox(height: Get.height/4,)
                          ],
                        );
                      } else if (Get.find<HomeController>().role.value ==
                          'Vendor') {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.EDIT_PROFILE);
                              },
                              child: Text('my_details'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.AVAILABLE_PURCHASED_CROPS);
                              },
                              child: Text('purchased'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            Get.find<HomeController>()
                                        .getUserModel
                                        .value
                                        .result!
                                        .flag !=
                                    true
                                ? InkWell(
                                    onTap: () {
                                      Get.toNamed(Routes.KYC_WELCOME_PAGE);
                                    },
                                    child: Text('update_kyc'.tr,
                                        style: TextStyles.kTSNFS17W500
                                            .copyWith(color: colorSaved)),
                                  )
                                : Row(
                                    children: [
                                      Text(
                                        'e_kyc_verified'.tr,
                                        style: TextStyles.kTSNFS17W500
                                            .copyWith(color: colorSaved),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Icon(
                                        Icons.verified,
                                        color: colorGreen,
                                        size: 20,
                                      )
                                    ],
                                  ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.SETTINGS);
                              },
                              child: Text('settings'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.CUSTOMER_SUPPORT);
                              },
                              child: Text('customer_support'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            //SizedBox(height: Get.height/4,)
                          ],
                        );
                      } else if (Get.find<HomeController>().role.value ==
                              'Landlord' ||
                          Get.find<HomeController>().role.value == 'Farmer') {
                        // Show for Landlord and Farmer
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.EDIT_PROFILE);
                              },
                              child: Text('my_details'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                if (Get.find<HomeController>().role.value ==
                                    'Landlord') {
                                  // Get.find<SelectRegionController>()
                                  //     .isLands
                                  //     .value = true;
                                  Get.toNamed(Routes.AVAILABLE_MY_LANDS);
                                } else if (Get.find<HomeController>()
                                        .role
                                        .value ==
                                    'Farmer') {
                                  // Get.find<SelectRegionController>()
                                  //     .isLands
                                  //     .value = true;
                                  Get.toNamed(Routes.AVAILABLE_FARMER_MY_LANDS);
                                }
                              },
                              child: Text('my_lands'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            if (Get.find<HomeController>().role.value ==
                                'Landlord') ...[
                              InkWell(
                                onTap: () {
                                  Get.toNamed(Routes.HIRED_FARMERS);
                                },
                                child: Text('farmers'.tr,
                                    style: TextStyles.kTSNFS17W500
                                        .copyWith(color: colorSaved)),
                              ),
                              const SizedBox(height: 30),
                            ],
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.HIRED_FARMWORKERS);
                              },
                              child: Text('farm_workers'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.SAVED);
                              },
                              child: Text('saved'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                if (Get.find<HomeController>().role.value ==
                                    'Landlord') {
                                  Get.toNamed(Routes.JOBS_POSTED_BY_LANDLORD);
                                } else if (Get.find<HomeController>()
                                        .role
                                        .value ==
                                    'Farmer') {
                                  Get.toNamed(Routes.JOBS_POSTED_BY_FARMER);
                                }
                              },
                              child: Text('jobs_posted'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.SETTINGS);
                              },
                              child: Text('settings'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                            const SizedBox(height: 30),
                            InkWell(
                              onTap: () {
                                Get.toNamed(Routes.CUSTOMER_SUPPORT);
                              },
                              child: Text('customer_support'.tr,
                                  style: TextStyles.kTSNFS17W500
                                      .copyWith(color: colorSaved)),
                            ),
                          ],
                        );
                      } else {
                        return SizedBox.shrink(); // Hide for other roles
                      }
                    }),
                    const SizedBox(height: 40),
                    CustomButton(
                      action: () async {
                        await SecureStorage().clearAll();
                        Get.offAllNamed(Routes.WELCOME_PAGE);
                      },
                      name: 'logout'.tr,
                    ),
                    const SizedBox(height: 20),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
