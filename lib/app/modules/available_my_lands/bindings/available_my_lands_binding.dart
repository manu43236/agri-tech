import 'package:agritech/app/modules/select_region/controllers/select_region_controller.dart';
import 'package:get/get.dart';

import '../controllers/available_my_lands_controller.dart';

class AvailableMyLandsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailableMyLandsController>(
      () => AvailableMyLandsController(),
    );
    Get.lazyPut<SelectRegionController>(
      () => SelectRegionController(),fenix: true,
    );
  }
}
