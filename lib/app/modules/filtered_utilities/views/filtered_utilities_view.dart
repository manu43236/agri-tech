import 'package:agritech/app/modules/select_region/controllers/select_region_controller.dart';
import 'package:agritech/app/modules/utility/controllers/utility_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../../instrument_details/controllers/instrument_details_controller.dart';
import '../controllers/filtered_utilities_controller.dart';

class FilteredUtilitiesView extends GetView<FilteredUtilitiesController> {
  const FilteredUtilitiesView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  Get.find<UtilityController>().userUtilities();
                  Get.toNamed(Routes.DASHBOARD);
                },
                child: const Icon(Icons.arrow_back_sharp),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '${'your_selected_region_is'.tr} ${Get.find<SelectRegionController>().getSelectedRegionName()}',
                    style: TextStyles.kTSCF12W500.copyWith(color: colorHello),
                  ),
                  InkWell(
                    onTap: () {
                      Get.toNamed(Routes.SELECT_REGION);
                    },
                    child: Image.asset(
                      cone,
                      height: 20,
                    ),
                  ),
                ],
              ),
              Expanded(
                child: (Get.find<FilteredUtilitiesController>()
                            .filterUtilitiesModel
                            .value
                            .result
                            ?.isNotEmpty ??
                        false)
                    ? ListView.separated(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        itemCount: Get.find<FilteredUtilitiesController>()
                                .filterUtilitiesModel
                                .value
                                .result
                                ?.length ??
                            0,
                        separatorBuilder: (context, index) => const SizedBox(
                          height: 5,
                        ),
                        itemBuilder: (context, index) {
                          final utility = Get.find<FilteredUtilitiesController>()
                              .filterUtilitiesModel
                              .value
                              .result![index];

                          return InkWell(
                            onTap: () {
                              
                              Get.find<InstrumentDetailsController>().getUtilityById(id:  utility.id.toString());
                              Get.toNamed(Routes.INSTRUMENT_DETAILS);
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(11),topRight: Radius.circular(11)),
                                  child: Image.network(
                                    utility.image ?? defaultImageUrl,
                                    height: Get.height * 0.2,
                                    width: Get.width,
                                    fit: BoxFit.cover,
                                    errorBuilder: (context, error, stackTrace) {
                                      return Image.asset(
                                        defaultImageUrl,
                                        height: Get.height * 0.2,
                                        width: Get.width,
                                        fit: BoxFit.cover,
                                      );
                                    },
                                  ),
                                ),
                                Container(
                                  width: Get.width,
                                  decoration: const BoxDecoration(
                                    color: colorAsh,
                                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(11),bottomRight: Radius.circular(11)),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          utility.nameOfInstrument ?? '',
                                          style: TextStyles.kTSDS14W500
                                              .copyWith(color: colorHello),
                                        ),
                                        const SizedBox(height: 5),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${'location'.tr}${utility.village ?? 'Unknown'}',
                                            ),
                                            SizedBox(width: 20,),
                                             Text(
                                              '${'price'.tr}${utility.pricePerDay ?? 'Unknown'}',
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      )
                    : Center(
                        child: Text('no_utilities_found_matching_the_criteria'.tr),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
