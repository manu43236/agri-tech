import 'package:get/get.dart';

import '../../saved_jobs/controllers/saved_jobs_controller.dart';
import '../controllers/detail_my_jobs_controller.dart';

class DetailMyJobsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailMyJobsController>(
      () => DetailMyJobsController(),
    );
     Get.lazyPut<SavedJobsController>(
      () => SavedJobsController(),
    );
  }
}
