import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../home/controllers/home_controller.dart';
import '../controllers/detail_recent_lands_controller.dart';

class DetailRecentLandsView extends GetView<DetailRecentLandsController> {
  const DetailRecentLandsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'detail_recent_lands'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          right: 20,
          left: 20,
        ),
        child: Obx(
          () => controller.apiLandIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer():
           SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: colorAsh, borderRadius: BorderRadius.circular(15)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.network(
                      controller.landByIdModel.value.result
                              ?.landDetails?.imageUrl ??
                          '',
                      height: Get.height * 0.3,
                      width: Get.width,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        // Display a local asset if the network image fails
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.3,
                          width: Get.width,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${controller.landByIdModel.value.result?.landDetails?.fullName ?? ''} ${'at'.tr}',
                            style: TextStyles.kTSFS20W700
                                .copyWith(color: colorDetails),
                          ),
                          Text(
                            '${controller.landByIdModel.value.result?.landDetails?.village ?? ''}, ${controller.landByIdModel.value.result?.landDetails?.district ?? ''}',
                            style: TextStyles.kTSDS14W500
                                .copyWith(color: colorDetails),
                          ),
                        ],
                      ),
                    ),
                    Obx(
                      () => InkWell(
                        onTap: () async {
                          if (controller.isFavourite.value ==
                              true) {
                            await Get.find<HomeController>().updateSavedUser(false,
                                id: controller.deleteId.value,
                                errorMessage: "land_unsaved_successfully".tr);
                            await controller.allLandsSaved(id: controller.id.value);
                            controller.isFavourite.value =
                                false;
                          } else {
                            await Get.find<HomeController>().saveLandAsFavourite(landId: controller.landId.value);
                            await controller.allLandsSaved(id: controller.id.value);
                            controller.isFavourite.value = true;
                          }
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(11),
                            color: controller.isFavourite.value
                                ? colorBlue.withOpacity(0.5)
                                : colorSave,
                          ),
                          child: Icon(
                            controller.isFavourite.value
                                ? Icons.bookmark
                                : Icons.bookmark_border_outlined,
                            color: colorBlack.withOpacity(0.5),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
                Row(
                  children: [
                    Image.asset(
                      waterCycle,
                      height: 15,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(
                      controller.landByIdModel.value.result
                                  ?.landDetails?.water ==
                              'No'
                          ? 'not_available'.tr
                          : 'available'.tr,
                      style:
                          TextStyles.kTSFS12W400.copyWith(color: colorDetails),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          radius: 26,
                          backgroundColor: colorAsh,
                          child: controller.landByIdModel.value
                                      .result?.landDetails?.profile !=
                                  null
                              ? ClipOval(
                                  child: Image.network(
                                    controller.landByIdModel
                                        .value.result!.landDetails?.profile!,
                                    fit: BoxFit.cover,
                                    height: Get.height * 0.1,
                                    width: Get.width * 0.2,
                                  ),
                                )
                              : const Icon(Icons.person,
                                  size: 40, color: Colors.grey),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              controller.landByIdModel.value
                                      .result?.landDetails?.fullName ??
                                  '',
                              style: TextStyles.kTSDS14W700
                                  .copyWith(color: colorPic),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Text(
                              controller.landByIdModel.value
                                      .result?.landDetails?.village ??
                                  '',
                              style: TextStyles.kTSWFS10W700
                                  .copyWith(color: colorDetails),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  controller.landByIdModel.value.result
                          ?.landDetails?.description ??
                      "",
                  style: TextStyles.kTSCF12W500,
                ),
                const SizedBox(
                  height: 20,
                ),
                controller.landByIdModel.value.result!
                        .farmerDetails!.isNotEmpty
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'hired_farmers'.tr,
                            style: TextStyles.kTSFS16W600
                                .copyWith(color: colorPic),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            height: Get.height * 0.07,
                            width: Get.width,
                            child: ListView.separated(
                              itemCount: controller.landByIdModel
                                  .value.result!.farmerDetails!.length,
                              scrollDirection: Axis.horizontal,
                              separatorBuilder: (context, index) =>
                                  const SizedBox(
                                width: 10,
                              ),
                              itemBuilder: (context, index) {
                                return Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    CircleAvatar(
                                      radius: 26,
                                      backgroundColor: colorAsh,
                                      child: controller
                                                  .landByIdModel
                                                  .value
                                                  .result
                                                  ?.farmerDetails![index]
                                                  .imageUrl !=
                                              null
                                          ? ClipOval(
                                              child: Image.network(
                                                controller
                                                    .landByIdModel
                                                    .value
                                                    .result
                                                    ?.farmerDetails![index]
                                                    .imageUrl!,
                                                fit: BoxFit.cover,
                                                height: Get.height * 0.1,
                                                width: Get.width * 0.2,
                                              ),
                                            )
                                          : const Icon(Icons.person,
                                              size: 40, color: Colors.grey),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          controller
                                                      .landByIdModel
                                                      .value
                                                      .result!
                                                      .farmerDetails !=
                                                  null
                                              ? controller
                                                  .landByIdModel
                                                  .value
                                                  .result!
                                                  .farmerDetails![index]
                                                  .name
                                                  .toString()
                                              : '',
                                          style: TextStyles.kTSDS14W700
                                              .copyWith(color: colorPic),
                                        ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          controller
                                                      .landByIdModel
                                                      .value
                                                      .result!
                                                      .farmerDetails !=
                                                  null
                                              ? controller
                                                  .landByIdModel
                                                  .value
                                                  .result!
                                                  .farmerDetails![index]
                                                  .address
                                                  .toString()
                                              : '',
                                          style: TextStyles.kTSWFS10W700
                                              .copyWith(color: colorDetails),
                                        )
                                      ],
                                    ),
                                  ],
                                );
                              },
                            ),
                          ),
                        ],
                      )
                    : const SizedBox(),
                const SizedBox(height: 10),
                Text(
                  'suggested_lands'.tr,
                  style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                ),
                const SizedBox(
                  height: 10,
                ),
             controller.suggestStatus.value == ApiStatus.LOADING ?  const SizedBox():
                 controller.suggestLands.value.isEmpty ?   Center(
                          child: Text(
                            'no_lands_are_available_near_your_location'.tr,
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.grey,
                            ),
                          ),
                        ): SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children:_listSuggest(),
                    )),
               
              ],
            ),
          ),
        ),
      ),
    );
  }

  // List<Widget> _listSuggest() {
  //   print("Hello");
  //   List<Widget> items = [];
  //   for (var i = 0; i < controller.suggestLands.value.length; i++) {
  //     items.add(
       
  //       InkWell(
  //       onTap: () async {
  //         controller.onRefresh(
  //             landId: controller.suggestLands.value[i].id.toString());
  //       },
  //       child: Container(
  //         margin: const EdgeInsets.all(5),
  //         decoration: BoxDecoration(
  //           color: colorGrey,
  //           borderRadius: BorderRadius.circular(11),
  //         ),
  //         child: ClipRRect(
  //           borderRadius: BorderRadius.circular(11),
  //           child: Image.network(
  //             controller.suggestLands.value[i].imageUrl,
  //             height: Get.height * 0.09,
  //             width: Get.width * 0.2,
  //             fit: BoxFit.cover,
  //             errorBuilder: (context, error, stackTrace) {
  //               return Image.asset(
  //                 defaultImageUrl,
  //                 height: Get.height * 0.09,
  //                 width: Get.width * 0.2,
  //                 fit: BoxFit.cover,
  //               );
  //             },
  //           ),
  //         ),
  //       ),
  //     ));
  //   }
  //   return items;
  // }
  List<Widget> _listSuggest() {
  List<Widget> items = [];
  for (var i = 0; i < controller.suggestLands.value.length; i++) {
    items.add(
      InkWell(
        onTap: () async {
          // Remove the tapped item from the list
          final tappedLandId = controller.suggestLands.value[i].id;
          controller.suggestLands.value.removeWhere((land) => land.id == tappedLandId);

          // Optionally, call onRefresh with the tapped landId
          await controller.onRefresh(landId: tappedLandId.toString());

          // Update the list after removal
          controller.suggestLands.refresh();
        },
        child: Container(
          margin: const EdgeInsets.all(5),
          decoration: BoxDecoration(
            color: colorGrey,
            borderRadius: BorderRadius.circular(11),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(11),
            child: Image.network(
              controller.suggestLands.value[i].imageUrl,
              height: Get.height * 0.09,
              width: Get.width * 0.2,
              fit: BoxFit.cover,
              errorBuilder: (context, error, stackTrace) {
                return Image.asset(
                  defaultImageUrl,
                  height: Get.height * 0.09,
                  width: Get.width * 0.2,
                  fit: BoxFit.cover,
                );
              },
            ),
          ),
        ),
      ),
    );
  }
  return items;
}

}

