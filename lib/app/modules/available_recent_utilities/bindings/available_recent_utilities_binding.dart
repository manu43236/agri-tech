import 'package:get/get.dart';

import '../../select_region/controllers/select_region_controller.dart';
import '../controllers/available_recent_utilities_controller.dart';

class AvailableRecentUtilitiesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailableRecentUtilitiesController>(
      () => AvailableRecentUtilitiesController(),
    );
    Get.lazyPut<SelectRegionController>(
      () => SelectRegionController(),fenix: true
    );
  }
}
