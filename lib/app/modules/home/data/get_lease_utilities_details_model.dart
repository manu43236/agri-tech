// To parse this JSON data, do
//
//     final getLeaseUtilitiesDetailsModel = getLeaseUtilitiesDetailsModelFromJson(jsonString);

import 'dart:convert';

GetLeaseUtilitiesDetailsModel getLeaseUtilitiesDetailsModelFromJson(String str) => GetLeaseUtilitiesDetailsModel.fromJson(json.decode(str));

String getLeaseUtilitiesDetailsModelToJson(GetLeaseUtilitiesDetailsModel data) => json.encode(data.toJson());

class GetLeaseUtilitiesDetailsModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetLeaseUtilitiesDetailsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetLeaseUtilitiesDetailsModel.fromJson(Map<String, dynamic> json) => GetLeaseUtilitiesDetailsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<Utility>? utilities;
    List<Utiliser>? utilisers;

    Result({
        this.utilities,
        this.utilisers,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        utilities: json["utilities"] == null ? [] : List<Utility>.from(json["utilities"]!.map((x) => Utility.fromJson(x))),
        utilisers: json["utilisers"] == null ? [] : List<Utiliser>.from(json["utilisers"]!.map((x) => Utiliser.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "utilities": utilities == null ? [] : List<dynamic>.from(utilities!.map((x) => x.toJson())),
        "utilisers": utilisers == null ? [] : List<dynamic>.from(utilisers!.map((x) => x.toJson())),
    };
}

class Utiliser {
    int? id;
    dynamic name;
    dynamic mobileNumber;
    dynamic address;
    dynamic image;

    Utiliser({
        this.id,
        this.name,
        this.mobileNumber,
        this.address,
        this.image,
    });

    factory Utiliser.fromJson(Map<String, dynamic> json) => Utiliser(
        id: json["id"],
        name: json["name"],
        mobileNumber: json["mobileNumber"],
        address: json["address"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "mobileNumber": mobileNumber,
        "address": address,
        "image": image,
    };
}

class Utility {
    int? id;
    int? utililityId;
    dynamic name;
    dynamic description;
    dynamic location;
    dynamic pricePerDay;
    dynamic image;

    Utility({
        this.id,
        this.utililityId,
        this.name,
        this.description,
        this.location,
        this.pricePerDay,
        this.image,
    });

    factory Utility.fromJson(Map<String, dynamic> json) => Utility(
        id: json["id"],
        utililityId: json["utililityId"],
        name: json["name"],
        description: json["description"],
        location: json["location"],
        pricePerDay: json["pricePerDay"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "utililityId": utililityId,
        "name": name,
        "description": description,
        "location": location,
        "pricePerDay": pricePerDay,
        "image": image,
    };
}
