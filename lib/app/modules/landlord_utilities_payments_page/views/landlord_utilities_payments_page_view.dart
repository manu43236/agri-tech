import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/landlord_utilities_payments_page_controller.dart';

class LandlordUtilitiesPaymentsPageView
    extends GetView<LandlordUtilitiesPaymentsPageController> {
  const LandlordUtilitiesPaymentsPageView({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('LandlordUtilitiesPaymentsPageView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'LandlordUtilitiesPaymentsPageView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
