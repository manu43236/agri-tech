import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../models/job_by_id_model.dart';
import '../../../../models/save_job_as_favourite_model.dart';
import '../../../../models/send_request_by_farmworker_model.dart';
import '../../farm_worker_home_page/data/send_request_by_farmworker_fetch_model.dart';
import '../../farm_worker_home_page/services/filter_jobs_repo_impl.dart';
import '../../home/services/job_by_id_repo_impl.dart';
import '../../notifications/services/notifications_repo_impl.dart';
import '../../saved/data/get_all_saved_Jobs_model.dart';
import '../../saved/services/saved_repo_impl.dart';

class DetailFarmworkerJobsController extends BaseController {

  Rx<SendRequestByFarmworkerModel> sendRequestByFarmworkerModel =
      Rx(SendRequestByFarmworkerModel());
  Rx<SendRequestByFarmworkerFetchModel> sendRequestByFarmworkerFetchModel =
      Rx(SendRequestByFarmworkerFetchModel());
  Rx<SaveJobAsFavouriteModel> saveJobAsFavouriteModel =
      Rx(SaveJobAsFavouriteModel());
  Rx<JobByIdModel> jobByIdModel = Rx(JobByIdModel());
  Rx<GetAllSavedJobsModel> getAllSavedJobsModel = Rx(GetAllSavedJobsModel());

  var statusForLandlord="".obs;
  var jobId = "".obs;
  var apiStatusLoad = ApiStatus.LOADING.obs;
  var apiJobIdStatus = ApiStatus.LOADING.obs;
  var isFarmworkerFavourite = false.obs;
  var deleteId = 0.obs;
  var id=''.obs;

  @override
  void onInit() async{
    id.value = await SecureStorage().readData(key: "id") ?? "";
    super.onInit();
    if(Get.arguments!=null){
       await getAllSavedJobs(userId: id.value);
       await getIdJobs(id: Get.arguments["jobId"]);
       await sentRequestByFarmworkerFetch(
                              id: jobByIdModel.value.result?.landLordId ??
                                  jobByIdModel.value.result?.farmerId,
                              role: jobByIdModel.value.result
                                          ?.landLordId ==
                                      null
                                  ? "Farmer"
                                  : "Landlord", );
    }
  }

  // jobs by id API
  Future<void> getIdJobs({id}) async {
    apiJobIdStatus.value = ApiStatus.LOADING;
    try {
      var result = await JobByIdRepoImpl(dioClient).getJob(id: id);

      result.fold((left) {
        eachJobhandleResponse(left);
        apiJobIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiJobIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void eachJobhandleResponse(JobByIdModel model) async {
    if (model.statusCode == 200) {
      jobByIdModel.value = model;

      // Ensure jobByIdModel.value.result is not null
      if (jobByIdModel.value.result != null) {
        isFarmworkerFavourite.value = false;

        print('Checking getAllSavedJobsModel...');

        if (getAllSavedJobsModel.value.result !=
                null &&
                    getAllSavedJobsModel
                    .value
                    .result!
                    .savedJobs !=
                null) {
          print('hiiiiiiiiiiii');
          for (var element in getAllSavedJobsModel
              .value
              .result!
              .savedJobs!) {
            print('for loop');
            print(element.jobid);
            print(jobByIdModel.value.result!.id);
            if (element.jobid == jobByIdModel.value.result!.id) {
              print('elwmmmmmmmmmm');
              deleteId.value = element.savedJobId!;
              print('delete id ${deleteId.value}');
              isFarmworkerFavourite.value = true;
            }
          }
        } else {
          print('Saved jobs or result is null');
        }
      } else {
        print('jobByIdModel.result is null');
      }
    }
  }

   // Save job as favourite API
  Future<void> saveFarmworkerJobAsFavourite() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await FilterJobsRepoImpl(dioClient).jobAsFavourite(params1: {
        "userid": Get.find<HomeController>().userId.value,
        "jobid": Get.find<HomeController>().jobId.value
      });
      print('userId ${Get.find<HomeController>().userId.value}');
      print('jobId ${Get.find<HomeController>().jobId.value}');
      print("===================== save job as favourite ==============");
      print(result);
      result.fold((model) {
        saveJobAsFavouriteModel.value = model;
        if (saveJobAsFavouriteModel.value.statusCode == 200) {
          Get.find<HomeController>().deleteId.value =
              saveJobAsFavouriteModel.value.result!.id!;
          Get.snackbar('Success', 'Job saved successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (saveJobAsFavouriteModel.value.statusCode == 400) {
          Get.snackbar('Failure', 'Job already saved',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

   //send request by farmworker API
  Future<void> sentRequestByFarmworker({id, role, jobId}) async {
    apiStatusLoad.value = ApiStatus.LOADING;
    try {
      var result = await NotificationsRepoImpl(dioClient)
          .sendRequestByFarmworkerModel(params1: {
        "farmworkerId":
            int.parse(Get.find<HomeController>().selectedLandlordId.value),
        "recipientId": id,
        "recipientRole": role,
        "jobId": jobId
      });
      print(
          'farmworker id is:${Get.find<HomeController>().selectedLandlordId.value}');
      print('recipient id is:${id}');
      print('recipient role is:${role}');
      print('job id:$jobId');
      print(
          "======================== sent notification by farmworker id ====================");
      print(result);
      result.fold((model) {
        sendRequestByFarmworkerModel.value = model;
        if (sendRequestByFarmworkerModel.value.statusCode == 200) {
          Get.snackbar('Success', 'Notification sent successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (sendRequestByFarmworkerModel.value.statusCode == 400) {
          Get.snackbar('message', 'Notification already sent to the landlord',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatusLoad.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatusLoad.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //send request by farmworker fetch API
  Future<void> sentRequestByFarmworkerFetch({id, role}) async {
    apiStatus.value = ApiStatus.LOADING;
 
    try {
      var result = await FilterJobsRepoImpl(dioClient)
          .farmworkersFetch(params1: {
        "farmworkerId":
            int.parse(Get.find<HomeController>().selectedLandlordId.value),
        "recipientId": id,
        "recipientRole": role,
      });
      print(
          'farmworker id is:${Get.find<HomeController>().selectedLandlordId.value}');
      print('recipient id is:${id}');
      print('recipient role is:${role}');
      print(
          "======================== sent notification by farmworker id fetch ====================");
      print(result);
      result.fold((model) {
        sendRequestByFarmworkerFetchModel.value = model;
        statusForLandlord.value="";
        for (var element in sendRequestByFarmworkerFetchModel.value.result!.jobDetails! ) {
          print('for loop');
          print(element.jobId!);
          print('ids${jobByIdModel.value.result!.id}');
          if(jobByIdModel.value.result!.landLordId!=null){
             statusForLandlord.value = "";
             if (element.jobId.toString() == jobByIdModel.value.result!.id.toString()) {
             print('equal land');
             statusForLandlord.value = element.statusForLandlord!;
             print(statusForLandlord.value);
             }
          }else if(jobByIdModel.value.result!.farmerId!=null){
             statusForLandlord.value = "";
             if (element.jobId.toString() == jobByIdModel.value.result!.id.toString()) {
             print('equal');
             statusForLandlord.value = element.statusForFarmer!;
             print(statusForLandlord.value);
             }
          }
         
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  
    // get all saved jobs API
  Future<void> getAllSavedJobs({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient).getAllJobs(userId: userId);
      print("===================== get all saved jobs ==============");
      print(result);
      result.fold((model) {
        getAllSavedJobsModel.value = model;
        print('model${getAllSavedJobsModel.value}');
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
