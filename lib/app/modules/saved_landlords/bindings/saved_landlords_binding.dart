import 'package:get/get.dart';

import '../controllers/saved_landlords_controller.dart';

class SavedLandlordsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SavedLandlordsController>(
      () => SavedLandlordsController(),
    );
  }
}
