// To parse this JSON data, do
//
//     final getNotificationsModel = getNotificationsModelFromJson(jsonString);

import 'dart:convert';

GetNotificationsModel getNotificationsModelFromJson(String str) => GetNotificationsModel.fromJson(json.decode(str));

String getNotificationsModelToJson(GetNotificationsModel data) => json.encode(data.toJson());

class GetNotificationsModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<Result>? result;

    GetNotificationsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetNotificationsModel.fromJson(Map<String, dynamic> json) => GetNotificationsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    dynamic message;
    dynamic name;
    dynamic from;
    dynamic to;
    bool? isRead;
    dynamic statusForLandlord;
    dynamic statusForFarmer;
    dynamic imageUrl;
    int? landId;
    dynamic jobId;
    dynamic statusForFarmworker;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;

    Result({
        this.id,
        this.message,
        this.name,
        this.from,
        this.to,
        this.isRead,
        this.statusForLandlord,
        this.statusForFarmer,
        this.imageUrl,
        this.landId,
        this.jobId,
        this.statusForFarmworker,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        message: json["message"],
        name: json["name"],
        from: json["from"],
        to: json["to"],
        isRead: json["is_read"],
        statusForLandlord: json["status_for_landlord"],
        statusForFarmer: json["status_for_farmer"],
        imageUrl: json["image_url"],
        landId: json["landId"],
        jobId: json["jobId"],
        statusForFarmworker: json["status_for_farmworker"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "message": message,
        "name": name,
        "from": from,
        "to": to,
        "is_read": isRead,
        "status_for_landlord": statusForLandlord,
        "status_for_farmer": statusForFarmer,
        "image_url": imageUrl,
        "landId": landId,
        "jobId": jobId,
        "status_for_farmworker": statusForFarmworker,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
