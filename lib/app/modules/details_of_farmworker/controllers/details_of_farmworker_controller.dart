import 'package:agritech/app/modules/details_of_farmworker/data/get_all_available_jobs_model.dart';
import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../models/get_user_model.dart';
import '../../../../models/send_notification_by_landlord_to_worker_model.dart';
import '../../home/controllers/home_controller.dart';
import '../../home/data/all_jobs_of_landlord_model.dart';
import '../../home/services/get_user_repo_impl.dart';
import '../../home/services/home_repo_impl.dart';
import '../../notifications/services/send_notification_by_landlord_to_worker_repo_impl.dart';
import '../../saved/data/get_all_saved_farmworkers_model.dart';
import '../../saved/services/saved_repo_impl.dart';
import '../data/near_by_farmworkers_model.dart';
import '../data/send_work_notification_by_landlord_or_farmer_fetch_model.dart';
import '../services/details_of_farmworker_repo_impl.dart';

class DetailsOfFarmworkerController extends BaseController {
  Rx<NearByFarmworkersModel> nearByFarmworkersModel =
      Rx(NearByFarmworkersModel());
  Rx<GetAvailableJobsModel> getAvailableJobsModel = Rx(GetAvailableJobsModel());
  Rx<SendNotificationByLandlordToWorkerModel>
      sendNotificationByLandlordToWorkerModel =
      Rx(SendNotificationByLandlordToWorkerModel());
  Rx<SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel>
      sendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel =
      Rx(SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel());
  Rx<GetUserModel> getUserProfileModel = Rx(GetUserModel());
  Rx<AllJobsOfLandlordModel> allJobsOfLandlordModel =
      Rx(AllJobsOfLandlordModel());
  Rx<GetAllSavedFarmworkerModel> getAllSavedFarmworkerModel =
      Rx(GetAllSavedFarmworkerModel());

  final LandlordController landlordController = Get.find<LandlordController>();
  final HomeController homeController=Get.find<HomeController>();

  var ongoingJobDescriptions = <Map<String, String>>[].obs;
  var selectedJobId = ''.obs;
  var selectedCrop = ''.obs;
  var statusForLandlord = ''.obs;
  var statusForFarmer = ''.obs;

  var apiStatusWorker = false.obs;

  var selectedJob = Rxn<Map<String, String>>();
  var showAllSkills = false.obs;

  var id=''.obs;
  var role=''.obs;
  var workerId=0.obs;
  Rx<List<SavedFarmer>> savedFarmers = Rx([]);
  var apiUserByProfileIdStatus=ApiStatus.LOADING.obs;
  var skills=[].obs;
  var isFarmworkerSaved = false.obs;
  var deleteId = 0.obs;
  var farmWorkerId = "".obs;

  @override
  void onInit() async{
    super.onInit();
    id.value=await SecureStorage().readData(key: "id"??'');
    role.value=await SecureStorage().readData(key:"role"??'');
    await allFarmworkersSaved(id: id.value);
    if (Get.arguments != null) {
      getUserByProfileId(
          id: Get.arguments["workerId"]);
    }
    ongoingJobDescriptions =
        ongoingJobDescriptions;
    addAlljobs(userId: homeController.userId.value);
    nearByFarmworkers(latitude: homeController.latitude.value,longitude: homeController.longitude.value);
    getAvailableJobs(userId: homeController.userId.value);
    fetchedJobDetails(senderId: homeController.selectedLandlordId.value,workerId: homeController.farmWorkerId.value,senderRole: homeController.role.value);
    
  }

  @override
  void onReady() {
    super.onReady();
  }


  // Method to toggle skill view
  void toggleShowAllSkills() {
    showAllSkills.value = !showAllSkills.value;
  }

  void setSelectedCrop(String crop) {
    selectedCrop.value = crop;
  }

  //near by farmworkers API
  Future<void> nearByFarmworkers({latitude,longitude}) async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result =
          await DetailsOfFarmworkerRepoImpl(dioClient).nearByFarmworkers(latitude: latitude,longitude: longitude);
      print('=============== near by farmworkers =========');
      print(result);
      result.fold((model) {
        nearByFarmworkersModel.value = model;
        //allSkills.value = getAllSkills(model.result?.farmworkers);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //get available jobs API
  Future<void> getAvailableJobs({userId}) async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result = await DetailsOfFarmworkerRepoImpl(dioClient).availableJobs(userId: userId);
      print('=============== available jobs list =========');
      print(result);
      result.fold((model) {
        getAvailableJobsModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //send notification by landlord to worker API
  Future<void> sentNotificationByLandlordToWorker({senderId,workerId,senderRole,jobId}) async {
    apiStatusWorker.value = false;
    try {
      var result = await SendNotificationByLandlordToWorkerRepoImpl(dioClient)
          .landlordToWorkerNotification(params1: {
        "senderId": senderId,//Get.find<HomeController>().selectedLandlordId.value,
        "farmworkerId":workerId, //Get.find<HomeController>().farmWorkerId.value,
        "senderRole": senderRole,//Get.find<HomeController>().role.value,
        "jobId": jobId//selectedJobId.value,
      });
      print(
          'sender id is:${Get.find<HomeController>().selectedLandlordId.value}');
      print(
          'farmworker id is:${Get.find<HomeController>().farmWorkerId.value}');
      print('job id is:${selectedJobId.value}');
      print('role is ${Get.find<HomeController>().role.value}');
      print(
          "======================== sent notification by landlord id to farmworker id ====================");
      print(result);
      result.fold((model) {
        sendNotificationByLandlordToWorkerModel.value = model;
        print(sendNotificationByLandlordToWorkerModel.value.result != null
            ? sendNotificationByLandlordToWorkerModel
                .value.result!.notification!.message
            : '');
        if (sendNotificationByLandlordToWorkerModel.value.statusCode == 200) {
          Get.snackbar('Success', 'Notification sent successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (sendNotificationByLandlordToWorkerModel.value.statusCode ==
            400) {
          Get.snackbar('Failure', 'Notification already sent to the farmworker',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatusWorker.value = true;
      }, (r) {
        apiStatusWorker.value = false;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //fetched job details API
  Future<void> fetchedJobDetails({senderId,workerId,senderRole}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailsOfFarmworkerRepoImpl(dioClient)
          .fetchFarmworkers(params1: {
        "senderId":   senderId, //Get.find<HomeController>().selectedLandlordId.value,
        "farmworkerId":workerId, //Get.find<HomeController>().farmWorkerId.value,
        "senderRole": senderRole//Get.find<HomeController>().role.value,
      });
     
      result.fold((model) {
        sendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel.value =
            model;
        for (var element
            in sendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel
                .value.result!.controlData!) {
          print('for loop');
          print(element.jobId);
          print(selectedJobId.value);
          if (Get.find<HomeController>().role.value == 'Landlord') {
            statusForLandlord.value == "";
            if (element.jobId.toString() == selectedJobId.value) {
              print('elwmmmmmmmmmm');
              statusForLandlord.value = element.statusForLandlord!;
            }
          } else if (Get.find<HomeController>().role.value == 'Farmer') {
            statusForLandlord.value == "";
            if (element.jobId.toString() == selectedJobId.value) {
              print('elwmmmmmmmmmm');
              statusForLandlord.value = element.statusForFarmer!;
            }
          }
        }

        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  // Get user profile by id API
  Future<void> getUserByProfileId({id}) async {
    apiUserByProfileIdStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await GetUserRepoImpl(dioClient).getUser(id: id);
      print("======================== user by id ====================");
      print('Result $result');
      result.fold((model) {
        getUserProfileModel.value = model;
        workerId.value=getUserProfileModel.value.result!.id!;
        skills.value =
        getUserProfileModel.value.result?.skills ?? [];
        print('model result ${getUserProfileModel.value}');
      
          isFarmworkerSaved.value = false;
          print('Farmworker saved: ${isFarmworkerSaved.value}');

           savedFarmers.value =getAllSavedFarmworkerModel
                  .value
                  .result
                  ?.savedFarmers??[];

          for (var element in savedFarmers.value) {
            if (element.farmworkerid == getUserProfileModel.value.result?.id) {
              deleteId.value = element.savedFarmworkerId ?? 0;
              print('Delete ID for farmworker: ${deleteId.value}');
              isFarmworkerSaved.value = true;
            }
          }

        apiUserByProfileIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiUserByProfileIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> addAlljobs({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await HomeRepoImpl(dioClient)
          .addJobsOfLandlord(userId: userId);
      print("========================All jobs of landlord====================");
      print(result);
      result.fold((left) {
        allJobshandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void allJobshandleResponse(AllJobsOfLandlordModel model) async {
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
      allJobsOfLandlordModel.value = model;
      print('77777777777777777777777');
       ongoingJobDescriptions.value =
          allJobsOfLandlordModel.value.result!.ongoingJobs!.map((job) {
        return {
          job.id.toString():
              "${job.fullName} job at ${job.village}"
        };
      }).toList();
      print(allJobsOfLandlordModel.value.result!.ongoingJobs?[0].description);
    }
  }

    Future<void> allFarmworkersSaved({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient)
          .allFarmworkers(userId: id);
      print("===================== get all saved farmworkers ==============");
      print(result);
      result.fold((model) {
        getAllSavedFarmworkerModel.value = model;
        print(getAllSavedFarmworkerModel.value.result!.farmerCount);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }
  
  @override
  void onClose() {
    super.onClose();
  }
}
