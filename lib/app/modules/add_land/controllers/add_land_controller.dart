import 'dart:io';

import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart' as dio;
import 'package:image_picker/image_picker.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../models/add_land_by_farmer_model.dart';
import '../../../../models/add_land_model.dart';
import '../../dashboard/controllers/dashboard_controller.dart';
import '../../home/controllers/home_controller.dart';
import '../services/add_land_by_farmer_repo_impl.dart';
import '../services/add_land_repo_impl.dart';

class AddLandController extends BaseController {

  
  var nameController = TextEditingController().obs;
  var landInAcresController = TextEditingController().obs;
  var landSurveyController = TextEditingController().obs;
  var stateController = TextEditingController().obs;
  var districtController = TextEditingController().obs;
  var mandalController = TextEditingController().obs;
  var villageController = TextEditingController().obs;
  var descriptionController = TextEditingController().obs;
  var waterController = TextEditingController().obs;
  var imageController = TextEditingController().obs;

  // var isFormValid=false.obs;
  

  var landImage = Rx<File?>(null);
  RxString landPath = ''.obs;
  RxString landFile = ''.obs;
  var landValue = ''.obs;
  var userId = "".obs;
  var landId = "".obs;
  var role = "".obs;
  var selectedStateIds = 0.obs;
  var selectedDistrictIds = 0.obs;
  var selectedMandalIds = 0.obs;
  var selectedOption = ''.obs;

  @override
  void onInit() async{
    super.onInit();
    userId.value = await SecureStorage().readData(key: "id") ?? "";
    role.value=Get.find<HomeController>().role.value;
  }

  void setSelectedOption(String value) {
    selectedOption.value = value;
    waterController.value.text = value;
    update();
  }


  void setImagePath(String path) {
    imageController.value.text = path;
  }

  Future<void> pickImage(ImageSource source) async {
    final XFile? image = await ImagePicker().pickImage(source: source);

    if (image != null) {
      landImage.value = File(image.path);
      landFile.value = image.name;
      landPath.value = image.path;
      setImagePath(image.path);
    } else {
      Get.snackbar('Error', 'No image selected');
    }
  }


  void removeImage() {
    landImage.value = null;
  }

  void _clearFormFields() {
  nameController.value.clear();
  landInAcresController.value.clear();
  landSurveyController.value.clear();
  stateController.value.clear();
  districtController.value.clear();
  mandalController.value.clear();
  villageController.value.clear();
  descriptionController.value.clear();
  selectedOption.value = '';
  waterController.value.clear();
  imageController.value.clear();
  landImage.value = null;
  landPath.value = '';
  landFile.value = '';
}


//Add Land by Landlord API
  void addLands() async {
    apiStatus.value = ApiStatus.LOADING;
    if (nameController.value.text.isNotEmpty &&
        landInAcresController.value.text.isNotEmpty &&
        landSurveyController.value.text.isNotEmpty &&
        stateController.value.text.isNotEmpty &&
        districtController.value.text.isNotEmpty &&
        mandalController.value.text.isNotEmpty &&
        villageController.value.text.isNotEmpty &&
        descriptionController.value.text.isNotEmpty &&
        waterController.value.text.isNotEmpty &&
        imageController.value.text.isNotEmpty) {
      try {
        dio.FormData formData = dio.FormData.fromMap({
          "landLordId": userId.value,
          "fullName": nameController.value.text,
          "landInAcres": landInAcresController.value.text,
          "surveyNum": landSurveyController.value.text,
          "state": stateController.value.text,
          "district": districtController.value.text,
          "mandal": mandalController.value.text,
          "village": villageController.value.text,
          "longitude":Get.find<HomeController>().latitude.value,
          "latitude":Get.find<HomeController>().latitude.value,
          "description": descriptionController.value.text,
          "water": waterController.value.text,
          "file": await dio.MultipartFile.fromFile(
            landPath.value,
            filename: landFile.value,
          ),
        });
        print('000000000000000000000000000000000000');
        print(formData);
        print(
          waterController.value.text,
        );

        var result =
            await AddLandRepoImpl(dioClient).doAddLand(params1: formData);
        print("result:$result");
        result.fold((left) {
          _handleResponse(left);
          apiStatus.value = ApiStatus.SUCCESS;
        }, (right) {
          apiStatus.value = ApiStatus.FAIL;
        });
      } catch (e) {
        print('Error: $e');
      }
    } else {
      print('One or more fields are empty.');
    }
  }

  void _handleResponse(AddLandModel model) async {
    if (model.statusCode == 200) {
      landId.value = model.result!.id.toString();
      await Get.find<LandlordController>().addAllLands(userId:userId.value );
      await Get.find<LandlordController>().recentLands();
      Get.find<DashboardController>().currentIndex.value = 0;
      Get.find<HomeController>().selectedIndex.value=0;
      Get.offNamed(Routes.DASHBOARD);
      nameController.value.clear();
      landInAcresController.value.clear();
      landSurveyController.value.clear();
      stateController.value.clear();
      districtController.value.clear();
      mandalController.value.clear();
      villageController.value.clear();
      descriptionController.value.clear();
      selectedOption.value ='';
      waterController.value.clear();
      //imageController.value.clear();
      removeImage();
       Get.snackbar(
        'Success', 
        'Land added successfully',
        backgroundColor: colorGreen,
        colorText: colorBlack
      );
    } else if (model.statusCode == 400) {
      Get.snackbar('Failed', ' Survey number already exists',
          backgroundColor: colorRed,
          colorText: colorWhite,
          snackPosition: SnackPosition.TOP);
    }
  }

    //Add Land by farmer API
  void addFarmerLands() async {
    apiStatus.value = ApiStatus.LOADING;
    if (nameController.value.text.isNotEmpty &&
        landInAcresController.value.text.isNotEmpty &&
        landSurveyController.value.text.isNotEmpty &&
        stateController.value.text.isNotEmpty &&
        districtController.value.text.isNotEmpty &&
        mandalController.value.text.isNotEmpty &&
        villageController.value.text.isNotEmpty &&
        descriptionController.value.text.isNotEmpty &&
        waterController.value.text.isNotEmpty &&
        imageController.value.text.isNotEmpty) {
      try {
        dio.FormData formData = dio.FormData.fromMap({
          "farmerId": userId.value,
          "fullName": nameController.value.text,
          "landInAcres": landInAcresController.value.text,
          "surveyNum": landSurveyController.value.text,
          "state": stateController.value.text,
          "district": districtController.value.text,
          "mandal": mandalController.value.text,
          "village": villageController.value.text,
          "longitude":Get.find<HomeController>().latitude.value,
          "latitude":Get.find<HomeController>().latitude.value,
          "description": descriptionController.value.text,
          "water": waterController.value.text,
          "file": await dio.MultipartFile.fromFile(
            landPath.value,
            filename: landFile.value,
          ),
        });
        print('000000000000000000000000000000000000');
        print(userId.value);
        print(waterController.value.text);
        print(formData);
        var result = await AddLandByFarmerRepoImpl(dioClient)
            .doAddLandFarmer(params1: formData);
        print("result:$result");
        result.fold((left) {
          _handleFarmerResponse(left);
          apiStatus.value = ApiStatus.SUCCESS;
        }, (right) {
          apiStatus.value = ApiStatus.FAIL;
        });
      } catch (e) {
        print('Error: $e');
      }
    } else {
      print('One or more fields are empty.');
    }
  }

  void _handleFarmerResponse(AddLandByFarmerModel model) async {
    if (model.statusCode == 200) {
      landId.value = model.result!.id.toString();
      await Get.find<LandlordController>().addAllFarmersLands(userId: userId.value);
      await Get.find<LandlordController>().recentLands();
      //Get.back();
      Get.find<DashboardController>().currentIndex.value = 0;
      Get.find<HomeController>().selectedIndex.value=0;
      Get.toNamed(Routes.DASHBOARD);
      // Get.find<DashboardController>().pageController.jumpToPage(0);
      nameController.value.clear();
      landInAcresController.value.clear();
      landSurveyController.value.clear();
      stateController.value.clear();
      districtController.value.clear();
      mandalController.value.clear();
      villageController.value.clear();
      descriptionController.value.clear();
      selectedOption.value ='';
      waterController.value.clear();
      removeImage();
       Get.snackbar(
        'Success', 
        'Land added successfully',
        backgroundColor: colorGreen,
        colorText: colorBlack
      );
    } else if (model.statusCode == 400) {
      Get.snackbar('Failed', ' Survey number already exists',
          backgroundColor: colorRed,
          colorText: colorWhite,
          snackPosition: SnackPosition.TOP);
    }
  }

  @override
  void onReady() {
    super.onReady();
     _clearFormFields();
  }

  @override
  void onClose() {
    super.onClose();
     _clearFormFields();
  }

}
