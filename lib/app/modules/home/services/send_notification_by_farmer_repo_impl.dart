import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../../../../models/send_notification_by_farmer_model.dart';
import 'send_notification_by_farmer_repo.dart';

class SendNotificationByFarmerRepoImpl extends SendNotificationByFarmerRepo with NetworkCheckService{
  final DioClient _dioClient;
  SendNotificationByFarmerRepoImpl(this._dioClient);
  @override
  Future<Either<SendNotificationByFarmerModel,Exception>> sentByFarmer({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('notification/sendNotificationByFarmer', Method.post,params: params1);
        return result.fold((l){
          SendNotificationByFarmerModel model=SendNotificationByFarmerModel.fromJson(l.data);
          return Left(model);
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}