import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../home/data/get_lease_utilities_details_model.dart';
import '../services.dart/available_leased_utilities_repo_impl.dart';

class AvailableLeasedUtilitiesController extends BaseController {

  Rx<GetLeaseUtilitiesDetailsModel> getLeasedUtilitiesDetailsModel =
      Rx(GetLeaseUtilitiesDetailsModel());

  var leasedUtilitiesSearch=''.obs;
  var utilitiId=0.obs;
  var apiLeaseUtilitiesStatus=ApiStatus.LOADING.obs;

  @override
  void onInit() {
    super.onInit();
    leasedUtility();
  }

  // Home Page leased utilities  API
  Future<void> leasedUtility() async {
    apiLeaseUtilitiesStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await AvailableLeasedUtilitiesRepoImpl(dioClient).leasedUtilities();
      print('============hey=============');
      print(result);
      result.fold((model) {
        if(model.statusCode==200){
          getLeasedUtilitiesDetailsModel.value=model;
          print(getLeasedUtilitiesDetailsModel.value.result!.utilities!.length);
        }else if(model.statusCode==400){
      print('===============status=============');
      getLeasedUtilitiesDetailsModel.value.result!.utilities!.clear();
      getLeasedUtilitiesDetailsModel.value.result!.utilities ==[];
    }
        apiLeaseUtilitiesStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiLeaseUtilitiesStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }


}
