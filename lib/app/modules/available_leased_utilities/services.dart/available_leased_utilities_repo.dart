import 'package:dartz/dartz.dart';

import '../../home/data/get_lease_utilities_details_model.dart';

abstract class AvailableLeasedUtilitiesRepo{
  Future<Either<GetLeaseUtilitiesDetailsModel,Exception>>  leasedUtilities();
}