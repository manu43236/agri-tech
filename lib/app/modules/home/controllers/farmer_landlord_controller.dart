import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/home/services/home_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../data/filter_near_landlords_model.dart';

class FarmerLandlordController extends BaseController{

  Rx<FilterNearLandlordModel> filterNearLandlordModel =
      Rx((FilterNearLandlordModel()));

  var farmerLandlordSearch=''.obs;

  @override
  void onInit() async{
    super.onInit();
    await filterLandlords(latitude: Get.find<HomeController>().latitude.value,longitude: Get.find<HomeController>().longitude.value,search: '');
  }

  // List of Landlords view
  Future<void> filterLandlords({latitude,longitude,search}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await HomeRepoImpl(dioClient).filterLandlords(latitude: latitude,longitude: longitude,search: search);
      print("======================== landlords list ====================");
      print(result);
      result.fold((left) {
        landlordshandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void landlordshandleResponse(FilterNearLandlordModel model) async {
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
      filterNearLandlordModel.value = model;
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}