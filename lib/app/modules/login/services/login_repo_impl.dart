
import 'package:agritech/app/modules/login/services/login_repo.dart';
import 'package:agritech/models/login_get_otp_model.dart';
import 'package:dartz/dartz.dart';
import '../../../../core/base/dio_client.dart';
import '../../../../core/network/network_check_service.dart';

class LoginRepoImpl extends LoginRepo with NetworkCheckService {
  final DioClient _dioClient;

  LoginRepoImpl(this._dioClient);

  @override
  Future<Either<LoginGetOtpModel, Exception>> loginOtp({params1}) async {
    bool data = await checkInternet();

    if (!data) {
      return Right(Exception('No Network found'));
    } else {
      try {
        print(params1);
        var result = await _dioClient.requestForAuth('User/userGetOtpForLogin', Method.post,
            params: params1);
        return result.fold((l) {
          LoginGetOtpModel model = LoginGetOtpModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      } catch (e) {
        return Right(Exception(e));
      }
    }
  }
}

