import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/date_format.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/detail_crop_controller.dart';

class DetailCropView extends GetView<DetailCropController> {
  const DetailCropView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'detail_crop'.tr,
            style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
          ),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: colorBlack,
            ),
            onPressed: () async {
              Get.back();
            },
          ),
        ),
        body: Obx(
          () => controller.apiDetailsStatus.value == ApiStatus.LOADING
              ? Shimmers().getBannerShimmer()
              : Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(11),
                        child: Image.network(
                          controller.getdetailsOfCropModel.value.result
                                  ?.cropImage ??
                              '',
                          height: Get.height * 0.3,
                          width: Get.width,
                          fit: BoxFit.cover,
                          errorBuilder: (context, error, stackTrace) {
                            return Image.asset(
                              defaultImageUrl,
                              height: Get.height * 0.2,
                              width: Get.width,
                              fit: BoxFit.cover,
                            );
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        controller
                                .getdetailsOfCropModel.value.result?.cropName ??
                            '',
                        style: TextStyles.kTSFS24W600.copyWith(
                            color: colorbackground,
                            fontWeight: FontWeight.w700),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        '${'location'.tr}${controller.getdetailsOfCropModel.value.result?.landDetails?.village ?? ''}',
                        style: TextStyles.kTSDS12W600
                            .copyWith(color: colorbackground),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${'quantity_available'.tr}${controller.getdetailsOfCropModel.value.result?.yieldDetails?.remainingQuantity ?? ''} tons',
                        style: TextStyles.kTSDS12W600
                            .copyWith(color: colorbackground),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${'owner'.tr}${controller.getdetailsOfCropModel.value.result?.ownerName ?? ''}',
                        style: TextStyles.kTSDS12W600
                            .copyWith(color: colorbackground),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${'total_days'.tr}${controller.getdetailsOfCropModel.value.result?.totaldays ?? ''}',
                        style: TextStyles.kTSDS12W600
                            .copyWith(color: colorbackground),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${'outdate'.tr}${formatDate(controller.getdetailsOfCropModel.value.result?.yieldDetails?.outedDate.toString() ?? '')}',
                        style: TextStyles.kTSDS12W600
                            .copyWith(color: colorbackground),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      // (controller.getdetailsOfCropModel.value.result!.yieldDetails!.status==false || controller.getdetailsOfCropModel.value.result!.yieldDetails!.quantity!=0)?
                      // InkWell(
                      //   onTap: () async{
                      //     await controller.createCropPayment(landId:controller.getdetailsOfCropModel.value.result!.landId,cropId: controller.getdetailsOfCropModel.value.result!.cropId );
                      //     Get.toNamed(Routes.VENDOR_CHECKOUT_DETAILS,arguments: {"id":controller.createCropPaymentModel.value.result?.id?.toString()??''});
                      //   },
                      //   child: Container(
                      //     padding: const EdgeInsets.symmetric(
                      //         horizontal: 20, vertical: 8),
                      //     decoration: BoxDecoration(
                      //         borderRadius: BorderRadius.circular(6),
                      //         color: primary),
                      //     child: Text(
                      //       'Avail this',
                      //       style: TextStyles.kTSDS12W500
                      //           .copyWith(color: colorWhite),
                      //     ),
                      //   ),
                      // ):const SizedBox(),
                      (controller.getdetailsOfCropModel.value.result!.yieldDetails!.remainingQuantity != 0 &&
 controller.getdetailsOfCropModel.value.result!.yieldDetails!.status == false)
    ? InkWell(
        onTap: () async {
          await controller.createCropPayment(
            landId: controller.getdetailsOfCropModel.value.result!.landId,
            cropId: controller.getdetailsOfCropModel.value.result!.cropId,
          );
          Get.toNamed(
            Routes.VENDOR_CHECKOUT_DETAILS,
            arguments: {
              "id": controller.createCropPaymentModel.value.result?.id?.toString() ?? ''
            },
          );
        },
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            color: primary,
          ),
          child: Text(
            'avail_this'.tr,
            style: TextStyles.kTSDS12W500.copyWith(color: colorWhite),
          ),
        ),
      )
    : const SizedBox(),

                    ],
                  ),
                ),
        ));
  }
}
