import 'package:get/get.dart';

import '../controllers/available_leased_utilities_controller.dart';

class AvailableLeasedUtilitiesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailableLeasedUtilitiesController>(
      () => AvailableLeasedUtilitiesController(),
    );
  }
}
