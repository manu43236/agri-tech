import 'package:get/get.dart';

import '../../details_of_farmworker/controllers/details_of_farmworker_controller.dart';
import '../controllers/detail_saved_farmworkers_controller.dart';

class DetailSavedFarmworkersBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailSavedFarmworkersController>(
      () => DetailSavedFarmworkersController(),
    );
    Get.lazyPut<DetailsOfFarmworkerController>(
      () => DetailsOfFarmworkerController(),
    );
  }
}
