import 'package:get/get.dart';

import '../../../../core/base/dropdowns/controller/district_dropdown_controller.dart';
import '../../../../core/base/dropdowns/controller/mandal_dropdown_controller.dart';
import '../../../../core/base/dropdowns/controller/state_dropdown_controller.dart';
import '../../../../core/base/dropdowns/controller/village_dropdown_controller.dart';
import '../controllers/add_land_controller.dart';

class AddLandBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AddLandController>(
      () => AddLandController(),
    );
     Get.lazyPut<StateDropdownController>(
      () => StateDropdownController(),
    );
    Get.lazyPut<DistrictDropdownController>(
      () => DistrictDropdownController(),
    );
     Get.lazyPut<MandalDropdownController>(
      () => MandalDropdownController(),
    );
     Get.lazyPut<VillageDropdownController>(
      () => VillageDropdownController(),
    );
  }
}
