import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';


class ProfilePageController extends GetxController {

  var profileImage = Rx<XFile?>(null);
  var profilePath=''.obs;
  var profileFile=''.obs;

 // var isLands=false.obs;


  @override
  void onInit() {
    super.onInit();
  }

   Future<void> pickImage(ImageSource source) async {
    final XFile? image = await ImagePicker().pickImage(source: source);

    if (image != null) {
     // selectedImagePath.value = pickedFile.path;
      profileImage.value=image;
      profileFile.value=profileImage.value!.name;
      profilePath.value=profileImage.value!.path;
      // Upload the image using Dio or any other method you prefer.
    } else {
      Get.snackbar('Error', 'No image selected');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
