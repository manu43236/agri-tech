import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../base/base_controller.dart';

abstract class SearchViews<T extends BaseController> extends GetView {
  SearchViews({Key? key}) : super(key: key);

  late T cController;
  @override
  Widget build(BuildContext context) {
    cController = Get.find<T>();
    return cBuild(context, cController);
  }

  Widget cBuild(BuildContext context, T cController);
}