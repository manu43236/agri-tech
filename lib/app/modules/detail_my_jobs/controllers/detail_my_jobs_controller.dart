import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../models/job_by_id_model.dart';
import '../../home/services/job_by_id_repo_impl.dart';

class DetailMyJobsController extends BaseController {

 final HomeController homeController=Get.find<HomeController>();

 var apiJobIdStatus = ApiStatus.LOADING.obs;
 Rx<JobByIdModel> jobByIdModel = Rx(JobByIdModel());

 var jobId = "".obs;

  @override
  void onInit() {
    super.onInit();
    if(Get.arguments!=null){
      getIdJobs(id: Get.arguments["jobId"]);
    }
  }

   // jobs by id API
  Future<void> getIdJobs({id}) async {
    apiJobIdStatus.value = ApiStatus.LOADING;
    try {
      var result = await JobByIdRepoImpl(dioClient).getJob(id: id);

      result.fold((left) {
        eachJobhandleResponse(left);
        apiJobIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiJobIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void eachJobhandleResponse(JobByIdModel model) async {
    if (model.statusCode == 200) {
      jobByIdModel.value = model;  
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  
}
