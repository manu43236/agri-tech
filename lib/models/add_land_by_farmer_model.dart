// To parse this JSON data, do
//
//     final addLandByFarmerModel = addLandByFarmerModelFromJson(jsonString);

import 'dart:convert';

AddLandByFarmerModel addLandByFarmerModelFromJson(String str) => AddLandByFarmerModel.fromJson(json.decode(str));

String addLandByFarmerModelToJson(AddLandByFarmerModel data) => json.encode(data.toJson());

class AddLandByFarmerModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    AddLandByFarmerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory AddLandByFarmerModel.fromJson(Map<String, dynamic> json) => AddLandByFarmerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    bool? flag;
    int? id;
    int? farmerId;
    dynamic fullName;
    dynamic landInAcres;
    dynamic surveyNum;
    dynamic state;
    dynamic district;
    dynamic mandal;
    dynamic village;
    dynamic latitude;
    dynamic longitude;
    dynamic radius;
    dynamic description;
    dynamic status;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic landLordId;
    dynamic water;
    dynamic imageId;
    dynamic imageUrl;

    Result({
        this.flag,
        this.id,
        this.farmerId,
        this.fullName,
        this.landInAcres,
        this.surveyNum,
        this.state,
        this.district,
        this.mandal,
        this.village,
        this.latitude,
        this.longitude,
        this.radius,
        this.description,
        this.status,
        this.updatedAt,
        this.createdAt,
        this.landLordId,
        this.water,
        this.imageId,
        this.imageUrl,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        flag: json["flag"],
        id: json["id"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        landInAcres: json["landInAcres"],
        surveyNum: json["surveyNum"],
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
        latitude: json["latitude"],
        longitude: json["longitude"]?.toDouble(),
        radius: json["radius"],
        description: json["description"],
        status: json["status"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        landLordId: json["landLordId"],
        water: json["water"],
        imageId: json["image_id"],
        imageUrl: json["image_url"],
    );

    Map<String, dynamic> toJson() => {
        "flag": flag,
        "id": id,
        "farmerId": farmerId,
        "fullName": fullName,
        "landInAcres": landInAcres,
        "surveyNum": surveyNum,
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
        "latitude": latitude,
        "longitude": longitude,
        "radius": radius,
        "description": description,
        "status": status,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "landLordId": landLordId,
        "water": water,
        "image_id": imageId,
        "image_url": imageUrl,
    };
}
