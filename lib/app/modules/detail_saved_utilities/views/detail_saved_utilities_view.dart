import 'package:agritech/app/modules/detail_saved_utilities/controllers/detail_saved_utilities_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../../details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import '../../utility/controllers/utility_controller.dart';

class DetailSavedUtilitiesView
    extends GetView<DetailSavedUtilitiesController> {
  const DetailSavedUtilitiesView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title: Text(
          'utility_details'.tr,
           style:  TextStyles.kTSFS24W600.copyWith(color: colorSuitable),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: ()async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20,),
        child: SingleChildScrollView(
          child: Obx(
            () =>controller.apigetUtilityStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): 
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Colors.grey.shade300,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.network(
                      controller
                              .getUtilityByIdModel
                              .value
                              .result
                              ?.utility?.image ??
                          defaultImageUrl,
                      height: Get.height * 0.3,
                      width: Get.width,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.3,
                          width: Get.width,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            '${controller.getUtilityByIdModel.value.result?.utility?.nameOfInstrument ?? ''}',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyles.kTSFS20W700
                                .copyWith(color: colorDetails),
                          ),
                          const SizedBox(height: 5),
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  '${controller.getUtilityByIdModel.value.result?.utility?.village ?? ''}, ${controller.getUtilityByIdModel.value.result?.utility?.mandal ?? ''}',
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 3,
                                  style: TextStyles.kTSDS14W500
                                      .copyWith(color: colorDetails),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                Text(
                  '${controller.getUtilityByIdModel.value.result?.utility?.pricePerDay ?? 0} ${'day'.tr}',
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.teal,
                  ),
                ),
                const SizedBox(height: 15),
                Text(
                  controller
                          .getUtilityByIdModel
                          .value
                          .result
                          ?.utility?.description ??
                      '',
                  style: TextStyles.kTSCF12W500,
                ),
                const SizedBox(height: 20),
                Text(
                  'more'.tr,
                  style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                ),
                const SizedBox(height: 10),
                SizedBox(
                  height: Get.height * 0.08,
                  width: Get.width,
                  child: Builder(
                    builder: (context) {
                      // Get the current selected utility ID
                      final selectedUtilityId =
                          controller
                                  .getUtilityByIdModel
                                  .value
                                  .result
                                  ?.utility?.id ??
                              '';

                      // Filter out the selected utility from the list
                      final filteredUtilities = controller.getAllSavedUtilityModel
                              .value.result!.utilitiesDetails
                              ?.where(
                                  (utility) => utility.id != selectedUtilityId)
                              .toList() ??
                          [];

                      // Check if the list is empty after filtering
                      if (filteredUtilities.isEmpty) {
                        return  Center(
                          child: Text(
                            'no_utilities_found_near_your_location'.tr,
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: colorGrey,
                            ),
                          ),
                        );
                      }

                      // Display the remaining utilities in a horizontal ListView
                      return ListView.separated(
                        itemCount: filteredUtilities.length,
                        scrollDirection: Axis.horizontal,
                        separatorBuilder: (context, index) =>
                            const SizedBox(width: 10),
                        itemBuilder: (context, index) {
                          final utility = filteredUtilities[index];

                          return InkWell(
                            onTap: () async {
                              // Get.find<UtilityController>().utilityId.value =
                              //     utility.id.toString();
                              await controller
                                  .getUtilityById(id:  utility.id.toString());
                              // await Get.find<
                              //         DetailsOfLandlordUtilitiesController>()
                              //     .getUtilityByLocation();
                             // Get.toNamed(Routes.DETAIL_SAVED_UTILITIES);
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.grey.shade400,
                                borderRadius: BorderRadius.circular(11),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(11),
                                child: Image.network(
                                  utility.image,
                                  height: Get.height * 0.09,
                                  width: Get.width * 0.2,
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, error, stackTrace) {
                                    return Image.asset(
                                      agri,
                                      height: Get.height * 0.09,
                                      width: Get.width * 0.2,
                                      fit: BoxFit.cover,
                                    );
                                  },
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    },
                  ),
                ),
                const SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _buildActionButton('call'.tr, Icons.call, () {}),
                    _buildActionButton(
                      'rent_now'.tr,
                      Icons.business_center,
                      () async {
                        // Get.find<UtilityController>().utilityId.value =
                        //     controller
                        //         .getUtilityByIdModel
                        //         .value
                        //         .result!.utility!
                        //         .id
                        //         .toString();
                        // await controller.createUtilityPayment(utilityId:controller
                        //         .getUtilityByIdModel
                        //         .value
                        //         .result!.utility!
                        //         .id! );
                        // Get.find<DetailsOfLandlordUtilitiesController>().createdUtilityId.value = Get.find<DetailsOfLandlordUtilitiesController>()
                        //     .createUtilityPaymentModel.value.result!.id
                        //     .toString();
                        // await controller.getUtilityPayment(createdUtillityId: controller.createdUtilityId.value);
                        // Get.find<DetailsOfLandlordUtilitiesController>().paymentUtilityId.value = Get.find<DetailsOfLandlordUtilitiesController>()
                        //     .getUtilityPaymentByIdModel.value.result!.id
                        //     .toString();
                        Get.toNamed(Routes.LANDLORD_UTILITIES_CHECKOUT_PAGE,
                            arguments: {
                              "id": controller
                                  .getUtilityByIdModel
                                  .value
                                  .result!.utility!
                                  .id,
                            });
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildActionButton(
      String label, IconData icon, Function()? onPressed) {
    return ElevatedButton.icon(
      onPressed: onPressed,
      icon: Icon(icon, color: colorWhite),
      label: Text(
        label,
        style: TextStyles.kTSFS16W700.copyWith(color: colorWhite),
      ),
      style: ElevatedButton.styleFrom(
        backgroundColor: primary,
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}
