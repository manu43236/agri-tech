import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../controllers/filter_vendor_controller.dart';

class FilterVendorView extends GetView<FilterVendorController> {
  const FilterVendorView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
       leading:IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
              Get.back();
          },
        ) ,
      ),
      body: const Center(
        child: Text(
          'FilterVendorView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
