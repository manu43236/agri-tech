import 'package:agritech/app/modules/details_of_landlord_land/services/details_of_landlord_land_repo_impl.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../detail_recent_lands/data/land_by_id_model.dart';
import '../../home/services/land_by_id_repo_impl.dart';
import '../../notifications/data/send_lease_interest_farmer_notification_model.dart';
import '../../notifications/services/notifications_repo_impl.dart';
import '../data/send_lease_interest_notification_by_farmer_fetch_model.dart';

class DetailsOfLandlordLandController extends BaseController {

  final HomeController homeController=Get.find<HomeController>();

  Rx<SendLeaseInterestNotificationByFarmerModel>
      sendLeaseInterestNotificationByFarmerModel =
      Rx(SendLeaseInterestNotificationByFarmerModel());

  Rx<SendLeaseInterestNotificationByFarmerFetchModel>
      sendLeaseInterestNotificationByFarmerFetchModel =
      Rx(SendLeaseInterestNotificationByFarmerFetchModel());

  Rx<LandByIdModel> landByIdModel = Rx(LandByIdModel());


  var landlordId = "".obs;
  var landId = "".obs;
  var statusForLandlord="".obs;
  var apiStatusUpdate  = ApiStatus.LOADING.obs;
  var apiLandIdStatus = ApiStatus.LOADING.obs;


  @override
  void onInit() async{
    super.onInit();
    await getIdLands(id: Get.arguments['landId']);
    landId.value=landByIdModel.value.result!.landDetails!.id.toString();
    landlordId.value=landByIdModel.value.result!.landDetails!.landLordId.toString();
    sendLeaseInterestNotificationByFarmerFetch();
  }

  @override
  void onReady() {
    super.onReady();
  }

  // Lands BY id API
  Future<void> getIdLands({id}) async {
    apiLandIdStatus.value = ApiStatus.LOADING;
    try {
      var result = await LandByIdRepoImpl(dioClient).getLand(params1: {
        "id": id,
      });

      result.fold((model) {
        landByIdModel.value = model;
        landId.value = model.result!.landDetails!.id.toString() ?? "";
        apiLandIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiLandIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //send notification by farmer to landlord API
  Future<void> sendLeaseInterestNotificationByFarmer() async {
    apiStatusUpdate.value = ApiStatus.LOADING;
    try {
      var result =
          await NotificationsRepoImpl(dioClient).sendLeaseInterest(params1: {
        "landLordId": landlordId.value,
        "farmerId": Get.find<HomeController>().userId.value,
        "landId": landId.value
      });
      print('land id is:${landId.value}');
      print('farmer id is:${Get.find<HomeController>().userId.value}');
      print('landlord id is:${landlordId.value}');
      print(
          "======================== sent notification by farmer to landlord ====================");
      print(result);
      result.fold((model) {
        sendLeaseInterestNotificationByFarmerModel.value = model;
        print(sendLeaseInterestNotificationByFarmerModel.value.result != null
            ? sendLeaseInterestNotificationByFarmerModel
                .value.result!.notification!.message
            : '');
        if (sendLeaseInterestNotificationByFarmerModel.value.statusCode ==
            200) {
          Get.snackbar('Success', 'Notification sent successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (sendLeaseInterestNotificationByFarmerModel
                .value.statusCode ==
            400) {
          Get.snackbar('Failure',
              'Notification already sent to the landlord for this land',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatusUpdate.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatusUpdate.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //send notification by farmer to landlord Fetch API
  Future<void> sendLeaseInterestNotificationByFarmerFetch() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await DetailsOfLandlordLandRepoImpl(dioClient).farmerFetch(params1: {
        "landLordId": landlordId.value,
        "farmerId": Get.find<HomeController>().userId.value,
      });
      print('farmer id is:${Get.find<HomeController>().userId.value}');
      print('landlord id is:${landlordId.value}');
      print(
          "======================== fetched landlord details ====================");
      print(result);
      result.fold((model) {
        sendLeaseInterestNotificationByFarmerFetchModel.value = model;
        
        for (var element in sendLeaseInterestNotificationByFarmerFetchModel.value.result!) {
            print('for loop');
            print(element.landDetails!.id);
            print(landId.value);
            statusForLandlord.value == "";
            if (element.landDetails!.id.toString() == landId.value) {
             print('fhghhhhhhhh');
             statusForLandlord.value = element.statusForLandlord!;
             print(statusForLandlord.value);
           }
        }
  
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onClose() {
    super.onClose();
  }

}
