import 'package:agritech/app/modules/home/services/home_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../../details_of_farmworker/data/near_by_farmworkers_model.dart';
import '../data/all_jobs_of_landlord_model.dart';
import '../data/all_lands_of_landlord.dart';
import '../data/filter_near_landlords_model.dart';
import '../data/get_all_jobs_by_farmer_model.dart';
import '../data/get_all_lands_by_farmer_model.dart';
import '../data/get_lease_utilities_details_model.dart';
import '../data/get_leased_jobs_details_model.dart';
import '../data/get_leased_lands_details_model.dart';
import '../data/landlord_farmers_model.dart';
import '../data/recent_lands_model.dart';

class HomeRepoImpl extends HomeRepo with NetworkCheckService{
  final DioClient _dioClient;
  HomeRepoImpl(this._dioClient);

  @override
  Future<Either<RecentLandsModel,Exception>> doRecentLands()async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Land/getAllLands', Method.get);
        return result.fold((l) {
          RecentLandsModel model = RecentLandsModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

   @override
  Future<Either<AddLandsOfLandlordModel,Exception>>  doAddAllLands({id})async{
    var data=await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result=await _dioClient.requestForAuth('Land/getAllLandsByLandLord?landLordId=${int.parse(id)}', Method.get);
        print('result repo:$result');
         return result.fold((l) {
          AddLandsOfLandlordModel model = AddLandsOfLandlordModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

   @override
  Future<Either<AllJobsOfLandlordModel,Exception>> addJobsOfLandlord({userId})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Jobs/getAllJobsByLandLordId?landLordId=$userId', Method.get);
        return result.fold((l) {
          AllJobsOfLandlordModel model = AllJobsOfLandlordModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetAllLandsByFarmerModel,Exception>>  allFarmerLands({params1})async{
    var data=await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result=await _dioClient.requestForAuth('Land/getAllLandsByFarmer', Method.get,params: params1);
         return result.fold((l) {
          GetAllLandsByFarmerModel model = GetAllLandsByFarmerModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

   @override
  Future<Either<GetAllJobsByFarmerModel,Exception>>  getAllFarmerJobs({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Jobs/getAllJobsByFarmerId', Method.get,params: params1);
       return result.fold((l) {
          GetAllJobsByFarmerModel model = GetAllJobsByFarmerModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<LandlordFarmersModel,Exception>>  doLFarmers({latitude,longitude,search})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Land/filterNearFarmers?latitude=$latitude&longitude=$longitude&search=$search', Method.get,);//Get.find<LandlordFarmerController>().landlordFarmerSearch.value
        return result.fold((l) {
          LandlordFarmersModel model = LandlordFarmersModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<NearByFarmworkersModel,Exception>>  doLFarmworker({latitude,longitude,search})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Land/filterNearFarmworker?latitude=$latitude&longitude=$longitude&search=$search', Method.get,);//{Get.find<LandlordWorkerController>().landlordWorkerSearch.value}
        return result.fold((l) {
          NearByFarmworkersModel model = NearByFarmworkersModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<FilterNearLandlordModel,Exception>> filterLandlords({latitude,longitude,search})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Land/filterNearLandlords?latitude=$latitude&longitude=$longitude&search=$search', Method.get,);//{Get.find<FarmerLandlordController>().farmerLandlordSearch.value}
        return result.fold((l){
          FilterNearLandlordModel model=FilterNearLandlordModel.fromJson(l.data);
          return Left(model);
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetLeaseUtilitiesDetailsModel,Exception>> getLeaseDetails({userId})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Leasetaker/getLeaseUtilityDetails?id=$userId', Method.get);
        return result.fold((l) {
          GetLeaseUtilitiesDetailsModel model = GetLeaseUtilitiesDetailsModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetLeasedJobDetailsModel,Exception>> getLeasedJobDetails({role,userId})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Leasetaker/getLeaseTakerDetails?type=$role&id=$userId', Method.get,);
        return result.fold((l) {
          GetLeasedJobDetailsModel model = GetLeasedJobDetailsModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetLeasedLandsDetailsModel,Exception>> getLeasedLandDetails({userId,role})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Leasetaker/getLeaseTakerDetails?type=$role&id=$userId', Method.get,);
        return result.fold((l) {
          GetLeasedLandsDetailsModel model = GetLeasedLandsDetailsModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}