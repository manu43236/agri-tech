// To parse this JSON data, do
//
//     final villageModel = villageModelFromJson(jsonString);

import 'dart:convert';

VillageModel villageModelFromJson(String str) => VillageModel.fromJson(json.decode(str));

String villageModelToJson(VillageModel data) => json.encode(data.toJson());

class VillageModel {
    int? statusCode;
    String? status;
    String? message;
    List<VillageResult>? result;

    VillageModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory VillageModel.fromJson(Map<String, dynamic> json) => VillageModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<VillageResult>.from(json["result"]!.map((x) => VillageResult.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class VillageResult {
    int? id;
    String? name;

    VillageResult({
        this.id,
        this.name,
    });

    factory VillageResult.fromJson(Map<String, dynamic> json) => VillageResult(
        id: json["id"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
    };
}
