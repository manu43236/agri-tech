import 'package:get/get.dart';

import '../controllers/saved_farmers_controller.dart';

class SavedFarmersBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SavedFarmersController>(
      () => SavedFarmersController(),
    );
  }
}
