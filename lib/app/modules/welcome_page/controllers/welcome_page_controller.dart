import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

class WelcomePageController extends BaseController {
  var output = "Agri Insights".obs;
  var welcomeFarmer='assets/images/welcomeFarmer.png';

  @override
  void onInit() {
    //sharedpref();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
