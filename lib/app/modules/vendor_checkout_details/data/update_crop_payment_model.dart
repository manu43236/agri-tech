import 'dart:convert';

/// Converts JSON string to `UpdateCropPaymentModel` object.
UpdateCropPaymentModel updateCropPaymentModelFromJson(String str) =>
    UpdateCropPaymentModel.fromJson(json.decode(str));

/// Converts `UpdateCropPaymentModel` object to JSON string.
String updateCropPaymentModelToJson(UpdateCropPaymentModel data) =>
    json.encode(data.toJson());

class UpdateCropPaymentModel {
  int? statusCode;
  dynamic status;
  dynamic message;
  Result? result;

  UpdateCropPaymentModel({
    this.statusCode,
    this.status,
    this.message,
    this.result,
  });

  factory UpdateCropPaymentModel.fromJson(Map<String, dynamic> json) =>
      UpdateCropPaymentModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result:
            json["result"] == null ? null : Result.fromJson(json["result"]),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
      };
}

class Result {
  int? id;
  dynamic quantity;
  dynamic remainingQuantity;
  dynamic perHectorcost;
  dynamic tenPercentPrice;
  dynamic tenPercentGst;
  dynamic tenPercentPayment;
  dynamic ninetyPercentPrice;
  dynamic nintyPercentGst;
  dynamic nintyPercentpayment;
  dynamic totalPayment;
  dynamic cropImageUrl;
  LandDetails? landDetails;

  Result({
    this.id,
    this.quantity,
    this.remainingQuantity,
    this.perHectorcost,
    this.tenPercentPrice,
    this.tenPercentGst,
    this.tenPercentPayment,
    this.ninetyPercentPrice,
    this.nintyPercentGst,
    this.nintyPercentpayment,
    this.totalPayment,
    this.cropImageUrl,
    this.landDetails,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"] is String ? int.tryParse(json["id"]) : json["id"],
        quantity: json["quantity"],
        remainingQuantity: json["remainingQuantity"],
        perHectorcost: json["perHectorcost"],
        tenPercentPrice: json["tenPercentPrice"],
        tenPercentGst: json["tenPercentGST"],
        tenPercentPayment: json["tenPercentPayment"],
        ninetyPercentPrice: json["ninetyPercentPrice"],
        nintyPercentGst: json["nintyPercentGST"],
        nintyPercentpayment: json["nintyPercentpayment"],
        totalPayment: json["totalPayment"],
        cropImageUrl: json["cropImageUrl"],
        landDetails: json["landDetails"] == null
            ? null
            : LandDetails.fromJson(json["landDetails"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "quantity": quantity,
        "remainingQuantity": remainingQuantity,
        "perHectorcost": perHectorcost,
        "tenPercentPrice": tenPercentPrice,
        "tenPercentGST": tenPercentGst,
        "tenPercentPayment": tenPercentPayment,
        "ninetyPercentPrice": ninetyPercentPrice,
        "nintyPercentGST": nintyPercentGst,
        "nintyPercentpayment": nintyPercentpayment,
        "totalPayment": totalPayment,
        "cropImageUrl": cropImageUrl,
        "landDetails": landDetails?.toJson(),
      };
}

class LandDetails {
  int? id;
  int? landLordId;
  dynamic farmerId;
  dynamic fullName;
  dynamic landInAcres;
  dynamic surveyNum;
  dynamic water;
  dynamic village;
  dynamic description;
  dynamic mandal;
  dynamic district;
  dynamic state;
  dynamic latitude;
  dynamic longitude;
  dynamic radius;
  int? imageId;
  dynamic imageUrl;
  dynamic status;
  bool? flag;
  dynamic createdAt;
  dynamic updatedAt;

  LandDetails({
    this.id,
    this.landLordId,
    this.farmerId,
    this.fullName,
    this.landInAcres,
    this.surveyNum,
    this.water,
    this.village,
    this.description,
    this.mandal,
    this.district,
    this.state,
    this.latitude,
    this.longitude,
    this.radius,
    this.imageId,
    this.imageUrl,
    this.status,
    this.flag,
    this.createdAt,
    this.updatedAt,
  });

  factory LandDetails.fromJson(Map<String, dynamic> json) => LandDetails(
        id: json["id"] is String ? int.tryParse(json["id"]) : json["id"],
        landLordId: json["landLordId"] is String
            ? int.tryParse(json["landLordId"])
            : json["landLordId"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        landInAcres: json["landInAcres"],
        surveyNum: json["surveyNum"],
        water: json["water"],
        village: json["village"],
        description: json["description"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        radius: json["radius"],
        imageId: json["image_id"] is String
            ? int.tryParse(json["image_id"])
            : json["image_id"],
        imageUrl: json["image_url"],
        status: json["status"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.tryParse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.tryParse(json["updatedAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "fullName": fullName,
        "landInAcres": landInAcres,
        "surveyNum": surveyNum,
        "water": water,
        "village": village,
        "description": description,
        "mandal": mandal,
        "district": district,
        "state": state,
        "latitude": latitude,
        "longitude": longitude,
        "radius": radius,
        "image_id": imageId,
        "image_url": imageUrl,
        "status": status,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
      };
}
