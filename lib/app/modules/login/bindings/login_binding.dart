
import 'package:get/get.dart';

import '../../translator_icon/controllers/translator_icon_controller.dart';
import '../controllers/login_controller.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginController>(
      () => LoginController(),
    );
    Get.lazyPut<TranslatorIconController>(
      () => TranslatorIconController(),
    );
  }
}
