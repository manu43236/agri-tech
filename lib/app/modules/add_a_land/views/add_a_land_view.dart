// import 'package:agritech/app/routes/app_pages.dart';
// import 'package:agritech/core/conts/color_consts.dart';
// import 'package:agritech/core/themes/text_styles.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import '../controllers/add_a_land_controller.dart';

// class AddALandView extends GetView<AddALandController> {
//   const AddALandView({Key? key}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return 
//        const Scaffold(
//         body: AddALandFormView(),
//     );
//   }
// }

// class AddALandFormView extends GetView<AddALandController> {
//   const AddALandFormView({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: Column(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               SizedBox(
//                 height: 30,
//               ),
//               Text(
//                 'Add a Land',
//                 style: TextStyles.kTSFS24W600.copyWith(color: colorBlack),
//               ),
//               Expanded(
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Text('No Lands',style: TextStyles.kTSFS18W500.copyWith(color: colorHello),),
//                     SizedBox(height: 10,),
//                     InkWell(
//                       onTap: () {
//                         Get.toNamed(Routes.ADD_LAND);
//                       },
//                       child: Text('Add Your Land here',style: TextStyles.kTSFS18W500.copyWith(color: colorBlue,decoration: TextDecoration.underline),)
//                     ),
//                     SizedBox(height: 50,),
//                     Text('No Job',style: TextStyles.kTSFS18W500.copyWith(color: colorHello),),
//                     SizedBox(height: 10,),
//                     InkWell(
//                       onTap: () {
//                         Get.toNamed(Routes.ADD_JOB);
//                       },
//                       child: Text('Add Job here',style: TextStyles.kTSFS18W500.copyWith(color: colorBlue,decoration: TextDecoration.underline),)
//                     ),
//                   ],
//                 ),
        
//               ),
//             ],
//           ),
//     );
//   }
// }



import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/add_a_land_controller.dart';

class AddALandView extends GetView<AddALandController> {
  const AddALandView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("add_land".tr),
      ),
      body: Column(
        children: [
          SizedBox(height: 30),
          Text('add_a_land'.tr, style: TextStyles.kTSFS24W600.copyWith(color: colorBlack)),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Obx(() {
                  return InkWell(
                    onTap: () {
                      controller.toggleAddLand(); // Toggles the visibility of the form
                    },
                    child: Text(
                      controller.showAddLand.value ? 'close'.tr : 'add_your_land_here'.tr,
                      style: TextStyles.kTSFS18W500.copyWith(
                        color: colorBlue,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  );
                }),
                SizedBox(height: 50),
                Text('no_job'.tr, style: TextStyles.kTSFS18W500.copyWith(color: colorHello)),
                SizedBox(height: 10),
                InkWell(
                  onTap: () {
                    Get.toNamed(Routes.ADD_JOB);
                  },
                  child: Text(
                    'add_job_here'.tr,
                    style: TextStyles.kTSFS18W500.copyWith(color: colorBlue, decoration: TextDecoration.underline),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

// class AddALandFormView extends GetView<AddALandController> {
//   const AddALandFormView({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.center,
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           SizedBox(height: 30),
//           Text('Add a Landssss', style: TextStyles.kTSFS24W600.copyWith(color: colorBlack)),
//           Expanded(
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 // State Field
//                 TextField(
//                   controller: controller.stateController,
//                   decoration: InputDecoration(labelText: 'States'),style: TextStyles.kTSCF12W500,
//                 ),
//                 SizedBox(height: 10),
//                 // District Field
//                 TextField(
//                   controller: controller.districtController,
//                   decoration: InputDecoration(labelText: 'District'),
//                 ),
//                 SizedBox(height: 10),
//                 // Village Field
//                 TextField(
//                   controller: controller.villageController,
//                   decoration: InputDecoration(labelText: 'Village'),
//                 ),
//                 SizedBox(height: 10),
//                 // Mandal Field
//                 TextField(
//                   controller: controller.mandalController,
//                   decoration: InputDecoration(labelText: 'Mandal'),
//                 ),
//                 SizedBox(height: 20),
//                 // Image Picker logic (Ensure this is properly reset too)
//                 Obx(() {
//                   return controller.image.value != null
//                       ? Image.file(File(controller.image.value!))
//                       : Text('No Image Selected');
//                 }),
//                 SizedBox(height: 20),
//                 // Reset Button or Add Land Button
//                 ElevatedButton(
//                   onPressed: () {
//                     controller.toggleAddLand();  // Toggle form visibility and reset fields
//                   },
//                   child: Text(controller.showAddLand.value ? 'Close' : 'Add Land'),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
