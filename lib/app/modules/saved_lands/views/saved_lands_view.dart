import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/saved_lands_controller.dart';

class SavedLandsView extends GetView<SavedLandsController> {
  const SavedLandsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'saved_lands'.tr,
          style: TextStyles.kTSFS20W500
              .copyWith(color: colorBlack, fontWeight: FontWeight.w400),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: Obx(
          () => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: controller.allSavedLandsModel.value.result != null &&
                        controller
                                .allSavedLandsModel.value.result!.savedLands !=
                            null &&
                        controller.allSavedLandsModel.value.result!.savedLands!
                            .isNotEmpty
                    ? ListView.separated(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        itemCount: controller.allSavedLandsModel.value.result!
                            .savedLands!.length,
                        separatorBuilder: (context, index) =>
                            const SizedBox(height: 10),
                        itemBuilder: (context, index) {
                          final savedLand = controller.allSavedLandsModel.value
                              .result!.savedLands![index];
                          return InkWell(
                            onTap: () async {
                              Get.find<HomeController>().landId.value =
                                  savedLand.landid.toString();
                              print(
                                  'id: ${Get.find<HomeController>().landId.value}');
                              //await Get.find<HomeController>().getIdLands();
                              Get.toNamed(Routes.DETAIL_SAVED_LANDS,arguments: {"landId": Get.find<HomeController>().landId.value});
                              // if(controller.role.value=='Landlord'){
                              //   print('========landlord');
                              //    Get.toNamed(Routes.DETAIL_SAVED_LANDS);
                              // }else if(controller.role.value=='Farmer'){
                              //   print('=========farmer');
                              //   Get.toNamed(Routes.DETAIL_SAVED_LANDS);
                              // }
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(11),
                                      topRight: Radius.circular(11)),
                                  child: Image.network(
                                    savedLand.imageUrl ?? '',
                                    height: Get.height * 0.2,
                                    width: Get.width,
                                    fit: BoxFit.cover,
                                    errorBuilder: (context, error, stackTrace) {
                                      return Image.asset(
                                        defaultImageUrl,
                                        height: Get.height * 0.2,
                                        width: Get.width,
                                        fit: BoxFit.cover,
                                      );
                                    },
                                  ),
                                ),
                                Container(
                                  width: Get.width,
                                  decoration: const BoxDecoration(
                                    color: colorAsh,
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(11),
                                        bottomRight: Radius.circular(11)),
                                    shape: BoxShape.rectangle,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${savedLand.fullName} ${'at'.tr} ${savedLand.village ?? ''}",
                                          style: TextStyles.kTSDS14W500
                                              .copyWith(color: colorHello),
                                        ),
                                        const SizedBox(height: 5),
                                        Text(
                                          '${'extent'.tr}${savedLand.landInAcres ?? 'NA'} ${'acres'.tr}      ${'water_facility'.tr}${savedLand.water ?? 'No'}',
                                          style: TextStyles.kTSFS10W500
                                              .copyWith(color: colorHello),
                                        ),
                                        const SizedBox(height: 5),
                                        // InkWell(
                                        //   onTap: () {},
                                        //   child: Container(
                                        //     padding: const EdgeInsets.symmetric(
                                        //         horizontal: 10, vertical: 5),
                                        //     decoration:
                                        //         const BoxDecoration(color: colorCost),
                                        //     child: const Text(
                                        //       'Cost Enquiry',
                                        //       style: TextStyle(
                                        //         color: colorWhite,
                                        //         fontSize: 6,
                                        //       ),
                                        //     ),
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      )
                    :  Center(child: Text('no_lands_saved'.tr)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
