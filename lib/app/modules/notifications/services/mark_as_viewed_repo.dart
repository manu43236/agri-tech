import 'package:dartz/dartz.dart';

abstract class MarkAsViewedRepo{
  Future<Either<dynamic,Exception>>  viewedNotifications({params1});
}