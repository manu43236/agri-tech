// To parse this JSON data, do
//
//     final getNotificationByLandlordIdModel = getNotificationByLandlordIdModelFromJson(jsonString);

import 'dart:convert';

GetNotificationByLandlordIdModel getNotificationByLandlordIdModelFromJson(String str) => GetNotificationByLandlordIdModel.fromJson(json.decode(str));

String getNotificationByLandlordIdModelToJson(GetNotificationByLandlordIdModel data) => json.encode(data.toJson());

class GetNotificationByLandlordIdModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<NfResult>? result;

    GetNotificationByLandlordIdModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetNotificationByLandlordIdModel.fromJson(Map<String, dynamic> json) => GetNotificationByLandlordIdModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<NfResult>.from(json["result"]!.map((x) => NfResult.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class NfResult {
    int? id;
    dynamic message;
    dynamic name;
    dynamic from;
    dynamic to;
    bool? isRead;
    dynamic statusForLandlord;
    dynamic statusForFarmer;
    dynamic imageUrl;
    int? landId;
    int? jobId;
    dynamic statusForFarmworker;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;

    NfResult({
        this.id,
        this.message,
        this.name,
        this.from,
        this.to,
        this.isRead,
        this.statusForLandlord,
        this.statusForFarmer,
        this.imageUrl,
        this.landId,
        this.jobId,
        this.statusForFarmworker,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory NfResult.fromJson(Map<String, dynamic> json) => NfResult(
        id: json["id"],
        message: json["message"],
        name: json["name"],
        from: json["from"],
        to: json["to"],
        isRead: json["is_read"],
        statusForLandlord: json["status_for_landlord"],
        statusForFarmer: json["status_for_farmer"],
        imageUrl: json["image_url"],
        landId: json["landId"],
        jobId: json["jobId"],
        statusForFarmworker: json["status_for_farmworker"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "message": message,
        "name": name,
        "from": from,
        "to": to,
        "is_read": isRead,
        "status_for_landlord": statusForLandlord,
        "status_for_farmer": statusForFarmer,
        "image_url": imageUrl,
        "landId": landId,
        "jobId": jobId,
        "status_for_farmworker": statusForFarmworker,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
