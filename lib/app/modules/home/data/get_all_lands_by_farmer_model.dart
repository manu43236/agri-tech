// To parse this JSON data, do
//
//     final getAllLandsByFarmerModel = getAllLandsByFarmerModelFromJson(jsonString);

import 'dart:convert';

GetAllLandsByFarmerModel getAllLandsByFarmerModelFromJson(String str) => GetAllLandsByFarmerModel.fromJson(json.decode(str));

String getAllLandsByFarmerModelToJson(GetAllLandsByFarmerModel data) => json.encode(data.toJson());

class GetAllLandsByFarmerModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetAllLandsByFarmerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetAllLandsByFarmerModel.fromJson(Map<String, dynamic> json) => GetAllLandsByFarmerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<dynamic>? completedLands;
    List<OngoingLand>? ongoingLands;
    List<dynamic>? notstartedLands;

    Result({
        this.completedLands,
        this.ongoingLands,
        this.notstartedLands,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        completedLands: json["completedLands"] == null ? [] : List<dynamic>.from(json["completedLands"]!.map((x) => x)),
        ongoingLands: json["ongoingLands"] == null ? [] : List<OngoingLand>.from(json["ongoingLands"]!.map((x) => OngoingLand.fromJson(x))),
        notstartedLands: json["notstartedLands"] == null ? [] : List<dynamic>.from(json["notstartedLands"]!.map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "completedLands": completedLands == null ? [] : List<dynamic>.from(completedLands!.map((x) => x)),
        "ongoingLands": ongoingLands == null ? [] : List<dynamic>.from(ongoingLands!.map((x) => x.toJson())),
        "notstartedLands": notstartedLands == null ? [] : List<dynamic>.from(notstartedLands!.map((x) => x)),
    };
}

class OngoingLand {
    int? id;
    dynamic landLordId;
    int? farmerId;
    dynamic fullName;
    dynamic landInAcres;
    dynamic surveyNum;
    dynamic water;
    dynamic village;
    dynamic description;
    dynamic mandal;
    dynamic district;
    dynamic state;
    dynamic latitude;
    dynamic longitude;
    dynamic radius;
    int? imageId;
    dynamic imageUrl;
    dynamic status;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;

    OngoingLand({
        this.id,
        this.landLordId,
        this.farmerId,
        this.fullName,
        this.landInAcres,
        this.surveyNum,
        this.water,
        this.village,
        this.description,
        this.mandal,
        this.district,
        this.state,
        this.latitude,
        this.longitude,
        this.radius,
        this.imageId,
        this.imageUrl,
        this.status,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory OngoingLand.fromJson(Map<String, dynamic> json) => OngoingLand(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        landInAcres: json["landInAcres"],
        surveyNum: json["surveyNum"],
        water: json["water"],
        village: json["village"],
        description: json["description"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        radius: json["radius"],
        imageId: json["image_id"],
        imageUrl: json["image_url"],
        status: json["status"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "fullName": fullName,
        "landInAcres": landInAcres,
        "surveyNum": surveyNum,
        "water": water,
        "village": village,
        "description": description,
        "mandal": mandal,
        "district": district,
        "state": state,
        "latitude": latitude,
        "longitude": longitude,
        "radius": radius,
        "image_id": imageId,
        "image_url": imageUrl,
        "status": status,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
