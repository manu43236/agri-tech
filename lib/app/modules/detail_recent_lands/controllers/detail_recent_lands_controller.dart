import 'package:agritech/app/modules/detail_recent_lands/services/detail_recent_lands_repo_impl.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:get/get.dart';

import '../../home/data/recent_lands_model.dart';
import '../../home/services/land_by_id_repo_impl.dart';
import '../../saved/data/get_all_saved_lands_model.dart';
import '../../saved/services/saved_repo_impl.dart';
import '../data/land_by_id_model.dart';

class DetailRecentLandsController extends BaseController {
  Rx<RecentLandsModel> recentFilterLandsModel = Rx(RecentLandsModel());
  Rx<AllSavedLandsModel> allSavedLandsModel = Rx(AllSavedLandsModel());
  Rx<LandByIdModel> landByIdModel = Rx(LandByIdModel());

  final HomeController homeController = Get.find<HomeController>();

  Rx<List<RecentResult>> suggestLands = Rx([]);

  var suggestStatus = ApiStatus.LOADING.obs;
  var id=''.obs;
  var apiLandIdStatus = ApiStatus.LOADING.obs;
  var isFavourite = false.obs;
  var deleteId=0.obs;
  var landId = "".obs;

  @override
  void onInit() async {
    super.onInit();
    id.value= await SecureStorage().readData(key: "id"??'');
    if (Get.arguments != null) {
      await allLandsSaved(id: id.value);
      await getIdLands(id: Get.arguments['landId']).then((val) {
        suggestedLands(
            state:
                landByIdModel.value.result!.landDetails!.state,
            district: 
                landByIdModel.value.result!.landDetails!.district,
            mandal:
                landByIdModel.value.result!.landDetails!.mandal);
      });
    }
  }

  onRefresh({landId}) async {
    await allLandsSaved(id: id.value);
    getIdLands(id: landId).then((val) {
      suggestedLands(
          state: landByIdModel.value.result!.landDetails!.state,
          district:
              landByIdModel.value.result!.landDetails!.district,
          mandal:
              landByIdModel.value.result!.landDetails!.mandal);
    });
  }

  Future<void> suggestedLands({state, district, mandal}) async {
    suggestStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await DetailRecentLandsRepoImpl(dioClient).filterLocations(params1: {
        "state": state,
        "district": district,
        "mandal": mandal,
        //"village": villageName.value
      });

      result.fold((left) {
        suggestedLandsLocationHandleResponse(left);
        suggestStatus.value = ApiStatus.SUCCESS;
      }, (right) {
        suggestStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print("error: $e");
      apiStatus.value = ApiStatus.FAIL;
    }
  }

void suggestedLandsLocationHandleResponse(RecentLandsModel model) {
  if (model.statusCode == 200) {
    // Filter lands excluding the current landId
    var filteredLands = model.result!
        .where((land) => land.id.toString() != landId.value)
        .toList();

    if (filteredLands.isNotEmpty) {
      suggestLands.value = filteredLands;
    } else {
      print('No lands match the criteria.');
      suggestLands.value = [];
    }
  } else {
    print("Unexpected status code: ${model.statusCode}");
    suggestLands.value = [];
  }
}

   //get all saved lands API
  Future<void> allLandsSaved({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient)
          .allSavedLands(userId: id);
      print("===================== get all saved lands ==============");
      print(result);
      result.fold((model) {
        allSavedLandsModel.value = model;
        print(allSavedLandsModel.value.result!.landCount!);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  // Lands BY id API
  Future<void> getIdLands({id}) async {
    apiLandIdStatus.value = ApiStatus.LOADING;
    try {
      var result = await LandByIdRepoImpl(dioClient).getLand(params1: {
        "id": id,
      });

      result.fold((model) {
        landByIdModel.value = model;
        landId.value = model.result!.landDetails!.id.toString() ?? "";
        if (model.statusCode == 200) {
          isFavourite.value = false;
          if (allSavedLandsModel
                      .value
                      .result !=
                  null &&
              allSavedLandsModel
                      .value
                      .result!
                      .savedLands !=
                  null) {
            for (var element in allSavedLandsModel
                .value
                .result!
                .savedLands!) {
              if (element.landid == model.result!.landDetails!.id) {
                deleteId.value = element.id!;
                isFavourite.value = true;
              }
            }
          }
        }
        apiLandIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiLandIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
