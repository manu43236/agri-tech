import 'package:agritech/app/modules/saved_utilities/services/saved_utilities_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../data/get_all_saved_utility_model.dart';

class SavedUtilitiesRepoImpl extends SavedUtilitiesRepo with NetworkCheckService{

  final DioClient _dioClient;
  SavedUtilitiesRepoImpl(this._dioClient);

  @override
  Future<Either<GetAllSavedUtilityModel,Exception>> getSavedUtilities({userId})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("NO Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('save/getSavedUtilities?userid=$userId', Method.get);
        return result.fold((l){
          GetAllSavedUtilityModel model=GetAllSavedUtilityModel.fromJson(l.data);
          return Left(model);
        },(r)=>Right(Exception(r)));
      }catch(e){
        return right(Exception(e));
      }
    }
  }
}