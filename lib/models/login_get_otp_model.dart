class LoginGetOtpModel {
  int? statusCode;
  String? status;
  String? message;
  Result? result;

  LoginGetOtpModel({this.statusCode, this.status, this.message, this.result});

  LoginGetOtpModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    status = json['status'];
    message = json['message'];
    result =
        json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.result != null) {
      data['result'] = this.result!.toJson();
    }
    return data;
  }
}

class Result {
  User? user;

  Result({this.user});

  Result.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class User {
  int? id;
  String? firstName;
  String? mobileNumber;
  String? role;
  int? age;
  String? gender;
  String? address;
  String? village;
  String? mandal;
  String? district;
  String? state;
  String? otp;
  dynamic? longitude;
  dynamic? latitude;
  String? accesstoken;
  int? profileId;
  String? imageUrl;
  String? createdAt;
  String? updatedAt;

  User(
      {this.id,
      this.firstName,
      this.mobileNumber,
      this.role,
      this.age,
      this.gender,
      this.address,
      this.village,
      this.mandal,
      this.district,
      this.state,
      this.otp,
      this.longitude,
      this.latitude,
      this.accesstoken,
      this.profileId,
      this.imageUrl,
      this.createdAt,
      this.updatedAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    mobileNumber = json['mobileNumber'];
    role = json['role'];
    age = json['age'];
    gender = json['gender'];
    address = json['address'];
    village = json['village'];
    mandal = json['mandal'];
    district = json['district'];
    state = json['state'];
    otp = json['otp'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    accesstoken = json['accesstoken'];
    profileId = json['profileId'];
    imageUrl = json['image_url'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['mobileNumber'] = this.mobileNumber;
    data['role'] = this.role;
    data['age'] = this.age;
    data['gender'] = this.gender;
    data['address'] = this.address;
    data['village'] = this.village;
    data['mandal'] = this.mandal;
    data['district'] = this.district;
    data['state'] = this.state;
    data['otp'] = this.otp;
    data['longitude'] = this.longitude;
    data['latitude'] = this.latitude;
    data['accesstoken'] = this.accesstoken;
    data['profileId'] = this.profileId;
    data['image_url'] = this.imageUrl;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}
