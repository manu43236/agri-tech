// To parse this JSON data, do
//
//     final addSkillModel = addSkillModelFromJson(jsonString);

import 'dart:convert';

AddSkillModel addSkillModelFromJson(String str) => AddSkillModel.fromJson(json.decode(str));

String addSkillModelToJson(AddSkillModel data) => json.encode(data.toJson());

class AddSkillModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    AddSkillModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory AddSkillModel.fromJson(Map<String, dynamic> json) => AddSkillModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    int? userId;
    dynamic skill;
    dynamic experience;
    dynamic description;
    dynamic updatedAt;
    dynamic createdAt;

    Result({
        this.id,
        this.userId,
        this.skill,
        this.experience,
        this.description,
        this.updatedAt,
        this.createdAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        userId: json["userId"],
        skill: json["skill"],
        experience: json["experience"],
        description: json["description"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "skill": skill,
        "experience": experience,
        "description": description,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
    };
}
