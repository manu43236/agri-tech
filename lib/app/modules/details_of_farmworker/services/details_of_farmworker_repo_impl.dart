import 'package:agritech/app/modules/details_of_farmworker/services/details_of_farmworker_repo.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../data/get_all_available_jobs_model.dart';
import '../data/near_by_farmworkers_model.dart';
import '../data/send_work_notification_by_landlord_or_farmer_fetch_model.dart';



class DetailsOfFarmworkerRepoImpl extends DetailsOfFarmworkerRepo with NetworkCheckService{
  final DioClient _dioClient;
  DetailsOfFarmworkerRepoImpl(this._dioClient);
  @override
  Future<Either<NearByFarmworkersModel,Exception>> nearByFarmworkers({latitude,longitude})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Land/filterNearFarmworker?latitude=$latitude&longitude=$longitude', Method.get);
        return result.fold((l){
          NearByFarmworkersModel model= NearByFarmworkersModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetAvailableJobsModel,Exception>> availableJobs({userId})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Jobs/getAvailableJobs?userId=$userId', Method.get);
        return result.fold((l){
          GetAvailableJobsModel model= GetAvailableJobsModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel,Exception>>  fetchFarmworkers({params1})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('notification/sendWorkNotificationByFarmerOrLandlordToFarmworker', Method.post,params: params1);
        return result.fold((l){
          SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel model= SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}