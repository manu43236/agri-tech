import 'package:dartz/dartz.dart';

import '../../home/data/all_lands_of_landlord.dart';

abstract class AvailableMyLandsRepo{
  Future<Either<AddLandsOfLandlordModel,Exception>> getLandlordLands();
}