
import 'package:agritech/app/modules/profile/data/get_user_roles_model.dart';
import 'package:agritech/models/profile_model.dart';
import 'package:dartz/dartz.dart';

abstract class ProfileRepo{
  Future<Either<ProfileModel,Exception>> doProfile({params1});
  Future<Either<GetUserRolesModel,Exception>> userRoles();
}