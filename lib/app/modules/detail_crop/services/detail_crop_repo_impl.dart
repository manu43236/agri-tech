import 'package:agritech/app/modules/detail_crop/services/detail_crop_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../data/create_crop_payment_model.dart';
import '../data/get_crop_availability_by_yield_id_model.dart';
import '../data/get_crop_payment_details_model.dart';

class DetailCropRepoImpl extends DetailCropRepo with NetworkCheckService{
  final DioClient _dioClient;
  DetailCropRepoImpl(this._dioClient);

  @override
  Future<Either<GetCropAvailabilityByYieldIdModel,Exception>> cropDetails({yieldId})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Vender/getCropAvailabilityByYieldId?yieldId=$yieldId', Method.get);
        return result.fold((l){
          GetCropAvailabilityByYieldIdModel model= GetCropAvailabilityByYieldIdModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }  

  @override
  Future<Either<CreateCropPaymentModel,Exception>> createCropPayment({params1})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Vender/createCropPayment', Method.post,params: params1);
        return result.fold((l){
          CreateCropPaymentModel model= CreateCropPaymentModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }  

  @override
  Future<Either<GetCropPaymentDetailsModel,Exception>> getCropPaymentDetails({id})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Vender/getCropPaymentDetails?id=$id', Method.get);
        return result.fold((l){
          GetCropPaymentDetailsModel model= GetCropPaymentDetailsModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}