import 'package:get/get.dart';

import '../controllers/detail_saved_lands_controller.dart';

class DetailSavedLandsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailSavedLandsController>(
      () => DetailSavedLandsController(),
    );
  }
}
