// To parse this JSON data, do
//
//     final recentLandsModel = recentLandsModelFromJson(jsonString);

import 'dart:convert';

RecentLandsModel recentLandsModelFromJson(String str) => RecentLandsModel.fromJson(json.decode(str));

String recentLandsModelToJson(RecentLandsModel data) => json.encode(data.toJson());

class RecentLandsModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<RecentResult>? result;

    RecentLandsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory RecentLandsModel.fromJson(Map<String, dynamic> json) => RecentLandsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<RecentResult>.from(json["result"]!.map((x) => RecentResult.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class RecentResult {
    int? id;
    int? landLordId;
    int? farmerId;
    dynamic fullName;
    dynamic landInAcres;
    dynamic surveyNum;
    dynamic water;
    dynamic village;
    dynamic description;
    dynamic mandal;
    dynamic district;
    dynamic state;
    dynamic latitude;
    dynamic longitude;
    dynamic radius;
    int? imageId;
    dynamic imageUrl;
    dynamic status;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;
    dynamic profile;

    RecentResult({
        this.id,
        this.landLordId,
        this.farmerId,
        this.fullName,
        this.landInAcres,
        this.surveyNum,
        this.water,
        this.village,
        this.description,
        this.mandal,
        this.district,
        this.state,
        this.latitude,
        this.longitude,
        this.radius,
        this.imageId,
        this.imageUrl,
        this.status,
        this.flag,
        this.createdAt,
        this.updatedAt,
        this.profile,
    });

    factory RecentResult.fromJson(Map<String, dynamic> json) => RecentResult(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        landInAcres: json["landInAcres"]?.toDouble(),
        surveyNum: json["surveyNum"],
        water: json["water"],
        village: json["village"],
        description: json["description"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        radius: json["radius"],
        imageId: json["image_id"],
        imageUrl: json["image_url"],
        status: json["status"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        profile: json["profile"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "fullName": fullName,
        "landInAcres": landInAcres,
        "surveyNum": surveyNum,
        "water": water,
        "village": village,
        "description": description,
        "mandal": mandal,
        "district": district,
        "state": state,
        "latitude": latitude,
        "longitude": longitude,
        "radius": radius,
        "image_id": imageId,
        "image_url": imageUrl,
        "status": status,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "profile": profile,
    };
}
