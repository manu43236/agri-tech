import 'package:agritech/app/modules/home/services/save_farmworker_as_favourite_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import '../../../../models/save_farmworker_as_favourite_model.dart';

class SaveFarmworkerAsFavouriteRepoImpl extends SaveFarmworkerAsFavouriteRepo with NetworkCheckService{
  final DioClient _dioClient;
  SaveFarmworkerAsFavouriteRepoImpl(this._dioClient);
  @override
  Future<Either<SaveFarmworkerAsFavouriteModel,Exception>> farmWorkerSaved({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('save/saveFarmerWorkerAsFavorite', Method.post,params: params1);
        return result.fold((l){
          SaveFarmworkerAsFavouriteModel model=SaveFarmworkerAsFavouriteModel.fromJson(l.data);
          return Left(model);
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}