import 'package:agritech/app/modules/home/services/job_by_id_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:agritech/models/job_by_id_model.dart';
import 'package:dartz/dartz.dart';

class JobByIdRepoImpl extends JobByIdRepo with NetworkCheckService{
  final DioClient _dioClient;
  JobByIdRepoImpl(this._dioClient);
  @override
  Future<Either<JobByIdModel,Exception>> getJob({id})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("NO Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Jobs/getJobById?id=$id', Method.get,);
        return result.fold((l){
          JobByIdModel model=JobByIdModel.fromJson(l.data);
          return Left(model);
        },(r)=>Right(Exception(r)));
      }catch(e){
        return right(Exception(e));
      }
    }
  }
}