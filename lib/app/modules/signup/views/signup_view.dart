import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../translator_icon/views/translator_icon_view.dart';
import '../controllers/signup_controller.dart';

class SignupView extends GetView<SignupController> {
  SignupView({Key? key}) : super(key: key);

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Form(
      key: _formKey,
      child: Column(
        children: [
          Stack(
            children: [
              SizedBox(
                  width: Get.width,
                  child: Image.asset(
                    appBarLogo,
                    fit: BoxFit.cover,
                  )),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 60),
                child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "sign_up".tr,
                      style:
                          TextStyles.kTSFS26W400.copyWith(color: Colors.white),
                    )),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 63,bottom: 60,left:280) ,
                child: InkWell(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return const TranslatorIconView();
                        },
                      );
                    },
                    child: Image.asset(
                      translatorIcon,
                      height: 35,
                    )),
              ),
          //      IconButton(
          //   padding:const EdgeInsets.only(top: 63,bottom: 60,left:280) ,
          //   icon: Icon(Icons.language),
          //   color: colorWhite,
          //   onPressed: () {
          //     showDialog(
          //       context: context,
          //       builder: (context) {
          //         return const TranslatorIconView();
          //       },
          //     );
          //   },
          // ),
            ],
          ),
          Expanded(child: Image.asset(farmer)),
          Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: colorBlack.withOpacity(0.2),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: const Offset(0, -3),
                  ),
                ],
                color: colorGhostWhite,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(27),
                    topRight: Radius.circular(27)),
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'phone_no'.tr,
                      style: TextStyles.kTSFS16W600.copyWith(color: colorBlack),
                    ),
                    TextFormField(
                      controller: controller.mobileController.value,
                      keyboardType: TextInputType.phone,
                      inputFormatters: [LengthLimitingTextInputFormatter(10)],
                      decoration: InputDecoration(
                        prefixIconConstraints:
                            const BoxConstraints(minWidth: 50, maxHeight: 50),
                        prefixIcon: SizedBox(
                          width: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Image.asset(telephone),
                              const SizedBox(
                                  height: 20,
                                  child: VerticalDivider(
                                    color: colorBlack,
                                    thickness: 1,
                                    width: 30,
                                  )),
                            ],
                          ),
                        ),
                        hintText: '+91 000-00-000'.tr,
                        hintStyle: TextStyles.kTSFS14WNORM
                            .copyWith(color: Colors.grey),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'required_mob_num'.tr;
                        } else if (value.length < 10) {
                          return 'enter_valid_mob_num'.tr;
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 60,
                    ),
                    Center(
                      child: InkWell(
                        onTap: () {
                          if (_formKey.currentState!.validate()) {
                            controller.doRegister();
                          }
                          //Get.toNamed(Routes.OTP);
                        },
                        child: Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(11),
                            color: primary,
                          ),
                          child: Text(
                            'get_otp'.tr,
                            style: TextStyles.kTSFS16W600
                                .copyWith(color: colorGhostWhite),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
