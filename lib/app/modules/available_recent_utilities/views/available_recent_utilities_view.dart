import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/text_fields/custom_search_widget.dart';
import '../../../routes/app_pages.dart';
import '../../select_region/views/select_region_view.dart';
import '../controllers/available_recent_utilities_controller.dart';

class AvailableRecentUtilitiesView extends GetView<AvailableRecentUtilitiesController> {
  const AvailableRecentUtilitiesView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'available_my_utilities'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
        actions: [
            InkWell(
              onTap: () {
                 Get.bottomSheet(
                isScrollControlled: true,
                isDismissible: true,
                SelectRegionView(type: 'recent_utilities'.tr,)
              );
              },
              child: Image.asset(
                cone,
                height: 20,
                width: 20,
              ),
            ),
            const SizedBox(
              width: 20,
            )
          ]
      ),
      body: Obx(
        () => Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomSearchField(
                  onChanged: (val) {
                    controller.availableRecentUtilSearch.value = val;
                    if (val.length >= 3 || val.isEmpty) {
                      controller.availableFilterRecentUtilities(search: controller.availableRecentUtilSearch.value,state: '',district: '',mandal: '',village: '');
                    }else if (val.isEmpty) {
                      controller.availableFilterRecentUtilities(search: controller.availableRecentUtilSearch.value,state: '',district: '',mandal: '',village: '');
                    }
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                controller.dataSelectedItem.value.isEmpty
                    ? const SizedBox()
                    : Text('${'your_selected_region_is'.tr} ${controller.dataSelectedItem.value}' ?? "", style: TextStyles.kTSDS14W500
                                          .copyWith(color: colorHello),),
                const SizedBox(height: 10),
                controller.apiFilterRecentUtilitiesStatus.value ==
                        ApiStatus.LOADING
                    ? Shimmers().getListShimmer()
                    : controller.getAvailableFilterRecentFourUtilitiesModel.value.result!.isEmpty
                        ?  Align(
                            alignment: Alignment.center,
                            child: Text(
                              "no_utilities_found".tr,
                              style:
                                  const TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                          )
                        :
                        Column(
                            children: _utilities(),
                          )
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _utilities() {
    List<Widget> items = [];
    for (var index = 0;
        index <
            controller
                .getAvailableFilterRecentFourUtilitiesModel.value.result!.length;
        index++) {
      items.add(InkWell(
        onTap: () async {
          Get.toNamed(Routes.MY_INSTRUMENTS_DETAILS, arguments: {
            "instrumentId": controller.getAvailableFilterRecentFourUtilitiesModel
                .value.result![index].id,
            "isLoan":controller.isLoan.value,
            "isInsurance":controller.isInsurance.value
          });
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(11), topRight: Radius.circular(11)),
              child: Image.network(
                controller.getAvailableFilterRecentFourUtilitiesModel.value
                            .result![index].image !=
                        null
                    ? controller.getAvailableFilterRecentFourUtilitiesModel.value
                        .result![index].image
                        .toString()
                    : '',
                height: Get.height * 0.2,
                width: Get.width,
                fit: BoxFit.cover,
                errorBuilder: (context, error, stackTrace) {
                  return Image.asset(
                    defaultImageUrl,
                    height: Get.height * 0.2,
                    width: Get.width,
                    fit: BoxFit.cover,
                  );
                },
              ),
            ),
            Container(
              width: Get.width,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(11),
                      bottomRight: Radius.circular(11)),
                  color: colorAsh,
                  shape: BoxShape.rectangle),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${controller.getAvailableFilterRecentFourUtilitiesModel.value.result![index].nameOfInstrument ?? ''}",
                      style: TextStyles.kTSDS14W500.copyWith(color: colorHello),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                      '${controller.getAvailableFilterRecentFourUtilitiesModel.value.result![index].village ?? ''}',
                      style: TextStyles.kTSFS12W500.copyWith(color: colorHello),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Text(
                          'status'.tr,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyles.kTSCF12W500
                              .copyWith(color: colorHello),
                        ),
                        Text(
                          '${controller.getAvailableFilterRecentFourUtilitiesModel.value.result![index].status == false ? "leased".tr : "available".tr}',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyles.kTSCF12W500.copyWith(
                              color: controller
                                          .getAvailableFilterRecentFourUtilitiesModel
                                          .value
                                          .result![index]
                                          .status ==
                                      false
                                  ? colorRed
                                  : colorGreen),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ));
    }
    return items;
  }
}
