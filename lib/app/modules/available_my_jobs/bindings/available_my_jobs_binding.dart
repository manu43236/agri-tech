import 'package:get/get.dart';

import '../../select_region/controllers/select_region_controller.dart';
import '../controllers/available_my_jobs_controller.dart';

class AvailableMyJobsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailableMyJobsController>(
      () => AvailableMyJobsController(),
    );
    Get.lazyPut<SelectRegionController>(
      () => SelectRegionController(),fenix: true
    );
  }
}
