// To parse this JSON data, do
//
//     final uploadIdDocumentModel = uploadIdDocumentModelFromJson(jsonString);

import 'dart:convert';

UploadIdDocumentModel uploadIdDocumentModelFromJson(String str) => UploadIdDocumentModel.fromJson(json.decode(str));

String uploadIdDocumentModelToJson(UploadIdDocumentModel data) => json.encode(data.toJson());

class UploadIdDocumentModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    UploadIdDocumentModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory UploadIdDocumentModel.fromJson(Map<String, dynamic> json) => UploadIdDocumentModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    dynamic selfie;
    dynamic voterId;
    dynamic voterIdStatus;
    dynamic adharId;
    dynamic adharIdStatus;
    dynamic panId;
    dynamic panIdStatus;

    Result({
        this.id,
        this.selfie,
        this.voterId,
        this.voterIdStatus,
        this.adharId,
        this.adharIdStatus,
        this.panId,
        this.panIdStatus,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        selfie: json["selfie"],
        voterId: json["voterId"],
        voterIdStatus: json["voterIdStatus"],
        adharId: json["adharId"],
        adharIdStatus: json["adharIdStatus"],
        panId: json["panId"],
        panIdStatus: json["panIdStatus"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "selfie": selfie,
        "voterId": voterId,
        "voterIdStatus": voterIdStatus,
        "adharId": adharId,
        "adharIdStatus": adharIdStatus,
        "panId": panId,
        "panIdStatus": panIdStatus,
    };
}
