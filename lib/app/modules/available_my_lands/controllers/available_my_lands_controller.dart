import 'package:agritech/app/modules/available_my_lands/services/available_my_lands_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';
import '../../available_recent_lands/data/recent_filter_model.dart';
import '../../available_recent_lands/services/available_recent_lands_repo_impl.dart';
import '../../home/controllers/home_controller.dart';
import '../../home/data/all_lands_of_landlord.dart';

class AvailableMyLandsController extends BaseController {

  Rx<AddLandsOfLandlordModel> getLandsOfLandlordModel =
      Rx(AddLandsOfLandlordModel());
  Rx<RecentFilterModel> myLandsFilterModel = Rx(RecentFilterModel());

  var isLoan=false.obs;
  var isInsurance=false.obs;

  var name = "".obs;
  var myCustomerSearchText = ''.obs;
  var dataSelectedItem = "".obs;
  var apiLordLandsStatus=ApiStatus.LOADING.obs;

  @override
  void onInit() {
    if (Get.arguments != null) {
      name.value = Get.arguments["heading"];
      isLoan.value=Get.arguments["isLoan"];
      isInsurance.value=Get.arguments["isInsurance"];
    }
    super.onInit();
    //getAllLandlordLands();
    myLandsFilterLandsList(landlordId:Get.find<HomeController>().userId.value,
    farmerId: '',search: myCustomerSearchText.value,state: '',district: '',mandal: '',village: '' );
  }

  // my Lands search View API
  Future<void> getAllLandlordLands() async {
    apiLordLandsStatus.value = ApiStatus.LOADING;
    try {
      //landlordId.value = Get.arguments as String;
      var result =
          await AvailableMyLandsRepoImpl(dioClient).getLandlordLands();
      print('llllllllllllllllllllllllllllllllllllllllll');
      print('results:$result');
      result.fold((left) {
        allLandshandleResponse(left);
        apiLordLandsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiLordLandsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void allLandshandleResponse(AddLandsOfLandlordModel model) async {
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
      getLandsOfLandlordModel.value.result?.ongoingLands?.clear();
      getLandsOfLandlordModel.value = model;
    }else if(model.statusCode==400){
      print('===============status=============');
      getLandsOfLandlordModel.value.result!.ongoingLands!.clear();
      getLandsOfLandlordModel.value.result!.ongoingLands ==[];
    }
  }

  void myLandsFilterLandsList({landlordId,farmerId,search,state,district,mandal,village}) async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result = await AvailableRecentLandsRepoImpl(dioClient).filterRecentLocations(landlordId: landlordId,farmerId: farmerId,search: search, state: state,district: district,mandal: mandal,village: village);
      print('=============== get all filter my lands list =========');
      print(result);
      result.fold((left) {
        recentFilterLandshandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void recentFilterLandshandleResponse(RecentFilterModel model) async {
    if (model.statusCode == 200) {
      myLandsFilterModel.value.result?.clear();
      myLandsFilterModel.value = model;
    }else if(model.statusCode==400){
      print('===============status=============');
      myLandsFilterModel.value.result?.clear();
      myLandsFilterModel.value.result! ==[];
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
