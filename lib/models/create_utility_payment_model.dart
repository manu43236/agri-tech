// To parse this JSON data, do
//
//     final createUtilityPaymentModel = createUtilityPaymentModelFromJson(jsonString);

import 'dart:convert';

CreateUtilityPaymentModel createUtilityPaymentModelFromJson(String str) => CreateUtilityPaymentModel.fromJson(json.decode(str));

String createUtilityPaymentModelToJson(CreateUtilityPaymentModel data) => json.encode(data.toJson());

class CreateUtilityPaymentModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    CreateUtilityPaymentModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory CreateUtilityPaymentModel.fromJson(Map<String, dynamic> json) => CreateUtilityPaymentModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    int? userId;
    int? utilityId;
    int? quantity;
    int? costOfSelectedItem;
    int? gst;
    int? totalPayment;
    bool? paymentStatus;
    dynamic razorpayOrderId;
    dynamic razorpayPaymentId;
    dynamic razorpaySignature;
    DateTime? createdAt;
    DateTime? updatedAt;
    UtilityDetails? utilityDetails;

    Result({
        this.id,
        this.userId,
        this.utilityId,
        this.quantity,
        this.costOfSelectedItem,
        this.gst,
        this.totalPayment,
        this.paymentStatus,
        this.razorpayOrderId,
        this.razorpayPaymentId,
        this.razorpaySignature,
        this.createdAt,
        this.updatedAt,
        this.utilityDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        userId: json["userId"],
        utilityId: json["utilityId"],
        quantity: json["quantity"],
        costOfSelectedItem: json["costOfSelectedItem"],
        gst: json["GST"],
        totalPayment: json["totalPayment"],
        paymentStatus: json["paymentStatus"],
        razorpayOrderId: json["razorpay_order_id"],
        razorpayPaymentId: json["razorpay_payment_id"],
        razorpaySignature: json["razorpay_signature"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        utilityDetails: json["utilityDetails"] == null ? null : UtilityDetails.fromJson(json["utilityDetails"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "utilityId": utilityId,
        "quantity": quantity,
        "costOfSelectedItem": costOfSelectedItem,
        "GST": gst,
        "totalPayment": totalPayment,
        "paymentStatus": paymentStatus,
        "razorpay_order_id": razorpayOrderId,
        "razorpay_payment_id": razorpayPaymentId,
        "razorpay_signature": razorpaySignature,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "utilityDetails": utilityDetails?.toJson(),
    };
}

class UtilityDetails {
    int? id;
    String? name;
    String? description;
    int? pricePerDay;
    Address? address;

    UtilityDetails({
        this.id,
        this.name,
        this.description,
        this.pricePerDay,
        this.address,
    });

    factory UtilityDetails.fromJson(Map<String, dynamic> json) => UtilityDetails(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        pricePerDay: json["pricePerDay"],
        address: json["address"] == null ? null : Address.fromJson(json["address"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "pricePerDay": pricePerDay,
        "address": address?.toJson(),
    };
}

class Address {
    String? state;
    String? district;
    String? mandal;
    String? village;

    Address({
        this.state,
        this.district,
        this.mandal,
        this.village,
    });

    factory Address.fromJson(Map<String, dynamic> json) => Address(
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
    );

    Map<String, dynamic> toJson() => {
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
    };
}
