import 'package:agritech/app/modules/detail_my_lands/controllers/detail_my_lands_controller.dart';
import 'package:agritech/app/modules/detail_recent_lands/controllers/detail_recent_lands_controller.dart';
import 'package:agritech/app/modules/details_of_landlord/controllers/details_of_landlord_controller.dart';
import 'package:agritech/app/modules/farm_worker_home_page/controllers/farm_worker_controller.dart';
import 'package:agritech/app/modules/filtered_utilities/controllers/filtered_utilities_controller.dart';
import 'package:agritech/app/modules/home/controllers/farmer_landlord_controller.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:agritech/app/modules/home/controllers/landlord_farmer_controller.dart';
import 'package:agritech/app/modules/my_instruments/controllers/my_instruments_controller.dart';
import 'package:agritech/app/modules/my_instruments_details/controllers/my_instruments_details_controller.dart';
import 'package:agritech/app/modules/suitable_crops/controllers/suitable_crops_controller.dart';
import 'package:agritech/app/modules/vendor/controllers/vendor_controller.dart';
import 'package:get/get.dart';

import '../../../../core/base/dropdowns/controller/district_dropdown_controller.dart';
import '../../../../core/base/dropdowns/controller/mandal_dropdown_controller.dart';
import '../../../../core/base/dropdowns/controller/state_dropdown_controller.dart';
import '../../../../core/base/dropdowns/controller/village_dropdown_controller.dart';
import '../../Landlord_utilities/controllers/landlord_utilities_controller.dart';
import '../../add_a_land/controllers/add_a_land_controller.dart';
import '../../available_leased_jobs/controllers/available_leased_jobs_controller.dart';
import '../../available_leased_lands/controllers/available_leased_lands_controller.dart';
import '../../available_leased_utilities/controllers/available_leased_utilities_controller.dart';
import '../../available_recent_lands/controllers/available_recent_lands_controller.dart';
import '../../details_of_farmer/controllers/details_of_farmer_controller.dart';
import '../../details_of_farmworker/controllers/details_of_farmworker_controller.dart';
import '../../details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import '../../home/controllers/landlord_worker_controller.dart';
import '../../instrument_details/controllers/instrument_details_controller.dart';
import '../../notifications/controllers/notifications_controller.dart';
import '../../profile/controllers/profile_controller.dart';
import '../../profile_page/controllers/profile_page_controller.dart';
import '../../saved_farmers/controllers/saved_farmers_controller.dart';
import '../../saved_farmworkers/controllers/saved_farmworkers_controller.dart';
import '../../saved_jobs/controllers/saved_jobs_controller.dart';
import '../../saved_landlords/controllers/saved_landlords_controller.dart';
import '../../saved_lands/controllers/saved_lands_controller.dart';
import '../../saved_utilities/controllers/saved_utilities_controller.dart';
import '../../translator_icon/controllers/translator_icon_controller.dart';
import '../../utility/controllers/utility_controller.dart';
import '../controllers/dashboard_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {

    Get.lazyPut<TranslatorIconController>(
      () => TranslatorIconController(),
    );

    Get.lazyPut<DashboardController>(
      () => DashboardController(),
    );
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<ProfilePageController>(
      () => ProfilePageController(),
    );
    Get.lazyPut<ProfileController>(
      () => ProfileController(),
    );
     Get.lazyPut<AddALandController>(
      () => AddALandController(),
    );
    Get.lazyPut<NotificationsController>(
      () => NotificationsController(),
    );
     Get.lazyPut<StateDropdownController>(
      () => StateDropdownController(),
    );
    Get.lazyPut<DistrictDropdownController>(
      () => DistrictDropdownController(),
    );
     Get.lazyPut<MandalDropdownController>(
      () => MandalDropdownController(),
    );
     Get.lazyPut<VillageDropdownController>(
      () => VillageDropdownController(),
    );
    Get.lazyPut<FarmWorkerHomePageController>(
      () => FarmWorkerHomePageController(),
    );
    Get.lazyPut<UtilityController>(
      ()=>UtilityController()
    );
    Get.lazyPut<VendorController>(
      ()=>VendorController()
    );
    Get.lazyPut<InstrumentDetailsController>(
      ()=>InstrumentDetailsController()
    );
    Get.lazyPut<FilteredUtilitiesController>(
      ()=>FilteredUtilitiesController()
    );
    Get.lazyPut<MyInstrumentsDetailsController>(
      ()=>MyInstrumentsDetailsController()
    );
     Get.lazyPut<DetailsOfLandlordUtilitiesController>(
      ()=>DetailsOfLandlordUtilitiesController()
    );
     Get.lazyPut<LandlordController>(
      ()=>LandlordController()
    );
     Get.lazyPut<DetailRecentLandsController>(
      ()=>DetailRecentLandsController()
    );
    // Get.lazyPut<SelectRegionController>(
    //   ()=>SelectRegionController()
    // );
     Get.lazyPut<AvailableRecentLandsController>(
      ()=>AvailableRecentLandsController()
    );
    Get.lazyPut<SuitableCropsController>(
      ()=>SuitableCropsController()
    );
    Get.lazyPut<MyInstrumentsController>(
      ()=>MyInstrumentsController()
    );
    Get.lazyPut<DetailMyLandsController>(
      ()=>DetailMyLandsController()
    );
    Get.lazyPut<LandlordFarmerController>(
      ()=>LandlordFarmerController()
    );
    Get.lazyPut<LandlordWorkerController>(
      ()=>LandlordWorkerController()
    );
     Get.lazyPut<FarmerLandlordController>(
      ()=>FarmerLandlordController()
    );
    Get.lazyPut<DetailsOfLandlordController>(
      ()=>DetailsOfLandlordController()
    );
    Get.lazyPut<DetailsOfFarmerController>(
      ()=>DetailsOfFarmerController()
    );
    Get.lazyPut<DetailsOfFarmworkerController>(
      ()=>DetailsOfFarmworkerController()
    );
    Get.lazyPut<SavedLandsController>(
      ()=>SavedLandsController()
    );
    Get.lazyPut<SavedFarmersController>(
      ()=>SavedFarmersController()
    );
    Get.lazyPut<SavedFarmworkersController>(
      ()=>SavedFarmworkersController()
    );
    Get.lazyPut<SavedLandlordsController>(
      ()=>SavedLandlordsController()
    );
    Get.lazyPut<SavedJobsController>(
      ()=>SavedJobsController()
    );
    Get.lazyPut<AvailableLeasedUtilitiesController>(
      () => AvailableLeasedUtilitiesController(),
    );
     Get.lazyPut<AvailableLeasedLandsController>(
      () => AvailableLeasedLandsController(),
    );
     Get.lazyPut<AvailableLeasedJobsController>(
      () => AvailableLeasedJobsController(),
    );
    Get.lazyPut<LandlordUtilitiesController>(
      () => LandlordUtilitiesController(),
    );
    Get.lazyPut<SavedUtilitiesController>(
      () => SavedUtilitiesController(),
    );
    Get.lazyPut<TranslatorIconController>(
      () => TranslatorIconController(),
    );
  }
  }

