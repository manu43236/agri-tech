import 'package:agritech/models/add_land_by_farmer_model.dart';
import 'package:dartz/dartz.dart';

abstract class AddLandByFarmerRepo{
  Future<Either<AddLandByFarmerModel,Exception>> doAddLandFarmer({params1});
}