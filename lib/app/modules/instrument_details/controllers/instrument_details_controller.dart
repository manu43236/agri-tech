import 'package:agritech/app/modules/details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/saved_utilities/controllers/saved_utilities_controller.dart';
import 'package:agritech/app/modules/utility/data/get_utility_by_id_model.dart';
import 'package:agritech/app/modules/utility/services/utility_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

class InstrumentDetailsController extends BaseController {

  Rx<GetUtilityByIdModel> getUtilityByIdModel = Rx(GetUtilityByIdModel());

  var utilityVillage="".obs;
  var apigetUtilityStatus=ApiStatus.LOADING.obs;

  @override
  void onInit() {
    super.onInit();
  }

  //get utility by id API
  Future<void> getUtilityById({id}) async {
    apigetUtilityStatus.value = ApiStatus.LOADING;
    try {
      var result = await UtilityRepoImpl(dioClient).getUtilityById(id: id);
      print("===================== get utility by id ==============");
      print(result);
      result.fold((model) {
        if(model.statusCode==200){
          getUtilityByIdModel.value = model;
        utilityVillage.value=getUtilityByIdModel.value.result!.utility!.village.toString();
        print(getUtilityByIdModel.value.result!.utility!.id);
        Get.find<DetailsOfLandlordUtilitiesController>().isUtilityFav.value = false;
        print('isUtilityFav${Get.find<DetailsOfLandlordUtilitiesController>().isUtilityFav.value}');
      if (Get.find<SavedUtilitiesController>().getAllSavedUtilityModel.value.result!=null) {
        print('hiiiiiiiiiiiiiiiiiiiiii');
        // allSavedLandsModel.value.result!.savedLands!.contains(element)
        for (var element in Get.find<SavedUtilitiesController>()
            .getAllSavedUtilityModel
            .value
            .result!.savedUtilities!) {
          print('for loop');
          print(element.id);
          print(getUtilityByIdModel.value.result!.utility!.id);
          if (element.utilityid == getUtilityByIdModel.value.result!.utility!.id) {
            print('elwmmmmmmmmmm');
            Get.find<HomeController>().deleteId.value = element.id!;
            print('delete id ${Get.find<HomeController>().deleteId.value}');
            Get.find<DetailsOfLandlordUtilitiesController>().isUtilityFav.value = true;
          }
        }
        }
      }
        apigetUtilityStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apigetUtilityStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

   void toggleSwitch(bool value) {
    //isSwitched.value = value;
    Get.find<InstrumentDetailsController>().getUtilityByIdModel.value.result!.utility!.status!=value;
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
