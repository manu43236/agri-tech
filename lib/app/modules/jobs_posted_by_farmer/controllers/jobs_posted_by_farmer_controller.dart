import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';

class JobsPostedByFarmerController extends GetxController {

  final LandlordController landlordController=Get.find<LandlordController>();

  var id=''.obs;

  @override
  void onInit() async{
    super.onInit();
    id.value= await SecureStorage().readData(key: "id"??'');
    landlordController.addAllFarmerjobs(userId: id.value);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
