import 'package:dartz/dartz.dart';

import '../data/get_crop_availability_by_yield_payment_id_model.dart';

abstract class DetailPurchasedCropRepo{
  Future<Either<GetCropAvailabilityByYieldPaymentIdModel,Exception>> detailPurchaseCrop();
}