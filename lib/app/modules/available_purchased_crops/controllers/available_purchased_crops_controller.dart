import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../vendor/data/get_vendor_crops_model.dart';
import '../../vendor/services/vendor_repo_impl.dart';

class AvailablePurchasedCropsController extends BaseController {

  var apiPurchasedCrops=ApiStatus.LOADING.obs;

  Rx<GetVendorCropsModel> getPurchasedAvailableCropsModel =
      Rx(GetVendorCropsModel());

  @override
  void onInit() {
    super.onInit();
    availablePurchasedCrops();
  }

   //filtered crops API
  Future<void> availablePurchasedCrops() async {
    apiPurchasedCrops.value = ApiStatus.LOADING;
    try {
      var result = await VendorRepoImpl(dioClient).purchasedCrops();
      print('=============== get available purchased crops =========');
      print(result);
      result.fold((model) {
        getPurchasedAvailableCropsModel.value = model;
        apiPurchasedCrops.value = ApiStatus.SUCCESS;
      }, (r) {
        apiPurchasedCrops.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
