import 'package:agritech/app/modules/vendor_checkout_details/services/vendors_checkout_details_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

import '../../../routes/app_pages.dart';
import '../../detail_crop/data/get_crop_payment_details_model.dart';
import '../../detail_crop/services/detail_crop_repo_impl.dart';
import '../data/intiate_crop_payment_model.dart';
import '../data/update_crop_payment_model.dart';
import '../data/verify_crop_payment_model.dart';

class VendorCheckoutDetailsController extends BaseController {

  Rx<GetCropPaymentDetailsModel> getCropPaymentDetailsModel =
      Rx(GetCropPaymentDetailsModel());

  Rx<UpdateCropPaymentModel> updateCropPaymentModel =
      Rx(UpdateCropPaymentModel());

  Rx<IntiateCropPaymentModel> intiateCropPaymentModel =
      Rx(IntiateCropPaymentModel());

  Rx<VerifyCropPaymentModel> verifyCropPaymentModel =
      Rx(VerifyCropPaymentModel());

  var apiCheckoutDetailsStatus=ApiStatus.LOADING.obs;
  var updateQuantityNumber=0.obs;

  var amount=0.obs;
  late Razorpay _razorpay;

  var razorpayOrderId=''.obs;
  var razorpayPaymentId=''.obs;
  var razorpaySignature=''.obs;
  var paymentId=''.obs;

  @override
  void onInit() {
    super.onInit();
     _razorpay=Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
    getCropPaymentDetails(id: Get.arguments["id"]);
  }

  void openCheckout(amount)async{
  amount=intiateCropPaymentModel.value.result!.amount*100;
  String orderId=intiateCropPaymentModel.value.result!.orderId;
  paymentId.value=intiateCropPaymentModel.value.result!.paymentId.toString();
  var options = {
  'key': 'rzp_test_qHm0r8qkmd7VWN',
  'amount': amount, 
  'order_id': orderId,
  'name': 'Agritech', 
  'description': 'utilities payment',
  'prefill': {
    'contact': '9000090000',
    'email': 'gaurav.kumar@example.com'
  },
  'externals':{
    'wallets':['paytm']
  },
  'method': {
      'netbanking': true,
      'card': true,
      'upi': true,
      'wallet': true,
      'paylater':true
    }
};

try{
  _razorpay.open(options);
}catch(e){
  debugPrint('e');
}
}

void _handlePaymentSuccess(PaymentSuccessResponse response) {
  Fluttertoast.showToast(msg: "payment successful"+response.paymentId!,toastLength: Toast.LENGTH_SHORT);

  Map<String, dynamic> verificationData = {
      "order_id": response.orderId,
      "payment_id": response.paymentId,
      "signature": response.signature,
  };
  print('order id :${response.orderId}');
  print('payment id :${response.paymentId}');
  print('signatue:${response.signature}');
  razorpayOrderId.value=response.orderId.toString();
  razorpayPaymentId.value=response.paymentId.toString();
  razorpaySignature.value=response.signature.toString();
  verifyCropPayment();
  getCropPaymentDetails(id: Get.arguments["id"]);
  Get.toNamed(Routes.VENDOR_PAYMENT_SUCCESSFUL);
  Future.delayed(Duration(seconds: 3), () {
    Get.offAllNamed(Routes.DASHBOARD);
  });
}

void _handlePaymentError(PaymentFailureResponse response) {
  Fluttertoast.showToast(msg: "payment fail"+response.message!,toastLength: Toast.LENGTH_SHORT);
  //Get.toNamed(Routes.LANDLORD_UTILITIES_CHECKOUT_PAGE);
}


void _handleExternalWallet(ExternalWalletResponse response) {
  Fluttertoast.showToast(msg: "external wallet"+response.walletName!,toastLength: Toast.LENGTH_SHORT);
}


  Future<void> getCropPaymentDetails({id}) async {
    apiCheckoutDetailsStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailCropRepoImpl(dioClient).getCropPaymentDetails(id: id);
      print('=============== get crop details =========');
      print(result);
      result.fold((model) {
        getCropPaymentDetailsModel.value = model;
        apiCheckoutDetailsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiCheckoutDetailsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> updateCropPayment({id,quantity}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await VendorsCheckoutDetailsRepoImpl(dioClient).updatePayment(id: id,quantity: quantity);
      print('=============== update crop payment =========');
      print(result);
      result.fold((model) {
        updateCropPaymentModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> intiateCropPayment({paymentId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await VendorsCheckoutDetailsRepoImpl(dioClient).intiatePayment(paymentId: paymentId);
      print('=============== intiate payment =========');
      print(result);
      result.fold((model) {
        intiateCropPaymentModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //verify crop payment API
  Future<void> verifyCropPayment() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await VendorsCheckoutDetailsRepoImpl(dioClient).verifyPayment(params1:{
        "razorpay_order_id":razorpayOrderId.value,
        "razorpay_payment_id":razorpayPaymentId.value,
        "razorpay_signature": razorpaySignature.value,
        "paymentId":paymentId.value
      });
      print("===================== verify crop payment ==============");
      print(result);
      result.fold((model) {
        verifyCropPaymentModel.value = model;
        print(verifyCropPaymentModel.value.result!.paymentRecord);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
