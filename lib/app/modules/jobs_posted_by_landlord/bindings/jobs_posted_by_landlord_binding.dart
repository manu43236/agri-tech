import 'package:get/get.dart';

import '../controllers/jobs_posted_by_landlord_controller.dart';

class JobsPostedByLandlordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<JobsPostedByLandlordController>(
      () => JobsPostedByLandlordController(),
    );
  }
}
