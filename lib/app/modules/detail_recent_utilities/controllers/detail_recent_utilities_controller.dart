import 'package:agritech/app/modules/utility/controllers/utility_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';
import '../../utility/data/get_recent_four_utilities_model.dart';
import '../../utility/data/get_utility_by_id_model.dart';
import '../../utility/services/utility_repo_impl.dart';

class DetailRecentUtilitiesController extends BaseController {
  //final UtilityController utilityController=Get.find<UtilityController>();

  Rx<GetUtilityByIdModel> getUtilityByIdModel = Rx(GetUtilityByIdModel());
  Rx<GetRecentFourUtilitiesModel> getRecentFourUtilitiesModel =
      Rx(GetRecentFourUtilitiesModel());
  var apigetUtilityStatus=ApiStatus.LOADING.obs;
  var mainUtilSearch = ''.obs;

  @override
  void onInit() async{
    super.onInit();
    if(Get.arguments != null) {
       await getUtilityById(id: Get.arguments['UtilId']);
    }
    await recentUtilities(search:mainUtilSearch.value );
  }


  //get utility by id API
  Future<void> getUtilityById({id}) async {
    apigetUtilityStatus.value = ApiStatus.LOADING;
    try {
      var result = await UtilityRepoImpl(dioClient).getUtilityById(id: id);
      print("===================== get utility by id ==============");
      print(result);
      result.fold((model) {
        if(model.statusCode==200){
          getUtilityByIdModel.value = model;
      }
        apigetUtilityStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apigetUtilityStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //recent four utilities API
  Future<void> recentUtilities({search}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await UtilityRepoImpl(dioClient).recentUtilities(search: search);
      print('=============== get recent utilities =========');
      print(result);
      result.fold((model) {
        getRecentFourUtilitiesModel.value = model;
        print(getRecentFourUtilitiesModel.value.result!.first.brandName);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

 

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
