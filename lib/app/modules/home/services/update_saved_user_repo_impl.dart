import 'package:agritech/app/modules/home/services/update_saved_user_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import '../../../../models/update_saved_user_model.dart';

class UpdateSavedUserRepoImpl extends UpdateSavedUserRepo with NetworkCheckService{
  final DioClient _dioClient;
  UpdateSavedUserRepoImpl(this._dioClient);

  @override
  Future<Either<UpdateSavedUserModel,Exception>>  updateSavedItems({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('save/updateLandOwner', Method.post,params: params1);
        return result.fold((l){
          UpdateSavedUserModel model=UpdateSavedUserModel.fromJson(l.data);
          return Left(model);
        },(r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}