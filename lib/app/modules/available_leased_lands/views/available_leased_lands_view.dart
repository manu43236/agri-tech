import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/text_fields/custom_search_widget.dart';
import '../../../routes/app_pages.dart';
import '../../home/controllers/home_controller.dart';
import '../controllers/available_leased_lands_controller.dart';

class AvailableLeasedLandsView extends GetView<AvailableLeasedLandsController> {
  const AvailableLeasedLandsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            'Available Leased Lands',
            style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
          ),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: colorBlack,
            ),
            onPressed: () async {
              // if (controller.name.value.isEmpty) {
              //   Get.back();
              // } 
              Get.back();
            },
          ),
      ),
      body:Obx(
        () => Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 10,
                ),
                CustomSearchField(
                  onChanged: (val) {
                    controller.farmerLeasedLandsSearch.value = val;
                    if (val.length >= 3) {
                      print("dataaaa");
                      controller.leasedSearchLands();
                    } else if (val.isEmpty) {
                      controller.leasedSearchLands();
                    }
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                // controller.name.value.isEmpty
                //     ? const SizedBox()
                //     : Text(controller.name.value ?? ""),
                SizedBox(
                  height: 10,
                ),
                controller.apiFarLeaseLandsStatus.value == ApiStatus.LOADING
                    ? const SizedBox()
                    : controller.getLeasedSearchLandsDetailsModel.value.result!
                            .lands!.isEmpty
                        ? const Align(
                            alignment: Alignment.center,
                            child: Text(
                              "No Lands Found",
                              style:
                                  TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                          )
                        : Column(
                            children: _avaliLand(),
                          )
              ],
            ),
          ),
        ),
      ),
    );
  }

   List<Widget> _avaliLand() {
    List<Widget> items = [];
    for (var index = 0;
        index <
            controller.getLeasedSearchLandsDetailsModel.value.result!.lands!.length;
        index++) {
      items.add(SizedBox(
        child: InkWell(
          onTap: () async {
            Get.find<HomeController>().landId.value = controller
                .getLeasedSearchLandsDetailsModel.value.result!.lands![index].id
                .toString();
            Get.toNamed(Routes.DETAIL_LEASED_LANDS,arguments: {"landId":Get.find<HomeController>().landId.value});
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(11),
                    topRight: Radius.circular(11)),
                child: Image.network(
                  controller.getLeasedSearchLandsDetailsModel.value.result!
                          .lands![index].image ??
                      '',
                  height: Get.height * 0.2,
                  width: Get.width,
                  fit: BoxFit.cover,
                  errorBuilder: (context, error, stackTrace) {
                    return Image.asset(
                      defaultImageUrl,
                      height: Get.height * 0.2,
                      width: Get.width,
                      fit: BoxFit.cover,
                    );
                  },
                ),
              ),
              Container(
                width: Get.width,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(11),
                        bottomRight: Radius.circular(11)),
                    color: colorAsh,
                    shape: BoxShape.rectangle),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${controller.getLeasedSearchLandsDetailsModel.value.result!.lands![index].details} Land at ${controller.getLeasedSearchLandsDetailsModel.value.result!.lands![index].location}",
                        style:
                            TextStyles.kTSDS14W500.copyWith(color: colorHello),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Extent-${controller.getLeasedSearchLandsDetailsModel.value.result!.lands![index].surveyNumber} Survey number',
                        style:
                            TextStyles.kTSFS10W500.copyWith(color: colorHello),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      // InkWell(
                      //   onTap: () {},
                      //   child: Container(
                      //     padding: const EdgeInsets.symmetric(
                      //         horizontal: 10, vertical: 5),
                      //     decoration: const BoxDecoration(
                      //       color: colorCost,
                      //     ),
                      //     child: const Text(
                      //       'Cost Enquiry',
                      //       style: TextStyle(
                      //         color: colorWhite,
                      //         fontSize: 12,
                      //       ),
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ));
    }
    return items;
  }
}
