import 'package:agritech/app/modules/details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import 'package:agritech/app/modules/landlord_utilities_checkout_page/services/landlord_utilities_checkout_page_repo_impl.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../details_of_landlord_utilities/data/create_utility_payment_model.dart';
import '../../details_of_landlord_utilities/services/details_of_landlord_utilities_repo_impl.dart';
import '../data/pay_payment_model.dart';
import '../data/verify_utility_payment_model.dart';

class LandlordUtilitiesCheckoutPageController extends BaseController {

  late Razorpay _razorpay;
  var amount=0.obs;

  var razorpayOrderId=''.obs;
  var razorpayPaymentId=''.obs;
  var razorpaySignature=''.obs;
  var paymentId=''.obs;

  Rx<CreateUtilityPaymentModel> updateUtilityPaymentModel = Rx(CreateUtilityPaymentModel());
  Rx<PayPaymentModel> payPaymentModel = Rx(PayPaymentModel());
  Rx<PayPaymentModel> payBalancePaymentModel = Rx(PayPaymentModel());
  Rx<VerifyUtilityPaymentModel> verifyUtilityPaymentModel = Rx(VerifyUtilityPaymentModel());
  Rx<CreateUtilityPaymentModel> createUtilityPaymentModel = Rx(CreateUtilityPaymentModel());
  Rx<CreateUtilityPaymentModel> getUtilityPaymentByIdModel = Rx(CreateUtilityPaymentModel());

  var updateQuantityNumber=0.obs;
  var createdUtilityId="".obs;
  var paymentUtilityId="".obs;
  var userId=''.obs;
  var apiUtilityPaymentStatus=ApiStatus.LOADING.obs;

  @override
  void onInit() async{
    super.onInit();
    userId.value=await SecureStorage().readData(key: "id");
    if(Get.arguments!=null){
      await createUtilityPayment(utilityId:Get.arguments["id"]);
      await getUtilityPayment(createdUtillityId: createdUtilityId.value);
    }
    _razorpay=Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  void openCheckout(amount)async{
  amount=payPaymentModel.value.result!.amount*100;
  String orderId=payPaymentModel.value.result!.orderId;
  paymentId.value=payPaymentModel.value.result!.paymentId.toString();
  var options = {
  'key': 'rzp_test_qHm0r8qkmd7VWN',
  'amount': amount, 
  'order_id': orderId,
  'name': 'Agritech', 
  'description': 'utilities payment',
  'prefill': {
    'contact': '9000090000',
    'email': 'gaurav.kumar@example.com'
  },
  'externals':{
    'wallets':['paytm']
  },
  'method': {
      'netbanking': true,
      'card': true,
      'upi': true,
      'wallet': true,
      'paylater':true
    }
};

try{
  _razorpay.open(options);
}catch(e){
  debugPrint('e');
}
}

void _handlePaymentSuccess(PaymentSuccessResponse response) async{
  Fluttertoast.showToast(msg: "payment successful"+response.paymentId!,toastLength: Toast.LENGTH_SHORT);

  Map<String, dynamic> verificationData = {
      "order_id": response.orderId,
      "payment_id": response.paymentId,
      "signature": response.signature,
  };
  print('order id :${response.orderId}');
  print('payment id :${response.paymentId}');
  print('signatue:${response.signature}');
  razorpayOrderId.value=response.orderId.toString();
  razorpayPaymentId.value=response.paymentId.toString();
  razorpaySignature.value=response.signature.toString();
  await verifyUtilityPayment();
  //Get.find<DetailsOfLandlordUtilitiesController>().getUtilityPayment();
  Get.toNamed(Routes.SUCCESS_PAYMENT_CHECKOUT_PAGE,arguments: {"id":verifyUtilityPaymentModel.value.result!.paymentRecord!.id!});
  Future.delayed(Duration(seconds: 3), () {
    Get.offAllNamed(Routes.DASHBOARD);
  });
}

void _handlePaymentError(PaymentFailureResponse response) {
  Fluttertoast.showToast(msg: "payment fail"+response.message!,toastLength: Toast.LENGTH_SHORT);
  //Get.toNamed(Routes.LANDLORD_UTILITIES_CHECKOUT_PAGE);
}


void _handleExternalWallet(ExternalWalletResponse response) {
  Fluttertoast.showToast(msg: "external wallet"+response.walletName!,toastLength: Toast.LENGTH_SHORT);
}

  //update utility payment API
  Future<void> updateUtilityPayment() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await LandlordUtilitiesCheckoutPageRepoImpl(dioClient).updateQuantity(params1:{
        "paymentId":int.parse(paymentUtilityId.value),
        "quantity":updateQuantityNumber.value
      });
      print("===================== create utility payment ==============");
      print(result);
      result.fold((model) {
        updateUtilityPaymentModel.value = model;
        createdUtilityId.value=updateUtilityPaymentModel.value.result!.id.toString();
        print(updateUtilityPaymentModel.value.result!.costOfSelectedItem);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //pay payment API
  Future<void> utilityPayment({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await LandlordUtilitiesCheckoutPageRepoImpl(dioClient).payPayment(paymentId:id );
      print("=====================  utility pay payment ==============");
      print(result);
      result.fold((model) {
        payPaymentModel.value = model;
        print(payPaymentModel.value.result!.amount);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //pay balance payment API
  Future<void> utilityBalancePayment({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await LandlordUtilitiesCheckoutPageRepoImpl(dioClient).payBalancePayment(balancePaymentId: id);
      print("=====================  utility balance pay payment ==============");
      print(result);
      result.fold((model) {
        payBalancePaymentModel.value = model;
        print(payBalancePaymentModel.value.result!.amount);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //verify payment API
  //update utility payment API
  Future<void> verifyUtilityPayment() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await LandlordUtilitiesCheckoutPageRepoImpl(dioClient).verifyPayment(params1:{
        "razorpay_order_id":razorpayOrderId.value,
        "razorpay_payment_id":razorpayPaymentId.value,
        "razorpay_signature": razorpaySignature.value,
        "paymentId":paymentId.value
      });
      print("===================== verify utility payment ==============");
      print(result);
      result.fold((model) {
        verifyUtilityPaymentModel.value = model;
        print(verifyUtilityPaymentModel.value.result!.paymentRecord);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //create utility payment API
  Future<void> createUtilityPayment({utilityId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailsOfLandlordUtilitiesRepoImpl(dioClient).createUtilityPayment(params1:{
        "userId": userId.value, //Get.find<HomeController>().selectedLandlordId.value,
        "utilityId": utilityId //Get.find<UtilityController>().utilityId.value
      });
      print("===================== create utility payment ==============");
      print(result);
      result.fold((model) {
        createUtilityPaymentModel.value = model;
        createdUtilityId.value = createUtilityPaymentModel.value.result!.id.toString();
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //get Utility Payment By Id  API
  Future<void> getUtilityPayment({createdUtillityId}) async {
    apiUtilityPaymentStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailsOfLandlordUtilitiesRepoImpl(dioClient).getUtilityPayment(id: createdUtillityId);
      print("===================== get utility payment by id ==============");
      print(result);
      result.fold((model) {
        getUtilityPaymentByIdModel.value = model;
        paymentUtilityId.value = getUtilityPaymentByIdModel.value.result!.id.toString();
        apiUtilityPaymentStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiUtilityPaymentStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
    _razorpay.clear(); 
  }

}
