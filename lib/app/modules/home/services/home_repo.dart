import 'package:agritech/app/modules/home/data/get_lease_utilities_details_model.dart';
import 'package:agritech/app/modules/home/data/get_leased_jobs_details_model.dart';
import 'package:agritech/app/modules/home/data/get_leased_lands_details_model.dart';
import 'package:dartz/dartz.dart';

import '../../details_of_farmworker/data/near_by_farmworkers_model.dart';
import '../data/all_jobs_of_landlord_model.dart';
import '../data/all_lands_of_landlord.dart';
import '../data/filter_near_landlords_model.dart';
import '../data/get_all_jobs_by_farmer_model.dart';
import '../data/get_all_lands_by_farmer_model.dart';
import '../data/landlord_farmers_model.dart';
import '../data/recent_lands_model.dart';

abstract class HomeRepo {
  Future<Either<RecentLandsModel,Exception>>  doRecentLands();
  Future<Either<AddLandsOfLandlordModel,Exception>>  doAddAllLands();
  Future<Either<AllJobsOfLandlordModel,Exception>>  addJobsOfLandlord();
  Future<Either<GetAllLandsByFarmerModel,Exception>>  allFarmerLands({params1});
  Future<Either<GetAllJobsByFarmerModel,Exception>>  getAllFarmerJobs({params1});
  Future<Either<LandlordFarmersModel,Exception>>  doLFarmers();
  Future<Either<NearByFarmworkersModel,Exception>>  doLFarmworker();
  Future<Either<FilterNearLandlordModel,Exception>> filterLandlords();
  Future<Either<GetLeaseUtilitiesDetailsModel,Exception>> getLeaseDetails();
  Future<Either<GetLeasedJobDetailsModel,Exception>> getLeasedJobDetails();
  Future<Either<GetLeasedLandsDetailsModel,Exception>> getLeasedLandDetails();
}