import 'package:agritech/app/modules/profile/data/get_user_roles_model.dart';
import 'package:agritech/app/modules/profile/services/profile_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:agritech/models/profile_model.dart';
import 'package:dartz/dartz.dart';


class ProfileRepoImpl extends ProfileRepo with NetworkCheckService {
  final DioClient _dioClient;

  ProfileRepoImpl(this._dioClient);

  @override
  Future<Either<ProfileModel, Exception>> doProfile({params1}) async {
    bool data = await checkInternet();

    if (!data) {
      return Right(Exception('No Network found'));
    } else {
      try {
        print(params1);
        var result = await _dioClient.requestForAuth('User/updateUser', Method.post,
            params: params1);
        return result.fold((l) {
          ProfileModel model = ProfileModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      } catch (e) {
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetUserRolesModel, Exception>> userRoles({lang}) async {
    bool data = await checkInternet();

    if (!data) {
      return Right(Exception('No Network found'));
    } else {
      try {
        var result = await _dioClient.requestForAuth('userrole/getAllRoles?language=$lang', Method.get);
        return result.fold((l) {
          GetUserRolesModel model = GetUserRolesModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      } catch (e) {
        return Right(Exception(e));
      }
    }
  }

}