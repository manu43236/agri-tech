// To parse this JSON data, do
//
//     final notificationsCountModel = notificationsCountModelFromJson(jsonString);

import 'dart:convert';

NotificationsCountModel notificationsCountModelFromJson(String str) => NotificationsCountModel.fromJson(json.decode(str));

String notificationsCountModelToJson(NotificationsCountModel data) => json.encode(data.toJson());

class NotificationsCountModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    NotificationsCountModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory NotificationsCountModel.fromJson(Map<String, dynamic> json) => NotificationsCountModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    dynamic unreadCount;

    Result({
        this.unreadCount,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        unreadCount: json["unreadCount"],
    );

    Map<String, dynamic> toJson() => {
        "unreadCount": unreadCount,
    };
}
