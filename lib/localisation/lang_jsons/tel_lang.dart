const telLang = {
  //welcome_page_view
  'already_have_an_account?': 'మీకు ఇప్పటికే ఖాతా ఉందా? ',
  'create_an_account': 'ఒక ఖాతాను సృష్టించండి',
  'here': ' ఇక్కడ',
  'login': 'లాగిన్',
  //Sign_up_view
  'sign_up': 'సైన్ అప్',
  'phone_no': 'ఫోన్ నంబర్',
  '+91 000-00-000': '+91 000-00-000',
  'required_mob_num': '* ఫోన్ నంబర్ అవసరం',
  'enter_valid_mob_num': '* చెల్లుబాటు అయ్యే ఫోన్ నంబర్‌ను నమోదు చేయండి',
  'get_otp': 'ఓటిపి పొందండి',
  //Otp_view
  'otp': 'ఓటిపి',
  'otp_verification': 'ఓటిపి ధృవీకరణ',
  'enter_the_verification_code_we_just_sent_on_your_mobile':
      'మీ ఫోన్‌కు పంపిన ధృవీకరణ కోడ్‌ను నమోదు చేయండి',
  'authenticate': 'ఆధారితం చేయండి',
  'resend_otp': 'ఓటిపి మళ్ళీ పంపండి',
  //Profile_view
  'gallery': 'గ్యాలరీ',
  'camera': 'క్యామరా',
  'enter_details': 'వివరాలు నమోదు చేయండి',
  'name': 'పేరు',
  'enter_your_name': 'మీ పేరును నమోదు చేయండి',
  'required_name': '* పేరు అవసరం',
  'age': 'వయసు',
  'required_age': '* వయసు అవసరం',
  'mobile_number': 'మొబైల్ నంబర్',
  'enter_your_mobile_number': 'మీ మొబైల్ నంబర్‌ను నమోదు చేయండి',
  'address': 'చిరునామా',
  'enter_location': 'ప్రాంతం నమోదు చేయండి',
  'required_address': '* చిరునామా అవసరం',
  'select_role': 'పాత్రను ఎంచుకోండి',
  'description': 'వివరణ',
  'enter_your_details': 'మీ వివరాలను నమోదు చేయండి',
  'required_description': '* వివరణ అవసరం',
  'save': 'సేవ్',
  'search_role': 'పాత్ర శోధించండి',
  'no_roles_available': 'ఎటువంటి పాత్రలు లభించలేదు',
  'please_add_roles_first': 'ముందుగా పాత్రలు జోడించండి',
  //Dashboard_view
  'add': 'జోడించు',
  'add_instrument': 'పరికరం జోడించు',
  'add_skill': 'నైపుణ్యాన్ని జోడించు',
  'add_land': 'భూమిని జోడించు',
  'add_job': 'ఉద్యోగాన్ని జోడించు',
  //home_view
  'landlord': 'భూస్వామి',
  'farmer': 'రైతు',
  'farm_worker': 'వ్యవసాయ కార్మికుడు',
  'utilities': 'సదుపాయాలు',
  'loans_insurance': 'రుణాలు & బీమా',
  'hello': 'హలో',
  'you_are_now_a': 'మీరు ఇప్పుడు ఒక ',
  'you': 'మీరు',
  'others': 'ఇతరులు',
  'vendor': 'విక్రేత',
  'something_went_wrong': 'ఏదో తప్పు జరిగింది',
  //my_lands_widget
  'available_lands': 'అందుబాటులో ఉన్న భూములు',
  'at': 'వద్ద',
  'view_all': 'అన్నీ చూడండి',
  'no_lands': 'భూములు లేవు',
  'add_your_land_here': 'మీ భూమిని ఇక్కడ జోడించండి',
  'my_lands': 'నా భూములు',
  'no_lands_add_it_here': 'భూములు లేవు, ఇక్కడ జోడించండి',
  //my_jobs_widget
  'my_jobs': 'నా ఉద్యోగాలు',
  'no_jobs_add_it_here': 'ఉద్యోగాలు లేవు, ఇక్కడ జోడించండి',
  //leased_utilities_widget
  'leased_utilities': 'లీజుకు ఇచ్చిన సదుపాయాలు',
  'no_leased_utilities': 'లీజుకు ఇచ్చిన సదుపాయాలు లేవు',
  //leased_jobs_widget
  'leased_jobs': 'లీజుకు ఇచ్చిన ఉద్యోగాలు',
  'no_leased_jobs': 'లీజుకు ఇచ్చిన ఉద్యోగాలు లేవు',
  //leased_lands_widget
  'leased_lands': 'లీజుకు ఇచ్చిన భూములు',
  'no_leased_lands': 'లీజుకు ఇచ్చిన భూములు లేవు',
  //available_recent_lands_view
  'available_recent_lands': 'అందుబాటులో ఉన్న తాజా భూములు',
  'extent': 'విస్తీర్ణం- ',
  'acres': 'ఎకరాలు',
  'water_facility': 'నీటి సదుపాయం- ',
  //detail_recent_lands_view
  'detail_recent_lands': 'తాజా భూముల వివరాలు',
  'land_unsaved_successfully': 'భూమి విజయవంతంగా సేవ్ చేయబడలేదు',
  'not_available': 'అందుబాటులో లేదు',
  'available': 'అందుబాటులో ఉంది',
  'hired_farmers': 'నియమిత రైతులు',
  'suggested_lands': 'సిఫారసు చేసిన భూములు',
  'no_lands_are_available_near_your_location':
      'మీ ప్రదేశం వద్ద భూములు అందుబాటులో లేవు',
  //available_my_lands_view
  'available_my_lands': 'అందుబాటులో ఉన్న నా భూములు',
  'no_lands_found': 'భూములు లభించలేదు',
  //detail_my_lands_view
  'detail_my_lands': 'నా భూముల వివరాలు',
  'avail_loan': 'రుణం పొందండి',
  'applied': 'అప్లై చేయబడింది',
  'avail_insurance': 'బీమా పొందండి',
  'suitable_crop': 'సరైన పంట',
  'no_description_available': 'వివరణ అందుబాటులో లేదు.',
  'no_lands_available': 'భూములు అందుబాటులో లేవు.',
  //suitable_crop_view
  'selected_crop_for': 'ఎంచుకున్న పంట కోసం',
  'days': 'రోజులు',
  'yield': 'ఉత్పత్తి',
  'tons': ' /టన్నులు',
  'enable_editing_to_modify_the_field':
      'క్షేత్రాన్ని మార్చడానికి ఎడిటింగ్ ప్రారంభించండి.',
  'submit': 'సమర్పించు',
  'no_dataa':
      'ఎంచుకున్న పంటకు డేటా అందుబాటులో లేదు. దయచేసి ఇతర పంటను ప్రయత్నించండి.',
//grid_view_widget
  'week': 'వారం',
  'done': 'పూర్తయింది',
//crops_dropdown_view
  'no_crops_available_please_add_crops_first':
      'పంటలు అందుబాటులో లేవు. దయచేసి ముందుగా పంటలను జోడించండి.',
  'select_crop': 'పంటను ఎంచుకోండి',
  'select_other_crop': 'ఇతర పంటను ఎంచుకోండి',
  'working_days_info': 'పని రోజుల సమాచారం',
  'no_of_working_days_for': 'పని రోజుల సంఖ్య',
  'no_working_days_data_available': 'పని రోజుల సమాచారం అందుబాటులో లేదు.',
//available_my_jobs_view
  'available_my_jobs': 'అందుబాటులో ఉన్న నా ఉద్యోగాలు',
  'no_jobs_found': 'ఏ ఉద్యోగాలు లభించలేదు',
  'location': 'ప్రదేశం- ',
//detail_my_jobs_view
  'detail_my_jobs': 'నా ఉద్యోగాల వివరాలు',
  'available_job_at': 'అందుబాటులో ఉన్న ఉద్యోగం వద్ద',
  'hired_farmworkers': 'కుదిరిన వ్యవసాయ కార్మికులు',
  'no_jobs_available': 'ఏ ఉద్యోగాలు లభించలేదు',
//available_leased_utilities
  'available_leased_utilities': 'అందుబాటులో ఉన్న లీజ్ చేసిన సౌకర్యాలు',
  'no_utilities_found': 'ఏ సౌకర్యాలు లభించలేదు',
  'utility_at': 'సౌకర్యం వద్ద',
//detail_leased_utilities_view
  'utility_details': 'సౌకర్య వివరాలు',
  'utility_owner': 'సౌకర్య యజమాని',
  'balance_payment': 'మిగిలిన చెల్లింపు',
//landlord_farmer_view
  'available_farmers': 'అందుబాటులో ఉన్న రైతులు',
  'no_farmers_found': 'ఏ రైతులు లభించలేదు',
//farmer_landlord_view
  'available_landlords': 'అందుబాటులో ఉన్న భూమి యజమానులు',
  'no_landlords_found': 'ఏ భూమి యజమానులు లభించలేదు',
//landlord_worker
  'available_farmWorkers': 'అందుబాటులో ఉన్న వ్యవసాయ కార్మికులు',
  'no_farm_workers_found': 'ఏ వ్యవసాయ కార్మికులు లభించలేదు',
//landlord_utilities_view
  'instruments': 'పరికరాలు',
  'no_instruments_available': 'ఏ పరికరాలు అందుబాటులో లేవు',
  'status': 'స్థితి : ',
  'leased': 'లీజ్ చేసినవి',
//loans_view
  'loans': 'రుణాలు',
  'insurance': 'బీమా',
//add_a_land_view
  'add_a_land': 'భూమిని జోడించండి',
  'no_job': 'ఏ ఉద్యోగం లేదు',
  'add_job_here': 'ఇక్కడ ఉద్యోగాన్ని జోడించండి',
//add_land_view
  'land_name': 'భూమి పేరు',
  'land_in_acres': 'భూమి ఎకరాల్లో',
  'required_acres': '* అవసరమైన ఎకరాలు',
  'land_survey_number': 'భూమి సర్వే నంబర్',
  'required_survey_number': '* అవసరమైన సర్వే నంబర్',
  'select_state': 'రాష్ట్రాన్ని ఎంచుకోండి',
  'state': 'రాష్ట్రం',
  'required_state': '* అవసరమైన రాష్ట్రం',
  'select_district': 'జిల్లాను ఎంచుకోండి',
  'district': 'జిల్లా',
  'required_district': '* అవసరమైన జిల్లా',
  'select_mandal': 'మండలాన్ని ఎంచుకోండి',
  'mandal': 'మండలం',
  'required_mandal': '* అవసరమైన మండలం',
  'select_village': 'గ్రామాన్ని ఎంచుకోండి',
  'village': 'గ్రామం',
  'required_village': '* అవసరమైన గ్రామం',
  'water_availability': 'నీటి లభ్యత',
  'yes': 'అవును',
  'no': 'కాదు',
  'image': 'చిత్రం',
  'upload_image_here': 'చిత్రాన్ని ఇక్కడ అప్‌లోడ్ చేయండి',
  'error': 'పొరపాటు',
  'required_to_add_land_image': '* భూమి చిత్రాన్ని జోడించడం అవసరం',
//add_job_view
  'add_a_job': 'ఒక ఉద్యోగాన్ని జోడించండి',
  'job_name': 'ఉద్యోగం పేరు',
  'start_date': 'ప్రారంభ తేది',
  'end_date': 'ముగింపు తేది',
  'required_to_add_job_image': '* ఉద్యోగ చిత్రాన్ని జోడించడం అవసరం',
//add_a_skill
  'add_a_skill': 'ఒక నైపుణ్యాన్ని జోడించండి',
  'skill_name': 'నైపుణ్యం పేరు',
  'experience': 'అనుభవం',
  'please_enter_your_experience': 'మీ అనుభవాన్ని నమోదు చేయండి',
//add_instrument
  'name_of_the_instrument': 'పరికరం పేరు',
  'required_name_of_the_instrument': '* పరికరం పేరు అవసరం',
  'price_per_day': 'రోజుకు ధర',
  'required_price': '* అవసరమైన ధర',
  'brand_name': 'బ్రాండ్ పేరు',
  'required_brand_name': '* అవసరమైన బ్రాండ్ పేరు',
  'required_to_add_image': 'చిత్రాన్ని జోడించడం అవసరం',
//details_of_farmer_view
  'details_of_farmer': 'రైతు వివరాలు',
  'no_address_provided': 'చిరునామా ఇవ్వలేదు',
  'farmer_unsaved_successfully': 'రైతును విజయవంతంగా తొలగించారు',
  'msg': 'సందేశం',
  'please_select_land_first': 'మొదట భూమిని ఎంచుకోండి',
  'hire': 'భర్తీ చేయండి',
  'description_about_him': 'ఆయన గురించి వివరాలు',
  'suggested_farmers': 'సూచించబడిన రైతులు',
  'no_nearby_farmers_found': 'పక్కన రైతులు కనబడలేదు',
  'select_land': 'భూమిని ఎంచుకోండి',
//details_of_farmworker_view
  'details_of_farmworker': 'వ్యవసాయ కార్మికుల వివరాలు',
  'farmworker_unsaved_successfully':
      'వ్యవసాయ కార్మికుడిని విజయవంతంగా తొలగించారు',
  'skills': 'నైపుణ్యాలు',
  'no_skills_found': 'ఏ నైపుణ్యాలు కనబడలేదు',
  'please_select_job_first': 'మొదట ఉద్యోగాన్ని ఎంచుకోండి',
  'suggested_farmworkers': 'సూచించబడిన వ్యవసాయ కార్మికులు',
  'no_farmworkers_available': 'ఏ వ్యవసాయ కార్మికులు అందుబాటులో లేరు',
  'no_near_by_farmworkers_available':
      'పక్కన వ్యవసాయ కార్మికులు అందుబాటులో లేరు',
  'select_job': 'ఉద్యోగాన్ని ఎంచుకోండి',
  'view_less': 'తక్కువగా చూడండి',
  'view_more': 'మరింత చూడండి',
//details_of_landlord_utilities_view
  'utility_unsaved_successfully': 'సౌకర్యాన్ని విజయవంతంగా తొలగించారు',
  'day': '/ రోజు',
  'more': 'మరిన్ని',
  'no_utilities_found_near_your_location':
      'మీ ప్రదేశం వద్ద సౌకర్యాలు లభించలేదు',
  'call': 'కాల్ చేయండి',
  'rent_now': 'లీజ్ చేయండి',
//landlord_utilities_checkout_page_view
  'total_cost_of_selected_item': 'ఎంచుకున్న వస్తువుల మొత్తం ఖర్చు',
  'rs': 'రూ',
  'advance_payment': 'ముందస్తు చెల్లింపు',
  'to_made_later_after_receiving_the_prod':
      'ఉత్పత్తిని అందుకున్న తర్వాత చెల్లించవలెను',
  'amount_to_be_paid': 'చెల్లించవలసిన మొత్తం',
  'gst': 'జీఎస్టీ',
  'total': 'మొత్తం',
  'make_a_payment': 'చెల్లింపు చేయండి',
//notifications_view
  'notifications': 'నోటిఫికేషన్లు',
  'approve': 'ఆమోదించు',
  'decline': 'తిరస్కరించు',
  'interested': 'ఆసక్తి ఉంది',
//profile_page_view
  'profile': 'ప్రొఫైల్',
  'my_details': 'నా వివరాలు',
  'saved': 'సేవ్ చేయబడింది',
  'settings': 'సెట్టింగ్స్',
  'customer_support': 'వినియోగదారు మద్దతు',
  'my_instruments': 'నా పరికరాలు',
  'purchased': 'కొనుగోలు చేయబడింది',
  'update_kyc': 'కేవైసీ నవీకరణ',
  'e_kyc_verified': 'ఇ-కేవైసీ ధృవీకరించబడింది',
  'farm_workers': 'వ్యవసాయ కార్మికులు',
  'jobs_posted': 'ఉద్యోగాలు పోస్ట్ చేయబడ్డాయి',
  'logout': 'లాగౌట్ చేయండి',
//edit_profile_view
  'edit': 'సవరించు',
  'required_mobile_number': '* అవసరమైన మొబైల్ నంబర్',
  'enter_valid_mobile_number': 'చెల్లుబాటు అయ్యే మొబైల్ నంబర్ నమోదు చేయండి',
  'enter_role': 'పాత్రను నమోదు చేయండి',
  'required_role': '* అవసరమైన పాత్ర',
//select_region_page
  'clear_all': 'అన్నీ క్లియర్ చేయండి',
  'select_your_region': 'మీ ప్రాంతాన్ని ఎంచుకోండి',
  'close': 'మూసివేయండి',
  'sta': 'రాష్ట్రం',
  'dis': 'జిల్లా',
  'man': 'మండలం',
  'vill': 'గ్రామం',
  'recent_Lands': 'ఇటీవలి భూములు',
  'my_Lands': 'నా భూములు',
  'my_Jobs': 'నా ఉద్యోగాలు',
  'farmer_My_Lands': 'రైతు నా భూములు',
  'farmer_My_Jobs':'రైతు నా ఉద్యోగాలు',
  'worker_jobs':'కార్మిక ఉద్యోగాలు',
  'my_Utilities':'నా ఉపయోగాలు',
  'recent_utilities':'తాజా ఉపయోగాలు',
//saved_view
  'farmers': 'రైతులు',
  'farmworkers': 'వ్యవసాయ కార్మికులు',
  'my_utilities': 'నా సౌకర్యాలు',
  'landlords': 'భూమి యజమానులు',
//saved_farmers_view
  'saved_farmers': 'సేవ్ చేసిన రైతులు',
  'no_farmers_saved': 'ఏ రైతులు సేవ్ చేయబడలేదు',
//saved_farmworkers_view
  'saved_farmworkers': 'సేవ్ చేసిన వ్యవసాయ కార్మికులు',
  'no_farm_workers_saved': 'ఏ వ్యవసాయ కార్మికులు సేవ్ చేయబడలేదు',
//saved_lands_view
  'saved_lands': 'సేవ్ చేసిన భూములు',
  'no_lands_saved': 'ఏ భూములు సేవ్ చేయబడలేదు',
//saved_utilities_view
  'saved_utilities': 'సేవ్ చేసిన సౌకర్యాలు',
  'ava': 'అందుబాటులో: ',
  'rented_out': 'లీజ్ చేయబడింది',
  'no_utilities_saved': 'ఏ సౌకర్యాలు సేవ్ చేయబడలేదు',
//saved_landlords_view
  'saved_landlords': 'సేవ్ చేసిన భూమి యజమానులు',
  'no_landlords_saved': 'ఏ భూమి యజమానులు సేవ్ చేయబడలేదు',
//saved_jobs_view
  'saved_jobs': 'సేవ్ చేసిన ఉద్యోగాలు',
  'job_at': 'ఉద్యోగం వద్ద',
  'number_of_days_to_work': 'పని చేయాల్సిన రోజుల సంఖ్య : 5',
  'no_jobs_saved': 'ఏ ఉద్యోగాలు సేవ్ చేయబడలేదు',
//hired_farmers_details_view
  'details': 'వివరాలు',
  'nam': 'పేరు : ',
  'loc': 'ప్రదేశం : ',
//jobs_posted_by_landlord_view
  'jobs': 'ఉద్యోగాలు',
  'posted_on': 'పోస్ట్ చేయబడిన తేదీ :',
//=================farmer_module=====================
  //detail_leased_lands_view
  'detail_leased_lands': 'వివరమైన లీజు భూములు',
  //details_of_landlord
  'details_of_landlord': 'భూమి యజమాని వివరాలు',
  'landlord_unsaved_successfully': 'భూమి యజమాని విజయవంతంగా సేవ్ చేయబడలేదు',
  //details_of_landlord_land_view
  'details_of_land': 'భూమి వివరాలు',
  'show_interest': 'ఆసక్తి చూపించండి',
  //detail_saved_lands_view
  'no_saved_lands_available': 'ముగింపు భూములు అందుబాటులో లేవు',
//============================farmworker_module====================
//farmworker_home_view_page
  'jobs_available': 'ఉపలబ్ధమయ్యే ఉద్యోగాలు',
//available_farmworker_jobs
  'available_farmworker_jobs': 'లభ్యమైన కూలి ఉద్యోగాలు',
//detail_farmworker_job
  'detail_jobs': 'ఉద్యోగ వివరాలు',
  'job_unsaved_successfully': 'ఉద్యోగం విజయవంతంగా సేవ్ కాలేదు',
  'interest': 'ఆసక్తి',
  'suggested_jobs': 'సూచించబడిన ఉద్యోగాలు',
  'your_selected_region_is': 'మీ ఎంపిక చేసిన ప్రాంతం: ',
//my_skills_widget
  'my_skills': 'నా నైపుణ్యాలు',
//detail_my_skills
  'detail_my_skill': 'నా నైపుణ్య వివరాలు',
  'years': 'సంవత్సరాలు',
  'experiences': 'అనుభవం:',
  'relevent_jobs': 'సంబంధిత ఉద్యోగాలు',
  'no_relevant_jobs_available.': 'సంబంధిత ఉద్యోగాలు లభ్యమవడం లేదు.',
  'no_jobs_found_matching_the_criteria':
      'మీ ప్రమాణాలకు సరిపడే ఉద్యోగాలు లభ్యం కాలేదు',
//detail_relevant_jobs
  'details_of_relevent_jobs': 'సంబంధిత ఉద్యోగాల వివరాలు',
//detail_saved_jobs_view
  'detail': 'వివరం',
  'Job_at': 'పని వద్ద',
//=========================utiliser_module======================
//recent_utilities_widget
'available_utilities':'అందుబాటులో ఉన్న ఉపకరణాలు',
'no_utilities':'ఉపకరణాలు లేవు',
//detail_recent_utilities
'detail_recent_utility':'తాజా సౌకర్య వివరాలు',
'lessee':'భూమి పాఠకుడు (లీజుదారు)',
'suggested_utilities':'సిఫారసు చేసిన ఉపకరణాలు',
//my_instrument_details_view
'my_instrument_details':'నా పరికర వివరాలు',
//available_recent_utilities_view
'available_recent_utilities':'అందుబాటులో ఉన్న తాజా ఉపకరణాలు',
'no_utilities_found_matching_the_criteria':'ప్రమాణాలకు సరిపడే ఉపకరణాలు కనుగొనబడలేదు',
//available_my_utilities_view
'available_my_utilities':'అందుబాటులో ఉన్న నా ఉపకరణాలు',
'no_instruments_added':'ఏ పరికరాలు జోడించలేదు',
//success_payment_checkout_page_view
'payment_checkout':'చెల్లింపు చెక్కౌట్',
'payment_is_successful':'చెల్లింపు విజయవంతమైంది',
//=======================vendor_module=================
//recent_crops_widgets
'available_crops':'అందుబాటులో ఉన్న పంటలు',
'crop_at':'పంట వద్ద',
'no_crops_found':'పంటలు లభించలేదు',
//purchased_crops_widget
'purchased_crops':'కొనుగోలు చేసిన పంటలు',
'no_pcurchased_crops':'కొనుగోలు చేసిన పంటలు లేవు',
//available_crops_view
'quantity':'మొత్తం:',
'Tons':'టన్నులు',
'outdate':'గడువు తేదీ - ',
'select_quantity':'మొత్తాన్ని ఎంచుకోండి',
//detail_crops_view
'detail_crop':'వివరాల పంట',
'quantity_available':'అందుబాటులో మొత్తం - ',
'owner':'యజమాని - ',
'total_days':'మొత్తం రోజులు - ',
'avail_this':'దీన్ని పొందండి',
'purchased_quantity':'కొనుగోలు చేసిన మొత్తం - ',
'outed_date':'గడువు తేదీ - ',
'payment_completed':'చెల్లింపు పూర్తయింది',
//vendor_payment_successful_view
'payment_successful':'చెల్లింపు విజయవంతం!',
 //vendor_checkout_details_view
'checkout':'చెక్కౌట్',
//purchased_checkout_details_view
'payment_made':'చెల్లింపు చేయబడింది',
//kyc_welcome_page_view
'kyc_details':'కెవైసి వివరాలు',
'verify_your_identity':'మీ వ్యక్తిత్వాన్ని ధృవీకరించండి',
'kyc_text':'హాయ్! మేము నిజంగా మీరు అనే విషయాన్ని ధృవీకరించాల్సిన అవసరం ఉంది. ఇది త్వరితమైన మరియు సులభమైన ప్రక్రియ, ఒకసారి మీరు ధృవీకరించబడిన తర్వాత, మీకు అన్ని ఫీచర్లకు పూర్తి ప్రాప్యత ఉంటుంది.',
'proceed':'ముందుకు సాగండి',
'skip':'దాటవేయి',
//kyc_details_page_view
'proof_of_identity':'గుర్తింపు రుజువు',
'in_order':'మీ నమోదు పూర్తి చేయడానికి, దయచేసి మీ గుర్తింపు కాపీని అప్‌లోడ్ చేయండి.',
'choose_your_identity_type':'మీ గుర్తింపు రకాన్ని ఎంచుకోండి:',
'aadhaar':'ఆధార్',
'pan_card':'పాన్ కార్డు',
'voter_id':'వోటర్ ఐడి',
'upload_identity':'గుర్తింపును అప్‌లోడ్ చేయండి',
'front_image':'ముందు చిత్రం',
'upload_required':'అప్‌లోడ్ అవసరం',
'please_upload_the_front_image_first':'ముందు చిత్రాన్ని ముందుగా అప్‌లోడ్ చేయండి.',
'back_image':'వెనుక చిత్రం',
'upload_selfie_picture':'సెల్ఫీ చిత్రాన్ని అప్‌లోడ్ చేయండి',
'selfie_image':'సెల్ఫీ చిత్రం',
'incomplete':'అసంపూర్ణం',
'please_upload_the_required_document':'అవసరమైన పత్రాన్ని అప్‌లోడ్ చేయండి.',
'continue':'ముందుకు సాగండి',
'price':'ధర -',
};
