import 'package:agritech/app/modules/detail_my_lands/services/detail_my_lands_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../data/send_notification_for_loan_model.dart';

class DetailMyLandsRepoImpl extends DetailMyLandsRepo with NetworkCheckService{
  final DioClient _dioClient;
  DetailMyLandsRepoImpl(this._dioClient);

  @override
  Future<Either<SendNotificationForLoanModel,Exception>> sendLoanNotification({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('notification/sendNotificationForLoan', Method.post,params: params1);
        return result.fold((l){
          SendNotificationForLoanModel model=SendNotificationForLoanModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}