

import '../../conts/api_const.dart';
import '../env.dart';

class DevEnv extends EnvVars {
  @override
  String get apiToken => '';

  @override
  String get baseURL => devBaseUri;

  @override
  String get title => 'AgriTech Dev';

  @override
  String get bearerToken => devBearerToken;

  @override
  String get authBaseURL => devAuthUri;
}
