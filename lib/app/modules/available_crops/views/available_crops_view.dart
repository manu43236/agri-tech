import 'package:agritech/core/utils/widget_utils/date_format.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/available_crops_controller.dart';

class AvailableCropsView extends GetView<AvailableCropsController> {
  const AvailableCropsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'available_crops'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: _openCropSelectionBottomSheet,
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 12, vertical: 8),
                        decoration: BoxDecoration(
                          color: colorAsh,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Row(
                          children: [
                            Text(
                              controller.selectedCrop.value.isEmpty
                                  ? 'select_crop'.tr
                                  : controller.selectedCrop.value,
                              style: TextStyles.kTSDS14W500,
                            ),
                            const Icon(Icons.arrow_drop_down),
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: _openQuantityFilterBottomSheet,
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 12, vertical: 8),
                        decoration: BoxDecoration(
                          color: colorAsh,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Row(
                          children: [
                            Text(
                              '${'quantity'.tr} ${controller.selectedMaxQuan.value} ${'Tons'.tr}',
                              style: TextStyles.kTSDS14W500,
                            ),
                            const Icon(Icons.arrow_drop_down),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                          // controller.filterWithCropQuantity(
                          //     crop: controller.selectedCrop.value,
                          //     minQuan: controller.selectedMinQuan.value == 0
                          //         ? ""
                          //         : controller.selectedMinQuan.value.toString(),
                          //     maxQuan: controller.selectedMaxQuan.value == 0
                          //         ? ""
                          //         : controller.selectedMaxQuan.value
                          //             .toString());
                      },
                      child: Image.asset(
                        cone,
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                controller.selectedCrop.value.isEmpty &&
                        controller.selectedMaxQuan.value == 0
                    ? controller.apiFilteredCrops.value == ApiStatus.LOADING
                        ? Shimmers().getListShimmer()
                        : controller.getfilteredCropAvailabilityModel.value
                                .result!.isEmpty
                            ?  Center(
                                child: Text(
                                  "no_crops_found".tr,
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.grey),
                                ),
                              )
                            : Obx(() => Column(children: _buildCropList()))
                    : controller.apiFiltercropquanStatus.value ==
                            ApiStatus.LOADING
                        ? Shimmers().getListShimmer()
                        : controller.filterWithCropAndQuantityModel.value
                                .result!.isEmpty
                            ?  Center(
                                child: Text(
                                  "no_crops_found".tr,
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.grey),
                                ),
                              )
                            : Obx(() => Column(children: _buildCropListsss())),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildCropListsss() {
    final crops =
        controller.filterWithCropAndQuantityModel.value.result!.where((crop) {
      final matchesCrop = controller.selectedCrop.value.isEmpty ||
          crop.cropDetails?.name == controller.selectedCrop.value;
      final matchesQuantity = controller.selectedMaxQuan.value == 0 ||
          (crop.yieldDetails?.quantity ?? 0) <=
              controller.selectedMaxQuan.value;
      return matchesCrop && matchesQuantity;
    }).toList();

    return crops.map((crop) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: InkWell(
          onTap: () {
            Get.toNamed(
              Routes.DETAIL_CROP,
              arguments: {"yieldId": crop.yieldDetails?.id},
            );
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(11),
                  topRight: Radius.circular(11),
                ),
                child: Image.network(
                  crop.cropDetails?.imageUrl ?? defaultImageUrl,
                  height: Get.height * 0.2,
                  width: Get.width,
                  fit: BoxFit.cover,
                  errorBuilder: (context, error, stackTrace) {
                    return Image.asset(
                      defaultImageUrl,
                      height: Get.height * 0.2,
                      width: Get.width,
                      fit: BoxFit.cover,
                    );
                  },
                ),
              ),
              Container(
                width: Get.width,
                decoration: const BoxDecoration(
                  color: colorAsh,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(11),
                    bottomRight: Radius.circular(11),
                  ),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        crop.cropDetails?.name ?? '',
                        style:
                            TextStyles.kTSDS14W500.copyWith(color: colorHello),
                      ),
                      const SizedBox(height: 5),
                      Text(
                        '${'location'.tr}${crop.landDetails?.village ?? ''}',
                        style:
                            TextStyles.kTSFS10W500.copyWith(color: colorHello),
                      ),
                      const SizedBox(height: 5),
                      Text(
                        '${'outdate'.tr}${formatDate(crop.yieldDetails!.outedDate.toString())}',
                        style:
                            TextStyles.kTSFS10W500.copyWith(color: colorHello),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }).toList();
  }

  List<Widget> _buildCropList() {
    final crops = controller.getfilteredCropAvailabilityModel.value.result!;
    //     .where((crop) {
    //   final matchesCrop = controller.selectedCrop.value.isEmpty ||
    //       crop.cropDetails?.name == controller.selectedCrop.value;
    //   final matchesQuantity = controller.selectedQuantity.value == 0 ||
    //       (crop.yieldDetails?.quantity ?? 0) >=
    //           controller.selectedQuantity.value;
    //   return matchesCrop && matchesQuantity;
    // }).toList();

    return crops.map((crop) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: InkWell(
          onTap: () {
            Get.toNamed(
              Routes.DETAIL_CROP,
              arguments: {"yieldId": crop.yieldDetails?.id},
            );
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(11),
                  topRight: Radius.circular(11),
                ),
                child: Image.network(
                  crop.cropDetails?.imageUrl ?? defaultImageUrl,
                  height: Get.height * 0.2,
                  width: Get.width,
                  fit: BoxFit.cover,
                  errorBuilder: (context, error, stackTrace) {
                    return Image.asset(
                      defaultImageUrl,
                      height: Get.height * 0.2,
                      width: Get.width,
                      fit: BoxFit.cover,
                    );
                  },
                ),
              ),
              Container(
                width: Get.width,
                decoration: const BoxDecoration(
                  color: colorAsh,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(11),
                    bottomRight: Radius.circular(11),
                  ),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        crop.cropDetails?.name ?? '',
                        style:
                            TextStyles.kTSDS14W500.copyWith(color: colorHello),
                      ),
                      const SizedBox(height: 5),
                      Text(
                        '${'location'.tr}${crop.landDetails?.village ?? ''}',
                        style:
                            TextStyles.kTSFS10W500.copyWith(color: colorHello),
                      ),
                      const SizedBox(height: 5),
                      Text(
                        '${'outdate'.tr}${formatDate(crop.yieldDetails!.outedDate.toString())}',
                        style:
                            TextStyles.kTSFS10W500.copyWith(color: colorHello),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }).toList();
  }

  void _openCropSelectionBottomSheet() {
    Get.bottomSheet(
      Obx(() => Container(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.vertical(top: Radius.circular(24)),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  offset: Offset(0, -2),
                  blurRadius: 8,
                ),
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Container(
                    width: 50,
                    height: 6,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade300,
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                ),
                const SizedBox(height: 15),
                Center(
                  child: Text(
                    'select_crop'.tr,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: primary,
                    ),
                  ),
                ),
                const SizedBox(height: 15),
                SizedBox(
                  height: Get.height * 0.45,
                  child: ListView.builder(
                    itemCount:
                        controller.getAllCropsModel.value.result?.length ?? 0,
                    itemBuilder: (context, index) {
                      final crop =
                          controller.getAllCropsModel.value.result![index];
                      final isSelected =
                          controller.selectedCrop.value == crop.name;
                      return GestureDetector(
                        onTap: () {
                          // controller.selectedMaxQuan.value=0;
                          // controller.selectedMinQuan.value=0;
                           controller.selectedCrop.value = crop.name;
                          controller.filterWithCropQuantity(
                              crop: controller.selectedCrop.value,
                              minQuan: controller.selectedMinQuan.value == 0
                                  ? ""
                                  : controller.selectedMinQuan.value.toString(),
                              maxQuan: controller.selectedMaxQuan.value == 0
                                  ? ""
                                  : controller.selectedMaxQuan.value
                                      .toString());
                          Get.back();
                        },
                        child: Container(
                          margin: const EdgeInsets.symmetric(vertical: 8),
                          padding: const EdgeInsets.symmetric(
                              vertical: 15, horizontal: 20),
                          decoration: BoxDecoration(
                            color: isSelected
                                ? primary.withOpacity(0.15)
                                : Colors.grey.shade100,
                            border: Border.all(
                              color:
                                  isSelected ? primary : Colors.grey.shade300,
                            ),
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Row(
                            children: [
                              Icon(
                                isSelected
                                    ? Icons.check_circle
                                    : Icons.circle_outlined,
                                color:
                                    isSelected ? primary : Colors.grey.shade400,
                                size: 24,
                              ),
                              const SizedBox(width: 15),
                              Expanded(
                                child: Text(
                                  crop.name,
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: isSelected
                                        ? primary
                                        : Colors.grey.shade800,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          )),
      isScrollControlled: true,
    );
  }

  void _openQuantityFilterBottomSheet() {
    Get.bottomSheet(
      Container(
        padding: const EdgeInsets.only(right: 20, left: 20, top: 10),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Center(
              child: Container(
                width: 60,
                height: 7,
                decoration: BoxDecoration(
                  color: Colors.grey.shade300,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
            ),
            const SizedBox(height: 16),
            Text(
              'select_quantity'.tr,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: primary,
              ),
            ),
            const SizedBox(height: 16),
            SizedBox(
              height: 300, // Adjust the height as needed
              child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  mainAxisSpacing: 8,
                  crossAxisSpacing: 8,
                  childAspectRatio: 2.5, // Adjust aspect ratio as needed
                ),
                itemCount: 10, // Total number of quantity ranges
                itemBuilder: (context, index) {
                  final quantities = [
                    {'label': '0 - 10', 'min': 0, 'max': 10},
                    {'label': '11 - 20', 'min': 11, 'max': 20},
                    {'label': '21 - 30', 'min': 21, 'max': 30},
                    {'label': '31 - 40', 'min': 31, 'max': 40},
                    {'label': '41 - 50', 'min': 41, 'max': 50},
                    {'label': '51 - 60', 'min': 51, 'max': 60},
                    {'label': '61 - 70', 'min': 61, 'max': 70},
                    {'label': '71 - 80', 'min': 71, 'max': 80},
                    {'label': '81 - 90', 'min': 81, 'max': 90},
                    {'label': '91 - 100', 'min': 91, 'max': 100},
                  ];
                  final quantity = quantities[index];
                  return _buildQuantityOption(
                    quantity['label']!.toString(),
                    quantity['min']!,
                    quantity['max']!,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildQuantityOption(String label, var minQuantity, var maxQuantity) {
    return GestureDetector(
      onTap: () {
        //controller.selectedCrop.value="";
        controller.selectedMinQuan.value = minQuantity;
        controller.selectedMaxQuan.value = maxQuantity;
        controller.filterWithCropQuantity(
          crop: controller.selectedCrop.value ?? "",
          minQuan: controller.selectedMinQuan.value,
          maxQuan: controller.selectedMaxQuan.value,
        );
        Get.back(); // Close the bottom sheet after selection
      },
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
        decoration: BoxDecoration(
          color: (controller.selectedMinQuan.value == minQuantity &&
                  controller.selectedMaxQuan.value == maxQuantity)
              ? Color(0xFF4D7881).withOpacity(0.3)
              : Colors.white,
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
            color: (controller.selectedMinQuan.value == minQuantity &&
                    controller.selectedMaxQuan.value == maxQuantity)
                ? primary
                : Colors.grey,
          ),
        ),
        child: Text(
          label,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: (controller.selectedMinQuan.value == minQuantity &&
                    controller.selectedMaxQuan.value == maxQuantity)
                ? primary
                : Colors.black,
          ),
        ),
      ),
    );
  }
}
