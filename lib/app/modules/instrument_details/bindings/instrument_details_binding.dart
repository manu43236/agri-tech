import 'package:get/get.dart';

import '../../details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import '../../saved_utilities/controllers/saved_utilities_controller.dart';
import '../controllers/instrument_details_controller.dart';

class InstrumentDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<InstrumentDetailsController>(
      () => InstrumentDetailsController(),
    );
    Get.lazyPut<SavedUtilitiesController>(
      () => SavedUtilitiesController(),
    );
    Get.lazyPut<DetailsOfLandlordUtilitiesController>(
      () => DetailsOfLandlordUtilitiesController(),
    );
  }
}
