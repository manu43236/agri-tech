import 'package:agritech/app/modules/home/services/land_by_id_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../../detail_recent_lands/data/land_by_id_model.dart';

class LandByIdRepoImpl extends LandByIdRepo with NetworkCheckService{
  final DioClient _dioClient;
  LandByIdRepoImpl(this._dioClient);
  @override
  Future<Either<LandByIdModel,Exception>> getLand({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("NO Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Land/getLandById', Method.get,params: params1);
        return result.fold((l){
          LandByIdModel model=LandByIdModel.fromJson(l.data);
          return Left(model);
        },(r)=>Right(Exception(r)));
      }catch(e){
        return right(Exception(e));
      }
    }
  }
}