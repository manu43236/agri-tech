import 'package:agritech/app/modules/home/services/state_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:agritech/app/modules/select_region/data/state_model.dart';
import 'package:dartz/dartz.dart';

class StateRepoImpl extends StateRepo with NetworkCheckService{
  final DioClient _dioClient;
  StateRepoImpl(this._dioClient);
  @override
  Future<Either<StateModel,Exception>> getState({lang})async{
    var data=await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result=await _dioClient.requestForAuth('State/getAllStates?language=$lang', Method.get);
        return  result.fold((l) {
          StateModel model = StateModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
         return Right(Exception(e));
      }
    }
  }
  
}