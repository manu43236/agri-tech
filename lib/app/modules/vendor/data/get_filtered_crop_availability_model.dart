import 'dart:convert';

// Convert JSON string to the model
GetFilteredCropAvailabilityModel getFilteredCropAvailabilityModelFromJson(String str) => GetFilteredCropAvailabilityModel.fromJson(json.decode(str));

// Convert model to JSON string
String getFilteredCropAvailabilityModelToJson(GetFilteredCropAvailabilityModel data) => json.encode(data.toJson());

class GetFilteredCropAvailabilityModel {
  int? statusCode;
  dynamic status;
  dynamic message;
  List<Result>? result;

  GetFilteredCropAvailabilityModel({
    this.statusCode,
    this.status,
    this.message,
    this.result,
  });

  factory GetFilteredCropAvailabilityModel.fromJson(Map<String, dynamic> json) => GetFilteredCropAvailabilityModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null
            ? []
            : List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
      };
}

class Result {
  LandDetails? landDetails;
  CropDetails? cropDetails;
  YieldDetails? yieldDetails;

  Result({
    this.landDetails,
    this.cropDetails,
    this.yieldDetails,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        landDetails: json["landDetails"] == null ? null : LandDetails.fromJson(json["landDetails"]),
        cropDetails: json["cropDetails"] == null ? null : CropDetails.fromJson(json["cropDetails"]),
        yieldDetails: json["yieldDetails"] == null ? null : YieldDetails.fromJson(json["yieldDetails"]),
      );

  Map<String, dynamic> toJson() => {
        "landDetails": landDetails?.toJson(),
        "cropDetails": cropDetails?.toJson(),
        "yieldDetails": yieldDetails?.toJson(),
      };
}

class CropDetails {
  int? id;
  dynamic name; // Changed enum to string for flexibility
  dynamic imageUrl;
  dynamic costPerHector;

  CropDetails({
    this.id,
    this.name,
    this.imageUrl,
    this.costPerHector,
  });

  factory CropDetails.fromJson(Map<String, dynamic> json) => CropDetails(
        id: json["id"],
        name: json["name"],
        imageUrl: json["image_url"],
        costPerHector: (json["costPerHector"] != null) ? json["costPerHector"].toDouble() : null,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image_url": imageUrl,
        "costPerHector": costPerHector,
      };
}

class LandDetails {
  int? id;
  dynamic latitude;
  dynamic longitude;
  dynamic village;
  dynamic mandal;
  dynamic district;
  dynamic state; // Changed enum to string for flexibility
  dynamic description;
  dynamic landInAcres;
  dynamic water; // Changed enum to string for flexibility

  LandDetails({
    this.id,
    this.latitude,
    this.longitude,
    this.village,
    this.mandal,
    this.district,
    this.state,
    this.description,
    this.landInAcres,
    this.water,
  });

  factory LandDetails.fromJson(Map<String, dynamic> json) => LandDetails(
        id: json["id"],
        latitude: json["latitude"]?.toDouble(),
        longitude: json["longitude"]?.toDouble(),
        village: json["village"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        description: json["description"],
        landInAcres: json["landInAcres"]?.toDouble(),
        water: json["water"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "latitude": latitude,
        "longitude": longitude,
        "village": village,
        "mandal": mandal,
        "district": district,
        "state": state,
        "description": description,
        "landInAcres": landInAcres,
        "water": water,
      };
}

class YieldDetails {
  int? id;
  dynamic outedDate;
  dynamic quantity;
  bool? status;

  YieldDetails({
    this.id,
    this.outedDate,
    this.quantity,
    this.status,
  });

  factory YieldDetails.fromJson(Map<String, dynamic> json) => YieldDetails(
        id: json["id"],
        outedDate: json["outedDate"] == null ? null : DateTime.parse(json["outedDate"]),
        quantity: (json["quantity"] != null) ? json["quantity"].toDouble() : null,
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "outedDate": outedDate?.toIso8601String(),
        "quantity": quantity,
        "status": status,
      };
}
