import 'package:agritech/models/send_notification_by_farmer_model.dart';
import 'package:dartz/dartz.dart';

abstract class SendNotificationByFarmerRepo{
  Future<Either<SendNotificationByFarmerModel,Exception>> sentByFarmer({params1});
}