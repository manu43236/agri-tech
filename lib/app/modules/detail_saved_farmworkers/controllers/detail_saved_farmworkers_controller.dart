import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../models/get_user_model.dart';
import '../../../../models/send_notification_by_landlord_to_worker_model.dart';
import '../../details_of_farmworker/data/get_all_available_jobs_model.dart';
import '../../details_of_farmworker/data/send_work_notification_by_landlord_or_farmer_fetch_model.dart';
import '../../details_of_farmworker/services/details_of_farmworker_repo_impl.dart';
import '../../home/services/get_user_repo_impl.dart';
import '../../notifications/services/send_notification_by_landlord_to_worker_repo_impl.dart';
import '../../saved/data/get_all_saved_farmworkers_model.dart';
import '../../saved/services/saved_repo_impl.dart';

class DetailSavedFarmworkersController extends BaseController {

  Rx<GetUserModel> getUserProfileModel = Rx(GetUserModel());
  Rx<GetAllSavedFarmworkerModel> getAllSavedFarmworkerModel =
      Rx(GetAllSavedFarmworkerModel());
  Rx<SendNotificationByLandlordToWorkerModel>
      sendNotificationByLandlordToWorkerModel =
      Rx(SendNotificationByLandlordToWorkerModel());
  Rx<SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel>
      sendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel =
      Rx(SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel());
  Rx<GetAvailableJobsModel> getAvailableJobsModel = Rx(GetAvailableJobsModel());
  

  var userId=''.obs;
  var role=''.obs;
  var ongoingJobDescriptions = <Map<String, String>>[].obs;
  var apiUserByProfileIdStatus=ApiStatus.LOADING.obs;

   var apiStatusWorker = false.obs;
   var statusForLandlord = ''.obs;
   var selectedJobId = ''.obs;
   var workerId=0.obs;
   var selectedJob = Rxn<Map<String, String>>();


  @override
  void onInit() async{
    super.onInit();
    userId.value = await SecureStorage().readData(key: "id") ?? "";
    role.value=await SecureStorage().readData(key:"role"??'');
    if(Get.arguments!=null){
      await getUserByProfileId(id: Get.arguments["workerId"]);
      await allFarmworkersSaved(id: userId.value);
      await getAvailableJobs(userId: userId.value);
    }
    //ongoingJobDescriptions=Get.find<LandlordController>().ongoingJobDescriptions;
  }

    // Get user profile by id API
  Future<void> getUserByProfileId({id}) async {
    apiUserByProfileIdStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await GetUserRepoImpl(dioClient).getUser(id: id);
      print("======================== user by id ====================");
      print('Result $result');
      result.fold((model) {
        getUserProfileModel.value = model;
        apiUserByProfileIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiUserByProfileIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> allFarmworkersSaved({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient)
          .allFarmworkers(userId: id);
      print("===================== get all saved farmworkers ==============");
      print(result);
      result.fold((model) {
        getAllSavedFarmworkerModel.value = model;
        print(getAllSavedFarmworkerModel.value.result!.farmerCount);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

   //send notification by landlord to worker API
  Future<void> sentNotificationByLandlordToWorker({senderId,workerId,senderRole,jobId}) async {
    apiStatusWorker.value = false;
    try {
      var result = await SendNotificationByLandlordToWorkerRepoImpl(dioClient)
          .landlordToWorkerNotification(params1: {
        "senderId": senderId,//Get.find<HomeController>().selectedLandlordId.value,
        "farmworkerId":workerId, //Get.find<HomeController>().farmWorkerId.value,
        "senderRole": senderRole,//Get.find<HomeController>().role.value,
        "jobId": jobId//selectedJobId.value,
      });
      print(
          "======================== sent notification by landlord id to farmworker id ====================");
      print(result);
      result.fold((model) {
        sendNotificationByLandlordToWorkerModel.value = model;
        print(sendNotificationByLandlordToWorkerModel.value.result != null
            ? sendNotificationByLandlordToWorkerModel
                .value.result!.notification!.message
            : '');
        if (sendNotificationByLandlordToWorkerModel.value.statusCode == 200) {
          Get.snackbar('Success', 'Notification sent successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (sendNotificationByLandlordToWorkerModel.value.statusCode ==
            400) {
          Get.snackbar('Failure', 'Notification already sent to the farmworker',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatusWorker.value = true;
      }, (r) {
        apiStatusWorker.value = false;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

   //fetched job details API
  Future<void> fetchedJobDetails({senderId,workerId,senderRole}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailsOfFarmworkerRepoImpl(dioClient)
          .fetchFarmworkers(params1: {
        "senderId":   senderId, //Get.find<HomeController>().selectedLandlordId.value,
        "farmworkerId":workerId, //Get.find<HomeController>().farmWorkerId.value,
        "senderRole": senderRole//Get.find<HomeController>().role.value,
      });
     
      result.fold((model) {
        sendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel.value =
            model;
        for (var element
            in sendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel
                .value.result!.controlData!) {
          print('for loop');
          print(element.jobId);
          print(selectedJobId.value);
          if (role.value == 'Landlord') {
            statusForLandlord.value == "";
            if (element.jobId.toString() == selectedJobId.value) {
              print('elwmmmmmmmmmm');
              statusForLandlord.value = element.statusForLandlord!;
            }
          } else if (role.value == 'Farmer') {
            statusForLandlord.value == "";
            if (element.jobId.toString() == selectedJobId.value) {
              print('elwmmmmmmmmmm');
              statusForLandlord.value = element.statusForFarmer!;
            }
          }
        }

        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //get available jobs API
  Future<void> getAvailableJobs({userId}) async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result = await DetailsOfFarmworkerRepoImpl(dioClient).availableJobs(userId: userId);
      print('=============== available jobs list =========');
      print(result);
      result.fold((model) {
        getAvailableJobsModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
