import 'package:get/get.dart';

import '../controllers/detail_saved_landlord_controller.dart';

class DetailSavedLandlordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailSavedLandlordController>(
      () => DetailSavedLandlordController(),
    );
  }
}
