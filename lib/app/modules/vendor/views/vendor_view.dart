import 'package:agritech/app/modules/vendor/views/widgets/purchased_crops_widgets.dart';
import 'package:agritech/app/modules/vendor/views/widgets/recent_crops_widgets.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/vendor_controller.dart';

class VendorView extends GetView<VendorController> {
  const VendorView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  SafeArea(
      child: RefreshIndicator(
        onRefresh: ()async {
          controller.recentCrops();
          controller.purchasedCrops();
        },
        child: const SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RecentCropsWidgets(),
              SizedBox(height: 10,),
              PurchasedCropsWidgets()
            ],
          ),
        ),
      ),
     ),
    );
  }
}
