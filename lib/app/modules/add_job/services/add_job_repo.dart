import 'package:agritech/models/add_job_model.dart';
import 'package:dartz/dartz.dart';

abstract class AddJobRepo{
  Future<Either<AddJobModel,Exception>>  doAddJob({params1});
}