import 'package:get/get.dart';

import '../controllers/hired_farmers_details_controller.dart';

class HiredFarmersDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HiredFarmersDetailsController>(
      () => HiredFarmersDetailsController(),
    );
  }
}
