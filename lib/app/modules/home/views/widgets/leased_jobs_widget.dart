import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:agritech/core/conts/img_const.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/base/base_controller.dart';
import '../../../../../core/conts/color_consts.dart';
import '../../../../../core/themes/text_styles.dart';
import '../../../../../core/utils/widget_utils/shimmers.dart';
import '../../../../routes/app_pages.dart';

class LeasedJobsWidget extends GetView<LandlordController> {
  const LeasedJobsWidget({super.key});

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Obx(
        () => Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'leased_jobs'.tr,
              style: TextStyles.kTSDS14W500.copyWith(color: colorHello),
            ),
            const SizedBox(
              height: 5,
            ),
            controller.apiStatus.value == ApiStatus.LOADING
                ? Shimmers().getShimmerItem()
                : controller.getLeasedJobDetailsModel.value.result !=
                            null &&
                        controller.getLeasedJobDetailsModel.value.result!
                            .jobs!.isNotEmpty
                    ? Column(
                        children: [
                          SizedBox(
                            height: Get.height * 0.14,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: controller
                                    .getLeasedJobDetailsModel
                                    .value
                                    .result!
                                    .jobs!
                                    .length,
                                itemBuilder: (context, index) {
                                  var land = controller
                                      .getLeasedJobDetailsModel
                                      .value
                                      .result!
                                      .jobs![index];
                                  final imageUrl =
                                      land.image ?? defaultImageUrl;
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: InkWell(
                                      onTap: () async {
                                        Get.find<HomeController>().jobId.value=land.id.toString();
                                        //Get.find<HomeController>().getIdJobs();
                                        Get.toNamed(
                                            Routes.DETAIL_LEASED_JOBS,arguments: {"jobId":Get.find<HomeController>().jobId.value}
                                           );
                                      },
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(12),
                                        child: Image.network(
                                          imageUrl ?? '',
                                          width: Get.width * 0.42,
                                          fit: BoxFit.cover,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            // Display a local asset if the network image fails
                                            return Image.asset(
                                              defaultImageUrl,
                                              width: Get.width * 0.42,
                                              fit: BoxFit.cover,
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                          ),
                          const SizedBox(height: 5),
                          Align(
                            alignment: Alignment.topRight,
                            child: InkWell(
                              onTap: () async {
                                // Get.find<SelectRegionController>().isLandlordLands.value = false;
                                // await Get.find<LandlordController>().addAllLands();
                                Get.toNamed(Routes.AVAILABLE_LEASED_JOBS);
                              },
                              child: Text(
                                'view_all'.tr,
                                style: TextStyles.kTSCF12W500.copyWith(
                                  fontSize: 10,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: Get.height * 0.03,
                          ),
                          Center(
                            child: Text(
                              'no_leased_jobs'.tr,
                              style: TextStyles.kTSFS18W500.copyWith(
                                color: colorGrey,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Get.height * 0.03,
                          ),
                        ],
                      ),
          ],
        ),
      ),
    );
  }
}
