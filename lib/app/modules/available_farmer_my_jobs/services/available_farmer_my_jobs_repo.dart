import 'package:dartz/dartz.dart';

import '../../home/data/get_all_jobs_by_farmer_model.dart';

abstract class AvailableFarmerMyJobsRepo{
  Future<Either<GetAllJobsByFarmerModel,Exception>> getFarmerSearchJobs();
}