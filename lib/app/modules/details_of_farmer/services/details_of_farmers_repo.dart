import 'package:agritech/app/modules/details_of_farmer/data/get_all_available_lands_of_landlord_model.dart';
import 'package:agritech/app/modules/details_of_farmer/data/near_by_farmers_model.dart';
import 'package:agritech/app/modules/details_of_farmer/data/send_notification_by_landlord_fetch_model.dart';
import 'package:dartz/dartz.dart';

abstract class DetailsOfFarmersRepo{
  Future<Either<NearByFarmersModel,Exception>> nearByFarmers();
  Future<Either<GetAllAvailableLandsOfLandlordModel,Exception>> availableLands({String id});
  Future<Either<SendNotificationByLandLordFetchModel,Exception>> fetchdetails({params1});
}