// To parse this JSON data, do
//
//     final saveFarmworkerAsFavouriteModel = saveFarmworkerAsFavouriteModelFromJson(jsonString);

import 'dart:convert';

SaveFarmworkerAsFavouriteModel saveFarmworkerAsFavouriteModelFromJson(String str) => SaveFarmworkerAsFavouriteModel.fromJson(json.decode(str));

String saveFarmworkerAsFavouriteModelToJson(SaveFarmworkerAsFavouriteModel data) => json.encode(data.toJson());

class SaveFarmworkerAsFavouriteModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    SaveFarmworkerAsFavouriteModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SaveFarmworkerAsFavouriteModel.fromJson(Map<String, dynamic> json) => SaveFarmworkerAsFavouriteModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    bool? flag;
    int? id;
    int? userid;
    int? farmworkerid;
    String? role;
    DateTime? updatedAt;
    DateTime? createdAt;
    dynamic landlordid;
    dynamic farmerid;
    dynamic landid;
    dynamic jobid;

    Result({
        this.flag,
        this.id,
        this.userid,
        this.farmworkerid,
        this.role,
        this.updatedAt,
        this.createdAt,
        this.landlordid,
        this.farmerid,
        this.landid,
        this.jobid,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        flag: json["flag"],
        id: json["id"],
        userid: json["userid"],
        farmworkerid: json["farmworkerid"],
        role: json["role"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        landlordid: json["landlordid"],
        farmerid: json["farmerid"],
        landid: json["landid"],
        jobid: json["jobid"],
    );

    Map<String, dynamic> toJson() => {
        "flag": flag,
        "id": id,
        "userid": userid,
        "farmworkerid": farmworkerid,
        "role": role,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "landlordid": landlordid,
        "farmerid": farmerid,
        "landid": landid,
        "jobid": jobid,
    };
}
