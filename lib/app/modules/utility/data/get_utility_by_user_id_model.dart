// To parse this JSON data, do
//
//     final getUtilityByUserIdModel = getUtilityByUserIdModelFromJson(jsonString);

import 'dart:convert';

GetUtilityByUserIdModel getUtilityByUserIdModelFromJson(String str) => GetUtilityByUserIdModel.fromJson(json.decode(str));

String getUtilityByUserIdModelToJson(GetUtilityByUserIdModel data) => json.encode(data.toJson());

class GetUtilityByUserIdModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<Result>? result;

    GetUtilityByUserIdModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetUtilityByUserIdModel.fromJson(Map<String, dynamic> json) => GetUtilityByUserIdModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    int? utililityId;
    dynamic nameOfInstrument;
    dynamic pricePerDay;
    dynamic brandName;
    dynamic state;
    dynamic district;
    dynamic mandal;
    dynamic village;
    dynamic description;
    dynamic imageId;
    dynamic image;
    dynamic latitude;
    dynamic longitude;
    bool? status;
    dynamic createdAt;
    dynamic updatedAt;

    Result({
        this.id,
        this.utililityId,
        this.nameOfInstrument,
        this.pricePerDay,
        this.brandName,
        this.state,
        this.district,
        this.mandal,
        this.village,
        this.description,
        this.imageId,
        this.image,
        this.latitude,
        this.longitude,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        utililityId: json["utililityId"],
        nameOfInstrument: json["nameOfInstrument"],
        pricePerDay: json["pricePerDay"],
        brandName: json["brandName"],
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
        description: json["description"],
        imageId: json["image_id"],
        image: json["image"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        status: json["status"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "utililityId": utililityId,
        "nameOfInstrument": nameOfInstrument,
        "pricePerDay": pricePerDay,
        "brandName": brandName,
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
        "description": description,
        "image_id": imageId,
        "image": image,
        "latitude": latitude,
        "longitude": longitude,
        "status": status,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
