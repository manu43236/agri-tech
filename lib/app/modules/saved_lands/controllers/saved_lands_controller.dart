import 'package:agritech/app/modules/saved/services/saved_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../saved/data/get_all_saved_lands_model.dart';

class SavedLandsController extends BaseController {

  Rx<AllSavedLandsModel> allSavedLandsModel = Rx(AllSavedLandsModel());

  var userId=''.obs;

  @override
  void onInit() async{
    super.onInit();
    userId.value = await SecureStorage().readData(key: "id") ?? "";
    await allLandsSaved(id:userId.value);
  }

    //get all saved lands API
  Future<void> allLandsSaved({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient)
          .allSavedLands(userId: id);
      print("===================== get all saved lands ==============");
      print(result);
      result.fold((model) {
        allSavedLandsModel.value = model;
        print(allSavedLandsModel.value.result!.landCount!);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
