import 'package:agritech/models/save_farmer_as_favourite_model.dart';
import 'package:agritech/models/save_landlord_as_favourite_model.dart';
import 'package:dartz/dartz.dart';

abstract class SaveFarmerAsFavouriteRepo{
  Future<Either<SaveFarmerAsFavouriteModel,Exception>> saveFarmer({params1});
  Future<Either<SaveLandlordAsFavouriteModel,Exception>> saveLandlord({params1});
}