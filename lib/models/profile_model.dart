// To parse this JSON data, do
//
//     final profileModel = profileModelFromJson(jsonString);

import 'dart:convert';

ProfileModel profileModelFromJson(String str) => ProfileModel.fromJson(json.decode(str));

String profileModelToJson(ProfileModel data) => json.encode(data.toJson());

class ProfileModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    ProfileModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory ProfileModel.fromJson(Map<String, dynamic> json) => ProfileModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    dynamic firstName;
    dynamic mobileNumber;
    dynamic role;
    dynamic age;
    dynamic gender;
    dynamic address;
    dynamic village;
    dynamic mandal;
    dynamic district;
    dynamic state;
    int? otp;
    dynamic longitude;
    dynamic latitude;
    dynamic accesstoken;
    dynamic profileId;
    dynamic imageUrl;
    dynamic description;
    dynamic locationDetails;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;

    Result({
        this.id,
        this.firstName,
        this.mobileNumber,
        this.role,
        this.age,
        this.gender,
        this.address,
        this.village,
        this.mandal,
        this.district,
        this.state,
        this.otp,
        this.longitude,
        this.latitude,
        this.accesstoken,
        this.profileId,
        this.imageUrl,
        this.description,
        this.locationDetails,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        firstName: json["firstName"],
        mobileNumber: json["mobileNumber"],
        role: json["role"],
        age: json["age"],
        gender: json["gender"],
        address: json["address"],
        village: json["village"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        otp: json["otp"],
        longitude: json["longitude"],
        latitude: json["latitude"],
        accesstoken: json["accesstoken"],
        profileId: json["profileId"],
        imageUrl: json["image_url"],
        description: json["Description"],
        locationDetails: json["location_details"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "mobileNumber": mobileNumber,
        "role": role,
        "age": age,
        "gender": gender,
        "address": address,
        "village": village,
        "mandal": mandal,
        "district": district,
        "state": state,
        "otp": otp,
        "longitude": longitude,
        "latitude": latitude,
        "accesstoken": accesstoken,
        "profileId": profileId,
        "image_url": imageUrl,
        "Description": description,
        "location_details": locationDetails,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
