// To parse this JSON data, do
//
//     final sendNotificationByLandLordFetchModel = sendNotificationByLandLordFetchModelFromJson(jsonString);

import 'dart:convert';

SendNotificationByLandLordFetchModel sendNotificationByLandLordFetchModelFromJson(String str) => SendNotificationByLandLordFetchModel.fromJson(json.decode(str));

String sendNotificationByLandLordFetchModelToJson(SendNotificationByLandLordFetchModel data) => json.encode(data.toJson());

class SendNotificationByLandLordFetchModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<Result>? result;

    SendNotificationByLandLordFetchModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SendNotificationByLandLordFetchModel.fromJson(Map<String, dynamic> json) => SendNotificationByLandLordFetchModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    dynamic statusForLandlord;
    LandDetails? landDetails;
    FarmerDetails? farmerDetails;

    Result({
        this.statusForLandlord,
        this.landDetails,
        this.farmerDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        statusForLandlord: json["status_for_landlord"],
        landDetails: json["landDetails"] == null ? null : LandDetails.fromJson(json["landDetails"]),
        farmerDetails: json["farmerDetails"] == null ? null : FarmerDetails.fromJson(json["farmerDetails"]),
    );

    Map<String, dynamic> toJson() => {
        "status_for_landlord": statusForLandlord,
        "landDetails": landDetails?.toJson(),
        "farmerDetails": farmerDetails?.toJson(),
    };
}

class FarmerDetails {
    int? id;
    dynamic firstName;
    dynamic imageUrl;

    FarmerDetails({
        this.id,
        this.firstName,
        this.imageUrl,
    });

    factory FarmerDetails.fromJson(Map<String, dynamic> json) => FarmerDetails(
        id: json["id"],
        firstName: json["firstName"],
        imageUrl: json["image_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "image_url": imageUrl,
    };
}

class LandDetails {
    int? id;
    int? landLordId;
    dynamic fullName;
    dynamic imageUrl;

    LandDetails({
        this.id,
        this.landLordId,
        this.fullName,
        this.imageUrl,
    });

    factory LandDetails.fromJson(Map<String, dynamic> json) => LandDetails(
        id: json["id"],
        landLordId: json["landLordId"],
        fullName: json["fullName"],
        imageUrl: json["image_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "fullName": fullName,
        "image_url": imageUrl,
    };
}
