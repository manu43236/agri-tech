import 'package:agritech/app/modules/home/services/village_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:agritech/app/modules/select_region/data/village_model.dart';
import 'package:dartz/dartz.dart';

class VillageRepoImpl extends VillageRepo with NetworkCheckService{
  final DioClient _dioClient;
  VillageRepoImpl( this._dioClient);
  @override
  Future<Either<VillageModel,Exception>> getVillage({id,lang})async{
    var data=await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Village/getVillagesByMandalId?mandal_id=$id&language=$lang',Method.get);
        return result.fold((l){
          VillageModel model=VillageModel.fromJson(l.data);
          return left(model);
        },(r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}