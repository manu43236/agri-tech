import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/notifications/data/get_notifications_model.dart';
import 'package:agritech/app/modules/notifications/data/notifications_count_model.dart';
import 'package:agritech/app/modules/notifications/data/reply_to_farmer_by_landlord_model.dart';
import 'package:agritech/app/modules/notifications/services/approved_by_landlord_repo_impl.dart';
import 'package:agritech/app/modules/notifications/services/delete_notification_repo_impl.dart';
import 'package:agritech/app/modules/notifications/services/mark_as_viewed_repo_impl.dart';
import 'package:agritech/app/modules/notifications/services/notifications_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:get/get.dart';
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../models/send_notification_by_farmer_model.dart';
import '../../home/services/send_notification_by_farmer_repo_impl.dart';
import '../data/accept_by_landlord_or_farmer_for_job_taken_farmworkermodel.dart';
import '../data/accept_request_by_farmer_or_landlord_model.dart';
import '../data/respond_by_farmworker_to_landlord_or_farmer_model.dart';
import '../services/farmer_delete_notification_repo_impl.dart';

class NotificationsController extends BaseController {
  var id = "".obs;
  var role = "".obs;
  var updatedId = "".obs;
  var isButton = false.obs;
  var selectedNotificationIndex = (-1).obs;

  var landlordId = "".obs;
  var landId = "".obs;
  var notificationId = "".obs;
  var notificationIds = "".obs;
  var farmerNotificationId = "".obs;
  var farmworkerNotificationId = "".obs;
  var landlordNotificationId = "".obs;

  var farmworkerIds=''.obs;
  var jobIds=''.obs;
  var toSenderId=''.obs;
  
  Rx<SendNotificationByFarmerModel> sendNotificationByFarmerModel =
      Rx(SendNotificationByFarmerModel());
  Rx<ReplyToFarmerByLandlordModel> replyToFarmerByLandlordModel =
      Rx(ReplyToFarmerByLandlordModel());
  Rx<AcceptRequestByFarmerOrLandlordModel> acceptRequestByFarmerOrLandlordModel =
      Rx(AcceptRequestByFarmerOrLandlordModel());
  Rx<NotificationsCountModel> notificationsCountModel =
      Rx(NotificationsCountModel());
  Rx<GetNotificationsModel> getNotificationsModel = Rx(GetNotificationsModel());
  Rx<RespondByFarmworkerToLandlordOrFarmerModel> respondByFarmworkerToLandlordOrFarmerModel = Rx(RespondByFarmworkerToLandlordOrFarmerModel());
  Rx<AcceptbylandlordOrFarmerForJobTakenFarmworkerModel> acceptbylandlordOrFarmerForJobTakenFarmworkerModel = Rx(AcceptbylandlordOrFarmerForJobTakenFarmworkerModel());

  final HomeController homeController=Get.find<HomeController>();

  @override
  void onInit() async {
    print("Init call");
    super.onInit();
    id.value = await SecureStorage().readData(key: "id") ?? "";
    role.value = await SecureStorage().readData(key: "role") ?? "";
    print('id:${id.value}');
    print('role:${role.value}');
    getNotifications(userId: id.value);
  }

  void updateState(int index) {
  
    if (selectedNotificationIndex.value == index && isButton.value) {
      // Collapse if the same item is tapped again
      isButton.value = false;
      selectedNotificationIndex.value = -1;
    } else {
      // Expand the buttons for the new item
      isButton.value = true;
      selectedNotificationIndex.value = index;
      print('isButton:${ isButton.value}');
      print('index tapped${selectedNotificationIndex.value}');
    }
  }

  // mark AS Viewed API
  Future<void> markAsViewed() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await MarkAsViewedRepoImpl(dioClient).viewedNotifications(
          params1: {"notificationId": notificationId.value});
      print('================ mark as viewed ===============');
      print(result);
      return result.fold((l) {
        print(l['message']);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print("error$e");
    }
  }

  // approved by landlord API
  Future<void> approved(int index) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await ApprovedByLandlordRepoImpl(dioClient)
          .approvedNotifications(
              params1: {"notificationId": notificationId.value});
      print('================ approved by landlord ===============');
      print(result);
      return result.fold((l) {
        print(l['message']);
        if (l['statusCode'] == 200) {
          Get.snackbar(
              'success', 'Notification Approved and lease created successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (l['statusCode'] == 400) {
          Get.snackbar('failed', 'Land not found for the landlord',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print("error$e");
    }
  }

  //delete notification API
  Future<void> deleteNotificationById() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DeleteNotificationRepoImpl(dioClient)
          .deleteNotification(
              params1: {"notificationId": landlordNotificationId.value});
      print('================ delete notification id ===============');
      print(result);
      return result.fold((l) {
        print(l['message']);
        if (l['statusCode'] == 200) {
          Get.snackbar('success', 'Notification deleted successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (l['statusCode'] == 400) {
          Get.snackbar('failed', 'Notification not found',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print("error$e");
    }
  }

  // farmer delete notification API
  Future<void> deleteFarmerNotificationById() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await FarmerDeleteNotificationRepoImpl(dioClient)
          .deleteFarmerNotification(
              params1: {"notificationId": farmerNotificationId.value});
      print('================ delete farmer notification id ===============');
      print(result);
      return result.fold((l) {
        print(l['message']);
        if (l['statusCode'] == 200) {
          Get.snackbar('success', 'Notification deleted successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (l['statusCode'] == 400) {
          Get.snackbar('failed', 'Notification not found',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print("error$e");
    }
  }

  //send notification by farmer API
  Future<void> sentNotificationByFarmer() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SendNotificationByFarmerRepoImpl(dioClient)
          .sentByFarmer(params1: {
        "landLordId": landlordId.value,
        "farmerId": Get.find<HomeController>().userId.value,
        "landId": landId.value
      });
      print('land id is:${landId.value}');
      print('farmer id is:${Get.find<HomeController>().userId.value}');
      print('landlord id is:${landlordId.value}');
      print(
          "======================== sent notification by farmer id ====================");
      print(result);
      result.fold((model) {
        sendNotificationByFarmerModel.value = model;
        print(sendNotificationByFarmerModel.value.result != null
            ? sendNotificationByFarmerModel.value.result!.message
            : '');
        if (sendNotificationByFarmerModel.value.statusCode == 200) {
          Get.snackbar('Success', 'Notification sent successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (sendNotificationByFarmerModel.value.statusCode == 400) {
          Get.snackbar('Failure', 'Notification already sent to the landlord',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //get notification count
  void getNotificationsCount() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await NotificationsRepoImpl(dioClient).getCount();
      print("===================== get unread count ==============");
      print(result);
      result.fold((model) {
        notificationsCountModel.value = model;
        print(notificationsCountModel.value.result!.unreadCount);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //get notification Api
  void getNotifications({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await NotificationsRepoImpl(dioClient).getAllNotifications(userId: userId);
      print("===================== get all notifications ==============");
      print(result);
      result.fold((model) {
        getNotificationsModel.value = model;
        
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //respondtoLandlordOrFarmerByWorker Api
  Future<void> respondtoLandlordOrFarmerByWorker() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await NotificationsRepoImpl(dioClient).respondByFarmworker();
      print("===================== respond to landlord or farmer by worker ==============");
      print(result);
      result.fold((model) {
        respondByFarmworkerToLandlordOrFarmerModel.value = model;
        print(respondByFarmworkerToLandlordOrFarmerModel.value.result!.notification);
         if (respondByFarmworkerToLandlordOrFarmerModel.value.statusCode == 200) {
          Get.snackbar('Success', 'Notification sent successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (respondByFarmworkerToLandlordOrFarmerModel.value.statusCode == 400) {
          Get.snackbar('Failure', 'Notification already sent to the landlord',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //accept by landlord or farmer for job taken farmworker Api
  Future<void> acceptByLandlordOrFarmerForJobTaken() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await NotificationsRepoImpl(dioClient).acceptLandlordOrFarmer();
      print("===================== accept by landlord or farmer for job taken farmworker ==============");
      print(result);
      result.fold((model) {
        acceptbylandlordOrFarmerForJobTakenFarmworkerModel.value = model;
        print(acceptbylandlordOrFarmerForJobTakenFarmworkerModel.value.result!.lease);
         if (acceptbylandlordOrFarmerForJobTakenFarmworkerModel.value.statusCode == 200) {
          Get.snackbar('Success', 'Notification sent successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (acceptbylandlordOrFarmerForJobTakenFarmworkerModel.value.statusCode == 400) {
          Get.snackbar('Failure', 'Notification already sent to the landlord',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //accept request by landlord or farmer when worker sent API
  Future<void> acceptRequestByFarmerOrLandlordOfWorker({id, jobId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await NotificationsRepoImpl(dioClient)
          .acceptRequestSentByWorker(params1: {
        "farmworkerId": id,
        "recipientId": Get.find<HomeController>().userId.value,
        "recipientRole": Get.find<HomeController>().role.value,
        "jobId": jobId
      });
      print('farmworker id is:${Get.find<HomeController>().selectedLandlordId.value}');
      print('recipient id is:${id}');
      print('recipient role is:${role}');
      print('job id:$jobId');
      print(
          "======================== sent notification by farmworker id ====================");
      print(result);
      result.fold((model) {
        acceptRequestByFarmerOrLandlordModel.value = model;
        print(acceptRequestByFarmerOrLandlordModel.value.result != null
            ? acceptRequestByFarmerOrLandlordModel.value.result!.notification!.message
            : '');
        if (acceptRequestByFarmerOrLandlordModel.value.statusCode == 200) {
          Get.snackbar('Success', 'Notification sent successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (acceptRequestByFarmerOrLandlordModel.value.statusCode == 400) {
          Get.snackbar('', 'Notification already sent to the landlord',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //reply to farmer by landlord API
  Future<void> replyToFarmerByLandlord({farmerId,landId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await NotificationsRepoImpl(dioClient)
          .replyToFarmer(params1: {
        "landLordId":int.parse(Get.find<HomeController>().userId.value),
        "farmerId": farmerId,
        "landId": landId
      });
      print('land id is:${landId}');
      print('landlord id is:${Get.find<HomeController>().userId.value}');
      print('farmer id is:${farmerId}');
      print(
          "======================== reply to farmer by landlord ====================");
      print(result);
      result.fold((model) {
        replyToFarmerByLandlordModel.value = model;
        if (replyToFarmerByLandlordModel.value.statusCode == 200) {
          Get.snackbar('Success', 'Notification sent successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (replyToFarmerByLandlordModel.value.statusCode == 400) {
          Get.snackbar('Failure', 'Notification already sent to the landlord',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
