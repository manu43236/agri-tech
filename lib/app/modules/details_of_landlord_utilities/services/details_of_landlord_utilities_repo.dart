import 'package:agritech/app/modules/details_of_landlord_utilities/data/create_utility_payment_model.dart';
import 'package:agritech/app/modules/utility/data/get_recent_four_utilities_model.dart';
import 'package:dartz/dartz.dart';

import '../../saved_utilities/data/get_all_saved_utility_model.dart';
import '../data/saved_utility_model.dart';

abstract class DetailsOfLandlordUtilitiesRepo{
  Future<Either<GetRecentFourUtilitiesModel,Exception>> getUtilitiesByLocation();
  Future<Either<CreateUtilityPaymentModel,Exception>>  createUtilityPayment({params1});
  Future<Either<CreateUtilityPaymentModel,Exception>> getUtilityPayment();
  Future<Either<SavedUtilityModel,Exception>> savedUtility({id});
}