import 'package:agritech/app/modules/kyc_details_page/data/upload_id_document_model.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dio/dio.dart' as dio;
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../routes/app_pages.dart';
import '../services/kyc_details_page_repo_impl.dart';

class KycDetailsPageController extends BaseController {
  RxString selectedIdentityType = "Aadhaar".obs;
  RxBool canUploadDocuments = false.obs;
  Rx<UploadIdDocumentModel> uploadIdDocumentModel = Rx(UploadIdDocumentModel());

  var proofImage = "".obs;
  var vendeId = "".obs;
  var frontImage = "".obs;
  var backImage = "".obs;
  var selfieImage = "".obs;
  var imagePath = [].obs;
  var userIds=''.obs;

  /// Sets the selected identity type and resets the images
  void setIdentityType(String value) {
    selectedIdentityType.value = value;
    canUploadDocuments.value = true;
    proofImage.value = "";
    frontImage.value = "";
    backImage.value = "";
    selfieImage.value = "";
  }

  /// Function to pick a front image from either camera or gallery
  Future<void> pickFrontImage(ImageSource source) async {
    final pickedFile = await ImagePicker().pickImage(source: source);
    if (pickedFile != null) {
      frontImage.value = pickedFile.path;
      imagePath.value.add(frontImage.value);
      print(imagePath.value);
    } else {
      Get.snackbar("Error", "No image selected for front ID.");
    }
  }

  /// Function to pick a back image from either camera or gallery
  Future<void> pickBackImage(ImageSource source) async {
    final pickedFile = await ImagePicker().pickImage(source: source);
    if (pickedFile != null) {
      backImage.value = pickedFile.path;
      imagePath.value.add(backImage.value);
      print(imagePath.value);
    } else {
      Get.snackbar("Error", "No image selected for back ID.");
    }
  }

  /// Function to take a selfie using the camera
  Future<void> pickSelfieImage(ImageSource camera) async {
    final pickedFile = await ImagePicker().pickImage(source: ImageSource.camera);
    if (pickedFile != null) {
      selfieImage.value = pickedFile.path;
      print('Selfie: ${selfieImage.value}');
    } else {
      Get.snackbar("Error", "No selfie captured.");
    }
  }

  /// Validate that all required images have been uploaded
  bool validateImages() {
    return frontImage.value.isNotEmpty &&
        backImage.value.isNotEmpty &&
        selfieImage.value.isNotEmpty;
  }

  @override
  void onInit()async {
    super.onInit();
     userIds.value = await SecureStorage().readData(key: "userIds") ?? "";
     print('user id${userIds.value}');
  }

Future<void> uploadIdDocument() async {
  apiStatus.value = ApiStatus.LOADING;
  print('Starting upload process');
  try {
    String documentType;

    switch (selectedIdentityType.value) {
      case "Aadhaar":
        documentType = "adharId";
        break;
      case "Pan Card":
        documentType = "panId";
        break;
      case "Voter Id":
        documentType = "voterId";
        break;
      default:
        Get.snackbar("Error", "Invalid identity type selected.");
        return;
    }

    // Prepare form data
    var formData = dio.FormData();

    formData.fields.addAll([
      MapEntry("vendeId", userIds.value),
      MapEntry("documentType", documentType),
    ]);

    if (selfieImage.value.isNotEmpty) {
      formData.files.add(MapEntry(
        "selfie",
        await dio.MultipartFile.fromFile(
          selfieImage.value,
          filename: selfieImage.value.split('/').last,
        ),
      ));
    }

    if (frontImage.value.isNotEmpty) {
      formData.files.add(MapEntry(
        "documentFront",
        await dio.MultipartFile.fromFile(
          frontImage.value,
          filename: frontImage.value.split('/').last,
        ),
      ));
    }

    if (backImage.value.isNotEmpty) {
      formData.files.add(MapEntry(
        "documentBack",
        await dio.MultipartFile.fromFile(
          backImage.value,
          filename: backImage.value.split('/').last,
        ),
      ));
    }

    print('FormData prepared: ${formData.fields}');

    // Send the API request
    var result =
        await KycDetailsPageRepoImpl(dioClient).uploadId(params1: formData);

    print('Result: $result');

    result.fold((success) {
      uploadIdDocumentModel.value = success;
      Get.toNamed(Routes.DASHBOARD);
      Get.snackbar("Success", "Document uploaded successfully.",backgroundColor: colorGreen,colorText: colorBlack);
      apiStatus.value=ApiStatus.SUCCESS;
    }, (error) {
      print('Error during upload: $error');
      Get.snackbar("Error", error.toString());
    });
  } catch (e, stacktrace) {
    print('Exception: $e');
    print('Stacktrace: $stacktrace');
    Get.snackbar("Error", "An unexpected error occurred: $e");
  } 
}


 @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
