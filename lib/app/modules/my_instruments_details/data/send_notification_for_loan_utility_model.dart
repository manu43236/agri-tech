// To parse this JSON data, do
//
//     final sendNotificationForLoanUtilityModel = sendNotificationForLoanUtilityModelFromJson(jsonString);

import 'dart:convert';

SendNotificationForLoanUtilityModel sendNotificationForLoanUtilityModelFromJson(String str) => SendNotificationForLoanUtilityModel.fromJson(json.decode(str));

String sendNotificationForLoanUtilityModelToJson(SendNotificationForLoanUtilityModel data) => json.encode(data.toJson());

class SendNotificationForLoanUtilityModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    SendNotificationForLoanUtilityModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SendNotificationForLoanUtilityModel.fromJson(Map<String, dynamic> json) => SendNotificationForLoanUtilityModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    bool? isRead;
    bool? flag;
    int? id;
    dynamic message;
    dynamic from;
    dynamic to;
    dynamic landId;
    dynamic jobId;
    int? utilityId;
    dynamic statusForFarmer;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic name;
    dynamic statusForLandlord;
    dynamic imageUrl;
    dynamic statusForFarmworker;
    dynamic statusForUtiliser;
    dynamic statusForLoanAgent;

    Result({
        this.isRead,
        this.flag,
        this.id,
        this.message,
        this.from,
        this.to,
        this.landId,
        this.jobId,
        this.utilityId,
        this.statusForFarmer,
        this.updatedAt,
        this.createdAt,
        this.name,
        this.statusForLandlord,
        this.imageUrl,
        this.statusForFarmworker,
        this.statusForUtiliser,
        this.statusForLoanAgent,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        isRead: json["is_read"],
        flag: json["flag"],
        id: json["id"],
        message: json["message"],
        from: json["from"],
        to: json["to"],
        landId: json["landId"],
        jobId: json["jobId"],
        utilityId: json["utilityId"],
        statusForFarmer: json["status_for_farmer"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        name: json["name"],
        statusForLandlord: json["status_for_landlord"],
        imageUrl: json["image_url"],
        statusForFarmworker: json["status_for_farmworker"],
        statusForUtiliser: json["status_for_utiliser"],
        statusForLoanAgent: json["status_for_loan_agent"],
    );

    Map<String, dynamic> toJson() => {
        "is_read": isRead,
        "flag": flag,
        "id": id,
        "message": message,
        "from": from,
        "to": to,
        "landId": landId,
        "jobId": jobId,
        "utilityId": utilityId,
        "status_for_farmer": statusForFarmer,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "name": name,
        "status_for_landlord": statusForLandlord,
        "image_url": imageUrl,
        "status_for_farmworker": statusForFarmworker,
        "status_for_utiliser": statusForUtiliser,
        "status_for_loan_agent": statusForLoanAgent,
    };
}
