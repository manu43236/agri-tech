import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/vendor/services/vendor_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../data/get_filtered_crop_availability_model.dart';
import '../data/get_vendor_crops_model.dart';

class VendorRepoImpl extends VendorRepo with NetworkCheckService{
  final DioClient _dioClient;
  VendorRepoImpl(this._dioClient);

  @override
  Future<Either<GetFilteredCropAvailabilityModel,Exception>>  getFilteredCrops({userId,latitude,longitude})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Vender/getFilteredCropAvailability?vendorId=$userId&latitude=$latitude&longitude=$longitude', Method.get);
        return result.fold((l){
          GetFilteredCropAvailabilityModel model= GetFilteredCropAvailabilityModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetVendorCropsModel,Exception>>  purchasedCrops()async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Vender/getVendorCrops?userId=${Get.find<HomeController>().userId.value}', Method.get);
        return result.fold((l){
          GetVendorCropsModel model= GetVendorCropsModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}