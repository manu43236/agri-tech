import 'package:agritech/app/modules/vendor_checkout_details/services/vendors_checkout_details_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../data/intiate_crop_payment_model.dart';
import '../data/update_crop_payment_model.dart';
import '../data/verify_crop_payment_model.dart';

class VendorsCheckoutDetailsRepoImpl extends VendorsCheckoutDetailsRepo with NetworkCheckService{
  final DioClient _dioClient;
  VendorsCheckoutDetailsRepoImpl(this._dioClient);

  @override
  Future<Either<UpdateCropPaymentModel,Exception>> updatePayment({id,quantity})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Vender/updateCropPayment?id=$id&quantity=$quantity', Method.get);
        return result.fold((l){
          UpdateCropPaymentModel model= UpdateCropPaymentModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<IntiateCropPaymentModel,Exception>> intiatePayment({paymentId})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Vender/initiateCropPayment?paymentId=$paymentId', Method.get);
        return result.fold((l){
          IntiateCropPaymentModel model= IntiateCropPaymentModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<VerifyCropPaymentModel,Exception>> verifyPayment({params1})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Vender/verifycropPayment', Method.post,params: params1);
        return result.fold((l){
          VerifyCropPaymentModel model= VerifyCropPaymentModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}