import 'package:agritech/app/modules/detail_purchased_crop/services/detail_purchased_crop_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../data/get_crop_availability_by_yield_payment_id_model.dart';

class DetailPurchasedCropRepoImpl extends DetailPurchasedCropRepo with NetworkCheckService{
  final DioClient _dioClient;
  DetailPurchasedCropRepoImpl(this._dioClient);

  @override
  Future<Either<GetCropAvailabilityByYieldPaymentIdModel,Exception>> detailPurchaseCrop({yieldId,id})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Vender/getCropAvailabilityByYieldIdWithPaymentId?yieldId=$yieldId&id=$id', Method.get);
        return result.fold((l){
          GetCropAvailabilityByYieldPaymentIdModel model= GetCropAvailabilityByYieldPaymentIdModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}