import 'package:get/get.dart';

import '../controllers/filtered_utilities_controller.dart';

class FilteredUtilitiesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FilteredUtilitiesController>(
      () => FilteredUtilitiesController(),
    );
  }
}
