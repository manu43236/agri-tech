import 'package:agritech/app/modules/hired_farmworkers/data/hired_farmers_farmworkers_model.dart';
import 'package:dartz/dartz.dart';

abstract class HiredFarmworkersRepo{
  Future<Either<HiredFarmersFarmworkersModel,Exception>> hired();
}