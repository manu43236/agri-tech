import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/detail_my_jobs_controller.dart';

class DetailMyJobsView extends GetView<DetailMyJobsController> {
  const DetailMyJobsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'detail_my_jobs'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(right: 20, left: 20),
        child: Obx(
          () =>controller.apiJobIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer():
           SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: colorAsh, borderRadius: BorderRadius.circular(13)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.network(
                      controller.jobByIdModel.value.result?.image
                              .toString() ??
                          defaultImageUrl,
                      height: Get.height * 0.35,
                      width: Get.width,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        // Display a local asset if the network image fails
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.35,
                          width: Get.width,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'available_job_at'.tr,
                          style: TextStyles.kTSFS20W700
                              .copyWith(color: colorDetails),
                        ),
                        Text(
                          '${controller.jobByIdModel.value.result?.village ?? ''} , ${controller.jobByIdModel.value.result?.district ?? ''} ',
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorDetails),
                        ),
                      ],
                    ),
                    // InkWell(
                    //   onTap: () {
                    //    controller.jobLandlordId.value= controller.jobByIdModel.value.result!.landLordId.toString();
                    //    print('52364678');
                    //    print(controller.jobLandlordId.value);

                    //     controller.saveJobAsFavourite();
                    //   },
                    //   child: Container(
                    //     padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    //     decoration: BoxDecoration(
                    //         borderRadius: BorderRadius.circular(11),
                    //         color: colorSave),
                    //     child: Icon(Icons.bookmark_border_outlined),
                    //   ),
                    // ),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          radius: 26,
                          backgroundColor: colorAsh,
                          child: controller.homeController.getUserModel.value
                                      .result?.imageUrl !=
                                  null
                              ? ClipOval(
                                  child: Image.network(
                                    controller.homeController.getUserModel.value
                                        .result!.imageUrl
                                        .toString(),
                                    fit: BoxFit.cover,
                                    height: Get.height * 0.1,
                                    width: Get.width * 0.2,
                                  ),
                                )
                              : const Icon(Icons.person,
                                  size: 40, color: Colors.grey),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              controller.homeController.getUserModel.value
                                        .result?.firstName??'',
                              style: TextStyles.kTSDS14W700
                                  .copyWith(color: colorPic),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Text(
                              controller.homeController.getUserModel.value
                                        .result?.address??'',
                              style: TextStyles.kTSWFS10W700
                                  .copyWith(color: colorDetails),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  controller.jobByIdModel.value.result?.description.toString()??'',
                  style: TextStyles.kTSCF12W500,
                ),
                const SizedBox(
                  height: 20,
                ),
                controller.jobByIdModel.value
                            .result!.farmworkerDetails!.isNotEmpty?
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'hired_farmworkers'.tr,
                      style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      height: Get.height * 0.07,
                      width: Get.width ,
                      child: ListView.separated(
                        itemCount: controller.jobByIdModel.value
                            .result!.farmworkerDetails!.length,
                        separatorBuilder: (context, index) => const SizedBox(
                          width: 10,
                        ),
                        itemBuilder: (context, index) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              CircleAvatar(
                                radius: 26,
                                backgroundColor: colorAsh,
                                child: controller
                                            .jobByIdModel
                                            .value
                                            .result
                                            ?.farmworkerDetails![index]
                                            .imageUrl !=
                                        null
                                    ? ClipOval(
                                        child: Image.network(
                                          controller
                                              .jobByIdModel
                                              .value
                                              .result
                                              ?.farmworkerDetails![index]
                                              .imageUrl!,
                                          fit: BoxFit.cover,
                                          height: Get.height * 0.1,
                                          width: Get.width * 0.2,
                                        ),
                                      )
                                    : const Icon(Icons.person,
                                        size: 40, color: Colors.grey),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    controller.jobByIdModel.value
                                                .result!.farmworkerDetails !=
                                            null
                                        ? controller
                                            .jobByIdModel
                                            .value
                                            .result!
                                            .farmworkerDetails![index]
                                            .firstName
                                            .toString()
                                        : '',
                                    style: TextStyles.kTSDS14W700
                                        .copyWith(color: colorPic),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    controller.jobByIdModel.value
                                                .result!.farmworkerDetails !=
                                            null
                                        ? controller
                                            .jobByIdModel
                                            .value
                                            .result!
                                            .farmworkerDetails![index]
                                            .address
                                            .toString()
                                        : '',
                                    style: TextStyles.kTSWFS10W700
                                        .copyWith(color: colorDetails),
                                  )
                                ],
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                  ],
                ):SizedBox(),
                Text(
                  'my_jobs'.tr,
                  style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                ),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: Get.height * 0.08,
                  width: Get.width,
                  child: Obx(() {
                    // Filter out the selected job from the list
                    final filteredJobs = Get.find<LandlordController>()
                        .allJobsOfLandlordModel
                        .value
                        .result!
                        .ongoingJobs!
                        .where((job) =>
                            job.id.toString() !=
                            Get.find<HomeController>().jobId.value)
                        .toList();

                    // Check if the filteredJobs list is empty
                    if (filteredJobs.isEmpty) {
                      return Center(
                        child: Text(
                          'no_jobs_available'.tr,
                          style:
                              TextStyles.kTSFS16W600.copyWith(color: colorGrey),
                        ),
                      );
                    } else {
                      // Display the job list if it's not empty
                      return ListView.separated(
                        itemCount: filteredJobs.length,
                        scrollDirection: Axis.horizontal,
                        separatorBuilder: (context, index) => const SizedBox(
                          width: 10,
                        ),
                        itemBuilder: (context, index) {
                          final job = filteredJobs[index];
                          return InkWell(
                            onTap: () async {
                              await controller.getIdJobs(id: job.id.toString());
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: colorGrey,
                                borderRadius: BorderRadius.circular(11),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(11),
                                child: Image.network(
                                  job.image.toString(),
                                  height: Get.height * 0.09,
                                  width: Get.width * 0.2,
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, error, stackTrace) {
                                    // Display a local asset if the network image fails
                                    return Image.asset(
                                      defaultImageUrl,
                                      height: Get.height * 0.09,
                                      width: Get.width * 0.2,
                                      fit: BoxFit.cover,
                                    );
                                  },
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    }
                  }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
