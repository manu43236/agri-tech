import 'package:agritech/app/modules/detail_crop/data/create_crop_payment_model.dart';
import 'package:agritech/app/modules/detail_crop/data/get_crop_availability_by_yield_id_model.dart';
import 'package:agritech/app/modules/detail_crop/data/get_crop_payment_details_model.dart';
import 'package:dartz/dartz.dart';

abstract class DetailCropRepo{
  Future<Either<GetCropAvailabilityByYieldIdModel,Exception>> cropDetails();
  Future<Either<CreateCropPaymentModel,Exception>> createCropPayment({params1});
  Future<Either<GetCropPaymentDetailsModel,Exception>> getCropPaymentDetails();
}