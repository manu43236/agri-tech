// To parse this JSON data, do
//
//     final getSavedFarmworkerModel = getSavedFarmworkerModelFromJson(jsonString);

import 'dart:convert';

GetSavedFarmworkerModel getSavedFarmworkerModelFromJson(String str) => GetSavedFarmworkerModel.fromJson(json.decode(str));

String getSavedFarmworkerModelToJson(GetSavedFarmworkerModel data) => json.encode(data.toJson());

class GetSavedFarmworkerModel {
    int? statusCode;
    String? status;
    String? message;
    List<Result>? result;

    GetSavedFarmworkerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetSavedFarmworkerModel.fromJson(Map<String, dynamic> json) => GetSavedFarmworkerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    int? userid;
    dynamic landlordid;
    dynamic farmerid;
    int? farmworkerid;
    String? role;
    dynamic landid;
    dynamic jobid;
    bool? flag;
    DateTime? createdAt;
    DateTime? updatedAt;

    Result({
        this.id,
        this.userid,
        this.landlordid,
        this.farmerid,
        this.farmworkerid,
        this.role,
        this.landid,
        this.jobid,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        userid: json["userid"],
        landlordid: json["landlordid"],
        farmerid: json["farmerid"],
        farmworkerid: json["farmworkerid"],
        role: json["role"],
        landid: json["landid"],
        jobid: json["jobid"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userid": userid,
        "landlordid": landlordid,
        "farmerid": farmerid,
        "farmworkerid": farmworkerid,
        "role": role,
        "landid": landid,
        "jobid": jobid,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
