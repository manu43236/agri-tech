import 'package:agritech/app/modules/leased_utilities_checkout_page/data/get_utility_ninty_percent_details_model.dart';
import 'package:dartz/dartz.dart';

abstract class LeasedUtilitiesCheckoutRepo{
  Future<Either<GetUtilityNintyPercentDetailsModel,Exception>> getNintyDetails({id});
}