import 'package:agritech/app/modules/available_my_jobs/controllers/available_my_jobs_controller.dart';
import 'package:agritech/app/modules/available_my_jobs/services/available_my_jobs_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../../home/controllers/home_controller.dart';
import '../../home/data/all_jobs_of_landlord_model.dart';
import '../data/jobs_filter_model.dart';

class AvailableMyJobsRepoImpl extends AvailableMyJobsRepo with NetworkCheckService{
  final DioClient _dioClient;
  AvailableMyJobsRepoImpl(this._dioClient);

  @override
  Future<Either<AllJobsOfLandlordModel,Exception>> getJobSearch()async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Jobs/getAllJobsByLandLordId?landLordId=${Get.find<HomeController>().userId.value}&search=${Get.find<AvailableMyJobsController>().myJobsSearch.value}', Method.get);
        return result.fold((l) {
          AllJobsOfLandlordModel model = AllJobsOfLandlordModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<JobsFilterModel,Exception>> filterOwnJobs({landlordId,farmerId,search,state,district,mandal,village})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Jobs/recent5,FilterOwnAndNearJobs?landlordid=$landlordId&farmerid=$farmerId&state=$state&district=$district&mandal=$mandal&village=$village&search=$search&latitude=''&longitude=''', Method.get);
        return result.fold((l) {
          JobsFilterModel model = JobsFilterModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}