import 'package:agritech/app/modules/select_region/data/mandal_model.dart';
import 'package:dartz/dartz.dart';

abstract class MandalRepo{
  Future<Either<MandalModel,Exception>> getMandal();
}