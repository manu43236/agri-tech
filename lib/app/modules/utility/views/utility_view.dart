import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../controllers/utility_controller.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import 'widgets/recent_utilities_widget.dart';

class UtilityView extends GetView<UtilityController> {
  const UtilityView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async {
          Get.find<UtilityController>().mainUtilSearch.value = '';
          Get.find<UtilityController>().userUtilities(userId:controller.id.value , search: '',state: '',district: '',mandal: '',village: '');
          Get.find<UtilityController>().recentUtilities(search: controller.mainUtilSearch.value);
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Column(
              children: [
                // Recent Utilities Section
                RecentUtilitiesWidget(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'instruments'.tr,
                      style: TextStyles.kTSDS12W700.copyWith(color: colorHello),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                // List of Utilities
                Obx(() {
                  final utilities = controller.getUtilityByUserIdModel.value.result;
                
                  // Debugging: Print utilities content
                  print("Utilities List: $utilities");
                
                  if (utilities == null || utilities.isEmpty) {
                    return Center(
                      child: Text(
                        'no_instruments_available'.tr,
                        style: TextStyles.kTSDS12W700.copyWith(color: colorHello),
                      ),
                    );
                  }
                
                  return SizedBox(
                   height: Get.height * 0.14,
                    child: controller.apiUserUtilityStatus.value == ApiStatus.LOADING
                  ? Shimmers().getShimmerItem(): ListView.separated(
                      itemCount: utilities.length,
                      scrollDirection: Axis.horizontal,
                      separatorBuilder: (context, index) => const SizedBox(width: 10),
                      itemBuilder: (context, index) {
                        final utility = utilities[index];
                        final imageUrl = utility.image ?? defaultImageUrl;
                                  
                        // Debugging: Print utility data
                        print("Utility at index $index: $utility");
                                  
                        return InkWell(
                          onTap: () {
                            // Set the utility ID and navigate to details page
                            //controller.utilityId.value = utility.id.toString();
                            Get.toNamed(Routes.MY_INSTRUMENTS_DETAILS, arguments:{"instrumentId":utility.id,"isLoan":false,"isInsurance":false} );
                          },
                          child: Column(
                            children: [
                              // Display the instrument image
                              ClipRRect(
                                borderRadius:  BorderRadius.circular(11),
                                child: Image.network(
                                  imageUrl,
                                  height: Get.height * 0.14,
                                  width: Get.width*0.42,
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, error, stackTrace) {
                                    return Image.asset(
                                      defaultImageUrl,
                                      height: Get.height * 0.14,
                                      width: Get.width*0.42 ,
                                      fit: BoxFit.cover,
                                    );
                                  },
                                ),
                              ), 
                            ],
                          ),
                        );
                      },
                    ),
                  );
                }),
                const SizedBox(height: 5),
              Align(
                alignment: Alignment.topRight,
                child: InkWell(
                  onTap: () async {
                    Get.toNamed(Routes.AVAILABLE_MY_UTILITIES);
                  },
                  child: Text(
                    'view_all'.tr,
                    style: TextStyles.kTSCF12W500.copyWith(
                      fontSize: 10,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
