import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/utility/controllers/utility_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../utility/data/get_recent_four_utilities_model.dart';
import '../../utility/services/utility_repo_impl.dart';

class AvailableRecentUtilitiesController extends BaseController {
  final UtilityController utilityController = Get.find<UtilityController>();
  Rx<GetRecentFourUtilitiesModel> getAvailableRecentFourUtilitiesModel =
      Rx(GetRecentFourUtilitiesModel());
  Rx<GetRecentFourUtilitiesModel> getAvailableFilterRecentFourUtilitiesModel =
      Rx(GetRecentFourUtilitiesModel());

  var apiFilterRecentUtilitiesStatus = ApiStatus.LOADING.obs;
  var dataSelectedItem = ''.obs;
  var availableRecentUtilSearch = ''.obs;
  var isLoan=false.obs;
  var isInsurance=false.obs;

  @override
  void onInit() async {
    super.onInit();
    //await availableRecentUtilities();
    await availableFilterRecentUtilities(
        search: '', state: '', district: '', mandal: '', village: '');
  }

  @override
  void onReady() {
    super.onReady();
  }

  //recent four utilities API
  Future<void> availableRecentUtilities() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await UtilityRepoImpl(dioClient).recentUtilities();
      print('=============== get recent utilities =========');
      print(result);
      result.fold((model) {
        getAvailableRecentFourUtilitiesModel.value = model;
        print(
            getAvailableRecentFourUtilitiesModel.value.result!.first.brandName);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> availableFilterRecentUtilities(
      {search, state, district, mandal, village}) async {
    apiFilterRecentUtilitiesStatus.value = ApiStatus.LOADING;
    try {
      var result = await UtilityRepoImpl(dioClient).filterUtilities(params1: {
        "village": village,
        "mandal": mandal,
        "district": district,
        "state": state,
        "latitude": Get.find<HomeController>().latitude.value,
        "longitude": Get.find<HomeController>().longitude.value,
        "search": search
      });
      print('=============== available filter recent utilities =========');
      print(result);
      result.fold((model) {
        if (model.statusCode == 200) {
          getAvailableFilterRecentFourUtilitiesModel.value = model;
          print(getAvailableFilterRecentFourUtilitiesModel
              .value.result!.first.brandName);
        } else if (model.statusCode == 400) {
          print('===============status=============');
          getAvailableFilterRecentFourUtilitiesModel.value.result!.clear();
          getAvailableFilterRecentFourUtilitiesModel.value.result! == [];
        }
        apiFilterRecentUtilitiesStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiFilterRecentUtilitiesStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
