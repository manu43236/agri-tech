// import 'package:agritech/app/modules/utility/data/create_utility_model.dart';
import 'package:agritech/app/modules/Landlord_utilities/data/filter_utilities_by_location_model.dart';
import 'package:agritech/models/create_utility_payment_model.dart';
import 'package:agritech/models/get_utility_payment_by_id_model.dart';
import 'package:dartz/dartz.dart';

import '../../../../models/create_utility_model.dart';
import '../../../../models/get_recent_four_utilities_model.dart';
import '../../../../models/get_utility_by_id_model.dart';
import '../../../../models/get_utility_by_location_model.dart';


abstract class LandlordUtilityRepo {
  Future<Either<GetRecentFourUtilitiesModel, Exception>> recentUtilities();
  Future<Either<CreateUtilityModel, Exception>> addInstrument({params1});
  Future<Either<GetUtilityByIdModel, Exception>> getUtilityById({id});
  Future<Either<GetUtilityByLocationModel, Exception>> getUtilitesByLocation();
  Future<Either<GetRecentFourUtilitiesModel, Exception>> filterUtilities({params1});
  Future<Either<CreateUtilityPaymentModel, Exception>> utilityPayment({params1});
   Future<Either<GetUtilityPaymentByIdModel, Exception>> getUtilityPayment();
   Future<Either<GetUtilityPaymentByIdModel, Exception>> getUtilityPaymentById({id});
   Future<Either<FilterUtilitiesByLocationModel,Exception>> filterUtilityLocation({params1});
}
