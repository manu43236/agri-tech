import 'package:agritech/app/modules/home/services/save_land_as_favourite_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../../../../models/save_land_as_favourite_model.dart';

class SaveLandAsFavouriteRepoImpl extends SaveLandAsFavouriteRepo with NetworkCheckService{
  final DioClient _dioClient;
  SaveLandAsFavouriteRepoImpl(this._dioClient);
  @override
  Future<Either<SaveLandAsFavouriteModel,Exception>> saveLand({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result=await _dioClient.requestForAuth('save/saveLandAsFavorite', Method.post,params: params1);
        return result.fold((l){
          SaveLandAsFavouriteModel model= SaveLandAsFavouriteModel.fromJson(l.data);
          return Left(model);
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}