// To parse this JSON data, do
//
//     final allSavedLandsModel = allSavedLandsModelFromJson(jsonString);

import 'dart:convert';

AllSavedLandsModel allSavedLandsModelFromJson(String str) => AllSavedLandsModel.fromJson(json.decode(str));

String allSavedLandsModelToJson(AllSavedLandsModel data) => json.encode(data.toJson());

class AllSavedLandsModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    AllSavedLandsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory AllSavedLandsModel.fromJson(Map<String, dynamic> json) => AllSavedLandsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<SavedLand>? savedLands;
    int? landCount;

    Result({
        this.savedLands,
        this.landCount,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        savedLands: json["savedLands"] == null ? [] : List<SavedLand>.from(json["savedLands"]!.map((x) => SavedLand.fromJson(x))),
        landCount: json["landCount"],
    );

    Map<String, dynamic> toJson() => {
        "savedLands": savedLands == null ? [] : List<dynamic>.from(savedLands!.map((x) => x.toJson())),
        "landCount": landCount,
    };
}

class SavedLand {
    int? id;
    int? landid;
    dynamic fullName;
    dynamic landInAcres;
    dynamic surveyNum;
    dynamic water;
    dynamic village;
    dynamic imageUrl;
    bool? flag;

    SavedLand({
        this.id,
        this.landid,
        this.fullName,
        this.landInAcres,
        this.surveyNum,
        this.water,
        this.village,
        this.imageUrl,
        this.flag,
    });

    factory SavedLand.fromJson(Map<String, dynamic> json) => SavedLand(
        id: json["id"],
        landid: json["landid"],
        fullName: json["fullName"],
        landInAcres: json["landInAcres"],
        surveyNum: json["surveyNum"],
        water: json["water"],
        village: json["village"],
        imageUrl: json["image_url"],
        flag: json["flag"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landid": landid,
        "fullName": fullName,
        "landInAcres": landInAcres,
        "surveyNum": surveyNum,
        "water": water,
        "village": village,
        "image_url": imageUrl,
        "flag": flag,
    };
}
