import 'package:get/get.dart';

import '../../details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import '../../instrument_details/controllers/instrument_details_controller.dart';
import '../../saved_utilities/controllers/saved_utilities_controller.dart';
import '../controllers/my_instruments_details_controller.dart';

class MyInstrumentsDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MyInstrumentsDetailsController>(
      () => MyInstrumentsDetailsController(),
    );
     Get.lazyPut<InstrumentDetailsController>(
      ()=>InstrumentDetailsController()
    );
    Get.lazyPut<DetailsOfLandlordUtilitiesController>(
      ()=>DetailsOfLandlordUtilitiesController()
    );
    Get.lazyPut<SavedUtilitiesController>(
      ()=>SavedUtilitiesController()
    );
  }
}
