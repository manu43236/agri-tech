import 'package:agritech/models/get_otp_model.dart';
import 'package:dartz/dartz.dart';

abstract class GetOtpRepo {
  Future<Either<GetOtpModel, Exception>> getOtp({params1});
}
