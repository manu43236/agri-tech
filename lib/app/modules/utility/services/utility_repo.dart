import 'package:agritech/app/modules/utility/data/create_utility_model.dart';
import 'package:agritech/app/modules/utility/data/get_utility_by_id_model.dart';
import 'package:agritech/app/modules/utility/data/get_utility_by_user_id_model.dart';
import 'package:dartz/dartz.dart';

import '../data/get_recent_four_utilities_model.dart';

abstract class UtilityRepo{
  Future<Either<GetRecentFourUtilitiesModel,Exception>>  recentUtilities();
  Future<Either<GetUtilityByUserIdModel,Exception>> getUtilities();
  Future<Either<CreateUtilityModel,Exception>> addInstrument({params1});
  Future<Either<GetUtilityByIdModel,Exception>> getUtilityById({id});
  Future<Either<GetRecentFourUtilitiesModel,Exception>> getUtilityByUserId();
  Future<Either<GetRecentFourUtilitiesModel,Exception>> filterUtilities({params1});
}