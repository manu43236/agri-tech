import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../controllers/hired_farmers_details_controller.dart';

class HiredFarmersDetailsView extends GetView<HiredFarmersDetailsController> {
  const HiredFarmersDetailsView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar:AppBar(
        title: Text(
          'details'.tr,
           style: TextStyles.kTSFS24W600.copyWith(color: colorDetails,fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: ()async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20,),
        child: Obx(
          () =>controller.apiUserByProfileIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): 
           SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(11),
                child: Image.network(
                  controller
                      .getUserProfileModel
                      .value
                      .result!
                      .imageUrl
                      .toString(),
                  fit: BoxFit.cover,
                  height: Get.height * 0.35,
                  width: Get.width,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            'nam'.tr,
                            style: TextStyles.kTSDS12W700
                                .copyWith(color: colorHello),
                          ),
                          Text(
                            controller
                                    .getUserProfileModel
                                    .value
                                    .result
                                    ?.firstName ??
                                'N/A',
                            style: TextStyles.kTSDS14W700
                                .copyWith(color: colorPic),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Text(
                            'loc'.tr,
                            style: TextStyles.kTSDS12W700
                                .copyWith(color: colorHello),
                          ),
                          Text(
                            controller
                                    .getUserProfileModel
                                    .value
                                    .result
                                    ?.address ??
                                'no_address_provided'.tr,
                            style: TextStyles.kTSDS14W700
                                .copyWith(color: colorPic),
                          ),
                        ],
                      ),
                      const SizedBox(height: 20,),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(11),
                          color: primary
                        ),
                        child: Text('Land at Hyd',style: TextStyles.kTSFS16W700.copyWith(color:colorWhite),),
                      ),
                      const SizedBox(height: 20,),
                      Text(
                        'description_about_him'.tr,
                        style: TextStyles.kTSDS12W700
                            .copyWith(color: colorDetails),
                      ),
                      const SizedBox(height: 10),
                      Text(
                        controller
                                .getUserProfileModel
                                .value
                                .result
                                ?.description ??
                            '',
                        style: TextStyles.kTSCF12W500,
                      ),
                    ],
                  ),
                ],
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
