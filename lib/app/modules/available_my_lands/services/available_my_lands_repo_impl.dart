import 'package:agritech/app/modules/available_my_lands/controllers/available_my_lands_controller.dart';
import 'package:agritech/app/modules/available_my_lands/services/available_my_lands_repo.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../../home/data/all_lands_of_landlord.dart';

class AvailableMyLandsRepoImpl extends AvailableMyLandsRepo with NetworkCheckService{
  final DioClient _dioClient;
  AvailableMyLandsRepoImpl(this._dioClient);

    @override
  Future<Either<AddLandsOfLandlordModel,Exception>> getLandlordLands()async{
    var data=await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result=await _dioClient.requestForAuth('Land/getAllLandsByLandLord?landLordId=${Get.find<HomeController>().userId.value}&search=${Get.find<AvailableMyLandsController>().myCustomerSearchText.value}', Method.get);
        print('result repo:$result');
         return result.fold((l) {
          AddLandsOfLandlordModel model = AddLandsOfLandlordModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

}