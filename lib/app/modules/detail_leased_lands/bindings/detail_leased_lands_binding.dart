import 'package:get/get.dart';

import '../../saved_lands/controllers/saved_lands_controller.dart';
import '../controllers/detail_leased_lands_controller.dart';

class DetailLeasedLandsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailLeasedLandsController>(
      () => DetailLeasedLandsController(),
    );
    Get.lazyPut<SavedLandsController>(
      () => SavedLandsController(),
    );
  }
}
