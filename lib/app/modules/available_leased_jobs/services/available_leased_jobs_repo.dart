import 'package:dartz/dartz.dart';

import '../../home/data/get_leased_jobs_details_model.dart';

abstract class AvailableLeasedJobsRepo{
  Future<Either<GetLeasedJobDetailsModel,Exception>>  leasedSearchJobs();
}