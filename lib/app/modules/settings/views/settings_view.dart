import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/settings_controller.dart';

class SettingsView extends GetView<SettingsController> {
  const SettingsView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text('settings'.tr,style:TextStyles.kTSFS24W600.copyWith(color: colorBlack) ,),
        centerTitle: true,
        leading: IconButton(
          onPressed: (){
            Get.back();
        }, 
        icon: Icon(Icons.arrow_back,color: colorBlack,)
        ),
      ),
      body: const Center(
        child: Text(
          'coming soon....',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
