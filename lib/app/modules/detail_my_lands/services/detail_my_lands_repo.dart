import 'package:agritech/app/modules/detail_my_lands/data/send_notification_for_loan_model.dart';
import 'package:dartz/dartz.dart';

abstract class DetailMyLandsRepo{
  Future<Either<SendNotificationForLoanModel,Exception>> sendLoanNotification({params1});
}