// To parse this JSON data, do
//
//     final acceptRequestByFarmerOrLandlordModel = acceptRequestByFarmerOrLandlordModelFromJson(jsonString);

import 'dart:convert';

AcceptRequestByFarmerOrLandlordModel acceptRequestByFarmerOrLandlordModelFromJson(String str) => AcceptRequestByFarmerOrLandlordModel.fromJson(json.decode(str));

String acceptRequestByFarmerOrLandlordModelToJson(AcceptRequestByFarmerOrLandlordModel data) => json.encode(data.toJson());

class AcceptRequestByFarmerOrLandlordModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    AcceptRequestByFarmerOrLandlordModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory AcceptRequestByFarmerOrLandlordModel.fromJson(Map<String, dynamic> json) => AcceptRequestByFarmerOrLandlordModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    Notification? notification;
    Lease? lease;
    dynamic farmworkerName;

    Result({
        this.notification,
        this.lease,
        this.farmworkerName,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        notification: json["notification"] == null ? null : Notification.fromJson(json["notification"]),
        lease: json["lease"] == null ? null : Lease.fromJson(json["lease"]),
        farmworkerName: json["farmworkerName"],
    );

    Map<String, dynamic> toJson() => {
        "notification": notification?.toJson(),
        "lease": lease?.toJson(),
        "farmworkerName": farmworkerName,
    };
}

class Lease {
    int? id;
    int? jobId;
    int? landlord;
    dynamic farmer;
    int? farmWorker;
    dynamic status;
    dynamic startingDate;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic utiliser;
    dynamic utilityId;
    dynamic landId;
    dynamic endingDate;

    Lease({
        this.id,
        this.jobId,
        this.landlord,
        this.farmer,
        this.farmWorker,
        this.status,
        this.startingDate,
        this.updatedAt,
        this.createdAt,
        this.utiliser,
        this.utilityId,
        this.landId,
        this.endingDate,
    });

    factory Lease.fromJson(Map<String, dynamic> json) => Lease(
        id: json["id"],
        jobId: json["jobId"],
        landlord: json["landlord"],
        farmer: json["farmer"],
        farmWorker: json["farmWorker"],
        status: json["status"],
        startingDate: json["starting_date"] == null ? null : DateTime.parse(json["starting_date"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        utiliser: json["utiliser"],
        utilityId: json["utilityId"],
        landId: json["landId"],
        endingDate: json["ending_date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "jobId": jobId,
        "landlord": landlord,
        "farmer": farmer,
        "farmWorker": farmWorker,
        "status": status,
        "starting_date": startingDate?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "utiliser": utiliser,
        "utilityId": utilityId,
        "landId": landId,
        "ending_date": endingDate,
    };
}

class Notification {
    bool? isRead;
    int? id;
    dynamic name;
    dynamic message;
    dynamic from;
    dynamic to;
    int? jobId;
    dynamic statusForFarmworker;
    dynamic imageUrl;
    bool? flag;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic statusForLandlord;
    dynamic statusForFarmer;
    dynamic landId;

    Notification({
        this.isRead,
        this.id,
        this.name,
        this.message,
        this.from,
        this.to,
        this.jobId,
        this.statusForFarmworker,
        this.imageUrl,
        this.flag,
        this.updatedAt,
        this.createdAt,
        this.statusForLandlord,
        this.statusForFarmer,
        this.landId,
    });

    factory Notification.fromJson(Map<String, dynamic> json) => Notification(
        isRead: json["is_read"],
        id: json["id"],
        name: json["name"],
        message: json["message"],
        from: json["from"],
        to: json["to"],
        jobId: json["jobId"],
        statusForFarmworker: json["status_for_farmworker"],
        imageUrl: json["image_url"],
        flag: json["flag"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        statusForLandlord: json["status_for_landlord"],
        statusForFarmer: json["status_for_farmer"],
        landId: json["landId"],
    );

    Map<String, dynamic> toJson() => {
        "is_read": isRead,
        "id": id,
        "name": name,
        "message": message,
        "from": from,
        "to": to,
        "jobId": jobId,
        "status_for_farmworker": statusForFarmworker,
        "image_url": imageUrl,
        "flag": flag,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "status_for_landlord": statusForLandlord,
        "status_for_farmer": statusForFarmer,
        "landId": landId,
    };
}
