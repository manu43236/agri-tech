import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../localisation/languages.dart';

class TranslatorIconController extends BaseController {

  var selectedLocale = Locale('en', 'US').obs;
  var selectedLang=''.obs;
  final List<Map<String, dynamic>> locales = [
    {'name': 'ENGLISH', 'locale': Locale('en', 'US')},
    {'name': 'తెలుగు', 'locale': Locale('te', 'IN')},
  ];

  @override
  void onInit() {
    super.onInit();
  }


  void updateLanguage(Locale locale) async{
    selectedLocale.value = locale;
    print('selected language: ${locale.languageCode}');
    //selectedLang.value=locale.languageCode.toString();
    await SecureStorage().writeStringData("lang",locale.languageCode.toString(),);
    Get.updateLocale(locale); 
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
