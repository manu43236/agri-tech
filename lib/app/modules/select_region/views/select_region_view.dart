import 'package:agritech/app/modules/available_farmworker_jobs/controllers/available_farmworker_jobs_controller.dart';
import 'package:agritech/app/modules/available_recent_lands/controllers/available_recent_lands_controller.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/select_region/controllers/select_region_controller.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../available_farmer_my_jobs/controllers/available_farmer_my_jobs_controller.dart';
import '../../available_farmer_my_lands/controllers/available_farmer_my_lands_controller.dart';
import '../../available_my_jobs/controllers/available_my_jobs_controller.dart';
import '../../available_my_lands/controllers/available_my_lands_controller.dart';
import '../../available_my_utilities/controllers/available_my_utilities_controller.dart';
import '../../available_recent_utilities/controllers/available_recent_utilities_controller.dart';

class SelectRegionView extends GetView<SelectRegionController> {
  String? type;
  SelectRegionView({super.key, this.type});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            padding: const EdgeInsets.only(top: 35),
            onPressed: () {
              Get.back();
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.black,
            )),
      ),
      body: Obx(
        () => controller.apiStateStatus.value == ApiStatus.LOADING
            ? const Center(child: CircularProgressIndicator())
            : Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 15),
                          child: Text(
                            'select_your_region'.tr,
                            style: TextStyles.kTSDS12W500.copyWith(
                                color: colorHello, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 15),
                          child: InkWell(
                              onTap: () {
                              controller.clearAll();
                              },
                              child: Text(
                                'clear_all'.tr,
                                style: TextStyles.kTSDS12W500.copyWith(
                                    color: colorHello,
                                    fontWeight: FontWeight.bold),
                              )),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    const Divider(
                      thickness: 1,
                      color: colorHello,
                      height: 0,
                    ),
                    Expanded(
                      child: Row(
                        children: [
                          // Left Section
                          Expanded(
                            flex: 1,
                            child: Container(
                              color: Colors.grey[200],
                              child: ListView.builder(
                                  itemCount: controller.items.length,
                                  itemBuilder: (context, index) {
                                    return _buildLeftItem(
                                        controller.items[index]);
                                  }),
                            ),
                          ),
                          // Right Section
                          Expanded(
                            flex: 2,
                            child: ListView(
                              padding: const EdgeInsets.all(8.0),
                              children: controller.selectedCategory.value
                                          .toLowerCase() ==
                                      "sta".tr
                                  ? controller.stateResult.value.map((item) {
                                      return _buildRightItem(item);
                                    }).toList()
                                  : controller.selectedCategory.value
                                              .toLowerCase() ==
                                          "dis".tr
                                      ? controller.districtResult.value
                                          .map((item) {
                                          return _buildRightItem(item);
                                        }).toList()
                                      : controller.selectedCategory.value
                                                  .toLowerCase() ==
                                              "man".tr
                                          ? controller.mandalResult.value
                                              .map((item) {
                                              return _buildRightItem(item);
                                            }).toList()
                                          : controller.selectedCategory.value
                                                      .toLowerCase() ==
                                                  "vill".tr
                                              ? controller.villageResult.value
                                                  .map((item) {
                                                  return _buildRightItem(item);
                                                }).toList()
                                              : [const SizedBox()],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Divider(
                      thickness: 1,
                      color: colorHello,
                      height: 0,
                    ),
                    // Bottom Buttons
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              controller.clearAll();
                              Get.back();
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              decoration: const BoxDecoration(
                                color: colorWhite,
                              ),
                              child: Text(
                                'close'.tr,
                                textAlign: TextAlign.center,
                                style: TextStyles.kTSDS12W500.copyWith(
                                    color: colorHello,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              print('ontap');
                              if (type == 'recent_Lands'.tr) {
                                //controller.filterLands();
                                print('ontap');
                                Get.find<AvailableRecentLandsController>().recentFilterLandsList(
                                  landlordId: '',farmerId: '',search: '',
                                  state: controller.selectedStates.value,
                                  district: controller.selectedDistricts.value,
                                  mandal: controller.selectedMandal.value,
                                  village: controller.selectedVillage.value,
                                );
                                Get.find<AvailableRecentLandsController>().dataSelectedItem.value=controller.getSelectedRegionName();
                                Get.back();
                              }else if (type == 'my_Lands'.tr) {
                                //controller.filterLands();
                                Get.find<AvailableMyLandsController>().myLandsFilterLandsList(
                                  landlordId: Get.find<HomeController>().userId.value,farmerId: '',search: '',
                                  state: controller.selectedStates.value,
                                  district: controller.selectedDistricts.value,
                                  mandal: controller.selectedMandal.value,
                                  village: controller.selectedVillage.value
                                );
                                Get.find<AvailableMyLandsController>().dataSelectedItem.value=controller.getSelectedRegionName();
                                Get.back();
                              }else if (type == 'farmer_My_Lands'.tr) {
                                //controller.filterLands();
                                Get.find<AvailableFarmerMyLandsController>().farmerLandsFilterLandsList(
                                  farmerId: Get.find<HomeController>().userId.value,landlordId: '',search: '',
                                  state: controller.selectedStates.value,
                                  district: controller.selectedDistricts.value,
                                  mandal: controller.selectedMandal.value,
                                  village: controller.selectedVillage.value
                                );
                                Get.find<AvailableFarmerMyLandsController>().dataSelectedItem.value=controller.getSelectedRegionName();
                                Get.back();
                              }else if (type == 'my_Jobs'.tr) {
                                //controller.filterLands();
                                Get.find<AvailableMyJobsController>().myJobsFilterJobsList(
                                  landlordId: Get.find<HomeController>().userId.value,farmerId: '',search: '',
                                  state: controller.selectedStates.value,
                                  district: controller.selectedDistricts.value,
                                  mandal: controller.selectedMandal.value,
                                  village: controller.selectedVillage.value
                                );
                                Get.find<AvailableMyJobsController>().dataSelectedItem.value=controller.getSelectedRegionName();
                                Get.back();
                              }else if (type == 'farmer_My_Jobs'.tr) {
                                //controller.filterLands();
                                Get.find<AvailableFarmerMyJobsController>().farmerMyJobsFilterJobsList(
                                  farmerId: Get.find<HomeController>().userId.value,landlordId: '',search: '',
                                  state: controller.selectedStates.value,
                                  district: controller.selectedDistricts.value,
                                  mandal: controller.selectedMandal.value,
                                  village: controller.selectedVillage.value
                                );
                                Get.find<AvailableFarmerMyJobsController>().dataSelectedItem.value=controller.getSelectedRegionName();
                                Get.back();
                              }
                              else if (type == 'worker_jobs'.tr) {
                                //controller.filterLands();
                                Get.find<AvailableFarmworkerJobsController>().filterJobsSearch(
                                  search: '',
                                  state: controller.selectedStates.value,
                                  district: controller.selectedDistricts.value,
                                  mandal: controller.selectedMandal.value,
                                  village: controller.selectedVillage.value
                                );
                                Get.find<AvailableFarmworkerJobsController>().dataSelectedItem.value=controller.getSelectedRegionName();
                                Get.back();
                              }
                              else if (type == 'myUtilities'.tr) {
                                //controller.filterLands();
                                Get.find<AvailableMyUtilitiesController>().availableUserUtilities(
                                  search: '',
                                  state: controller.selectedStates.value,
                                  district: controller.selectedDistricts.value,
                                  mandal: controller.selectedMandal.value,
                                  village: controller.selectedVillage.value
                                );
                                Get.find<AvailableMyUtilitiesController>().dataSelectedItem.value=controller.getSelectedRegionName();
                                Get.back();
                              }
                              else if (type == 'recent_utilities'.tr) {
                                Get.find<AvailableRecentUtilitiesController>().availableFilterRecentUtilities(
                                  search: '',
                                  state: controller.selectedStates.value,
                                  district: controller.selectedDistricts.value,
                                  mandal: controller.selectedMandal.value,
                                  village: controller.selectedVillage.value
                                );
                                Get.find<AvailableRecentUtilitiesController>().dataSelectedItem.value=controller.getSelectedRegionName();
                                Get.back();
                              }
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              decoration: BoxDecoration(
                                color: colorGrey.shade200,
                              ),
                              child: Text(
                                'Apply'.tr,
                                textAlign: TextAlign.center,
                                style: TextStyles.kTSDS12W500.copyWith(
                                    color: colorHello,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  Widget _buildLeftItem(String title) {
    return Obx(
      () => Container(
        color: controller.selectedCategory.value.toLowerCase() == title.toLowerCase()
            ? colorWhite
            : Colors.transparent,
        child: ListTile(
          title: Text(
            title,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 14,
              color: colorBlack,
            ),
          ),
          onTap: () {
            controller.selectedCategory.value = title;
          },
        ),
      ),
    );
  }

  Widget _buildRightItem(dynamic item) {
    return Obx(
      () => Container(
        color: controller.selectedItem.value == item.id.toString()
            ? Colors.blue[100]
            : Colors.transparent,
        child: ListTile(
          title: Text(
            item.name,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: controller.selectedItem.value == item.id.toString()
                  ? Colors.blue
                  : Colors.black,
            ),
          ),
          onTap: () {
            controller.onItemSelected(controller.selectedCategory.value, item);
          },
        ),
      ),
    );
  }
}
