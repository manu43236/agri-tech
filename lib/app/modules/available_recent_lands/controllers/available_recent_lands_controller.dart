import 'package:agritech/app/modules/available_recent_lands/services/available_recent_lands_repo_impl.dart';
import 'package:agritech/app/modules/select_region/controllers/select_region_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../home/data/recent_lands_model.dart';
import '../../home/services/home_repo_impl.dart';
import '../data/recent_filter_model.dart';


class AvailableRecentLandsController extends BaseController {

  var name = "".obs;
  var stateNam=''.obs;
  var dataSelectedItem = "".obs;
  Rx<RecentLandsModel> recentLandsListModel = Rx(RecentLandsModel());
  Rx<RecentFilterModel> recentFilterModel = Rx(RecentFilterModel());

  // @override
  // void onInit() {
  //   super.onInit();
  //   print('================================ on in it ==========================');
  //   recentLandsList();
  //   if (Get.arguments != null) {
  //      print("Arguments Received: ${Get.arguments}");
  //      print(Get.arguments['state'] ?? "");
  //      print(Get.arguments['district'] ?? "");
  //      print(Get.arguments['mandal'] ?? "");
  //      print(Get.arguments['village'] ?? "");
  //      //name.value=Get.arguments['heading'];
  //      name.value = Get.arguments?['heading'] ?? "";
  //      print(name.value);
  //   }
    
  // }
  @override
void onInit() {
  super.onInit();
  print('================================ on init ==========================');
   // recentLandsList();
  recentFilterLandsList(landlordId: '',farmerId: '',search: '', state: '',district: '',mandal: '',village: '',);
}



  // recent lands for top
   recentLandsList() async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result = await HomeRepoImpl(dioClient).doRecentLands();
      print('=============== get all recent lands list =========');
      print(result);
      result.fold((left) {
        recentLandshandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void recentLandshandleResponse(RecentLandsModel model) async {
    if (model.statusCode == 200) {
      recentLandsListModel.value = model;
    }
  }

   // recent lands for top
   recentFilterLandsList({landlordId,farmerId,search,state,district,mandal,village}) async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result = await AvailableRecentLandsRepoImpl(dioClient).filterRecentLocations(landlordId: landlordId,farmerId: farmerId,search: search, state: state,district: district,mandal: mandal,village: village);
      print('=============== get all filter recent lands list =========');
      print(result);
      result.fold((left) {
        recentFilterLandshandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void recentFilterLandshandleResponse(RecentFilterModel model) async {
    if (model.statusCode == 200) {
      recentFilterModel.value = model;
    }else if(model.statusCode==400){
      recentFilterModel.value.result?.clear();
      recentFilterModel.value.result! ==[];
    }
  }



  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  
}
