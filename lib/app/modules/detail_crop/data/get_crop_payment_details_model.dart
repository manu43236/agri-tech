// To parse this JSON data, do
//
//     final getCropPaymentDetailsModel = getCropPaymentDetailsModelFromJson(jsonString);

import 'dart:convert';

GetCropPaymentDetailsModel getCropPaymentDetailsModelFromJson(String str) => GetCropPaymentDetailsModel.fromJson(json.decode(str));

String getCropPaymentDetailsModelToJson(GetCropPaymentDetailsModel data) => json.encode(data.toJson());

class GetCropPaymentDetailsModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetCropPaymentDetailsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetCropPaymentDetailsModel.fromJson(Map<String, dynamic> json) => GetCropPaymentDetailsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    CropDetails? cropDetails;
    OwnerDetails? ownerDetails;
    LandDetails? landDetails;
    Map<String, int>? paymentDetails;

    Result({
        this.cropDetails,
        this.ownerDetails,
        this.landDetails,
        this.paymentDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        cropDetails: json["cropDetails"] == null ? null : CropDetails.fromJson(json["cropDetails"]),
        ownerDetails: json["ownerDetails"] == null ? null : OwnerDetails.fromJson(json["ownerDetails"]),
        landDetails: json["landDetails"] == null ? null : LandDetails.fromJson(json["landDetails"]),
        paymentDetails: Map.from(json["paymentDetails"]!).map((k, v) => MapEntry<String, int>(k, v)),
    );

    Map<String, dynamic> toJson() => {
        "cropDetails": cropDetails?.toJson(),
        "ownerDetails": ownerDetails?.toJson(),
        "landDetails": landDetails?.toJson(),
        "paymentDetails": Map.from(paymentDetails!).map((k, v) => MapEntry<String, dynamic>(k, v)),
    };
}

class CropDetails {
    dynamic name;
    int? costPerHector;
    dynamic imageUrl;

    CropDetails({
        this.name,
        this.costPerHector,
        this.imageUrl,
    });

    factory CropDetails.fromJson(Map<String, dynamic> json) => CropDetails(
        name: json["name"],
        costPerHector: json["costPerHector"],
        imageUrl: json["image_url"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "costPerHector": costPerHector,
        "image_url": imageUrl,
    };
}

class LandDetails {
    int? id;
    int? landLordId;
    int? farmerId;
    dynamic fullName;
    dynamic landInAcres;
    dynamic surveyNum;
    dynamic water;
    dynamic village;
    dynamic description;
    dynamic mandal;
    dynamic district;
    dynamic state;
    double? latitude;
    double? longitude;
    dynamic radius;
    int? imageId;
    dynamic imageUrl;
    dynamic status;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;

    LandDetails({
        this.id,
        this.landLordId,
        this.farmerId,
        this.fullName,
        this.landInAcres,
        this.surveyNum,
        this.water,
        this.village,
        this.description,
        this.mandal,
        this.district,
        this.state,
        this.latitude,
        this.longitude,
        this.radius,
        this.imageId,
        this.imageUrl,
        this.status,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory LandDetails.fromJson(Map<String, dynamic> json) => LandDetails(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        landInAcres: json["landInAcres"],
        surveyNum: json["surveyNum"],
        water: json["water"],
        village: json["village"],
        description: json["description"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        radius: json["radius"],
        imageId: json["image_id"],
        imageUrl: json["image_url"],
        status: json["status"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "fullName": fullName,
        "landInAcres": landInAcres,
        "surveyNum": surveyNum,
        "water": water,
        "village": village,
        "description": description,
        "mandal": mandal,
        "district": district,
        "state": state,
        "latitude": latitude,
        "longitude": longitude,
        "radius": radius,
        "image_id": imageId,
        "image_url": imageUrl,
        "status": status,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}

class OwnerDetails {
    int? id;
    dynamic firstName;

    OwnerDetails({
        this.id,
        this.firstName,
    });

    factory OwnerDetails.fromJson(Map<String, dynamic> json) => OwnerDetails(
        id: json["id"],
        firstName: json["firstName"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
    };
}
