import 'package:get/get.dart';

import '../controllers/detail_relevant_jobs_controller.dart';

class DetailRelevantJobsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailRelevantJobsController>(
      () => DetailRelevantJobsController(),
    );
  }
}
