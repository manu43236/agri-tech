// To parse this JSON data, do
//
//     final addJobByFarmerModel = addJobByFarmerModelFromJson(jsonString);

import 'dart:convert';

AddJobByFarmerModel addJobByFarmerModelFromJson(String str) => AddJobByFarmerModel.fromJson(json.decode(str));

String addJobByFarmerModelToJson(AddJobByFarmerModel data) => json.encode(data.toJson());

class AddJobByFarmerModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    AddJobByFarmerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory AddJobByFarmerModel.fromJson(Map<String, dynamic> json) => AddJobByFarmerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    bool? flag;
    int? id;
    int? farmerId;
    String? fullName;
    String? description;
    DateTime? startDate;
    DateTime? endDate;
    String? state;
    String? district;
    String? village;
    String? mandal;
    String? status;
    DateTime? updatedAt;
    DateTime? createdAt;
    dynamic landLordId;
    dynamic imageId;
    dynamic image;

    Result({
        this.flag,
        this.id,
        this.farmerId,
        this.fullName,
        this.description,
        this.startDate,
        this.endDate,
        this.state,
        this.district,
        this.village,
        this.mandal,
        this.status,
        this.updatedAt,
        this.createdAt,
        this.landLordId,
        this.imageId,
        this.image,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        flag: json["flag"],
        id: json["id"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        description: json["description"],
        startDate: json["startDate"] == null ? null : DateTime.parse(json["startDate"]),
        endDate: json["endDate"] == null ? null : DateTime.parse(json["endDate"]),
        state: json["state"],
        district: json["district"],
        village: json["village"],
        mandal: json["mandal"],
        status: json["status"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        landLordId: json["landLordId"],
        imageId: json["image_id"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "flag": flag,
        "id": id,
        "farmerId": farmerId,
        "fullName": fullName,
        "description": description,
        "startDate": startDate?.toIso8601String(),
        "endDate": endDate?.toIso8601String(),
        "state": state,
        "district": district,
        "village": village,
        "mandal": mandal,
        "status": status,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "landLordId": landLordId,
        "image_id": imageId,
        "image": image,
    };
}
