import 'package:dartz/dartz.dart';

import '../../home/data/get_all_lands_by_farmer_model.dart';

abstract class AvailableFarmerMyLandsRepo{
  Future<Either<GetAllLandsByFarmerModel,Exception>> getFarmerLandsSearch();
}