import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:get/get.dart';

import '../../saved/data/get_all_saved_Jobs_model.dart';
import '../../saved/services/saved_repo_impl.dart';

class SavedJobsController extends BaseController {

  Rx<GetAllSavedJobsModel> getAllSavedJobsModel = Rx(GetAllSavedJobsModel());

  var id=''.obs;

  @override
  void onInit() async {
    super.onInit();
    id.value=await SecureStorage().readData(key: "id"??"");
    await getAllSavedJobs(userId: id.value);
  }

    // get all saved jobs API
  Future<void> getAllSavedJobs({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient).getAllJobs(userId: userId);
      print("===================== get all saved jobs ==============");
      print(result);
      result.fold((model) {
        getAllSavedJobsModel.value = model;
        print('model${getAllSavedJobsModel.value}');
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
