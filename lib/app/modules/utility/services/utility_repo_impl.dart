import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/utility/data/get_utility_by_id_model.dart';
import 'package:agritech/app/modules/utility/services/utility_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';


import '../data/create_utility_model.dart';
import '../data/get_recent_four_utilities_model.dart';
import '../data/get_utility_by_user_id_model.dart';

class UtilityRepoImpl extends UtilityRepo with NetworkCheckService{
  final DioClient _dioClient;
  UtilityRepoImpl(this._dioClient);

  @override
  Future<Either<GetRecentFourUtilitiesModel,Exception>>  recentUtilities({search})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('utility/getRecentFourUtilities?search=$search', Method.get);//?search=${Get.find<LandlordUtilitiesController>().utilitySearch.value}
        return result.fold((l){
          GetRecentFourUtilitiesModel model= GetRecentFourUtilitiesModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetUtilityByUserIdModel,Exception>> getUtilities({userId,search,state,district,mandal,village})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('utility/getUtilityByUtililityId?utililityId=$userId&search=$search&state=$state&district=$district&mandal=$mandal&village=$village', Method.get);
        return result.fold((l){
          GetUtilityByUserIdModel model= GetUtilityByUserIdModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<CreateUtilityModel,Exception>> addInstrument({params1})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('utility/createUtility', Method.post,params: params1);
        return result.fold((l){
          CreateUtilityModel model= CreateUtilityModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetUtilityByIdModel,Exception>> getUtilityById({id})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('utility/getUtilityById?id=$id', Method.get);
        return result.fold((l){
          GetUtilityByIdModel model= GetUtilityByIdModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetRecentFourUtilitiesModel,Exception>> getUtilityByUserId()async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('utility/getUtilityByUtililityId?utililityId=${Get.find<HomeController>().selectedLandlordId.value}', Method.get);
        return result.fold((l){
          GetRecentFourUtilitiesModel model= GetRecentFourUtilitiesModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  Future<Either<GetRecentFourUtilitiesModel,Exception>> filterUtilities({params1})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('utility/filterUtilitiesByLocation', Method.post,params: params1);
        return result.fold((l){
          GetRecentFourUtilitiesModel model= GetRecentFourUtilitiesModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}