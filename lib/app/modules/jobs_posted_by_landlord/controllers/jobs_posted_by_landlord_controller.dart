import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../home/data/all_jobs_of_landlord_model.dart';
import '../../home/services/home_repo_impl.dart';

class JobsPostedByLandlordController extends BaseController {

  Rx<AllJobsOfLandlordModel> allJobsOfLandlordModel =
      Rx(AllJobsOfLandlordModel());

  var id=''.obs;

  @override
  void onInit() async{
    super.onInit();
    id.value= await SecureStorage().readData(key: "id"??'');
    await addAlljobs(userId: id.value);
  }

  //Home Page ALL JOBS View
  Future<void> addAlljobs({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await HomeRepoImpl(dioClient)
          .addJobsOfLandlord(userId: userId);
      print("========================All jobs of landlord====================");
      print(result);
      result.fold((left) {
        allJobshandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void allJobshandleResponse(AllJobsOfLandlordModel model) async {
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
      allJobsOfLandlordModel.value = model;
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
