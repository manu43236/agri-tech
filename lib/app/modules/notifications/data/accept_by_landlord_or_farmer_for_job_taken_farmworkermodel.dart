// To parse this JSON data, do
//
//     final acceptbylandlordOrFarmerForJobTakenFarmworkerModel = acceptbylandlordOrFarmerForJobTakenFarmworkerModelFromJson(jsonString);

import 'dart:convert';

AcceptbylandlordOrFarmerForJobTakenFarmworkerModel acceptbylandlordOrFarmerForJobTakenFarmworkerModelFromJson(String str) => AcceptbylandlordOrFarmerForJobTakenFarmworkerModel.fromJson(json.decode(str));

String acceptbylandlordOrFarmerForJobTakenFarmworkerModelToJson(AcceptbylandlordOrFarmerForJobTakenFarmworkerModel data) => json.encode(data.toJson());

class AcceptbylandlordOrFarmerForJobTakenFarmworkerModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    AcceptbylandlordOrFarmerForJobTakenFarmworkerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory AcceptbylandlordOrFarmerForJobTakenFarmworkerModel.fromJson(Map<String, dynamic> json) => AcceptbylandlordOrFarmerForJobTakenFarmworkerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    Lease? lease;
    NewNotification? newNotification;

    Result({
        this.lease,
        this.newNotification,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        lease: json["lease"] == null ? null : Lease.fromJson(json["lease"]),
        newNotification: json["newNotification"] == null ? null : NewNotification.fromJson(json["newNotification"]),
    );

    Map<String, dynamic> toJson() => {
        "lease": lease?.toJson(),
        "newNotification": newNotification?.toJson(),
    };
}

class Lease {
    int? id;
    int? landlord;
    int? farmWorker;
    int? jobId;
    dynamic status;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic farmer;
    dynamic landId;
    dynamic startingDate;
    dynamic endingDate;

    Lease({
        this.id,
        this.landlord,
        this.farmWorker,
        this.jobId,
        this.status,
        this.updatedAt,
        this.createdAt,
        this.farmer,
        this.landId,
        this.startingDate,
        this.endingDate,
    });

    factory Lease.fromJson(Map<String, dynamic> json) => Lease(
        id: json["id"],
        landlord: json["landlord"],
        farmWorker: json["farmWorker"],
        jobId: json["jobId"],
        status: json["status"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        farmer: json["farmer"],
        landId: json["landId"],
        startingDate: json["starting_date"],
        endingDate: json["ending_date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landlord": landlord,
        "farmWorker": farmWorker,
        "jobId": jobId,
        "status": status,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "farmer": farmer,
        "landId": landId,
        "starting_date": startingDate,
        "ending_date": endingDate,
    };
}

class NewNotification {
    bool? isRead;
    int? id;
    dynamic name;
    dynamic message;
    dynamic from;
    dynamic to;
    int? jobId;
    dynamic statusForLandlord;
    dynamic statusForFarmer;
    dynamic statusForFarmworker;
    dynamic imageUrl;
    bool? flag;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic landId;

    NewNotification({
        this.isRead,
        this.id,
        this.name,
        this.message,
        this.from,
        this.to,
        this.jobId,
        this.statusForLandlord,
        this.statusForFarmer,
        this.statusForFarmworker,
        this.imageUrl,
        this.flag,
        this.updatedAt,
        this.createdAt,
        this.landId,
    });

    factory NewNotification.fromJson(Map<String, dynamic> json) => NewNotification(
        isRead: json["is_read"],
        id: json["id"],
        name: json["name"],
        message: json["message"],
        from: json["from"],
        to: json["to"],
        jobId: json["jobId"],
        statusForLandlord: json["status_for_landlord"],
        statusForFarmer: json["status_for_farmer"],
        statusForFarmworker: json["status_for_farmworker"],
        imageUrl: json["image_url"],
        flag: json["flag"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        landId: json["landId"],
    );

    Map<String, dynamic> toJson() => {
        "is_read": isRead,
        "id": id,
        "name": name,
        "message": message,
        "from": from,
        "to": to,
        "jobId": jobId,
        "status_for_landlord": statusForLandlord,
        "status_for_farmer": statusForFarmer,
        "status_for_farmworker": statusForFarmworker,
        "image_url": imageUrl,
        "flag": flag,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "landId": landId,
    };
}
