import 'package:agritech/app/modules/farm_worker_home_page/controllers/farm_worker_controller.dart';
import 'package:agritech/app/modules/farm_worker_home_page/views/widgets/my_skills_widget.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../routes/app_pages.dart';
import '../../home/controllers/landlord_controller.dart';
import '../../home/views/widgets/leased_jobs_widget.dart';
import '../../home/views/widgets/leased_utilities_widget.dart';

class FarmWorkerHomeView extends GetView<FarmWorkerHomePageController> {
  const FarmWorkerHomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async{
          controller.filterJobs();
          Get.find<LandlordController>().leasedJobs();
          Get.find<LandlordController>().leasedUtilities();
          controller.getUserByIdSkills();
        },
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'jobs_available'.tr,
                      style: TextStyles.kTSDS12W700.copyWith(color: colorHello),
                    ),
                    // IconButton(
                    //   onPressed: () {
                    //     controller.isJobs.value = true;
                    //     Get.toNamed(Routes.SELECT_REGION);
                    //   },
                    //   icon: const Icon(Icons.more_horiz, color: colorDots),
                    // ),
                  ],
                ),
              ),
              const SizedBox(height: 10),
              Obx(
                () {
                  final jobs = controller.filterJobsByModel.value.result;
                  if (jobs == null || jobs.isEmpty) {
                    return Center(
                      child: Text(
                        'no_jobs_available'.tr,
                        style: TextStyles.kTSDS14W500.copyWith(color: colorGrey),
                      ),
                    );
                  }
          
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: SizedBox(
                      height: Get.height * 0.14,
                      child: ListView.separated(
                        separatorBuilder: (context, index) =>
                            const SizedBox(width: 10),
                        itemCount: jobs.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          final job = jobs[index].job!;
                          return InkWell(
                            onTap: () async {
                              Get.find<HomeController>().jobId.value =
                                  job.id.toString();
                              // await Get.find<SavedJobsController>()
                              //     .getAllSavedJobs();
                              // await Get.find<HomeController>().getIdJobs();
                              Get.toNamed(Routes.DETAIL_FARMWORKER_JOBS,arguments: {"jobId":Get.find<HomeController>().jobId.value});
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(11),
                              child: job.image?.isNotEmpty == true
                                  ? Image.network(
                                      job.image!,
                                      width: Get.width * 0.42,
                                      fit: BoxFit.cover,
                                      errorBuilder: (context, error, stackTrace) {
                                        return _placeholderImage();
                                      },
                                    )
                                  : _placeholderImage(),
                            ),
                          );
                        },
                      ),
                    ),
                  );
                },
              ),
              const SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                    onTap: () async {
                      // Get.find<SelectRegionController>().isLandlordLands.value = false;
                      // await Get.find<LandlordController>().addAllLands();
                      Get.toNamed(Routes.AVAILABLE_FARMWORKER_JOBS);
                    },
                    child: Text(
                      'view_all'.tr,
                      style: TextStyles.kTSCF12W500.copyWith(
                        fontSize: 10,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10,),
              const LeasedJobsWidget(),
              const SizedBox(height: 10,),
              const LeasedUtilitiesWidget(),
              const SizedBox(height: 10,),
              const MySkillsWidget()
            ],
          ),
        ),
      ),
    );
  }

  Widget _placeholderImage() {
    return Image.asset(
      'assets/images/agri.png',
      height: Get.height * 0.12,
      width: Get.width * 0.5,
      fit: BoxFit.cover,
    );
  }
}
