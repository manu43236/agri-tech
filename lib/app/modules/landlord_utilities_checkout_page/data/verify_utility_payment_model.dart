// To parse this JSON data, do
//
//     final verifyUtilityPaymentModel = verifyUtilityPaymentModelFromJson(jsonString);

import 'dart:convert';

VerifyUtilityPaymentModel verifyUtilityPaymentModelFromJson(String str) => VerifyUtilityPaymentModel.fromJson(json.decode(str));

String verifyUtilityPaymentModelToJson(VerifyUtilityPaymentModel data) => json.encode(data.toJson());

class VerifyUtilityPaymentModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    VerifyUtilityPaymentModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory VerifyUtilityPaymentModel.fromJson(Map<String, dynamic> json) => VerifyUtilityPaymentModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    PaymentRecord? paymentRecord;
    Utility? utility;
    Lease? lease;

    Result({
        this.paymentRecord,
        this.utility,
        this.lease,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        paymentRecord: json["paymentRecord"] == null ? null : PaymentRecord.fromJson(json["paymentRecord"]),
        utility: json["utility"] == null ? null : Utility.fromJson(json["utility"]),
        lease: json["Lease"] == null ? null : Lease.fromJson(json["Lease"]),
    );

    Map<String, dynamic> toJson() => {
        "paymentRecord": paymentRecord?.toJson(),
        "utility": utility?.toJson(),
        "Lease": lease?.toJson(),
    };
}

class Lease {
    int? id;
    int? utilityid;
    dynamic utiliser;
    dynamic farmer;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic landlord;
    dynamic farmWorker;
    dynamic landId;
    dynamic jobId;
    dynamic startingDate;
    dynamic endingDate;
    dynamic status;

    Lease({
        this.id,
        this.utilityid,
        this.utiliser,
        this.farmer,
        this.updatedAt,
        this.createdAt,
        this.landlord,
        this.farmWorker,
        this.landId,
        this.jobId,
        this.startingDate,
        this.endingDate,
        this.status,
    });

    factory Lease.fromJson(Map<String, dynamic> json) => Lease(
        id: json["id"],
        utilityid: json["utilityid"],
        utiliser: json["utiliser"],
        farmer: json["farmer"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        landlord: json["landlord"],
        farmWorker: json["farmWorker"],
        landId: json["landId"],
        jobId: json["jobId"],
        startingDate: json["starting_date"],
        endingDate: json["ending_date"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "utilityid": utilityid,
        "utiliser": utiliser,
        "farmer": farmer,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "landlord": landlord,
        "farmWorker": farmWorker,
        "landId": landId,
        "jobId": jobId,
        "starting_date": startingDate,
        "ending_date": endingDate,
        "status": status,
    };
}

class PaymentRecord {
    int? id;
    int? userId;
    int? utilityId;
    dynamic quantity;
    dynamic costOfSelectedItem;
    dynamic tenPercentGst;
    dynamic nintyPercentGst;
    dynamic tenPercentPayment;
    dynamic nintyPercentpayment;
    dynamic totalPayment;
    dynamic paymentStatus;
    dynamic amountPaid;
    dynamic tenPercentPrice;
    dynamic ninetyPercentPrice;
    dynamic razorpayOrderId;
    dynamic razorpayPaymentId;
    dynamic razorpaySignature;
    dynamic createdAt;
    dynamic updatedAt;

    PaymentRecord({
        this.id,
        this.userId,
        this.utilityId,
        this.quantity,
        this.costOfSelectedItem,
        this.tenPercentGst,
        this.nintyPercentGst,
        this.tenPercentPayment,
        this.nintyPercentpayment,
        this.totalPayment,
        this.paymentStatus,
        this.amountPaid,
        this.tenPercentPrice,
        this.ninetyPercentPrice,
        this.razorpayOrderId,
        this.razorpayPaymentId,
        this.razorpaySignature,
        this.createdAt,
        this.updatedAt,
    });

    factory PaymentRecord.fromJson(Map<String, dynamic> json) => PaymentRecord(
        id: json["id"],
        userId: json["userId"],
        utilityId: json["utilityId"],
        quantity: json["quantity"],
        costOfSelectedItem: json["costOfSelectedItem"],
        tenPercentGst: json["tenPercentGST"]?.toDouble(),
        nintyPercentGst: json["nintyPercentGST"]?.toDouble(),
        tenPercentPayment: json["tenPercentPayment"]?.toDouble(),
        nintyPercentpayment: json["nintyPercentpayment"]?.toDouble(),
        totalPayment: json["totalPayment"],
        paymentStatus: json["paymentStatus"],
        amountPaid: json["amountPaid"]?.toDouble(),
        tenPercentPrice: json["tenPercentPrice"]?.toDouble(),
        ninetyPercentPrice: json["ninetyPercentPrice"]?.toDouble(),
        razorpayOrderId: json["razorpay_order_id"],
        razorpayPaymentId: json["razorpay_payment_id"],
        razorpaySignature: json["razorpay_signature"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "utilityId": utilityId,
        "quantity": quantity,
        "costOfSelectedItem": costOfSelectedItem,
        "tenPercentGST": tenPercentGst,
        "nintyPercentGST": nintyPercentGst,
        "tenPercentPayment": tenPercentPayment,
        "nintyPercentpayment": nintyPercentpayment,
        "totalPayment": totalPayment,
        "paymentStatus": paymentStatus,
        "amountPaid": amountPaid,
        "tenPercentPrice": tenPercentPrice,
        "ninetyPercentPrice": ninetyPercentPrice,
        "razorpay_order_id": razorpayOrderId,
        "razorpay_payment_id": razorpayPaymentId,
        "razorpay_signature": razorpaySignature,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}

class Utility {
    int? id;
    int? utililityId;
    dynamic nameOfInstrument;
    dynamic pricePerDay;
    dynamic brandName;
    dynamic state;
    dynamic district;
    dynamic mandal;
    dynamic village;
    dynamic description;
    int? imageId;
    dynamic image;
    dynamic latitude;
    dynamic longitude;
    bool? status;
    dynamic createdAt;
    dynamic updatedAt;

    Utility({
        this.id,
        this.utililityId,
        this.nameOfInstrument,
        this.pricePerDay,
        this.brandName,
        this.state,
        this.district,
        this.mandal,
        this.village,
        this.description,
        this.imageId,
        this.image,
        this.latitude,
        this.longitude,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Utility.fromJson(Map<String, dynamic> json) => Utility(
        id: json["id"],
        utililityId: json["utililityId"],
        nameOfInstrument: json["nameOfInstrument"],
        pricePerDay: json["pricePerDay"],
        brandName: json["brandName"],
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
        description: json["description"],
        imageId: json["image_id"],
        image: json["image"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        status: json["status"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "utililityId": utililityId,
        "nameOfInstrument": nameOfInstrument,
        "pricePerDay": pricePerDay,
        "brandName": brandName,
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
        "description": description,
        "image_id": imageId,
        "image": image,
        "latitude": latitude,
        "longitude": longitude,
        "status": status,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
