// To parse this JSON data, do
//
//     final getNintyPercentCropPaymentModel = getNintyPercentCropPaymentModelFromJson(jsonString);

import 'dart:convert';

GetNintyPercentCropPaymentModel getNintyPercentCropPaymentModelFromJson(String str) => GetNintyPercentCropPaymentModel.fromJson(json.decode(str));

String getNintyPercentCropPaymentModelToJson(GetNintyPercentCropPaymentModel data) => json.encode(data.toJson());

class GetNintyPercentCropPaymentModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetNintyPercentCropPaymentModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetNintyPercentCropPaymentModel.fromJson(Map<String, dynamic> json) => GetNintyPercentCropPaymentModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    Map<String, int>? utilityPayment;
    MasterCrop? masterCrop;
    AgriYield? agriYield;
    User? user;

    Result({
        this.utilityPayment,
        this.masterCrop,
        this.agriYield,
        this.user,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        utilityPayment: Map.from(json["utilityPayment"]!).map((k, v) => MapEntry<String, int>(k, v)),
        masterCrop: json["masterCrop"] == null ? null : MasterCrop.fromJson(json["masterCrop"]),
        agriYield: json["agriYield"] == null ? null : AgriYield.fromJson(json["agriYield"]),
        user: json["user"] == null ? null : User.fromJson(json["user"]),
    );

    Map<String, dynamic> toJson() => {
        "utilityPayment": Map.from(utilityPayment!).map((k, v) => MapEntry<String, dynamic>(k, v)),
        "masterCrop": masterCrop?.toJson(),
        "agriYield": agriYield?.toJson(),
        "user": user?.toJson(),
    };
}

class AgriYield {
    int? userid;
    dynamic hectors;
    dynamic remainingQuantity;

    AgriYield({
        this.userid,
        this.hectors,
        this.remainingQuantity,
    });

    factory AgriYield.fromJson(Map<String, dynamic> json) => AgriYield(
        userid: json["userid"],
        hectors: json["hectors"],
        remainingQuantity: json["remainingQuantity"],
    );

    Map<String, dynamic> toJson() => {
        "userid": userid,
        "hectors": hectors,
        "remainingQuantity": remainingQuantity,
    };
}

class MasterCrop {
    int? id;
    dynamic name;
    dynamic costPerHector;
    dynamic imageUrl;

    MasterCrop({
        this.id,
        this.name,
        this.costPerHector,
        this.imageUrl,
    });

    factory MasterCrop.fromJson(Map<String, dynamic> json) => MasterCrop(
        id: json["id"],
        name: json["name"],
        costPerHector: json["costPerHector"],
        imageUrl: json["image_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "costPerHector": costPerHector,
        "image_url": imageUrl,
    };
}

class User {
    dynamic firstName;

    User({
        this.firstName,
    });

    factory User.fromJson(Map<String, dynamic> json) => User(
        firstName: json["firstName"],
    );

    Map<String, dynamic> toJson() => {
        "firstName": firstName,
    };
}
