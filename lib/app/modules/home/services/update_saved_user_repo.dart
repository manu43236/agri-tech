import 'package:agritech/models/update_saved_user_model.dart';
import 'package:dartz/dartz.dart';

abstract class UpdateSavedUserRepo{
  Future<Either<UpdateSavedUserModel,Exception>>  updateSavedItems({params1});
}