import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../controllers/detail_recent_utilities_controller.dart';

class DetailRecentUtilitiesView
    extends GetView<DetailRecentUtilitiesController> {
  const DetailRecentUtilitiesView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'detail_recent_utility'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          right: 20,
          left: 20,
        ),
        child: SingleChildScrollView(
          child: Obx(
            () => controller.apigetUtilityStatus.value == ApiStatus.LOADING
                ? Shimmers().getListShimmer()
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: colorAsh,
                            borderRadius: BorderRadius.circular(15)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: Image.network(
                            controller.getUtilityByIdModel.value.result?.utility
                                    ?.image ??
                                defaultImageUrl,
                            height: Get.height * 0.3,
                            width: Get.width,
                            fit: BoxFit.cover,
                            errorBuilder: (context, error, stackTrace) {
                              // Display a local asset if the network image fails
                              return Image.asset(
                                defaultImageUrl,
                                height: Get.height * 0.3,
                                width: Get.width,
                                fit: BoxFit.cover,
                              );
                            },
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${controller.getUtilityByIdModel.value.result?.utility?.nameOfInstrument ?? ''}',
                                  style: TextStyles.kTSFS20W700
                                      .copyWith(color: colorDetails),
                                ),
                                Text(
                                  '${controller.getUtilityByIdModel.value.result?.utility?.village ?? ''}, ${controller.getUtilityByIdModel.value.result?.utility?.district ?? ''}',
                                  style: TextStyles.kTSDS14W500
                                      .copyWith(color: colorDetails),
                                ),
                              ],
                            ),
                          ),
                          // Obx(
                          //   () => InkWell(
                          //     onTap: () async {
                          //       if (Get.find<DetailsOfLandlordUtilitiesController>().isUtilityFav.value ==
                          //           true) {
                          //         await Get.find<HomeController>().updateSavedUser(
                          //             false,
                          //             errorMessage: "utility_unsaved_successfully".tr);
                          //         await Get.find<SavedUtilitiesController>()
                          //             .getSavedUtilities();
                          //         Get.find<DetailsOfLandlordUtilitiesController>().isUtilityFav.value = false;
                          //       } else {
                          //         await Get.find<DetailsOfLandlordUtilitiesController>()
                          //             .saveUtilityAsFavourite(id:controller.instrumentDetailsController.getUtilityByIdModel.value.result!.utility!.id );
                          //         await Get.find<SavedUtilitiesController>().getSavedUtilities();
                          //         print('save updated');
                          //         Get.find<DetailsOfLandlordUtilitiesController>().isUtilityFav.value = true;
                          //       }
                          //     },
                          //     child: Container(
                          //       padding: const EdgeInsets.symmetric(
                          //           horizontal: 10, vertical: 10),
                          //       decoration: BoxDecoration(
                          //         borderRadius: BorderRadius.circular(11),
                          //         color: Get.find<DetailsOfLandlordUtilitiesController>().isUtilityFav.value
                          //             ? colorBlue.withOpacity(0.5)
                          //             : colorSave,
                          //       ),
                          //       child: Icon(
                          //         Get.find<DetailsOfLandlordUtilitiesController>().isUtilityFav.value
                          //             ? Icons.bookmark
                          //             : Icons.bookmark_border_outlined,
                          //         color: colorBlack.withOpacity(0.5),
                          //       ),
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              CircleAvatar(
                                  radius: 26,
                                  backgroundColor: colorAsh,
                                  child: ClipOval(
                                    child: Image.network(
                                      controller.getUtilityByIdModel.value
                                              .result!.utiliser?.imageUrl ??
                                          defaultImageUrl,
                                      height: Get.height * 0.1,
                                      width: Get.width * 0.2,
                                      fit: BoxFit.cover,
                                      errorBuilder:
                                          (context, error, stackTrace) {
                                        return Image.asset(
                                          defaultImageUrl,
                                          height: Get.height * 0.1,
                                          width: Get.width * 0.2,
                                          fit: BoxFit.cover,
                                        );
                                      },
                                    ),
                                  )),
                              const SizedBox(
                                width: 10,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    controller.getUtilityByIdModel.value.result
                                            ?.utiliser?.firstName ??
                                        '',
                                    style: TextStyles.kTSDS14W700
                                        .copyWith(color: colorPic),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    controller.getUtilityByIdModel.value.result
                                            ?.utiliser?.address ??
                                        '',
                                    style: TextStyles.kTSWFS10W700
                                        .copyWith(color: colorDetails),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        controller.getUtilityByIdModel.value.result?.utility
                                ?.description ??
                            "",
                        style: TextStyles.kTSCF12W500,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      controller.getUtilityByIdModel.value.result?.leasetaker !=
                              null
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'lessee'.tr,
                                  style: TextStyles.kTSFS16W600
                                      .copyWith(color: primary),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        CircleAvatar(
                                            radius: 26,
                                            backgroundColor: colorAsh,
                                            child: ClipOval(
                                              child: Image.network(
                                                controller
                                                    .getUtilityByIdModel
                                                    .value
                                                    .result!
                                                    .leasetaker!
                                                    .imageUrl.toString(),
                                                height: Get.height * 0.1,
                                                width: Get.width * 0.2,
                                                fit: BoxFit.cover,
                                                errorBuilder: (context, error,
                                                    stackTrace) {
                                                  return Image.asset(
                                                    defaultImageUrl,
                                                    height: Get.height * 0.1,
                                                    width: Get.width * 0.2,
                                                    fit: BoxFit.cover,
                                                  );
                                                },
                                              ),
                                            )),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              controller
                                                      .getUtilityByIdModel
                                                      .value
                                                      .result
                                                      ?.leasetaker
                                                      ?.firstName ??
                                                  '',
                                              style: TextStyles.kTSDS14W700
                                                  .copyWith(color: colorPic),
                                            ),
                                            const SizedBox(
                                              height: 5,
                                            ),
                                            Text(
                                              controller
                                                      .getUtilityByIdModel
                                                      .value
                                                      .result
                                                      ?.leasetaker
                                                      ?.address ??
                                                  '',
                                              style: TextStyles.kTSWFS10W700
                                                  .copyWith(
                                                      color: colorDetails),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            )
                          : const SizedBox(),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        'suggested_utilities'.tr,
                        style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: Get.height * 0.08,
                        width: Get.width,
                        child: (controller
                                    .getRecentFourUtilitiesModel.value.result
                                    ?.where((utility) =>
                                        utility.id.toString() !=
                                        controller.getUtilityByIdModel.value
                                            .result?.utility?.id
                                            .toString())
                                    .isEmpty ??
                                true)
                            ? Center(
                                child: Text(
                                  'no_lands_are_available_near_your_location'
                                      .tr,
                                  style: const TextStyle(
                                    fontSize: 16,
                                    color: Colors.grey,
                                  ),
                                ),
                              )
                            : ListView.separated(
                                itemCount: controller
                                        .getRecentFourUtilitiesModel
                                        .value
                                        .result
                                        ?.where((utility) =>
                                            utility.id.toString() !=
                                            controller.getUtilityByIdModel.value
                                                .result?.utility?.id
                                                .toString())
                                        .length ??
                                    0,
                                scrollDirection: Axis.horizontal,
                                separatorBuilder: (context, index) =>
                                    const SizedBox(width: 10),
                                itemBuilder: (context, index) {
                                  final filteredLands = controller
                                      .getRecentFourUtilitiesModel
                                      .value
                                      .result
                                      ?.where((utility) =>
                                          utility.id.toString() !=
                                          controller.getUtilityByIdModel.value
                                              .result?.utility?.id
                                              .toString())
                                      .toList();
                                  final utility = filteredLands;
                                  return InkWell(
                                    onTap: () async {
                                      controller.getUtilityById(
                                          id: utility[index].id);
                                      //Get.toNamed(Routes.DETAIL_RECENT_UTILITIES);
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: colorGrey,
                                        borderRadius: BorderRadius.circular(11),
                                      ),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(11),
                                        child: Image.network(
                                          utility![index].image,
                                          height: Get.height * 0.09,
                                          width: Get.width * 0.2,
                                          fit: BoxFit.cover,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            return Image.asset(
                                              defaultImageUrl,
                                              height: Get.height * 0.09,
                                              width: Get.width * 0.2,
                                              fit: BoxFit.cover,
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                      )
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
