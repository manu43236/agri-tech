import 'package:agritech/app/modules/available_my_utilities/services/available_my_utilities_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../utility/data/get_utility_by_user_id_model.dart';

class AvailableMyUtilitiesController extends BaseController {

  Rx<GetUtilityByUserIdModel> getAvailableUtilityByUserIdModel =
      Rx(GetUtilityByUserIdModel());

  var availableUtilSearch=''.obs;
  var apiUtilitySearchStatus=ApiStatus.LOADING.obs;
  var dataSelectedItem=''.obs;
  var isLoan=false.obs;
  var isInsurance=false.obs;

  @override
  void onInit() async {
    super.onInit();
    if(Get.arguments!=null){
      isLoan.value=Get.arguments["isLoan"];
      isInsurance.value=Get.arguments["isInsurance"];
    }
    await availableUserUtilities(search: '',state: '',district: '',mandal: '',village: '');
  }

  //available utilities API
  Future<void> availableUserUtilities({search,state,district,mandal,village}) async {
    apiUtilitySearchStatus.value = ApiStatus.LOADING;

    try {
      var result = await AvailableMyUtilitiesRepoImpl(dioClient).availableSearchUtility(search: search,state: state,district: district,mandal: mandal,village: village);
      print('=============== get user utilities =========');
      print(result);
      result.fold((model) {
        if (model.statusCode == 200) {
          getAvailableUtilityByUserIdModel.value = model;
          print(getAvailableUtilityByUserIdModel.value.result!.first.brandName);
        } else if (model.statusCode == 400) {
          print('===============status=============');
          getAvailableUtilityByUserIdModel.value.result!.clear();
          getAvailableUtilityByUserIdModel.value.result! == [];
        }
        apiUtilitySearchStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiUtilitySearchStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
