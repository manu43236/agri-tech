import 'package:agritech/models/get_user_model.dart';
import 'package:dartz/dartz.dart';

abstract class GetUserRepo{
  Future<Either<GetUserModel,Exception>>  getUser();
}