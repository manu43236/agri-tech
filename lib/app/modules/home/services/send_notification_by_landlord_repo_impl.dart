import 'package:agritech/app/modules/home/services/send_notification_by_landlord_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../../../../models/send_notification_by_landlord_model.dart';

class SendNotificationByLandlordRepoImpl extends SendNotificationByLandlordRepo with NetworkCheckService{
  final DioClient _dioClient;
  SendNotificationByLandlordRepoImpl(this._dioClient);
  @override
  Future<Either<SendNotificationByLandlordModel,Exception>> sentNotification({params1})async{
     var data= await checkInternet();
     if(!data){
       return Right(Exception("No Network found"));
     }else{
      try{
        var result= await _dioClient.requestForAuth('notification/sendNotificationByLandLord', Method.post,params: params1);
        return result.fold((l){
          SendNotificationByLandlordModel model=SendNotificationByLandlordModel.fromJson(l.data);
          return Left(model);
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
     }
  }
}