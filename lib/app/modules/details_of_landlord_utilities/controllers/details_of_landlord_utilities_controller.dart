import 'package:agritech/app/modules/details_of_landlord_utilities/services/details_of_landlord_utilities_repo_impl.dart';
import 'package:agritech/app/modules/utility/data/get_recent_four_utilities_model.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../saved_utilities/data/get_all_saved_utility_model.dart';
import '../../saved_utilities/services/saved_utilities_repo_impl.dart';
import '../../utility/data/get_utility_by_id_model.dart';
import '../../utility/services/utility_repo_impl.dart';
import '../data/create_utility_payment_model.dart';
import '../data/saved_utility_model.dart';

class DetailsOfLandlordUtilitiesController extends BaseController {

  Rx<GetRecentFourUtilitiesModel> getUtilityByLocationModel = Rx(GetRecentFourUtilitiesModel());
  Rx<CreateUtilityPaymentModel> createUtilityPaymentModel = Rx(CreateUtilityPaymentModel());
  Rx<CreateUtilityPaymentModel> getUtilityPaymentByIdModel = Rx(CreateUtilityPaymentModel());
  Rx<SavedUtilityModel> saveUtilityAsFavouriteModel =
      Rx(SavedUtilityModel());
  Rx<GetUtilityByIdModel> getUtilityByIdModel = Rx(GetUtilityByIdModel());
  Rx<GetAllSavedUtilityModel> getAllSavedUtilityModel =
      Rx(GetAllSavedUtilityModel());

  

  var createdUtilityId="".obs;
  var paymentUtilityId="".obs;
  var utilityVillage = "".obs;

  var isUtilityFav=false.obs;
  var id=''.obs;
  var deleteId=0.obs;

  var apigetUtilityStatus=ApiStatus.LOADING.obs;
  var apiSavedUtilityStatus=ApiStatus.LOADING.obs;


  @override
  void onInit() async{
    super.onInit();
    id.value=await SecureStorage().readData(key: "id"??'');
    if(Get.arguments!=null){
       await getSavedUtilities(userId: id.value);
       await getUtilityById(id: Get.arguments["utilId"]);
       await getUtilityByLocation(village: utilityVillage.value);
    }
  }

  //get utility by id API
  Future<void> getUtilityById({id}) async {
    apigetUtilityStatus.value = ApiStatus.LOADING;
    try {
      var result = await UtilityRepoImpl(dioClient).getUtilityById(id: id);
      print("===================== get utility by id ==============");
      print(result);
      result.fold((model) {
        if(model.statusCode==200){
          getUtilityByIdModel.value = model;
        utilityVillage.value=getUtilityByIdModel.value.result!.utility!.village.toString();
        print(getUtilityByIdModel.value.result!.utility!.id);
        isUtilityFav.value = false;
        print('isUtilityFav${isUtilityFav.value}');
      if (getAllSavedUtilityModel.value.result!=null) {
        print('hiiiiiiiiiiiiiiiiiiiiii');
        // allSavedLandsModel.value.result!.savedLands!.contains(element)
        for (var element in getAllSavedUtilityModel
            .value
            .result!.savedUtilities!) {
          print('for loop');
          print(element.id);
          print(getUtilityByIdModel.value.result!.utility!.id);
          if (element.utilityid == getUtilityByIdModel.value.result!.utility!.id) {
            print('elwmmmmmmmmmm');
            deleteId.value = element.id!;
            print('delete id ${deleteId.value}');
            isUtilityFav.value = true;
          }
        }
        }
      }
        apigetUtilityStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apigetUtilityStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //get utility by location API
  Future<void> getUtilityByLocation({village}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailsOfLandlordUtilitiesRepoImpl(dioClient).getUtilitiesByLocation(village: village);
      print("===================== get utility by location ==============");
      print(result);
      result.fold((model) {
        getUtilityByLocationModel.value = model;
        print(getUtilityByLocationModel.value.result![0].nameOfInstrument);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //create utility payment API
  Future<void> createUtilityPayment({utilityId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailsOfLandlordUtilitiesRepoImpl(dioClient).createUtilityPayment(params1:{
        "userId": id.value, //Get.find<HomeController>().selectedLandlordId.value,
        "utilityId": utilityId //Get.find<UtilityController>().utilityId.value
      });
      print("===================== create utility payment ==============");
      print(result);
      result.fold((model) {
        createUtilityPaymentModel.value = model;
        createdUtilityId.value = createUtilityPaymentModel.value.result!.id.toString();
        print(createUtilityPaymentModel.value.result!.costOfSelectedItem);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //get Utility Payment By Id  API
  Future<void> getUtilityPayment({createdUtillityId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailsOfLandlordUtilitiesRepoImpl(dioClient).getUtilityPayment(id: createdUtillityId);
      print("===================== get utility payment by id ==============");
      print(result);
      result.fold((model) {
        getUtilityPaymentByIdModel.value = model;
        paymentUtilityId.value = getUtilityPaymentByIdModel.value.result!.id.toString();
        print(getUtilityPaymentByIdModel.value.result!.costOfSelectedItem);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

   //save utility as favourite API
  Future<void> saveUtilityAsFavourite({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailsOfLandlordUtilitiesRepoImpl(dioClient).savedUtility(id: id);
      print(
          "======================== save utility as favourite ====================");
      print(result);
      result.fold((model) async {
        saveUtilityAsFavouriteModel.value = model;
        print('statusCode:${saveUtilityAsFavouriteModel.value.statusCode}');
        if (saveUtilityAsFavouriteModel.value.statusCode == 200) {
          deleteId.value =saveUtilityAsFavouriteModel.value.result!.id!;
          print('venkyyyy${saveUtilityAsFavouriteModel.value.result!.id!}');
          Get.snackbar('Success', 'Utility saved successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (saveUtilityAsFavouriteModel.value.statusCode == 400) {
          Get.snackbar('Failed', 'Utility already saved',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

   //get saved utilities API
  Future<void> getSavedUtilities({userId}) async {
    apiSavedUtilityStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedUtilitiesRepoImpl(dioClient).getSavedUtilities(userId: userId);
      print(
          "======================== get saved utilities ====================");
      print(result);
      result.fold((model) async {
        getAllSavedUtilityModel.value = model;
        apiSavedUtilityStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiSavedUtilityStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }


  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}

