import 'package:get/get.dart';

import '../controllers/loans_view_controller.dart';

class LoansViewBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoansViewController>(
      () => LoansViewController(),
    );
  }
}
