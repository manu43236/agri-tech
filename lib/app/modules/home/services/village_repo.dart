import 'package:agritech/app/modules/select_region/data/village_model.dart';
import 'package:dartz/dartz.dart';

abstract class VillageRepo{
  Future<Either<VillageModel,Exception>> getVillage();
}