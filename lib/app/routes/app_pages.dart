import 'package:get/get.dart';

import '../modules/Landlord_utilities/bindings/landlord_utilities_binding.dart';
import '../modules/Landlord_utilities/views/landlord_utilities_view.dart';
import '../modules/add_a_land/bindings/add_a_land_binding.dart';
import '../modules/add_a_land/views/add_a_land_view.dart';
import '../modules/add_instrument/bindings/add_instrument_binding.dart';
import '../modules/add_instrument/views/add_instrument_view.dart';
import '../modules/add_job/bindings/add_job_binding.dart';
import '../modules/add_job/views/add_job_view.dart';
import '../modules/add_land/bindings/add_land_binding.dart';
import '../modules/add_land/views/add_land_view.dart';
import '../modules/add_skill/bindings/add_skill_binding.dart';
import '../modules/add_skill/views/add_skill_view.dart';
import '../modules/available_crops/bindings/available_crops_binding.dart';
import '../modules/available_crops/views/available_crops_view.dart';
import '../modules/available_farmer_my_jobs/bindings/available_farmer_my_jobs_binding.dart';
import '../modules/available_farmer_my_jobs/views/available_farmer_my_jobs_view.dart';
import '../modules/available_farmer_my_lands/bindings/available_farmer_my_lands_binding.dart';
import '../modules/available_farmer_my_lands/views/available_farmer_my_lands_view.dart';
import '../modules/available_farmworker_jobs/bindings/available_farmworker_jobs_binding.dart';
import '../modules/available_farmworker_jobs/views/available_farmworker_jobs_view.dart';
import '../modules/available_leased_jobs/bindings/available_leased_jobs_binding.dart';
import '../modules/available_leased_jobs/views/available_leased_jobs_view.dart';
import '../modules/available_leased_lands/bindings/available_leased_lands_binding.dart';
import '../modules/available_leased_lands/views/available_leased_lands_view.dart';
import '../modules/available_leased_utilities/bindings/available_leased_utilities_binding.dart';
import '../modules/available_leased_utilities/views/available_leased_utilities_view.dart';
import '../modules/available_my_jobs/bindings/available_my_jobs_binding.dart';
import '../modules/available_my_jobs/views/available_my_jobs_view.dart';
import '../modules/available_my_lands/bindings/available_my_lands_binding.dart';
import '../modules/available_my_lands/views/available_my_lands_view.dart';
import '../modules/available_my_skills/bindings/available_my_skills_binding.dart';
import '../modules/available_my_skills/views/available_my_skills_view.dart';
import '../modules/available_my_utilities/bindings/available_my_utilities_binding.dart';
import '../modules/available_my_utilities/views/available_my_utilities_view.dart';
import '../modules/available_purchased_crops/bindings/available_purchased_crops_binding.dart';
import '../modules/available_purchased_crops/views/available_purchased_crops_view.dart';
import '../modules/available_recent_lands/bindings/available_recent_lands_binding.dart';
import '../modules/available_recent_lands/views/available_recent_lands_view.dart';
import '../modules/available_recent_utilities/bindings/available_recent_utilities_binding.dart';
import '../modules/available_recent_utilities/views/available_recent_utilities_view.dart';
import '../modules/customer_support/bindings/customer_support_binding.dart';
import '../modules/customer_support/views/customer_support_view.dart';
import '../modules/dashboard/bindings/dashboard_binding.dart';
import '../modules/dashboard/views/dashboard_view.dart';
import '../modules/detail_crop/bindings/detail_crop_binding.dart';
import '../modules/detail_crop/views/detail_crop_view.dart';
import '../modules/detail_farmer_my_jobs/bindings/detail_farmer_my_jobs_binding.dart';
import '../modules/detail_farmer_my_jobs/views/detail_farmer_my_jobs_view.dart';
import '../modules/detail_farmer_my_lands/bindings/detail_farmer_my_lands_binding.dart';
import '../modules/detail_farmer_my_lands/views/detail_farmer_my_lands_view.dart';
import '../modules/detail_farmworker_jobs/bindings/detail_farmworker_jobs_binding.dart';
import '../modules/detail_farmworker_jobs/views/detail_farmworker_jobs_view.dart';
import '../modules/detail_leased_jobs/bindings/detail_leased_jobs_binding.dart';
import '../modules/detail_leased_jobs/views/detail_leased_jobs_view.dart';
import '../modules/detail_leased_lands/bindings/detail_leased_lands_binding.dart';
import '../modules/detail_leased_lands/views/detail_leased_lands_view.dart';
import '../modules/detail_leased_utilities/bindings/detail_leased_utilities_binding.dart';
import '../modules/detail_leased_utilities/views/detail_leased_utilities_view.dart';
import '../modules/detail_my_jobs/bindings/detail_my_jobs_binding.dart';
import '../modules/detail_my_jobs/views/detail_my_jobs_view.dart';
import '../modules/detail_my_lands/bindings/detail_my_lands_binding.dart';
import '../modules/detail_my_lands/views/detail_my_lands_view.dart';
import '../modules/detail_my_skills/bindings/detail_my_skills_binding.dart';
import '../modules/detail_my_skills/views/detail_my_skills_view.dart';
import '../modules/detail_purchased_crop/bindings/detail_purchased_crop_binding.dart';
import '../modules/detail_purchased_crop/views/detail_purchased_crop_view.dart';
import '../modules/detail_recent_lands/bindings/detail_recent_lands_binding.dart';
import '../modules/detail_recent_lands/views/detail_recent_lands_view.dart';
import '../modules/detail_recent_utilities/bindings/detail_recent_utilities_binding.dart';
import '../modules/detail_recent_utilities/views/detail_recent_utilities_view.dart';
import '../modules/detail_relevant_jobs/bindings/detail_relevant_jobs_binding.dart';
import '../modules/detail_relevant_jobs/views/detail_relevant_jobs_view.dart';
import '../modules/detail_saved_farmers/bindings/detail_saved_farmers_binding.dart';
import '../modules/detail_saved_farmers/views/detail_saved_farmers_view.dart';
import '../modules/detail_saved_farmworkers/bindings/detail_saved_farmworkers_binding.dart';
import '../modules/detail_saved_farmworkers/views/detail_saved_farmworkers_view.dart';
import '../modules/detail_saved_job/bindings/detail_saved_job_binding.dart';
import '../modules/detail_saved_job/views/detail_saved_job_view.dart';
import '../modules/detail_saved_landlord/bindings/detail_saved_landlord_binding.dart';
import '../modules/detail_saved_landlord/views/detail_saved_landlord_view.dart';
import '../modules/detail_saved_lands/bindings/detail_saved_lands_binding.dart';
import '../modules/detail_saved_lands/views/detail_saved_lands_view.dart';
import '../modules/detail_saved_utilities/bindings/detail_saved_utilities_binding.dart';
import '../modules/detail_saved_utilities/views/detail_saved_utilities_view.dart';
import '../modules/details_of_farmer/bindings/details_of_farmer_binding.dart';
import '../modules/details_of_farmer/views/details_of_farmer_view.dart';
import '../modules/details_of_farmworker/bindings/details_of_farmworker_binding.dart';
import '../modules/details_of_farmworker/views/details_of_farmworker_view.dart';
import '../modules/details_of_landlord/bindings/details_of_landlord_binding.dart';
import '../modules/details_of_landlord/views/details_of_landlord_view.dart';
import '../modules/details_of_landlord_land/bindings/details_of_landlord_land_binding.dart';
import '../modules/details_of_landlord_land/views/details_of_landlord_land_view.dart';
import '../modules/details_of_landlord_utilities/bindings/details_of_landlord_utilities_binding.dart';
import '../modules/details_of_landlord_utilities/views/details_of_landlord_utilities_view.dart';
import '../modules/edit_profile/bindings/edit_profile_binding.dart';
import '../modules/edit_profile/views/edit_profile_view.dart';
import '../modules/farm_worker_home_page/bindings/farm_worker_home_page_binding.dart';
import '../modules/farm_worker_home_page/views/farm_worker_home_view.dart';
import '../modules/farm_worker_utility/bindings/farm_worker_utility_binding.dart';
import '../modules/farm_worker_utility/views/farm_worker_utility_view.dart';
import '../modules/filter_vendor/bindings/filter_vendor_binding.dart';
import '../modules/filter_vendor/views/filter_vendor_view.dart';
import '../modules/filtered_utilities/bindings/filtered_utilities_binding.dart';
import '../modules/filtered_utilities/views/filtered_utilities_view.dart';
import '../modules/hired_farmers/bindings/hired_farmers_binding.dart';
import '../modules/hired_farmers/views/hired_farmers_view.dart';
import '../modules/hired_farmers_details/bindings/hired_farmers_details_binding.dart';
import '../modules/hired_farmers_details/views/hired_farmers_details_view.dart';
import '../modules/hired_farmworkers/bindings/hired_farmworkers_binding.dart';
import '../modules/hired_farmworkers/views/hired_farmworkers_view.dart';
import '../modules/hired_farmworkers_details/bindings/hired_farmworkers_details_binding.dart';
import '../modules/hired_farmworkers_details/views/hired_farmworkers_details_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/home/views/landlord_farmer.dart';
import '../modules/home/views/landlord_worker.dart';
import '../modules/instrument_details/bindings/instrument_details_binding.dart';
import '../modules/instrument_details/views/instrument_details_view.dart';
import '../modules/jobs_posted_by_farmer/bindings/jobs_posted_by_farmer_binding.dart';
import '../modules/jobs_posted_by_farmer/views/jobs_posted_by_farmer_view.dart';
import '../modules/jobs_posted_by_landlord/bindings/jobs_posted_by_landlord_binding.dart';
import '../modules/jobs_posted_by_landlord/views/jobs_posted_by_landlord_view.dart';
import '../modules/kyc_details_page/bindings/kyc_details_page_binding.dart';
import '../modules/kyc_details_page/views/kyc_details_page_view.dart';
import '../modules/kyc_welcome_page/bindings/kyc_welcome_page_binding.dart';
import '../modules/kyc_welcome_page/views/kyc_welcome_page_view.dart';
import '../modules/landlord_utilities_checkout_page/bindings/landlord_utilities_checkout_page_binding.dart';
import '../modules/landlord_utilities_checkout_page/views/landlord_utilities_checkout_page_view.dart';
import '../modules/landlord_utilities_payments_page/bindings/landlord_utilities_payments_page_binding.dart';
import '../modules/landlord_utilities_payments_page/views/landlord_utilities_payments_page_view.dart';
import '../modules/leased_utilities_checkout_page/bindings/leased_utilities_checkout_page_binding.dart';
import '../modules/leased_utilities_checkout_page/views/leased_utilities_checkout_page_view.dart';
import '../modules/loans/bindings/loans_view_binding.dart';
import '../modules/loans/views/loans_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/my_instruments/bindings/my_instruments_binding.dart';
import '../modules/my_instruments/views/my_instruments_view.dart';
import '../modules/my_instruments_details/bindings/my_instruments_details_binding.dart';
import '../modules/my_instruments_details/views/my_instruments_details_view.dart';
import '../modules/notifications/bindings/notifications_binding.dart';
import '../modules/notifications/views/notifications_view.dart';
import '../modules/otp/bindings/otp_binding.dart';
import '../modules/otp/views/otp_view.dart';
import '../modules/profile/bindings/profile_binding.dart';
import '../modules/profile/views/profile_view.dart';
import '../modules/profile_page/bindings/profile_page_binding.dart';
import '../modules/profile_page/views/profile_page_view.dart';
import '../modules/purchased_checkout_details/bindings/purchased_checkout_details_binding.dart';
import '../modules/purchased_checkout_details/views/purchased_checkout_details_view.dart';
import '../modules/saved/bindings/saved_binding.dart';
import '../modules/saved/views/saved_view.dart';
import '../modules/saved_farmers/bindings/saved_farmers_binding.dart';
import '../modules/saved_farmers/views/saved_farmers_view.dart';
import '../modules/saved_farmworkers/bindings/saved_farmworkers_binding.dart';
import '../modules/saved_farmworkers/views/saved_farmworkers_view.dart';
import '../modules/saved_jobs/bindings/saved_jobs_binding.dart';
import '../modules/saved_jobs/views/saved_jobs_view.dart';
import '../modules/saved_landlords/bindings/saved_landlords_binding.dart';
import '../modules/saved_landlords/views/saved_landlords_view.dart';
import '../modules/saved_lands/bindings/saved_lands_binding.dart';
import '../modules/saved_lands/views/saved_lands_view.dart';
import '../modules/saved_utilities/bindings/saved_utilities_binding.dart';
import '../modules/saved_utilities/views/saved_utilities_view.dart';
import '../modules/search/bindings/search_binding.dart';
import '../modules/search/views/search_view.dart';
import '../modules/select_region/bindings/select_region_binding.dart';
import '../modules/select_region/views/select_region_view.dart';
import '../modules/settings/bindings/settings_binding.dart';
import '../modules/settings/views/settings_view.dart';
import '../modules/signup/bindings/signup_binding.dart';
import '../modules/signup/views/signup_view.dart';
import '../modules/splash_screen/bindings/splash_screen_binding.dart';
import '../modules/splash_screen/views/splash_screen_view.dart';
import '../modules/success_payment_checkout_page/bindings/success_payment_checkout_page_binding.dart';
import '../modules/success_payment_checkout_page/views/success_payment_checkout_page_view.dart';
import '../modules/suitable_crops/bindings/suitable_crops_binding.dart';
import '../modules/suitable_crops/views/suitable_crops_view.dart';
import '../modules/translator_icon/bindings/translator_icon_binding.dart';
import '../modules/translator_icon/views/translator_icon_view.dart';
import '../modules/utility/bindings/utility_binding.dart';
import '../modules/utility/views/utility_view.dart';
import '../modules/utility_Loans/bindings/utility_loans_binding.dart';
import '../modules/utility_Loans/views/utility_loans_view.dart';
import '../modules/utility_vendor/bindings/utility_vendor_binding.dart';
import '../modules/utility_vendor/views/utility_vendor_view.dart';
import '../modules/vendor/bindings/vendor_binding.dart';
import '../modules/vendor/views/vendor_view.dart';
import '../modules/vendor_checkout_details/bindings/vendor_checkout_details_binding.dart';
import '../modules/vendor_checkout_details/views/vendor_checkout_details_view.dart';
import '../modules/vendor_loans/bindings/vendor_loans_binding.dart';
import '../modules/vendor_loans/views/vendor_loans_view.dart';
import '../modules/vendor_payment_successful/bindings/vendor_payment_successful_binding.dart';
import '../modules/vendor_payment_successful/views/vendor_payment_successful_view.dart';
import '../modules/welcome_page/bindings/welcome_page_binding.dart';
import '../modules/welcome_page/views/welcome_page_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LANDLORD_FARMER,
      page: () => const LandlordFarmer(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LANDLORD_WORKER,
      page: () => const LandlordWorker(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SELECT_REGION,
      page: () => SelectRegionView(),
      binding: SelectRegionBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_RECENT_LANDS,
      page: () => const AvailableRecentLandsView(),
      binding: AvailableRecentLandsBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_MY_LANDS,
      page: () => const AvailableMyLandsView(),
      binding: AvailableMyLandsBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_MY_JOBS,
      page: () => const AvailableMyJobsView(),
      binding: AvailableMyJobsBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_FARMER_MY_LANDS,
      page: () => const AvailableFarmerMyLandsView(),
      binding: AvailableFarmerMyLandsBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_FARMER_MY_JOBS,
      page: () => const AvailableFarmerMyJobsView(),
      binding: AvailableFarmerMyJobsBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_RECENT_LANDS,
      page: () => const DetailRecentLandsView(),
      binding: DetailRecentLandsBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_MY_LANDS,
      page: () => const DetailMyLandsView(),
      binding: DetailMyLandsBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_SAVED_LANDS,
      page: () => const DetailSavedLandsView(),
      binding: DetailSavedLandsBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_MY_JOBS,
      page: () => const DetailMyJobsView(),
      binding: DetailMyJobsBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_FARMER_MY_LANDS,
      page: () => const DetailFarmerMyLandsView(),
      binding: DetailFarmerMyLandsBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_FARMER_MY_JOBS,
      page: () => const DetailFarmerMyJobsView(),
      binding: DetailFarmerMyJobsBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_FARMWORKER_JOBS,
      page: () => const DetailFarmworkerJobsView(),
      binding: DetailFarmworkerJobsBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.WELCOME_PAGE,
      page: () => const WelcomePageView(),
      binding: WelcomePageBinding(),
    ),
    GetPage(
      name: _Paths.OTP,
      page: () => OtpView(),
      binding: OtpBinding(),
    ),
    GetPage(
      name: _Paths.PROFILE,
      page: () => ProfileView(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: _Paths.SIGNUP,
      page: () => SignupView(),
      binding: SignupBinding(),
    ),
    GetPage(
      name: _Paths.ADD_A_LAND,
      page: () => const AddALandView(),
      binding: AddALandBinding(),
    ),
    GetPage(
      name: _Paths.ADD_LAND,
      page: () => AddLandView(),
      binding: AddLandBinding(),
    ),
    GetPage(
      name: _Paths.ADD_JOB,
      page: () => AddJobView(),
      binding: AddJobBinding(),
    ),
    GetPage(
      name: _Paths.NOTIFICATIONS,
      page: () => const NotificationsView(),
      binding: NotificationsBinding(),
    ),
    GetPage(
      name: _Paths.SEARCH,
      page: () => const SearchView(),
      binding: SearchBinding(),
    ),
    GetPage(
      name: _Paths.DASHBOARD,
      page: () => const DashboardView(),
      binding: DashboardBinding(),
    ),
    GetPage(
      name: _Paths.PROFILE_PAGE,
      page: () => const ProfilePageView(),
      binding: ProfilePageBinding(),
    ),
    GetPage(
      name: _Paths.SAVED,
      page: () => const SavedView(),
      binding: SavedBinding(),
    ),
    GetPage(
      name: _Paths.SAVED_FARMERS,
      page: () => const SavedFarmersView(),
      binding: SavedFarmersBinding(),
    ),
    GetPage(
      name: _Paths.SAVED_FARMWORKERS,
      page: () => const SavedFarmworkersView(),
      binding: SavedFarmworkersBinding(),
    ),
    GetPage(
      name: _Paths.SAVED_LANDLORDS,
      page: () => const SavedLandlordsView(),
      binding: SavedLandlordsBinding(),
    ),
    GetPage(
      name: _Paths.SAVED_LANDS,
      page: () => const SavedLandsView(),
      binding: SavedLandsBinding(),
    ),
    GetPage(
      name: _Paths.SAVED_JOBS,
      page: () => const SavedJobsView(),
      binding: SavedJobsBinding(),
    ),
    GetPage(
      name: _Paths.EDIT_PROFILE,
      page: () => const EditProfileView(),
      binding: EditProfileBinding(),
    ),
    GetPage(
      name: _Paths.FARM_WORKER_HOME_PAGE,
      page: () => const FarmWorkerHomeView(),
      binding: FarmWorkerHomePageBinding(),
    ),
    GetPage(
      name: _Paths.FARM_WORKER_UTILITY,
      page: () => const FarmWorkerUtilityView(),
      binding: FarmWorkerUtilityBinding(),
    ),
    GetPage(
      name: _Paths.DETAILS_OF_FARMER,
      page: () => const DetailsOfFarmerView(),
      binding: DetailsOfFarmerBinding(),
    ),
    GetPage(
      name: _Paths.DETAILS_OF_FARMWORKER,
      page: () => const DetailsOfFarmworkerView(),
      binding: DetailsOfFarmworkerBinding(),
    ),
    GetPage(
      name: _Paths.DETAILS_OF_LANDLORD,
      page: () => const DetailsOfLandlordView(),
      binding: DetailsOfLandlordBinding(),
    ),
    GetPage(
      name: _Paths.DETAILS_OF_LANDLORD_LAND,
      page: () => const DetailsOfLandlordLandView(),
      binding: DetailsOfLandlordLandBinding(),
    ),
    GetPage(
      name: _Paths.LANDLORD_UTILITIES,
      page: () => const LandlordUtilitiesView(),
      binding: LandlordUtilitiesBinding(),
    ),
    GetPage(
      name: _Paths.LANDLORD_UTILITIES_CHECKOUT_PAGE,
      page: () => const LandlordUtilitiesCheckoutPageView(),
      binding: LandlordUtilitiesCheckoutPageBinding(),
    ),
    GetPage(
      name: _Paths.LANDLORD_UTILITIES_PAYMENTS_PAGE,
      page: () => const LandlordUtilitiesPaymentsPageView(),
      binding: LandlordUtilitiesPaymentsPageBinding(),
    ),
    GetPage(
      name: _Paths.MY_INSTRUMENTS,
      page: () => const MyInstrumentsView(),
      binding: MyInstrumentsBinding(),
    ),
    GetPage(
      name: _Paths.MY_INSTRUMENTS_DETAILS,
      page: () => const MyInstrumentsDetailsView(),
      binding: MyInstrumentsDetailsBinding(),
    ),
    GetPage(
      name: _Paths.UTILITY,
      page: () => const UtilityView(),
      binding: UtilityBinding(),
    ),
    GetPage(
      name: _Paths.UTILITY_VENDOR,
      page: () => const UtilityVendorView(),
      binding: UtilityVendorBinding(),
    ),
    GetPage(
      name: _Paths.UTILITY_LOANS,
      page: () => const UtilityLoansView(),
      binding: UtilityLoansBinding(),
    ),
    GetPage(
      name: _Paths.ADD_INSTRUMENT,
      page: () => AddInstrumentView(),
      binding: AddInstrumentBinding(),
    ),
    GetPage(
      name: _Paths.INSTRUMENT_DETAILS,
      page: () => const InstrumentDetailsView(),
      binding: InstrumentDetailsBinding(),
    ),
    GetPage(
      name: _Paths.MY_INSTRUMENTS,
      page: () => const MyInstrumentsView(),
      binding: MyInstrumentsBinding(),
    ),
    GetPage(
      name: _Paths.FILTERED_UTILITIES,
      page: () => const FilteredUtilitiesView(),
      binding: FilteredUtilitiesBinding(),
    ),
    GetPage(
      name: _Paths.HIRED_FARMWORKERS,
      page: () => HiredFarmworkersView(),
      binding: HiredFarmworkersBinding(),
    ),
    GetPage(
      name: _Paths.HIRED_FARMWORKERS_DETAILS,
      page: () => const HiredFarmworkersDetailsView(),
      binding: HiredFarmworkersDetailsBinding(),
    ),
    GetPage(
      name: _Paths.HIRED_FARMERS,
      page: () => HiredFarmersView(),
      binding: HiredFarmersBinding(),
    ),
    GetPage(
      name: _Paths.HIRED_FARMERS_DETAILS,
      page: () => const HiredFarmersDetailsView(),
      binding: HiredFarmersDetailsBinding(),
    ),
    GetPage(
      name: _Paths.DETAILS_OF_LANDLORD_UTILITIES,
      page: () => const DetailsOfLandlordUtilitiesView(),
      binding: DetailsOfLandlordUtilitiesBinding(),
    ),
    GetPage(
      name: _Paths.SUITABLE_CROPS,
      page: () => const SuitableCropsView(),
      binding: SuitableCropsBinding(),
    ),
    GetPage(
      name: _Paths.JOBS_POSTED_BY_LANDLORD,
      page: () => const JobsPostedByLandlordView(),
      binding: JobsPostedByLandlordBinding(),
    ),
    GetPage(
      name: _Paths.JOBS_POSTED_BY_FARMER,
      page: () => const JobsPostedByFarmerView(),
      binding: JobsPostedByFarmerBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_SAVED_FARMERS,
      page: () => const DetailSavedFarmersView(),
      binding: DetailSavedFarmersBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_SAVED_FARMWORKERS,
      page: () => const DetailSavedFarmworkersView(),
      binding: DetailSavedFarmworkersBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_SAVED_LANDLORD,
      page: () => const DetailSavedLandlordView(),
      binding: DetailSavedLandlordBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_SAVED_JOB,
      page: () => const DetailSavedJobView(),
      binding: DetailSavedJobBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH_SCREEN,
      page: () => const SplashScreenView(),
      binding: SplashScreenBinding(),
    ),
    GetPage(
      name: _Paths.SETTINGS,
      page: () => const SettingsView(),
      binding: SettingsBinding(),
    ),
    GetPage(
      name: _Paths.CUSTOMER_SUPPORT,
      page: () => const CustomerSupportView(),
      binding: CustomerSupportBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_LEASED_UTILITIES,
      page: () => const AvailableLeasedUtilitiesView(),
      binding: AvailableLeasedUtilitiesBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_LEASED_UTILITIES,
      page: () => const DetailLeasedUtilitiesView(),
      binding: DetailLeasedUtilitiesBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_FARMWORKER_JOBS,
      page: () => const AvailableFarmworkerJobsView(),
      binding: AvailableFarmworkerJobsBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_LEASED_JOBS,
      page: () => const DetailLeasedJobsView(),
      binding: DetailLeasedJobsBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_LEASED_LANDS,
      page: () => const DetailLeasedLandsView(),
      binding: DetailLeasedLandsBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_LEASED_LANDS,
      page: () => const AvailableLeasedLandsView(),
      binding: AvailableLeasedLandsBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_LEASED_JOBS,
      page: () => const AvailableLeasedJobsView(),
      binding: AvailableLeasedJobsBinding(),
    ),
    GetPage(
      name: _Paths.LEASED_UTILITIES_CHECKOUT_PAGE,
      page: () => const LeasedUtilitiesCheckoutPageView(),
      binding: LeasedUtilitiesCheckoutPageBinding(),
    ),
    GetPage(
      name: _Paths.SUCCESS_PAYMENT_CHECKOUT_PAGE,
      page: () => const SuccessPaymentCheckoutPageView(),
      binding: SuccessPaymentCheckoutPageBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_RECENT_UTILITIES,
      page: () => const AvailableRecentUtilitiesView(),
      binding: AvailableRecentUtilitiesBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_RECENT_UTILITIES,
      page: () => const DetailRecentUtilitiesView(),
      binding: DetailRecentUtilitiesBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_MY_UTILITIES,
      page: () => const AvailableMyUtilitiesView(),
      binding: AvailableMyUtilitiesBinding(),
    ),
    GetPage(
      name: _Paths.SAVED_UTILITIES,
      page: () => const SavedUtilitiesView(),
      binding: SavedUtilitiesBinding(),
    ),
    GetPage(
      name: _Paths.LOANS_VIEW,
      page: () => const LoansView(),
      binding: LoansViewBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_SAVED_UTILITIES,
      page: () => const DetailSavedUtilitiesView(),
      binding: DetailSavedUtilitiesBinding(),
    ),
    GetPage(
      name: _Paths.VENDOR,
      page: () => const VendorView(),
      binding: VendorBinding(),
    ),
    GetPage(
      name: _Paths.VENDOR_LOANS,
      page: () => const VendorLoansView(),
      binding: VendorLoansBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_CROPS,
      page: () => const AvailableCropsView(),
      binding: AvailableCropsBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_CROP,
      page: () => const DetailCropView(),
      binding: DetailCropBinding(),
    ),
    GetPage(
      name: _Paths.FILTER_VENDOR,
      page: () => const FilterVendorView(),
      binding: FilterVendorBinding(),
    ),
    GetPage(
      name: _Paths.VENDOR_CHECKOUT_DETAILS,
      page: () => const VendorCheckoutDetailsView(),
      binding: VendorCheckoutDetailsBinding(),
    ),
    GetPage(
      name: _Paths.PURCHASED_CHECKOUT_DETAILS,
      page: () => const PurchasedCheckoutDetailsView(),
      binding: PurchasedCheckoutDetailsBinding(),
    ),
    GetPage(
      name: _Paths.VENDOR_PAYMENT_SUCCESSFUL,
      page: () => const VendorPaymentSuccessfulView(),
      binding: VendorPaymentSuccessfulBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_PURCHASED_CROP,
      page: () => const DetailPurchasedCropView(),
      binding: DetailPurchasedCropBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_PURCHASED_CROPS,
      page: () => const AvailablePurchasedCropsView(),
      binding: AvailablePurchasedCropsBinding(),
    ),
    GetPage(
      name: _Paths.KYC_WELCOME_PAGE,
      page: () => const KycWelcomePageView(),
      binding: KycWelcomePageBinding(),
    ),
    GetPage(
      name: _Paths.KYC_DETAILS_PAGE,
      page: () => const KycDetailsPageView(),
      binding: KycDetailsPageBinding(),
    ),
    GetPage(
      name: _Paths.ADD_SKILL,
      page: () => AddSkillView(),
      binding: AddSkillBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_MY_SKILLS,
      page: () => const DetailMySkillsView(),
      binding: DetailMySkillsBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_RELEVANT_JOBS,
      page: () => const DetailRelevantJobsView(),
      binding: DetailRelevantJobsBinding(),
    ),
    GetPage(
      name: _Paths.AVAILABLE_MY_SKILLS,
      page: () => const AvailableMySkillsView(),
      binding: AvailableMySkillsBinding(),
    ),
    GetPage(
      name: _Paths.TRANSLATOR_ICON,
      page: () => const TranslatorIconView(),
      binding: TranslatorIconBinding(),
    ),
  ];
}
