import 'package:agritech/models/login_get_otp_model.dart';
import 'package:dartz/dartz.dart';

abstract class LoginRepo{
  Future<Either<LoginGetOtpModel,Exception>> loginOtp({params1});
}