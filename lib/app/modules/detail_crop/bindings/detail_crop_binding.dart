import 'package:get/get.dart';

import '../controllers/detail_crop_controller.dart';

class DetailCropBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailCropController>(
      () => DetailCropController(),
    );
  }
}
