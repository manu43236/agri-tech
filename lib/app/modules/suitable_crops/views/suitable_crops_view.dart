import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../../../core/base/dropdowns/view/crops_dropdown.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/themes/text_styles.dart';
import '../../home/controllers/home_controller.dart';
import 'grid_view_widget.dart';
import '../controllers/suitable_crops_controller.dart';

class SuitableCropsView extends GetView<SuitableCropsController> {
  const SuitableCropsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final homeController = Get.find<HomeController>();
    print('jjjjjjjjjjjjjjjjj');
    print('hhhhhhhhhhhh${controller.selectedValue.value}');
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'suitable_crop'.tr,
          style: TextStyles.kTSFS24W600.copyWith(color: colorSuitable),
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            // controller.selectedValue.value = '';
            //controller.selectedCropName.value = '';
            Get.back();
          },
          icon: const Icon(Icons.arrow_back, color: colorBlack),
        ),
      ),
      body: Obx(
        () => SingleChildScrollView(
          child:
              // controller.apiUserSelectedCropStatus.value == ApiStatus.LOADING
              //     ? Shimmers().getShimmerItem()
              //     :
              Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Crop Dropdown
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                child: 
                controller.dataa.value.isNotEmpty
                    ? Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Container(
                          width: Get.width,
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          decoration: BoxDecoration(
                              color: primary,
                              borderRadius: BorderRadius.circular(11)),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: Text(
                              controller.selectedValue.value,
                              style: TextStyles.kTSFS16W400.copyWith(
                                  color: colorWhite,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      )
                    : 
                    CropDropdown(
                        textEditingController: TextEditingController(),
                        onSelected: (selectedCrop) async {
                          if (selectedCrop != null) {
                            controller.selectedValue.value = selectedCrop;
                            final selectedCropData = controller
                                .getCropsByMandalModel.value.result!.crops
                                ?.firstWhereOrNull(
                                    (crop) => crop.name == selectedCrop);

                            if (selectedCropData != null) {
                              await controller
                                  .getCropById(selectedCropData.id.toString());
                              await controller.CreateSelectedCropWeek();
                              // await controller.getUserSelectedCrop(
                              //   homeController.landId.value,
                              //   controller.cropId.value,
                              // );
                              await controller.getUserSelectedCrop(
                                landId:homeController.landId.value,
                                cropId:controller.cropId.value,
                                lang: controller.selectedLang.value
                              );
                              await controller.getYieldHectors(
                                  landId: homeController.landId.value,
                                  cropIds: controller.cropId.value);
                              print('done');
                            }
                          }
                        },
                      ),
              ),
              const SizedBox(height: 20),

              // Display selected crop details if available
              controller.selectedValue.value.isNotEmpty
                  ? Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Container(
                        // height: 40,
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        width: Get.width,
                        decoration: BoxDecoration(
                          color: colorSuitableCrop,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            '${'selected_crop_for'.tr} ${controller.getUserSelectedCropModel.value.result?.masterCrops![0].totaldays} ${'days'.tr}',
                            style: TextStyles.kTSFS16W700
                                .copyWith(color: colorWhite),
                            textAlign: TextAlign.start,
                          ),
                        ),
                      ),
                    )
                  : Obx(() {
                      final cropByIdModel = controller.getCropByIdModel.value;
                      if (controller.selectedValue.value.isNotEmpty &&
                          cropByIdModel.result?.totaldays != null) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Container(
                            // height: 40,
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            width: Get.width,
                            decoration: BoxDecoration(
                              color: colorSuitableCrop,
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 20),
                              child: Text(
                                '${'selected_crop_for'.tr} ${cropByIdModel.result!.totaldays} ${'days'.tr}',
                                style: TextStyles.kTSFS16W700
                                    .copyWith(color: colorWhite),
                                textAlign: TextAlign.start,
                              ),
                            ),
                          ),
                        );
                      } else {
                        return const SizedBox();
                      }
                    }),
              const SizedBox(height: 20),
              //Text('')
              controller.selectedValue.value.isNotEmpty
                  ? Padding(
                      padding: const EdgeInsets.only(left: 20,right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          SizedBox(
                            width: 130,
                            child: Obx(
                              () => Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('yield'.tr,style: TextStyles.kTSDS14W500,),
                                  const SizedBox(height: 3,),
                                  SizedBox(
                                    height: 35,
                                    child: TextFormField(
                                      controller:
                                          controller.hectorsController.value,
                                      keyboardType: TextInputType.number,
                                      readOnly: !controller.isEditing.value,
                                      //style: TextStyle(color: colorWhite),
                                      decoration: InputDecoration(
                                        filled: true, // Enable background color
                                        fillColor: colorWhite,
                                        border: const OutlineInputBorder(
                                          
                                            //borderRadius: BorderRadius.circular(12), // Curved borders
                                            //borderSide: BorderSide.none,
                                            ),
                                        contentPadding: const EdgeInsets.symmetric(
                                            vertical: 0, horizontal: 5),
                                        isDense: true,
                                        suffixText: 'tons'.tr, // Add the suffix here
                                        suffixStyle: TextStyles.kTSDS14W500
                                            .copyWith(color: colorGrey),
                                        suffixIcon: InkWell(
                                          onTap: () {
                                            print('on tap');
                                            //controller.toggleEditing();
                                            controller.isEditing.value = !controller.isEditing.value;
                                          },
                                          child: Icon(
                                            //Icons.edit
                                            controller.isEditing.value
                                                ? Icons.check
                                                : Icons.edit,
                                            color: controller.isEditing.value
                                                ? primary
                                                : colorGrey,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(width: 10),
                          GestureDetector(
                            onTap: () {
                              if (!controller.isEditing.value) {
                                controller
                                    .toggleEditing(); // Disable editing and save
                              } else {
                                Fluttertoast.showToast(
                                  msg: "enable_editing_to_modify_the_field".tr,
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 14.0,
                                );
                              }
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 9),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  color:
                                      primary // Replace with your `primary` color
                                  ),
                              child: Text(
                                'submit'.tr,
                                style: const TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Obx(() {
                      final cropByIdModel = controller.getCropByIdModel.value;
                      if (controller.selectedValue.value.isNotEmpty &&
                          cropByIdModel.result?.totaldays != null) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Row(
                            children: [
                              SizedBox(
                                width: 100,
                                child: Obx(
                                  () => Container(
                                    height: 35,
                                    child: TextFormField(
                                      controller:
                                          controller.hectorsController.value,
                                      keyboardType: TextInputType.number,
                                      readOnly: !controller.isEditing.value,
                                      textAlign: TextAlign.right,
                                      //style: TextStyle(color: colorWhite),
                                      decoration: InputDecoration(
                                        filled: true, // Enable background color
                                        fillColor: colorWhite,
                                        border: const OutlineInputBorder(
                                            //borderRadius: BorderRadius.circular(12), // Curved borders
                                            //borderSide: BorderSide.none,
                                            ),
                                        contentPadding: const EdgeInsets.symmetric(
                                            vertical: 0, horizontal: 5),
                                        isDense: true,
                                        suffixText:
                                            'tons'.tr, // Add the suffix here
                                        suffixIconConstraints:
                                            const BoxConstraints(maxWidth: 50),
                                        suffixStyle: TextStyles.kTSDS14W500
                                            .copyWith(color: colorGrey),
                                        suffixIcon: InkWell(
                                          onTap: () {
                                            print('on tap');
                                           // controller.toggleEditing();
                                           controller.isEditing.value = !controller.isEditing.value;
                                          },
                                          child: Icon(
                                            //Icons.edit
                                            controller.isEditing.value
                                                ? Icons.check
                                                : Icons.edit,
                                            color: controller.isEditing.value
                                                ? primary
                                                : colorGrey,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(width: 10),
                              GestureDetector(
                                onTap: () {
                                  if (!controller.isEditing.value) {
                                    controller
                                        .toggleEditing(); // Disable editing and save
                                  } else {
                                    Fluttertoast.showToast(
                                      msg:
                                          "enable_editing_to_modify_the_field".tr,
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white,
                                      fontSize: 14.0,
                                    );
                                  }
                                },
                                child: Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 9),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color:
                                          primary // Replace with your `primary` color
                                      ),
                                  child: Text(
                                    'submit'.tr,
                                    style: const TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      } else {
                        return const SizedBox();
                      }
                    }),
              const SizedBox(height: 20),
              Obx(() {
                if (controller.selectedValue.value.isNotEmpty &&
                    controller.getUserSelectedCropModel.value.result != null &&
                    controller.getUserSelectedCropModel.value.result!
                        .groupedCrops!.isNotEmpty) {
                  return SizedBox(
                    height: Get.height,
                    child: const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: GridViewWidget(),
                    ),
                  );
                } else if (controller.selectedValue.value.isNotEmpty) {
                  // Show error message if no data is found
                  return Center(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Text(
                        'no_dataa'.tr,
                        style:
                            TextStyle(color: Colors.red.shade300, fontSize: 16),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  );
                } else {
                  return const SizedBox();
                }
              }),
            ],
          ),
        ),
      ),
    );
  }
}
