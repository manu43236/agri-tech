import 'package:agritech/app/modules/detail_recent_lands/data/land_by_id_model.dart';
import 'package:dartz/dartz.dart';

abstract class LandByIdRepo{
  Future<Either<LandByIdModel,Exception>> getLand({params1});
}