import 'package:get/get.dart';

import '../../details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import '../../landlord_utilities_checkout_page/controllers/landlord_utilities_checkout_page_controller.dart';
import '../controllers/leased_utilities_checkout_page_controller.dart';

class LeasedUtilitiesCheckoutPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LeasedUtilitiesCheckoutPageController>(
      () => LeasedUtilitiesCheckoutPageController(),
    );
    Get.lazyPut<DetailsOfLandlordUtilitiesController>(
      () => DetailsOfLandlordUtilitiesController(),
    );
    Get.lazyPut<LandlordUtilitiesCheckoutPageController>(
      () => LandlordUtilitiesCheckoutPageController(),
    );
  }
}
