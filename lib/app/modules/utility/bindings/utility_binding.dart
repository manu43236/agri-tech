import 'package:get/get.dart';

import '../../instrument_details/controllers/instrument_details_controller.dart';
import '../../my_instruments/controllers/my_instruments_controller.dart';
import '../controllers/utility_controller.dart';

class UtilityBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UtilityController>(
      () => UtilityController(),
    );
    Get.lazyPut<InstrumentDetailsController>(
      ()=>InstrumentDetailsController()
    );
    Get.lazyPut<MyInstrumentsController>(
      ()=>MyInstrumentsController()
    );
  }
}
