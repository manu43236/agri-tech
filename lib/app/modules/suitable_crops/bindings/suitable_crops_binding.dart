import 'package:get/get.dart';

import '../controllers/suitable_crops_controller.dart';

class SuitableCropsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SuitableCropsController>(
      () => SuitableCropsController(),
    );
  }
}
