import 'package:agritech/models/register_model.dart';
import 'package:dartz/dartz.dart';
import '../../../../core/base/dio_client.dart';
import '../../../../core/network/network_check_service.dart';
import 'signup_repo.dart';

class SignUpRepoImpl extends SignUpRepo with NetworkCheckService {
  final DioClient _dioClient;

  SignUpRepoImpl(this._dioClient);

  @override
  Future<Either<RegisterModel, Exception>> doSignup({params1}) async {
    bool data = await checkInternet();

    if (!data) {
      return Right(Exception('No Network found'));
    } else {
      try {
        print(params1);
        var result = await _dioClient.requestForAuth('User/userRegister', Method.post,
            params: params1);
        return result.fold((l) {
          RegisterModel model = RegisterModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      } catch (e) {
        return Right(Exception(e));
      }
    }
  }
}
