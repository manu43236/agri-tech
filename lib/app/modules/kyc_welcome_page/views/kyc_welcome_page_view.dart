import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/kyc_welcome_page_controller.dart';

class KycWelcomePageView extends GetView<KycWelcomePageController> {
  const KycWelcomePageView({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text('kyc_details'.tr, style: TextStyles.kTSFS16W600.copyWith(fontWeight: FontWeight.w500,color:colorBlack )),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
        SizedBox(height: Get.height*0.1),
          Image.asset(faceId,height: Get.height*0.15),
          const SizedBox(height: 20),
          Text('verify_your_identity'.tr, style: TextStyles.kTSFS26W800),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
            child: Text(
              "kyc_text".tr,
              style: TextStyles.kTSFS12W500
                  .copyWith(color: const Color(0xff838BA1)),
            ),
          ),
          SizedBox(height:Get.height*0.25),
          InkWell(
            onTap: () {
              Get.toNamed(Routes.KYC_DETAILS_PAGE);
            },
            child: Container(
              padding:
                  const EdgeInsets.symmetric(horizontal: 135,vertical: 18),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18),
                color: primary,
              ),
              child: Text(
                'proceed'.tr,
                style: TextStyles.kTSFS16W600
                    .copyWith(color: colorGhostWhite),
              ),
            ),
          ),
            const SizedBox(height:20),
          InkWell(
            onTap: () {
              Get.toNamed(Routes.DASHBOARD);
            },
            child: Text(
              'skip'.tr,
              style: TextStyles.kTSFS14W700
            ),
          ),
        ]),
      ),
    );
  }
}
