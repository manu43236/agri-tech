import 'package:agritech/app/modules/home/data/get_leased_jobs_details_model.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../home/services/home_repo_impl.dart';
import '../services/available_leased_jobs_repo_impl.dart';

class AvailableLeasedJobsController extends BaseController {

  Rx<GetLeasedJobDetailsModel> getLeasedSearchJobsDetailsModel =
      Rx(GetLeasedJobDetailsModel());

  var workerLeasedJobSearch=''.obs;
  var apiWorkerLeasedJobsStatus=ApiStatus.LOADING.obs;

  @override
  void onInit() {
    super.onInit();
    leasedJobs();
  }

  // Home Page leased jobs  API
  Future<void> leasedJobs() async {
    apiWorkerLeasedJobsStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await AvailableLeasedJobsRepoImpl(dioClient).leasedSearchJobs();
      print('============hey=============');
      print(result);
      result.fold((model) {
        if(model.statusCode==200){
          getLeasedSearchJobsDetailsModel.value=model;
          print(getLeasedSearchJobsDetailsModel.value.result!.jobs!.length);
        }else if(model.statusCode==400){
     
      getLeasedSearchJobsDetailsModel.value.result!.jobs!.clear();
      getLeasedSearchJobsDetailsModel.value.result!.jobs ==[];
    }
        apiWorkerLeasedJobsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiWorkerLeasedJobsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
