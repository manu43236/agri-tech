import 'package:flutter/material.dart';

import '../conts/color_consts.dart';


class TextStyles {
  static const TextStyle kTSNFS16 = TextStyle(
      color: Colors.white,
      fontSize: 16,
      fontWeight: FontWeight.normal,
      fontFamily: 'Inters');
  static const TextStyle kTSNFS16W400 = TextStyle(
      color: Colors.white,
      fontSize: 16,
      fontWeight: FontWeight.w400,
      fontFamily: 'Inters');

  static const TextStyle kTSNFS16W600 = TextStyle(
      color: Colors.white,
      fontSize: 16,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');

  static const TextStyle kTSNFS16W500 = TextStyle(
      color: Colors.white,
      fontSize: 16,
      fontWeight: FontWeight.w500,
      fontFamily: 'Inters');

static const TextStyle kTSNFS11W400 = TextStyle(
      color: Colors.white,
      fontSize: 11,
      fontWeight: FontWeight.w400,
      fontFamily: 'Inters');

  static const TextStyle kTSNFS11W600 = TextStyle(
      color: Colors.white,
      fontSize: 11,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');

  static const TextStyle kTSNFS14 = TextStyle(
      color: Colors.white,
      fontSize: 14,
      fontWeight: FontWeight.normal,
      fontFamily: 'Inters');

  static const TextStyle kTSNFS14W700 = TextStyle(
      color: Colors.white,
      fontSize: 14,
      fontWeight: FontWeight.w700,
      fontFamily: 'Inters');
  static const TextStyle kTSNFS14W400 = TextStyle(
      color: Colors.white,
      fontSize: 14,
      fontWeight: FontWeight.w400,
      fontFamily: 'Inters');

  static const TextStyle kTSNFS13 = TextStyle(
      color: Colors.white,
      fontSize: 13,
      fontWeight: FontWeight.normal,
      fontFamily: 'Inters');

  static const TextStyle kTSNFS13W400 = TextStyle(
      color: Colors.white,
      fontSize: 13,
      fontWeight: FontWeight.w400,
      fontFamily: 'Inters');

  static const TextStyle kTSNFS13W700 = TextStyle(
      color: Colors.white,
      fontSize: 13,
      fontWeight: FontWeight.w700,
      fontFamily: 'Inters');

  static const TextStyle kTSNFS18 = TextStyle(
      color: Colors.white,
      fontSize: 18,
      fontWeight: FontWeight.normal,
      fontFamily: 'Inters');

  static const TextStyle kTSFS16W400 = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w400, fontFamily: 'Inters');
      static const TextStyle kTSCF12W500 = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w500, fontFamily: 'Inters');

  static const TextStyle kTSFS16W600 = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w600, fontFamily: 'Inters');

  static const TextStyle kTSFS12W600 = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w600, fontFamily: 'Inters');

  static const TextStyle kTSFS12W400 = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w400, fontFamily: 'Inters');
  static const TextStyle kTSFS12WBOLD = TextStyle(
      fontSize: 12, fontWeight: FontWeight.bold, fontFamily: 'Inters',decoration: TextDecoration.underline);
  static const TextStyle kTSFS14W600 = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w600, fontFamily: 'Inters');
  static const TextStyle kTSFS12W500 = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w500, fontFamily: 'Inters');

  static const TextStyle kTSFS12W700 = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w700, fontFamily: 'Inters');

  static const TextStyle kTSFS12W = TextStyle(
      fontSize: 12, fontWeight: FontWeight.normal, fontFamily: 'Inters');
  static const TextStyle kTSWFS10W500 = TextStyle(
      color: Colors.white,
      fontSize: 10,
      fontWeight: FontWeight.w500,
      fontFamily: 'Inters');
  static const TextStyle kTSWFS10W600 = TextStyle(
      color: Colors.white,
      fontSize: 10,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');

  static const TextStyle kTSWFS10W700 = TextStyle(
      color: Colors.white,
      fontSize: 10,
      fontWeight: FontWeight.w700,
      fontFamily: 'Inters');

  static const TextStyle kTSWFS13W600 = TextStyle(
      color: Colors.white,
      fontSize: 13,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');

  static const TextStyle kTSFS14WNORM = TextStyle(
      color: primary,
      fontSize: 14,
      fontWeight: FontWeight.normal,
      fontFamily: 'Inters');

  static const TextStyle kTSFS15W = TextStyle(
      fontSize: 15, fontWeight: FontWeight.normal, fontFamily: 'Inters');
  static const TextStyle kTSFS15W600 = TextStyle(
      fontSize: 15, fontWeight: FontWeight.w600, fontFamily: 'Inters');

  static const TextStyle kTSFS16W700 = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w700, fontFamily: 'Inters');
  static const TextStyle kTSFS14W700 = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w700, fontFamily: 'Inters');

  static const TextStyle kTSFS18WBOLD = TextStyle(
      color: Colors.white,
      fontSize: 18,
      fontWeight: FontWeight.bold,
      fontFamily: 'Inters');

  static const TextStyle kTSFS18W600 = TextStyle(
      color: Colors.white,
      fontSize: 18,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');

  static const TextStyle kTSYFS18W500 = TextStyle(
      color: primary,
      fontSize: 15,
      fontWeight: FontWeight.w500,
      fontFamily: 'Inters');
  static const TextStyle kTSFS18W500 = TextStyle(
      color: primary,
      fontSize: 18,
      fontWeight: FontWeight.w500,
      fontFamily: 'Inters');
  //kTSFS14W600

  static const TextStyle kTSFS20W600 = TextStyle(
      color: Colors.white,
      fontSize: 20,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');
  static const TextStyle kTSFS20Blue700 = TextStyle(
      color: colorBlueDarkBtn,
      fontSize: 20,
      fontWeight: FontWeight.w700,
      fontFamily: 'Inters');
  static const TextStyle kTSFS20W700 = TextStyle(
      color: Colors.black,
      fontSize: 20,
      fontWeight: FontWeight.w700,
      fontFamily: 'Inters');

  static const TextStyle kTSFS20WBOLD = TextStyle(
      color: primary,
      fontSize: 20,
      fontWeight: FontWeight.bold,
      fontFamily: 'Inters');
  static const TextStyle kTSFS20W500 = TextStyle(
      color: Colors.white,
      fontSize: 20,
      fontWeight: FontWeight.w500,
      fontFamily: 'Inters');
  static const TextStyle kTSDS12W700 = TextStyle(
      color: addHMRTextColor,
      fontSize: 12,
      fontWeight: FontWeight.w700,
      fontFamily: 'Inters');
       static const TextStyle kTSDS12W7D00 = TextStyle(
      color: colorBlueDarkBtn,
      fontSize: 12,
      fontWeight: FontWeight.w700,
      fontFamily: 'Inters');
  static const TextStyle kTSDS14W500 = TextStyle(
      color: addServiceHintTextColor,
      fontSize: 14,
      fontWeight: FontWeight.w500,
      fontFamily: 'Inters');
  static const TextStyle kTSDS12W500 = TextStyle(
      color: addServiceTextColor,
      fontSize: 12,
      fontWeight: FontWeight.w500,
      fontFamily: 'Inters');
      
  static final TextStyle kTSDS14W700 = TextStyle(
      color: addServiceTextColor.withOpacity(1),
      fontSize: 14,
      fontWeight: FontWeight.w700,
      fontFamily: 'Inters');
      static final TextStyle kTSDS14W600 = TextStyle(
      color: addServiceTextColor.withOpacity(1),
      fontSize: 14,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');
  static final TextStyle kTSDS12W600 = TextStyle(
      color: addServiceTextColor.withOpacity(1),
      fontSize: 12,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');

  static const TextStyle kTSFS22W400 = TextStyle(
      color: Colors.white,
      fontSize: 22,
      fontWeight: FontWeight.w400,
      fontFamily: 'Inters');

  static const TextStyle kTSFS22W600 = TextStyle(
      color: Colors.white,
      fontSize: 22,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');
  static const TextStyle kTSFS24W600 = TextStyle(
      color: Colors.white,
      fontSize: 24,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');

  static const TextStyle kTSFS22W700 = TextStyle(
      color: Colors.white,
      fontSize: 22,
      fontWeight: FontWeight.w700,
      fontFamily: 'Inters');

  static const TextStyle kTSFS26W600 = TextStyle(
      color: Colors.white,
      fontSize: 26,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');

  static const TextStyle kTSFS26W800 = TextStyle(
      color: addServiceDropDownBtnColor,
      fontSize: 26,
      fontWeight: FontWeight.w800,
      fontFamily: 'Inters');
  static const TextStyle kTSFS26W400 = TextStyle(
      color: Colors.black,
      fontSize: 26,
      fontWeight: FontWeight.w400,
      fontFamily: 'Inters');

  static const TextStyle kTSFS30W600 = TextStyle(
      color: Colors.white,
      fontSize: 30,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');
      //CC413D
       static const TextStyle kTSFS36W600 = TextStyle(
      color: expenseCardOne,
      fontSize: 36,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');
  static const TextStyle kTSFS30W800 = TextStyle(
      color: Colors.white,
      fontSize: 30,
      fontWeight: FontWeight.w800,
      fontFamily: 'Inters');
  static const TextStyle kTSFS35WB = TextStyle(
      color: Colors.white,
      fontSize: 35,
      fontWeight: FontWeight.bold,
      fontFamily: 'Inters');

  static const TextStyle kTSFS10W400 = TextStyle(
      fontSize: 10, fontWeight: FontWeight.w400, fontFamily: 'Inters');

  static const TextStyle kTSFS10W500 = TextStyle(
      fontSize: 10, fontWeight: FontWeight.w500, fontFamily: 'Inters');

  static const TextStyle kTSFS10W600 = TextStyle(
      fontSize: 10, fontWeight: FontWeight.w600, fontFamily: 'Inters');

  static const TextStyle kTSFS11W600 = TextStyle(
      fontSize: 10, fontWeight: FontWeight.w600, fontFamily: 'Inters');

  static const TextStyle kTSNFS14W500 = TextStyle(
      color: Colors.white,
      fontSize: 14,
      fontWeight: FontWeight.w500,
      fontFamily: 'Inters');
  static const TextStyle kTSNFS14W600 = TextStyle(
      color: Colors.white,
      fontSize: 14,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');
  static const TextStyle kTSNFS12W600 = TextStyle(
      color: Colors.white,
      fontSize: 12,
      fontWeight: FontWeight.w600,
      fontFamily: 'Inters');
  static const TextStyle kTSNFS12W500 = TextStyle(
      color: Colors.white,
      fontSize: 12,
      fontWeight: FontWeight.w500,
      fontFamily: 'Inters');

  static const TextStyle kTSNFS17W900 = TextStyle(
      color: Colors.white,
      fontSize: 17,
      fontWeight: FontWeight.w900,
      fontFamily: 'Inters');

 static const TextStyle kTSNFS17W500 = TextStyle(
      color: Colors.white,
      fontSize: 17,
      fontWeight: FontWeight.w500,
      fontFamily: 'Inters');

  static const TextStyle kTSNFS17W400 = TextStyle(
      color: Colors.white,
      fontSize: 17,
      fontWeight: FontWeight.w400,
      fontFamily: 'Inters');

  static const TextStyle kTSNFS34W400 = TextStyle(
      color: Colors.white,
      fontSize: 34,
      fontWeight: FontWeight.w400,
      fontFamily: 'Inters');
}

class Borders {
  BorderSide borderSide = const BorderSide(
    color: Colors.white,
    width: 1,
  );
}
