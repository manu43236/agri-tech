import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../models/get_user_model.dart';
import '../../home/data/all_lands_of_landlord.dart';
import '../../home/services/get_user_repo_impl.dart';
import '../../home/services/home_repo_impl.dart';
import '../../saved/data/get_all_saved_landlord_model.dart';
import '../../saved/services/saved_repo_impl.dart';

class DetailsOfLandlordController extends BaseController {
  final LandlordController landlordController = Get.find<LandlordController>();

  Rx<GetUserModel> getUserProfileModel = Rx(GetUserModel());
  Rx<AddLandsOfLandlordModel> addLandsOfLandlordModels =
      Rx(AddLandsOfLandlordModel());
  Rx<GetAllSavedLandlordModel> getAllSavedLandlordModel =
      Rx(GetAllSavedLandlordModel());

  var userId = "".obs;
  var id="".obs;
  var ids=''.obs;

  Rx<List<SavedFarmer>> savedFarmers = Rx([]);
  var apiUserByProfileIdStatus=ApiStatus.LOADING.obs;
  var isLandlordSaved = false.obs;
  var deleteId = 0.obs;

  @override
  void onInit() async{
    ids.value= await SecureStorage().readData(key: "id"??'');
    if (Get.arguments != null) {
      userId.value = Get.arguments['userId'];
    }
    super.onInit();
    await getAllLandlordsSaved(userId: ids.value);
    await getUserByProfileId(id:userId.value);
    id.value= getUserProfileModel.value.result!.id.toString();
    addAllLandss(userId: id.value);
  }


    // Get user profile by id API
  Future<void> getUserByProfileId({id}) async {
    apiUserByProfileIdStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await GetUserRepoImpl(dioClient).getUser(id: id);
      print("======================== user by id ====================");
      print('Result $result');
      result.fold((model) {
        getUserProfileModel.value = model;

        print('model result ${getUserProfileModel.value}');

        
          isLandlordSaved.value = false;
          print('Landlord saved: ${isLandlordSaved.value}');
          
          final savedLandlords = 
                  getAllSavedLandlordModel
                  .value
                  .result
                  ?.savedFarmers ??
              [];
          print('saved landlords:${savedLandlords}');

          for (var element in savedLandlords) {
            print('vccccccccc');
            if (element.landlordid == getUserProfileModel.value.result?.id) {
              deleteId.value = element.savedId ?? 0;
              print('Delete ID for landlord: ${deleteId.value}');
              isLandlordSaved.value = true;
            }
          }

        apiUserByProfileIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiUserByProfileIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> addAllLandss({userId}) async {
   addLandsOfLandlordModels.value.result?.ongoingLands?.clear();
  apiStatus.value = ApiStatus.LOADING;
  try {
    var result = await HomeRepoImpl(dioClient).doAddAllLands(id: userId);
    print('Fetching lands API result...');
    result.fold((left) {
      allLandshandleResponses(left);
      apiStatus.value = ApiStatus.SUCCESS;
    }, (r) {
      apiStatus.value = ApiStatus.FAIL;
    });
  } catch (e) {
    print('Error: $e');
    apiStatus.value = ApiStatus.FAIL;
  }
}

  void allLandshandleResponses(AddLandsOfLandlordModel model) {
   if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
       addLandsOfLandlordModels.value.result?.ongoingLands?.clear();
       addLandsOfLandlordModels.value = model;
      print('77777777777777777777777');
      print( addLandsOfLandlordModels.value.result!.ongoingLands?[0].description);
    }
}


  //get all saved landlords API
  Future<void> getAllLandlordsSaved({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient).savedLandlord(userId: userId);
      print('=================get saved landlords=============');
      print(result);
      result.fold(
        (model) {
          getAllSavedLandlordModel.value = model;
          apiStatus.value = ApiStatus.SUCCESS;
        },(r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
