import 'package:get/get.dart';

import '../../saved_jobs/controllers/saved_jobs_controller.dart';
import '../controllers/detail_farmworker_jobs_controller.dart';

class DetailFarmworkerJobsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailFarmworkerJobsController>(
      () => DetailFarmworkerJobsController(),
    );
    Get.lazyPut<SavedJobsController>(
      () => SavedJobsController(),
    );
  }
}
