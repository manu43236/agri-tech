// To parse this JSON data, do
//
//     final getUtilityByIdModel = getUtilityByIdModelFromJson(jsonString);

import 'dart:convert';

GetUtilityByIdModel getUtilityByIdModelFromJson(String str) => GetUtilityByIdModel.fromJson(json.decode(str));

String getUtilityByIdModelToJson(GetUtilityByIdModel data) => json.encode(data.toJson());

class GetUtilityByIdModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetUtilityByIdModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetUtilityByIdModel.fromJson(Map<String, dynamic> json) => GetUtilityByIdModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    Utility? utility;
    Lease? lease;
    Leasetaker? leasetaker;
    Leasetaker? utiliser;
    ControllingData? controllingData;

    Result({
        this.utility,
        this.lease,
        this.leasetaker,
        this.utiliser,
        this.controllingData,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        utility: json["utility"] == null ? null : Utility.fromJson(json["utility"]),
        lease: json["lease"] == null ? null : Lease.fromJson(json["lease"]),
        leasetaker: json["leasetaker"] == null ? null : Leasetaker.fromJson(json["leasetaker"]),
        utiliser: json["utiliser"] == null ? null : Leasetaker.fromJson(json["utiliser"]),
        controllingData: json["controllingData"] == null ? null : ControllingData.fromJson(json["controllingData"]),
    );

    Map<String, dynamic> toJson() => {
        "utility": utility?.toJson(),
        "lease": lease?.toJson(),
        "leasetaker": leasetaker?.toJson(),
        "utiliser": utiliser?.toJson(),
        "controllingData": controllingData?.toJson(),
    };
}

class ControllingData {
    dynamic statusForLoan;
    dynamic statusForInsuranceAgent;

    ControllingData({
        this.statusForLoan,
        this.statusForInsuranceAgent,
    });

    factory ControllingData.fromJson(Map<String, dynamic> json) => ControllingData(
        statusForLoan: json["status_for_loan"],
        statusForInsuranceAgent: json["status_for_insurance_agent"],
    );

    Map<String, dynamic> toJson() => {
        "status_for_loan": statusForLoan,
        "status_for_insurance_agent": statusForInsuranceAgent,
    };
}

class Lease {
    int? id;
    dynamic landlord;
    int? farmer;
    dynamic farmWorker;
    int? utiliser;
    int? utilityid;
    dynamic landId;
    dynamic jobId;
    dynamic startingDate;
    dynamic endingDate;
    dynamic status;
    dynamic createdAt;
    dynamic updatedAt;

    Lease({
        this.id,
        this.landlord,
        this.farmer,
        this.farmWorker,
        this.utiliser,
        this.utilityid,
        this.landId,
        this.jobId,
        this.startingDate,
        this.endingDate,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Lease.fromJson(Map<String, dynamic> json) => Lease(
        id: json["id"],
        landlord: json["landlord"],
        farmer: json["farmer"],
        farmWorker: json["farmWorker"],
        utiliser: json["utiliser"],
        utilityid: json["utilityid"],
        landId: json["landId"],
        jobId: json["jobId"],
        startingDate: json["starting_date"],
        endingDate: json["ending_date"],
        status: json["status"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landlord": landlord,
        "farmer": farmer,
        "farmWorker": farmWorker,
        "utiliser": utiliser,
        "utilityid": utilityid,
        "landId": landId,
        "jobId": jobId,
        "starting_date": startingDate,
        "ending_date": endingDate,
        "status": status,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}

class Leasetaker {
    int? id;
    dynamic firstName;
    dynamic mobileNumber;
    dynamic role;
    dynamic age;
    dynamic gender;
    dynamic address;
    dynamic village;
    dynamic mandal;
    dynamic district;
    dynamic state;
    int? otp;
    dynamic longitude;
    dynamic latitude;
    dynamic accesstoken;
    int? profileId;
    dynamic imageUrl;
    dynamic description;
    dynamic locationDetails;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;

    Leasetaker({
        this.id,
        this.firstName,
        this.mobileNumber,
        this.role,
        this.age,
        this.gender,
        this.address,
        this.village,
        this.mandal,
        this.district,
        this.state,
        this.otp,
        this.longitude,
        this.latitude,
        this.accesstoken,
        this.profileId,
        this.imageUrl,
        this.description,
        this.locationDetails,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory Leasetaker.fromJson(Map<String, dynamic> json) => Leasetaker(
        id: json["id"],
        firstName: json["firstName"],
        mobileNumber: json["mobileNumber"],
        role: json["role"],
        age: json["age"],
        gender: json["gender"],
        address: json["address"],
        village: json["village"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        otp: json["otp"],
        longitude: json["longitude"]?.toDouble(),
        latitude: json["latitude"]?.toDouble(),
        accesstoken: json["accesstoken"],
        profileId: json["profileId"],
        imageUrl: json["image_url"],
        description: json["Description"],
        locationDetails: json["location_details"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "mobileNumber": mobileNumber,
        "role": role,
        "age": age,
        "gender": gender,
        "address": address,
        "village": village,
        "mandal": mandal,
        "district": district,
        "state": state,
        "otp": otp,
        "longitude": longitude,
        "latitude": latitude,
        "accesstoken": accesstoken,
        "profileId": profileId,
        "image_url": imageUrl,
        "Description": description,
        "location_details": locationDetails,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}

class Utility {
    int? id;
    int? utililityId;
    dynamic nameOfInstrument;
    dynamic pricePerDay;
    dynamic brandName;
    dynamic state;
    dynamic district;
    dynamic mandal;
    dynamic village;
    dynamic description;
    int? imageId;
    dynamic image;
    dynamic latitude;
    dynamic longitude;
    bool? status;
    dynamic createdAt;
    dynamic updatedAt;

    Utility({
        this.id,
        this.utililityId,
        this.nameOfInstrument,
        this.pricePerDay,
        this.brandName,
        this.state,
        this.district,
        this.mandal,
        this.village,
        this.description,
        this.imageId,
        this.image,
        this.latitude,
        this.longitude,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Utility.fromJson(Map<String, dynamic> json) => Utility(
        id: json["id"],
        utililityId: json["utililityId"],
        nameOfInstrument: json["nameOfInstrument"],
        pricePerDay: json["pricePerDay"],
        brandName: json["brandName"],
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
        description: json["description"],
        imageId: json["image_id"],
        image: json["image"],
        latitude: json["latitude"]?.toDouble(),
        longitude: json["longitude"]?.toDouble(),
        status: json["status"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "utililityId": utililityId,
        "nameOfInstrument": nameOfInstrument,
        "pricePerDay": pricePerDay,
        "brandName": brandName,
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
        "description": description,
        "image_id": imageId,
        "image": image,
        "latitude": latitude,
        "longitude": longitude,
        "status": status,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
