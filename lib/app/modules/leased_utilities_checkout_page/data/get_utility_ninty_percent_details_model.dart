// To parse this JSON data, do
//
//     final getUtilityNintyPercentDetailsModel = getUtilityNintyPercentDetailsModelFromJson(jsonString);

import 'dart:convert';

GetUtilityNintyPercentDetailsModel getUtilityNintyPercentDetailsModelFromJson(String str) => GetUtilityNintyPercentDetailsModel.fromJson(json.decode(str));

String getUtilityNintyPercentDetailsModelToJson(GetUtilityNintyPercentDetailsModel data) => json.encode(data.toJson());

class GetUtilityNintyPercentDetailsModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetUtilityNintyPercentDetailsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetUtilityNintyPercentDetailsModel.fromJson(Map<String, dynamic> json) => GetUtilityNintyPercentDetailsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    PaymentDetails? paymentDetails;
    UtilityDetails? utilityDetails;

    Result({
        this.paymentDetails,
        this.utilityDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        paymentDetails: json["paymentDetails"] == null ? null : PaymentDetails.fromJson(json["paymentDetails"]),
        utilityDetails: json["utilityDetails"] == null ? null : UtilityDetails.fromJson(json["utilityDetails"]),
    );

    Map<String, dynamic> toJson() => {
        "paymentDetails": paymentDetails?.toJson(),
        "utilityDetails": utilityDetails?.toJson(),
    };
}

class PaymentDetails {
    int? id;
    int? userId;
    int? utilityId;
    dynamic quantity;
    dynamic costOfSelectedItem;
    dynamic tenPercentGst;
    dynamic nintyPercentGst;
    dynamic tenPercentPayment;
    dynamic nintyPercentpayment;
    dynamic totalPayment;
    dynamic paymentStatus;
    dynamic amountPaid;
    dynamic tenPercentPrice;
    dynamic ninetyPercentPrice;
    dynamic razorpayOrderId;
    dynamic razorpayPaymentId;
    dynamic razorpaySignature;
    dynamic createdAt;
    dynamic updatedAt;

    PaymentDetails({
        this.id,
        this.userId,
        this.utilityId,
        this.quantity,
        this.costOfSelectedItem,
        this.tenPercentGst,
        this.nintyPercentGst,
        this.tenPercentPayment,
        this.nintyPercentpayment,
        this.totalPayment,
        this.paymentStatus,
        this.amountPaid,
        this.tenPercentPrice,
        this.ninetyPercentPrice,
        this.razorpayOrderId,
        this.razorpayPaymentId,
        this.razorpaySignature,
        this.createdAt,
        this.updatedAt,
    });

    factory PaymentDetails.fromJson(Map<String, dynamic> json) => PaymentDetails(
        id: json["id"],
        userId: json["userId"],
        utilityId: json["utilityId"],
        quantity: json["quantity"],
        costOfSelectedItem: json["costOfSelectedItem"],
        tenPercentGst: json["tenPercentGST"]?.toDouble(),
        nintyPercentGst: json["nintyPercentGST"]?.toDouble(),
        tenPercentPayment: json["tenPercentPayment"]?.toDouble(),
        nintyPercentpayment: json["nintyPercentpayment"]?.toDouble(),
        totalPayment: json["totalPayment"],
        paymentStatus: json["paymentStatus"],
        amountPaid: json["amountPaid"]?.toDouble(),
        tenPercentPrice: json["tenPercentPrice"]?.toDouble(),
        ninetyPercentPrice: json["ninetyPercentPrice"]?.toDouble(),
        razorpayOrderId: json["razorpay_order_id"],
        razorpayPaymentId: json["razorpay_payment_id"],
        razorpaySignature: json["razorpay_signature"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "utilityId": utilityId,
        "quantity": quantity,
        "costOfSelectedItem": costOfSelectedItem,
        "tenPercentGST": tenPercentGst,
        "nintyPercentGST": nintyPercentGst,
        "tenPercentPayment": tenPercentPayment,
        "nintyPercentpayment": nintyPercentpayment,
        "totalPayment": totalPayment,
        "paymentStatus": paymentStatus,
        "amountPaid": amountPaid,
        "tenPercentPrice": tenPercentPrice,
        "ninetyPercentPrice": ninetyPercentPrice,
        "razorpay_order_id": razorpayOrderId,
        "razorpay_payment_id": razorpayPaymentId,
        "razorpay_signature": razorpaySignature,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}

class UtilityDetails {
    int? id;
    dynamic name;
    dynamic description;
    dynamic pricePerDay;
    dynamic image;
    Address? address;

    UtilityDetails({
        this.id,
        this.name,
        this.description,
        this.pricePerDay,
        this.image,
        this.address,
    });

    factory UtilityDetails.fromJson(Map<String, dynamic> json) => UtilityDetails(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        pricePerDay: json["pricePerDay"],
        image: json["image"],
        address: json["address"] == null ? null : Address.fromJson(json["address"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "pricePerDay": pricePerDay,
        "image": image,
        "address": address?.toJson(),
    };
}

class Address {
    dynamic state;
    dynamic district;
    dynamic mandal;
    dynamic village;

    Address({
        this.state,
        this.district,
        this.mandal,
        this.village,
    });

    factory Address.fromJson(Map<String, dynamic> json) => Address(
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
    );

    Map<String, dynamic> toJson() => {
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
    };
}
