import 'package:agritech/app/modules/add_job/services/add_job_by_farmer_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import '../../../../models/add_job_by_farmer_model.dart';

class AddJobByFarmerRepoImpl extends AddJobByFarmerRepo with NetworkCheckService{
  final DioClient _dioClient;
  AddJobByFarmerRepoImpl(this._dioClient);
  @override
  Future<Either<AddJobByFarmerModel,Exception>>  doAddFarmerJob({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Jobs/addJobByFarmer', Method.post,params: params1);
       return result.fold((l) {
          AddJobByFarmerModel model = AddJobByFarmerModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}