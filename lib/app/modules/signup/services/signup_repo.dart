import 'package:agritech/models/register_model.dart';
import 'package:dartz/dartz.dart';

abstract class SignUpRepo {
  Future<Either<RegisterModel, Exception>> doSignup({params1});
}
