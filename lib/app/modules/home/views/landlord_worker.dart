import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/home/controllers/landlord_worker_controller.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../../../../core/utils/widget_utils/text_fields/custom_search_widget.dart';

class LandlordWorker extends GetView<LandlordWorkerController> {
  const LandlordWorker({super.key});

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    final imageSize = Get.width * 0.15;
    final innerCircleSize = Get.width * 0.14;

    return RefreshIndicator(
      onRefresh: () async {
        controller.landlordWorkerSearch.value = '';
        controller.filterFarmworkers(latitude:Get.find<HomeController>().latitude.value,longitude:Get.find<HomeController>().longitude.value,search:controller.landlordWorkerSearch.value);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Text(
              'available_farmWorkers'.tr,
              style: TextStyles.kTSFS14W600.copyWith(
                color: colorHello,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: CustomSearchField(
              onChanged: (val) {
                controller.landlordWorkerSearch.value = val;
                if (val.length >= 3) {
                  controller.filterFarmworkers(latitude:Get.find<HomeController>().latitude.value,longitude:Get.find<HomeController>().longitude.value,search:controller.landlordWorkerSearch.value);
                } else if (val.isEmpty) {
                  controller.filterFarmworkers(latitude:Get.find<HomeController>().latitude.value,longitude:Get.find<HomeController>().longitude.value,search:controller.landlordWorkerSearch.value);
                }
              },
            ),
          ),
          Expanded(
            child: Obx(() {
              final farmers = controller
                      .landlordFarmWorkerModel.value.result?.farmworkers ??
                  [];
              return controller.apiStatus.value == ApiStatus.LOADING
                  ? Shimmers().getListShimmer()
                  : farmers.isNotEmpty
                      ? ListView.builder(
                          itemCount: farmers.length,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          itemBuilder: (context, index) {
                            final farmer = farmers[index];
                            return InkWell(
                              onTap: () async {
                                Get.find<HomeController>().farmWorkerId.value =
                                    controller.landlordFarmWorkerModel.value
                                        .result!.farmworkers![index].id
                                        .toString();
                                
                                Get.toNamed(Routes.DETAILS_OF_FARMWORKER,arguments: {"workerId":Get.find<HomeController>().farmWorkerId.value});
                              },
                              child: Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 10),
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(19),
                                  boxShadow: [
                                    BoxShadow(
                                      color: colorBlack.withOpacity(0.2),
                                      blurRadius: 5,
                                      spreadRadius: 1,
                                      offset: const Offset(0, 3),
                                      blurStyle: BlurStyle.inner,
                                    ),
                                  ],
                                  color: colorWhite,
                                ),
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 20),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      // Image Section
                                      Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          Container(
                                            height: imageSize,
                                            width: imageSize,
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: colorCircleWorker,
                                            ),
                                          ),
                                          Positioned(
                                            left: 2,
                                            child: Container(
                                              height: innerCircleSize,
                                              width: innerCircleSize,
                                              decoration: const BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: colorWhite,
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            child: ClipOval(
                                              child: Image.network(
                                                farmer.imageUrl ??
                                                    defaultImageUrl,
                                                height: imageSize - 5,
                                                width: imageSize - 5,
                                                fit: BoxFit.cover,
                                                errorBuilder: (context, error,
                                                    stackTrace) {
                                                  return Image.asset(
                                                    defaultImageUrl,
                                                    height: imageSize - 5,
                                                    width: imageSize - 5,
                                                    fit: BoxFit.cover,
                                                  );
                                                },
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(width: 20,),
                                      // Text Column Section
                                      Expanded(
                                        flex: 2,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              farmer.firstName ?? 'N/A',
                                              style: TextStyles.kTSCF12W500,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            Text(
                                              farmer.age?.toString() ?? 'N/A',
                                              style: TextStyles.kTSCF12W500,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            Text(
                                              farmer.address ?? '-',
                                              style: TextStyles.kTSCF12W500,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(width: 20,),
                                      // Skills Section
                                      Expanded(
                                        flex: 1,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              farmer.skills != null &&
                                                      farmer.skills!.isNotEmpty
                                                  ? farmer.skills!.first.skill ??
                                                      'N/A'
                                                  : '',
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyles.kTSCF12W500,
                                            ),
                                            const SizedBox(height: 2),
                                            Text(
                                              farmer.skills != null &&
                                                      farmer.skills!.length > 1
                                                  ? (farmer.skills![1].skill ??
                                                      'N/A')
                                                  : '',
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyles.kTSCF12W500,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        )
                      : Align(
                          alignment: Alignment.center,
                          child: Text(
                            'no_farm_workers_found'.tr,
                            style: TextStyles.kTSDS12W500,
                          ),
                        );
            }),
          ),
        ],
      ),
    );
  }
}
