// To parse this JSON data, do
//
//     final createCropPaymentModel = createCropPaymentModelFromJson(jsonString);

import 'dart:convert';

CreateCropPaymentModel createCropPaymentModelFromJson(String str) => CreateCropPaymentModel.fromJson(json.decode(str));

String createCropPaymentModelToJson(CreateCropPaymentModel data) => json.encode(data.toJson());

class CreateCropPaymentModel {
    dynamic statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    CreateCropPaymentModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory CreateCropPaymentModel.fromJson(Map<String, dynamic> json) => CreateCropPaymentModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    int? cropId;
    int? landId;
    dynamic hectors;
    dynamic remainingQuantity;
    dynamic perHectorcost;
    dynamic tenPercentPrice;
    dynamic tenPercentGst;
    dynamic tenPercentPayment;
    dynamic ninetyPercentPrice;
    dynamic nintyPercentGst;
    dynamic nintyPercentpayment;
    dynamic cropImageUrl;
    LandDetails? landDetails;

    Result({
        this.id,
        this.cropId,
        this.landId,
        this.hectors,
        this.remainingQuantity,
        this.perHectorcost,
        this.tenPercentPrice,
        this.tenPercentGst,
        this.tenPercentPayment,
        this.ninetyPercentPrice,
        this.nintyPercentGst,
        this.nintyPercentpayment,
        this.cropImageUrl,
        this.landDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        cropId: json["cropId"],
        landId: json["landId"],
        hectors: json["hectors"],
        remainingQuantity: json["remainingQuantity"],
        perHectorcost: json["perHectorcost"],
        tenPercentPrice: json["tenPercentPrice"],
        tenPercentGst: json["tenPercentGST"],
        tenPercentPayment: json["tenPercentPayment"],
        ninetyPercentPrice: json["ninetyPercentPrice"],
        nintyPercentGst: json["nintyPercentGST"],
        nintyPercentpayment: json["nintyPercentpayment"],
        cropImageUrl: json["cropImageUrl"],
        landDetails: json["landDetails"] == null ? null : LandDetails.fromJson(json["landDetails"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "cropId": cropId,
        "landId": landId,
        "hectors": hectors,
        "remainingQuantity": remainingQuantity,
        "perHectorcost": perHectorcost,
        "tenPercentPrice": tenPercentPrice,
        "tenPercentGST": tenPercentGst,
        "tenPercentPayment": tenPercentPayment,
        "ninetyPercentPrice": ninetyPercentPrice,
        "nintyPercentGST": nintyPercentGst,
        "nintyPercentpayment": nintyPercentpayment,
        "cropImageUrl": cropImageUrl,
        "landDetails": landDetails?.toJson(),
    };
}

class LandDetails {
    int? id;
    int? landLordId;
    int? farmerId;
    dynamic fullName;
    dynamic landInAcres;
    dynamic surveyNum;
    dynamic water;
    dynamic village;
    dynamic description;
    dynamic mandal;
    dynamic district;
    dynamic state;
    dynamic latitude;
    dynamic longitude;
    dynamic radius;
    dynamic imageId;
    dynamic imageUrl;
    dynamic status;
    bool? flag;
    DateTime? createdAt;
    DateTime? updatedAt;

    LandDetails({
        this.id,
        this.landLordId,
        this.farmerId,
        this.fullName,
        this.landInAcres,
        this.surveyNum,
        this.water,
        this.village,
        this.description,
        this.mandal,
        this.district,
        this.state,
        this.latitude,
        this.longitude,
        this.radius,
        this.imageId,
        this.imageUrl,
        this.status,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory LandDetails.fromJson(Map<String, dynamic> json) => LandDetails(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        landInAcres: json["landInAcres"],
        surveyNum: json["surveyNum"],
        water: json["water"],
        village: json["village"],
        description: json["description"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        radius: json["radius"],
        imageId: json["image_id"],
        imageUrl: json["image_url"],
        status: json["status"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "fullName": fullName,
        "landInAcres": landInAcres,
        "surveyNum": surveyNum,
        "water": water,
        "village": village,
        "description": description,
        "mandal": mandal,
        "district": district,
        "state": state,
        "latitude": latitude,
        "longitude": longitude,
        "radius": radius,
        "image_id": imageId,
        "image_url": imageUrl,
        "status": status,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
