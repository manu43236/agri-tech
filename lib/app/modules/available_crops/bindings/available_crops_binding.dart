import 'package:get/get.dart';

import '../controllers/available_crops_controller.dart';

class AvailableCropsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailableCropsController>(
      () => AvailableCropsController(),
    );
  }
}
