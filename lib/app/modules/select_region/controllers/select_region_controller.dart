import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:get/get.dart';
import '../data/district_model.dart';
import '../data/mandal_model.dart';
import '../data/state_model.dart';
import '../data/village_model.dart';
import '../services/select_region_repo_impl.dart';

class SelectRegionController extends BaseController {
  List<String> items = ['state'.tr, 'district'.tr, 'mandal'.tr, 'village'.tr];
  var selectedCategory = 'state'.tr.obs;

  var selectedMandal = ''.obs;
  var selectedVillage = ''.obs;
  var selectedDistricts = ''.obs;
  var selectedStates = ''.obs;

  Rx<List<StateResult>> stateResult = Rx([]);
  Rx<List<DistrictResult>> districtResult = Rx([]);
  Rx<List<MandalResult>> mandalResult = Rx([]);
  Rx<List<VillageResult>> villageResult = Rx([]);

  var selectedDistrictIndex = (-1).obs;
  var selectedMandalIndex = (-1).obs;
  var selectedVillageIndex = (-1).obs;

  var stateName = "".obs;
  var districtName = "".obs;
  var mandalName = "".obs;
  var villageName = "".obs;

  var apiStateStatus = ApiStatus.LOADING.obs;
  var apiDistrictStatus = ApiStatus.LOADING.obs;
  var apiMandalStatus = ApiStatus.LOADING.obs;
  var apiVillageStatus = ApiStatus.LOADING.obs;
  var type = ''.obs;

  var selectedItem = ''.obs;
  var selectedLang = ''.obs;

  @override
  void onInit() async {
    super.onInit();
    selectedLang.value = await SecureStorage().readData(key: "lang");
    await selectState(lang: selectedLang.value);
  }

  String getSelectedRegionName() {
    if (selectedVillage.value.isNotEmpty) {
      return selectedVillage.value;
    } else if (selectedMandal.value.isNotEmpty) {
      return selectedMandal.value;
    } else if (selectedDistricts.value.isNotEmpty) {
      return selectedDistricts.value;
    } else if (selectedStates.value.isNotEmpty) {
      return selectedStates.value;
    } else {
      return 'None'; // Fallback if no region is selected
    }
  }

  //State API
  Future<void> selectState({lang}) async {
    apiStateStatus.value = ApiStatus.LOADING;
    try {
      var result = await SelectRegionRepoImpl(dioClient).getState(lang: lang);
      result.fold((left) {
        stateResult.value.clear();
        for (var element in left.result!) {
          stateResult.value.add(element);
        }
        apiStateStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStateStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

//District API Call
  Future<void> selectDistrict({stateId, lang}) async {
    apiDistrictStatus.value = ApiStatus.LOADING;
    try {
      var result = await SelectRegionRepoImpl(dioClient)
          .getDistrict(id: stateId, lang: lang);
      print("========================district====================");
      print(result);
      result.fold((left) {
        districtResult.value.clear();
        selectedDistrictIndex.value = -1;
        for (var element in left.result!) {
          districtResult.value.add(element);
        }
        selectedCategory.value = 'dis'.tr;
        apiDistrictStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiDistrictStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

//Mandal API
  Future<void> selectMandal({districtId, lang}) async {
    apiMandalStatus.value = ApiStatus.LOADING;
    try {
      var result = await SelectRegionRepoImpl(dioClient)
          .getMandal(id: districtId, lang: lang);
      print("========================mandal====================");
      print(result);
      result.fold((left) {
        mandalResult.value.clear();
        selectedMandalIndex.value = -1;
        for (var element in left.result!) {
          mandalResult.value.add(element);
        }
        selectedCategory.value = 'man'.tr;
        apiMandalStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiMandalStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //Villages API

  Future<void> selectVillage({mandalId, lang}) async {
    apiVillageStatus.value = ApiStatus.LOADING;
    try {
      var result = await SelectRegionRepoImpl(dioClient)
          .getVillage(id: mandalId, lang: lang);
      print("========================village====================");
      print(result);
      result.fold((left) {
        if (left.statusCode == 200) {
          villageResult.value.clear();
          selectedVillageIndex.value = -1;
          for (var element in left.result!) {
            villageResult.value.add(element);
          }
        } else if (left.statusCode == 400) {
          villageResult.value.clear();
          print(villageResult.value);
          print("No villages data found");
        }
        selectedCategory.value = 'vill'.tr;
        apiVillageStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiVillageStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void onItemSelected(String category, dynamic item) async {
    selectedItem.value = item.id.toString();
    print('Selected item: ${selectedItem.value}');
    print('Selected name: ${item.name.toString()}');
    print('Selected category: ${category.tr.trim().toLowerCase()}');

    // Normalize the category for comparison
    String normalizedCategory = category.tr.trim().toLowerCase();

    switch (normalizedCategory) {
      case 'state': // English
      case 'రాష్ట్రం': // Telugu for 'state'
        selectedStates.value = item.name;
        print('Selected State: ${selectedStates.value}');
        await selectDistrict(
            stateId: item.id.toString(), lang: selectedLang.value);
        break;

      case 'district': // English
      case 'జిల్లా': // Telugu for 'district'
        selectedDistricts.value = item.name;
        print('Selected District: ${selectedDistricts.value}');
        await selectMandal(
            districtId: item.id.toString(), lang: selectedLang.value);
        break;

      case 'mandal': // English
      case 'మండలం': // Telugu for 'mandal'
        selectedMandal.value = item.name;
        print('Selected Mandal: ${selectedMandal.value}');
        await selectVillage(
            mandalId: item.id.toString(), lang: selectedLang.value);
        break;

      case 'village': // English
      case 'గ్రామం': // Telugu for 'village'
        selectedVillage.value = item.name;
        print('Selected Village: ${selectedVillage.value}');
        break;

      default:
        print('No match for category: $normalizedCategory');
        break;
    }
  }

  // void onItemSelected(String category, dynamic item) async{
  //   selectedItem.value = item.id.toString();
  //   print('selected item${selectedItem.value}');
  //   print('selected name:${item.name.toString()}');
  //   print('selected category:${category.tr.trim().toLowerCase()}');

  //   switch (category.tr.trim().toLowerCase()) {
  //     case 'state':
  //       selectedStates.value = item.name;
  //       print(selectedStates.value);
  //       await selectDistrict(stateId: item.id.toString(),lang: selectedLang.value);
  //       //selectedCategory.value = 'district';
  //       break;
  //     case 'district':
  //       selectedDistricts.value = item.name;
  //       await selectMandal(districtId: item.id.toString(),lang: selectedLang.value);
  //       //selectedCategory.value = 'mandal';
  //       break;
  //     case 'mandal':
  //       selectedMandal.value = item.name;
  //       await selectVillage(mandalId: item.id.toString(),lang: selectedLang.value);
  //       //selectedCategory.value = 'village';
  //       break;
  //     case 'village':
  //       selectedVillage.value = item.name;
  //       break;
  //   }
  // }

  void clearAll() {
    selectedStates.value = '';
    selectedDistricts.value = '';
    selectedMandal.value = '';
    selectedVillage.value = '';

    districtResult.value.clear();
    mandalResult.value.clear();
    villageResult.value.clear();

    selectedCategory.value = 'sta'.tr; // Highlight "State"
    selectState(); // Reload the states list
  }
}
