import 'package:get/get.dart';

import '../../notifications/controllers/notifications_controller.dart';
import '../../saved_farmworkers/controllers/saved_farmworkers_controller.dart';
import '../controllers/details_of_farmworker_controller.dart';

class DetailsOfFarmworkerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailsOfFarmworkerController>(
      () => DetailsOfFarmworkerController(),
    );
    Get.lazyPut<NotificationsController>(
      () => NotificationsController(),
    );
     Get.lazyPut<SavedFarmworkersController>(
      ()=>SavedFarmworkersController()
    );
  }
}
