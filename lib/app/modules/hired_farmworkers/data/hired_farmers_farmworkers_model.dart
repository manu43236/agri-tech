// To parse this JSON data, do
//
//     final hiredFarmersFarmworkersModel = hiredFarmersFarmworkersModelFromJson(jsonString);

import 'dart:convert';

HiredFarmersFarmworkersModel hiredFarmersFarmworkersModelFromJson(String str) => HiredFarmersFarmworkersModel.fromJson(json.decode(str));

String hiredFarmersFarmworkersModelToJson(HiredFarmersFarmworkersModel data) => json.encode(data.toJson());

class HiredFarmersFarmworkersModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    HiredFarmersFarmworkersModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory HiredFarmersFarmworkersModel.fromJson(Map<String, dynamic> json) => HiredFarmersFarmworkersModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<Farm>? farmers;
    List<Farm>? farmworkers;

    Result({
        this.farmers,
        this.farmworkers,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        farmers: json["farmers"] == null ? [] : List<Farm>.from(json["farmers"]!.map((x) => Farm.fromJson(x))),
        farmworkers: json["farmworkers"] == null ? [] : List<Farm>.from(json["farmworkers"]!.map((x) => Farm.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "farmers": farmers == null ? [] : List<dynamic>.from(farmers!.map((x) => x.toJson())),
        "farmworkers": farmworkers == null ? [] : List<dynamic>.from(farmworkers!.map((x) => x.toJson())),
    };
}

class Farm {
    int? id;
    dynamic firstName;
    dynamic role;
    dynamic age;
    dynamic address;
    dynamic imageUrl;

    Farm({
        this.id,
        this.firstName,
        this.role,
        this.age,
        this.address,
        this.imageUrl,
    });

    factory Farm.fromJson(Map<String, dynamic> json) => Farm(
        id: json["id"],
        firstName: json["firstName"],
        role: json["role"],
        age: json["age"],
        address: json["address"],
        imageUrl: json["image_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "role": role,
        "age": age,
        "address": address,
        "image_url": imageUrl,
    };
}
