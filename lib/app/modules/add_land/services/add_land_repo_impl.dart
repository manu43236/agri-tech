import 'package:agritech/app/modules/add_land/services/add_land_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:agritech/models/add_land_model.dart';
import 'package:dartz/dartz.dart';

class AddLandRepoImpl extends AddLandRepo with NetworkCheckService{

  final DioClient _dioClient;
  AddLandRepoImpl(this._dioClient);

  @override
  Future<Either<AddLandModel,Exception>> doAddLand({params1})async{
    bool data=await checkInternet();
    if(!data){
      return Right(Exception("No Internet found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Land/addLand', Method.post,params: params1);
        return result.fold((l) {
          AddLandModel model = AddLandModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}