import 'package:dartz/dartz.dart';

import '../../home/data/all_jobs_of_landlord_model.dart';
import '../data/jobs_filter_model.dart';

abstract class AvailableMyJobsRepo{
  Future<Either<AllJobsOfLandlordModel,Exception>> getJobSearch();
  Future<Either<JobsFilterModel,Exception>> filterOwnJobs();
}