import 'package:get/get.dart';

import '../controllers/vendor_payment_successful_controller.dart';

class VendorPaymentSuccessfulBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VendorPaymentSuccessfulController>(
      () => VendorPaymentSuccessfulController(),
    );
  }
}
