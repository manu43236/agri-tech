import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../models/job_by_id_model.dart';
import '../../home/services/job_by_id_repo_impl.dart';

class DetailLeasedJobsController extends BaseController {

  Rx<JobByIdModel> jobByIdModel = Rx(JobByIdModel());

  var jobId = "".obs;
  var apiJobIdStatus = ApiStatus.LOADING.obs;
  var isFarmworkerFavourite = false.obs;
  var deleteId = 0.obs;
  var id=''.obs;

  @override
  void onInit() async{
    super.onInit();
    if(Get.arguments!=null){
      await getIdJobs(id: Get.arguments["jobId"]);
    }
  }

  // jobs by id API
  Future<void> getIdJobs({id}) async {
    apiJobIdStatus.value = ApiStatus.LOADING;
    try {
      var result = await JobByIdRepoImpl(dioClient).getJob(id: id);

      result.fold((left) {
        eachJobhandleResponse(left);
        apiJobIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiJobIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void eachJobhandleResponse(JobByIdModel model) async {
    if (model.statusCode == 200) {
      jobByIdModel.value = model;
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
