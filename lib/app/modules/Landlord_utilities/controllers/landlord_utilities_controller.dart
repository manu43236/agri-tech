import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../models/get_recent_four_utilities_model.dart';
import '../data/filter_utilities_by_location_model.dart';
import '../services/landlord_utility_repo_impl.dart';

class LandlordUtilitiesController extends BaseController {

  Rx<GetRecentFourUtilitiesModel> getRecentSearchFourUtilitiesModel = Rx(GetRecentFourUtilitiesModel());
  Rx<FilterUtilitiesByLocationModel> filterUtilitiesByLocationModel = Rx(FilterUtilitiesByLocationModel());


  var utilitySearch=''.obs;

  @override
  void onInit() {
    super.onInit();
    filterUtilitiesByLocation(latitude: Get.find<HomeController>().latitude.value,longitude: Get.find<HomeController>().longitude.value);
  }

  Future<void> recentUtilities() async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result = await LandlordUtilityRepoImpl(dioClient).recentUtilities();
      print('=============== get four recent utilities =========');
      print(result);
      result.fold((model) {
        getRecentSearchFourUtilitiesModel.value=model;
        print(getRecentSearchFourUtilitiesModel.value.result!.first.brandName);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> filterUtilitiesByLocation({latitude,longitude}) async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result = await LandlordUtilityRepoImpl(dioClient).filterUtilityLocation(params1: {
        "latitude": latitude,
        "longitude": longitude,
        "search":utilitySearch.value
      });
      print('=============== filter utilities by location  =========');
      print(result);
      result.fold((model) {
        filterUtilitiesByLocationModel.value=model;
        print(filterUtilitiesByLocationModel.value.result!.first.brandName);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
