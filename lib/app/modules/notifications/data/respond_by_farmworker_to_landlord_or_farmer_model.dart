// To parse this JSON data, do
//
//     final respondByFarmworkerToLandlordOrFarmerModel = respondByFarmworkerToLandlordOrFarmerModelFromJson(jsonString);

import 'dart:convert';

RespondByFarmworkerToLandlordOrFarmerModel respondByFarmworkerToLandlordOrFarmerModelFromJson(String str) => RespondByFarmworkerToLandlordOrFarmerModel.fromJson(json.decode(str));

String respondByFarmworkerToLandlordOrFarmerModelToJson(RespondByFarmworkerToLandlordOrFarmerModel data) => json.encode(data.toJson());

class RespondByFarmworkerToLandlordOrFarmerModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    RespondByFarmworkerToLandlordOrFarmerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory RespondByFarmworkerToLandlordOrFarmerModel.fromJson(Map<String, dynamic> json) => RespondByFarmworkerToLandlordOrFarmerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    Notification? notification;
    dynamic senderName;

    Result({
        this.notification,
        this.senderName,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        notification: json["notification"] == null ? null : Notification.fromJson(json["notification"]),
        senderName: json["senderName"],
    );

    Map<String, dynamic> toJson() => {
        "notification": notification?.toJson(),
        "senderName": senderName,
    };
}

class Notification {
    bool? isRead;
    int? id;
    dynamic name;
    dynamic message;
    dynamic from;
    dynamic to;
    int? jobId;
    dynamic statusForLandlord;
    dynamic statusForFarmer;
    dynamic imageUrl;
    bool? flag;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic landId;
    dynamic statusForFarmworker;

    Notification({
        this.isRead,
        this.id,
        this.name,
        this.message,
        this.from,
        this.to,
        this.jobId,
        this.statusForLandlord,
        this.statusForFarmer,
        this.imageUrl,
        this.flag,
        this.updatedAt,
        this.createdAt,
        this.landId,
        this.statusForFarmworker,
    });

    factory Notification.fromJson(Map<String, dynamic> json) => Notification(
        isRead: json["is_read"],
        id: json["id"],
        name: json["name"],
        message: json["message"],
        from: json["from"],
        to: json["to"],
        jobId: json["jobId"],
        statusForLandlord: json["status_for_landlord"],
        statusForFarmer: json["status_for_farmer"],
        imageUrl: json["image_url"],
        flag: json["flag"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        landId: json["landId"],
        statusForFarmworker: json["status_for_farmworker"],
    );

    Map<String, dynamic> toJson() => {
        "is_read": isRead,
        "id": id,
        "name": name,
        "message": message,
        "from": from,
        "to": to,
        "jobId": jobId,
        "status_for_landlord": statusForLandlord,
        "status_for_farmer": statusForFarmer,
        "image_url": imageUrl,
        "flag": flag,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "landId": landId,
        "status_for_farmworker": statusForFarmworker,
    };
}
