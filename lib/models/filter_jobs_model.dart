// To parse this JSON data, do
//
//     final filterJobsModel = filterJobsModelFromJson(jsonString);

import 'dart:convert';

FilterJobsModel filterJobsModelFromJson(String str) => FilterJobsModel.fromJson(json.decode(str));

String filterJobsModelToJson(FilterJobsModel data) => json.encode(data.toJson());

class FilterJobsModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<Result>? result;

    FilterJobsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory FilterJobsModel.fromJson(Map<String, dynamic> json) => FilterJobsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    Job? job;

    Result({
        this.job,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        job: json["job"] == null ? null : Job.fromJson(json["job"]),
    );

    Map<String, dynamic> toJson() => {
        "job": job?.toJson(),
    };
}

class Job {
    int? id;
    dynamic landLordId;
    int? farmerId;
    dynamic fullName;
    dynamic startDate;
    dynamic endDate;
    dynamic latitude;
    dynamic longitude;
    dynamic state;
    dynamic district;
    dynamic village;
    dynamic mandal;
    dynamic description;
    int? imageId;
    dynamic image;
    dynamic status;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;

    Job({
        this.id,
        this.landLordId,
        this.farmerId,
        this.fullName,
        this.startDate,
        this.endDate,
        this.latitude,
        this.longitude,
        this.state,
        this.district,
        this.village,
        this.mandal,
        this.description,
        this.imageId,
        this.image,
        this.status,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory Job.fromJson(Map<String, dynamic> json) => Job(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        startDate: json["startDate"] == null ? null : DateTime.parse(json["startDate"]),
        endDate: json["endDate"] == null ? null : DateTime.parse(json["endDate"]),
        latitude: json["latitude"],
        longitude: json["longitude"],
        state: json["state"],
        district: json["district"],
        village: json["village"],
        mandal: json["mandal"],
        description: json["description"],
        imageId: json["image_id"],
        image: json["image"],
        status: json["status"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "fullName": fullName,
        "startDate": startDate?.toIso8601String(),
        "endDate": endDate?.toIso8601String(),
        "latitude": latitude,
        "longitude": longitude,
        "state": state,
        "district": district,
        "village": village,
        "mandal": mandal,
        "description": description,
        "image_id": imageId,
        "image": image,
        "status": status,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
