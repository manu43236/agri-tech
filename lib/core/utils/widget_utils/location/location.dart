// import 'package:flutter/material.dart';
// import 'package:geocoding/geocoding.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:get/get.dart';

// import '../../../../app/modules/home/controllers/home_controller.dart';

// class Location {
//   double latitude = 0.0;
//   double longitude = 0.0;
//   String address = '';
//   bool isLoading = true;

//   var id=0.obs;

//   Future<void> determinePosition() async {
//     bool serviceEnabled;
//     LocationPermission permission;

//     // Check if location services are enabled
//     serviceEnabled = await Geolocator.isLocationServiceEnabled();
//     if (!serviceEnabled) {
//       _showLocationDialog();
//       return Future.error('Location services are disabled.');
//     }

//     // Check for location permissions
//     permission = await Geolocator.checkPermission();
//     if (permission == LocationPermission.denied) {
//       permission = await Geolocator.requestPermission();
//       if (permission == LocationPermission.denied) {
//         return Future.error('Location permissions are denied');
//       }
//     }

//     if (permission == LocationPermission.deniedForever) {
//       return Future.error('Location permissions are permanently denied.');
//     }

//     // Attempt to use the last known location if available for faster results
//     Position? lastKnownPosition = await Geolocator.getLastKnownPosition();
//     if (lastKnownPosition != null) {
//       latitude = lastKnownPosition.latitude;
//       longitude = lastKnownPosition.longitude;
//       print('Using last known location: $latitude, $longitude');
//     } else {
//       // Get the current location of the device with medium accuracy for faster results
//       Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.medium);
//       latitude = position.latitude;
//       longitude = position.longitude;
//       print("Latitude: $latitude");
//       print("Longitude: $longitude");
//     }

//     await getAddressFromLatLng();
//   }

//   Future<void> getAddressFromLatLng() async {
//     try {
//       List<Placemark> placemarks = await placemarkFromCoordinates(latitude, longitude);
//       Placemark place = placemarks[0];
//       address = '${place.country}, ${place.administrativeArea}, ${place.locality}, ${place.name}, ${place.street}, ${place.subLocality}, ${place.postalCode}';
//       print(address);
//     } catch (e) {
//       print(e);
//     }
//   }

//   void _showLocationDialog() {
//     Get.dialog(
//       AlertDialog(
//         title: const Text('Location Services Disabled'),
//         content: const Text('Please turn on location services to use this feature.'),
//         actions: [
//           TextButton(
//             child: const Text('Settings'),
//             onPressed: () {
//               Geolocator.openLocationSettings();
//               Get.back();
//               if(latitude!=0.0 && longitude!=0.0){
//                  _fetchBackendData(); 
//               }
//             },
//           ),
//           TextButton(
//             child: const Text('Cancel'),
//             onPressed: () {
//               Get.back(); // Close the dialog
//             },
//           ),
//         ],
//       ),
//     );
//   }

//   void _fetchBackendData() async{
//     final homeController = Get.find<HomeController>();
//     print('Fetching backend data with location: $latitude, $longitude');
//     homeController.loadInitialData();
//   }
// }

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';

class Location  {
  var latitude = 0.0.obs;
  var longitude = 0.0.obs;
  var address = ''.obs;
  var isLoading = true.obs;



  Future<void> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // _showLocationDialog();
      // return;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
     
    }

    if (permission == LocationPermission.deniedForever) {
      _showLocationDialog();
      return;
    }

    try {
      Position position = await Geolocator.getCurrentPosition();
      latitude.value = position.latitude;
      longitude.value = position.longitude;
      await getAddressFromLatLng();
    } catch (e) {
      print('Error fetching location: $e');
    } finally {
      isLoading.value = false;
    }
  }

  Future<void> getAddressFromLatLng() async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(latitude.value, longitude.value);
      Placemark place = placemarks[0];
      address.value =
          '${place.country}, ${place.administrativeArea}, ${place.locality}, ${place.name}, ${place.street}, ${place.subLocality}, ${place.postalCode}';
      print('Address: ${address.value}');
    } catch (e) {
      print('Error fetching address: $e');
    }
  }

  void _showLocationDialog() {
    Get.dialog(
      AlertDialog(
        title: const Text('Location Services Disabled'),
        content: const Text('Please turn on location services to use this feature.'),
        actions: [
          TextButton(
            child: const Text('Settings'),
            onPressed: () {
              Geolocator.openLocationSettings();
              Get.back();
            },
          ),
          TextButton(
            child: const Text('Cancel'),
            onPressed: () {
              Get.back();
            },
          ),
        ],
      ),
    );
  }
}
