import 'package:get/get.dart';

import '../../saved_lands/controllers/saved_lands_controller.dart';
import '../controllers/detail_farmer_my_lands_controller.dart';

class DetailFarmerMyLandsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailFarmerMyLandsController>(
      () => DetailFarmerMyLandsController(),
    );
    Get.lazyPut<SavedLandsController>(
      () => SavedLandsController(),
    );
  }
}
