import 'package:agritech/app/modules/signup/controllers/signup_controller.dart';
import 'package:get/get.dart';

import '../../otp/controllers/otp_controller.dart';
import '../../translator_icon/controllers/translator_icon_controller.dart';



class SignupBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SignupController>(
      () => SignupController(),
    );
     Get.lazyPut<OtpController>(
      () => OtpController(),
    );
    Get.lazyPut<TranslatorIconController>(
      () => TranslatorIconController(),
    );
  }
}
