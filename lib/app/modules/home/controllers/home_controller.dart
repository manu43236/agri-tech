import 'package:agritech/app/modules/home/controllers/farmer_landlord_controller.dart';
import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:agritech/app/modules/home/services/get_user_repo_impl.dart';
import 'package:agritech/app/modules/home/services/save_land_as_favourite_repo_impl.dart';
import 'package:agritech/app/modules/notifications/controllers/notifications_controller.dart';
import 'package:agritech/app/modules/utility/controllers/utility_controller.dart';
import 'package:agritech/app/modules/vendor/controllers/vendor_controller.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/models/filter_near_jobs_model.dart';
import 'package:agritech/models/get_user_model.dart';
import 'package:agritech/models/save_farmer_as_favourite_model.dart';
import 'package:agritech/models/save_farmworker_as_favourite_model.dart';
import 'package:agritech/models/save_job_as_favourite_model.dart';
import 'package:agritech/models/save_land_as_favourite_model.dart';
import 'package:agritech/models/save_landlord_as_favourite_model.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../core/utils/widget_utils/location/location.dart';
import '../../Landlord_utilities/controllers/landlord_utilities_controller.dart';
import '../../../../models/update_saved_user_model.dart';
import '../../farm_worker_home_page/controllers/farm_worker_controller.dart';
import '../data/recent_lands_model.dart';
import '../services/save_farmer_as_favourite_repo_impl.dart';
import '../services/save_farmworker_as_favourite_repo_impl.dart';
import '../services/update_saved_user_repo_impl.dart';
import 'landlord_farmer_controller.dart';
import 'landlord_worker_controller.dart';

class HomeController extends BaseController {
  Rx<RecentLandsModel> recentFilterLandsModel = Rx(RecentLandsModel());
  Rx<FilterNearJobsModel> recentFilterJobsModel = Rx(FilterNearJobsModel());
  Rx<GetUserModel> getUserModel = Rx(GetUserModel());
  Rx<SaveLandAsFavouriteModel> saveLandAsFavouriteModel =
      Rx(SaveLandAsFavouriteModel());
  Rx<SaveFarmerAsFavouriteModel> saveFarmerAsFavouriteModel =
      Rx(SaveFarmerAsFavouriteModel());
  Rx<SaveLandlordAsFavouriteModel> saveLandlordAsFavouriteModel =
      Rx(SaveLandlordAsFavouriteModel());
  Rx<SaveJobAsFavouriteModel> saveJobAsFavouriteModel =
      Rx(SaveJobAsFavouriteModel());
  Rx<SaveFarmworkerAsFavouriteModel> saveFarmworkerAsFavouriteModel =
      Rx(SaveFarmworkerAsFavouriteModel());
  Rx<UpdateSavedUserModel> updateSavedUserModel = Rx(UpdateSavedUserModel());

  var latitude = 0.0.obs;
  var longitude = 0.0.obs;

  var userId = "".obs;
  var farmerId = "".obs;
  var farmWorkerId = "".obs;

  RxInt selectedIndex = 0.obs;

  var landlordId = "".obs;
  var selectedLandlordId = "".obs;
  var landId = "".obs;
  var jobId = "".obs;

  var savedId = ''.obs;
  var deleteId = 0.obs;

  var farmerSavedIds = <String, String>{}.obs;

  var savedFarmworkerIds = <String, String>{}.obs;
  var isFarmworkerSaved = false.obs;

  var profileName = "".obs;
  var role = "".obs;
  var profileImage = "".obs;

  RxString selectedValue = ''.obs;

  var locale=[
    {'name':'ENGLISH','locale':Locale('en','US')},
    {'name':'తెలుగు','locale':Locale('te','IN')}
  ];

  var selectedLocale = Locale('en', 'US').obs;

  void updateLang(Locale locale){
     selectedLocale.value = locale;
    Get.back();
    Get.updateLocale(locale);
  }

  @override
  void onInit() async {
    super.onInit();
    print('===========HomeController: initializing location==========');
    
    Location().determinePosition().then((val) async{
      latitude.value =  Location().latitude.toDouble();
      longitude.value = Location().longitude.toDouble();
      loadInitialData();
    });

    print('Latitude: ${latitude.value}, Longitude: ${longitude.value}');
  }

  void loadInitialData() async {
    userId.value = await SecureStorage().readData(key: "id") ?? "";
    role.value = await SecureStorage().readData(key: "role") ?? "";
    print("secure Id : ${userId.value}");
    await getUserById(id: userId.value);

    if(role.value!='Utiliser'){
      Get.find<NotificationsController>().getNotificationsCount();

    }

    if (role.value == 'Landlord') {
      Get.find<LandlordController>().recentLands();
      Get.find<LandlordController>().addAllLands(userId: userId.value);
      Get.find<LandlordController>().addAlljobs(userId: userId.value);
      Get.find<LandlordController>().leasedUtilities(userId: userId.value);
    } else if (role.value == 'Farmer') {
      Get.find<LandlordController>().recentLands();
      Get.find<LandlordController>().addAllFarmersLands(userId: userId.value);
      Get.find<LandlordController>().addAllFarmerjobs(userId: userId.value);
      Get.find<LandlordController>().leasedLands(userId: userId.value,role: role.value);
      Get.find<LandlordController>().leasedUtilities(userId: userId.value);
    } else if (role.value == 'Farm Worker') {
      final FarmWorkerHomePageController farmWorkerHomePageController= Get.find<FarmWorkerHomePageController>();
      await farmWorkerHomePageController.filterJobs(state: '',district: '',mandal: '',village: '',search: '');
      Get.find<LandlordController>().leasedJobs(userId: userId.value,role: role.value);
      Get.find<LandlordController>().leasedUtilities(userId: userId.value);
      farmWorkerHomePageController.getUserByIdSkills(userId: userId.value);
    } else if (role.value == 'Utiliser') {
      final UtilityController utilityController= Get.find<UtilityController>();
      utilityController.userUtilities(userId: userId.value, search: '',state: '',district: '',mandal: '',village: '');
    } else if (role.value == 'Vendor') {
      final VendorController vendorController= Get.find<VendorController>();
      vendorController.recentCrops(userId: userId.value,latitude: latitude.value,longitude: longitude.value);
    }
  }

  void updateIndex(int index) async {
    selectedIndex.value = index;
    print(index);
    role.value == 'Landlord' && index == 0
        ? (
            Get.find<LandlordController>().recentLands(),
            Get.find<LandlordController>().addAllLands(userId: userId.value),
            Get.find<LandlordController>().addAlljobs(userId: userId.value),
            Get.find<LandlordController>().leasedUtilities(userId: userId.value),
          )
        : null;
    role.value == 'Landlord' && index == 1
        ? (
            Get.find<LandlordFarmerController>().landlordFarmerSearch.value ='',
            Get.find<LandlordFarmerController>().filterFarmers(latitude: latitude.value,longitude: longitude.value,search:Get.find<LandlordFarmerController>().landlordFarmerSearch.value )
          )
        : null;
    role.value == 'Landlord' && index == 2
        ? (
            Get.find<LandlordWorkerController>().landlordWorkerSearch.value ='',
            Get.find<LandlordWorkerController>().filterFarmworkers(latitude: latitude.value,longitude: longitude.value,search: Get.find<LandlordWorkerController>().landlordWorkerSearch.value)
          )
        : null;
    role.value == 'Landlord' && index == 3
        ? (
            Get.find<LandlordUtilitiesController>().utilitySearch.value = '',
            Get.find<LandlordUtilitiesController>().filterUtilitiesByLocation(latitude: latitude.value,longitude: longitude.value)
          )
        : null;
    role.value == 'Farmer' && index == 0
        ? (
            Get.find<LandlordController>().recentLands(),
            Get.find<LandlordController>().addAllFarmersLands(userId: userId.value),
            Get.find<LandlordController>().addAllFarmerjobs(userId: userId.value),
            Get.find<LandlordController>().leasedLands(userId: userId.value,role: role.value),
            Get.find<LandlordController>().leasedUtilities(userId: userId.value)
          )
        : null;
    role.value == 'Farmer' && index == 1
        ? (
            Get.find<FarmerLandlordController>().farmerLandlordSearch.value = '',
            Get.find<FarmerLandlordController>().filterLandlords(latitude: latitude.value,longitude: longitude.value,search:Get.find<FarmerLandlordController>().farmerLandlordSearch)
          )
        : null;
    role.value == 'Farmer' && index == 2
        ? (
            Get.find<LandlordWorkerController>().landlordWorkerSearch.value ='',
            Get.find<LandlordWorkerController>().filterFarmworkers(latitude: latitude.value,longitude: longitude.value,search: Get.find<LandlordWorkerController>().landlordWorkerSearch.value)
          )
        : null;
    role.value == 'Farmer' && index == 3
        ? (
            Get.find<LandlordUtilitiesController>().utilitySearch.value = '',
            Get.find<LandlordUtilitiesController>().filterUtilitiesByLocation(latitude: latitude.value,longitude: longitude.value)
          )
        : null;
    role.value == 'Farm Worker' && index == 0
        ? (
            Get.find<FarmWorkerHomePageController>().filterJobs(state: '',district: '',mandal: '',village: '',search: ''),
            Get.find<LandlordController>().leasedJobs(userId: userId.value,role: role.value),
            Get.find<LandlordController>().leasedUtilities(userId: userId.value),
            Get.find<FarmWorkerHomePageController>().getUserByIdSkills(userId: userId.value)
          )
        : null;
    role.value == 'Farm Worker' && index == 1
        ? (
            Get.find<LandlordUtilitiesController>().utilitySearch.value = '',
            Get.find<LandlordUtilitiesController>().filterUtilitiesByLocation(latitude: latitude.value,longitude: longitude.value)
          )
        : null;
    role.value == 'Utiliser' && index == 0
        ? (
            Get.find<UtilityController>().mainUtilSearch.value = '',
            Get.find<UtilityController>().userUtilities(userId: userId.value),
            Get.find<UtilityController>().recentUtilities(search: Get.find<UtilityController>(). mainUtilSearch.value)
          )
        : null;
    role.value == 'Vendor' && index == 0
        ? (Get.find<VendorController>().recentCrops(userId: userId.value,latitude: latitude.value,longitude: longitude.value))
        : null;
  }

  void reorderItems(List<Color> colors, List<String> imagePaths, List<String> names, String role) {
  String translatedYou = 'you'.tr;
  print("Translated 'you': $translatedYou"); // Check the output

  int index = names.indexOf(role);
  if (index != -1) {
    Color roleColor = colors.removeAt(index);
    String roleImagePath = imagePaths.removeAt(index);
    names.removeAt(index);

    colors.insert(0, roleColor);
    imagePaths.insert(0, roleImagePath);
    names.insert(0, translatedYou);
  }
}

  // Get user by id API
  Future<void> getUserById({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await GetUserRepoImpl(dioClient).getUser(id: id);
      print("======================== user by id ====================");
      print(result);
      result.fold((model) async {
        getUserModel.value = model;
        profileName.value = getUserModel.value.result != null
            ? getUserModel.value.result!.firstName.toString()
            : '';
        profileImage.value = getUserModel.value.result != null
            ? getUserModel.value.result!.imageUrl.toString()
            : '';
        selectedLandlordId.value = getUserModel.value.result!.id.toString();
        longitude.value = getUserModel.value.result!.longitude!;
        latitude.value = getUserModel.value.result!.latitude!;
        print(profileImage.value);
        //role.value= getUserModel.value.result!.role!;
        await SecureStorage().writeStringData(
            "role",
            getUserModel.value.result != null
                ? getUserModel.value.result!.role.toString()
                : '');
        role.value = await SecureStorage().readData(key: "role");
        print('role:${role.value}');
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //save land as favourite API
  Future<void> saveLandAsFavourite({landId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SaveLandAsFavouriteRepoImpl(dioClient)
          .saveLand(params1: {"userid": userId.value, "landid": landId});
      print(
          "======================== save land as favourite ====================");
      print(result);
      result.fold((model) async {
        saveLandAsFavouriteModel.value = model;
        print('statusCode:${saveLandAsFavouriteModel.value.statusCode}');
        if (saveLandAsFavouriteModel.value.statusCode == 200) {
          deleteId.value = saveLandAsFavouriteModel.value.result != null
              ? saveLandAsFavouriteModel.value.result!.id!
              : 0;
          Get.snackbar('Success', 'Land saved successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (saveLandAsFavouriteModel.value.statusCode == 400) {
          Get.snackbar('Failed', 'Land already saved',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        print(savedId.value);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> saveLandAsFavouriteFarmer() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SaveLandAsFavouriteRepoImpl(dioClient).saveLand(
          params1: {
            "userid": userId.value,
            "farmerid": farmerId.value,
            "landid": landId.value
          });
      print(
          "======================== save land as favourite ====================");
      print(result);
      result.fold((model) async {
        saveLandAsFavouriteModel.value = model;
        print('statusCode:${saveLandAsFavouriteModel.value.statusCode}');
        if (saveLandAsFavouriteModel.value.statusCode == 200) {
          deleteId.value = saveLandAsFavouriteModel.value.result != null
              ? saveLandAsFavouriteModel.value.result!.id!
              : 0;
          Get.snackbar('Success', 'Land saved successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (saveLandAsFavouriteModel.value.statusCode == 400) {
          Get.snackbar('Failed', 'Land already saved',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        print(savedId.value);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //save farmer as favourite API
  Future<void> saveFarmerAsFavourite() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SaveFarmerAsFavouriteRepoImpl(dioClient).saveFarmer(
          params1: {"userid": userId.value, "farmerid": farmerId.value});
      print('=================save farmer as favourite=============');
      print(result);
      result.fold((model) {
        saveFarmerAsFavouriteModel.value = model;
        if (saveFarmerAsFavouriteModel.value.statusCode == 200) {
          deleteId.value = saveFarmerAsFavouriteModel.value.result!.id!;
          print(
              'Saved farmer with ID: ${farmerId.value}, Saved ID: ${savedId.value}');

          // Update farmerSavedIds with the new saved ID
          farmerSavedIds[farmerId.value] = savedId.value;

          print('Updated Saved IDs Map: $farmerSavedIds');

          Get.snackbar('Success', 'Farmer saved successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (saveFarmerAsFavouriteModel.value.statusCode == 400) {
          Get.snackbar('Failure', 'Farmer already saved',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) => apiStatus.value = ApiStatus.FAIL);
    } catch (e) {
      print('Error saving farmer: $e');
    }
  }

  //save landlord as favourite API
  Future<void> saveLandlordAsFavourite() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SaveFarmerAsFavouriteRepoImpl(dioClient).saveLandlord(
          params1: {"userid": userId.value, "landlordid": landlordId.value});
      print('=================save landlord as favourite=============');
      print(result);
      result.fold((model) {
        saveLandlordAsFavouriteModel.value = model;
        if (saveLandlordAsFavouriteModel.value.statusCode == 200) {
          deleteId.value = saveLandlordAsFavouriteModel.value.result!.id!;
          print(
              'Saved farmer with ID: ${landlordId.value}, Saved ID: ${deleteId.value}');

          // Update farmerSavedIds with the new saved ID
          farmerSavedIds[landlordId.value] = deleteId.value.toString();

          print('Updated Saved IDs Map: $farmerSavedIds');

          Get.snackbar('Success', 'Landlord saved successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (saveLandlordAsFavouriteModel.value.statusCode == 400) {
          Get.snackbar('Failure', 'Landlord already saved',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) => apiStatus.value = ApiStatus.FAIL);
    } catch (e) {
      print('Error saving farmer: $e');
    }
  }

// save farmworker as favourite API
  Future<void> saveFarmworkerAsFavourite() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SaveFarmworkerAsFavouriteRepoImpl(dioClient)
          .farmWorkerSaved(params1: {
        "userid": userId.value,
        "farmworkerid": farmWorkerId.value
      });
      print('=================save farmworker as favourite=============');
      print('userId:${userId.value}');
      print('farmworkerId:${farmWorkerId.value}');
      print(result);
      result.fold((model) {
        saveFarmworkerAsFavouriteModel.value = model;
        if (saveFarmworkerAsFavouriteModel.value.statusCode == 200) {
          deleteId.value = saveFarmworkerAsFavouriteModel.value.result!.id!;
          isFarmworkerSaved.value = true;
          print('savedId:${deleteId.value}');
          print('savedFarmworkerIds:$savedFarmworkerIds');
          Get.snackbar('Success', 'Farmworker saved successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (saveFarmworkerAsFavouriteModel.value.statusCode == 400) {
          Get.snackbar('Failure', 'Farmworker already saved',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) => apiStatus.value = ApiStatus.FAIL);
    } catch (e) {
      print('error ${e}');
    }
  }

//update saved user
  Future<void> updateSavedUser(bool value, {String? errorMessage,id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      print('delete id : ${deleteId.value}');
      var result = await UpdateSavedUserRepoImpl(dioClient)
          .updateSavedItems(params1: {"id": id, "flag": value});

      print("===================== update saved user ==============");
      print(result);
      result.fold((model) {
        updateSavedUserModel.value = model;
        print(updateSavedUserModel.value.result != null
            ? updateSavedUserModel.value.result!.flag
            : '');
        if (updateSavedUserModel.value.statusCode == 200) {
          Get.snackbar('Success', errorMessage ?? '',
              backgroundColor: colorGreen, colorText: colorBlack);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
