import 'package:agritech/app/modules/utility/controllers/utility_controller.dart';
import 'package:agritech/core/conts/img_const.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../../core/base/base_controller.dart';
import '../../../../../core/conts/color_consts.dart';
import '../../../../../core/themes/text_styles.dart';
import '../../../../../core/utils/widget_utils/shimmers.dart';
import '../../../../routes/app_pages.dart';

class RecentUtilitiesWidget extends GetView<UtilityController> {
  const RecentUtilitiesWidget({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final PageController _pageController = PageController();
    return Obx(
      () => Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'available_utilities'.tr,
              style: TextStyles.kTSFS14W600
                  .copyWith(color: colorHello, fontWeight: FontWeight.w500),
            ),
          ],
        ),
        SizedBox(height: 10,),
        controller.apiStatus.value == ApiStatus.LOADING
            ? Shimmers().getShimmerItem()
            : controller.getRecentFourUtilitiesModel.value.result != null &&
                    controller.getRecentFourUtilitiesModel.value.result!.isNotEmpty
                ? Column(
                    children: [
                      SizedBox(
                        height: Get.height / 2.9,
                        child: PageView.builder(
                            controller: _pageController,
                            onPageChanged: (index) {
                              controller.currentPage.value = index;
                            },
                            scrollDirection: Axis.horizontal,
                            itemCount: controller
                                .getRecentFourUtilitiesModel.value.result!.length,
                            itemBuilder: (context, index) {
                              final utility = controller
                                  .getRecentFourUtilitiesModel.value.result![index];
                              final imageUrl = utility.image ??
                                  agri;
                              return Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 5),
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${controller.getRecentFourUtilitiesModel.value.result![index].nameOfInstrument} ${'utility_at'.tr} ${controller.getRecentFourUtilitiesModel.value.result![index].village}",
                                      //textAlign: TextAlign.start,
                                      style: TextStyles.kTSDS14W500
                                          .copyWith(color: colorHello),
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    InkWell(
                                      onTap: () async {
                                        Get.toNamed(Routes.DETAIL_RECENT_UTILITIES,arguments: {"UtilId":controller.getRecentFourUtilitiesModel.value.result![index].id!});
                                      },
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(12),
                                        child: Image.network(
                                          imageUrl,
                                          height: Get.height / 3.5,
                                          width: Get.width,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }),
                      ),
                      Obx(() => Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: List.generate(
                              controller
                                  .getRecentFourUtilitiesModel.value.result!.length,
                              (index) => buildDot(
                                  index, controller.currentPage.value),
                            ),
                          )),
                      Align(
                        alignment: Alignment.topRight,
                        child: InkWell(
                          onTap: () {
                            Get.toNamed(Routes.AVAILABLE_RECENT_UTILITIES);
                          },
                          child: Text(
                            'view_all'.tr,
                            style: TextStyles.kTSCF12W500.copyWith(
                                fontSize: 10,
                                decoration: TextDecoration.underline),
                          ),
                        ),
                      ),
                    ],
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(height: 60),
                      Text(
                        'no_utilities'.tr,
                        style: TextStyles.kTSFS18W500
                            .copyWith(color: colorHello),
                      ),
                      const SizedBox(height: 60),
                    ],
                  ),
      ]),
    );
  }

  Widget buildDot(int index, int currentIndex) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 2),
      width: currentIndex == index ? 10 : 8,
      height: currentIndex == index ? 12 : 8,
      decoration: BoxDecoration(
        color: currentIndex == index ? Colors.blue : Colors.grey,
        shape: BoxShape.circle,
      ),
    );
  }
}
