import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/farm_worker_utility_controller.dart';

class FarmWorkerUtilityView extends GetView<FarmWorkerUtilityController> {
  const FarmWorkerUtilityView({Key? key}) : super(key: key);
  @override
   Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              'UTILITIES',
              style: TextStyles.kTSDS12W700.copyWith(color: colorHello),
            ),
            const SizedBox(height: 10),
            Expanded(
              child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, // 2 items per row
                  crossAxisSpacing: 10, // Space between grid items
                  childAspectRatio: 0.9, // Adjust the ratio for item height
                ),
                itemCount: 4,
                itemBuilder: (context, index) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ClipRRect(
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(11),
                          topRight: Radius.circular(11),
                        ),
                        child: Image.asset(
                          farmWorkerHome,
                          height: Get.height * 0.12,
                          width: Get.width * 0.5,
                          fit: BoxFit.cover,
                        ),
                      ),
                      Container(
                        width: Get.width,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.only(
                            bottomLeft: Radius.circular(11),
                            bottomRight: Radius.circular(11),
                          ),
                          color: colorGrey.shade100,
                        ),
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              'Job at Rajamundry',
                              style: TextStyles.kTSCF12W500
                                  .copyWith(color: colorHello),
                            ),
                            Text(
                              'Kovvuru',
                              style: TextStyles.kTSCF12W500
                                  .copyWith(color: colorHello),
                            ),
                            Text(
                              'Days of Work :',
                              style: TextStyles.kTSCF12W500
                                  .copyWith(color: colorHello),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
