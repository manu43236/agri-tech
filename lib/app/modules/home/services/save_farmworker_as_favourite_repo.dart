import 'package:agritech/models/save_farmworker_as_favourite_model.dart';
import 'package:dartz/dartz.dart';

abstract class SaveFarmworkerAsFavouriteRepo{
  Future<Either<SaveFarmworkerAsFavouriteModel,Exception>> farmWorkerSaved({params1});
}