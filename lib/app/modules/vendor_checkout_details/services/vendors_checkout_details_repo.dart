import 'package:agritech/app/modules/vendor_checkout_details/data/intiate_crop_payment_model.dart';
import 'package:agritech/app/modules/vendor_checkout_details/data/update_crop_payment_model.dart';
import 'package:agritech/app/modules/vendor_checkout_details/data/verify_crop_payment_model.dart';
import 'package:dartz/dartz.dart';

abstract class VendorsCheckoutDetailsRepo{
  Future<Either<UpdateCropPaymentModel,Exception>> updatePayment();
  Future<Either<IntiateCropPaymentModel,Exception>> intiatePayment();
  Future<Either<VerifyCropPaymentModel,Exception>> verifyPayment({params1});
}