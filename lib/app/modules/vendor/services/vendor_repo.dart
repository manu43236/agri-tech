import 'package:agritech/app/modules/vendor/data/get_filtered_crop_availability_model.dart';
import 'package:agritech/app/modules/vendor/data/get_vendor_crops_model.dart';
import 'package:dartz/dartz.dart';

abstract class VendorRepo{
  Future<Either<GetFilteredCropAvailabilityModel,Exception>>  getFilteredCrops();
  Future<Either<GetVendorCropsModel,Exception>>  purchasedCrops();
}