import 'package:agritech/app/modules/available_my_utilities/controllers/available_my_utilities_controller.dart';
import 'package:agritech/app/modules/available_my_utilities/services/available_my_utilities_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../../home/controllers/home_controller.dart';
import '../../utility/data/get_utility_by_user_id_model.dart';

class AvailableMyUtilitiesRepoImpl extends AvailableMyUtilitiesRepo with NetworkCheckService{
  final DioClient _dioClient;
  AvailableMyUtilitiesRepoImpl(this._dioClient);

  @override
  Future<Either<GetUtilityByUserIdModel,Exception>> availableSearchUtility({search,state,district,mandal,village})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('utility/getUtilityByUtililityId?utililityId=${Get.find<HomeController>().userId.value}&search=$search&state=$state&district=$district&mandal=$mandal&village=$village', Method.get);
        return result.fold((l){
          GetUtilityByUserIdModel model= GetUtilityByUserIdModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}