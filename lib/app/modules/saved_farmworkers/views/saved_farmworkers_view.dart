import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/saved_farmworkers_controller.dart';

class SavedFarmworkersView extends GetView<SavedFarmworkersController> {
  const SavedFarmworkersView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'saved_farmworkers'.tr,
          style: TextStyles.kTSFS20W500
              .copyWith(color: colorBlack, fontWeight: FontWeight.w400),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Obx(()=>
          Column(
            children: [
              SizedBox(
                height: Get.height,
                child: controller.getAllSavedFarmworkerModel.value.result?.savedFarmers != null &&
                        controller.getAllSavedFarmworkerModel.value.result!.savedFarmers!.isNotEmpty
                    ? ListView.builder(
                        itemCount: controller.getAllSavedFarmworkerModel.value.result!.savedFarmers!.length,
                        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                        itemBuilder: (context, index) {
                          final savedFarmer = controller.getAllSavedFarmworkerModel.value.result!.savedFarmers![index];
                          return InkWell(
                            onTap: () async {
                              //Get.find<HomeController>().farmWorkerId.value = savedFarmer.farmworkerid?.toString() ?? '';
                              //await Get.find<HomeController>().getUserByProfileId(id:Get.find<HomeController>().farmWorkerId.value);
                              Get.toNamed(Routes.DETAIL_SAVED_FARMWORKERS,arguments: {"workerId":savedFarmer.farmworkerid?.toString()});
                            },
                            child: Container(
                              margin: const EdgeInsets.symmetric(vertical: 10),
                              padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 20),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(19),
                                boxShadow: [
                                  BoxShadow(
                                    color: colorBlack.withOpacity(0.2),
                                    blurRadius: 5,
                                    spreadRadius: 1,
                                    offset: const Offset(0, 3),
                                    blurStyle: BlurStyle.inner,
                                  ),
                                ],
                                color: colorWhite,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Container(
                                        height: 64,
                                        width: 64,
                                        decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: colorCircleWorker,
                                        ),
                                      ),
                                      Positioned(
                                        left: 2,
                                        child: Container(
                                          height: 60,
                                          width: 60,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: colorWhite,
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        child: Container(
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: colorCircleWorker,
                                          ),
                                          child: ClipOval(
                                            child: Image.network(
                                              savedFarmer.imageUrl.toString(),
                                               height: Get.height*0.07,
                                              width: Get.width*0.16,
                                              fit: BoxFit.cover,
                                              errorBuilder: (context, error, stackTrace) => const Icon(Icons.error),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(width: 30,),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        savedFarmer.firstName ?? '',
                                        style: TextStyles.kTSCF12W500,
                                      ),
                                      Text(
                                        savedFarmer.age?.toString() ?? '-',
                                        style: TextStyles.kTSCF12W500,
                                      ),
                                      Text(
                                        savedFarmer.address?? '-',
                                        style: TextStyles.kTSCF12W500,
                                      ),
                                    ],
                                  ),
                                  
                                ],
                              ),
                            ),
                          );
                        },
                      )
                    : Align(
                        alignment: Alignment.topCenter,
                        child: Padding(
                          padding:const EdgeInsets.symmetric(vertical: 150),
                          child: Text(
                            'no_farm_workers_saved'.tr,
                            style: TextStyles.kTSFS16W400,
                          ),
                        ),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
