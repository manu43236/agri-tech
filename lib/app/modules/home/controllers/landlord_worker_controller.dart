import 'package:agritech/app/modules/home/services/home_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../details_of_farmworker/data/near_by_farmworkers_model.dart';
import 'home_controller.dart';

class LandlordWorkerController extends BaseController{

  Rx<NearByFarmworkersModel> landlordFarmWorkerModel =
      Rx(NearByFarmworkersModel());


  var landlordWorkerSearch=''.obs;

  @override
  void onInit() async{
    super.onInit();
    filterFarmworkers(latitude: Get.find<HomeController>().latitude.value,longitude: Get.find<HomeController>().longitude.value,search: '');
  }

  //List of Farm Worker View
  Future<void> filterFarmworkers({latitude,longitude,search}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await HomeRepoImpl(dioClient).doLFarmworker(latitude: latitude,longitude: longitude,search: search);
      print(
          "======================== landlord farmworkers list ====================");
      print(result);
      result.fold((left) {
        farmworkershandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void farmworkershandleResponse(NearByFarmworkersModel model) async {
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
      landlordFarmWorkerModel.value = model;
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}