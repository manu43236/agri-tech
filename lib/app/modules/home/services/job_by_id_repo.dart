import 'package:agritech/models/job_by_id_model.dart';
import 'package:dartz/dartz.dart';

abstract class JobByIdRepo{
  Future<Either<JobByIdModel,Exception>> getJob();
}