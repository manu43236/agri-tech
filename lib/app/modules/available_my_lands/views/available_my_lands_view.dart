import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/text_fields/custom_search_widget.dart';
import '../../../routes/app_pages.dart';
import '../../home/controllers/home_controller.dart';
import '../../select_region/views/select_region_view.dart';
import '../controllers/available_my_lands_controller.dart';

class AvailableMyLandsView extends GetView<AvailableMyLandsController> {
  const AvailableMyLandsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'available_my_lands'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
        actions: [
          InkWell(
            onTap: () {
               Get.bottomSheet(
                isScrollControlled: true,
                isDismissible: true,
                SelectRegionView(type: 'my_Lands'.tr,)
              );
            },
            child: Image.asset(
              cone,
              height: 20,
              width: 20,
            ),
          ),
          const SizedBox(
            width: 20,
          )
        ],
      ),
      body: Obx(()=>
         Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child:  SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomSearchField(
                    onChanged: (val) {
                      controller.myCustomerSearchText.value = val;
                      if (val.length >= 3) {
                       controller. myLandsFilterLandsList(landlordId:Get.find<HomeController>().userId.value,
    farmerId: '',search:controller. myCustomerSearchText.value,state: '',district: '',mandal: '',village: '' );
                        //controller.getAllLandlordLands();
                      } else if (val.isEmpty) {
                        controller.myLandsFilterLandsList(landlordId:Get.find<HomeController>().userId.value,
    farmerId: '',search: controller.myCustomerSearchText.value,state: '',district: '',mandal: '',village: '' );
                        //controller.getAllLandlordLands();
                      }
                    },
                  ),
                  SizedBox(height: 5,),
                  controller.dataSelectedItem.value.isEmpty
                      ? const SizedBox()
                      : Text('${'your_selected_region_is'.tr} ${controller.dataSelectedItem.value}' ?? "", style: TextStyles.kTSDS14W500
                                          .copyWith(color: colorHello),),
                  const SizedBox(
                    height: 10,
                  ),
                  controller.apiStatus.value == ApiStatus.LOADING
                      ? const SizedBox()
                      : controller.myLandsFilterModel.value.result!.isEmpty
                          ?  Align(
                              alignment: Alignment.center,
                              child: Text(
                                "no_lands_found".tr,
                                style:const TextStyle(fontSize: 16, color: Colors.grey),
                              ),
                            )
                          : Column(
                              children: _lordLands(),
                            )
                ],
              ),
            ),
        ),
      ),
    );
  }

  List<Widget> _lordLands() {
    List<Widget> items = [];
    for (var index = 0;
        index <
            controller
                .myLandsFilterModel.value.result!.length;
        index++) {
      items.add(InkWell(
        onTap: () async {
          Get.find<HomeController>().landId.value = controller
              .myLandsFilterModel.value.result![index].id
              .toString();
          //await Get.find<HomeController>().getIdLands(id:Get.find<HomeController>().landId.value );
          Get.toNamed(Routes.DETAIL_MY_LANDS,arguments: {"landId": Get.find<HomeController>().landId.value ,"isLoan":controller.isLoan.value,"isInsurance":controller.isInsurance.value});
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //Image.asset(land,),
            ClipRRect(
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(11), topRight: Radius.circular(11)),
              child: Image.network(
                controller.myLandsFilterModel.value.result?
                        [index].imageUrl ??
                    '',
                height: Get.height * 0.2,
                width: Get.width,
                fit: BoxFit.cover,
                errorBuilder: (context, error, stackTrace) {
                  return Image.asset(
                    defaultImageUrl,
                    height: Get.height * 0.2,
                    width: Get.width,
                    fit: BoxFit.cover,
                  );
                },
              ),
            ),
            Container(
              width: Get.width,
              decoration: const BoxDecoration(
                  color: colorAsh,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(11),
                      bottomRight: Radius.circular(11)),
                  shape: BoxShape.rectangle),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${controller.myLandsFilterModel.value.result![index].fullName} ${'at'.tr} ${controller.myLandsFilterModel.value.result![index].village}",
                      style: TextStyles.kTSDS14W500.copyWith(color: colorHello),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                      '${'extent'.tr}${controller.myLandsFilterModel.value.result![index].landInAcres} ${'acres'.tr}      ${'water_facility'.tr}${controller.myLandsFilterModel.value.result![index].water != null ? controller.myLandsFilterModel.value.result![index].water : 'NA'}',
                      style: TextStyles.kTSFS10W500.copyWith(color: colorHello),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    // InkWell(
                    //   onTap: () {},
                    //   child: Container(
                    //     padding: const EdgeInsets.symmetric(
                    //         horizontal: 10, vertical: 5),
                    //     decoration: const BoxDecoration(
                    //       color: colorCost,
                    //     ),
                    //     child: const Text(
                    //       'Cost Enquiry',
                    //       style: TextStyle(
                    //         color: colorWhite,
                    //         fontSize: 12,
                    //       ),
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ));
    }
    return items;
  }
}
