import 'package:get/get.dart';

import '../controllers/jobs_posted_by_farmer_controller.dart';

class JobsPostedByFarmerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<JobsPostedByFarmerController>(
      () => JobsPostedByFarmerController(),
    );
  }
}
