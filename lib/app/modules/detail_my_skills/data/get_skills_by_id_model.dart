// To parse this JSON data, do
//
//     final getSkillsByIdModel = getSkillsByIdModelFromJson(jsonString);

import 'dart:convert';

GetSkillsByIdModel getSkillsByIdModelFromJson(String str) => GetSkillsByIdModel.fromJson(json.decode(str));

String getSkillsByIdModelToJson(GetSkillsByIdModel data) => json.encode(data.toJson());

class GetSkillsByIdModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<Result>? result;

    GetSkillsByIdModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetSkillsByIdModel.fromJson(Map<String, dynamic> json) => GetSkillsByIdModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    int? userId;
    dynamic skill;
    dynamic experience;
    dynamic description;
    dynamic createdAt;
    dynamic updatedAt;

    Result({
        this.id,
        this.userId,
        this.skill,
        this.experience,
        this.description,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        userId: json["userId"],
        skill: json["skill"],
        experience: json["experience"],
        description: json["description"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "skill": skill,
        "experience": experience,
        "description": description,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
