// To parse this JSON data, do
//
//     final savedUtilityModel = savedUtilityModelFromJson(jsonString);

import 'dart:convert';

SavedUtilityModel savedUtilityModelFromJson(String str) => SavedUtilityModel.fromJson(json.decode(str));

String savedUtilityModelToJson(SavedUtilityModel data) => json.encode(data.toJson());

class SavedUtilityModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    SavedUtilityModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SavedUtilityModel.fromJson(Map<String, dynamic> json) => SavedUtilityModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    int? userid;
    dynamic landlordid;
    dynamic farmerid;
    dynamic farmworkerid;
    dynamic role;
    dynamic landid;
    dynamic jobid;
    int? utilityid;
    bool? flag;
    dynamic imageUrl;
    dynamic createdAt;
    dynamic updatedAt;

    Result({
        this.id,
        this.userid,
        this.landlordid,
        this.farmerid,
        this.farmworkerid,
        this.role,
        this.landid,
        this.jobid,
        this.utilityid,
        this.flag,
        this.imageUrl,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        userid: json["userid"],
        landlordid: json["landlordid"],
        farmerid: json["farmerid"],
        farmworkerid: json["farmworkerid"],
        role: json["role"],
        landid: json["landid"],
        jobid: json["jobid"],
        utilityid: json["utilityid"],
        flag: json["flag"],
        imageUrl: json["image_url"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userid": userid,
        "landlordid": landlordid,
        "farmerid": farmerid,
        "farmworkerid": farmworkerid,
        "role": role,
        "landid": landid,
        "jobid": jobid,
        "utilityid": utilityid,
        "flag": flag,
        "image_url": imageUrl,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
