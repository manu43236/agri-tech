import 'package:get/get.dart';

import '../controllers/hired_farmworkers_controller.dart';

class HiredFarmworkersBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HiredFarmworkersController>(
      () => HiredFarmworkersController(),
    );
  }
}
