const String TOKEN = 'TOKEN';
const String MOBILE='mobileNumber';
const String ROLE='role';
const String USERIDS='userIds';
const String ERROR = 'error';
const String mobile = 'Mobile';
const String franchiseeId = 'franchiseeId';
const String contactId = 'contactId';
const String userName = 'userName';
const String hasFranchise = 'hasFranchise';
const String requesting = 'REQUESTING';
const String callback = 'callback';
const String revisionRequested = 'REVISION REQUESTED';

const String requested = 'REQUESTED';
const String quoted = 'QUOTED';
const String submitted = 'SUBMITTED';
const String accepted = 'ACCEPTED';
const String accepting = 'ACCEPTING';
String rupeeSymbol = "₹";
const String success = "SUCCESS";
const String priceFinalised = 'price_finalised';

const String requestingPending = 'REQUESTING PENDING';
const String quotePending = 'QUOTE PENDING';
const String equipmentType = 'Equipment Type';
const String notInitiated = 'Not-Initiated';
const String pdiInspection = 'PDI-Inspection';
const String heuristic = 'heuristic';
const String every6Months = 'every 6 months';
const String offerType = 'offer';

const String fleetTag = 'Fleet';
const String ueTag = 'UE';
const String homeTag = 'Home';
const String invalidOTP = 'Invalid OTP, please enter valid OTP';
const String countryCode = '91';

const String myContactsMenu = 'My Contacts';
const String dashboardMenu = 'Dashboard';

const String myMachinesMenu = 'My Machines';
const String myAddressMenu = 'My Address';
const String gvMenu = 'Gift Vouchers/Coupon Codes';
const String myCartMenu = 'My Cart';
const String myContactUsMenu = 'Contact Us';
const String myOrdersMenu = 'My Orders';
const String myEnqMenu = 'My Enquiries';
const String chatWithUsMenu = 'Chat With Us';
const String myProjectsMenu = 'My Projects';
const String myCustomersMenu = 'My Customers';
const String myMachineRatesMenu = 'My Machine Rates';
const String myContractsMenu = 'My Contracts';

const String myLogOutMenu = 'Log Out';

const String billing = 'billing';
const String delivery = 'delivery';
const String india = 'INDIA';
const String appSource = 'App';
const String priceMatch = 'Price Match';

const String askforApproval = 'ask for approval';
const String autoApproved = 'auto approved';
const String approved = 'approved';
const String sendforApproval = 'send for approval';
const String sentforApproval = 'sent for approval';
const String denied = 'denied';
const String partiallyApproved = 'partially approved';

const List<String> yearsList = [
  "2024",
  "2023",
  "2022",
  "2021",
  "2020",
  "2019",
  "2018",
  "2017",
  "2016",
  "2015",
  "2014",
  "2013",
  "2012",
  "2011",
  "2010",
  "2009",
  "2008",
  "2007",
  "2006",
  "2005",
  "2004",
  "2003",
  "2002",
  "2001",
];

//Image capture UE sell
const String frontView = 'Front View';
const String backView = 'Back View';
const String leftView = 'Left View';
const String rightView = 'Right View';
const String hourMeter = 'HourMeter';
const String wheelCopy = 'wheelCopy';
const String rc = 'RC';
const String vin = 'VIN';

const String showcase = 'Showcase';

const String insideView = 'InsideView';
const String outsideView = 'OutsideView';

//Parts
const String offerId = '1000';
const String dylOrg = 'DYL';
const int constEnqIndex = 10000;

const String role = 'role';
const String OPERATOR = 'OPERATOR';
const String ALLSERVICE = 'ALLSERVICE';

const String giftVoucher = 'giftvoucher';
const String inrTag = 'INR';
const String usdTag = 'USD';
const String noActiveCoupons =
    'No Active/Not Valid Coupons found for given customer';
const String paidTag = 'paid';
const String packedTag = 'Packed';
const String enquiriesTag = 'enquiries';
const String chaseSoTag = 'chaseSo';
const String quotationsTag = 'quotations';

const String SUMMARY = 'SUMMARY';

const String enquiryTag = 'enquiry';
const String groupcategoryTag = 'groupcategory';
const String modelTag = 'model';
const String phoneNumber = '+91 7676244444';
const String addContract =
    'Hi, I am interested in the Add Contracts feature of Fleet Management app';
const String generateInvoice =
    'Hi, I am interested in the Generate Invoice feature of Fleet Management app';
