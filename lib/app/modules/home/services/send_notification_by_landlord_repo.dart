import 'package:agritech/models/send_notification_by_landlord_model.dart';
import 'package:dartz/dartz.dart';

abstract class SendNotificationByLandlordRepo{
  Future<Either<SendNotificationByLandlordModel,Exception>> sentNotification({params1});
}