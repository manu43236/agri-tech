import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../../home/controllers/home_controller.dart';
import '../controllers/detail_leased_lands_controller.dart';

class DetailLeasedLandsView extends GetView<DetailLeasedLandsController> {
  const DetailLeasedLandsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'detail_leased_lands'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () =>controller.apiLandIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer():
         Padding(
          padding: const EdgeInsets.only(right: 20, left: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: colorAsh,
                    borderRadius: BorderRadius.circular(13),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.network(
                      controller.landByIdModel.value.result
                              ?.landDetails?.imageUrl
                              .toString() ??
                          defaultImageUrl,
                      height: Get.height * 0.3,
                      width: Get.width,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        // Display a local asset if the network image fails
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.3,
                          width: Get.width,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${controller.landByIdModel.value.result?.landDetails?.fullName ?? 'Unknown'} land at',
                            style: TextStyles.kTSFS20W700
                                .copyWith(color: colorDetails),
                          ),
                          Text(
                            '${controller.landByIdModel.value.result?.landDetails?.village ?? ''}, ${controller.landByIdModel.value.result?.landDetails?.district ?? ''}',
                            style: TextStyles.kTSDS14W500
                                .copyWith(color: colorDetails),
                          ),
                        ],
                      ),
                    ),
                    
                  ],
                ),
                const SizedBox(height: 15),
                Row(
                  children: [
                    Image.asset(waterCycle, height: 15),
                    const SizedBox(width: 10),
                    Text(
                      controller.landByIdModel.value.result
                                  ?.landDetails?.water ==
                              'No'
                          ? 'not_available'.tr
                          : 'available'.tr,
                      style:
                          TextStyles.kTSFS12W400.copyWith(color: colorDetails),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Align(
                  alignment: Alignment.center,
                  child: InkWell(
                    onTap: () async {
                      //await Get.find<SuitableCropsController>().getAllCrops();
                      Get.toNamed(Routes.SUITABLE_CROPS,arguments: {"mandalId":controller.landByIdModel.value.result
                                  ?.landDetails?.mandal, "landId":controller.landByIdModel
                            .value.result?.landDetails?.id.toString(),
                        "cropId":controller.landByIdModel
                            .value.result?.landDetails?.cropId.toString()});
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 25),
                      decoration: BoxDecoration(
                        color: colorPic,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text(
                        'suitable_crop'.tr,
                        style:
                            TextStyles.kTSFS16W700.copyWith(color: colorWhite),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        CircleAvatar(
                          radius: 26,
                          backgroundColor: colorAsh,
                          child: controller.homeController.getUserModel.value
                                      .result?.imageUrl !=
                                  null
                              ? ClipOval(
                                  child: Image.network(
                                    Get.find<HomeController>()
                                        .getUserModel
                                        .value
                                        .result!
                                        .imageUrl
                                        .toString(),
                                    fit: BoxFit.cover,
                                    height: Get.height * 0.1,
                                    width: Get.width * 0.2,
                                  ),
                                )
                              : const Icon(Icons.person,
                                  size: 40, color: Colors.grey),
                        ),
                        const SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              controller.homeController.getUserModel.value
                                      .result?.firstName ??
                                  '',
                              style: TextStyles.kTSDS14W700
                                  .copyWith(color: colorPic),
                            ),
                            const SizedBox(height: 5),
                            Text(
                              controller.homeController.getUserModel.value
                                      .result?.address ??
                                  '',
                              style: TextStyles.kTSWFS10W700
                                  .copyWith(color: colorDetails),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Text(
                  controller.landByIdModel.value.result
                          ?.landDetails?.description ??
                      '',
                  style: TextStyles.kTSCF12W500,
                ),
                const SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
