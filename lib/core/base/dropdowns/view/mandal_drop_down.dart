// import 'package:agritech/core/base/base_controller.dart';
// import 'package:agritech/core/base/dropdowns/controller/village_dropdown_controller.dart';
// import 'package:agritech/core/utils/widget_utils/selector_text_field.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import '../../../themes/text_styles.dart';
// import '../../../utils/widget_utils/search_dialog.dart';
// import '../../../utils/widget_utils/search_view.dart';
// import '../controller/mandal_dropdown_controller.dart';

// // ignore: must_be_immutable
// class MandalDropDown extends SearchViews<MandalDropdownController> {
//   TextEditingController? textEditingController;
//   int? id;
//   MandalDropDown({super.key,this.textEditingController,this.id});

//   @override
//   Widget cBuild(BuildContext context,MandalDropdownController cController) {
//     return Obx(

//       ()=> cController.apiStatus == ApiStatus.LOADING ?
//       const SizedBox():
//       InkWell(
//         onTap: () async {
//           var item = await Get.dialog(SearchSelectionDialogView(
//                 items: cController.mandal,
//                 searchHint: 'Select Mandal'.tr,
//                 title: 'Select Mandal'.tr,
//               ));
//               textEditingController!.text = item;
//               print("dialog open ===========");
//               print(item);
//               print(cController.mandalResult.value.where((val)=>val.name == item).toList());
//               var result = cController.mandalResult.value.where((val)=>val.name == item).toList();
//               print(result.first.id);
//               id =result.first.id; 
//               Get.find<VillageDropdownController>().getAllVillages(id:id);
//         },
//         child: SelectorCustomTextField(
//               leftPadding: 5,
//               rightPadding: 0,
//               cMargin: const EdgeInsets.symmetric(horizontal: 4),
//               lableStyle: TextStyles.kTSDS12W500,
//               hintStyle: TextStyles.kTSDS14W500,
//               lable: 'Mandal'.tr,
//               controller: textEditingController!,
//               hintText: 'Select Mandal'.tr,
//               showTextFieldLable: true,
//               showSufixIcon:const SizedBox(),
//               isEnabled: false,
//             ),
//       ),
//     );
//   }
// }

import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/selector_text_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../themes/text_styles.dart';
import '../../../utils/widget_utils/search_dialog.dart';
import '../../../utils/widget_utils/search_view.dart';
import '../controller/mandal_dropdown_controller.dart';
import '../controller/village_dropdown_controller.dart';

class MandalDropDown extends SearchViews<MandalDropdownController> {
  TextEditingController? textEditingController;
  int? id;
  final String? Function(String?)? validator;

  MandalDropDown({super.key, this.textEditingController, this.id, this.validator});

  @override
  Widget cBuild(BuildContext context, MandalDropdownController cController) {
    return Obx(() {
      // If API is loading, show an empty widget
      if (cController.apiStatus == ApiStatus.LOADING) {
        return const SizedBox();
      }

      // Return the dropdown UI
      return FormField<String>(
        validator: validator,
        builder: (FormFieldState<String> field) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () async {
                  // Open the dialog to select the mandal
                  var item = await Get.dialog(SearchSelectionDialogView(
                    items: cController.mandal,
                    searchHint: 'select_mandal'.tr,
                    title: 'select_mandal'.tr,
                  ));

                  // If the user selected a mandal
                  if (item != null) {
                    textEditingController!.text = item;
                    var result = cController.mandalResult.value
                        .where((val) => val.name == item)
                        .toList();
                    if (result.isNotEmpty) {
                      id = result.first.id;

                      // Trigger village fetching
                      Get.find<VillageDropdownController>().getAllVillages(id: id,lang: cController.selectedLang.value);
                    }

                    // Update the form field with the selected mandal
                    field.didChange(item);
                  }
                },
                child: SelectorCustomTextField(
                  leftPadding: 5,
                  rightPadding: 0,
                  cMargin: const EdgeInsets.symmetric(horizontal: 0),
                  lableStyle: TextStyles.kTSDS12W500,
                  hintStyle: TextStyles.kTSDS14W500,
                  lable: 'mandal'.tr,
                  controller: textEditingController!,
                  hintText: 'select_mandal'.tr,
                  showTextFieldLable: true,
                  showSufixIcon: const SizedBox(),
                  isEnabled: false,
                ),
              ),
              // Display the error message if there's a validation error
              if (field.hasError)
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Text(
                    field.errorText!,
                    style: TextStyle(color: Colors.red, fontSize: 12),
                  ),
                ),
            ],
          );
        },
      );
    });
  }
}
