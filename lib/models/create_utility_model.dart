// To parse this JSON data, do
//
//     final createUtilityModel = createUtilityModelFromJson(jsonString);

import 'dart:convert';

CreateUtilityModel createUtilityModelFromJson(String str) => CreateUtilityModel.fromJson(json.decode(str));

String createUtilityModelToJson(CreateUtilityModel data) => json.encode(data.toJson());

class CreateUtilityModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    CreateUtilityModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory CreateUtilityModel.fromJson(Map<String, dynamic> json) => CreateUtilityModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    int? utililityId;
    dynamic nameOfInstrument;
    dynamic pricePerDay;
    dynamic brandName;
    dynamic state;
    dynamic district;
    dynamic mandal;
    dynamic village;
    dynamic description;
    bool? status;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic imageId;
    dynamic image;

    Result({
        this.id,
        this.utililityId,
        this.nameOfInstrument,
        this.pricePerDay,
        this.brandName,
        this.state,
        this.district,
        this.mandal,
        this.village,
        this.description,
        this.status,
        this.updatedAt,
        this.createdAt,
        this.imageId,
        this.image,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        utililityId: json["utililityId"],
        nameOfInstrument: json["nameOfInstrument"],
        pricePerDay: json["pricePerDay"],
        brandName: json["brandName"],
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
        description: json["description"],
        status: json["status"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        imageId: json["image_id"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "utililityId": utililityId,
        "nameOfInstrument": nameOfInstrument,
        "pricePerDay": pricePerDay,
        "brandName": brandName,
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
        "description": description,
        "status": status,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "image_id": imageId,
        "image": image,
    };
}

