import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/utility_vendor_controller.dart';

class UtilityVendorView extends GetView<UtilityVendorController> {
  const UtilityVendorView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: const Center(
        child: Text(
          'UtilityVendorView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
