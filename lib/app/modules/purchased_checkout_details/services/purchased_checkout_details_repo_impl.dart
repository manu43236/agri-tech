import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/purchased_checkout_details/services/purchased_checkout_details_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../data/get_ninty_percent_crop_payment_details_model.dart';

class PurchasedCheckoutDetailsRepoImpl extends PurchasedCheckoutDetailsRepo with NetworkCheckService{
  final DioClient _dioClient;
  PurchasedCheckoutDetailsRepoImpl(this._dioClient);

  @override
  Future<Either<GetNintyPercentCropPaymentModel,Exception>> getDetails({landId,cropId,quantity})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Vender/getNintyPercentCropPaymentPayedByVendor?userId=${Get.find<HomeController>().userId.value}&landId=$landId&cropId=$cropId&quantity=$quantity', Method.get);
        return result.fold((l){
          GetNintyPercentCropPaymentModel model= GetNintyPercentCropPaymentModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}