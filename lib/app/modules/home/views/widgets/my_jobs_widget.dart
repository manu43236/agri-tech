import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:agritech/core/conts/img_const.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/base/base_controller.dart';
import '../../../../../core/conts/color_consts.dart';
import '../../../../../core/themes/text_styles.dart';
import '../../../../../core/utils/widget_utils/shimmers.dart';
import '../../../../routes/app_pages.dart';

class MyJobsWidget extends GetView<LandlordController> {
  const MyJobsWidget({super.key});

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Obx(
        () => Column(
          children: [
            Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'my_jobs'.tr,
                  style: TextStyles.kTSFS12W500.copyWith(color: colorHello),
                )),
            const SizedBox(
              height: 5,
            ),
            controller.apiStatus.value == ApiStatus.LOADING 
                ? Shimmers().getShimmerItem()
                :
            controller.allJobsOfLandlordModel.value.result != null &&
                    controller.allJobsOfLandlordModel.value.result!.ongoingJobs!.isNotEmpty
                ? Column(
                    children: [
                      SizedBox(
                        height: Get.height * 0.14,
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: controller.allJobsOfLandlordModel.value
                                .result!.ongoingJobs!.length,
                            itemBuilder: (context, index) {
                              final job = controller.allJobsOfLandlordModel
                                  .value.result!.ongoingJobs![index];
                              final imageUrl = job.image ??
                                  agri;
                              return Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 5),
                                child: InkWell(
                                  onTap: () async {
                                    Get.find<HomeController>().jobId.value = controller
                                        .allJobsOfLandlordModel
                                        .value
                                        .result!
                                        .ongoingJobs![index]
                                        .id
                                        .toString();
                                    //await Get.find<HomeController>().getIdJobs();
                                    Get.toNamed(Routes.DETAIL_MY_JOBS,arguments: {"jobId": Get.find<HomeController>().jobId.value});
                                  },
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(12),
                                    child: Image.network(
                                          imageUrl ?? '',
                                          width: Get.width * 0.42,
                                          fit: BoxFit.cover,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            // Display a local asset if the network image fails
                                            return Image.asset(
                                              defaultImageUrl,
                                              width: Get.width * 0.42,
                                              fit: BoxFit.cover,
                                            );
                                          },
                                        ),
                                    
                                  ),
                                ),
                              );
                            }),
                      ),
                      const SizedBox(height: 5),
                      Align(
                        alignment: Alignment.topRight,
                        child: InkWell(
                          onTap: () {
                            // Get.find<SelectRegionController>().isLandlordJobs.value = false;
                            // print('is job ${Get.find<SelectRegionController>().isLandlordJobs.value}');
                            Get.toNamed(Routes.AVAILABLE_MY_JOBS);
                          },
                          child: Text(
                            'view_all'.tr,
                            style: TextStyles.kTSCF12W500.copyWith(
                                fontSize: 10,
                                decoration: TextDecoration.underline),
                          ),
                        ),
                      ),
                    ],
                  )
                : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: Get.height * 0.03,
                      ),
                      InkWell(
                          onTap: () {
                            Get.toNamed(Routes.ADD_JOB);
                          },
                          child: Text(
                            'no_jobs_add_it_here'.tr,
                            style: TextStyles.kTSFS18W500.copyWith(
                                color: colorBlue,
                                decoration: TextDecoration.underline),
                          )),
                      SizedBox(
                        height: Get.height * 0.03,
                      ),
                    ],
                  ),
          ],
        ),
      ),
    );
  }
}
