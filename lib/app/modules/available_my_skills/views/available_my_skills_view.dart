import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/available_my_skills_controller.dart';

class AvailableMySkillsView extends GetView<AvailableMySkillsController> {
  const AvailableMySkillsView({Key? key}) : super(key: key);
  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Available My Skills',
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
              Get.back();
          },
        ),
      ),
      body: Obx(()=>
         Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child:  SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  controller.apiAvailableSkillsStatus.value == ApiStatus.LOADING
                      ? const SizedBox()
                      : controller.getAvailableSkillsByUserIdModel.value.result!
                              .isEmpty
                          ? const Align(
                              alignment: Alignment.center,
                              child: Text(
                                "No Skills Found",
                                style: TextStyle(fontSize: 16, color: Colors.grey),
                              ),
                            )
                          : Column(
                              children: _lordLands(),
                            )
                ],
              ),
            ),
        ),
      ),
    );
  }

  List<Widget> _lordLands() {
    List<Widget> items = [];
    for (var index = 0;
        index <
            controller
                .getAvailableSkillsByUserIdModel.value.result!.length;
        index++) {
      items.add(InkWell(
        onTap: () async {
          Get.toNamed(Routes.DETAIL_MY_SKILLS,arguments: {"id":controller
                .getAvailableSkillsByUserIdModel.value.result![index].id,"names":controller
                .getAvailableSkillsByUserIdModel.value.result![index].skill});
        },
        child: Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Image.asset(land,),
              ClipRRect(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(11), topRight: Radius.circular(11)),
                child: 
                    Image.asset(
                      defaultImageUrl,
                      height: Get.height * 0.2,
                      width: Get.width,
                      fit: BoxFit.cover,
                    ),
              ),
              Container(
                width: Get.width,
                decoration: const BoxDecoration(
                    color: colorAsh,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(11),
                        bottomRight: Radius.circular(11)),
                    shape: BoxShape.rectangle),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${controller.getAvailableSkillsByUserIdModel.value.result![index].skill}",
                        style: TextStyles.kTSDS14W500.copyWith(color: colorHello),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Experience-${controller.getAvailableSkillsByUserIdModel.value.result![index].experience} years',
                        style: TextStyles.kTSFS10W500.copyWith(color: colorHello),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ));
    }
    return items;
  }
}
