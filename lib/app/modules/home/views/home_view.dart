import 'package:agritech/app/modules/farm_worker_home_page/views/farm_worker_home_view.dart';
import 'package:agritech/app/modules/home/views/farmer.dart';
import 'package:agritech/app/modules/home/views/farmer_landlord.dart';
import 'package:agritech/app/modules/home/views/landlord_farmer.dart';
import 'package:agritech/app/modules/home/views/landlord_worker.dart';
import 'package:agritech/app/modules/home/views/worker_farmer.dart';
import 'package:agritech/app/modules/home/views/landlord_view.dart';
import 'package:agritech/app/modules/loans/views/loans_view.dart';
import 'package:agritech/app/modules/notifications/views/widgets/notification_icon_view.dart';
import 'package:agritech/app/modules/translator_icon/views/translator_icon_view.dart';
import 'package:agritech/app/modules/utility/views/utility_view.dart';
import 'package:agritech/app/modules/utility_vendor/views/utility_vendor_view.dart';
import 'package:agritech/app/modules/vendor/views/vendor_view.dart';
import 'package:agritech/app/modules/vendor_loans/views/vendor_loans_view.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/conts/img_const.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:agritech/core/utils/widget_utils/story_circle.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../../Landlord_utilities/views/landlord_utilities_view.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  HomeView({Key? key}) : super(key: key);

  final List<Color> colors = [
    colorCircleLandlord,
    colorCircleFarmer,
    colorCircleWorker,
    colorCircleUtilities,
    colorCircleLoans
  ];
  final List<String> imagePaths = [
    landlord,
    farmers,
    farmWorker,
    utility,
    utility
  ];
  final List<String> names = [
    'landlord'.tr,
    'farmer'.tr,
    'farm_worker'.tr,
    'utilities'.tr,
    'loans_insurance'.tr
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Text('hello'.tr,
                          style: TextStyles.kTSDS12W700
                              .copyWith(color: colorHello)),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child:controller.apiStatus.value==ApiStatus.LOADING?Shimmers().getShimmerForText(): 
                      Text(controller.profileName.value,
                          style: TextStyles.kTSFS24W600
                              .copyWith(color: colorHello)),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child:controller.apiStatus.value==ApiStatus.LOADING?Shimmers().getShimmerForText(): 
                       Text(
                          'you_are_now_a'.tr + '${controller.role.value}',
                          style: TextStyles.kTSDS12W700
                              .copyWith(color: colorHello)),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    InkWell(
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (context) {
                              return const TranslatorIconView();
                            },
                          );
                        },
                        child: Image.asset(
                          transIcon,
                          height: 35,
                        )),
                    SizedBox(
                      width: 20,
                    ),
                    controller.role.value != 'Utiliser'
                        ? const NotificationIconView()
                        : const SizedBox()
                  ],
                ),
              ],
            ),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: SizedBox(
                height: Get.height * 0.122,
                child: Obx(() {
                  // Create a copy of the original lists
                  List<Color> reorderedColors = List.from(colors);
                  List<String> reorderedImagePaths = List.from(imagePaths);
                  List<String> reorderedNames = List.from(names);

                  // Reorder based on the selected role
                  if (controller.role.value == 'Landlord') {
                    reorderedColors = [
                      colorCircleLandlord,
                      colorCircleFarmer,
                      colorCircleWorker,
                      colorCircleUtilities,
                      colorCircleLoans
                    ];
                    reorderedImagePaths = [
                      landlord,
                      farmers,
                      farmWorker,
                      utility,
                      utility
                    ];
                    reorderedNames = [
                      'you'.tr,
                      'farmer'.tr,
                      'farm_worker'.tr,
                      'utilities'.tr,
                      'loans_insurance'.tr
                    ];
                  }
                  if (controller.role.value == 'Farmer') {
                    reorderedColors = [
                      colorCircleLandlord,
                      colorCircleFarmer,
                      colorCircleWorker,
                      colorCircleUtilities,
                      colorCircleLoans
                    ];
                    reorderedImagePaths = [
                      landlord,
                      farmers,
                      farmWorker,
                      utility,
                      utility
                    ];
                    reorderedNames = [
                      'you'.tr,
                      'landlord'.tr,
                      'farm_worker'.tr,
                      'utilities'.tr,
                      'loans_insurance'.tr
                    ];
                  }

                  if (controller.role.value == 'Farm Worker') {
                    reorderedColors = [
                      colorCircleWorker,
                      colorCircleUtilities,
                      colorCircleOthers
                    ];
                    reorderedImagePaths = [farmWorker, utility, utility];
                    reorderedNames = ['you'.tr, 'utilities'.tr, 'others'.tr];
                  }
                  if (controller.role.value == 'Utiliser') {
                    reorderedColors = [
                      colorCircleWorker,
                      colorCircleUtilities,
                      colorCircleOthers
                    ];
                    reorderedImagePaths = [farmWorker, utility, utility];
                    reorderedNames = [
                      'you'.tr,
                      'vendor'.tr,
                      'loans_insurance'.tr
                    ];
                  }
                  if (controller.role.value == 'Vendor') {
                    reorderedColors = [
                      colorCircleLandlord,
                      colorCircleUtilities
                    ];
                    reorderedImagePaths = [landlord, utility];
                    reorderedNames = ['you'.tr, 'loans_insurance'.tr];
                  }
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: reorderedColors.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () => controller.updateIndex(index),
                        child: CircleDesign(
                          color: reorderedColors[index],
                          imagePath: reorderedImagePaths[index],
                          name: reorderedNames[index],
                        ),
                      );
                    },
                  );
                }),
              ),
            ),
            const SizedBox(height: 5),
            _buildRoleSpecificView(),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

  _buildRoleSpecificView() {
    return Expanded(
      child: Obx(() {
        int index = controller.selectedIndex.value;
        return controller.role.value.toLowerCase() == 'landlord' ||
                controller.role.value.toLowerCase() == 'farmer'
            ? (index == 0
                ? controller.role.value.toLowerCase() == 'landlord'
                    ? const LandlordView()
                    : const Farmerr()
                : index == 1
                    ? controller.role.value.toLowerCase() == 'landlord'
                        ? const LandlordFarmer()
                        : const FarmerLandlord()
                    : index == 2
                        ? const LandlordWorker()
                        : index == 3
                            ? const LandlordUtilitiesView()
                            : index == 4
                                ? const LoansView()
                                : const SizedBox.shrink())
            : controller.role.value.toLowerCase() == 'farm worker'
                ? (index == 0
                    ? const FarmWorkerHomeView()
                    : index == 1
                        ? const LandlordUtilitiesView()
                        : index == 2
                            ? const WorkerFarmer()
                            : const SizedBox.shrink())
                : controller.role.value.toLowerCase() == 'utiliser'
                    ? (index == 0
                        ? const UtilityView()
                        : index == 1
                            ? const UtilityVendorView()
                            : index == 2
                                ? const LoansView()
                                : const SizedBox.shrink())
                    : controller.role.value.toLowerCase() == 'vendor'
                        ? (index == 0
                            ? const VendorView()
                            : index == 1
                                ? const VendorLoansView()
                                : const SizedBox.shrink())
                        : Shimmers().getListShimmer();
                        //  Center(
                        //     child: Text("something_went_wrong".tr),
                        //   );
      }),
    );
  }
}
