import 'package:agritech/app/modules/select_region/data/district_model.dart';
import 'package:dartz/dartz.dart';

abstract class DistrictRepo{
Future<Either<DistrictModel,Exception>>  getDistrict();
}