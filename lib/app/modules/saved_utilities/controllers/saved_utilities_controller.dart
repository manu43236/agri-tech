import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../data/get_all_saved_utility_model.dart';
import '../services/saved_utilities_repo_impl.dart';

class SavedUtilitiesController extends BaseController {

  Rx<GetAllSavedUtilityModel> getAllSavedUtilityModel =
      Rx(GetAllSavedUtilityModel());

  var userId=''.obs;
  var apiSavedUtilityStatus=ApiStatus.LOADING.obs;

  @override
  void onInit() async{
    super.onInit();
    userId.value = await SecureStorage().readData(key: "id") ?? "";
    getSavedUtilities(userId: userId.value);
  }

   //get saved utilities API
  Future<void> getSavedUtilities({userId}) async {
    apiSavedUtilityStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedUtilitiesRepoImpl(dioClient).getSavedUtilities(userId: userId);
      print(
          "======================== get saved utilities ====================");
      print(result);
      result.fold((model) async {
        getAllSavedUtilityModel.value = model;
        apiSavedUtilityStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiSavedUtilityStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
