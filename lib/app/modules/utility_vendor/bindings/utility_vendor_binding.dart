import 'package:get/get.dart';

import '../controllers/utility_vendor_controller.dart';

class UtilityVendorBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UtilityVendorController>(
      () => UtilityVendorController(),
    );
  }
}
