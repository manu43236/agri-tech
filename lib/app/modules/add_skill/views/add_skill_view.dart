import 'package:agritech/app/modules/add_skill/views/custom_experience_field.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../../add_land/views/custom_text_form_field.dart';
import '../../home/views/custom_text.dart';
import '../controllers/add_skill_controller.dart';

class AddSkillView extends GetView<AddSkillController> {
  AddSkillView({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'add_a_skill'.tr,
          style: TextStyles.kTSFS24W600
              .copyWith(color: colorDetails, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () {
            Get.offNamed(Routes.DASHBOARD);
          },
        ),
      ),
      body: Obx(
        () => SingleChildScrollView(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 0),
          child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CustomText(name: 'skill_name'.tr),
                const SizedBox(height: 5),
                CustomTextFormField(
                  controller: controller.nameController.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_name'.tr;
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                CustomText(name: 'experience'.tr),
                const SizedBox(height: 5),
                // CustomTextFormField(
                //   controller: controller.expirenceController.value,
                //   validator: (value) {
                //     if (value == null || value.isEmpty) {
                //       return '* required experience';
                //     }
                //     return null;
                //   },
                // ),
                CustomExperienceField(
                  controller: controller.expirenceController.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "please_enter_your_experience".tr;
                    }
                    return null;
                  },
                ),
                CustomText(name: 'description'.tr),
                const SizedBox(height: 5),
                CustomTextFormField(
                  controller: controller.descriptionController.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_description'.tr;
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 30,
                ),
                Center(
                  child: InkWell(
                    onTap: () {
                      //isFormValid.value = true;
                      print('on tap');
                      if (_formKey.currentState!.validate()) {
                        controller.addSkill();
                      }
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 7),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(11),
                        color: primary,
                      ),
                      child: Text(
                        'add_skill'.tr,
                        style:
                            TextStyles.kTSFS18W500.copyWith(color: colorWhite),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
