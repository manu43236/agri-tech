import 'package:agritech/app/modules/utility/data/get_recent_four_utilities_model.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../utility/data/get_utility_by_user_id_model.dart';
import '../../utility/services/utility_repo_impl.dart';

class MyInstrumentsController extends BaseController {

  Rx<GetRecentFourUtilitiesModel> getUtilityByUserIdModel = Rx(GetRecentFourUtilitiesModel());

  Rx<GetUtilityByUserIdModel> getUtilityUserIdModel =
      Rx(GetUtilityByUserIdModel());

  var apiUserUtilityStatus=ApiStatus.LOADING.obs;
  var id=''.obs;

  @override
  void onInit() async{
    id.value= await SecureStorage().readData(key: "id");
    super.onInit();
    userUtilities(userId:id.value , search: '',state: '',district: '',mandal: '',village: '');
  }

  //get utility by id API
  Future<void> getUtilityById() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await UtilityRepoImpl(dioClient).getUtilityByUserId();
      print("===================== get utility by user id ==============");
      print(result);
      result.fold((model) {
        getUtilityByUserIdModel.value = model;
        print(getUtilityByUserIdModel.value.result![0].id);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //user utilities API
  Future<void> userUtilities({userId,search,state,district,mandal,village}) async {
    apiUserUtilityStatus.value = ApiStatus.LOADING;

    try {
      var result = await UtilityRepoImpl(dioClient).getUtilities(userId: userId, search: search,state: state,district: district,mandal: mandal,village: village);
      print('=============== get user utilities =========');
      print(result);
      result.fold((model) {
        if (model.statusCode == 200) {
          getUtilityUserIdModel.value = model;
        } 
        apiUserUtilityStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiUserUtilityStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
