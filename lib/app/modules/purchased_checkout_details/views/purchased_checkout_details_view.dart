import 'package:agritech/app/modules/vendor_checkout_details/controllers/vendor_checkout_details_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/purchased_checkout_details_controller.dart';

class PurchasedCheckoutDetailsView
    extends GetView<PurchasedCheckoutDetailsController> {
  const PurchasedCheckoutDetailsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
       title: Text(
          'checkout'.tr,
          style: TextStyles.kTSFS24W600
              .copyWith(color: colorSuitable, fontWeight: FontWeight.w800),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body:
      Obx(
        () => 
        controller.apiNintyPercentDetails.value==ApiStatus.LOADING?Shimmers().getListShimmer():
        Padding(
          padding: const EdgeInsets.only(left: 25, right: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.network(
                      controller.getNintyPercentCropPaymentModel.value.result?.masterCrop?.imageUrl.toString()??defaultImageUrl,
                      height: Get.height * 0.18,
                      width: Get.width,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        // Display a local asset if the network image fails
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.18,
                          width: Get.width,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                  ),
              const SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          controller.getNintyPercentCropPaymentModel.value.result?.masterCrop?.name??
                              '',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyles.kTSFS20W700
                              .copyWith(color: colorSuitable),
                        ),
                        const SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                '${controller.getNintyPercentCropPaymentModel.value.result?.user?.firstName ?? ''}',
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                style: TextStyles.kTSDS14W500
                                    .copyWith(color: colorSuitable),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Flexible(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      
                      const SizedBox(
                        width: 5,
                      ),
                      Text(
                       '${ controller.getNintyPercentCropPaymentModel.value.result?.utilityPayment?["quantity"].toString() ??''} ${'Tons'.tr}',
                        style: TextStyles.kTSFS20W700
                            .copyWith(color: colorSuitable, fontSize: 20),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                     
                    ],
                  )),
                ],
              ),
              const SizedBox(height: 50,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('total_cost_of_selected_item'.tr,
                      style: TextStyles.kTSDS14W500
                          .copyWith(color: colorSuitable)),
                  Text(
                    'Rs ${controller.getNintyPercentCropPaymentModel.value.result?.utilityPayment?["totalPayment"].toString() ?? "-"}',
                    style:
                        TextStyles.kTSDS14W500.copyWith(color: colorSuitable),
                  )
                ],
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('payment_made'.tr,
                      style: TextStyles.kTSDS14W500
                          .copyWith(color: colorSuitable)),
                  Text(
                      '${'rs'.tr} ${controller.getNintyPercentCropPaymentModel.value.result?.utilityPayment?["tenPercentPrice"].toString() ?? "-"}',
                      style:
                          TextStyles.kTSDS14W500.copyWith(color: colorSuitable))
                ],
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('balance_payment'.tr,
                      style: TextStyles.kTSDS14W500
                          .copyWith(color: colorSuitable)),
                  Text(
                      '${'rs'.tr} ${controller.getNintyPercentCropPaymentModel.value.result?.utilityPayment?["ninetyPercentPrice"].toString()}',
                      style:
                          TextStyles.kTSDS14W500.copyWith(color: colorSuitable))
                ],
              ),
              const SizedBox(height: 20),
              const Divider(
                thickness: 1,
                color: colorBlack,
              ),
              const SizedBox(height: 20),
              // Price and Total
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('amount_to_be_paid'.tr,
                      style: TextStyles.kTSDS14W500
                          .copyWith(color: colorSuitable)),
                  Text(
                    '${'rs'.tr} ${controller.getNintyPercentCropPaymentModel.value.result?.utilityPayment?["ninetyPercentPrice"].toString() ?? "-"}',
                    style:
                        TextStyles.kTSDS14W500.copyWith(color: colorSuitable),
                  )
                ],
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('gst'.tr,
                      style: TextStyles.kTSDS14W500
                          .copyWith(color: colorSuitable)),
                  Text(
                      '${'rs'.tr} ${controller.getNintyPercentCropPaymentModel.value.result?.utilityPayment?["nintyPercentGST"].toString() ?? "-"}',
                      style:
                          TextStyles.kTSDS14W500.copyWith(color: colorSuitable))
                ],
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('total'.tr,
                      style: TextStyles.kTSDS14W500
                          .copyWith(color: colorSuitable)),
                  Text(
                      '${'rs'.tr} ${controller.getNintyPercentCropPaymentModel.value.result?.utilityPayment?["nintyPercentpayment"].toString() ?? "-"}',
                      style:
                          TextStyles.kTSDS14W500.copyWith(color: colorSuitable))
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              Center(
                child: InkWell(
                  onTap: () async{
                    await Get.find<VendorCheckoutDetailsController>().intiateCropPayment(paymentId:controller.getNintyPercentCropPaymentModel.value.result?.utilityPayment?["id"] );
                    Get.find<VendorCheckoutDetailsController>().openCheckout(Get.find<VendorCheckoutDetailsController>().amount.value);
                  },
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(13),
                        color: primary),
                    child: Text(
                      'make_a_payment'.tr,
                      style: TextStyles.kTSFS16W700.copyWith(color: colorWhite),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
