// To parse this JSON data, do
//
//     final jobByIdModel = jobByIdModelFromJson(jsonString);

import 'dart:convert';

JobByIdModel jobByIdModelFromJson(String str) => JobByIdModel.fromJson(json.decode(str));

String jobByIdModelToJson(JobByIdModel data) => json.encode(data.toJson());

class JobByIdModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    JobByIdModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory JobByIdModel.fromJson(Map<String, dynamic> json) => JobByIdModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    dynamic landLordId;
    int? farmerId;
    dynamic fullName;
    dynamic startDate;
    dynamic endDate;
    dynamic state;
    dynamic district;
    dynamic village;
    dynamic mandal;
    dynamic description;
    dynamic image;
    dynamic status;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;
    JobOwnerDetails? jobOwnerDetails;
    List<dynamic>? notificationStatuses;
    List<FarmworkerDetail>? farmworkerDetails;

    Result({
        this.id,
        this.landLordId,
        this.farmerId,
        this.fullName,
        this.startDate,
        this.endDate,
        this.state,
        this.district,
        this.village,
        this.mandal,
        this.description,
        this.image,
        this.status,
        this.flag,
        this.createdAt,
        this.updatedAt,
        this.jobOwnerDetails,
        this.notificationStatuses,
        this.farmworkerDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        startDate: json["startDate"] == null ? null : DateTime.parse(json["startDate"]),
        endDate: json["endDate"] == null ? null : DateTime.parse(json["endDate"]),
        state: json["state"],
        district: json["district"],
        village: json["village"],
        mandal: json["mandal"],
        description: json["description"],
        image: json["image"],
        status: json["status"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        jobOwnerDetails: json["jobOwnerDetails"] == null ? null : JobOwnerDetails.fromJson(json["jobOwnerDetails"]),
        notificationStatuses: json["notificationStatuses"] == null ? [] : List<dynamic>.from(json["notificationStatuses"]!.map((x) => x)),
        farmworkerDetails: json["farmworkerDetails"] == null ? [] : List<FarmworkerDetail>.from(json["farmworkerDetails"]!.map((x) => FarmworkerDetail.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "fullName": fullName,
        "startDate": startDate?.toIso8601String(),
        "endDate": endDate?.toIso8601String(),
        "state": state,
        "district": district,
        "village": village,
        "mandal": mandal,
        "description": description,
        "image": image,
        "status": status,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "jobOwnerDetails": jobOwnerDetails?.toJson(),
        "notificationStatuses": notificationStatuses == null ? [] : List<dynamic>.from(notificationStatuses!.map((x) => x)),
        "farmworkerDetails": farmworkerDetails == null ? [] : List<dynamic>.from(farmworkerDetails!.map((x) => x.toJson())),
    };
}

class FarmworkerDetail {
    int? id;
    dynamic firstName;
    dynamic mobileNumber;
    dynamic imageUrl;
    dynamic address;

    FarmworkerDetail({
        this.id,
        this.firstName,
        this.mobileNumber,
        this.imageUrl,
        this.address,
    });

    factory FarmworkerDetail.fromJson(Map<String, dynamic> json) => FarmworkerDetail(
        id: json["id"],
        firstName: json["firstName"],
        mobileNumber: json["mobileNumber"],
        imageUrl: json["image_url"],
        address: json["address"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "mobileNumber": mobileNumber,
        "image_url": imageUrl,
        "address": address,
    };
}

class JobOwnerDetails {
    dynamic imageUrl;
    dynamic firstName;
    dynamic address;

    JobOwnerDetails({
        this.imageUrl,
        this.firstName,
        this.address,
    });

    factory JobOwnerDetails.fromJson(Map<String, dynamic> json) => JobOwnerDetails(
        imageUrl: json["image_url"],
        firstName: json["firstName"],
        address: json["address"],
    );

    Map<String, dynamic> toJson() => {
        "image_url": imageUrl,
        "firstName": firstName,
        "address": address,
    };
}
