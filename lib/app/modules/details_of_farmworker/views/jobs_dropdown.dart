import 'package:agritech/app/modules/details_of_farmworker/controllers/details_of_farmworker_controller.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';

class JobsDropdown extends GetView<DetailsOfFarmworkerController> {
  const JobsDropdown({super.key});

  void show(BuildContext context) {
    List<dynamic> jobs = [];
    Get.bottomSheet(
      Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.center,
            child: InkWell(
              onTap: () {
                Get.back();
              },
              child: Container(
                  padding: const EdgeInsets.all(5),
                  decoration: const BoxDecoration(
                      color: primary, shape: BoxShape.circle),
                  child: const Icon(
                    Icons.close,
                    size: 25,
                    color: colorWhite,
                  )),
            ),
          ),
          Container(
            height: Get.height * 0.75,
            padding: const EdgeInsets.all(16),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(16),
              ),
            ),
            child: Obx(() {
              final jobs = controller.getAvailableJobsModel.value.result!;
              if (jobs.isEmpty) {
                return Center(
                    child: Text(
                  "no_jobs_available".tr,
                  style: TextStyles.kTSCF12W500.copyWith(color: colorGrey),
                ));
              }
              return GridView.builder(
                shrinkWrap: true,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  childAspectRatio: 1,
                ),
                itemCount: jobs.length,
                itemBuilder: (context, index) {
                  final job = jobs[index];
                  return GestureDetector(
                    onTap: () async{
                      // Update selected land details in the controller
                      controller.selectedJob.value = {
                        'name': job.fullName,
                        'image': job.image ?? agri,
                      };
                      controller.selectedJobId.value = job.id.toString();
                      controller.statusForLandlord.value='';
                     // controller.statusForFarmer.value='';
                      await controller.fetchedJobDetails(senderId: controller.id.value,
                                          workerId: controller.workerId.value,
                                          senderRole: controller.role.value);
                      print('status zzz:${controller.statusForLandlord.value}');
                      //print('status ppp:${controller.statusForFarmer.value}');
                      // if(Get.find<HomeController>().role.value =='Landlord'){
                      //   controller.statusForLandlord.value='';
                      //   await controller.FetchedJobDetails();
                      //   print('status zzz:${controller.statusForLandlord.value}');
                      // }
                      // else if(Get.find<HomeController>().role.value =='Farmer'){
                      //   controller.statusForFarmer.value='';
                      //   await controller.FetchedFarmerJobDetails();
                      //   print('status ppp:${controller.statusForFarmer.value}');
                      // }
                      Get.back(); // Close the bottom sheet after selection
                    },
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ClipOval(
                          child: Image.network(
                            job.image ?? agri,
                            width: 70,
                            height: 70,
                            fit: BoxFit.cover,
                          ),
                        ),
                        const SizedBox(height: 8),
                        Flexible(
                          child: Text(
                            job.fullName,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  );
                },
              );
            }),
          ),
        ],
      ),
      isScrollControlled: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        show(context);
      },
      child: Obx(() {
        final selectedJob = controller.selectedJob.value;
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: primary,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              if (selectedJob != null)
                Expanded(
                  child: Row(
                    children: [
                      ClipOval(
                        child: Image.network(
                          selectedJob['image'] ?? '',
                          width: 25,
                          height: 25,
                          fit: BoxFit.cover,
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: Text(
                          selectedJob['name'] ?? '',
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              else
                 Expanded(
                  child: Text(
                    'select_job'.tr,
                    style:const TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
              const SizedBox(
                width: 50,
              ),
              const Icon(Icons.keyboard_arrow_down_rounded,
                  color: Colors.white, size: 20),
            ],
          ),
        );
      }),
    );
  }
}
