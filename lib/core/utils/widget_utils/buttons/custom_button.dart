import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({super.key, required this.action, required this.name});
  final Function() action;
  final String name;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: action,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 10),
        width:Get.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(11),
          color: primary,
        ),
        child: Text(name,textAlign: TextAlign.center, style: TextStyles.kTSFS18W600.copyWith(color: Color(0xffF8F8FF)),),
      ),
    );
  }
}