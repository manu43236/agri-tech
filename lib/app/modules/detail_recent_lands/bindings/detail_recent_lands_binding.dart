import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:get/get.dart';

import '../../saved_lands/controllers/saved_lands_controller.dart';
import '../controllers/detail_recent_lands_controller.dart';

class DetailRecentLandsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailRecentLandsController>(
      () => DetailRecentLandsController(),
    );
    Get.lazyPut<SavedLandsController>(
      () => SavedLandsController(),
    );
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
  }
}
