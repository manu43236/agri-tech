// ignore_for_file: must_be_immutable

import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class SelectorCustomTextField extends StatelessWidget {
  final String lable;

  final TextEditingController controller;

  final TextInputType keyboardType;

  int maxLines;
  final double editorHeight;

  String hintText;

  List<TextInputFormatter> inputformate;

  final TextStyle lableStyle;
  final TextStyle hintStyle;

  final int maxLength;

  final bool showTextFieldLable;
  final Color fieldBgColor;
  final bool isEnabled;
  final bool isReadOnly;
  final Widget? showSufixIcon;
  final Widget? showLocationIcon;
  final BoxDecoration? containerDecoration;
  final EdgeInsets? containerPadding;
  final Function()? ontap;
  final Function()? onEditingC;
  final Function(String)? cOnChanged;
  final FocusNode? focusnode;
  final EdgeInsets? cMargin;
  final double leftPadding;
  final double rightPadding;
  final Color? suffixIconColor;
  final TextStyle? textStyle;
  final EdgeInsets? contentPadding;

  SelectorCustomTextField({
    super.key,
    this.lable = '',
    required this.controller,
    this.maxLines = 1,
    this.lableStyle = TextStyles.kTSNFS14,
    this.hintStyle = TextStyles.kTSNFS14,
    this.hintText = '',
    this.showTextFieldLable = false,
    this.isEnabled = true,
    this.isReadOnly = false,
    this.fieldBgColor = colorWhite,
    this.maxLength = 100,
    this.inputformate = const [],
    this.keyboardType = TextInputType.name,
    this.showSufixIcon,
    this.showLocationIcon,
    this.containerDecoration,
    this.containerPadding,
    this.ontap,
    this.editorHeight = 0,
    this.onEditingC,
    this.focusnode,
    this.cOnChanged,
    this.leftPadding = 14,
    this.rightPadding = 4,
    this.cMargin =  const EdgeInsets.symmetric(horizontal: 10.0),
    this.suffixIconColor,
    this.textStyle,
    this.contentPadding ,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (lable.isNotEmpty)
          Padding(
            padding: EdgeInsets.only(left: leftPadding, bottom: rightPadding),
            child: Text(
              lable,
              style: lableStyle,
            ),
          ),
        Container(
          decoration: BoxDecoration(
            color: fieldBgColor,
            borderRadius: const BorderRadius.horizontal(
                left: Radius.circular(12), right: Radius.circular(12)),
            border: fieldBgColor == Colors.white
                ? Border.all(color: colorTextFieldBorder, width: 2)
                : null,
          ),
          padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 8),
          margin: cMargin ?? const EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: editorHeight == 0 ? Get.height / 18 : editorHeight,
                  child: TextField(
                    onTap: ontap,
                    minLines: 1,
                    maxLines: maxLines,
                    focusNode: focusnode ?? FocusNode(),
                    onEditingComplete: onEditingC,
                    onChanged: cOnChanged,
                    controller: controller,
                    keyboardType: keyboardType,
                    maxLength: maxLength,
                    buildCounter: null,
                    enabled: isEnabled,
                    readOnly: isReadOnly,
                    onSubmitted: (value) {
                      FocusScope.of(context).unfocus();
                    },
                    inputFormatters: inputformate,
                    style: textStyle ?? TextStyles.kTSFS12W500.copyWith(color: colorBlack),
                    //style: lableStyle,
                    decoration: InputDecoration(
                      hintText: hintText,
                      hintStyle: hintStyle,
                      contentPadding: contentPadding,
                      border: InputBorder.none,
                      suffixIcon: showLocationIcon ??
                          (showSufixIcon == null
                              ? null
                              :  Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Icon(
                                    Icons.keyboard_arrow_down,
                                    color: suffixIconColor ?? Colors.black38,
                                    size: 24.0,
                                  ),
                                )),
                      counterText: '',
                    ),
                  ),
                )
              ]),
        ),
      ],
    );
  }
}