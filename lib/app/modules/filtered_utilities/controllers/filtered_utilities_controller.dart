import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/select_region/controllers/select_region_controller.dart';
import 'package:agritech/app/modules/utility/controllers/utility_controller.dart';
import 'package:agritech/app/modules/utility/data/get_recent_four_utilities_model.dart';
import 'package:agritech/app/modules/utility/services/utility_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../routes/app_pages.dart';

class FilteredUtilitiesController extends BaseController {
  Rx<GetRecentFourUtilitiesModel> filterUtilitiesModel =
      Rx(GetRecentFourUtilitiesModel());

  @override
  void onInit() {
    super.onInit();
  }

  //filter utilities by location API
  Future<void> filterUtilities() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await UtilityRepoImpl(dioClient).filterUtilities(params1: {
        "utilityId": Get.find<HomeController>().selectedLandlordId.value,
        "state": Get.find<SelectRegionController>().stateName.value,
        "district": Get.find<SelectRegionController>().districtName.value,
        "mandal": Get.find<SelectRegionController>().mandalName.value,
        "village": Get.find<SelectRegionController>().villageName.value
      });
      print(
          '===================filter utilities by location======================');
      print(result);
      print(Get.find<SelectRegionController>().stateName.value);
      print(Get.find<SelectRegionController>().districtName.value);
      print(Get.find<SelectRegionController>().mandalName.value);
      print(Get.find<SelectRegionController>().villageName.value);
      result.fold((left) {
        filterUtilitiesLocationHandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print("error$e");
    }
  }

  void filterUtilitiesLocationHandleResponse(
      GetRecentFourUtilitiesModel model) async {
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);

      // Initialize `result` in `filterUtilitiesModel` if it's null
      filterUtilitiesModel.value.result ??= [];

      // Check if `model.result` is null before accessing it
      if (model.result != null) {
        for (var element in model.result!) {
          filterUtilitiesModel.value.result!.add(element);
        }
      } else {
        print("No utilities found in the response.");
      }

      if (Get.find<UtilityController>().isUtility.value == true) {
        print('=======available utilities=========');
        Get.toNamed(Routes.FILTERED_UTILITIES);
      }
    } else {
      Get.toNamed(Routes.FILTERED_UTILITIES);
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
