// To parse this JSON data, do
//
//     final getUtilityPaymentByIdModel = getUtilityPaymentByIdModelFromJson(jsonString);

import 'dart:convert';

GetUtilityPaymentByIdModel getUtilityPaymentByIdModelFromJson(String str) => GetUtilityPaymentByIdModel.fromJson(json.decode(str));

String getUtilityPaymentByIdModelToJson(GetUtilityPaymentByIdModel data) => json.encode(data.toJson());

class GetUtilityPaymentByIdModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetUtilityPaymentByIdModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetUtilityPaymentByIdModel.fromJson(Map<String, dynamic> json) => GetUtilityPaymentByIdModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
   dynamic userId;
   dynamic utilityId;
   dynamic quantity;
   dynamic costOfSelectedItem;
   dynamic gst;
   dynamic totalPayment;
    bool? paymentStatus;
    dynamic razorpayOrderId;
    dynamic razorpayPaymentId;
    dynamic razorpaySignature;
    DateTime? createdAt;
    DateTime? updatedAt;
    UtilityDetails? utilityDetails;

    Result({
        this.id,
        this.userId,
        this.utilityId,
        this.quantity,
        this.costOfSelectedItem,
        this.gst,
        this.totalPayment,
        this.paymentStatus,
        this.razorpayOrderId,
        this.razorpayPaymentId,
        this.razorpaySignature,
        this.createdAt,
        this.updatedAt,
        this.utilityDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        userId: json["userId"],
        utilityId: json["utilityId"],
        quantity: json["quantity"],
        costOfSelectedItem: json["costOfSelectedItem"],
        gst: json["GST"],
        totalPayment: json["totalPayment"],
        paymentStatus: json["paymentStatus"],
        razorpayOrderId: json["razorpay_order_id"],
        razorpayPaymentId: json["razorpay_payment_id"],
        razorpaySignature: json["razorpay_signature"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        utilityDetails: json["utilityDetails"] == null ? null : UtilityDetails.fromJson(json["utilityDetails"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "utilityId": utilityId,
        "quantity": quantity,
        "costOfSelectedItem": costOfSelectedItem,
        "GST": gst,
        "totalPayment": totalPayment,
        "paymentStatus": paymentStatus,
        "razorpay_order_id": razorpayOrderId,
        "razorpay_payment_id": razorpayPaymentId,
        "razorpay_signature": razorpaySignature,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "utilityDetails": utilityDetails?.toJson(),
    };
}

class UtilityDetails {
   int? id;
    dynamic name;
    dynamic description;
   dynamic pricePerDay;
    Address? address;

    UtilityDetails({
        this.id,
        this.name,
        this.description,
        this.pricePerDay,
        this.address,
    });

    factory UtilityDetails.fromJson(Map<String, dynamic> json) => UtilityDetails(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        pricePerDay: json["pricePerDay"],
        address: json["address"] == null ? null : Address.fromJson(json["address"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "pricePerDay": pricePerDay,
        "address": address?.toJson(),
    };
}

class Address {
    dynamic state;
    dynamic district;
    dynamic mandal;
    dynamic village;

    Address({
        this.state,
        this.district,
        this.mandal,
        this.village,
    });

    factory Address.fromJson(Map<String, dynamic> json) => Address(
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
    );

    Map<String, dynamic> toJson() => {
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
    };
}
