import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/detail_saved_landlord_controller.dart';

class DetailSavedLandlordView extends GetView<DetailSavedLandlordController> {
  const DetailSavedLandlordView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'details_of_landlord'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: ()async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20,),
        child: Obx(
          () =>controller.apiUserByProfileIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): 
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CircleAvatar(
                    radius: 25,
                    backgroundColor: colorAsh,
                    child: controller.getUserProfileModel.value.result?.imageUrl != null
                        ? ClipOval(
                            child: Image.network(
                              controller.getUserProfileModel.value.result!.imageUrl!,
                              fit: BoxFit.cover,
                              height: 50,
                              width: 50,
                            ),
                          )
                        : const Icon(Icons.person, size: 40, color: Colors.grey),
                  ),
                  const SizedBox(width: 10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        controller.getUserProfileModel.value.result?.firstName ?? 'N/A',
                        style: TextStyles.kTSDS14W700.copyWith(color: colorPic),
                      ),
                      const SizedBox(height: 5),
                      Text(
                        controller.getUserProfileModel.value.result?.address ?? 'No address provided',
                        style: TextStyles.kTSWFS10W600.copyWith(color: colorDetails),
                      ),
                    ],
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Text(
                'description_about_him'.tr,
                style: TextStyles.kTSDS12W700.copyWith(color: colorDetails),
              ),
              const SizedBox(height: 10),
              Text(
                controller.getUserProfileModel.value.result?.description ?? '',
                style: TextStyles.kTSCF12W500,
              ),
              const SizedBox(height: 10),
              Text(
                'my_lands'.tr,
                style: TextStyles.kTSDS12W700.copyWith(color: colorDetails),
              ),
              Expanded(
                child: Obx(
                  () =>controller.addLandsOfLandlordModels.value.result?.ongoingLands != null &&
                          controller.addLandsOfLandlordModels.value.result!.ongoingLands!.isNotEmpty
                      ? ListView.builder(
                          itemCount: controller.addLandsOfLandlordModels.value.result!.ongoingLands!.length,
                          itemBuilder: (context, index) {
                            final land = controller.addLandsOfLandlordModels.value.result!.ongoingLands![index];
                            return InkWell(
                              onTap: () async {
                                //controller.homeController.landId.value=land.id.toString();
                                //await controller.homeController.getIdLands();
                                Get.toNamed(Routes.DETAILS_OF_LANDLORD_LAND,arguments: {"landId":land.id.toString()});
                              },
                              child: Container(
                                margin: const EdgeInsets.symmetric(vertical: 10),
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(19),
                                  boxShadow: [
                                    BoxShadow(
                                      color: colorBlack.withOpacity(0.2),
                                      blurRadius: 5,
                                      spreadRadius: 1,
                                      offset: const Offset(0, 3),
                                      blurStyle: BlurStyle.inner,
                                    ),
                                  ],
                                  color: colorWhite,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 20),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(11),
                                        child: land.imageUrl != null && land.imageUrl!.isNotEmpty
                                            ? Image.network(
                                                land.imageUrl!,
                                                height: Get.height * 0.08,
                                                width: Get.width * 0.3,
                                                fit: BoxFit.cover,
                                              )
                                            : Image.asset(
                                                agri,
                                                height: 50,
                                                width: 50,
                                              ),
                                      ),
                                      const SizedBox(width: 30),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('${land.fullName} ${'at'.tr}', style: TextStyles.kTSCF12W500),
                                          Text(land.village, style: TextStyles.kTSCF12W500),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        )
                      :  Center(child: Text('no_lands_found'.tr, style: TextStyles.kTSDS12W500)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
