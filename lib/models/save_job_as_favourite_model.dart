// To parse this JSON data, do
//
//     final saveJobAsFavouriteModel = saveJobAsFavouriteModelFromJson(jsonString);

import 'dart:convert';

SaveJobAsFavouriteModel saveJobAsFavouriteModelFromJson(String str) => SaveJobAsFavouriteModel.fromJson(json.decode(str));

String saveJobAsFavouriteModelToJson(SaveJobAsFavouriteModel data) => json.encode(data.toJson());

class SaveJobAsFavouriteModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    SaveJobAsFavouriteModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SaveJobAsFavouriteModel.fromJson(Map<String, dynamic> json) => SaveJobAsFavouriteModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    bool? flag;
    int? id;
    int? userid;
    int? jobid;
    int? landlordid;
    DateTime? updatedAt;
    DateTime? createdAt;
    dynamic farmerid;
    dynamic farmworkerid;
    dynamic role;
    dynamic landid;

    Result({
        this.flag,
        this.id,
        this.userid,
        this.jobid,
        this.landlordid,
        this.updatedAt,
        this.createdAt,
        this.farmerid,
        this.farmworkerid,
        this.role,
        this.landid,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        flag: json["flag"],
        id: json["id"],
        userid: json["userid"],
        jobid: json["jobid"],
        landlordid: json["landlordid"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        farmerid: json["farmerid"],
        farmworkerid: json["farmworkerid"],
        role: json["role"],
        landid: json["landid"],
    );

    Map<String, dynamic> toJson() => {
        "flag": flag,
        "id": id,
        "userid": userid,
        "jobid": jobid,
        "landlordid": landlordid,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "farmerid": farmerid,
        "farmworkerid": farmworkerid,
        "role": role,
        "landid": landid,
    };
}
