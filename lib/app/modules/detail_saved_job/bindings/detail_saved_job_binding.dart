import 'package:get/get.dart';

import '../controllers/detail_saved_job_controller.dart';

class DetailSavedJobBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailSavedJobController>(
      () => DetailSavedJobController(),
    );
  }
}
