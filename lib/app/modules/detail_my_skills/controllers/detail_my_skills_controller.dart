import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../data/get_skills_by_id_model.dart';
import '../data/relevant_skill_jobs_model.dart';
import '../services/detail_my_skills_repo_impl.dart';

class DetailMySkillsController extends BaseController {

  Rx<GetSkillsByIdModel> getSkillsByIdModel = Rx(GetSkillsByIdModel());
  Rx<RelavantSkillJobsModel> relevantSkillJobsModel = Rx(RelavantSkillJobsModel());
  

  final HomeController homeController=Get.find<HomeController>();

  var apiGetSkillsStatus=ApiStatus.LOADING.obs;
  

  @override
  void onInit() {
    super.onInit();
    if(Get.arguments!=null){
      getByIdSkills(id: Get.arguments["id"]);
      getRelevantJobSkills(name: Get.arguments["names"]);
    }
  }

  // get  by id skills API
  Future<void> getByIdSkills({id}) async {
    apiGetSkillsStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailMySkillsRepoImpl(dioClient).getSkillById(id: id);
      print("===================== get by id skills ==============");
      print(result);
      result.fold((model) {
        getSkillsByIdModel.value = model;
        apiGetSkillsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiGetSkillsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  // get relevant skills  jobs API
  Future<void> getRelevantJobSkills({name}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailMySkillsRepoImpl(dioClient).getSkillJobs(name: name);
      print("===================== get relevant skills jobs ==============");
      print(result);
      result.fold((model) {
        relevantSkillJobsModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
