import 'package:agritech/app/modules/detail_my_skills/services/detail_my_skills_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../data/get_skills_by_id_model.dart';
import '../data/relevant_skill_jobs_model.dart';

class DetailMySkillsRepoImpl extends DetailMySkillsRepo with NetworkCheckService{
  final DioClient _dioClient;
  DetailMySkillsRepoImpl(this._dioClient);

  @override
  Future<Either<GetSkillsByIdModel,Exception>> getSkillById({id})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('FarmerWorkerSkills/getSkillById?id=$id', Method.get);
        return result.fold((l){
          GetSkillsByIdModel model=GetSkillsByIdModel.fromJson(l.data);
          return Left(model);
        }, (r)=>  Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<RelavantSkillJobsModel,Exception>> getSkillJobs({name})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('FarmerWorkerSkills/searchJobs?search=$name', Method.get);
        return result.fold((l){
          RelavantSkillJobsModel model=RelavantSkillJobsModel.fromJson(l.data);
          return Left(model);
        }, (r)=>  Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}