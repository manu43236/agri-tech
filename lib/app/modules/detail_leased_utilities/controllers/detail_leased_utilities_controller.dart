import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../utility/data/get_utility_by_id_model.dart';
import '../../utility/services/utility_repo_impl.dart';

class DetailLeasedUtilitiesController extends BaseController {

  Rx<GetUtilityByIdModel> getUtilityByIdModel = Rx(GetUtilityByIdModel());
  var apigetUtilityStatus=ApiStatus.LOADING.obs;
  var utilityVillage=''.obs;
  

  @override
  void onInit() async{
    super.onInit();
    if(Get.arguments != null) {
       await getUtilityById(id: Get.arguments['leaseUtilId']);
    }
   
  }

   //get utility by id API
  Future<void> getUtilityById({id}) async {
    apigetUtilityStatus.value = ApiStatus.LOADING;
    try {
      var result = await UtilityRepoImpl(dioClient).getUtilityById(id: id);
      print("===================== get utility by id ==============");
      print(result);
      result.fold((model) {
        if(model.statusCode==200){
        getUtilityByIdModel.value = model;
        utilityVillage.value=getUtilityByIdModel.value.result!.utility!.village.toString();      
      }
        apigetUtilityStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apigetUtilityStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }


}
