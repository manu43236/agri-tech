// To parse this JSON data, do
//
//     final getVendorCropsModel = getVendorCropsModelFromJson(jsonString);

import 'dart:convert';

GetVendorCropsModel getVendorCropsModelFromJson(String str) => GetVendorCropsModel.fromJson(json.decode(str));

String getVendorCropsModelToJson(GetVendorCropsModel data) => json.encode(data.toJson());

class GetVendorCropsModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<Result>? result;

    GetVendorCropsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetVendorCropsModel.fromJson(Map<String, dynamic> json) => GetVendorCropsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    int? landId;
    int? cropId;
    dynamic quantity;
    dynamic totalPayment;
    dynamic paymentStatus;
    List<AgriYield>? agriYields;
    CropDetails? cropDetails;

    Result({
        this.id,
        this.landId,
        this.cropId,
        this.quantity,
        this.totalPayment,
        this.paymentStatus,
        this.agriYields,
        this.cropDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        landId: json["landId"],
        cropId: json["cropId"],
        quantity: json["quantity"],
        totalPayment: json["totalPayment"],
        paymentStatus: json["paymentStatus"],
        agriYields: json["agriYields"] == null ? [] : List<AgriYield>.from(json["agriYields"]!.map((x) => AgriYield.fromJson(x))),
        cropDetails: json["cropDetails"] == null ? null : CropDetails.fromJson(json["cropDetails"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landId": landId,
        "cropId": cropId,
        "quantity": quantity,
        "totalPayment": totalPayment,
        "paymentStatus": paymentStatus,
        "agriYields": agriYields == null ? [] : List<dynamic>.from(agriYields!.map((x) => x.toJson())),
        "cropDetails": cropDetails?.toJson(),
    };
}

class AgriYield {
    int? id;
    int? landid;
    int? cropId;
    dynamic outedDate;

    AgriYield({
        this.id,
        this.landid,
        this.cropId,
        this.outedDate,
    });

    factory AgriYield.fromJson(Map<String, dynamic> json) => AgriYield(
        id: json["id"],
        landid: json["landid"],
        cropId: json["cropId"],
        outedDate: json["outed_date"] == null ? null : DateTime.parse(json["outed_date"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landid": landid,
        "cropId": cropId,
        "outed_date": outedDate?.toIso8601String(),
    };
}

class CropDetails {
    dynamic name;
    dynamic costPerHector;
    dynamic imageUrl;
    dynamic totaldays;

    CropDetails({
        this.name,
        this.costPerHector,
        this.imageUrl,
        this.totaldays,
    });

    factory CropDetails.fromJson(Map<String, dynamic> json) => CropDetails(
        name: json["name"],
        costPerHector: json["costPerHector"],
        imageUrl: json["image_url"],
        totaldays: json["totaldays"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "costPerHector": costPerHector,
        "image_url": imageUrl,
        "totaldays": totaldays,
    };
}
