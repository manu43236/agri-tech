import 'package:get/get.dart';

import '../../details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import '../../instrument_details/controllers/instrument_details_controller.dart';
import '../controllers/detail_saved_utilities_controller.dart';

class DetailSavedUtilitiesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailSavedUtilitiesController>(
      () => DetailSavedUtilitiesController(),
    );
    Get.lazyPut<InstrumentDetailsController>(
      () => InstrumentDetailsController(),
    );
    Get.lazyPut<DetailsOfLandlordUtilitiesController>(
      () => DetailsOfLandlordUtilitiesController(),
    );
  }
}
