import 'package:agritech/app/modules/dashboard/controllers/dashboard_controller.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:get/get.dart';

import '../../farm_worker_home_page/controllers/farm_worker_controller.dart';
import '../controllers/notifications_controller.dart';

class NotificationsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<NotificationsController>(
      () => NotificationsController(),
       fenix: true
    );
     Get.lazyPut<HomeController>(
      () => HomeController(),
       fenix: true
    );
    Get.lazyPut<DashboardController>(
      () => DashboardController(),
       fenix: true
    );
    Get.lazyPut<FarmWorkerHomePageController>(
      () => FarmWorkerHomePageController(),
       fenix: true
    );
  }
}
