import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/conts/img_const.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/welcome_page_controller.dart';

class WelcomePageView extends GetView<WelcomePageController> {
  const WelcomePageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
         Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Image.asset(logo),
              Image.asset(controller.welcomeFarmer,height: Get.height*0.5,),
              const SizedBox(height: 40),
              // Text(
              //  // 'Agri Insights',
              //   controller.output.value.isNotEmpty
              //       ? controller.output.value
              //       : "Welcome to Agritech!",
              //   style: TextStyles.kTSFS26W400,
              // ),
              Center(child: Image.asset(agriLogo,width: Get.width*0.5,height: Get.height*0.15,)),
              const SizedBox(height: 35),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "already_have_an_account?".tr,
                    style: TextStyles.kTSFS12W400,
                  ),
                  InkWell(
                    onTap: () {
                      Get.toNamed(Routes.SIGNUP);
                    },
                    child: Text(
                      "create_an_account".tr,
                      style: TextStyles.kTSFS12WBOLD.copyWith(color: colorBlue),
                    ),
                  ),
                  Text(
                    "here".tr,
                    style: TextStyles.kTSFS12W400.copyWith(color: primary),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              InkWell(
                onTap: () {
                  Get.toNamed(Routes.LOGIN);
                },
                child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                      margin:const EdgeInsets.symmetric(horizontal: 20,vertical: 10) ,
                      width: double.infinity,
                      decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(11),
                         color: primary,
                      ),
                      child: Center(child: Text('login'.tr,style: TextStyles.kTSFS18W600.copyWith(color:  const Color(0xffF8F8FF)),)),
                    ),
              ),
            ],
          ),
      
    );
  }
}
