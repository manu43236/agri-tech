// To parse this JSON data, do
//
//     final getLeasedLandsDetailsModel = getLeasedLandsDetailsModelFromJson(jsonString);

import 'dart:convert';

GetLeasedLandsDetailsModel getLeasedLandsDetailsModelFromJson(String str) => GetLeasedLandsDetailsModel.fromJson(json.decode(str));

String getLeasedLandsDetailsModelToJson(GetLeasedLandsDetailsModel data) => json.encode(data.toJson());

class GetLeasedLandsDetailsModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetLeasedLandsDetailsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetLeasedLandsDetailsModel.fromJson(Map<String, dynamic> json) => GetLeasedLandsDetailsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    Farmer? farmer;
    List<Land>? lands;

    Result({
        this.farmer,
        this.lands,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        farmer: json["farmer"] == null ? null : Farmer.fromJson(json["farmer"]),
        lands: json["lands"] == null ? [] : List<Land>.from(json["lands"]!.map((x) => Land.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "farmer": farmer?.toJson(),
        "lands": lands == null ? [] : List<dynamic>.from(lands!.map((x) => x.toJson())),
    };
}

class Farmer {
    int? id;
    dynamic name;
    dynamic mobileNumber;
    dynamic address;
    dynamic image;

    Farmer({
        this.id,
        this.name,
        this.mobileNumber,
        this.address,
        this.image,
    });

    factory Farmer.fromJson(Map<String, dynamic> json) => Farmer(
        id: json["id"],
        name: json["name"],
        mobileNumber: json["mobileNumber"],
        address: json["address"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "mobileNumber": mobileNumber,
        "address": address,
        "image": image,
    };
}

class Land {
    int? id;
    dynamic details;
    dynamic location;
    dynamic surveyNumber;
    dynamic image;

    Land({
        this.id,
        this.details,
        this.location,
        this.surveyNumber,
        this.image,
    });

    factory Land.fromJson(Map<String, dynamic> json) => Land(
        id: json["id"],
        details: json["details"],
        location: json["location"],
        surveyNumber: json["surveyNumber"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "details": details,
        "location": location,
        "surveyNumber": surveyNumber,
        "image": image,
    };
}
