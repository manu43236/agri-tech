// To parse this JSON data, do
//
//     final getCropByIdModel = getCropByIdModelFromJson(jsonString);

import 'dart:convert';

GetCropByIdModel getCropByIdModelFromJson(String str) => GetCropByIdModel.fromJson(json.decode(str));

String getCropByIdModelToJson(GetCropByIdModel data) => json.encode(data.toJson());

class GetCropByIdModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    GetCropByIdModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetCropByIdModel.fromJson(Map<String, dynamic> json) => GetCropByIdModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    String? name;
    int? totaldays;
    DateTime? createdAt;
    DateTime? updatedAt;

    Result({
        this.id,
        this.name,
        this.totaldays,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        name: json["name"],
        totaldays: json["totaldays"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "totaldays": totaldays,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
