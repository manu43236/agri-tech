import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../saved/data/get_all_saved_farmer_model.dart';
import '../../saved/services/saved_repo_impl.dart';

class SavedFarmersController extends BaseController {

   Rx<GetAllSavedFarmerModel> getAllSavedFarmerModel =
      Rx(GetAllSavedFarmerModel());

  var userId=''.obs;

  @override
  void onInit() async{
    super.onInit();
    userId.value = await SecureStorage().readData(key: "id") ?? "";
    allFarmersSaved(id: userId.value);
  }

  Future<void> allFarmersSaved({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient)
          .savedFarmersList(userId: id);
      result.fold(
        (model) {
          getAllSavedFarmerModel.value = model;
          apiStatus.value = ApiStatus.SUCCESS;
        },(r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
