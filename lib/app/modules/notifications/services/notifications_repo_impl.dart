import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/notifications/controllers/notifications_controller.dart';
import 'package:agritech/app/modules/notifications/data/update_flag_model.dart';
import 'package:agritech/app/modules/notifications/services/notifications_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../../../../models/accept_by_farmworker_model.dart';
import '../../../../models/get_all_notifications_of_Farmworker_model.dart';
import '../../../../models/send_request_by_farmworker_model.dart';
import '../data/accept_by_landlord_or_farmer_for_job_taken_farmworkermodel.dart';
import '../data/accept_request_by_farmer_or_landlord_model.dart';
import '../data/get_notifications_model.dart';
import '../data/notifications_count_model.dart';
import '../data/reply_to_farmer_by_landlord_model.dart';
import '../data/respond_by_farmworker_to_landlord_or_farmer_model.dart';
import '../data/send_lease_interest_farmer_notification_model.dart';

class NotificationsRepoImpl extends NotificationsRepo with NetworkCheckService{
  final DioClient _dioClient;
  NotificationsRepoImpl(this._dioClient);

   Future<Either<GetAllNotificationsByFarmworkerModel,Exception>> getAllFarmworkerNotifications({params1})async{
      var data = await checkInternet();
      if(!data){
        return Right(Exception("No Network found"));
      }else{
        try{
          var id =Get.find<HomeController>().selectedLandlordId.value;
          var result = await _dioClient.requestForAuth('notification/getNotificationsForFarmworker?farmworkerId=$id', Method.get);
          return result.fold((l){
            GetAllNotificationsByFarmworkerModel model = GetAllNotificationsByFarmworkerModel.fromJson(l.data);
            return Left(model);
          }, (r)=>Right(Exception(r)));
        }catch(e){
          return Right(Exception(e));
        }
      }
   }

  Future<Either<SendRequestByFarmworkerModel,Exception>> sendRequestByFarmworkerModel({params1})async{
     var data = await checkInternet();
      if(!data){
        return Right(Exception("No Network found"));
      }else{
        try{
          var result = await _dioClient.requestForAuth('notification/sendRequestByFarmworker', Method.post,params: params1);
          return result.fold((l){
            SendRequestByFarmworkerModel model = SendRequestByFarmworkerModel.fromJson(l.data);
            return Left(model);
          }, (r)=>Right(Exception(r)));
        }catch(e){
          return Right(Exception(e));
        }
      }
  }

   @override
   Future<Either<dynamic,Exception>>  viewedFarmworkerNotifications({params1})async{
      var data = await checkInternet();
      if(!data){
        return Right(Exception("No Network found"));
      }else{
        try{
          var result= await _dioClient.requestForAuth('notification/markAsViewed', Method.get,params: params1);
          return result.fold((l){
           return Left(l.data);  //l['message']
          }, (r)=> Right(Exception(r)));
        }catch(e){
          return Right(Exception(e));
        }
      }
   }

   @override
   Future<Either<AcceptByFarmworkerModel,Exception>> acceptFarmworkerNotification({params1})async{
     var data = await checkInternet();
     if(!data){
      return Right(Exception("No Network found"));
     }else{
      try{
        var result = await _dioClient.requestForAuth('notification/acceptByFarmworker', Method.post,params: params1);
        return result.fold((l){
          AcceptByFarmworkerModel model=AcceptByFarmworkerModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
     }
   }
   
   @override
   Future<Either<UpdateFlagModel,Exception>> updateFlag() async{
      var data = await checkInternet();
      if(!data){
        return Right(Exception("No Network found"));
      }else{
        try{
          //var id =Get.find<HomeController>().selectedLandlordId.value;
          var result = await _dioClient.requestForAuth('notification/updateFlag?notificationId=${Get.find<NotificationsController>().updatedId.value}', Method.get);
          return result.fold((l){
            UpdateFlagModel model = UpdateFlagModel.fromJson(l.data);
            return Left(model);
          }, (r)=>Right(Exception(r)));
        }catch(e){
          return Right(Exception(e));
        }
      }
   }

   @override
    Future<Either<SendLeaseInterestNotificationByFarmerModel,Exception>> sendLeaseInterest({params1})async{
     var data = await checkInternet();
     if(!data){
      return Right(Exception("No Network found"));
     }else{
      try{
        var result = await _dioClient.requestForAuth('notification/sendLeaseInterestNotificationByFarmer', Method.post,params: params1);
        return result.fold((l){
          SendLeaseInterestNotificationByFarmerModel model=SendLeaseInterestNotificationByFarmerModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
     }
   }

   @override
   Future<Either<NotificationsCountModel,Exception>> getCount() async{
      var data = await checkInternet();
      if(!data){
        return Right(Exception("No Network found"));
      }else{
        try{
          var result = await _dioClient.requestForAuth('notification/UnreadNotificationsCount?user_id=${Get.find<HomeController>().userId.value}', Method.get);
          return result.fold((l){
            NotificationsCountModel model = NotificationsCountModel.fromJson(l.data);
            return Left(model);
          }, (r)=>Right(Exception(r)));
        }catch(e){
          return Right(Exception(e));
        }
      }
   }

   @override
   Future<Either<GetNotificationsModel,Exception>> getAllNotifications({userId}) async{
      var data = await checkInternet();
      if(!data){
        return Right(Exception("No Network found"));
      }else{
        try{
          var result = await _dioClient.requestForAuth('notification/getNotifications?userId=$userId', Method.get);
          return result.fold((l){
            GetNotificationsModel model = GetNotificationsModel.fromJson(l.data);
            return Left(model);
          }, (r)=>Right(Exception(r)));
        }catch(e){
          return Right(Exception(e));
        }
      }
   }

    @override
     Future<Either<SendRequestByFarmworkerModel,Exception>> sendRequestByFarmworker({params1})async{
     var data = await checkInternet();
     if(!data){
      return Right(Exception("No Network found"));
     }else{
      try{
        var result = await _dioClient.requestForAuth('notification/sendRequestByFarmworker', Method.post,params: params1);
        return result.fold((l){
          SendRequestByFarmworkerModel model=SendRequestByFarmworkerModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
     }
   }

   @override
   Future<Either<RespondByFarmworkerToLandlordOrFarmerModel,Exception>> respondByFarmworker() async{
      var data = await checkInternet();
      if(!data){
        return Right(Exception("No Network found"));
      }else{
        try{
          var result = await _dioClient.requestForAuth('notification/respondByFarmworkerToLandlordOrFarmer?farmworkerId=${Get.find<NotificationsController>().farmworkerIds.value}&senderId=${Get.find<NotificationsController>().toSenderId.value}&jobId=${Get.find<NotificationsController>().jobIds.value}', Method.get);
          return result.fold((l){
            RespondByFarmworkerToLandlordOrFarmerModel model = RespondByFarmworkerToLandlordOrFarmerModel.fromJson(l.data);
            return Left(model);
          }, (r)=>Right(Exception(r)));
        }catch(e){
          return Right(Exception(e));
        }
      }
   }

   @override
   Future<Either<AcceptbylandlordOrFarmerForJobTakenFarmworkerModel,Exception>> acceptLandlordOrFarmer() async{
      var data = await checkInternet();
      if(!data){
        return Right(Exception("No Network found"));
      }else{
        try{
          var result = await _dioClient.requestForAuth('notification/acceptbylandlordOrFarmerForJobTakenFarmworker?notificationId=${Get.find<NotificationsController>().notificationId.value}', Method.get);
          return result.fold((l){
            AcceptbylandlordOrFarmerForJobTakenFarmworkerModel model = AcceptbylandlordOrFarmerForJobTakenFarmworkerModel.fromJson(l.data);
            return Left(model);
          }, (r)=>Right(Exception(r)));
        }catch(e){
          return Right(Exception(e));
        }
      }
   }

   @override
   Future<Either<AcceptRequestByFarmerOrLandlordModel,Exception>> acceptRequestSentByWorker({params1})async{
     var data = await checkInternet();
     if(!data){
      return Right(Exception("No Network found"));
     }else{
      try{
        var result = await _dioClient.requestForAuth('notification/acceptRequestByFarmerOrLandlord', Method.post,params: params1);
        return result.fold((l){
          AcceptRequestByFarmerOrLandlordModel model=AcceptRequestByFarmerOrLandlordModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
     }
   }

   @override
   Future<Either<ReplyToFarmerByLandlordModel,Exception>> replyToFarmer({params1})async{
     var data = await checkInternet();
     if(!data){
      return Right(Exception("No Network found"));
     }else{
      try{
        var result = await _dioClient.requestForAuth('notification/replyToFarmerByLandlord', Method.post,params: params1);
        return result.fold((l){
          ReplyToFarmerByLandlordModel model=ReplyToFarmerByLandlordModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
     }
   }
}