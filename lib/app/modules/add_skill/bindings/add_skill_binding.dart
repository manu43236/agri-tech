import 'package:get/get.dart';

import '../controllers/add_skill_controller.dart';

class AddSkillBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AddSkillController>(
      () => AddSkillController(),
    );
  }
}
