import 'package:get/get.dart';

import '../controllers/saved_lands_controller.dart';

class SavedLandsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SavedLandsController>(
      () => SavedLandsController(),
    );
  }
}
