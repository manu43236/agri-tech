import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/instrument_details_controller.dart';

class InstrumentDetailsView extends GetView<InstrumentDetailsController> {
  const InstrumentDetailsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title: Text(
          'Instrument Details',
           style:  TextStyles.kTSFS24W600.copyWith(color: colorDetails),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: ()async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20,),
        child: Obx(
          () => SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(11),
                  child: Image.network(
                    controller.getUtilityByIdModel.value.result?.utility?.image ??
                        defaultImageUrl,
                    height: Get.height * 0.35,
                    width: Get.width,
                    fit: BoxFit.cover,
                    errorBuilder: (context, error, stackTrace) {
                      return Image.asset(
                        defaultImageUrl,
                        height: Get.height * 0.35,
                        width: Get.width,
                        fit: BoxFit.cover,
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20),
                Text(
                  controller.getUtilityByIdModel.value.result?.utility?.nameOfInstrument ?? 'N/A',
                  style: TextStyles.kTSFS20W700.copyWith(color: colorHello),
                ),
                Text(
                  '${controller.getUtilityByIdModel.value.result?.utility?.mandal ?? 'N/A'} , ${controller.getUtilityByIdModel.value.result?.utility?.village ?? 'N/A'}',
                  style: TextStyles.kTSFS14W700.copyWith(color: colorHello),
                ),
                Text(
                  'Price : ${controller.getUtilityByIdModel.value.result?.utility?.pricePerDay ?? 'N/A'}',
                  style: TextStyles.kTSFS14W700.copyWith(color: colorHello),
                ),
                const SizedBox(height: 10),
                Text(
                  'Description About him',
                  style: TextStyles.kTSDS12W700.copyWith(color: colorDetails),
                ),
                const SizedBox(height: 10),
                Text(
                  controller.getUtilityByIdModel.value.result?.utility?.description ?? 'No description available',
                  style: TextStyles.kTSCF12W500.copyWith(color: colorBlack),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
