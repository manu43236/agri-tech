import 'package:agritech/app/modules/available_farmer_my_lands/services/available_farmer_my_lands_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../available_recent_lands/data/recent_filter_model.dart';
import '../../available_recent_lands/services/available_recent_lands_repo_impl.dart';
import '../../home/controllers/home_controller.dart';
import '../../home/data/get_all_lands_by_farmer_model.dart';

class AvailableFarmerMyLandsController extends BaseController {

  Rx<GetAllLandsByFarmerModel> getLandsByFarmerModel =
      Rx(GetAllLandsByFarmerModel());
  Rx<RecentFilterModel> farmerMyLandsFilterModel = Rx(RecentFilterModel());

  var name = "".obs;
  var farmerLandsSearch=''.obs;
  var dataSelectedItem=''.obs;
  var apiFarLandsStatus = ApiStatus.LOADING.obs;

  var isLoan=false.obs;
  var isInsurance=false.obs;

  @override
  void onInit() {
    if (Get.arguments != null) {
      name.value = Get.arguments["heading"];
      isLoan.value=Get.arguments["isLoan"];
      isInsurance.value=Get.arguments["isInsurance"];
    }
    super.onInit();
    //getFarmersLands();
    farmerLandsFilterLandsList(farmerId:Get.find<HomeController>().userId.value,
    landlordId: '',search: farmerLandsSearch.value,state: '',district: '',mandal: '',village: '' );
  }

  // Home Page ALL Lands of farmer flow API
  Future<void> getFarmersLands() async {
    apiFarLandsStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await AvailableFarmerMyLandsRepoImpl(dioClient).getFarmerLandsSearch();
      print('============hey=============');
      print(result);
      result.fold((left) {
        allFarmerLandshandleResponse(left);
        apiFarLandsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiFarLandsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void allFarmerLandshandleResponse(GetAllLandsByFarmerModel model) async {
    if (model.statusCode == 200) {
   
      getLandsByFarmerModel.value = model;
     
    }else if(model.statusCode==400){
     
      getLandsByFarmerModel.value.result!.ongoingLands!.clear();
      getLandsByFarmerModel.value.result!.ongoingLands ==[];
    }
  }

   void farmerLandsFilterLandsList({landlordId,farmerId,search,state,district,mandal,village}) async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result = await AvailableRecentLandsRepoImpl(dioClient).filterRecentLocations(landlordId: landlordId,farmerId: farmerId,search: search, state: state,district: district,mandal: mandal,village: village);
      print('=============== get all filter farmer my lands list =========');
      print(result);
      result.fold((left) {
        recentFilterLandshandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void recentFilterLandshandleResponse(RecentFilterModel model) async {
    if (model.statusCode == 200) {
      farmerMyLandsFilterModel.value.result?.clear();
      farmerMyLandsFilterModel.value = model;
    }else if(model.statusCode==400){
      print('===============status=============');
      farmerMyLandsFilterModel.value.result?.clear();
      farmerMyLandsFilterModel.value.result! ==[];
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
