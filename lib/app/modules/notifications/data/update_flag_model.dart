// To parse this JSON data, do
//
//     final updateFlagModel = updateFlagModelFromJson(jsonString);

import 'dart:convert';

UpdateFlagModel updateFlagModelFromJson(String str) => UpdateFlagModel.fromJson(json.decode(str));

String updateFlagModelToJson(UpdateFlagModel data) => json.encode(data.toJson());

class UpdateFlagModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    UpdateFlagModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory UpdateFlagModel.fromJson(Map<String, dynamic> json) => UpdateFlagModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    dynamic message;
    dynamic name;
    dynamic from;
    dynamic to;
    bool? isRead;
    dynamic statusForLandlord;
    dynamic statusForFarmer;
    dynamic imageUrl;
    int? landId;
    dynamic jobId;
    dynamic statusForFarmworker;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;

    Result({
        this.id,
        this.message,
        this.name,
        this.from,
        this.to,
        this.isRead,
        this.statusForLandlord,
        this.statusForFarmer,
        this.imageUrl,
        this.landId,
        this.jobId,
        this.statusForFarmworker,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        message: json["message"],
        name: json["name"],
        from: json["from"],
        to: json["to"],
        isRead: json["is_read"],
        statusForLandlord: json["status_for_landlord"],
        statusForFarmer: json["status_for_farmer"],
        imageUrl: json["image_url"],
        landId: json["landId"],
        jobId: json["jobId"],
        statusForFarmworker: json["status_for_farmworker"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "message": message,
        "name": name,
        "from": from,
        "to": to,
        "is_read": isRead,
        "status_for_landlord": statusForLandlord,
        "status_for_farmer": statusForFarmer,
        "image_url": imageUrl,
        "landId": landId,
        "jobId": jobId,
        "status_for_farmworker": statusForFarmworker,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
