import 'package:agritech/app/modules/edit_profile/services/edit_profile_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:agritech/models/profile_model.dart';
import 'package:dartz/dartz.dart';


class EditProfileRepoImpl extends  EditProfileRepo with NetworkCheckService {
  final DioClient _dioClient;

  EditProfileRepoImpl(this._dioClient);

  @override
  Future<Either<ProfileModel, Exception>> doEditProfile({params1}) async {
    bool data = await checkInternet();

    if (!data) {
      return Right(Exception('No Network found'));
    } else {
      try {
        print(params1);
        var result = await _dioClient.requestForAuth('User/updateUser', Method.post,
            params: params1);
        return result.fold((l) {
          ProfileModel model = ProfileModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      } catch (e) {
        return Right(Exception(e));
      }
    }
  }
}