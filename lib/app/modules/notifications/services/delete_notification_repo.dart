import 'package:dartz/dartz.dart';

abstract class DeleteNotificationRepo{
  Future<Either<dynamic,Exception>>  deleteNotification({params1});
}