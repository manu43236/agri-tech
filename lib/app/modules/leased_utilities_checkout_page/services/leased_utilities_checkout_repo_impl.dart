import 'package:agritech/app/modules/leased_utilities_checkout_page/services/leased_utilities_checkout_repo.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../data/get_utility_ninty_percent_details_model.dart';

class LeasedUtilitiesCheckoutRepoImpl extends LeasedUtilitiesCheckoutRepo with NetworkCheckService{
  final DioClient _dioClient;
  LeasedUtilitiesCheckoutRepoImpl(this._dioClient);

  @override
  Future<Either<GetUtilityNintyPercentDetailsModel,Exception>> getNintyDetails({id})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('utilityPayment/getUtilityninetypercentPaymentDetails?utilityId=$id&userId=${Get.find<HomeController>().userId.value}', Method.get);
        return result.fold((l){
          GetUtilityNintyPercentDetailsModel model=GetUtilityNintyPercentDetailsModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}