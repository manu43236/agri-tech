import 'package:get/get.dart';

import '../../detail_crop/controllers/detail_crop_controller.dart';
import '../controllers/detail_purchased_crop_controller.dart';

class DetailPurchasedCropBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailPurchasedCropController>(
      () => DetailPurchasedCropController(),
    );
    Get.lazyPut<DetailCropController>(
      () => DetailCropController(),
    );
  }
}
