import 'package:agritech/app/modules/landlord_utilities_checkout_page/controllers/landlord_utilities_checkout_page_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/leased_utilities_checkout_page_controller.dart';

class LeasedUtilitiesCheckoutPageView
    extends GetView<LeasedUtilitiesCheckoutPageController> {
  const LeasedUtilitiesCheckoutPageView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'checkout'.tr,
           style:  TextStyles.kTSFS24W600.copyWith(color: colorSuitable, fontWeight: FontWeight.w800),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: ()async {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => 
        controller.apiStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer():
        Padding(
          padding: const EdgeInsets.only(left: 25, right: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.network(
                      controller.getUtilityNintyPercentDetailsModel.value.result?.utilityDetails?.image??'',
                      height: Get.height * 0.18,
                      width: Get.width,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        // Display a local asset if the network image fails
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.18,
                          width: Get.width,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                  ),
              const SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          controller.getUtilityNintyPercentDetailsModel.value.result!.utilityDetails!.name??
                              '',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyles.kTSFS20W700
                              .copyWith(color: colorSuitable),
                        ),
                        const SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                '${controller.getUtilityNintyPercentDetailsModel.value.result!.utilityDetails!.address!.mandal ?? ''} , ${controller.getUtilityNintyPercentDetailsModel.value.result!.utilityDetails!.address!.village ?? ''}',
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                style: TextStyles.kTSDS14W500
                                    .copyWith(color: colorSuitable),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Flexible(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      
                      const SizedBox(
                        width: 5,
                      ),
                      Text(
                        controller.getUtilityNintyPercentDetailsModel.value.result!.paymentDetails!.quantity.toString() ??
                            '',
                        style: TextStyles.kTSFS20W700
                            .copyWith(color: colorSuitable, fontSize: 20),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                     
                    ],
                  )),
                ],
              ),
              const SizedBox(height: 50,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('total_cost_of_selected_item'.tr,
                      style: TextStyles.kTSDS14W500
                          .copyWith(color: colorSuitable)),
                  Text(
                    '${'rs'.tr} ${controller.getUtilityNintyPercentDetailsModel.value.result!.paymentDetails!.costOfSelectedItem.toString() ?? "-"}',
                    style:
                        TextStyles.kTSDS14W500.copyWith(color: colorSuitable),
                  )
                ],
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('payment_made'.tr,
                      style: TextStyles.kTSDS14W500
                          .copyWith(color: colorSuitable)),
                  Text(
                      '${'rs'.tr} ${controller.getUtilityNintyPercentDetailsModel.value.result!.paymentDetails!.tenPercentPrice ?? "-"}',
                      style:
                          TextStyles.kTSDS14W500.copyWith(color: colorSuitable))
                ],
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('balance_payment'.tr,
                      style: TextStyles.kTSDS14W500
                          .copyWith(color: colorSuitable)),
                  Text(
                      '${'rs'.tr} ${controller.getUtilityNintyPercentDetailsModel.value.result!.paymentDetails!.ninetyPercentPrice}',
                      style:
                          TextStyles.kTSDS14W500.copyWith(color: colorSuitable))
                ],
              ),
              const SizedBox(height: 20),
              const Divider(
                thickness: 1,
                color: colorBlack,
              ),
              const SizedBox(height: 20),
              // Price and Total
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('amount_to_be_paid'.tr,
                      style: TextStyles.kTSDS14W500
                          .copyWith(color: colorSuitable)),
                  Text(
                    '${'rs'.tr} ${controller.getUtilityNintyPercentDetailsModel.value.result!.paymentDetails!.ninetyPercentPrice ?? "-"}',
                    style:
                        TextStyles.kTSDS14W500.copyWith(color: colorSuitable),
                  )
                ],
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('gst'.tr,
                      style: TextStyles.kTSDS14W500
                          .copyWith(color: colorSuitable)),
                  Text(
                      '${'rs'.tr} ${controller.getUtilityNintyPercentDetailsModel.value.result!.paymentDetails!.nintyPercentGst ?? "-"}',
                      style:
                          TextStyles.kTSDS14W500.copyWith(color: colorSuitable))
                ],
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('total'.tr,
                      style: TextStyles.kTSDS14W500
                          .copyWith(color: colorSuitable)),
                  Text(
                      '${'rs'.tr} ${controller.getUtilityNintyPercentDetailsModel.value.result!.paymentDetails!.nintyPercentpayment!}',
                      style:
                          TextStyles.kTSDS14W500.copyWith(color: colorSuitable))
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              Center(
                child: InkWell(
                  onTap: () async{
                    await Get.find<LandlordUtilitiesCheckoutPageController>().utilityPayment(
                            id: controller
                                .getUtilityNintyPercentDetailsModel
                                .value
                                .result!
                                .paymentDetails!.id);
                        Get.find<LandlordUtilitiesCheckoutPageController>().openCheckout(Get.find<LandlordUtilitiesCheckoutPageController>().amount.value);
                    //controller.openCheckout(controller.amount.value);
                  },
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(13),
                        color: primary),
                    child: Text(
                      'make_a_payment'.tr,
                      style: TextStyles.kTSFS16W700.copyWith(color: colorWhite),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
