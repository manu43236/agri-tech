import 'package:agritech/app/modules/detail_recent_lands/services/detail_recent_lands_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../../home/data/recent_lands_model.dart';
import '../data/land_by_id_model.dart';

class DetailRecentLandsRepoImpl extends DetailRecentLandsRepo with NetworkCheckService{
  final DioClient _dioClient;
  DetailRecentLandsRepoImpl(this._dioClient);

  @override
  Future<Either<LandByIdModel,Exception>> getLand({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("NO Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Land/getLandById', Method.get,params: params1);
        return result.fold((l){
          LandByIdModel model=LandByIdModel.fromJson(l.data);
          return Left(model);
        },(r)=>Right(Exception(r)));
      }catch(e){
        return right(Exception(e));
      }
    }
  }

  @override
  Future<Either<RecentLandsModel,Exception>>  filterLocations({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Land/filterLandsByLocation', Method.post,params: params1);
        return result.fold((l){
          RecentLandsModel model=RecentLandsModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}