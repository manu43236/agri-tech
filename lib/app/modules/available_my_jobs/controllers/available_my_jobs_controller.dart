import 'package:agritech/app/modules/available_my_jobs/data/jobs_filter_model.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../home/data/all_jobs_of_landlord_model.dart';
import '../services/available_my_jobs_repo_impl.dart';

class AvailableMyJobsController extends BaseController {

Rx<AllJobsOfLandlordModel> getJobsOfLandlordModel =
      Rx(AllJobsOfLandlordModel());
Rx<JobsFilterModel> myJobsFilterModel = Rx(JobsFilterModel());

 var name = "".obs;
 var myJobsSearch=''.obs;
 var apiLordJobsStatus=ApiStatus.LOADING.obs;
 var apiFilterJobsStatus=ApiStatus.LOADING.obs;
 var dataSelectedItem = "".obs;

  @override
  void onInit() {
    if (Get.arguments != null) {
      name.value = Get.arguments["heading"];
    }
    super.onInit();
    //getAllLandlordJobs();
    myJobsFilterJobsList(landlordId: Get.find<HomeController>().userId.value,
     farmerId: '',search: myJobsSearch.value,state: '',district: '',mandal: '',village: '' );
  }

  // my jobs search View API
  Future<void> getAllLandlordJobs() async {
    apiLordJobsStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await AvailableMyJobsRepoImpl(dioClient).getJobSearch();
      print('llllllllllllllllllllllllllllllllllllllllll');
      print('results:$result');
      result.fold((left) {
        allLandshandleResponse(left);
        apiLordJobsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiLordJobsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void allLandshandleResponse(AllJobsOfLandlordModel model) async {
    print('zzzzzzzzzzzzzzzz');
    print(model);
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
      // getJobsOfLandlordModel.value.result?.ongoingJobs?.clear();
      getJobsOfLandlordModel.value = model;
      print('77777777777777777777777');
      print(getJobsOfLandlordModel.value.result!.ongoingJobs![0].description);
    }else if(model.statusCode==400){
      print('===============status=============');
      getJobsOfLandlordModel.value.result!.ongoingJobs!.clear();
      getJobsOfLandlordModel.value.result!.ongoingJobs ==[];
    }
  }

  void myJobsFilterJobsList({landlordId,farmerId,search,state,district,mandal,village}) async {
    apiFilterJobsStatus.value = ApiStatus.LOADING;

    try {
      var result = await AvailableMyJobsRepoImpl(dioClient).filterOwnJobs(landlordId: landlordId,farmerId: farmerId,search: search, state: state,district: district,mandal: mandal,village: village);
      print('=============== get all filter my jobs list =========');
      print(result);
      result.fold((left) {
        filterJobshandleResponse(left);
        apiFilterJobsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiFilterJobsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void filterJobshandleResponse(JobsFilterModel model) async {
    if (model.statusCode == 200) {
      myJobsFilterModel.value.result?.clear();
      myJobsFilterModel.value = model;
    }else if(model.statusCode==400){
      print('===============status=============');
      myJobsFilterModel.value.result?.clear();
      myJobsFilterModel.value.result! ==[];
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
