// To parse this JSON data, do
//
//     final getAllCropsModel = getAllCropsModelFromJson(jsonString);

import 'dart:convert';

GetAllCropsModel getAllCropsModelFromJson(String str) => GetAllCropsModel.fromJson(json.decode(str));

String getAllCropsModelToJson(GetAllCropsModel data) => json.encode(data.toJson());

class GetAllCropsModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<Result>? result;

    GetAllCropsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetAllCropsModel.fromJson(Map<String, dynamic> json) => GetAllCropsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    dynamic name;
    dynamic totaldays;
    dynamic imageUrl;
    dynamic costPerHector;
    dynamic createdAt;
    dynamic updatedAt;

    Result({
        this.id,
        this.name,
        this.totaldays,
        this.imageUrl,
        this.costPerHector,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        name: json["name"],
        totaldays: json["totaldays"],
        imageUrl: json["image_url"],
        costPerHector: json["costPerHector"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "totaldays": totaldays,
        "image_url": imageUrl,
        "costPerHector": costPerHector,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
