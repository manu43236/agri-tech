import 'package:get/get.dart';

import '../../details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import '../../select_region/controllers/select_region_controller.dart';
import '../controllers/available_my_utilities_controller.dart';

class AvailableMyUtilitiesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailableMyUtilitiesController>(
      () => AvailableMyUtilitiesController(),
    );
     Get.lazyPut<DetailsOfLandlordUtilitiesController>(
      () => DetailsOfLandlordUtilitiesController(),
    ); 
     Get.lazyPut<SelectRegionController>(
      () => SelectRegionController(),fenix: true
    ); 
  }
}
