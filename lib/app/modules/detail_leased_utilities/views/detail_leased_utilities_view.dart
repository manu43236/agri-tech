import 'package:agritech/app/modules/detail_leased_utilities/controllers/detail_leased_utilities_controller.dart';
import 'package:agritech/app/modules/leased_utilities_checkout_page/controllers/leased_utilities_checkout_page_controller.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';

class DetailLeasedUtilitiesView
    extends GetView<DetailLeasedUtilitiesController> {
  const DetailLeasedUtilitiesView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'utility_details'.tr,
          style: TextStyles.kTSFS24W600.copyWith(color: colorSuitable),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: SingleChildScrollView(
          child: Obx(
            () => controller.apigetUtilityStatus
                        .value ==
                    ApiStatus.LOADING
                ? Shimmers().getListShimmer()
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.grey.shade300,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: Image.network(
                            controller
                                    .getUtilityByIdModel
                                    .value
                                    .result
                                    ?.utility!
                                    .image ??
                                defaultImageUrl,
                            height: Get.height * 0.3,
                            width: Get.width,
                            fit: BoxFit.cover,
                            errorBuilder: (context, error, stackTrace) {
                              return Image.asset(
                                defaultImageUrl,
                                height: Get.height * 0.3,
                                width: Get.width,
                                fit: BoxFit.cover,
                              );
                            },
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${controller.getUtilityByIdModel.value.result?.utility!.nameOfInstrument ?? ''}',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyles.kTSFS20W700
                                .copyWith(color: colorDetails),
                          ),
                          const SizedBox(height: 5),
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  '${controller.getUtilityByIdModel.value.result?.utility!.village ?? ''}, ${controller.getUtilityByIdModel.value.result?.utility?.mandal ?? ''}',
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 3,
                                  style: TextStyles.kTSDS14W500
                                      .copyWith(color: colorDetails),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Text(
                        '${controller.getUtilityByIdModel.value.result?.utility!.pricePerDay ?? 0} / day',
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.teal,
                        ),
                      ),
                      const SizedBox(height: 15),
                      Text(
                        controller
                                .getUtilityByIdModel
                                .value
                                .result
                                ?.utility
                                ?.description ??
                            '',
                        style: TextStyles.kTSCF12W500,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        'utility_owner'.tr,
                        style: TextStyles.kTSFS16W600.copyWith(
                            color: primary, fontWeight: FontWeight.w500),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CircleAvatar(
                              radius: 26,
                              backgroundColor: colorAsh,
                              child: ClipOval(
                                child: Image.network(
                                  controller
                                          .getUtilityByIdModel
                                          .value
                                          .result
                                          ?.utiliser
                                          ?.imageUrl ??
                                      defaultImageUrl,
                                  height: Get.height * 0.1,
                                  width: Get.width * 0.2,
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, error, stackTrace) {
                                    return Image.asset(
                                      defaultImageUrl,
                                      height: Get.height * 0.1,
                                      width: Get.width * 0.2,
                                      fit: BoxFit.cover,
                                    );
                                  },
                                ),
                              )),
                          const SizedBox(
                            width: 10,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                controller
                                        .getUtilityByIdModel
                                        .value
                                        .result
                                        ?.utiliser
                                        ?.firstName ??
                                    '',
                                style: TextStyles.kTSDS14W700
                                    .copyWith(color: colorPic),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                controller
                                        .getUtilityByIdModel
                                        .value
                                        .result
                                        ?.utiliser
                                        ?.address ??
                                    '',
                                style: TextStyles.kTSWFS10W700
                                    .copyWith(color: colorDetails),
                              )
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(height: 50),
                      Center(
                        child: InkWell(
                          onTap: () async {
                            // await Get.find<
                            //         LeasedUtilitiesCheckoutPageController>()
                            //     .getNintyPercentUtilityDetails(
                            //         id: controller
                            //             .getUtilityByIdModel
                            //             .value
                            //             .result!
                            //             .utility!
                            //             .id);
                            Get.toNamed(Routes.LEASED_UTILITIES_CHECKOUT_PAGE,arguments: {"id":controller
                                        .getUtilityByIdModel
                                        .value
                                        .result!
                                        .utility!
                                        .id!});
                            // print('on tap');
                            // Get.toNamed(Routes.LEASED_UTILITIES_CHECKOUT_PAGE,arguments: {"utilityId":controller.instrumentDetailsController
                            //       .getUtilityByIdModel
                            //       .value
                            //       .result!.id});
                            // print('done');
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(13),
                                color: primary),
                            child: Text(
                              'balance_payment'.tr,
                              style: TextStyles.kTSFS16W700
                                  .copyWith(color: colorWhite),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
