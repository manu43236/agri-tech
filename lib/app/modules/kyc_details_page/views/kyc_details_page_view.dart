import 'dart:io';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/kyc_details_page_controller.dart';

class KycDetailsPageView extends GetView<KycDetailsPageController> {
  const KycDetailsPageView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('kyc_details'.tr,
              style: TextStyles.kTSFS16W600.copyWith(color: colorBlack)),
          centerTitle: true,
          leading: IconButton(
            onPressed: Get.back,
            color: colorBlack,
            icon: const Icon(Icons.arrow_back),
          ),
        ),
        body: Obx(
          () => Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            SizedBox(height: Get.height * 0.02),
             Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Text("proof_of_identity".tr, style: TextStyles.kTSDS14W500),
            ),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                "in_order".tr,
                style: TextStyles.kTSFS14W600
                    .copyWith(color: const Color(0xff838BA1)),
              ),
            ),
            const SizedBox(height: 20),

            // Identity Type Selection with Radio Buttons
             Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Text("choose_your_identity_type".tr,
                  style: TextStyles.kTSDS14W500),
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: ["aadhaar".tr, "pan_card".tr, "voter_id".tr]
                  .map(
                    (type) => Expanded(
                      child: RadioListTile<String>(
                        value: type,
                        groupValue: controller.selectedIdentityType.value,
                        onChanged: (value) {
                          controller.setIdentityType(value!);
                        },
                        title: Text(
                          type,
                          style: TextStyles.kTSDS14W500.copyWith(fontSize: 10),
                        ),
                        activeColor: primary,
                        dense: true,
                        contentPadding: EdgeInsets.zero,
                      ),
                    ),
                  )
                  .toList(),
            ),
            // Unified Upload Section
            Column(
              children: [
                // Upload Document from Gallery or File Picker (PDF, Image, etc.)
                // fileUploadSection(
                //   "Upload Document (PDF, Image, etc.)",
                //   controller.uploadedDocument,
                //   () => controller.pickDocument(ImageSource.gallery), // Fixed Callback
                // ),
                // Take Picture with Camera
                const SizedBox(height: 20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Upload Identity Section
                     Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'upload_identity'.tr,
                        style: TextStyles.kTSFS14W600,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        // Front Image Box
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.symmetric(horizontal: 15),
                            padding: const EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Column(
                              children: [
                                SizedBox(
                                  width: Get.width * 0.3,
                                  height: Get.width * 0.3,
                                  child: Stack(
                                    children: [
                                      if (controller
                                          .frontImage.value.isEmpty) ...[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            IconButton(
                                              onPressed: () {
                                                showBottom(
                                                    context, "front_image".tr);
                                                // controller.pickFrontImage(ImageSource.camera); // Camera option
                                              },
                                              icon: const Icon(Icons.camera_alt,
                                                  color: Colors.black),
                                              iconSize: 30,
                                            ),
                                            // IconButton(
                                            //   onPressed: () {
                                            //     controller.pickFrontImage(ImageSource.gallery); // Gallery option
                                            //   },
                                            //   icon: const Icon(Icons.photo_library, color: Colors.black),
                                            //   iconSize: 30,
                                            // ),
                                          ],
                                        ),
                                      ] else ...[
                                        GestureDetector(
                                          onTap: () {
                                            controller.pickFrontImage(ImageSource
                                                .camera); // Replace front image
                                          },
                                          child: Image.file(
                                            File(controller.frontImage.value),
                                            width: Get.width * 0.3,
                                            height: Get.width * 0.3,
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                        Positioned(
                                          top: 5,
                                          right: 5,
                                          child: IconButton(
                                            onPressed: () {
                                              controller.frontImage.value = "";
                                              // showBottom(context,"frontImage");
                                              controller.imagePath.value
                                                  .removeAt(0);
                                              print(controller.imagePath);
                                            },
                                            icon: const Icon(Icons.cancel,
                                                color: Colors.red),
                                            iconSize: 20,
                                          ),
                                        ),
                                      ],
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 10),
                                Text('front_image'.tr,
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.grey)),
                              ],
                            ),
                          ),
                        ),
                        // Back Image Box
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.symmetric(horizontal: 15),
                            padding: const EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Column(
                              children: [
                                SizedBox(
                                  width: Get.width * 0.3,
                                  height: Get.width * 0.3,
                                  child: Stack(
                                    children: [
                                      // Camera & Gallery Icon for Back Image
                                      if (controller
                                          .frontImage.value.isEmpty) ...[
                                        IconButton(
                                          onPressed: () {
                                            Get.snackbar(
                                              'upload_required'.tr,
                                              'please_upload_the_front_image_first'.tr,
                                              snackPosition:
                                                  SnackPosition.BOTTOM,
                                            );
                                          },
                                          icon: const Icon(Icons.add_a_photo,
                                              color: Colors.grey),
                                          iconSize: 30,
                                        ),
                                      ] else if (controller
                                          .backImage.value.isEmpty) ...[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            IconButton(
                                              onPressed: () {
                                                showBottom(
                                                    context, "back_image".tr);
                                                // controller.pickBackImage(ImageSource.camera); // Camera option
                                              },
                                              icon: const Icon(Icons.camera_alt,
                                                  color: Colors.black),
                                              iconSize: 30,
                                            ),
                                            // IconButton(
                                            //   onPressed: () {
                                            //     controller.pickBackImage(ImageSource.gallery); // Gallery option
                                            //   },
                                            //   icon: const Icon(Icons.photo_library, color: Colors.black),
                                            //   iconSize: 30,
                                            // ),
                                          ],
                                        ),
                                      ] else ...[
                                        GestureDetector(
                                          onTap: () {
                                            controller.pickBackImage(ImageSource
                                                .camera); // Replace back image
                                          },
                                          child: Image.file(
                                            File(controller.backImage.value),
                                            width: Get.width * 0.3,
                                            height: Get.width * 0.3,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        Positioned(
                                          top: 5,
                                          right: 5,
                                          child: IconButton(
                                            onPressed: () {
                                              controller.backImage.value = "";
                                              //  showBottom(context,"backImage");
                                              // controller.imagePath.value.removeAt(1);
                                              // print( controller.imagePath);
                                            },
                                            icon: const Icon(Icons.cancel,
                                                color: Colors.red),
                                            iconSize: 20,
                                          ),
                                        ),
                                      ],
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 10),
                                Text('back_image'.tr,
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.grey)),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Upload Selfie Section
                    Padding(
                      padding: EdgeInsets.all(30),
                      child: Text(
                        'upload_selfie_picture'.tr,
                        style: TextStyles.kTSFS14W600,
                      ),
                    ),
                    Center(
                      child: Container(
                        margin: const EdgeInsets.symmetric(horizontal: 10),
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          children: [
                            SizedBox(
                              width: Get.width * 0.35,
                              height: Get.height * 0.08,
                              child: Stack(
                                children: [
                                  if (controller.selfieImage.value.isEmpty) ...[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        IconButton(
                                          onPressed: () {
                                            controller.pickSelfieImage(
                                                ImageSource
                                                    .camera); // Camera option
                                          },
                                          icon: const Icon(Icons.camera_alt,
                                              color: Colors.blue),
                                          iconSize: 30,
                                        ),
                                      ],
                                    ),
                                  ] else ...[
                                    Stack(
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            controller.pickSelfieImage(ImageSource
                                                .camera); // Replace selfie image
                                          },
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            child: Image.file(
                                              File(
                                                  controller.selfieImage.value),
                                              width: Get.width * 0.35,
                                              height: Get.width * 0.35,
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          top: 5,
                                          right: 5,
                                          child: IconButton(
                                            onPressed: () {
                                              controller.selfieImage.value =
                                                  ""; // Remove selfie image
                                            },
                                            icon: const Icon(Icons.cancel,
                                                color: Colors.red),
                                            iconSize: 20,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ],
                              ),
                            ),
                            const SizedBox(height: 10),
                            Text('selfie_image'.tr,
                                style: TextStyle(
                                    fontSize: 14, color: Colors.grey)),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const Spacer(),
            Center(
              child: ElevatedButton(
                onPressed: () {
                  if (controller.validateImages()) {
                    print("uploading");
                    controller
                        .uploadIdDocument(); // Upload the document and image
                  } else {
                    Get.snackbar(
                        "incomplete".tr, "please_upload_the_required_document".tr);
                  }
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: primary,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 80, vertical: 15),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
                child: Text(
                  'continue'.tr,
                  style:
                      TextStyles.kTSFS16W600.copyWith(color: colorGhostWhite),
                ),
              ),
            ),
            const SizedBox(height: 20),
          ]),
        ));
  }

  showBottom(BuildContext context, type) {
    return showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.photo_library),
                title:  Text('gallery'.tr),
                onTap: () {
                  if (type == "frontImage") {
                    controller.pickFrontImage(ImageSource.gallery);
                    Navigator.of(context).pop();
                  } else {
                    controller.pickBackImage(ImageSource.gallery);
                    Navigator.of(context).pop();
                  }
                },
              ),
              ListTile(
                leading: const Icon(Icons.photo_camera),
                title:  Text('camera'.tr),
                onTap: () {
                  if (type == "frontImage") {
                    controller.pickFrontImage(ImageSource.camera);
                    Navigator.of(context).pop();
                  } else {
                    controller.pickBackImage(ImageSource.camera);
                    Navigator.of(context).pop();
                  }
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
