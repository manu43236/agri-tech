import 'package:get/get.dart';

import '../controllers/utility_loans_controller.dart';

class UtilityLoansBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UtilityLoansController>(
      () => UtilityLoansController(),
    );
  }
}
