import 'package:get/get.dart';

import '../controllers/available_my_skills_controller.dart';

class AvailableMySkillsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailableMySkillsController>(
      () => AvailableMySkillsController(),
    );
  }
}
