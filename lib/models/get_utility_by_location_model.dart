// To parse this JSON data, do
//
//     final getUtilityByLocationModel = getUtilityByLocationModelFromJson(jsonString);

import 'dart:convert';

GetUtilityByLocationModel getUtilityByLocationModelFromJson(String str) => GetUtilityByLocationModel.fromJson(json.decode(str));

String getUtilityByLocationModelToJson(GetUtilityByLocationModel data) => json.encode(data.toJson());

class GetUtilityByLocationModel {
    int? statusCode;
    String? status;
    String? message;
    List<Result>? result;

    GetUtilityByLocationModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetUtilityByLocationModel.fromJson(Map<String, dynamic> json) => GetUtilityByLocationModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    int? utililityId;
    String? nameOfInstrument;
    int? pricePerDay;
    String? brandName;
    String? state;
    String? district;
    String? mandal;
    String? village;
    String? description;
    dynamic imageId;
    dynamic image;
    bool? status;
    DateTime? createdAt;
    DateTime? updatedAt;

    Result({
        this.id,
        this.utililityId,
        this.nameOfInstrument,
        this.pricePerDay,
        this.brandName,
        this.state,
        this.district,
        this.mandal,
        this.village,
        this.description,
        this.imageId,
        this.image,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        utililityId: json["utililityId"],
        nameOfInstrument: json["nameOfInstrument"],
        pricePerDay: json["pricePerDay"],
        brandName: json["brandName"],
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
        description: json["description"],
        imageId: json["image_id"],
        image: json["image"],
        status: json["status"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "utililityId": utililityId,
        "nameOfInstrument": nameOfInstrument,
        "pricePerDay": pricePerDay,
        "brandName": brandName,
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
        "description": description,
        "image_id": imageId,
        "image": image,
        "status": status,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
