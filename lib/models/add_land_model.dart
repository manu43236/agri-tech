class AddLandModel {
  int? statusCode;
  dynamic status;
  dynamic message;
  Result? result;

  AddLandModel({this.statusCode, this.status, this.message, this.result});

  AddLandModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    status = json['status'];
    message = json['message'];
    result =
        json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.result != null) {
      data['result'] = this.result!.toJson();
    }
    return data;
  }
}

class Result {
  int? id;
  int? landLordId;
  dynamic fullName;
  dynamic landInAcres;
  dynamic surveyNum;
  dynamic state;
  dynamic district;
  dynamic mandal;
  dynamic village;
  dynamic latitude;
  dynamic longitude;
  dynamic radius;
  dynamic description;
  dynamic status;
  dynamic updatedAt;
  dynamic createdAt;
  dynamic farmerId;
  dynamic water;
  dynamic imageId;
  dynamic imageUrl;

  Result(
      {this.id,
      this.landLordId,
      this.fullName,
      this.landInAcres,
      this.surveyNum,
      this.state,
      this.district,
      this.mandal,
      this.village,
      this.latitude,
      this.longitude,
      this.radius,
      this.description,
      this.status,
      this.updatedAt,
      this.createdAt,
      this.farmerId,
      this.water,
      this.imageId,
      this.imageUrl});

  Result.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    landLordId = json['landLordId'];
    fullName = json['fullName'];
    landInAcres = json['landInAcres'];
    surveyNum = json['surveyNum'];
    state = json['state'];
    district = json['district'];
    mandal = json['mandal'];
    village = json['village'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    radius = json['radius'];
    description = json['description'];
    status = json['status'];
    updatedAt = json['updatedAt'];
    createdAt = json['createdAt'];
    farmerId = json['farmerId'];
    water = json['water'];
    imageId = json['image_id'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['landLordId'] = this.landLordId;
    data['fullName'] = this.fullName;
    data['landInAcres'] = this.landInAcres;
    data['surveyNum'] = this.surveyNum;
    data['state'] = this.state;
    data['district'] = this.district;
    data['mandal'] = this.mandal;
    data['village'] = this.village;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['radius'] = this.radius;
    data['description'] = this.description;
    data['status'] = this.status;
    data['updatedAt'] = this.updatedAt;
    data['createdAt'] = this.createdAt;
    data['farmerId'] = this.farmerId;
    data['water'] = this.water;
    data['image_id'] = this.imageId;
    data['image_url'] = this.imageUrl;
    return data;
  }
}
