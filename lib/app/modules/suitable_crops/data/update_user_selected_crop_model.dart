// To parse this JSON data, do
//
//     final updateUserSelectedCropModel = updateUserSelectedCropModelFromJson(jsonString);

import 'dart:convert';

UpdateUserSelectedCropModel updateUserSelectedCropModelFromJson(String str) => UpdateUserSelectedCropModel.fromJson(json.decode(str));

String updateUserSelectedCropModelToJson(UpdateUserSelectedCropModel data) => json.encode(data.toJson());

class UpdateUserSelectedCropModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    UpdateUserSelectedCropModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory UpdateUserSelectedCropModel.fromJson(Map<String, dynamic> json) => UpdateUserSelectedCropModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    int? userid;
    int? landid;
    int? cropId;
    int? weeks;
    bool? weekenable;
    int? steps;
    bool? stepsflag;
    DateTime? createdDate;
    DateTime? createdAt;
    DateTime? updatedAt;

    Result({
        this.id,
        this.userid,
        this.landid,
        this.cropId,
        this.weeks,
        this.weekenable,
        this.steps,
        this.stepsflag,
        this.createdDate,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        userid: json["userid"],
        landid: json["landid"],
        cropId: json["cropId"],
        weeks: json["weeks"],
        weekenable: json["weekenable"],
        steps: json["steps"],
        stepsflag: json["stepsflag"],
        createdDate: json["created_date"] == null ? null : DateTime.parse(json["created_date"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userid": userid,
        "landid": landid,
        "cropId": cropId,
        "weeks": weeks,
        "weekenable": weekenable,
        "steps": steps,
        "stepsflag": stepsflag,
        "created_date": "${createdDate!.year.toString().padLeft(4, '0')}-${createdDate!.month.toString().padLeft(2, '0')}-${createdDate!.day.toString().padLeft(2, '0')}",
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
