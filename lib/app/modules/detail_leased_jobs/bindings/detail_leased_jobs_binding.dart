import 'package:get/get.dart';

import '../../saved_jobs/controllers/saved_jobs_controller.dart';
import '../controllers/detail_leased_jobs_controller.dart';

class DetailLeasedJobsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailLeasedJobsController>(
      () => DetailLeasedJobsController(),
    );
    Get.lazyPut<SavedJobsController>(
      () => SavedJobsController(),
    );
  }
}
