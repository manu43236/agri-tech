class RegisterModel {
  int? statusCode;
  String? status;
  String? message;
  Result? result;

  RegisterModel({this.statusCode, this.status, this.message, this.result});

  RegisterModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    status = json['status'];
    message = json['message'];
    result =
        json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.result != null) {
      data['result'] = this.result!.toJson();
    }
    return data;
  }
}

class Result {
  int? id;
  dynamic? firstName;
  String? mobileNumber;
  dynamic? role;
  dynamic? age;
  dynamic? gender;
  dynamic? address;
  dynamic? village;
  dynamic? mandal;
  dynamic? district;
  dynamic? state;
  String? accesstoken;
  dynamic? profileId;
  dynamic? imageUrl;
  dynamic? latitude;
  dynamic? longitude;
  int? otp;
  String? updatedAt;
  String? createdAt;

  Result(
      {this.id,
      this.firstName,
      this.mobileNumber,
      this.role,
      this.age,
      this.gender,
      this.address,
      this.village,
      this.mandal,
      this.district,
      this.state,
      this.accesstoken,
      this.profileId,
      this.imageUrl,
      this.latitude,
      this.longitude,
      this.otp,
      this.updatedAt,
      this.createdAt});

  Result.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    mobileNumber = json['mobileNumber'];
    role = json['role'];
    age = json['age'];
    gender = json['gender'];
    address = json['address'];
    village = json['village'];
    mandal = json['mandal'];
    district = json['district'];
    state = json['state'];
    accesstoken = json['accesstoken'];
    profileId = json['profileId'];
    imageUrl = json['image_url'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    otp = json['otp'];
    updatedAt = json['updatedAt'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['mobileNumber'] = this.mobileNumber;
    data['role'] = this.role;
    data['age'] = this.age;
    data['gender'] = this.gender;
    data['address'] = this.address;
    data['village'] = this.village;
    data['mandal'] = this.mandal;
    data['district'] = this.district;
    data['state'] = this.state;
    data['accesstoken'] = this.accesstoken;
    data['profileId'] = this.profileId;
    data['image_url'] = this.imageUrl;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['otp'] = this.otp;
    data['updatedAt'] = this.updatedAt;
    data['createdAt'] = this.createdAt;
    return data;
  }
}
