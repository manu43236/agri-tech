import 'package:get/get.dart';

import '../controllers/available_leased_jobs_controller.dart';

class AvailableLeasedJobsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailableLeasedJobsController>(
      () => AvailableLeasedJobsController(),
    );
  }
}
