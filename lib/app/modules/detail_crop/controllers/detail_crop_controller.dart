import 'package:agritech/app/modules/detail_crop/services/detail_crop_repo_impl.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../data/create_crop_payment_model.dart';
import '../data/get_crop_availability_by_yield_id_model.dart';

class DetailCropController extends BaseController {

  Rx<GetCropAvailabilityByYieldIdModel> getdetailsOfCropModel =
      Rx(GetCropAvailabilityByYieldIdModel());

  Rx<CreateCropPaymentModel> createCropPaymentModel =
      Rx(CreateCropPaymentModel());

  var apiDetailsStatus=ApiStatus.LOADING.obs;

  @override
  void onInit() {
    super.onInit();
    if(Get.arguments!=null){
      getDetailsOfCrop(yieldId: Get.arguments["yieldId"]);
    }
  }

  Future<void> getDetailsOfCrop({yieldId}) async {
    apiDetailsStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailCropRepoImpl(dioClient).cropDetails(yieldId: yieldId);
      print('=============== get crop details =========');
      print(result);
      result.fold((model) {
        getdetailsOfCropModel.value = model;
        apiDetailsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiDetailsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> createCropPayment({landId,cropId}) async {
    apiDetailsStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailCropRepoImpl(dioClient).createCropPayment(params1: {
        "landId": landId,
        "cropId": cropId,
        "vendeId":Get.find<HomeController>().userId.value
      });
      print('=============== create crop payment =========');
      print(result);
      result.fold((model) {
        if(model.statusCode==200){
          createCropPaymentModel.value = model;
        }
        // else if(model.statusCode==400){

        // }
        
        apiDetailsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiDetailsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
