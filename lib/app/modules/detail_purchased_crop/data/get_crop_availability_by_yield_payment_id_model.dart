// To parse this JSON data, do
//
//     final getCropAvailabilityByYieldPaymentIdModel = getCropAvailabilityByYieldPaymentIdModelFromJson(jsonString);

import 'dart:convert';

GetCropAvailabilityByYieldPaymentIdModel getCropAvailabilityByYieldPaymentIdModelFromJson(String str) => GetCropAvailabilityByYieldPaymentIdModel.fromJson(json.decode(str));

String getCropAvailabilityByYieldPaymentIdModelToJson(GetCropAvailabilityByYieldPaymentIdModel data) => json.encode(data.toJson());

class GetCropAvailabilityByYieldPaymentIdModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetCropAvailabilityByYieldPaymentIdModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetCropAvailabilityByYieldPaymentIdModel.fromJson(Map<String, dynamic> json) => GetCropAvailabilityByYieldPaymentIdModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? yieldId;
    int? cropId;
    dynamic cropName;
    dynamic cropImage;
    dynamic totaldays;
    dynamic costPerHector;
    int? landId;
    dynamic ownerName;
    LandDetails? landDetails;
    YieldDetails? yieldDetails;
    List<Croppaymentstatus>? croppaymentstatus;

    Result({
        this.yieldId,
        this.cropId,
        this.cropName,
        this.cropImage,
        this.totaldays,
        this.costPerHector,
        this.landId,
        this.ownerName,
        this.landDetails,
        this.yieldDetails,
        this.croppaymentstatus,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        yieldId: json["yieldId"],
        cropId: json["cropId"],
        cropName: json["cropName"],
        cropImage: json["cropImage"],
        totaldays: json["totaldays"],
        costPerHector: json["costPerHector"],
        landId: json["landId"],
        ownerName: json["ownerName"],
        landDetails: json["landDetails"] == null ? null : LandDetails.fromJson(json["landDetails"]),
        yieldDetails: json["yieldDetails"] == null ? null : YieldDetails.fromJson(json["yieldDetails"]),
        croppaymentstatus: json["croppaymentstatus"] == null ? [] : List<Croppaymentstatus>.from(json["croppaymentstatus"]!.map((x) => Croppaymentstatus.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "yieldId": yieldId,
        "cropId": cropId,
        "cropName": cropName,
        "cropImage": cropImage,
        "totaldays": totaldays,
        "costPerHector": costPerHector,
        "landId": landId,
        "ownerName": ownerName,
        "landDetails": landDetails?.toJson(),
        "yieldDetails": yieldDetails?.toJson(),
        "croppaymentstatus": croppaymentstatus == null ? [] : List<dynamic>.from(croppaymentstatus!.map((x) => x.toJson())),
    };
}

class Croppaymentstatus {
    int? id;
    int? userId;
    int? cropId;
    int? landId;
    int? utilityId;
    dynamic quantity;
    dynamic hectors;
    dynamic remainingQuantity;
    dynamic perHectorcost;
    dynamic costOfSelectedItem;
    dynamic tenPercentGst;
    dynamic nintyPercentGst;
    dynamic tenPercentPayment;
    dynamic nintyPercentpayment;
    dynamic totalPayment;
    dynamic paymentStatus;
    dynamic amountPaid;
    dynamic tenPercentPrice;
    dynamic ninetyPercentPrice;
    dynamic razorpayOrderId;
    dynamic razorpayPaymentId;
    dynamic razorpaySignature;
    dynamic createdAt;
    dynamic updatedAt;

    Croppaymentstatus({
        this.id,
        this.userId,
        this.cropId,
        this.landId,
        this.utilityId,
        this.quantity,
        this.hectors,
        this.remainingQuantity,
        this.perHectorcost,
        this.costOfSelectedItem,
        this.tenPercentGst,
        this.nintyPercentGst,
        this.tenPercentPayment,
        this.nintyPercentpayment,
        this.totalPayment,
        this.paymentStatus,
        this.amountPaid,
        this.tenPercentPrice,
        this.ninetyPercentPrice,
        this.razorpayOrderId,
        this.razorpayPaymentId,
        this.razorpaySignature,
        this.createdAt,
        this.updatedAt,
    });

    factory Croppaymentstatus.fromJson(Map<String, dynamic> json) => Croppaymentstatus(
        id: json["id"],
        userId: json["userId"],
        cropId: json["cropId"],
        landId: json["landId"],
        utilityId: json["utilityId"],
        quantity: json["quantity"],
        hectors: json["hectors"],
        remainingQuantity: json["remainingQuantity"],
        perHectorcost: json["perHectorcost"],
        costOfSelectedItem: json["costOfSelectedItem"],
        tenPercentGst: json["tenPercentGST"],
        nintyPercentGst: json["nintyPercentGST"],
        tenPercentPayment: json["tenPercentPayment"],
        nintyPercentpayment: json["nintyPercentpayment"],
        totalPayment: json["totalPayment"],
        paymentStatus: json["paymentStatus"],
        amountPaid: json["amountPaid"],
        tenPercentPrice: json["tenPercentPrice"],
        ninetyPercentPrice: json["ninetyPercentPrice"],
        razorpayOrderId: json["razorpay_order_id"],
        razorpayPaymentId: json["razorpay_payment_id"],
        razorpaySignature: json["razorpay_signature"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "cropId": cropId,
        "landId": landId,
        "utilityId": utilityId,
        "quantity": quantity,
        "hectors": hectors,
        "remainingQuantity": remainingQuantity,
        "perHectorcost": perHectorcost,
        "costOfSelectedItem": costOfSelectedItem,
        "tenPercentGST": tenPercentGst,
        "nintyPercentGST": nintyPercentGst,
        "tenPercentPayment": tenPercentPayment,
        "nintyPercentpayment": nintyPercentpayment,
        "totalPayment": totalPayment,
        "paymentStatus": paymentStatus,
        "amountPaid": amountPaid,
        "tenPercentPrice": tenPercentPrice,
        "ninetyPercentPrice": ninetyPercentPrice,
        "razorpay_order_id": razorpayOrderId,
        "razorpay_payment_id": razorpayPaymentId,
        "razorpay_signature": razorpaySignature,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}

class LandDetails {
    int? id;
    int? landLordId;
    dynamic farmerId;
    dynamic landInAcres;
    dynamic village;
    dynamic district;
    dynamic state;
    dynamic description;
    dynamic latitude;
    dynamic longitude;
    dynamic surveyNum;
    dynamic mandal;
    dynamic fullName;
    dynamic water;

    LandDetails({
        this.id,
        this.landLordId,
        this.farmerId,
        this.landInAcres,
        this.village,
        this.district,
        this.state,
        this.description,
        this.latitude,
        this.longitude,
        this.surveyNum,
        this.mandal,
        this.fullName,
        this.water,
    });

    factory LandDetails.fromJson(Map<String, dynamic> json) => LandDetails(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        landInAcres: json["landInAcres"],
        village: json["village"],
        district: json["district"],
        state: json["state"],
        description: json["description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        surveyNum: json["surveyNum"],
        mandal: json["mandal"],
        fullName: json["fullName"],
        water: json["water"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "landInAcres": landInAcres,
        "village": village,
        "district": district,
        "state": state,
        "description": description,
        "latitude": latitude,
        "longitude": longitude,
        "surveyNum": surveyNum,
        "mandal": mandal,
        "fullName": fullName,
        "water": water,
    };
}

class YieldDetails {
    dynamic outedDate;
    dynamic hectors;
    bool? status;
    dynamic remainingQuantity;
    dynamic quantity;

    YieldDetails({
        this.outedDate,
        this.hectors,
        this.status,
        this.remainingQuantity,
        this.quantity,
    });

    factory YieldDetails.fromJson(Map<String, dynamic> json) => YieldDetails(
        outedDate: json["outedDate"] == null ? null : DateTime.parse(json["outedDate"]),
        hectors: json["hectors"],
        status: json["status"],
        remainingQuantity: json["remainingQuantity"],
        quantity: json["quantity"],
    );

    Map<String, dynamic> toJson() => {
        "outedDate": outedDate?.toIso8601String(),
        "hectors": hectors,
        "status": status,
        "remainingQuantity": remainingQuantity,
        "quantity": quantity,
    };
}
