import 'package:get/get.dart';

import '../controllers/saved_farmworkers_controller.dart';

class SavedFarmworkersBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SavedFarmworkersController>(
      () => SavedFarmworkersController(),
    );
  }
}
