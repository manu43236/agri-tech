import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/detail_my_skills_controller.dart';

class DetailMySkillsView extends GetView<DetailMySkillsController> {
  const DetailMySkillsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'detail_my_skill'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Obx(
          () => controller.apiGetSkillsStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: Get.height * 0.35,
                  width: Get.width,
                  decoration: BoxDecoration(
                    color: colorAsh,
                    borderRadius: BorderRadius.circular(13),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.asset(
                      agri,
                      height: Get.height * 0.1,
                      width: Get.width * 0.2,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Text(
                  controller.getSkillsByIdModel.value.result?.first.skill ??
                      'Unknown Skill',
                  style: TextStyles.kTSFS20W700.copyWith(color: colorDetails),
                ),
                const SizedBox(height: 5),
                Text(
                  '${'experiences'.tr} ${controller.getSkillsByIdModel.value.result?.first.experience ?? '0'} ${'years'.tr}',
                  style: TextStyles.kTSDS12W500.copyWith(color: colorDetails),
                ),
                const SizedBox(height: 15),
                Row(
                  children: [
                    CircleAvatar(
                      radius: 26,
                      backgroundColor: colorAsh,
                      child: ClipOval(
                        child: Image.network(
                          controller.homeController.getUserModel.value.result
                                  ?.imageUrl ??
                              defaultImageUrl,
                          height: Get.height * 0.1,
                          width: Get.width * 0.2,
                          fit: BoxFit.cover,
                          errorBuilder: (context, error, stackTrace) {
                            return Image.asset(
                              defaultImageUrl,
                              height: Get.height * 0.1,
                              width: Get.width * 0.2,
                              fit: BoxFit.cover,
                            );
                          },
                        ),
                      ),
                    ),
                    const SizedBox(width: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          controller.homeController.getUserModel.value.result
                                  ?.firstName ??
                              'Unknown User',
                          style: TextStyles.kTSDS14W700
                              .copyWith(color: colorPic),
                        ),
                        const SizedBox(height: 5),
                        Text(
                          controller.homeController.getUserModel.value.result
                                  ?.address ??
                              'Unknown Address',
                          style: TextStyles.kTSWFS10W700
                              .copyWith(color: colorDetails),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Text(
                  controller.getSkillsByIdModel.value.result?.first.description ??
                      'no_description_available'.tr,
                  style: TextStyles.kTSCF12W500,
                ),
                const SizedBox(height: 20),
                Text(
                  'relevent_jobs'.tr,
                  style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                ),
                const SizedBox(height: 10),
                SizedBox(
                  height: Get.height * 0.08,
                  child: (controller.relevantSkillJobsModel.value.result?.isNotEmpty ??
                          false)
                      ? ListView.separated(
                          itemCount: controller.relevantSkillJobsModel.value
                              .result!
                              .length,
                          scrollDirection: Axis.horizontal,
                          separatorBuilder: (context, index) =>
                              const SizedBox(width: 10),
                          itemBuilder: (context, index) {
                            final filteredLands = controller
                                .relevantSkillJobsModel.value.result!
                                ;

                            return InkWell(
                              onTap: () async {
                                Get.toNamed(Routes.DETAIL_RELEVANT_JOBS,arguments: {"jobId":filteredLands[index].id.toString()});
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: colorGrey,
                                  borderRadius: BorderRadius.circular(11),
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(11),
                                  child: Image.network(
                                    filteredLands[index].image ??
                                        defaultImageUrl,
                                    height: Get.height * 0.09,
                                    width: Get.width * 0.2,
                                    fit: BoxFit.cover,
                                    errorBuilder: (context, error, stackTrace) {
                                      return Image.asset(
                                        defaultImageUrl,
                                        height: Get.height * 0.09,
                                        width: Get.width * 0.2,
                                        fit: BoxFit.cover,
                                      );
                                    },
                                  ),
                                ),
                              ),
                            );
                          },
                        )
                      : Center(
                          child: Text(
                            'no_relevant_jobs_available'.tr,
                            style: TextStyles.kTSDS14W500
                                .copyWith(color: colorGrey),
                          ),
                        ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
