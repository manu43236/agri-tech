import 'package:get/get.dart';

import '../controllers/landlord_utilities_payments_page_controller.dart';

class LandlordUtilitiesPaymentsPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LandlordUtilitiesPaymentsPageController>(
      () => LandlordUtilitiesPaymentsPageController(),
    );
  }
}
