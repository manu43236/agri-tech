// To parse this JSON data, do
//
//     final nearByFarmworkersModel = nearByFarmworkersModelFromJson(jsonString);

import 'dart:convert';

NearByFarmworkersModel nearByFarmworkersModelFromJson(String str) {
  return NearByFarmworkersModel.fromJson(json.decode(str));
}

String nearByFarmworkersModelToJson(NearByFarmworkersModel data) {
  return json.encode(data.toJson());
}

class NearByFarmworkersModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    NearByFarmworkersModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory NearByFarmworkersModel.fromJson(Map<String, dynamic> json) => NearByFarmworkersModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<Farmworker>? farmworkers;

    Result({
        this.farmworkers,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        farmworkers: json["farmworkers"] == null ? [] : List<Farmworker>.from(json["farmworkers"]!.map((x) => Farmworker.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "farmworkers": farmworkers == null ? [] : List<dynamic>.from(farmworkers!.map((x) => x.toJson())),
    };
}

class Farmworker {
    int? id;
    dynamic firstName;
    dynamic mobileNumber;
    dynamic role;
    dynamic age;
    dynamic gender;
    dynamic address;
    dynamic village;
    dynamic mandal;
    dynamic district;
    dynamic state;
    int? otp;
    dynamic longitude;
    dynamic latitude;
    dynamic accesstoken;
    int? profileId;
    dynamic imageUrl;
    dynamic description;
    dynamic locationDetails;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;
    List<Skill>? skills;

    Farmworker({
        this.id,
        this.firstName,
        this.mobileNumber,
        this.role,
        this.age,
        this.gender,
        this.address,
        this.village,
        this.mandal,
        this.district,
        this.state,
        this.otp,
        this.longitude,
        this.latitude,
        this.accesstoken,
        this.profileId,
        this.imageUrl,
        this.description,
        this.locationDetails,
        this.flag,
        this.createdAt,
        this.updatedAt,
        this.skills,
    });

    factory Farmworker.fromJson(Map<String, dynamic> json) => Farmworker(
        id: json["id"],
        firstName: json["firstName"],
        mobileNumber: json["mobileNumber"],
        role: json["role"],
        age: json["age"],
        gender: json["gender"],
        address: json["address"],
        village: json["village"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        otp: json["otp"],
        longitude: json["longitude"],
        latitude: json["latitude"],
        accesstoken: json["accesstoken"],
        profileId: json["profileId"],
        imageUrl: json["image_url"],
        description: json["Description"],
        locationDetails: json["location_details"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        skills: json["skills"] == null ? [] : List<Skill>.from(json["skills"]!.map((x) => Skill.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "mobileNumber": mobileNumber,
        "role": role,
        "age": age,
        "gender": gender,
        "address": address,
        "village": village,
        "mandal": mandal,
        "district": district,
        "state": state,
        "otp": otp,
        "longitude": longitude,
        "latitude": latitude,
        "accesstoken": accesstoken,
        "profileId": profileId,
        "image_url": imageUrl,
        "Description": description,
        "location_details": locationDetails,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "skills": skills == null ? [] : List<dynamic>.from(skills!.map((x) => x.toJson())),
    };
}

class Skill {
    int? id;
    int? userId;
    dynamic skill;
    dynamic experience;
    dynamic description;
    dynamic createdAt;
    dynamic updatedAt;

    Skill({
        this.id,
        this.userId,
        this.skill,
        this.experience,
        this.description,
        this.createdAt,
        this.updatedAt,
    });

    factory Skill.fromJson(Map<String, dynamic> json) => Skill(
        id: json["id"],
        userId: json["userId"],
        skill: json["skill"],
        experience: json["experience"],
        description: json["description"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "skill": skill,
        "experience": experience,
        "description": description,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
