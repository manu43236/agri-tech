import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/conts/img_const.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../../../routes/app_pages.dart';
import '../controllers/details_of_landlord_controller.dart';

class DetailsOfLandlordView extends GetView<DetailsOfLandlordController> {
  const DetailsOfLandlordView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'details_of_landlord'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            controller.landlordController.addLandsOfLandlordModel.value.result?.ongoingLands?.clear();
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Obx(
          () =>controller.apiUserByProfileIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): 
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CircleAvatar(
                    radius: 25,
                    backgroundColor: colorAsh,
                    child: ClipOval(
                      child: controller.getUserProfileModel.value.result?.imageUrl?.isNotEmpty == true
                          ? Image.network(
                              controller.getUserProfileModel.value.result!.imageUrl!,
                              fit: BoxFit.cover,
                              height: 50,
                              width: 50,
                            )
                          : const Icon(Icons.person, size: 40, color: Colors.grey),
                    ),
                  ),
                  const SizedBox(width: 10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        controller.getUserProfileModel.value.result?.firstName ?? 'N/A',
                        style: TextStyles.kTSDS14W700.copyWith(color: colorPic),
                      ),
                      const SizedBox(height: 5),
                      Text(
                        controller.getUserProfileModel.value.result?.address ?? 'No address provided',
                        style: TextStyles.kTSWFS10W600.copyWith(color: colorDetails),
                      ),
                    ],
                  ),
                  const Spacer(),
                  InkWell(
                    onTap: () async {
                      if (controller.isLandlordSaved.value==true) {
                        await Get.find<HomeController>().updateSavedUser(false,id:controller.deleteId.value , errorMessage: "landlord_unsaved_successfully".tr);
                        await controller.getAllLandlordsSaved(userId: controller.ids.value);
                        controller.isLandlordSaved.value =
                              false;
                      } else {
                        await Get.find<HomeController>().saveLandlordAsFavourite();
                        await controller.getAllLandlordsSaved(userId: controller.ids.value);
                        controller.isLandlordSaved.value =
                              true;
                      }
                    },
                    child: Obx(
                      () => Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(11),
                          color: controller.isLandlordSaved.value ? colorBlue.withOpacity(0.5) : colorSave,
                        ),
                        child: Icon(
                          controller.isLandlordSaved.value ? Icons.bookmark : Icons.bookmark_border_outlined,
                          color: colorBlack.withOpacity(0.5),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Text(
                'description_about_him'.tr,
                style: TextStyles.kTSDS12W700.copyWith(color: colorDetails),
              ),
              const SizedBox(height: 10),
              Text(
                controller.getUserProfileModel.value.result?.description ?? 'No description provided',
                style: TextStyles.kTSCF12W500,
              ),
              const SizedBox(height: 10),
              Text(
                'my_lands'.tr,
                style: TextStyles.kTSDS12W700.copyWith(color: colorDetails),
              ),
              Expanded(
                child: Obx(
                  () {
                    final ongoingLands = controller.addLandsOfLandlordModels.value.result?.ongoingLands??[];
                    if (ongoingLands != null && ongoingLands.isNotEmpty) {
                      return ListView.builder(
                        itemCount: ongoingLands.length,
                        itemBuilder: (context, index) {
                          final land = ongoingLands[index];
                          return InkWell(
                            onTap: () async {
                              Get.toNamed(Routes.DETAILS_OF_LANDLORD_LAND,arguments: {"landId":land.id.toString()});
                            },
                            child: Container(
                              margin: const EdgeInsets.symmetric(vertical: 10),
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(19),
                                boxShadow: [
                                  BoxShadow(
                                    color: colorBlack.withOpacity(0.2),
                                    blurRadius: 5,
                                    spreadRadius: 1,
                                    offset: const Offset(0, 3),
                                    blurStyle: BlurStyle.inner,
                                  ),
                                ],
                                color: colorWhite,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(11),
                                      child: 
                                          Image.network(
                                              land.imageUrl,
                                              height: Get.height * 0.08,
                                              width: Get.width * 0.3,
                                              fit: BoxFit.cover,
                                              errorBuilder:
                                                  (context, error, stackTrace) {
                                                return Image.asset(
                                                  defaultImageUrl,
                                                   height: Get.height * 0.08,
                                                   width: Get.width * 0.3,
                                                  fit: BoxFit.cover,
                                                );
                                              },
                                            ),
                                    ),
                                    const SizedBox(width: 30),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('${land.fullName} ${'at'.tr}', style: TextStyles.kTSCF12W500),
                                        Text(land.village, style: TextStyles.kTSCF12W500),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    } else {
                      return  Center(child: Text('no_lands_found'.tr, style: TextStyles.kTSDS12W500));
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
