import 'package:get/get.dart';
import '../controllers/landlord_utilities_checkout_page_controller.dart';

class LandlordUtilitiesCheckoutPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LandlordUtilitiesCheckoutPageController>(
      () => LandlordUtilitiesCheckoutPageController(),
    );
  }
}
