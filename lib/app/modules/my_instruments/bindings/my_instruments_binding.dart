import 'package:agritech/app/modules/instrument_details/controllers/instrument_details_controller.dart';
import 'package:get/get.dart';

import '../controllers/my_instruments_controller.dart';

class MyInstrumentsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MyInstrumentsController>(
      () => MyInstrumentsController(),
    );
     Get.lazyPut<InstrumentDetailsController>(
      () => InstrumentDetailsController(),
    );
  }
}
