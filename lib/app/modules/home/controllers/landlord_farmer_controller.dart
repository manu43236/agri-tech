import 'package:agritech/app/modules/home/services/home_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../data/landlord_farmers_model.dart';
import 'home_controller.dart';

class LandlordFarmerController extends BaseController{

  Rx<LandlordFarmersModel> landlordFarmersModel = Rx(LandlordFarmersModel());

  var landlordFarmerSearch=''.obs;

  @override
  void onInit() async{
    super.onInit();
    await filterFarmers(latitude: Get.find<HomeController>().latitude.value,longitude: Get.find<HomeController>().longitude.value,search: '');
  }

  // List of Farmers View
  Future<void> filterFarmers({latitude,longitude,search}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await HomeRepoImpl(dioClient).doLFarmers(latitude: latitude,longitude: longitude,search: search);
      print(
          "======================== landlord farmers list ====================");
      print(result);
      result.fold((left) {
        farmershandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
      
    }
  }

  void farmershandleResponse(LandlordFarmersModel model) async {
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
      landlordFarmersModel.value = model;

      print(landlordFarmersModel.value.result!.farmers![0].role);
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}