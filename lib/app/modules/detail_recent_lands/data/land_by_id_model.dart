// // To parse this JSON data, do
// //
// //     final landByIdModel = landByIdModelFromJson(jsonString);

// import 'dart:convert';

// LandByIdModel landByIdModelFromJson(String str) => LandByIdModel.fromJson(json.decode(str));

// String landByIdModelToJson(LandByIdModel data) => json.encode(data.toJson());

// class LandByIdModel {
//     int? statusCode;
//     dynamic status;
//     dynamic message;
//     Result? result;

//     LandByIdModel({
//         this.statusCode,
//         this.status,
//         this.message,
//         this.result,
//     });

//     factory LandByIdModel.fromJson(Map<String, dynamic> json) => LandByIdModel(
//         statusCode: json["statusCode"],
//         status: json["status"],
//         message: json["message"],
//         result: json["result"] == null ? null : Result.fromJson(json["result"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "statusCode": statusCode,
//         "status": status,
//         "message": message,
//         "result": result?.toJson(),
//     };
// }

// class Result {
//     LandDetails? landDetails;
//     dynamic statusForLandlord;
//     dynamic statusForFarmer;
//     dynamic loanAgentStatus;
//     dynamic insuranceAgentStatus;
//     List<dynamic>? farmerDetails;

//     Result({
//         this.landDetails,
//         this.statusForLandlord,
//         this.statusForFarmer,
//         this.loanAgentStatus,
//         this.insuranceAgentStatus,
//         this.farmerDetails,
//     });

//     factory Result.fromJson(Map<String, dynamic> json) => Result(
//         landDetails: json["landDetails"] == null ? null : LandDetails.fromJson(json["landDetails"]),
//         statusForLandlord: json["status_for_landlord"],
//         statusForFarmer: json["status_for_farmer"],
//         loanAgentStatus: json["loanAgentStatus"],
//         insuranceAgentStatus: json["insuranceAgentStatus"],
//         farmerDetails: json["farmerDetails"] == null ? [] : List<dynamic>.from(json["farmerDetails"]!.map((x) => x)),
//     );

//     Map<String, dynamic> toJson() => {
//         "landDetails": landDetails?.toJson(),
//         "status_for_landlord": statusForLandlord,
//         "status_for_farmer": statusForFarmer,
//         "loanAgentStatus": loanAgentStatus,
//         "insuranceAgentStatus": insuranceAgentStatus,
//         "farmerDetails": farmerDetails == null ? [] : List<dynamic>.from(farmerDetails!.map((x) => x)),
//     };
// }

// class LandDetails {
//     int? id;
//     int? landLordId;
//     dynamic farmerId;
//     dynamic fullName;
//     dynamic landInAcres;
//     dynamic surveyNum;
//     dynamic water;
//     dynamic village;
//     dynamic description;
//     dynamic mandal;
//     dynamic district;
//     dynamic state;
//     dynamic latitude;
//     dynamic longitude;
//     dynamic radius;
//     int? imageId;
//     dynamic imageUrl;
//     dynamic flag;
//     dynamic createdAt;
//     dynamic updatedAt;
//     dynamic profile;
//     dynamic cropId;

//     LandDetails({
//         this.id,
//         this.landLordId,
//         this.farmerId,
//         this.fullName,
//         this.landInAcres,
//         this.surveyNum,
//         this.water,
//         this.village,
//         this.description,
//         this.mandal,
//         this.district,
//         this.state,
//         this.latitude,
//         this.longitude,
//         this.radius,
//         this.imageId,
//         this.imageUrl,
//         this.flag,
//         this.createdAt,
//         this.updatedAt,
//         this.profile,
//         this.cropId,
//     });

//     factory LandDetails.fromJson(Map<String, dynamic> json) => LandDetails(
//         id: json["id"],
//         landLordId: json["landLordId"],
//         farmerId: json["farmerId"],
//         fullName: json["fullName"],
//         landInAcres: json["landInAcres"],
//         surveyNum: json["surveyNum"],
//         water: json["water"],
//         village: json["village"],
//         description: json["description"],
//         mandal: json["mandal"],
//         district: json["district"],
//         state: json["state"],
//         latitude: json["latitude"]?.toDouble(),
//         longitude: json["longitude"]?.toDouble(),
//         radius: json["radius"],
//         imageId: json["image_id"],
//         imageUrl: json["image_url"],
//         flag: json["flag"],
//         createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
//         updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
//         profile: json["profile"],
//         cropId: json["cropId"],
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "landLordId": landLordId,
//         "farmerId": farmerId,
//         "fullName": fullName,
//         "landInAcres": landInAcres,
//         "surveyNum": surveyNum,
//         "water": water,
//         "village": village,
//         "description": description,
//         "mandal": mandal,
//         "district": district,
//         "state": state,
//         "latitude": latitude,
//         "longitude": longitude,
//         "radius": radius,
//         "image_id": imageId,
//         "image_url": imageUrl,
//         "flag": flag,
//         "createdAt": createdAt?.toIso8601String(),
//         "updatedAt": updatedAt?.toIso8601String(),
//         "profile": profile,
//         "cropId": cropId,
//     };
// }

// To parse this JSON data, do
//
//     final landByIdModel = landByIdModelFromJson(jsonString);

import 'dart:convert';

LandByIdModel landByIdModelFromJson(String str) => LandByIdModel.fromJson(json.decode(str));

String landByIdModelToJson(LandByIdModel data) => json.encode(data.toJson());

class LandByIdModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    LandByIdModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory LandByIdModel.fromJson(Map<String, dynamic> json) => LandByIdModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    LandDetails? landDetails;
    dynamic statusForLandlord;
    dynamic statusForFarmer;
    dynamic loanAgentStatus;
    dynamic insuranceAgentStatus;
    List<FarmerDetail>? farmerDetails;

    Result({
        this.landDetails,
        this.statusForLandlord,
        this.statusForFarmer,
        this.loanAgentStatus,
        this.insuranceAgentStatus,
        this.farmerDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        landDetails: json["landDetails"] == null ? null : LandDetails.fromJson(json["landDetails"]),
        statusForLandlord: json["status_for_landlord"],
        statusForFarmer: json["status_for_farmer"],
        loanAgentStatus: json["loanAgentStatus"],
        insuranceAgentStatus: json["insuranceAgentStatus"],
        farmerDetails: json["farmerDetails"] == null ? [] : List<FarmerDetail>.from(json["farmerDetails"]!.map((x) => FarmerDetail.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "landDetails": landDetails?.toJson(),
        "status_for_landlord": statusForLandlord,
        "status_for_farmer": statusForFarmer,
        "loanAgentStatus": loanAgentStatus,
        "insuranceAgentStatus": insuranceAgentStatus,
        "farmerDetails": farmerDetails == null ? [] : List<dynamic>.from(farmerDetails!.map((x) => x.toJson())),
    };
}

class FarmerDetail {
    int? id;
    dynamic name;
    dynamic address;
    dynamic mobileNumber;
    dynamic imageUrl;

    FarmerDetail({
        this.id,
        this.name,
        this.address,
        this.mobileNumber,
        this.imageUrl,
    });

    factory FarmerDetail.fromJson(Map<String, dynamic> json) => FarmerDetail(
        id: json["id"],
        name: json["name"],
        address: json["address"],
        mobileNumber: json["mobileNumber"],
        imageUrl: json["image_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "address": address,
        "mobileNumber": mobileNumber,
        "image_url": imageUrl,
    };
}

class LandDetails {
    int? id;
    int? landLordId;
    dynamic farmerId;
    dynamic fullName;
    dynamic landInAcres;
    dynamic surveyNum;
    dynamic water;
    dynamic village;
    dynamic description;
    dynamic mandal;
    dynamic district;
    dynamic state;
    dynamic latitude;
    dynamic longitude;
    dynamic radius;
    int? imageId;
    dynamic imageUrl;
    dynamic flag;
    dynamic createdAt;
    dynamic updatedAt;
    dynamic profile;
    int? cropId;

    LandDetails({
        this.id,
        this.landLordId,
        this.farmerId,
        this.fullName,
        this.landInAcres,
        this.surveyNum,
        this.water,
        this.village,
        this.description,
        this.mandal,
        this.district,
        this.state,
        this.latitude,
        this.longitude,
        this.radius,
        this.imageId,
        this.imageUrl,
        this.flag,
        this.createdAt,
        this.updatedAt,
        this.profile,
        this.cropId,
    });

    factory LandDetails.fromJson(Map<String, dynamic> json) => LandDetails(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        landInAcres: json["landInAcres"],
        surveyNum: json["surveyNum"],
        water: json["water"],
        village: json["village"],
        description: json["description"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        radius: json["radius"],
        imageId: json["image_id"],
        imageUrl: json["image_url"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        profile: json["profile"],
        cropId: json["cropId"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "fullName": fullName,
        "landInAcres": landInAcres,
        "surveyNum": surveyNum,
        "water": water,
        "village": village,
        "description": description,
        "mandal": mandal,
        "district": district,
        "state": state,
        "latitude": latitude,
        "longitude": longitude,
        "radius": radius,
        "image_id": imageId,
        "image_url": imageUrl,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "profile": profile,
        "cropId": cropId,
    };
}
