// To parse this JSON data, do
//
//     final getAllSavedJobsModel = getAllSavedJobsModelFromJson(jsonString);

import 'dart:convert';

GetAllSavedJobsModel getAllSavedJobsModelFromJson(String str) => GetAllSavedJobsModel.fromJson(json.decode(str));

String getAllSavedJobsModelToJson(GetAllSavedJobsModel data) => json.encode(data.toJson());

class GetAllSavedJobsModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    GetAllSavedJobsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetAllSavedJobsModel.fromJson(Map<String, dynamic> json) => GetAllSavedJobsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<SavedJob>? savedJobs;
    int? jobCount;

    Result({
        this.savedJobs,
        this.jobCount,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        savedJobs: json["savedJobs"] == null ? [] : List<SavedJob>.from(json["savedJobs"]!.map((x) => SavedJob.fromJson(x))),
        jobCount: json["jobCount"],
    );

    Map<String, dynamic> toJson() => {
        "savedJobs": savedJobs == null ? [] : List<dynamic>.from(savedJobs!.map((x) => x.toJson())),
        "jobCount": jobCount,
    };
}

class SavedJob {
    int? savedJobId;
    int? jobid;
    String? name;
    String? jobDescription;
    String? location;
    DateTime? createdBy;
    String? imageUrl;
    dynamic landlordid;
    dynamic farmerid;
    bool? flag;

    SavedJob({
        this.savedJobId,
        this.jobid,
        this.name,
        this.jobDescription,
        this.location,
        this.createdBy,
        this.imageUrl,
        this.landlordid,
        this.farmerid,
        this.flag,
    });

    factory SavedJob.fromJson(Map<String, dynamic> json) => SavedJob(
        savedJobId: json["savedJobId"],
        jobid: json["jobid"],
        name: json["Name"],
        jobDescription: json["jobDescription"],
        location: json["location"],
        createdBy: json["createdBy"] == null ? null : DateTime.parse(json["createdBy"]),
        imageUrl: json["image_url"],
        landlordid: json["landlordid"],
        farmerid: json["farmerid"],
        flag: json["flag"],
    );

    Map<String, dynamic> toJson() => {
        "savedJobId": savedJobId,
        "jobid": jobid,
        "Name": name,
        "jobDescription": jobDescription,
        "location": location,
        "createdBy": createdBy?.toIso8601String(),
        "image_url": imageUrl,
        "landlordid": landlordid,
        "farmerid": farmerid,
        "flag": flag,
    };
}
