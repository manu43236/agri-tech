// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';

// import '../../../../core/conts/color_consts.dart';

// class CustomDatePicker extends StatelessWidget {
//   final TextEditingController controller;
//   final String? Function(String?)? validator;  // Added validator parameter

//   CustomDatePicker({
//     required this.controller,
//     this.validator,  // Make sure to pass the validator during initialization
//   });

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
//       decoration: BoxDecoration(
//         border: Border.all(color: colorTextFieldBorder, width: 2),
//         borderRadius: BorderRadius.circular(12.0),
//       ),
//       child: TextFormField(
//         controller: controller, // editing controller of this TextField
//         decoration: const InputDecoration(
//           border: InputBorder.none,
//           icon: Icon(Icons.calendar_today), // icon of text field
//         ),
//         readOnly: true, // set it true, so that user will not able to edit text
//         validator: validator,  // Apply the validator here
//         onTap: () async {
//           DateTime? pickedDate = await showDatePicker(
//             context: context,
//             initialDate: DateTime.now(),
//             firstDate: DateTime.now(), // DateTime.now() - not to allow to choose before today.
//             lastDate: DateTime(2101),
//           );

//           if (pickedDate != null) {
//             String formattedDate = DateFormat('yyyy-MM-dd').format(pickedDate);
//             controller.text = formattedDate; // set output date to TextField value.
//           } else {
//             print('Date is not selected');
//           }
//         },
//       ),
//     );
//   }
// }
// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import '../../../../core/conts/color_consts.dart';

// class CustomDatePicker extends StatelessWidget {
//   final TextEditingController controller;
//   final String? Function(String?)? validator;

//   CustomDatePicker({
//     required this.controller,
//     this.validator,
//   });

//   @override
//   Widget build(BuildContext context) {
//     String? errorText = validator != null ? validator!(controller.text) : null;

//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         Container(
//           padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
//           decoration: BoxDecoration(
//             border: Border.all(color: colorTextFieldBorder, width: 2),
//             borderRadius: BorderRadius.circular(12.0),
//           ),
//           child: TextFormField(
//             controller: controller,
//             decoration: const InputDecoration(
//               border: InputBorder.none,
//               icon: Icon(Icons.calendar_today),
//             ),
//             readOnly: true,
//             onTap: () async {
//               DateTime? pickedDate = await showDatePicker(
//                 context: context,
//                 initialDate: DateTime.now(),
//                 firstDate: DateTime.now(),
//                 lastDate: DateTime(2101),
//               );
//               if (pickedDate != null) {
//                 String formattedDate =
//                     DateFormat('yyyy-MM-dd').format(pickedDate);
//                 controller.text = formattedDate;
//               } else {
//                 print('Date is not selected');
//               }
//             },
//           ),
//         ),
//         // Display the validation message below the container
//         if (errorText != null && errorText.isNotEmpty)
//           Padding(
//             padding: const EdgeInsets.only(top: 5.0),
//             child: Text(
//               errorText,
//               style: TextStyle(color: Colors.red, fontSize: 12),
//             ),
//           ),
//       ],
//     );
//   }
// }

// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';

// class CustomDatePicker extends StatelessWidget {
//   final TextEditingController controller;
//   final String? Function(String?)? validator;
//   final TextEditingController? endDateController;

//   CustomDatePicker({
//     required this.controller,
//     this.validator,
//     this.endDateController, required bool isStartDate, required DateTime lastDate, required DateTime firstDate, required void Function(DateTime date) onDateSelected,
//   });

//   @override
//   Widget build(BuildContext context) {
//     String? errorText = validator != null ? validator!(controller.text) : null;

//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         // Start Date Picker
//         Container(
//           padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
//           decoration: BoxDecoration(
//             border: Border.all(color: Colors.grey, width: 2),
//             borderRadius: BorderRadius.circular(12.0),
//           ),
//           child: TextFormField(
//             controller: controller,
//             decoration: const InputDecoration(
//               border: InputBorder.none,
//               icon: Icon(Icons.calendar_today),
//             ),
//             readOnly: true,
//             onTap: () async {
//               DateTime? pickedDate = await showDatePicker(
//                 context: context,
//                 initialDate: DateTime.now(),
//                 firstDate: DateTime.now(),
//                 lastDate: DateTime(2101),
//               );
//               if (pickedDate != null) {
//                 String formattedDate =
//                     DateFormat('yyyy-MM-dd').format(pickedDate);
//                 controller.text = formattedDate;

//                 // Update end date to disable past dates
//                 if (endDateController != null) {
//                   endDateController!.text = ''; // Clear previous selection
//                 }
//               } else {
//                 print('Date is not selected');
//               }
//             },
//           ),
//         ),

//         // Validation message for start date
//         if (errorText != null && errorText.isNotEmpty)
//           Padding(
//             padding: const EdgeInsets.only(top: 5.0),
//             child: Text(
//               errorText,
//               style: TextStyle(color: Colors.red, fontSize: 12),
//             ),
//           ),

//         // End Date Picker (if endDateController is provided)
//         if (endDateController != null)
//           Container(
//             padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
//             decoration: BoxDecoration(
//               border: Border.all(color: Colors.grey, width: 2),
//               borderRadius: BorderRadius.circular(12.0),
//             ),
//             child: TextFormField(
//               controller: endDateController,
//               decoration: const InputDecoration(
//                 border: InputBorder.none,
//                 icon: Icon(Icons.calendar_today),
//               ),
//               readOnly: true,
//               onTap: () async {
//                 DateTime? pickedDate = await showDatePicker(
//                   context: context,
//                   initialDate: DateTime.now(),
//                   firstDate: DateTime.parse(controller.text), // Disable dates before start date
//                   lastDate: DateTime(2101),
//                 );
//                 if (pickedDate != null) {
//                   String formattedDate =
//                       DateFormat('yyyy-MM-dd').format(pickedDate);
//                   endDateController!.text = formattedDate;
//                 } else {
//                   print('End date is not selected');
//                 }
//               },
//             ),
//           ),
//       ],
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CustomDatePicker extends StatelessWidget {
  final TextEditingController controller;
  final String? Function(String?)? validator;
  final TextEditingController? endDateController;

  CustomDatePicker({
    required this.controller,
    this.validator,
    this.endDateController,
    required bool isStartDate,
    required DateTime lastDate,
    required DateTime firstDate,
    required void Function(DateTime date) onDateSelected,
  });

  @override
  Widget build(BuildContext context) {
    String? errorText = validator != null ? validator!(controller.text) : null;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Start Date Picker
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey, width: 2),
            borderRadius: BorderRadius.circular(12.0),
          ),
          child: TextFormField(
            controller: controller,
            decoration: const InputDecoration(
              border: InputBorder.none,
              icon: Icon(Icons.calendar_today),
            ),
            readOnly: true,
            onTap: () async {
              DateTime? pickedDate = await showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime.now(),
                lastDate: DateTime(2101),
              );
              if (pickedDate != null) {
                String formattedDate =
                    DateFormat('yyyy-MM-dd').format(pickedDate);
                controller.text = formattedDate;

                // Update end date to disable past dates
                if (endDateController != null) {
                  endDateController!.text = ''; // Clear previous selection
                }
              } else {
                print('Date is not selected');
              }
            },
          ),
        ),

        // Validation message for start date
        if (errorText != null && errorText.isNotEmpty)
          Padding(
            padding: const EdgeInsets.only(top: 5.0),
            child: Text(
              errorText,
              style: TextStyle(color: Colors.red, fontSize: 12),
            ),
          ),

        // End Date Picker (if endDateController is provided)
        if (endDateController != null)
          Container(
            padding:
                const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 2),
              borderRadius: BorderRadius.circular(12.0),
            ),
            child: TextFormField(
              controller: endDateController,
              decoration: const InputDecoration(
                border: InputBorder.none,
                icon: Icon(Icons.calendar_today),
              ),
              readOnly: true,
              onTap: () async {
                // Ensure the start date is selected before allowing end date selection
                if (controller.text.isEmpty) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                        content: Text('Please select a start date first.')),
                  );
                  return;
                }

                DateTime startDate =
                    DateFormat('yyyy-MM-dd').parse(controller.text);

                DateTime? pickedDate = await showDatePicker(
                  context: context,
                  initialDate: startDate, // Use the selected start date
                  firstDate:
                      startDate, // Start date cannot be before the selected start date
                  lastDate: DateTime(2101),
                );
                if (pickedDate != null) {
                  String formattedDate =
                      DateFormat('yyyy-MM-dd').format(pickedDate);
                  endDateController!.text = formattedDate;
                } else {
                  print('End date is not selected');
                }
              },
            ),
          ),
      ],
    );
  }
}
