import 'package:get/get.dart';

import '../controllers/hired_farmworkers_details_controller.dart';

class HiredFarmworkersDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HiredFarmworkersDetailsController>(
      () => HiredFarmworkersDetailsController(),
    );
  }
}
