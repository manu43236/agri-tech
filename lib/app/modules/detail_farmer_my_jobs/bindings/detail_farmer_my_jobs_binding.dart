import 'package:get/get.dart';

import '../controllers/detail_farmer_my_jobs_controller.dart';

class DetailFarmerMyJobsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailFarmerMyJobsController>(
      () => DetailFarmerMyJobsController(),
    );
  }
}
