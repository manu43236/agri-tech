// import 'package:flutter/material.dart';

// import 'package:get/get.dart';

// import '../controllers/translator_icon_controller.dart';

// class TranslatorIconView extends GetView<TranslatorIconController> {
//   const TranslatorIconView({Key? key}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return AlertDialog(
//       title: const Text(
//         'Select a Language',
//         style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
//       ),
//       content: SizedBox(
//         width: double.maxFinite,
//         child: ListView.separated(
//           shrinkWrap: true,
//           itemCount: controller.locales.length,
//           separatorBuilder: (context, index) {
//             return const Divider(color: Colors.grey);
//           },
//           itemBuilder: (context, index) {
//             final locale = controller.locales[index];
//             return ListTile(
//               title: Text(locale['name']),
//               onTap: () {
//                 controller.updateLanguage(locale['locale']);
//                 Get.back();
//               },
//             );
//           },
//         ),
//       ),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../controllers/translator_icon_controller.dart';

class TranslatorIconView extends GetView<TranslatorIconController> {
  const TranslatorIconView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      backgroundColor: Colors.white,
      title: const Text(
        'Select a Language',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
          color: primary,
        ),
      ),
      content: SizedBox(
        width: double.maxFinite,
        child: ListView.separated(
          shrinkWrap: true,
          itemCount: controller.locales.length,
          separatorBuilder: (context, index) {
            return const Divider(color: Colors.grey, height: 1);
          },
          itemBuilder: (context, index) {
            final locale = controller.locales[index];
            return InkWell(
              onTap: () {
                controller.updateLanguage(locale['locale']);
                Get.back();
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16.0),
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(12),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.1),
                        spreadRadius: 1,
                        blurRadius: 5,
                        offset: Offset(0, 2),
                      ),
                    ],
                  ),
                  child: Row(
                    children: [
                      Icon(
                        Icons.language,
                        color: primary,
                      ),
                      const SizedBox(width: 12),
                      Text(
                        locale['name'],
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Colors.black87,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
