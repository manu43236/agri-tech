import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/text_fields/custom_search_widget.dart';
import '../../../routes/app_pages.dart';
import '../../select_region/views/select_region_view.dart';
import '../controllers/available_farmer_my_jobs_controller.dart';

class AvailableFarmerMyJobsView
    extends GetView<AvailableFarmerMyJobsController> {
  const AvailableFarmerMyJobsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'available_my_jobs'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
              Get.back();
          },
        ),
        actions: [
          InkWell(
            onTap: () {
               Get.bottomSheet(
                isScrollControlled: true,
                isDismissible: true,
                SelectRegionView(type: 'farmer_My_Jobs'.tr,)
              );
            },
            child: Image.asset(
              cone,
              height: 20,
              width: 20,
            ),
          ),
          const SizedBox(
            width: 20,
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Obx(
          () => SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 10,
                ),
                CustomSearchField(
                  onChanged: (val) {
                    controller.farmerMyJobsSearch.value = val;
                    if (val.length >= 3) {
                      controller.farmerMyJobsFilterJobsList(landlordId: '',farmerId: Get.find<HomeController>().userId.value,
    search: controller.farmerMyJobsSearch.value,state: '',district: '',mandal: '',village: '' );
                    } else if (val.isEmpty) {
                      controller.farmerMyJobsFilterJobsList(landlordId: '',farmerId: Get.find<HomeController>().userId.value,
    search:controller.farmerMyJobsSearch.value,state: '',district: '',mandal: '',village: '' );
                    }
                  },
                ),
                controller.dataSelectedItem.value.isEmpty
                      ? const SizedBox()
                      : Text('${'your_selected_region_is'.tr} ${controller.dataSelectedItem.value}' ?? "", style: TextStyles.kTSDS14W500
                                          .copyWith(color: colorHello),),
                const SizedBox(
                  height: 10,
                ),
                controller.apiFilterFarmerJobsStatus.value == ApiStatus.LOADING
                    ? const SizedBox()
                    : controller.farmerMyJobsFilterModel.value.result!
                            .isEmpty
                        ?  Align(
                            alignment: Alignment.center,
                            child: Text(
                              "no_jobs_found".tr,
                              style:
                                  TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                          )
                        : Column(
                            children: _avaliJob(),
                          )
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _avaliJob() {
    List<Widget> items = [];
    for (var index = 0;
        index <
            controller.farmerMyJobsFilterModel.value.result!.length;
        index++) {
      items.add(InkWell(
        onTap: () async {
          Get.find<HomeController>().jobId.value = controller
              .farmerMyJobsFilterModel.value.result![index].job!.id
              .toString();
          Get.toNamed(Routes.DETAIL_FARMER_MY_JOBS,arguments: {"jobId": Get.find<HomeController>().jobId.value});
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(11), topRight: Radius.circular(11)),
              child: Image.network(
                controller.farmerMyJobsFilterModel.value.result?
                        [index].job?.image ??
                    '',
                height: Get.height * 0.2,
                width: Get.width,
                fit: BoxFit.cover,
                errorBuilder: (context, error, stackTrace) {
                  return Image.asset(
                    defaultImageUrl,
                    height: Get.height * 0.2,
                    width: Get.width,
                    fit: BoxFit.cover,
                  );
                },
              ),
            ),
            Container(
              width: Get.width,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(11),
                      bottomRight: Radius.circular(11)),
                  color: colorAsh,
                  shape: BoxShape.rectangle),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${controller.farmerMyJobsFilterModel.value.result![index].job!.fullName}",
                      style: TextStyles.kTSDS14W500.copyWith(color: colorHello),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                        '${'location'.tr}${controller.farmerMyJobsFilterModel.value.result![index].job!.village}'),
                    const SizedBox(
                      height: 5,
                    ),
                    // InkWell(
                    //   onTap: () {},
                    //   child: Container(
                    //     padding: const EdgeInsets.symmetric(
                    //         horizontal: 10, vertical: 5),
                    //     decoration: const BoxDecoration(
                    //       color: colorCost,
                    //     ),
                    //     child: const Text(
                    //       'Enquiry',
                    //       style: TextStyle(
                    //         color: colorWhite,
                    //         fontSize: 12,
                    //       ),
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ));
    }
    return items;
  }
}
