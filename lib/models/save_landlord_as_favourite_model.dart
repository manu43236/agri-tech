// To parse this JSON data, do
//
//     final saveLandlordAsFavouriteModel = saveLandlordAsFavouriteModelFromJson(jsonString);

import 'dart:convert';

SaveLandlordAsFavouriteModel saveLandlordAsFavouriteModelFromJson(String str) => SaveLandlordAsFavouriteModel.fromJson(json.decode(str));

String saveLandlordAsFavouriteModelToJson(SaveLandlordAsFavouriteModel data) => json.encode(data.toJson());

class SaveLandlordAsFavouriteModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    SaveLandlordAsFavouriteModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SaveLandlordAsFavouriteModel.fromJson(Map<String, dynamic> json) => SaveLandlordAsFavouriteModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    int? userid;
    int? landlordid;
    dynamic role;
    bool? flag;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic farmerid;
    dynamic farmworkerid;
    dynamic landid;
    dynamic jobid;
    dynamic imageUrl;

    Result({
        this.id,
        this.userid,
        this.landlordid,
        this.role,
        this.flag,
        this.updatedAt,
        this.createdAt,
        this.farmerid,
        this.farmworkerid,
        this.landid,
        this.jobid,
        this.imageUrl,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        userid: json["userid"],
        landlordid: json["landlordid"],
        role: json["role"],
        flag: json["flag"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        farmerid: json["farmerid"],
        farmworkerid: json["farmworkerid"],
        landid: json["landid"],
        jobid: json["jobid"],
        imageUrl: json["image_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userid": userid,
        "landlordid": landlordid,
        "role": role,
        "flag": flag,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "farmerid": farmerid,
        "farmworkerid": farmworkerid,
        "landid": landid,
        "jobid": jobid,
        "image_url": imageUrl,
    };
}
