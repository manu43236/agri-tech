// import 'package:agritech/app/modules/suitable_crops/controllers/suitable_crops_controller.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// import '../../../../app/modules/home/controllers/home_controller.dart';
// import '../../../conts/color_consts.dart';
// import '../../../themes/text_styles.dart';
// import '../../../utils/widget_utils/search_dialog.dart';
// import '../../../utils/widget_utils/search_view.dart';
// import '../../../utils/widget_utils/selector_text_field.dart';
// import '../../base_controller.dart';

// class CropDropdown extends SearchViews<HomeController> {
//   final TextEditingController textEditingController;
//   final Function(String?)? onSelected;

//   CropDropdown({
//     super.key,
//     required this.textEditingController,
//     this.onSelected,
//   });

//   @override
//   Widget cBuild(BuildContext context, HomeController cController) {
//     return Obx(
//       () => cController.apiStatus == ApiStatus.LOADING
//           ? const CircularProgressIndicator()
//           : 
//           GestureDetector(
//               onTap: () async {
//                 List<dynamic> cropList = Get.find<SuitableCropsController>().getAllCropsModel.value.result?.crops ?? [];
//                 if (cropList.isEmpty) {
//                   Get.snackbar("No Crops Available", "Please add crops first");
//                   return;
//                 }

//                 // Display search dialog to select a crop
//                 List<String> items = cropList.map((crop) => crop.name.toString()).toList();
//                 var selectedItem = await Get.dialog(
//                   SearchSelectionDialogView(
//                     items: items,
//                     searchHint: 'Search Crop'.tr,
//                     title: 'Select Crop'.tr,
//                   ),
//                 );

//                 if (selectedItem != null) {
//                   textEditingController.text = selectedItem;
//                   var result = cropList.firstWhere((val) => val.name == selectedItem);

//                   // Update the observable in the controller
//                   Get.find<SuitableCropsController>().selectedCropName.value = result.name;
//                   onSelected?.call(Get.find<SuitableCropsController>().selectedCropName.value);
//                 }
//               },
//               child: SelectorCustomTextField(
//                 controller: textEditingController,
//                 // Update the hint text to show the selected crop or default hint
//                 hintText: Get.find<SuitableCropsController>().selectedCropName.value.isNotEmpty
//                     ? Get.find<SuitableCropsController>().selectedCropName.value
//                     : 'Select Crop'.tr,
//                 fieldBgColor: primary,
//                 containerPadding: EdgeInsets.symmetric(vertical: 0),
//                 lableStyle: TextStyles.kTSDS12W500,
//                 hintStyle: TextStyles.kTSFS16W700.copyWith(color: colorWhite),
//                 textStyle: textEditingController.text.isEmpty
//                         ? TextStyles.kTSFS16W700.copyWith(color: colorBlack)
//                         : TextStyles.kTSFS16W700.copyWith(color: colorWhite),
//                 isEnabled: false,
//                 suffixIconColor: Colors.white,
//                 showSufixIcon: const Icon(Icons.arrow_drop_down, color: colorWhite),
//               ),
//             ),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../app/modules/home/controllers/home_controller.dart';
import '../../../../app/modules/suitable_crops/controllers/suitable_crops_controller.dart';
import '../../../../app/modules/suitable_crops/data/get_crops_by_mandal_model.dart';
import '../../../conts/color_consts.dart';
import '../../../conts/img_const.dart';
import '../../../themes/text_styles.dart';
import '../../../utils/widget_utils/selector_text_field.dart';

class CropDropdown extends StatelessWidget {
  final TextEditingController textEditingController;
  final Function(String?)? onSelected;

  static const String defaultImageUrl = agri;

  CropDropdown({
    Key? key,
    required this.textEditingController,
    this.onSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        SuitableCropsController suitableCropsController = Get.find<SuitableCropsController>();

        // Show loading indicator while API is loading
        // if (suitableCropsController.apiStatus.value == ApiStatus.LOADING) {
        //   return const CircularProgressIndicator();
        // }

        // Fetch crop list and append "Others" only if it doesn't already exist
        List<Crop> cropList = suitableCropsController.getCropsByMandalModel.value.result?.crops ?? [];
        if (!cropList.any((crop) => crop.name == 'others'.tr)) {
          cropList.add(Crop(name: 'others'.tr, imageUrl: null)); // Add "Others" option only once
        }

        // Show message if no crops are available
        if (cropList.isEmpty) {
          return Center(child: Text("no_crops_available_please_add_crops_first".tr));
        }

        return GestureDetector(
          onTap: () async {
            var selectedCrop = await Get.dialog(
              Dialog(
                backgroundColor: Colors.white,
                child: Container(
                  height: Get.height*0.5,
                  padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 18),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text('select_crop'.tr, style: TextStyles. kTSFS20W700),
                      const SizedBox(height: 16),
                      // ListView to display crops
                      Expanded(
                        child: ListView.builder(
                          itemCount: cropList.length,
                          itemBuilder: (context, index) {
                            Crop crop = cropList[index];

                            return GestureDetector(
                              onTap: () {
                                Get.back(result: crop);
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8.0),
                                child: Row(
                                  children: [
                                    crop.imageUrl != null
                                        ? ClipOval(child: Image.network(crop.imageUrl!,height:Get.height*0.08,width: Get.width*0.16,fit: BoxFit.cover,))
                                        : const Icon(Icons.crop),
                                    const SizedBox(width: 20,height: 10,),
                                    Text(crop.name ?? 'Unknown', style: TextStyles.kTSDS12W500.copyWith(fontSize: 13)),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );

            if (selectedCrop != null) {
              if (selectedCrop.name == 'others'.tr) {
                // Open another dropdown or dialog for "Others"
                var otherCrop = await _showOtherCropsDropdown(context);
                if (otherCrop != null) {
                  textEditingController.text = otherCrop;
                  suitableCropsController.selectedCropName.value = otherCrop;
                  onSelected?.call(otherCrop);
                }
              } else {
                // Handle normal crop selection
                textEditingController.text = selectedCrop.name ?? '';
                suitableCropsController.selectedCropName.value = selectedCrop.name ?? '';
                onSelected?.call(selectedCrop.name);
              }
            }
          },
          child: SelectorCustomTextField(
            controller: textEditingController,
            hintText: suitableCropsController.selectedCropName.value.isNotEmpty
                ?  suitableCropsController.selectedCropName.value
                : 'select_crop'.tr,
            fieldBgColor: primary,
            containerPadding: EdgeInsets.symmetric(vertical: 0),
            lableStyle: TextStyles.kTSDS12W500,
            hintStyle: TextStyles.kTSFS16W700.copyWith(color: colorWhite),
            textStyle: textEditingController.text.isEmpty
                ? TextStyles.kTSFS16W700.copyWith(color: colorBlack)
                : TextStyles.kTSFS16W700.copyWith(color: colorWhite),
            isEnabled: false,
            suffixIconColor: Colors.white,
            showSufixIcon: const Icon(Icons.arrow_drop_down, color: colorWhite),
          ),
        );
      },
    );
  }
Future<String?> _showOtherCropsDropdown(BuildContext context) async {
  SuitableCropsController suitableCropsController = Get.find<SuitableCropsController>();
  List<Crop> otherCropsList = suitableCropsController
      .getCropsByMandalModel.value.result?.otherCrops ?? [];

  return await Get.dialog(
    Dialog(
      backgroundColor: Colors.white,
      child: Container(
        padding: const EdgeInsets.all(18.0),
        height:Get.height*0.5 ,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text('select_other_crop'.tr, style: TextStyles.kTSFS20W700),
            const SizedBox(height: 16),
            Expanded(
              child: ListView.builder(
                itemCount: otherCropsList.length,
                itemBuilder: (context, index) {
                  Crop crop = otherCropsList[index];
                  return GestureDetector(
                    onTap: () async {
                       Get.back(result: crop.name);
                      try {
                        // Set the selected crop name
                        suitableCropsController.selectedCropName.value = crop.name ?? '';

                        // Call APIs to fetch working days data
                        await suitableCropsController.getCropById(crop.id.toString());
                        await suitableCropsController.CreateSelectedCropWeek();
                        await suitableCropsController.getUserSelectedCrop(
                          landId:Get.find<HomeController>().landId.value,
                          cropId:suitableCropsController.cropId.value,
                          lang: suitableCropsController.selectedLang.value
                        );

                        // Extract and display working days
                        var workingDays = suitableCropsController
                            .getUserSelectedCropModel.value.result?.groupedCrops?.length;

                        if (workingDays != null) {
                          // Show working days info
                          Get.snackbar(
                            'working_days_info'.tr,
                            '${'no_of_working_days_for'.tr} ${crop.name}: $workingDays',
                            snackPosition: SnackPosition.BOTTOM,
                          );
                        } else {
                          // Handle case where working days data is unavailable
                          throw Exception('no_working_days_data_available'.tr);
                        }

                        // Return the crop name
                        Get.back(result: crop.name);
                      } catch (e) {
                        print(e);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        children: [
                          crop.imageUrl != null
                              ? ClipOval(
                                  child: Image.network(
                                    crop.imageUrl!,
                                    fit: BoxFit.cover,
                                    height: Get.height * 0.08,
                                    width: Get.width * 0.16,
                                  ),
                                )
                              : const Icon(Icons.crop),
                          const SizedBox(width: 16),
                          Text(crop.name ?? 'Unknown', style: TextStyles.kTSFS12W600.copyWith(fontSize: 13,fontWeight: FontWeight.w500)),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

}