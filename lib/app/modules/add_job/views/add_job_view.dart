import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import '../../../../core/base/dropdowns/view/district_drop_down.dart';
import '../../../../core/base/dropdowns/view/mandal_drop_down.dart';
import '../../../../core/base/dropdowns/view/state_drop_down.dart';
import '../../../../core/base/dropdowns/view/village_drop_down.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../../home/views/custom_text.dart';
import '../../add_land/views/custom_text_form_field.dart';
import '../controllers/add_job_controller.dart';

class AddJobView extends GetView<AddJobController> {
  AddJobView({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  void showPicker(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.photo_library),
                title: Text('gallery'.tr),
                onTap: () {
                  controller.pickImage(ImageSource.gallery);
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: const Icon(Icons.photo_camera),
                title: Text('camera'.tr),
                onTap: () {
                  controller.pickImage(ImageSource.camera);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'add_a_job'.tr,
          style: TextStyles.kTSFS24W600
              .copyWith(color: colorDetails, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () {
            Get.offNamed(Routes.DASHBOARD);
          },
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only( left: 20, right: 20,bottom: 20),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(name: 'job_name'.tr),
              const SizedBox(height: 5),
              CustomTextFormField(
                controller: controller.nameJobController.value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'required_name'.tr;
                  }
                  return null;
                },
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomText(name: 'start_date'.tr),
                        const SizedBox(height: 5),
                        Obx(
                          () => InkWell(
                            onTap: () async {
                              DateTime? date = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                builder: (context, child) {
                                  return Theme(
                                    data: ThemeData.light().copyWith(
                                      colorScheme: const ColorScheme.light(
                                        primary: primary,
                                        onSurface: colorUeBg,
                                      ),
                                      buttonTheme: const ButtonThemeData(
                                        colorScheme: ColorScheme.light(
                                          primary: primary,
                                          onSurface: colorUeBg,
                                        ),
                                      ),
                                    ),
                                    child: child!,
                                  );
                                },
                                firstDate: DateTime.now(),
                                lastDate: DateTime(2100),
                              );
                              if (date != null) {
                                controller.startDate.value =
                                    DateFormat('yyyy-MM-dd').format(date);
                                controller.startDateController.value.text =
                                    controller.startDate.value;
                                print(
                                    controller.startDateController.value.text);
                              }
                            },
                            child: Container(
                              //margin: const EdgeInsets.all(8.0),
                              padding: const EdgeInsets.all(8.0),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: colorTextFieldBorder, width: 2),
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(8.0),
                                ),
                              ),
                              child: Row(
                                children: [
                                  const Icon(
                                    Icons.calendar_month,
                                    color: addServiceDropDownBtnColor,
                                    size: 20,
                                  ),
                                  const SizedBox(
                                    width: 8.0,
                                  ),
                                  Text(controller.startDate.value),
                                  const Spacer(),
                                  const Icon(
                                    Icons.keyboard_arrow_down,
                                    color: addServiceDropDownBtnColor,
                                    size: 24.0,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(width: 5),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomText(name: 'end_date'.tr),
                        const SizedBox(height: 5),
                        Obx(() => InkWell(
                            onTap: controller.startDate.value == ""
                                ? () {}
                                : () async {
                                    DateTime? date = await showDatePicker(
                                      context: context,
                                      initialDate:
                                          controller.startDate.value == ""
                                              ? DateTime.now()
                                              : controller.convertStringToDate(
                                                  date: controller
                                                      .startDateController
                                                      .value
                                                      .text),
                                      builder: (context, child) {
                                        return Theme(
                                          data: ThemeData.light().copyWith(
                                            colorScheme:
                                                const ColorScheme.light(
                                              primary: primary,
                                              onSurface: colorUeBg,
                                            ),
                                            buttonTheme: const ButtonThemeData(
                                              colorScheme: ColorScheme.light(
                                                primary: primary,
                                                onSurface: colorUeBg,
                                              ),
                                            ),
                                          ),
                                          child: child!,
                                        );
                                      },
                                      firstDate: controller.convertStringToDate(
                                          date: controller
                                              .startDateController.value.text),
                                      lastDate: DateTime(2100),
                                    );
                                    if (date != null) {
                                      controller.endDate.value =
                                          DateFormat('yyyy-MM-dd').format(date);
                                      controller.endDateController.value.text =
                                          controller.endDate.value;
                                    }
                                  },
                            child: Container(
                             // margin: const EdgeInsets.all(8.0),
                              padding: const EdgeInsets.all(8.0),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: colorTextFieldBorder, width: 2),
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(8.0),
                                ),
                              ),
                              child: Row(
                                children: [
                                  const Icon(
                                    Icons.calendar_month,
                                    color: addServiceDropDownBtnColor,
                                    size: 20,
                                  ),
                                  const SizedBox(
                                    width: 8.0,
                                  ),
                                  Text(controller.endDate.value),
                                  const Spacer(),
                                  const Icon(
                                    Icons.keyboard_arrow_down,
                                    color: addServiceDropDownBtnColor,
                                    size: 24.0,
                                  )
                                ],
                              ),
                            )))
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              StateDropDown(
                textEditingController: controller.stateJobController.value,
                id: controller.selectedStateIds.value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'required_state'.tr;
                  }
                  return null;
                },
              ),
              const SizedBox(height: 10),
              DistrictDropDown(
                textEditingController: controller.districtJobController.value,
                id: controller.selectedDistrictIds.value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'required_district'.tr;
                  }
                  return null;
                },
              ),
              const SizedBox(height: 10),
              MandalDropDown(
                textEditingController: controller.mandalJobController.value,
                id: controller.selectedMandalIds.value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'required_mandal'.tr;
                  }
                  return null;
                },
              ),
              VillageDropDown(
                textEditingController: controller.villageJobController.value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'required_village'.tr;
                  }
                  return null;
                },
              ),
              const SizedBox(height: 10),
              CustomText(name: 'description'.tr),
              const SizedBox(height: 5),
              CustomTextFormField(
                controller: controller.descriptionJobController.value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'required_description'.tr;
                  }
                  return null;
                },
              ),
              CustomText(name: 'image'.tr),
              const SizedBox(height: 5),
              GestureDetector(
                onTap: () => showPicker(context),
                child: Container(
                  height: 150,
                  decoration: BoxDecoration(
                    border: Border.all(color: colorTextFieldBorder, width: 2),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Obx(
                    () {
                      final landImage = controller.landImage.value;
                      if (landImage == null) {
                        return Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const Icon(Icons.image,
                                  size: 50, color: Colors.grey),
                              Text('upload_image_here'.tr,
                                  style: TextStyles.kTSDS12W600),
                            ],
                          ),
                        );
                      } else {
                        return Stack(
                          children: [
                            Positioned.fill(
                              child: Image.file(landImage, fit: BoxFit.cover),
                            ),
                            Positioned(
                              top: 8,
                              right: 8,
                              child: GestureDetector(
                                onTap: () => controller.removeImage(),
                                child: const CircleAvatar(
                                  radius: 15,
                                  backgroundColor: Colors.red,
                                  child: Icon(Icons.close,
                                      size: 20, color: Colors.white),
                                ),
                              ),
                            ),
                          ],
                        );
                      }
                    },
                  ),
                ),
              ),
              const SizedBox(height: 20),
              Center(
                child: InkWell(
                  onTap: () {
                    controller.isFormSubmitted.value = true;
                    if (_formKey.currentState!.validate()) {
                      if (controller.startDateController.value.text.isEmpty ||
                          controller.endDateController.value.text.isEmpty) {
                        return; // Return early if any date is missing
                      }
                      if (controller.landPath.value.isEmpty) {
                        Get.snackbar('error'.tr, 'required_to_add_job_image'.tr,
                            snackPosition: SnackPosition.TOP,
                            backgroundColor: colorRed,
                            colorText: colorWhite);
                      } else {
                        print('on tapping');
                        if (controller.role.value == 'Landlord') {
                          print('add job');
                          controller.addJobs();
                        } else if (controller.role.value == 'Farmer') {
                          print('add farmer job');
                          controller.addJobByFarmer();
                        }
                      }
                    }
                  },
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 7),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(11),
                      color: primary,
                    ),
                    child: Text(
                      'add_job'.tr,
                      style: TextStyles.kTSFS18W500.copyWith(color: colorWhite),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
