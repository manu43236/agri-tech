import 'package:agritech/app/modules/add_skill/data/add_skill_model.dart';
import 'package:dartz/dartz.dart';

abstract class AddSkillRepo{
  Future<Either<AddSkillModel,Exception>> addSkill({params1});
}