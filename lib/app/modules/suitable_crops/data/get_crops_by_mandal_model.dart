import 'dart:convert';

GetCropsByMandalModel getCropsByMandalModelFromJson(String str) => GetCropsByMandalModel.fromJson(json.decode(str));

String getCropsByMandalModelToJson(GetCropsByMandalModel data) => json.encode(data.toJson());

class GetCropsByMandalModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetCropsByMandalModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetCropsByMandalModel.fromJson(Map<String, dynamic> json) => GetCropsByMandalModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<Crop>? crops;
    List<Crop>? otherCrops;

    Result({
        this.crops,
        this.otherCrops,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        crops: json["crops"] == null ? [] : List<Crop>.from(json["crops"]!.map((x) => Crop.fromJson(x))),
        otherCrops: json["otherCrops"] == null ? [] : List<Crop>.from(json["otherCrops"]!.map((x) => Crop.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "crops": crops == null ? [] : List<dynamic>.from(crops!.map((x) => x.toJson())),
        "otherCrops": otherCrops == null ? [] : List<dynamic>.from(otherCrops!.map((x) => x.toJson())),
    };
}

class Crop {
    int? id;
    dynamic name;
    dynamic totaldays;
    dynamic imageUrl;
    dynamic createdAt;
    dynamic updatedAt;

    Crop({
        this.id,
        this.name,
        this.totaldays,
        this.imageUrl,
        this.createdAt,
        this.updatedAt,
    });

    factory Crop.fromJson(Map<String, dynamic> json) => Crop(
        id: json["id"],
        name: json["name"],
        totaldays: json["totaldays"],
        imageUrl: json["image_url"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "totaldays": totaldays,
        "image_url": imageUrl,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}