import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/suitable_crops/services/suitable_crop_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../data/create_selected_crop_week_model.dart';
import '../data/get_crops_by_mandal_model.dart';
import '../data/get_crop_by_id_model.dart';
import '../data/get_user_selected_crop_model.dart';
import '../data/get_yields_in_hector_model.dart';

class SuitableCropRepoImpl extends SuitableCropRepo with NetworkCheckService{
  final DioClient _dioClient;
  SuitableCropRepoImpl(this._dioClient);

  @override
  Future<Either<GetCropsByMandalModel,Exception>> getCrops({id,lang})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Crop/getCropsByMandal?mandal=$id&language=$lang', Method.get);
        return result.fold((l){
          GetCropsByMandalModel model=GetCropsByMandalModel.fromJson(l.data);
          return Left(model);
        }, ((r)=>Right(Exception(r))));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetCropByIdModel,Exception>> getCropById({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Crop/getCropById?cropId=$params1', Method.get);
        return result.fold((l){
          GetCropByIdModel model=GetCropByIdModel.fromJson(l.data);
          return Left(model);
        }, ((r)=>Right(Exception(r))));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

   @override
  Future<Either<CreateSelectedCropWeekModel,Exception>> createCropWeek({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('UserSelectedCrop/createUserSelectedCrop', Method.post,params: params1);
        return result.fold((l){
          CreateSelectedCropWeekModel model=CreateSelectedCropWeekModel.fromJson(l.data);
          return Left(model);
        }, ((r)=>Right(Exception(r))));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

   @override
  Future<Either<GetUserSelectedCropModel,Exception>> getUserCrop({landId,cropId,lang})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('UserSelectedCrop/getUserSelectedCropByLandAndCropId?landid=$landId&cropId=$cropId&language=$lang', Method.get,);
        return result.fold((l){
          GetUserSelectedCropModel model=GetUserSelectedCropModel.fromJson(l.data);
          return Left(model);
        }, ((r)=>Right(Exception(r))));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<dynamic,Exception>>  updateUserCrop({params1})async{
    var data= await checkInternet();
    print(data);
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('UserSelectedCrop/updateUserSelectedCropByid', Method.post,params: params1);
        return result.fold((l){
          return Left(l.data);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetYieldsInHectorModel,Exception>> getYield({landId,cropId})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('UserSelectedCrop/getYieldsInHector?userid=${Get.find<HomeController>().userId.value}&landid=$landId&cropId=$cropId', Method.get,);
        return result.fold((l){
          GetYieldsInHectorModel model=GetYieldsInHectorModel.fromJson(l.data);
          return Left(model);
        }, ((r)=>Right(Exception(r))));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<dynamic,Exception>> updateYield({userId,landId,cropId,hector})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('UserSelectedCrop/updateYieldInHector?userid=$userId&landid=$landId&cropId=$cropId&hectors=$hector', Method.get,);
        return result.fold((l){
          return Left(l.data);
        }, ((r)=>Right(Exception(r))));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}