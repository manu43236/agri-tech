import 'package:agritech/app/modules/suitable_crops/data/get_crop_by_id_model.dart';
import 'package:agritech/app/modules/suitable_crops/data/get_yields_in_hector_model.dart';
import 'package:dartz/dartz.dart';

import '../data/create_selected_crop_week_model.dart';
import '../data/get_crops_by_mandal_model.dart';
import '../data/get_user_selected_crop_model.dart';

abstract class SuitableCropRepo{
  Future<Either<GetCropsByMandalModel,Exception>>  getCrops({id});
  Future<Either<GetCropByIdModel,Exception>>  getCropById({params1});
  Future<Either<CreateSelectedCropWeekModel,Exception>>  createCropWeek({params1});
  Future<Either<GetUserSelectedCropModel,Exception>>  getUserCrop({landId,cropId,lang});
  Future<Either<dynamic,Exception>>  updateUserCrop({params1});
  Future<Either<GetYieldsInHectorModel,Exception>> getYield();
  Future<Either<dynamic,Exception>> updateYield();
}