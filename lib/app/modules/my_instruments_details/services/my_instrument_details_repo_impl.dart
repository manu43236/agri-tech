 import 'package:agritech/app/modules/my_instruments_details/services/my_instruments_details_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../data/send_notification_for_loan_utility_model.dart';

class MyInstrumentDetailsRepoImpl extends MyInstrumentsDetailsRepo with NetworkCheckService{
  final DioClient _dioClient;
  MyInstrumentDetailsRepoImpl(this._dioClient);

  @override
  Future<Either<SendNotificationForLoanUtilityModel,Exception>> sendUtilityLoan({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('notification/sendNotificationForLoan', Method.post,params: params1);
        return result.fold((l){
          SendNotificationForLoanUtilityModel model=SendNotificationForLoanUtilityModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}