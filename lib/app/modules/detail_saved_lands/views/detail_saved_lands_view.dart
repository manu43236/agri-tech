import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/detail_saved_lands_controller.dart';

class DetailSavedLandsView extends GetView<DetailSavedLandsController> {
  const DetailSavedLandsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'detail'.tr,
          style: TextStyles.kTSFS20W500
              .copyWith(color: colorBlack, fontWeight: FontWeight.w400),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () =>controller.apiLandIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): 
        Padding(
          padding: const EdgeInsets.only(right: 20, left: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: colorAsh,
                    borderRadius: BorderRadius.circular(13),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.network(
                      (controller.landByIdModel.value.result
                                  ?.landDetails?.imageUrl?.isNotEmpty ??
                              false)
                          ? controller.landByIdModel.value
                              .result!.landDetails?.imageUrl!
                          : '',
                      height: Get.height * 0.3,
                      width: Get.width,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        // Display a local asset if the network image fails
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.3,
                          width: Get.width,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${controller.landByIdModel.value.result?.landDetails?.fullName ?? 'Unknown'} ${'at'.tr}',
                          style: TextStyles.kTSFS20W700
                              .copyWith(color: colorDetails),
                        ),
                        Text(
                          '${controller.landByIdModel.value.result?.landDetails?.village ?? ''}, ${controller.landByIdModel.value.result?.landDetails?.district ?? ''}',
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorDetails),
                        ),
                      ],
                    ),
                    // Obx(
                    //   () => InkWell(
                    //     onTap: () async {
                    //       if (controller.isFavourite.value == true) {
                    //         await controller.updateSavedUser(false);
                    //         await controller.allLandsSaved();
                    //         controller.isFavourite.value = false;
                    //       } else {
                    //         await controller.saveLandAsFavourite();
                    //         await controller.allLandsSaved();
                    //         controller.isFavourite.value = true;
                    //       }
                    //     },
                    //     child: Container(
                    //       padding: const EdgeInsets.symmetric(
                    //           horizontal: 10, vertical: 10),
                    //       decoration: BoxDecoration(
                    //         borderRadius: BorderRadius.circular(11),
                    //         color: controller.isFavourite.value
                    //             ? colorBlue.withOpacity(0.5)
                    //             : colorSave,
                    //       ),
                    //       child: Icon(
                    //         controller.isFavourite.value
                    //             ? Icons.bookmark
                    //             : Icons.bookmark_border_outlined,
                    //         color: colorBlack.withOpacity(0.5),
                    //       ),
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
                const SizedBox(height: 15),
                Row(
                  children: [
                    Image.asset(waterCycle, height: 15),
                    const SizedBox(width: 10),
                    Text(
                      controller.landByIdModel.value.result
                                  ?.landDetails?.water ==
                              'No'
                          ? 'not_available'.tr
                          : 'available'.tr,
                      style:
                          TextStyles.kTSFS12W400.copyWith(color: colorDetails),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        CircleAvatar(
                          radius: 26,
                          backgroundColor: colorAsh,
                          child: controller.homeController.getUserModel.value
                                      .result?.imageUrl !=
                                  null
                              ? ClipOval(
                                  child: Image.network(
                                    controller.homeController.getUserModel.value
                                        .result!.imageUrl
                                        .toString(),
                                    fit: BoxFit.cover,
                                    height: Get.height * 0.1,
                                    width: Get.width * 0.2,
                                  ),
                                )
                              : const Icon(Icons.person,
                                  size: 40, color: Colors.grey),
                        ),
                        const SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              controller.homeController.getUserModel.value
                                      .result?.firstName ??
                                  '',
                              style: TextStyles.kTSDS14W700
                                  .copyWith(color: colorPic),
                            ),
                            const SizedBox(height: 5),
                            Text(
                              controller.homeController.getUserModel.value
                                      .result?.address ??
                                  '',
                              style: TextStyles.kTSWFS10W700
                                  .copyWith(color: colorDetails),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Text(
                  controller.landByIdModel.value.result
                          ?.landDetails?.description ??
                      '',
                  style: TextStyles.kTSCF12W500,
                ),
                const SizedBox(height: 10),
                Text(
                  'saved_lands'.tr,
                  style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                ),
                const SizedBox(height: 10),
                SizedBox(
                  height: Get.height * 0.08,
                  width: Get.width,
                  child: Obx(
                    () {
                      final filteredSavedLands = controller
                          .allSavedLandsModel.value.result?.savedLands?.where((savedLand) =>
                              savedLand.landid !=
                              controller
                                  .landByIdModel
                                  .value
                                  .result
                                  ?.landDetails?.id)
                          .toList();

                      if (filteredSavedLands!.isEmpty) {
                        return Center(
                          child: Text(
                            'no_saved_lands_available'.tr,
                            style: TextStyles.kTSDS14W500
                                .copyWith(color: colorGrey),
                          ),
                        );
                      }

                      return ListView.separated(
                        itemCount: filteredSavedLands.length,
                        scrollDirection: Axis.horizontal,
                        separatorBuilder: (context, index) =>
                            const SizedBox(width: 10),
                        itemBuilder: (context, index) {
                          final savedLand = filteredSavedLands[index];

                          return InkWell(
                            onTap: () async {
                              await controller.getIdLands(id:savedLand.landid.toString() );
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: colorGrey,
                                borderRadius: BorderRadius.circular(11),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(11),
                                child: Image.network(
                                  (savedLand.imageUrl?.isNotEmpty ?? false)
                                      ? savedLand.imageUrl!
                                      : '',
                                  height: Get.height * 0.09,
                                  width: Get.width * 0.2,
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, error, stackTrace) {
                                    // Display a local asset if the network image fails
                                    return Image.asset(
                                      defaultImageUrl,
                                      height: Get.height * 0.09,
                                      width: Get.width * 0.2,
                                      fit: BoxFit.cover,
                                    );
                                  },
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
