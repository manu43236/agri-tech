import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../models/job_by_id_model.dart';
import '../../home/services/job_by_id_repo_impl.dart';
import '../../saved/data/get_all_saved_Jobs_model.dart';
import '../../saved/services/saved_repo_impl.dart';

class DetailSavedJobController extends BaseController {

  final HomeController homeController=Get.find<HomeController>();

  Rx<JobByIdModel> jobByIdModel = Rx(JobByIdModel());
  Rx<GetAllSavedJobsModel> getAllSavedJobsModel = Rx(GetAllSavedJobsModel());

  var apiJobIdStatus = ApiStatus.LOADING.obs;
  var id=''.obs;

  @override
  void onInit() async{
    super.onInit();
    id.value=await SecureStorage().readData(key: "id"??"");
    if(Get.arguments!=null){
      getIdJobs(id: Get.arguments["jobId"]);
    }
    getAllSavedJobs(userId: id.value);
  }

  // jobs by id API
  Future<void> getIdJobs({id}) async {
    apiJobIdStatus.value = ApiStatus.LOADING;
    try {
      var result = await JobByIdRepoImpl(dioClient).getJob(id: id);

      result.fold((left) {
        eachJobhandleResponse(left);
        apiJobIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiJobIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void eachJobhandleResponse(JobByIdModel model) async {
    if (model.statusCode == 200) {
      jobByIdModel.value = model;
    }
  }

    // get all saved jobs API
  Future<void> getAllSavedJobs({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient).getAllJobs(userId: userId);
      print("===================== get all saved jobs ==============");
      print(result);
      result.fold((model) {
        getAllSavedJobsModel.value = model;
        print('model${getAllSavedJobsModel.value}');
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
