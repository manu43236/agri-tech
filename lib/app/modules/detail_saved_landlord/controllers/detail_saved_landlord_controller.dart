import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../models/get_user_model.dart';
import '../../home/data/all_lands_of_landlord.dart';
import '../../home/services/get_user_repo_impl.dart';
import '../../home/services/home_repo_impl.dart';

class DetailSavedLandlordController extends BaseController {

  Rx<GetUserModel> getUserProfileModel = Rx(GetUserModel());
  Rx<AddLandsOfLandlordModel> addLandsOfLandlordModels =
      Rx(AddLandsOfLandlordModel());

 // final HomeController homeController=Get.find<HomeController>();
  //final LandlordController landlordController=Get.find<LandlordController>();

  var userId=''.obs;
  var apiUserByProfileIdStatus=ApiStatus.LOADING.obs;

  @override
  void onInit() {
    super.onInit();
    if(Get.arguments!=null){
      userId.value=Get.arguments["userId"];
    }
    getUserByProfileId(id:userId.value);
    addAllLandss(userId: userId.value);
  }

   // Get user profile by id API
  Future<void> getUserByProfileId({id}) async {
    apiUserByProfileIdStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await GetUserRepoImpl(dioClient).getUser(id: id);
      print("======================== user by id ====================");
      print('Result $result');
      result.fold((model) {
        getUserProfileModel.value = model;

        apiUserByProfileIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiUserByProfileIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> addAllLandss({userId}) async {
   addLandsOfLandlordModels.value.result?.ongoingLands?.clear();
  apiStatus.value = ApiStatus.LOADING;
  try {
    var result = await HomeRepoImpl(dioClient).doAddAllLands(id: userId);
    print('Fetching lands API result...');
    result.fold((left) {
      allLandshandleResponses(left);
      apiStatus.value = ApiStatus.SUCCESS;
    }, (r) {
      apiStatus.value = ApiStatus.FAIL;
    });
  } catch (e) {
    print('Error: $e');
    apiStatus.value = ApiStatus.FAIL;
  }
}

void allLandshandleResponses(AddLandsOfLandlordModel model) {
   if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
       addLandsOfLandlordModels.value.result?.ongoingLands?.clear();
       addLandsOfLandlordModels.value = model;
      print('77777777777777777777777');
      print( addLandsOfLandlordModels.value.result!.ongoingLands?[0].description);
    }
}

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
