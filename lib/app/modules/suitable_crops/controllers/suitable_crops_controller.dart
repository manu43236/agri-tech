import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/suitable_crops/data/get_yields_in_hector_model.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../data/create_selected_crop_week_model.dart';
import '../data/get_crops_by_mandal_model.dart';
import '../data/get_crop_by_id_model.dart';
import '../data/get_user_selected_crop_model.dart';
import '../data/update_user_selected_crop_model.dart';
import '../services/suitable_crop_repo_impl.dart';

class SuitableCropsController extends BaseController {
  Rx<TextEditingController> hectorsController = TextEditingController().obs;

  Rx<GetCropsByMandalModel> getCropsByMandalModel = Rx(GetCropsByMandalModel());
  Rx<GetCropByIdModel> getCropByIdModel = Rx(GetCropByIdModel());
  Rx<CreateSelectedCropWeekModel> createSelectedCropWeekModel =
      Rx(CreateSelectedCropWeekModel());
  Rx<GetUserSelectedCropModel> getUserSelectedCropModel =
      Rx(GetUserSelectedCropModel());
  Rx<UpdateUserSelectedCropModel> updateUserSelectedCropModel =
      Rx(UpdateUserSelectedCropModel());
  Rx<GetYieldsInHectorModel> getYieldsInHectorModel =
      Rx(GetYieldsInHectorModel());

  var cropId = ''.obs;
  var descriptionId = 0.obs;
  var selectedCropName = ''.obs;
  RxString selectedValue = ''.obs;
  var apiUserSelectedCropStatus = ApiStatus.LOADING.obs;

  var landIds = 0.obs;
  var cropIdss = 0.obs;
  var isEditing = false.obs;
  var hectorsText = ''.obs;
  var cropIdsss=0.obs;

  var dataa=[].obs;
  var dateCreated=''.obs;

  var selectedLang=''.obs;
  var userId=''.obs;

  @override
  void onInit() async {
    super.onInit();
    userId.value = await SecureStorage().readData(key: "id") ?? "";
    print('gggggggggggggggggg');
    print(Get.arguments["cropId"]);
    selectedLang.value = await SecureStorage().readData(key: "lang");
    if (Get.arguments != null) {
      // await getUserSelectedCrop(
      //     Get.arguments["landId"], Get.arguments["cropId"]);
      await getUserSelectedCrop(landId:Get.arguments["landId"],cropId:Get.arguments["cropId"],lang:selectedLang.value);
      await getAllCrops(id: Get.arguments["mandalId"],lang: selectedLang.value);
      await getYieldHectors(
          landId: Get.arguments["landId"], cropIds: Get.arguments["cropId"]);
    }
  }

  // void updateTextField(String value) {
  //   hectorsController.value.text = value;
  //   updateYieldHectors(
  //       landId: landId.value,
  //       cropIds: cropIdsss.value,
  //       hector: hectorsController.value.text);
  // }


  void toggleEditing() {
    // isEditing.value = !isEditing.value;
    if (!isEditing.value) {
      hectorsText.value = hectorsController.value.text; // Save value
      if (hectorsController.value.text.isNotEmpty) {
        updateYieldHectors(
            userId: userId.value,
            landId: landIds.value,
            cropIds: cropIdsss.value,
            hector: hectorsController.value.text);
        Fluttertoast.showToast(
          msg: "Tons saved: ${hectorsText.value}  /tons",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 14.0,
        );
      } else {
        Fluttertoast.showToast(
          msg: "Please enter the field.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 14.0,
        );
      }
    }
  }

  //get all crops
  Future<void> getAllCrops({id,lang}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SuitableCropRepoImpl(dioClient).getCrops(id: id,lang: lang);
      print("===================== get all crops ==============");
      print(result);
      result.fold((model) {
        getCropsByMandalModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //get crop by id
  Future<void> getCropById(String value) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await SuitableCropRepoImpl(dioClient).getCropById(params1: value);
      print("======================== get crop by id ====================");
      print(result);
      result.fold((model) {
        getCropByIdModel.value = model;
        cropId.value = getCropByIdModel.value.result!.id.toString();
        print('cropId${cropId.value}');
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //create selected crop week
  Future<void> CreateSelectedCropWeek() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await SuitableCropRepoImpl(dioClient).createCropWeek(params1: {
        "userid": Get.find<HomeController>().userId.value,
        "landid": Get.find<HomeController>().landId.value,
        "cropId": cropId.value,
      });
      print(
          "======================== create selected crop week ====================");
      print('userId:${Get.find<HomeController>().userId.value}');
      print("landid:${Get.find<HomeController>().landId.value}");
      print('cropId:${cropId.value}');
      print(result);
      result.fold((model) {
        createSelectedCropWeekModel.value = model;
        if (createSelectedCropWeekModel.value.statusCode == 400) {
          // getUserSelectedCrop(
          //   Get.find<HomeController>().landId.value,
          //   cropId.value,
          // );
          getUserSelectedCrop(landId:Get.find<HomeController>().landId.value,cropId:cropId.value,lang:selectedLang.value);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //get user selected crop
  Future<void> getUserSelectedCrop({landId,cropId,lang}) async {
    apiUserSelectedCropStatus.value = ApiStatus.LOADING;
    try {
      var result = await SuitableCropRepoImpl(dioClient)
          .getUserCrop(landId: landId,cropId: cropId,lang: lang);
      print(
          "======================== get user selected crop ====================");
      print(result);
      result.fold((model) {
        getUserSelectedCropModel.value = model;
        dataa.value = getUserSelectedCropModel.value.result!.groupedCrops!.first.crops!.where((element) => element.stepsflag == true).toList();
        print(getUserSelectedCropModel
                                                                .value
                                                                .result!.groupedCrops![0]
                                                                .crops![0]
                                                                .createdDate==DateTime.now());
        print(dataa.value.length);
        selectedValue.value = getUserSelectedCropModel
            .value.result!.masterCrops![0].name
            .toString();
          landIds.value = getUserSelectedCropModel
            .value.result!.agriYieldData!.first.landid!;

        cropIdsss.value = getUserSelectedCropModel
            .value.result!.agriYieldData!.first.cropId!;
        print('crop idss:${cropIdsss.value}');
        print('landId${landId}');
        apiUserSelectedCropStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiUserSelectedCropStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //update user selected crop
  Future<void> updateUserSelectedCrop(bool value) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SuitableCropRepoImpl(dioClient).updateUserCrop(
          params1: {"id": descriptionId.value, "stepsflag": value});
      print(descriptionId.value);
      print(value);
      print(
          "======================== update user selected crop ====================");
      print(result);
      result.fold((l) {
        //updateUserSelectedCropModel.value = model;
        print(l['message']);
        cropIdss.value=l['result']['cropId'];
        print(l['result']['cropId']);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //get yield hector API
  Future<void> getYieldHectors({landId, cropIds}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SuitableCropRepoImpl(dioClient)
          .getYield(landId: landId, cropId: cropIds);
      print("===================== get yield in hectors ==============");
      print(result);
      result.fold((model) {
        getYieldsInHectorModel.value = model;
        hectorsController.value.text =
            getYieldsInHectorModel.value.result![0].hectors.toString();
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //update yield hector API
  Future<void> updateYieldHectors({userId,landId, cropIds, hector}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SuitableCropRepoImpl(dioClient)
          .updateYield(userId: userId, landId: landId, cropId: cropIds, hector: hector);
      print("===================== update yield in hectors ==============");
      print(result);
      result.fold((l) {
        print(l['message']);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
