import 'package:agritech/app/modules/notifications/services/send_notification_by_landlord_to_worker_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../../../../models/send_notification_by_landlord_to_worker_model.dart';

class SendNotificationByLandlordToWorkerRepoImpl extends SendNotificationByLandlordToWorkerRepo with NetworkCheckService{
  final DioClient _dioClient;
  SendNotificationByLandlordToWorkerRepoImpl(this._dioClient);

  Future<Either<SendNotificationByLandlordToWorkerModel,Exception>> landlordToWorkerNotification({params1})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('notification/sendWorkNotificationByFarmerOrLandlordToFarmworker', Method.post,params: params1);
        return result.fold((l){
          SendNotificationByLandlordToWorkerModel model=SendNotificationByLandlordToWorkerModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}