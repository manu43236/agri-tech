import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../data/get_crop_availability_by_yield_payment_id_model.dart';
import '../services/detail_purchased_crop_repo_impl.dart';

class DetailPurchasedCropController extends BaseController {

  Rx<GetCropAvailabilityByYieldPaymentIdModel> getdetailsOfPurchasedCropModel =
      Rx(GetCropAvailabilityByYieldPaymentIdModel());

  var apiDetailsStatus=ApiStatus.LOADING.obs;

  @override
  void onInit() {
    super.onInit();
    if(Get.arguments!=null){
      getDetailsOfCrop(yieldId: Get.arguments["yieldId"],id: Get.arguments["id"]);
    }
  }

  Future<void> getDetailsOfCrop({yieldId,id}) async {
    apiDetailsStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailPurchasedCropRepoImpl(dioClient).detailPurchaseCrop(yieldId: yieldId,id: id);
      print('=============== get crop details =========');
      print(result);
      result.fold((model) {
        getdetailsOfPurchasedCropModel.value = model;
        apiDetailsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiDetailsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
