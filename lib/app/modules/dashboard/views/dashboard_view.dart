import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/conts/img_const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../../home/views/home_view.dart';
import '../../profile_page/views/profile_page_view.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:agritech/app/routes/app_pages.dart';
import '../controllers/dashboard_controller.dart';

class DashboardView extends GetView<DashboardController> {
  const DashboardView({super.key});

  @override
  Widget build(BuildContext context) {
    final homeController = Get.find<HomeController>();

    return Obx(
      () => WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          floatingActionButton: homeController.role != 'Vendor'
              ? FloatingActionButton(
                  onPressed: () {
                    Get.bottomSheet(
                      isScrollControlled: true,
                      isDismissible: true,
                      Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(21),
                            topRight: Radius.circular(21),
                          ),
                          color: colorWhite,
                        ),
                        child: Wrap(
                          children: [
                            // Top section of the bottom sheet
                            Container(
                              width: double.infinity,
                              padding: const EdgeInsets.symmetric(vertical: 11),
                              decoration: const BoxDecoration(
                                color: primary,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(21),
                                  topRight: Radius.circular(21),
                                ),
                              ),
                              child: Text(
                                'add'.tr,
                                textAlign: TextAlign.center,
                                style: TextStyles.kTSFS20W500.copyWith(
                                  color: colorWhite,
                                ),
                              ),
                            ),
                            // Conditional options based on user role
                            if (homeController.role.value == 'Utiliser')
                              // Show only 'Add Instrument' for Utiliser role
                              ListTile(
                                leading: const Icon(Icons.build),
                                title: InkWell(
                                  onTap: () {
                                    Get.toNamed(Routes.ADD_INSTRUMENT);
                                  },
                                  child:  Text('add_instrument'.tr),
                                ),
                            ),
                            if (homeController.role.value == 'Farm Worker')
                              // Show only 'Add Instrument' for Utiliser role
                              ListTile(
                                leading:  Image.asset(skillIcon,height: 25,fit: BoxFit.cover,),
                                title: InkWell(
                                  onTap: () {
                                    Get.toNamed(Routes.ADD_SKILL);
                                  },
                                  child:  Text('add_skill'.tr),
                                ),
                            ),
                            if (homeController.role.value == 'Farmer' ||
                                homeController.role.value == 'Landlord') ...[
                              // Show 'Add Land' and 'Add Job' for Farmer and Landlord roles
                              ListTile(
                                leading: const Icon(Icons.landscape),
                                title: InkWell(
                                  onTap: () {
                                    Get.toNamed(Routes.ADD_LAND);
                                  },
                                  child:  Text('add_land'.tr),
                                ),
                              ),
                              ListTile(
                                leading: const Icon(Icons.work),
                                title: InkWell(
                                  onTap: () {
                                    Get.toNamed(Routes.ADD_JOB);
                                  },
                                  child:  Text('add_job'.tr),
                                ),
                              ),
                            ],
                          ],
                        ),
                      ),
                    );
                  },
                  backgroundColor: primary,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: const Icon(Icons.add),
                )
              : null,
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          body: SafeArea(
            child: Obx(() {
              switch (controller.currentIndex.value) {
                case 0:
                  return controller.apiStatus.value == ApiStatus.LOADING
                      ? Shimmers().getListShimmer()
                      : HomeView();
                case 1:
                  return controller.apiStatus.value == ApiStatus.LOADING
                      ? Shimmers().getListShimmer()
                      : const ProfilePageView();
                default:
                  return const SizedBox.shrink();
              }
            }),
          ),
          bottomNavigationBar: BottomNavigationBar(
            selectedItemColor: primary,
            iconSize: 24,
            unselectedItemColor: colorGrey,
            onTap: (index) {
              controller.updateIndex(index);
            },
            currentIndex: controller.currentIndex.value,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            type: BottomNavigationBarType.fixed,
            items: [
              _bottomNavigationBarItem(icon: Icons.home, label: 'Home'),
              // _bottomNavigationBarItem(
              //     icon: CupertinoIcons.search, label: 'Search'),
              // _bottomNavigationBarItem(
              //     icon: CupertinoIcons.bell, label: 'Notification'),
              _bottomNavigationBarItem(
                  icon: CupertinoIcons.person_2_fill, label: 'Profile'),
            ],
          ),
        ),
      ),
    );
  }

  BottomNavigationBarItem _bottomNavigationBarItem({
    required IconData icon,
    required String label,
  }) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      label: label,
    );
  }
}
