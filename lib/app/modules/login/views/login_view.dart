import 'package:agritech/app/modules/login/controllers/login_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../translator_icon/views/translator_icon_view.dart';

class LoginView extends GetView<LoginController> {
  LoginView({Key? key}) : super(key: key);

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              SizedBox(
                  width: Get.width,
                  child: Image.asset(
                    appBarLogo,
                    fit: BoxFit.cover,
                  )),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 60),
                child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "login".tr,
                      style:
                          TextStyles.kTSFS26W400.copyWith(color: Colors.white),
                    )),
              ),
              //     IconButton(
              //   padding:const EdgeInsets.only(top: 63,bottom: 60,left:280) ,
              //   icon: Icon(Icons.language),
              //   color: colorWhite,
              //   onPressed: () {
              //     showDialog(
              //       context: context,
              //       builder: (context) {
              //         return const TranslatorIconView();
              //       },
              //     );
              //   },
              // ),
              Padding(
                padding: const EdgeInsets.only(top: 63,bottom: 60,left:280) ,
                child: InkWell(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return const TranslatorIconView();
                        },
                      );
                    },
                    child: Image.asset(
                      translatorIcon,
                      height: 35,
                    )),
              ),
            ],
          ),
          Expanded(child: Image.asset(farmer)),
          Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: colorBlack.withOpacity(0.2),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: const Offset(0, -3),
                  ),
                ],
                color: colorGhostWhite,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(27),
                    topRight: Radius.circular(27)),
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'phone_no'.tr,
                      style: TextStyles.kTSFS16W600.copyWith(color: colorBlack),
                    ),
                    TextFormField(
                      controller: controller.mobileController.value,
                      keyboardType: TextInputType.phone,
                      inputFormatters: [LengthLimitingTextInputFormatter(10)],
                      decoration: InputDecoration(
                        prefixIconConstraints:
                            const BoxConstraints(minWidth: 50, maxHeight: 50),
                        prefixIcon: SizedBox(
                          width: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Image.asset(telephone),
                              const SizedBox(
                                  height: 20,
                                  child: VerticalDivider(
                                    color: colorBlack,
                                    thickness: 1,
                                    width: 30,
                                  )),
                            ],
                          ),
                        ),
                        hintText: '+91 000-00-000'.tr,
                        hintStyle: TextStyles.kTSFS14WNORM
                            .copyWith(color: Colors.grey),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'required_mob_num'.tr;
                        } else if (value.length < 10) {
                          return 'enter_valid_mob_num'.tr;
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 60,
                    ),
                    Center(
                      child: InkWell(
                        onTap: () {
                          if (_formKey.currentState!.validate()) {
                            controller.doLoginGetOtp();
                          }
                          // if(Get.find<ProfileController>().role.value!=""){
                          //   controller.doLoginGetOtp();
                          // }
                          // else{
                          //   Get.toNamed(Routes.PROFILE);
                          //    //Get.snackbar('Failure', 'Please select role in Profile',snackPosition: SnackPosition.TOP,backgroundColor: colorRed,colorText: colorWhite);
                          // }
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 30, vertical: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(11),
                            color: primary,
                          ),
                          child: Text(
                            'get_otp'.tr,
                            style: TextStyles.kTSFS16W600
                                .copyWith(color: colorGhostWhite),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
