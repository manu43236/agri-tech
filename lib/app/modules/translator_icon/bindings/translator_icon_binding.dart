import 'package:get/get.dart';

import '../controllers/translator_icon_controller.dart';

class TranslatorIconBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TranslatorIconController>(
      () => TranslatorIconController(),fenix: true
    );
  }
}
