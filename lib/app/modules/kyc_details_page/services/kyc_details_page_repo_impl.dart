import 'package:agritech/app/modules/kyc_details_page/data/upload_id_document_model.dart';
import 'package:agritech/app/modules/kyc_details_page/services/kyc_details_page_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

class KycDetailsPageRepoImpl extends KycDetailsPageRepo
    with NetworkCheckService {
  final DioClient _dioClient;
  KycDetailsPageRepoImpl(this._dioClient);
  @override
  Future<Either<UploadIdDocumentModel, Exception>> uploadId({params1}) async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("NO Network found"));
    } else {
      try {
        var result = await _dioClient.requestForAuth(
            'Vender/uploadIdentityDocument', Method.post,
            params: params1);
        return result.fold((l) {
          UploadIdDocumentModel model = UploadIdDocumentModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      } catch (e) {
        return Right(Exception(e));
      }
    }
  }
}
