import 'package:agritech/app/modules/details_of_landlord_land/services/details_of_landlord_land_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../data/send_lease_interest_notification_by_farmer_fetch_model.dart';

class DetailsOfLandlordLandRepoImpl extends DetailsOfLandlordLandRepo with NetworkCheckService{
  final DioClient _dioClient;
  DetailsOfLandlordLandRepoImpl(this._dioClient);

  
  @override
   Future<Either<SendLeaseInterestNotificationByFarmerFetchModel,Exception>> farmerFetch({params1})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('notification/sendLeaseInterestNotificationByFarmer', Method.post,params: params1);
        return result.fold((l){
          SendLeaseInterestNotificationByFarmerFetchModel model= SendLeaseInterestNotificationByFarmerFetchModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}