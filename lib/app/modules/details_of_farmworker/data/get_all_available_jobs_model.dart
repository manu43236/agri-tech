// To parse this JSON data, do
//
//     final getAvailableJobsModel = getAvailableJobsModelFromJson(jsonString);

import 'dart:convert';

GetAvailableJobsModel getAvailableJobsModelFromJson(String str) => GetAvailableJobsModel.fromJson(json.decode(str));

String getAvailableJobsModelToJson(GetAvailableJobsModel data) => json.encode(data.toJson());

class GetAvailableJobsModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<Result>? result;

    GetAvailableJobsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetAvailableJobsModel.fromJson(Map<String, dynamic> json) => GetAvailableJobsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    dynamic fullName;
    dynamic description;
    dynamic status;
    int? landLordId;
    dynamic farmerId;
    dynamic state;
    dynamic district;
    dynamic mandal;
    dynamic village;
    dynamic image;

    Result({
        this.id,
        this.fullName,
        this.description,
        this.status,
        this.landLordId,
        this.farmerId,
        this.state,
        this.district,
        this.mandal,
        this.village,
        this.image,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        fullName: json["fullName"],
        description: json["description"],
        status: json["status"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "fullName": fullName,
        "description": description,
        "status": status,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
        "image": image,
    };
}
