import 'package:agritech/app/modules/home/services/district_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/app/modules/select_region/data/district_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/storage_utils/secure_storage.dart';

class DistrictDropdownController extends BaseController {

  var selectMake = 'Select District'.obs;
  var district= <String>[].obs;
  Rx<List<DistrictResult>> districtResult = Rx([]);
  var items = <String>[].obs;
  var selectedLang=''.obs;

  TextEditingController districtController = TextEditingController();
  @override
  void onInit() async{
    selectedLang.value = await SecureStorage().readData(key: "lang");
    super.onInit();
  }


  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  getAllDistricts({id,lang}) async {
    apiStatus.value = ApiStatus.LOADING;
    print(id);
    district.clear();
    items.clear();
    if(id != 0){
    var result = await DistrictRepoImpl(dioClient).getDistrict(id: id,lang: lang);
    print("result");
    print(result);
    result.fold((l) {
      if (l.result!.isNotEmpty) {
        districtResult.value = l.result!;
        for (var element in l.result!) {
          items.add(element.name!);
        }
        district.addAll(items);
        
        print(district);
      }
      apiStatus.value = ApiStatus.SUCCESS;
    
    }, (r) {
     
      apiStatus.value = ApiStatus.FAIL;
    });
  }
  }
}