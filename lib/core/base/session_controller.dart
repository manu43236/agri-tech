import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../conts/app_consts.dart';
import '../utils/storage_utils/secure_storage.dart';

class SessionController extends BaseController {
  RxString initialRoute = ''.obs;
  @override
  void onInit() {
    checkUserSession();
    super.onInit();
  }

  void checkUserSession() async {
    String token = await SecureStorage().readData(key: MOBILE ) ?? '';
    print('======================token================');
    print(token);
    if (token.isEmpty) {
      print('EMPTY');
      initialRoute.value = Routes.DASHBOARD;
    } else {
      print('NOT EMPTY');
      initialRoute.value = Routes.WELCOME_PAGE;
    }
  }
}
