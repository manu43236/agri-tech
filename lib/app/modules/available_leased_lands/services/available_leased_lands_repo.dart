import 'package:dartz/dartz.dart';

import '../../home/data/get_leased_lands_details_model.dart';

abstract class AvailableLeasedLandsRepo{
  Future<Either<GetLeasedLandsDetailsModel,Exception>>  leasedSearchLands();
}