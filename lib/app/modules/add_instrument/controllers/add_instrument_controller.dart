import 'dart:io';

import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/utility/controllers/utility_controller.dart';
import 'package:agritech/app/modules/utility/data/create_utility_model.dart';
import 'package:agritech/app/modules/utility/services/utility_repo_impl.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dio/dio.dart' as dio;

import '../../../../core/conts/color_consts.dart';
import '../../dashboard/controllers/dashboard_controller.dart';
import '../../my_instruments/controllers/my_instruments_controller.dart';

class AddInstrumentController extends BaseController {

  var nameController = TextEditingController().obs;
  var priceController = TextEditingController().obs;
  var brandController = TextEditingController().obs;
  var stateController = TextEditingController().obs;
  var districtController = TextEditingController().obs;
  var mandalController = TextEditingController().obs;
  var villageController = TextEditingController().obs;
  var descriptionController = TextEditingController().obs;
  var imageController = TextEditingController().obs;

  var landImage = Rx<File?>(null);
  RxString landPath = ''.obs;
  RxString landFile = ''.obs;

  var selectedStateIds = 0.obs;
  var selectedDistrictIds = 0.obs;
  var selectedMandalIds = 0.obs;
  var isFormSubmitted = false.obs;
  var utilityId=''.obs;
  var userId=''.obs;


  @override
  void onInit() async{
    userId.value=await SecureStorage().readData(key: "id"??'');
    super.onInit();
  }

  void setImagePath(String path) {
    imageController.value.text = path;
  }

  void removeImage() {
    landImage.value = null;
  }

  Future<void> pickImage(ImageSource source) async {
    final XFile? image = await ImagePicker().pickImage(source: source);

    if (image != null) {
      landImage.value = File(image.path);
      landFile.value = image.name;
      landPath.value = image.path;
      setImagePath(image.path);
    } else {
      Get.snackbar('Error', 'No image selected');
    }
  }

  //Add instrument by Utiliser API
  void addInstrument() async {
    apiStatus.value = ApiStatus.LOADING;
    if (nameController.value.text.isNotEmpty &&
        priceController.value.text.isNotEmpty&&
        brandController.value.text.isNotEmpty&&
        stateController.value.text.isNotEmpty &&
        districtController.value.text.isNotEmpty &&
        mandalController.value.text.isNotEmpty &&
        villageController.value.text.isNotEmpty &&
        descriptionController.value.text.isNotEmpty &&
        imageController.value.text.isNotEmpty) {
      try {
        dio.FormData formData = dio.FormData.fromMap({
          "utililityId": Get.find<HomeController>().selectedLandlordId.value,
          "nameOfInstrument": nameController.value.text,
          "pricePerDay": priceController.value.text,
          "brandName": brandController.value.text,
          "state": stateController.value.text,
          "district": districtController.value.text,
          "mandal": mandalController.value.text,
          "village": villageController.value.text,
          "longitude":Get.find<HomeController>().longitude.value,
          "latitude":Get.find<HomeController>().latitude.value,
          "description": descriptionController.value.text,
          "file": await dio.MultipartFile.fromFile(
            landPath.value,
            filename: landFile.value,
          ),
        });
        print('000000000000000000000000000000000000');
        print(formData);
        var result =
            await UtilityRepoImpl(dioClient).addInstrument(params1: formData);
        print("result:$result");
        result.fold((left) {
          _handleResponse(left);
          apiStatus.value = ApiStatus.SUCCESS;
        }, (right) {
          apiStatus.value = ApiStatus.FAIL;
        });
      } catch (e) {
        print('Error: $e');
      }
    } else {
      print('One or more fields are empty.');
    }
  }

  void _handleResponse(CreateUtilityModel model) async {
    if (model.statusCode == 200) {
      utilityId.value = model.result!.id.toString();
      await Get.find<UtilityController>().userUtilities(userId:userId.value,search: '',state: '',district: '',mandal: '',village: '' );
      await Get.find<UtilityController>().recentUtilities(search: Get.find<UtilityController>(). mainUtilSearch.value);
      await Get.find<MyInstrumentsController>().getUtilityById();
      //Get.back();
      Get.find<DashboardController>().currentIndex.value = 0;
      Get.toNamed(Routes.DASHBOARD);
      // Get.find<DashboardController>().pageController.jumpToPage(0);
      nameController.value.clear();
      priceController.value.clear();
      brandController.value.clear();
      stateController.value.clear();
      districtController.value.clear();
      mandalController.value.clear();
      villageController.value.clear();
      descriptionController.value.clear();
      removeImage();
       Get.snackbar(
        'Success', 
        'Instrument added successfully',
        backgroundColor: colorGreen,
        colorText: colorBlack
      );
    } else if (model.statusCode == 400) {
      Get.snackbar('Failed', ' A utility with the same details already exists',
          backgroundColor: colorRed,
          colorText: colorWhite,
          snackPosition: SnackPosition.TOP);
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  
}
