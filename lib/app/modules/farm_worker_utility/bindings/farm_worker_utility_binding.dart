import 'package:get/get.dart';

import '../controllers/farm_worker_utility_controller.dart';

class FarmWorkerUtilityBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FarmWorkerUtilityController>(
      () => FarmWorkerUtilityController(),
    );
  }
}
