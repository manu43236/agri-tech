import 'package:agritech/app/modules/purchased_checkout_details/data/get_ninty_percent_crop_payment_details_model.dart';
import 'package:dartz/dartz.dart';

abstract class PurchasedCheckoutDetailsRepo{
  Future<Either<GetNintyPercentCropPaymentModel,Exception>> getDetails();
}