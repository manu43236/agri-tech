import 'package:agritech/app/modules/Landlord_utilities/controllers/landlord_utilities_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/text_fields/custom_search_widget.dart';
import '../../../routes/app_pages.dart';


class LandlordUtilitiesView extends GetView<LandlordUtilitiesController> {
  const LandlordUtilitiesView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async{
          controller.utilitySearch.value='';
          controller.filterUtilitiesByLocation();
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'instruments'.tr,
                style: TextStyles.kTSDS12W700.copyWith(color: colorHello),
              ),
              const SizedBox(
                height: 10,
              ),
              CustomSearchField(
                    onChanged: (val) {
                      controller.utilitySearch.value=val;
                      print("Dataaaa");
                      print(controller.filterUtilitiesByLocationModel.value.result);
                      if(val.length>=3){
                        controller.filterUtilitiesByLocation();
                      }else if(val.isEmpty){
                        controller.filterUtilitiesByLocation();
                      }
                    },
              ),
               const SizedBox(
                height: 10,
              ),
              Expanded(
                child: Obx(() {
                  final utilities = controller
                      .filterUtilitiesByLocationModel
                      .value
                      .result;
                  if (utilities == null || utilities.isEmpty) {
                    return Center(
                      child: Text(
                        'no_instruments_available'.tr,
                        style: TextStyles.kTSDS12W700.copyWith(color: colorHello),
                      ),
                    );
                  }
        
                  return GridView.builder(
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, // 2 items per row
                      crossAxisSpacing: 10, // Space between grid items
                      childAspectRatio: 0.9,
                      mainAxisSpacing: 10, // Adjust the ratio for item height
                    ),
                    itemCount: utilities.length,
                    itemBuilder: (context, index) {
                      final utility = utilities[index];
                      final imageUrl = utility.image ?? defaultImageUrl;
        
                      return InkWell(
                        onTap: () async {
                          // await Get.find<SavedUtilitiesController>().getSavedUtilities();
                          // await Get.find<InstrumentDetailsController>()
                          //     .getUtilityById(id:  utility.id);
                          // await Get.find<DetailsOfLandlordUtilitiesController>().getUtilityByLocation();
                          Get.toNamed(Routes.DETAILS_OF_LANDLORD_UTILITIES,arguments: {"utilId":utility.id});
                        },
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ClipRRect(
                              borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(11),
                                topRight: Radius.circular(11),
                              ),
                              child: Image.network(
                                imageUrl,
                                height: Get.height * 0.12,
                                width: Get.width * 0.5,
                                fit: BoxFit.cover,
                                errorBuilder: (context, error, stackTrace) {
                                  // Display a local asset if the network image fails
                                  return Image.asset(
                                    defaultImageUrl,
                                    height: Get.height * 0.12,
                                    width: Get.width * 0.5,
                                    fit: BoxFit.cover,
                                  );
                                },
                              ),
                            ),
                            Expanded(
                              child: Container(
                                width: Get.width,
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.only(
                                    bottomLeft: Radius.circular(11),
                                    bottomRight: Radius.circular(11),
                                  ),
                                  color: colorGrey.shade100,
                                ),
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      utility.nameOfInstrument ?? '',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyles.kTSCF12W500
                                          .copyWith(color: colorHello),
                                    ),
                                    Text(
                                      utility.village ?? '',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyles.kTSCF12W500
                                          .copyWith(color: colorHello),
                                    ),
                                    Row(
                                      children: [
                                        Text('status'.tr,style: TextStyles.kTSCF12W500
                                              .copyWith(color: colorHello)),
                                        Text(
                                          '${utility.status==true?"available".tr:"leased".tr ?? 'N/A'}',
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyles.kTSCF12W500
                                              .copyWith(color: utility.status==true?colorGreen:colorRed),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  );
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
