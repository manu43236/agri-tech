
import 'package:agritech/app/modules/available_recent_lands/data/recent_filter_model.dart';
import 'package:dartz/dartz.dart';

import '../../home/data/recent_lands_model.dart';

abstract class AvailableRecentLandsRepo{
  Future<Either<RecentLandsModel,Exception>>  filterLocations({params1});
  Future<Either<RecentFilterModel,Exception>> filterRecentLocations();
}