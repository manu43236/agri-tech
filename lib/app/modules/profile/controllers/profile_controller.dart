import 'package:agritech/app/modules/otp/controllers/otp_controller.dart';
import 'package:agritech/app/modules/profile/data/get_user_roles_model.dart';
import 'package:agritech/app/modules/profile/services/profile_repo_impl.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/conts/app_consts.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/models/profile_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dio/dio.dart' as dio;
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../core/utils/widget_utils/location/location.dart';

class ProfileController extends BaseController {
  Rx<TextEditingController> mobileController = Rx(TextEditingController());
  Rx<TextEditingController> nameController = Rx(TextEditingController());
  Rx<TextEditingController> addressController = Rx(TextEditingController());
  Rx<TextEditingController> ageController = Rx(TextEditingController());
  //Rx<SingleValueDropDownController> roleController = Rx(SingleValueDropDownController());
  Rx<TextEditingController> descriptionController = Rx(TextEditingController());
  Rx<TextEditingController> roleController = Rx(TextEditingController());

  var longitude = 0.0.obs;
  var latitude = 0.0.obs;
  var address = "".obs;
  var image = "".obs;
  var profileName = "".obs;
  var role = "".obs;
  var profileImage = Rx<XFile?>(null);
  var profilePath = ''.obs;
  var profileFile = ''.obs;
  var selectedValue = ''.obs;

  var selectedLang=''.obs;

  var id = "".obs;
  List<String> dropdownItems = [
    "Landlord",
    "Farmer",
    "Farm Worker",
    "Utiliser"
  ];
  //var roleItems=[].obs;
  RxList<dynamic> roleItems = <dynamic>[].obs;

  //final ImagePicker _picker = ImagePicker();

  Rx<ProfileModel> profileModel = Rx(ProfileModel());
  Rx<GetUserRolesModel> getUserRolesModel = Rx(GetUserRolesModel());

  @override
  void onInit() async {
    super.onInit();
    selectedLang.value = await SecureStorage().readData(key: "lang");
    id.value = await SecureStorage().readData(key: "id");
    mobileController.value.text = Get.find<OtpController>().mobileNumber;
   Location location = Location();
    await location.determinePosition();
    await getUserRoles(lang: selectedLang.value);
   if (location.address.isNotEmpty) {
      latitude.value = location.latitude.toDouble();
      longitude.value = location.longitude.toDouble();
      address.value = location.address.toString();
      print('Address: ${address.value}');
    } else {
      print('Address could not be retrieved.');
    }
  }

  void setSelectedValue(String value) {
    selectedValue.value = value;
  }

  Future<void> updateProfile() async {
    if (mobileController.value.text.isNotEmpty &&
        nameController.value.text.isNotEmpty &&
        addressController.value.text.isNotEmpty &&
        roleController.value.text.isNotEmpty &&
        descriptionController.value.text.isNotEmpty) {
      try {
        var objj = {
          "id": int.parse(id.value),
          "mobileNumber": mobileController.value.text,
          "file": profilePath.value.isEmpty
              ? null
              : await dio.MultipartFile.fromFile(profilePath.value,
                  filename: profileFile.value),
          "address": addressController.value.text,
          "age": ageController.value.text,
          //"role":roleController.value.dropDownValue!.name,
          "role": roleController.value.text,
          "firstName": nameController.value.text,
          "longitude": longitude.value.toString(),
          "latitude": latitude.value.toString(),
          "Description": descriptionController.value.text,
          "location_details": address.value
        };
        dio.FormData formData = dio.FormData.fromMap(objj);
        var result =
            await ProfileRepoImpl(dioClient).doProfile(params1: formData);
        print('===============result=============');
        print(result);
        //print( await dio.MultipartFile.fromFile(selectedImagePath.value, filename: selectedImagePath.value.split('/').last));
        result.fold((left) {
          _handleResponse(left);
        }, (r) {
          apiStatus.value = ApiStatus.FAIL;
        });
      } catch (e) {
        print('Error: $e');
      }
    }
  }

  void _handleResponse(ProfileModel model) async {
    if (model.statusCode == 200) {
      print('==================dashboard==============');
      profileModel.value = model;
      profileName.value = profileModel.value.result!.firstName.toString();
      role.value = profileModel.value.result!.role.toString();
      image.value = profileModel.value.result!.imageUrl.toString();
      print(image.value);
      await SecureStorage().writeStringData("role", role.value);
      await SecureStorage().writeStringData(TOKEN, model.result!.accesstoken);
      await SecureStorage().writeStringData(USERIDS, model.result!.id.toString());
      print('profile role:${role.value}');
      print('token${model.result!.accesstoken}');

      Get.snackbar('success', 'User updated successfully',
          snackPosition: SnackPosition.TOP,
          backgroundColor: colorGreen,
          colorText: colorBlack);
      //Get.find<DashboardController>().currentIndex.value=0;
      if(role.value=="Vendor"){
        Get.toNamed(Routes.KYC_WELCOME_PAGE);
      }else{
        Get.offAllNamed(Routes.DASHBOARD);
      }
    }
  }

  //get user roles API
  Future<void> getUserRoles({lang}) async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result = await ProfileRepoImpl(dioClient).userRoles(lang: lang);
      print(result);
      result.fold((model) {
        getUserRolesModel.value = model;
        // roleItems.value=getUserRolesModel.value.result!;
        print(getUserRolesModel.value.result![0].name.toString());
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<void> pickImage(ImageSource source) async {
    final XFile? image = await ImagePicker().pickImage(source: source);

    if (image != null) {
      // selectedImagePath.value = pickedFile.path;
      profileImage.value = image;
      profileFile.value = profileImage.value!.name;
      profilePath.value = profileImage.value!.path;
      // Upload the image using Dio or any other method you prefer.
    } else {
      Get.snackbar('Error', 'No image selected');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
