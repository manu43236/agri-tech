import 'package:agritech/app/modules/notifications/controllers/notifications_controller.dart';
import 'package:agritech/app/modules/notifications/services/delete_notification_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

class DeleteNotificationRepoImpl extends DeleteNotificationRepo with NetworkCheckService{
  final DioClient _dioClient;
  DeleteNotificationRepoImpl(this._dioClient);
  Future<Either<dynamic,Exception>>  deleteNotification({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('notification/deleteNotification?notificationId=${Get.find<NotificationsController>().notificationId.value}', Method.get,params: params1);
        return result.fold((l){
          return Left(l.data);
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}