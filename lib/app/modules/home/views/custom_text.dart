import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';

class CustomText extends StatelessWidget {
  final String name;
  const CustomText({super.key, required this.name});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 6),
      child: Text(name,style: TextStyles.kTSDS12W500,),
    );
  }
}