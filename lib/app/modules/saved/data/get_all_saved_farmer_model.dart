// To parse this JSON data, do
//
//     final getAllSavedFarmerModel = getAllSavedFarmerModelFromJson(jsonString);

import 'dart:convert';

GetAllSavedFarmerModel getAllSavedFarmerModelFromJson(String str) => GetAllSavedFarmerModel.fromJson(json.decode(str));

String getAllSavedFarmerModelToJson(GetAllSavedFarmerModel data) => json.encode(data.toJson());

class GetAllSavedFarmerModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    GetAllSavedFarmerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetAllSavedFarmerModel.fromJson(Map<String, dynamic> json) => GetAllSavedFarmerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<SavedFarmer>? savedFarmers;
    int? farmerCount;

    Result({
        this.savedFarmers,
        this.farmerCount,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        savedFarmers: json["savedFarmers"] == null ? [] : List<SavedFarmer>.from(json["savedFarmers"]!.map((x) => SavedFarmer.fromJson(x))),
        farmerCount: json["farmerCount"],
    );

    Map<String, dynamic> toJson() => {
        "savedFarmers": savedFarmers == null ? [] : List<dynamic>.from(savedFarmers!.map((x) => x.toJson())),
        "farmerCount": farmerCount,
    };
}

class SavedFarmer {
    int? savedFarmerId;
    int? farmerid;
    String? firstName;
    String? locationDetails;
    String? mobileNumber;
    String? imageUrl;
    int? age;
    String? address;
    bool? flag;

    SavedFarmer({
        this.savedFarmerId,
        this.farmerid,
        this.firstName,
        this.locationDetails,
        this.mobileNumber,
        this.imageUrl,
        this.age,
        this.address,
        this.flag,
    });

    factory SavedFarmer.fromJson(Map<String, dynamic> json) => SavedFarmer(
        savedFarmerId: json["savedFarmerId"],
        farmerid: json["farmerid"],
        firstName: json["firstName"],
        locationDetails: json["location_details"],
        mobileNumber: json["mobileNumber"],
        imageUrl: json["image_url"],
        age: json["age"],
        address: json["address"],
        flag: json["flag"],
    );

    Map<String, dynamic> toJson() => {
        "savedFarmerId": savedFarmerId,
        "farmerid": farmerid,
        "firstName": firstName,
        "location_details": locationDetails,
        "mobileNumber": mobileNumber,
        "image_url": imageUrl,
        "age": age,
        "address": address,
        "flag": flag,
    };
}
