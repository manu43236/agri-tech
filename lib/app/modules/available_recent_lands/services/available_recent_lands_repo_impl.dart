import 'package:agritech/app/modules/available_recent_lands/services/available_recent_lands_repo.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../../home/data/recent_lands_model.dart';
import '../data/recent_filter_model.dart';

class AvailableRecentLandsRepoImpl extends AvailableRecentLandsRepo with NetworkCheckService{
  final DioClient _dioClient;
  AvailableRecentLandsRepoImpl(this._dioClient);

  @override
  Future<Either<RecentLandsModel,Exception>>  filterLocations({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Land/filterLandsByLocation', Method.post,params: params1);
        return result.fold((l){
          RecentLandsModel model=RecentLandsModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<RecentFilterModel,Exception>> filterRecentLocations({landlordId,farmerId,search,state,district,mandal,village})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Land/recent5,FilterOwnAndNearLands?landlordid=$landlordId&farmerid=$farmerId&search=$search&state=$state&district=$district&mandal=$mandal&village=$village&latitude=''&longitude=''', Method.get,);
        return result.fold((l){
          RecentFilterModel model=RecentFilterModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}