// To parse this JSON data, do
//
//     final getAllSavedUtilityModel = getAllSavedUtilityModelFromJson(jsonString);

import 'dart:convert';

GetAllSavedUtilityModel getAllSavedUtilityModelFromJson(String str) => GetAllSavedUtilityModel.fromJson(json.decode(str));

String getAllSavedUtilityModelToJson(GetAllSavedUtilityModel data) => json.encode(data.toJson());

class GetAllSavedUtilityModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetAllSavedUtilityModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetAllSavedUtilityModel.fromJson(Map<String, dynamic> json) => GetAllSavedUtilityModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<UtilitiesDetail>? utilitiesDetails;
    List<SavedUtility>? savedUtilities;

    Result({
        this.utilitiesDetails,
        this.savedUtilities,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        utilitiesDetails: json["utilitiesDetails"] == null ? [] : List<UtilitiesDetail>.from(json["utilitiesDetails"]!.map((x) => UtilitiesDetail.fromJson(x))),
        savedUtilities: json["savedUtilities"] == null ? [] : List<SavedUtility>.from(json["savedUtilities"]!.map((x) => SavedUtility.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "utilitiesDetails": utilitiesDetails == null ? [] : List<dynamic>.from(utilitiesDetails!.map((x) => x.toJson())),
        "savedUtilities": savedUtilities == null ? [] : List<dynamic>.from(savedUtilities!.map((x) => x.toJson())),
    };
}

class SavedUtility {
    int? id;
    int? utilityid;

    SavedUtility({
        this.id,
        this.utilityid,
    });

    factory SavedUtility.fromJson(Map<String, dynamic> json) => SavedUtility(
        id: json["id"],
        utilityid: json["utilityid"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "utilityid": utilityid,
    };
}

class UtilitiesDetail {
    int? id;
    int? utililityId;
    dynamic nameOfInstrument;
    dynamic pricePerDay;
    dynamic brandName;
    dynamic state;
    dynamic district;
    dynamic mandal;
    dynamic village;
    dynamic description;
    int? imageId;
    dynamic image;
    dynamic latitude;
    dynamic longitude;
    bool? status;
    dynamic createdAt;
    dynamic updatedAt;

    UtilitiesDetail({
        this.id,
        this.utililityId,
        this.nameOfInstrument,
        this.pricePerDay,
        this.brandName,
        this.state,
        this.district,
        this.mandal,
        this.village,
        this.description,
        this.imageId,
        this.image,
        this.latitude,
        this.longitude,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory UtilitiesDetail.fromJson(Map<String, dynamic> json) => UtilitiesDetail(
        id: json["id"],
        utililityId: json["utililityId"],
        nameOfInstrument: json["nameOfInstrument"],
        pricePerDay: json["pricePerDay"],
        brandName: json["brandName"],
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
        description: json["description"],
        imageId: json["image_id"],
        image: json["image"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        status: json["status"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "utililityId": utililityId,
        "nameOfInstrument": nameOfInstrument,
        "pricePerDay": pricePerDay,
        "brandName": brandName,
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
        "description": description,
        "image_id": imageId,
        "image": image,
        "latitude": latitude,
        "longitude": longitude,
        "status": status,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
