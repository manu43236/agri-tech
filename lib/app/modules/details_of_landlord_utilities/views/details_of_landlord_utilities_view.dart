import 'dart:ffi';

import 'package:agritech/app/modules/Landlord_utilities/controllers/landlord_utilities_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../../home/controllers/home_controller.dart';
import '../../utility/controllers/utility_controller.dart';
import '../controllers/details_of_landlord_utilities_controller.dart';

class DetailsOfLandlordUtilitiesView
    extends GetView<DetailsOfLandlordUtilitiesController> {
  const DetailsOfLandlordUtilitiesView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title: Text(
          'utility_details'.tr,
           style:  TextStyles.kTSFS24W600.copyWith(color: colorSuitable),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: ()async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20,),
        child: SingleChildScrollView(
          child: Obx(
            () =>controller.apigetUtilityStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer():
             Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Colors.grey.shade300,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.network(
                      controller
                              .getUtilityByIdModel
                              .value
                              .result
                              ?.utility?.image ??
                          defaultImageUrl,
                      height: Get.height * 0.3,
                      width: Get.width,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.3,
                          width: Get.width,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            '${controller.getUtilityByIdModel.value.result?.utility?.nameOfInstrument ?? ''}',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyles.kTSFS20W700
                                .copyWith(color: colorDetails),
                          ),
                          const SizedBox(height: 5),
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  '${controller.getUtilityByIdModel.value.result?.utility?.village ?? ''}, ${controller.getUtilityByIdModel.value.result?.utility?.mandal ?? ''}',
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 3,
                                  style: TextStyles.kTSDS14W500
                                      .copyWith(color: colorDetails),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                        width: 8), // Add some space before the status text
                    Flexible(
                      child: Obx(
                        () => InkWell(
                          onTap: () async {
                            if (controller.isUtilityFav.value ==
                                true) {
                              await Get.find<HomeController>().updateSavedUser(
                                  false,
                                  id: controller.deleteId.value,
                                  errorMessage: "utility_unsaved_successfully".tr);
                              await controller
                                  .getSavedUtilities(userId: controller.id.value);
                              controller.isUtilityFav.value = false;
                            } else {
                              await controller
                                  .saveUtilityAsFavourite(id:controller.getUtilityByIdModel.value.result!.utility!.id );
                              await controller.getSavedUtilities(userId: controller.id.value);
                              print('save updated');
                              controller.isUtilityFav.value = true;
                            }
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(11),
                              color: controller.isUtilityFav.value
                                  ? colorBlue.withOpacity(0.5)
                                  : colorSave,
                            ),
                            child: Icon(
                              controller.isUtilityFav.value
                                  ? Icons.bookmark
                                  : Icons.bookmark_border_outlined,
                              color: colorBlack.withOpacity(0.5),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                Text(
                  '${controller.getUtilityByIdModel.value.result?.utility?.pricePerDay ?? 0} ${'day'.tr}',
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.teal,
                  ),
                ),
                const SizedBox(height: 15),
                Text(
                  controller
                          .getUtilityByIdModel
                          .value
                          .result
                          ?.utility?.description ??
                      '',
                  style: TextStyles.kTSCF12W500,
                ),
                const SizedBox(height: 20),
                Text(
                  'more'.tr,
                  style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                ),
                const SizedBox(height: 10),
                SizedBox(
                  height: Get.height * 0.08,
                  width: Get.width,
                  child: Builder(
                    builder: (context) {
                      // Get the current selected utility ID
                      final selectedUtilityId =
                          controller
                                  .getUtilityByIdModel
                                  .value
                                  .result
                                  ?.utility?.id ??
                              '';

                      // Filter out the selected utility from the list
                      final filteredUtilities = Get.find<LandlordUtilitiesController>().filterUtilitiesByLocationModel
                              .value.result
                              ?.where(
                                  (utility) => utility.id != selectedUtilityId)
                              .toList() ??
                          [];

                      // Check if the list is empty after filtering
                      if (filteredUtilities.isEmpty) {
                        return  Center(
                          child: Text(
                            'no_utilities_found_near_your_location'.tr,
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: colorGrey,
                            ),
                          ),
                        );
                      }

                      // Display the remaining utilities in a horizontal ListView
                      return ListView.separated(
                        itemCount: filteredUtilities.length,
                        scrollDirection: Axis.horizontal,
                        separatorBuilder: (context, index) =>
                            const SizedBox(width: 10),
                        itemBuilder: (context, index) {
                          final utility = filteredUtilities[index];

                          return InkWell(
                            onTap: () async {
                              Get.find<UtilityController>().utilityId.value =
                                  utility.id.toString();
                              await controller
                                  .getUtilityById(id:  utility.id.toString());
                              await controller
                                  .getUtilityByLocation(village: utility.village.toString());
                              Get.toNamed(Routes.DETAILS_OF_LANDLORD_UTILITIES);
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.grey.shade400,
                                borderRadius: BorderRadius.circular(11),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(11),
                                child: Image.network(
                                  utility.image.toString(),
                                  height: Get.height * 0.09,
                                  width: Get.width * 0.2,
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, error, stackTrace) {
                                    return Image.asset(
                                      agri,
                                      height: Get.height * 0.09,
                                      width: Get.width * 0.2,
                                      fit: BoxFit.cover,
                                    );
                                  },
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    },
                  ),
                ),
                const SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _buildActionButton('call'.tr, Icons.call, () {}),
                    _buildActionButton(
                      'rent_now'.tr,
                      Icons.business_center,
                      () async {
                        // Get.find<UtilityController>().utilityId.value =
                        //     controller
                        //         .getUtilityByIdModel
                        //         .value
                        //         .result!.utility!
                        //         .id
                        //         .toString();
                        // await controller.createUtilityPayment();
                        // controller.createdUtilityId.value = controller
                        //     .createUtilityPaymentModel.value.result!.id
                        //     .toString();
                        // await controller.getUtilityPayment();
                        // controller.paymentUtilityId.value = controller
                        //     .getUtilityPaymentByIdModel.value.result!.id
                        //     .toString();
                        Get.toNamed(Routes.LANDLORD_UTILITIES_CHECKOUT_PAGE,
                            arguments: {
                              "id": controller
                                  .getUtilityByIdModel
                                  .value
                                  .result!.utility!
                                  .id!
                            });
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildActionButton(
      String label, IconData icon, Function()? onPressed) {
    return ElevatedButton.icon(
      onPressed: onPressed,
      icon: Icon(icon, color: colorWhite),
      label: Text(
        label,
        style: TextStyles.kTSFS16W700.copyWith(color: colorWhite),
      ),
      style: ElevatedButton.styleFrom(
        backgroundColor: primary,
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}
