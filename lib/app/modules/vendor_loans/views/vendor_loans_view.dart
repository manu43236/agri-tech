import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/vendor_loans_controller.dart';

class VendorLoansView extends GetView<VendorLoansController> {
  const VendorLoansView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('VendorLoansView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'VendorLoansView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
