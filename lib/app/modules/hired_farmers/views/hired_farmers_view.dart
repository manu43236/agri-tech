import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/hired_farmers_controller.dart';

class HiredFarmersView extends GetView<HiredFarmersController> {
   HiredFarmersView({Key? key}) : super(key: key);

  final imageSize = Get.width * 0.15;       
  final innerCircleSize = Get.width * 0.14; 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title: Text(
          'hired_farmers'.tr,
           style: TextStyles.kTSFS24W600.copyWith(color: colorHello,fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: ()async {
            Get.back();
          },
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Obx(() {
              final farmers = controller.hiredFarmersFarmworkersModel.value.result?.farmers;

              if (farmers == null || farmers.isEmpty) {
                return  Center(
                  child: Text(
                    'no_farmers_found'.tr,
                    style: TextStyles.kTSDS12W500,
                  ),
                );
              }

              return ListView.builder(
                itemCount: farmers.length,
                padding: const EdgeInsets.only(left: 20, right: 20,top: 20),
                itemBuilder: (context, index) {
                  final farmworker = farmers[index];
                  return InkWell(
                    onTap: () async {
                      Get.toNamed(Routes.HIRED_FARMERS_DETAILS,arguments: {"id":controller.hiredFarmersFarmworkersModel.value.result!.farmers![index].id.toString()});
                    },
                    child: Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(19),
                        boxShadow: [
                          BoxShadow(
                            color: colorBlack.withOpacity(0.2),
                            blurRadius: 5,
                            spreadRadius: 1,
                            offset: const Offset(0, 3),
                          ),
                        ],
                        color: colorWhite,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Stack(
                              alignment: Alignment.center,
                              children: [
                                Container(
                                  height: imageSize,
                                  width: imageSize,
                                  decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: colorCircleWorker,
                                  ),
                                ),
                                Positioned(
                                  left: 2,
                                  child: Container(
                                    height: innerCircleSize,
                                    width: innerCircleSize,
                                    decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: colorWhite,
                                    ),
                                  ),
                                ),
                                ClipOval(
                                  child: Image.network(
                                    farmworker.imageUrl ?? '',
                                    height: imageSize-5,
                                    width: imageSize-5,
                                    fit: BoxFit.cover,
                                    errorBuilder: (context, error, stackTrace) => const Icon(
                                      Icons.person,
                                      size: 47,
                                      color: colorCircleWorker,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(width: 30),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  farmworker.firstName ?? '-',
                                  style: TextStyles.kTSCF12W500,
                                ),
                                Text(
                                  farmworker.age.toString() ?? '-',
                                  style: TextStyles.kTSCF12W500,
                                ),
                                Text(
                                  farmworker.address ?? '-',
                                  style: TextStyles.kTSCF12W500,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            }),
          ),
        ],
      ),
    );
  }
}
