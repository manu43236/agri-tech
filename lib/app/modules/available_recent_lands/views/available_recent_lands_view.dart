import 'package:agritech/app/modules/select_region/views/select_region_view.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/available_recent_lands_controller.dart';

class AvailableRecentLandsView extends GetView<AvailableRecentLandsController> {
  const AvailableRecentLandsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'available_recent_lands'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () {
           // Get.toNamed(Routes.DASHBOARD);
           Get.back();
           //Get.back();
          },
        ),
        actions: [
          InkWell(
            onTap: () {
              // Get.toNamed(
              //   Routes.SELECT_REGION,
              //   arguments: {"type": "recentLands"},
              // );
              Get.bottomSheet(
                isScrollControlled: true,
                isDismissible: true,
                SelectRegionView(type: 'recent_Lands'.tr,)
              );
            },
            child: Image.asset(
              cone,
              height: 20,
              width: 20,
            ),
          ),
          const SizedBox(width: 20),
        ],
      ),
      body: Obx(
        () {
          // Display shimmer or loading indicator during API call
          if (controller.apiStatus.value == ApiStatus.LOADING) {
            return Shimmers().getListShimmer();
          }

          // Check if there are results or display no lands message
          final results = controller.recentFilterModel.value.result;
          if (results == null || results.isEmpty) {
            return const Center(
              child: Text(
                'No lands found matching the criteria',
                style: TextStyle(fontSize: 16),
              ),
            );
          }

          // Display list of lands
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                controller.dataSelectedItem.value.isEmpty
                   ? const SizedBox()
                   : Text('Your selected region is ${controller.dataSelectedItem.value}' ?? "", style: TextStyles.kTSDS14W500
                                          .copyWith(color: colorHello),),
                Expanded(
                  child: ListView.separated(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    itemCount: results.length,
                    separatorBuilder: (context, index) => const SizedBox(height: 10),
                    itemBuilder: (context, index) {
                      final land = results[index];
                      return InkWell(
                        onTap: () {
                          Get.toNamed(
                            Routes.DETAIL_RECENT_LANDS,
                            arguments: {"landId": land.id},
                          );
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ClipRRect(
                              borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(11),
                                topRight: Radius.circular(11),
                              ),
                              child: Image.network(
                                land.imageUrl ?? '',
                                height: Get.height * 0.2,
                                width: Get.width,
                                fit: BoxFit.cover,
                                errorBuilder: (context, error, stackTrace) {
                                  return Image.asset(
                                    defaultImageUrl,
                                    height: Get.height * 0.2,
                                    width: Get.width,
                                    fit: BoxFit.cover,
                                  );
                                },
                              ),
                            ),
                            Container(
                              width: Get.width,
                              decoration: const BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(11),
                                  bottomRight: Radius.circular(11),
                                ),
                                color: colorAsh,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 10,
                                  vertical: 10,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${land.fullName ?? ''} ${'at'.tr} ${land.village ?? ''}",
                                      style: TextStyles.kTSDS14W500
                                          .copyWith(color: colorHello),
                                    ),
                                    const SizedBox(height: 5),
                                    Text(
                                      '${'extent'.tr}${land.landInAcres ?? ''} ${'acres'.tr}      ${'water_facility'.tr}${land.water ?? 'No'}',
                                      style: TextStyles.kTSFS10W500
                                          .copyWith(color: colorHello),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
