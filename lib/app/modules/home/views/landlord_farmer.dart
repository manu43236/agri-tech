import 'package:agritech/app/modules/home/controllers/landlord_farmer_controller.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:agritech/core/utils/widget_utils/text_fields/custom_search_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../controllers/home_controller.dart';

class LandlordFarmer extends GetView<LandlordFarmerController> {
  const LandlordFarmer({super.key});

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    final imageSize = Get.width * 0.15;
    final innerCircleSize = Get.width * 0.14;

    return RefreshIndicator(
      onRefresh: ()async {
        controller.landlordFarmerSearch.value='';
        controller.filterFarmers(latitude:Get.find<HomeController>().latitude.value,longitude:Get.find<HomeController>().longitude.value,search:controller.landlordFarmerSearch.value);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Text(
              'available_farmers'.tr,
              style: TextStyles.kTSFS14W600
                  .copyWith(color: colorHello, fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: CustomSearchField(
              onChanged: (val) {
                controller.landlordFarmerSearch.value = val;
                if (val.length >= 3) {
                  controller.filterFarmers(latitude:Get.find<HomeController>().latitude.value,longitude:Get.find<HomeController>().longitude.value,search:controller.landlordFarmerSearch.value);
                } else if (val.isEmpty) {
                  controller.filterFarmers(latitude:Get.find<HomeController>().latitude.value,longitude:Get.find<HomeController>().longitude.value,search:controller.landlordFarmerSearch.value);
                }
              },
            ),
          ),
          Expanded(
            child: Obx(
              () {
                final farmers =
                    controller.landlordFarmersModel.value.result?.farmers ?? [];
      
                if (farmers.isEmpty) {
                  return  Align(
                    alignment: Alignment.center,
                    child: Text(
                      'no_farmers_found'.tr,
                      style: TextStyles.kTSDS12W500,
                    ),
                  );
                }
      
                return controller.apiStatus.value == ApiStatus.LOADING
                    ? Shimmers().getListShimmer()
                    : ListView.builder(
                        itemCount: farmers.length,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 20),
                        itemBuilder: (context, index) {
                          final farmer = farmers[index];
      
                          return InkWell(
                            onTap: () async {
                              Get.find<HomeController>().farmerId.value =
                                  farmer.id.toString();
                              Get.toNamed(Routes.DETAILS_OF_FARMER,arguments: {"farmerId":Get.find<HomeController>().farmerId.value});
                            },
                            child: Container(
                              margin: const EdgeInsets.symmetric(vertical: 10),
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(19),
                                boxShadow: [
                                  BoxShadow(
                                    color: colorBlack.withOpacity(0.2),
                                    blurRadius: 5,
                                    spreadRadius: 1,
                                    offset: const Offset(0, 3),
                                    blurStyle: BlurStyle.inner,
                                  ),
                                ],
                                color: colorWhite,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        Container(
                                          height: imageSize,
                                          width: imageSize,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: colorCircleFarmer,
                                          ),
                                        ),
                                        Positioned(
                                          left: 2,
                                          child: Container(
                                            height: innerCircleSize,
                                            width: innerCircleSize,
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: colorWhite,
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          child: Container(
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: colorCircleFarmer,
                                            ),
                                            child: ClipOval(
                                              child: Image.network(
                                                farmer.imageUrl ??
                                                    defaultImageUrl,
                                                height: imageSize - 5,
                                                width: imageSize - 5,
                                                fit: BoxFit.cover,
                                                errorBuilder:
                                                    (context, error, stackTrace) {
                                                  return Image.asset(
                                                    defaultImageUrl,
                                                    height: imageSize - 5,
                                                    width: imageSize - 5,
                                                    fit: BoxFit.cover,
                                                  );
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(width: 30),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          farmer.firstName ?? '-',
                                          style: TextStyles.kTSCF12W500,
                                        ),
                                        Text(
                                          farmer.age?.toString() ?? '-',
                                          style: TextStyles.kTSCF12W500,
                                        ),
                                        Text(
                                          farmer.address ?? '-',
                                          style: TextStyles.kTSCF12W500,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      );
              },
            ),
          ),
        ],
      ),
    );
  }
}
