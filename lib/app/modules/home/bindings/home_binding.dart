import 'package:agritech/app/modules/add_a_land/controllers/add_a_land_controller.dart';
import 'package:agritech/app/modules/dashboard/controllers/dashboard_controller.dart';
import 'package:agritech/app/modules/my_instruments/controllers/my_instruments_controller.dart';
import 'package:agritech/app/modules/details_of_farmworker/controllers/details_of_farmworker_controller.dart';
import 'package:agritech/app/modules/my_instruments_details/controllers/my_instruments_details_controller.dart';
import 'package:agritech/app/modules/utility/controllers/utility_controller.dart';
import 'package:agritech/core/base/dropdowns/controller/crops_dropdown_controller.dart';
import 'package:agritech/core/base/dropdowns/controller/district_dropdown_controller.dart';
import 'package:agritech/core/base/dropdowns/controller/state_dropdown_controller.dart';
import 'package:get/get.dart';

import '../../../../core/base/dropdowns/controller/mandal_dropdown_controller.dart';
import '../../../../core/base/dropdowns/controller/village_dropdown_controller.dart';
import '../../Landlord_utilities/controllers/landlord_utilities_controller.dart';
import '../../available_leased_jobs/controllers/available_leased_jobs_controller.dart';
import '../../available_leased_lands/controllers/available_leased_lands_controller.dart';
import '../../available_leased_utilities/controllers/available_leased_utilities_controller.dart';
import '../../details_of_farmer/controllers/details_of_farmer_controller.dart';
import '../../farm_worker_home_page/controllers/farm_worker_controller.dart';
import '../../landlord_utilities_checkout_page/controllers/landlord_utilities_checkout_page_controller.dart';
import '../../instrument_details/controllers/instrument_details_controller.dart';
import '../../notifications/controllers/notifications_controller.dart';
import '../../profile_page/controllers/profile_page_controller.dart';
import '../../saved_farmworkers/controllers/saved_farmworkers_controller.dart';
import '../../saved_jobs/controllers/saved_jobs_controller.dart';
import '../../saved_landlords/controllers/saved_landlords_controller.dart';
import '../../saved_utilities/controllers/saved_utilities_controller.dart';
import '../../translator_icon/controllers/translator_icon_controller.dart';
import '../../vendor/controllers/vendor_controller.dart';
import '../controllers/home_controller.dart';
import '../controllers/landlord_controller.dart';
import '../controllers/landlord_farmer_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
     Get.lazyPut<TranslatorIconController>(
      () => TranslatorIconController(),
    );
    Get.lazyPut<DashboardController>(
      () => DashboardController(),
    );
    Get.lazyPut<AddALandController>(
      () => AddALandController(),
    );
    Get.lazyPut<StateDropdownController>(
      () => StateDropdownController(),
    );
    Get.lazyPut<DistrictDropdownController>(
      () => DistrictDropdownController(),
    );
    Get.lazyPut<MandalDropdownController>(
      () => MandalDropdownController(),
    );
    Get.lazyPut<VillageDropdownController>(
      () => VillageDropdownController(),
    );

    Get.lazyPut<NotificationsController>(
      () => NotificationsController(),
    );
    Get.lazyPut<CropsDropdownController>(
      () => CropsDropdownController(),
    );
    Get.lazyPut<AvailableLeasedLandsController>(
      () => AvailableLeasedLandsController(),
    );
    Get.lazyPut<AvailableLeasedJobsController>(
      () => AvailableLeasedJobsController(),
    );
    Get.lazyPut<AvailableLeasedUtilitiesController>(
      () => AvailableLeasedUtilitiesController(),
    );
    Get.lazyPut<SavedUtilitiesController>(
      () => SavedUtilitiesController(),
    );
    Get.lazyPut<VendorController>(
      ()=>VendorController()
    );
    Get.lazyPut<ProfilePageController>(() => ProfilePageController());
    Get.lazyPut<FarmWorkerHomePageController>(
        () => FarmWorkerHomePageController());
    Get.lazyPut<LandlordUtilitiesController>(
        () => LandlordUtilitiesController());
    Get.lazyPut<LandlordUtilitiesCheckoutPageController>(
        () => LandlordUtilitiesCheckoutPageController());
    Get.lazyPut<UtilityController>(() => UtilityController());
    Get.lazyPut<InstrumentDetailsController>(
        () => InstrumentDetailsController());
    Get.lazyPut<MyInstrumentsDetailsController>(
        () => MyInstrumentsDetailsController());
    Get.lazyPut<MyInstrumentsController>(() => MyInstrumentsController());
    Get.lazyPut<DetailsOfFarmworkerController>(
        () => DetailsOfFarmworkerController());
    Get.lazyPut<DetailsOfFarmerController>(() => DetailsOfFarmerController());
    Get.lazyPut<SavedJobsController>(() => SavedJobsController());
    Get.lazyPut<SavedLandlordsController>(() => SavedLandlordsController());
    Get.lazyPut<SavedFarmworkersController>(() => SavedFarmworkersController());
    Get.lazyPut<LandlordFarmerController>(() => LandlordFarmerController());
    Get.lazyPut<TranslatorIconController>(
      ()=>TranslatorIconController()
    );
    Get.lazyPut<LandlordController>(
      ()=>LandlordController()
    );
  }
}
