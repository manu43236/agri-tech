import 'package:agritech/app/modules/saved/services/saved_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../../home/controllers/home_controller.dart';
import '../data/get_all_saved_Jobs_model.dart';
import '../data/get_all_saved_farmer_model.dart';
import '../data/get_all_saved_farmworkers_model.dart';
import '../data/get_all_saved_landlord_model.dart';
import '../data/get_all_saved_lands_model.dart';

class SavedRepoImpl extends SavedRepo with NetworkCheckService{
  final DioClient _dioClient;
  SavedRepoImpl(this._dioClient);

  @override
  Future<Either<AllSavedLandsModel,Exception>> allSavedLands({userId})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('save/getAllSavedLands?userid=$userId', Method.get);
        return result.fold((l){
          AllSavedLandsModel model=AllSavedLandsModel.fromJson(l.data);
          return Left(model);
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetAllSavedFarmerModel,Exception>> savedFarmersList({userId})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('save/getAllSavedFarmers?userid=$userId', Method.get);
        return result.fold((l){
          return Left(GetAllSavedFarmerModel.fromJson(l.data));
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetAllSavedLandlordModel,Exception>> savedLandlord({userId})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        print('idddd ${Get.find<HomeController>().selectedLandlordId.value}');
        var result= await _dioClient.requestForAuth('save/getAllSavedLandlords?userid=$userId', Method.get);
        return result.fold((l){
          return Left(GetAllSavedLandlordModel.fromJson(l.data));
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetAllSavedFarmworkerModel,Exception>> allFarmworkers({userId})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('save/getAllSavedFarmerworkers?userid=$userId', Method.get);
        return result.fold((l){
          GetAllSavedFarmworkerModel model=GetAllSavedFarmworkerModel.fromJson(l.data);
          return Left(model);
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<GetAllSavedJobsModel,Exception>> getAllJobs({userId})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('save/getAllSavedJobs?userid=$userId', Method.get);
        return result.fold((l){
          GetAllSavedJobsModel model=GetAllSavedJobsModel.fromJson(l.data);
          return Left(model);
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}