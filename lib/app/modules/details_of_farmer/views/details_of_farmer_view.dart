import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/details_of_farmer_controller.dart';
import 'lands_dropdown.dart';

class DetailsOfFarmerView extends GetView<DetailsOfFarmerController> {
  const DetailsOfFarmerView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'details_of_farmer'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: Obx(
          () {
            return controller.apiUserByProfileIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): 
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: ClipOval(
                            child: 
                            Image.network(
                       controller
                                  .getUserProfileModel
                                  .value
                                  .result!
                                  .imageUrl
                                  .toString()??
                          '',
                      height: Get.height * 0.4,
                    width: Get.height * 0.4,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.4,
                    width: Get.height * 0.4,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller
                                    .getUserProfileModel
                                    .value
                                    .result
                                    ?.firstName ??
                                'N/A',
                            style: TextStyles.kTSDS14W700
                                .copyWith(color: colorPic),
                          ),
                          const SizedBox(height: 5),
                          Text(
                            controller
                                    .getUserProfileModel
                                    .value
                                    .result
                                    ?.age
                                    .toString() ??
                                '0',
                            style: TextStyles.kTSDS14W700
                                .copyWith(color: colorPic),
                          ),
                          const SizedBox(height: 5),
                          Text(
                            controller
                                    .getUserProfileModel
                                    .value
                                    .result
                                    ?.address ??
                                'no_address_provided'.tr,
                            style: TextStyles.kTSDS14W700
                                .copyWith(color: colorPic),
                          ),
                        ],
                      ),
                      Obx(
                        () => InkWell(
                          onTap: () async {
                            if (controller
                                    .farmerFavourite
                                    .value ==
                                true) {
                              await controller.homeController.updateSavedUser(
                                  false,
                                  id: controller.deleteId.value,
                                  errorMessage: "farmer_unsaved_successfully".tr);
                              await controller
                                  .allFarmersSaved(id: controller.id.value);
                              controller.farmerFavourite.value =
                                  false;
                            } else {
                              if (controller.role.value ==
                                  'Landlord') {
                                await controller.homeController
                                    .saveFarmerAsFavourite();
                              } else if (controller
                                      .role
                                      .value ==
                                  'Farmer') {
                                await controller.homeController
                                    .saveLandAsFavouriteFarmer();
                              }
                              await controller
                                  .allFarmersSaved(id: controller.id.value);
                              controller.farmerFavourite.value =true;
                            }
                          },
                          child: Container(
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(11),
                              color: controller
                                      .farmerFavourite
                                      .value
                                  ? colorBlue.withOpacity(0.5)
                                  : colorSave,
                            ),
                            child: Icon(
                              controller.farmerFavourite.value
                                  ? Icons.bookmark
                                  : Icons.bookmark_border_outlined,
                              color: colorBlack.withOpacity(0.5),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(child: const LandsDropdown()),
                      const SizedBox(
                          width:
                              50), // Spacing between Dropdown and Hire button
                              
                          controller.statusForLandlord.value == ""
                              ?   InkWell(
                                  onTap: () async{
                                    if (controller.selectedLand.value == null) {
                                      Get.snackbar(
                                          'msg'.tr, 'please_select_land_first'.tr,
                                          backgroundColor: colorGreen,
                                          colorText: colorBlack);
                                    } else {
                                      controller.availableLandsList(id(
                                          controller.id
                                              .value));
                                      await controller.sentNotificationByLandlordId(landlordId: controller.id.value,farmerId: controller.farmerId.value,landId: controller.selectedLandId.value);
                                      await controller.FetchedLandsDetails(userId: controller.id.value,farmerId: controller.farmerId.value);
                                    }
                                  },
                                  child: 
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 11),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(7),
                                      color: colorPic,
                                    ),
                                    child: Text(
                                      'hire'.tr,
                                      style: TextStyles.kTSNFS16W600
                                          .copyWith(color: colorWhite),
                                    ),
                                  ),
                                ) 
                              : Text(
                                  controller.statusForLandlord.value,
                                  style: TextStyles.kTSFS18W600.copyWith(
                                      color: controller
                                                  .statusForLandlord.value ==
                                              'pending'
                                          ? Color.fromARGB(255, 227, 154, 44)
                                          : primary),
                                )
                    ],
                  ),
                  const SizedBox(height: 20),
                  Text(
                    'description_about_him'.tr,
                    style: TextStyles.kTSDS12W700.copyWith(color: colorDetails),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    controller
                            .getUserProfileModel
                            .value
                            .result
                            ?.description
                            .toString() ??
                        '',
                    style: TextStyles.kTSCF12W500,
                  ),
                  const SizedBox(height: 20),
                  Text(
                    'suggested_farmers'.tr,
                    style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                  ),
                  const SizedBox(height: 10),
                  SizedBox(
                    height: Get.height * 0.08,
                    child: controller
                                    .nearByFarmersModel.value.result?.farmers !=
                                null &&
                            controller.nearByFarmersModel.value.result!.farmers!
                                .where((farmer) =>
                                    farmer.id.toString() !=
                                    controller.farmerId.value)
                                .isNotEmpty
                        ? ListView.separated(
                            itemCount: controller
                                .nearByFarmersModel.value.result!.farmers!
                                .where((farmer) =>
                                    farmer.id.toString() !=
                                    controller.farmerId.value)
                                .length,
                            scrollDirection: Axis.horizontal,
                            separatorBuilder: (context, index) =>
                                const SizedBox(width: 10),
                            itemBuilder: (context, index) {
                              final filteredFarmers = controller
                                  .nearByFarmersModel.value.result!.farmers!
                                  .where((farmer) =>
                                      farmer.id.toString() !=
                                      controller.farmerId.value)
                                  .toList();

                              return InkWell(
                                onTap: () async {
                                  controller.selectedLand.value = null;
                                  controller.statusForLandlord.value ='';
                                  await controller
                                      .getUserByProfileId(id:filteredFarmers[index].id.toString());
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: colorGrey,
                                    borderRadius: BorderRadius.circular(11),
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(11),
                                    child: Image.network(
                                      filteredFarmers[index]
                                          .imageUrl
                                          .toString(),
                                      height: Get.height * 0.09,
                                      width: Get.width * 0.2,
                                      fit: BoxFit.cover,
                                      errorBuilder:
                                          (context, error, stackTrace) {
                                        return Image.asset(
                                          defaultImageUrl,
                                          height: Get.height * 0.09,
                                          width: Get.width * 0.2,
                                          fit: BoxFit.cover,
                                        );
                                      },
                                    ),
                                  ),
                                ),
                              );
                            },
                          )
                        : Center(
                            child: Text(
                              'no_nearby_farmers_found'.tr,
                              style: TextStyles.kTSDS14W500
                                  .copyWith(color: colorGrey),
                            ),
                          ),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
