// To parse this JSON data, do
//
//     final sendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel = sendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModelFromJson(jsonString);

import 'dart:convert';

SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel sendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModelFromJson(String str) => SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel.fromJson(json.decode(str));

String sendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModelToJson(SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel data) => json.encode(data.toJson());

class SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel.fromJson(Map<String, dynamic> json) => SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<ControlDatum>? controlData;
    FarmworkerDetails? farmworkerDetails;

    Result({
        this.controlData,
        this.farmworkerDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        controlData: json["controlData"] == null ? [] : List<ControlDatum>.from(json["controlData"]!.map((x) => ControlDatum.fromJson(x))),
        farmworkerDetails: json["farmworkerDetails"] == null ? null : FarmworkerDetails.fromJson(json["farmworkerDetails"]),
    );

    Map<String, dynamic> toJson() => {
        "controlData": controlData == null ? [] : List<dynamic>.from(controlData!.map((x) => x.toJson())),
        "farmworkerDetails": farmworkerDetails?.toJson(),
    };
}

class ControlDatum {
    int? id;
    dynamic landLordId;
    int? farmerId;
    int? farmWorkerId;
    dynamic landId;
    int? jobId;
    dynamic statusForLandlord;
    dynamic statusForFarmer;
    dynamic statusForFarmworker;
    dynamic createdAt;
    dynamic updatedAt;

    ControlDatum({
        this.id,
        this.landLordId,
        this.farmerId,
        this.farmWorkerId,
        this.landId,
        this.jobId,
        this.statusForLandlord,
        this.statusForFarmer,
        this.statusForFarmworker,
        this.createdAt,
        this.updatedAt,
    });

    factory ControlDatum.fromJson(Map<String, dynamic> json) => ControlDatum(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        farmWorkerId: json["farmWorkerId"],
        landId: json["landId"],
        jobId: json["jobId"],
        statusForLandlord: json["status_for_landlord"],
        statusForFarmer: json["status_for_farmer"],
        statusForFarmworker: json["status_for_farmworker"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "farmWorkerId": farmWorkerId,
        "landId": landId,
        "jobId": jobId,
        "status_for_landlord": statusForLandlord,
        "status_for_farmer": statusForFarmer,
        "status_for_farmworker": statusForFarmworker,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}

class FarmworkerDetails {
    int? id;
    dynamic firstName;
    dynamic imageUrl;
    dynamic mobileNumber;

    FarmworkerDetails({
        this.id,
        this.firstName,
        this.imageUrl,
        this.mobileNumber,
    });

    factory FarmworkerDetails.fromJson(Map<String, dynamic> json) => FarmworkerDetails(
        id: json["id"],
        firstName: json["firstName"],
        imageUrl: json["image_url"],
        mobileNumber: json["mobileNumber"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "image_url": imageUrl,
        "mobileNumber": mobileNumber,
    };
}
