import 'package:get/get.dart';

import '../controllers/saved_utilities_controller.dart';

class SavedUtilitiesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SavedUtilitiesController>(
      () => SavedUtilitiesController(),
    );
  }
}
