import 'package:get/get.dart';

import '../controllers/available_leased_lands_controller.dart';

class AvailableLeasedLandsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailableLeasedLandsController>(
      () => AvailableLeasedLandsController(),
    );
  }
}
