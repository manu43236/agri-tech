import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../core/base/dropdowns/view/district_drop_down.dart';
import '../../../../core/base/dropdowns/view/mandal_drop_down.dart';
import '../../../../core/base/dropdowns/view/state_drop_down.dart';
import '../../../../core/base/dropdowns/view/village_drop_down.dart';
import '../../add_land/views/custom_text_form_field.dart';
import '../../home/views/custom_text.dart';
import '../controllers/add_instrument_controller.dart';

class AddInstrumentView extends GetView<AddInstrumentController> {
  AddInstrumentView({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  void showPicker(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.photo_library),
                title: Text('gallery'.tr),
                onTap: () {
                  controller.pickImage(ImageSource.gallery);
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: const Icon(Icons.photo_camera),
                title: Text('camera'.tr),
                onTap: () {
                  controller.pickImage(ImageSource.camera);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'add_instrument'.tr,
          style: TextStyles.kTSFS24W600
              .copyWith(color: colorDetails, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => SingleChildScrollView(
          padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CustomText(name: 'name_of_the_instrument'.tr),
                const SizedBox(height: 5),
                CustomTextFormField(
                  controller: controller.nameController.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_name_of_the_instrument'.tr;
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(name: 'price_per_day'.tr),
                          const SizedBox(height: 5),
                          CustomTextFormField(
                            controller: controller.priceController.value,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'required_price'.tr;
                              }
                              return null;
                            },
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 5),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(name: 'brand_name'.tr),
                          const SizedBox(height: 5),
                          CustomTextFormField(
                            controller: controller.brandController.value,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'required_brand_name'.tr;
                              }
                              return null;
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                StateDropDown(
                  textEditingController: controller.stateController.value,
                  id: controller.selectedStateIds.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_state'.tr;
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                DistrictDropDown(
                  textEditingController: controller.districtController.value,
                  id: controller.selectedDistrictIds.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_district'.tr;
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                MandalDropDown(
                  textEditingController: controller.mandalController.value,
                  id: controller.selectedMandalIds.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_mandal'.tr;
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                VillageDropDown(
                  textEditingController: controller.villageController.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_village'.tr;
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                CustomText(name: 'description'.tr),
                const SizedBox(height: 5),
                CustomTextFormField(
                  controller: controller.descriptionController.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_description'.tr;
                    }
                    return null;
                  },
                ),
                CustomText(name: 'image'.tr),
                const SizedBox(height: 5),
                GestureDetector(
                  onTap: () => showPicker(context),
                  child: Container(
                    height: 150,
                    decoration: BoxDecoration(
                      border: Border.all(color: colorTextFieldBorder, width: 2),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Obx(
                      () {
                        final landImage = controller.landImage.value;
                        if (landImage == null) {
                          return Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Icon(Icons.image,
                                    size: 50, color: Colors.grey),
                                Text('upload_image_here'.tr,
                                    style: TextStyles.kTSDS12W600),
                              ],
                            ),
                          );
                        } else {
                          return Stack(
                            children: [
                              Positioned.fill(
                                child: Image.file(landImage, fit: BoxFit.cover),
                              ),
                              Positioned(
                                top: 8,
                                right: 8,
                                child: GestureDetector(
                                  onTap: () => controller.removeImage(),
                                  child: const CircleAvatar(
                                    radius: 15,
                                    backgroundColor: Colors.red,
                                    child: Icon(Icons.close,
                                        size: 20, color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          );
                        }
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Center(
                  child: InkWell(
                    onTap: () {
                      controller.isFormSubmitted.value = true;
                      if (_formKey.currentState!.validate()) {
                        controller.addInstrument();
                        if (controller.landPath.value.isEmpty) {
                          Get.snackbar('error'.tr, 'required_to_add_image'.tr,
                              snackPosition: SnackPosition.TOP,
                              backgroundColor: colorRed,
                              colorText: colorWhite);
                        }
                      }
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 12),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(11),
                        color: primary,
                      ),
                      child: Text(
                        'add_instrument'.tr,
                        style:
                            TextStyles.kTSNFS16W600.copyWith(color: colorWhite),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
