import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../farm_worker_home_page/data/get_skills_by_user_id_model.dart';
import '../../farm_worker_home_page/services/filter_jobs_repo_impl.dart';

class AvailableMySkillsController extends BaseController {

  Rx<GetSkillsByUserIdModel> getAvailableSkillsByUserIdModel = Rx(GetSkillsByUserIdModel());

  var apiAvailableSkillsStatus=ApiStatus.LOADING.obs;
  var userId=''.obs;

  @override
  void onInit() async{
    super.onInit();
    userId.value= await SecureStorage().readData(key: "id"??'');
    getAvailableUserByIdSkills(userId: userId.value);
  }

  // get user by id skills API
  Future<void> getAvailableUserByIdSkills({userId}) async {
    apiAvailableSkillsStatus.value = ApiStatus.LOADING;
    try {
      var result = await FilterJobsRepoImpl(dioClient).getUserSkills(userId: userId);
      print("===================== get user skills ==============");
      print(result);
      result.fold((model) {
        getAvailableSkillsByUserIdModel.value = model;
        apiAvailableSkillsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiAvailableSkillsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
