
import 'package:agritech/app/modules/login/services/login_otp_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:agritech/models/otp_model.dart';
import 'package:dartz/dartz.dart';

class LoginOTPRepoImpl extends LoginOTPRepo with NetworkCheckService{

  final DioClient _dioClient;
  LoginOTPRepoImpl(this._dioClient);

  
  @override
  Future<Either<OTPModel,Exception>> doLoginOTP({params1}) async{
    bool data= await checkInternet();
    if(!data){
      return Right(Exception("No network Found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('User/verifyOtpAndLogin',Method.post,params: params1 );
        return result.fold((l){
          OTPModel model=OTPModel.fromJson(l.data);
          return Left(model);
        }, (r)=>Right(Exception(r)));

      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}