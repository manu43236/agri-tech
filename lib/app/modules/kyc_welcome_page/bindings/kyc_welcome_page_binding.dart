import 'package:get/get.dart';

import '../controllers/kyc_welcome_page_controller.dart';

class KycWelcomePageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<KycWelcomePageController>(
      () => KycWelcomePageController(),
    );
  }
}
