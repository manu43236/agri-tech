import 'package:agritech/app/modules/home/services/get_user_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:agritech/models/get_user_model.dart';
import 'package:dartz/dartz.dart';

class GetUserRepoImpl extends GetUserRepo with NetworkCheckService{
  final DioClient _dioClient;
  GetUserRepoImpl(this._dioClient);
  @override
  Future<Either<GetUserModel,Exception>> getUser({id})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('User/getUserById?id=$id', Method.get,);
        return result.fold((l){
          GetUserModel model=GetUserModel.fromJson(l.data);
          return Left(model);
        }, 
        (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}