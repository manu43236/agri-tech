import 'package:agritech/app/modules/saved_utilities/controllers/saved_utilities_controller.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';

class SavedUtilitiesView extends GetView<SavedUtilitiesController> {
  const SavedUtilitiesView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'saved_utilities'.tr,
          style: TextStyles.kTSFS20W500
              .copyWith(color: colorBlack, fontWeight: FontWeight.w400),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: Obx(
          () =>controller.apiSavedUtilityStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): 
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: controller.getAllSavedUtilityModel.value.result !=
                            null &&
                        controller.getAllSavedUtilityModel.value.result !=
                            null &&
                        controller
                            .getAllSavedUtilityModel.value.result!.utilitiesDetails!.isNotEmpty
                    ? ListView.separated(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        itemCount: controller
                            .getAllSavedUtilityModel.value.result!.utilitiesDetails!.length,
                        separatorBuilder: (context, index) =>
                            const SizedBox(height: 10),
                        itemBuilder: (context, index) {
                          final savedUtility = controller
                              .getAllSavedUtilityModel.value.result!.utilitiesDetails![index];
                          return InkWell(
                            onTap: () async {
                               Get.toNamed(Routes.DETAIL_SAVED_UTILITIES,arguments: {"utilIds":savedUtility.id});
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(11),
                                      topRight: Radius.circular(11)),
                                  child:
                                  Image.network(
                                  savedUtility.image,
                                  height: Get.height * 0.2,
                                  width: Get.width,
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, error, stackTrace) {
                                    return Image.asset(
                                      agri,
                                      height: Get.height * 0.2,
                                      width: Get.width,
                                      fit: BoxFit.cover,
                                    );
                                  },
                                ),
                                ),
                                Container(
                                  width: Get.width,
                                  decoration: const BoxDecoration(
                                    color: colorAsh,
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(11),
                                        bottomRight: Radius.circular(11)),
                                    shape: BoxShape.rectangle,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${savedUtility.nameOfInstrument} ${'utility_at'.tr} ${savedUtility.village ?? 'Unknown'}",
                                          style: TextStyles.kTSDS14W500
                                              .copyWith(color: colorHello),
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: [
                                            Text(
                                              'ava'.tr,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyles.kTSCF12W500
                                                  .copyWith(color: colorHello),
                                            ),
                                            Text(
                                              '${savedUtility.status == false ? "leased".tr : "rented_out".tr}',
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyles.kTSCF12W500
                                                  .copyWith(
                                                      color:
                                                          savedUtility.status ==
                                                                  false
                                                              ? colorRed
                                                              : colorGreen),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      )
                    :  Center(child: Text('no_utilities_saved'.tr)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
