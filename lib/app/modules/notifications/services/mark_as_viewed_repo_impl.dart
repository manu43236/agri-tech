
import 'package:agritech/app/modules/notifications/services/mark_as_viewed_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

class MarkAsViewedRepoImpl extends MarkAsViewedRepo with NetworkCheckService{
  final DioClient _dioClient;
  MarkAsViewedRepoImpl(this._dioClient);
  @override
   Future<Either<dynamic,Exception>>  viewedNotifications({params1})async{
      var data = await checkInternet();
      if(!data){
        return Right(Exception("No Network found"));
      }else{
        try{
          var result= await _dioClient.requestForAuth('notification/markAsViewed', Method.get,params: params1);
          return result.fold((l){
           return Left(l.data);  //l['message']
          }, (r)=> Right(Exception(r)));
        }catch(e){
          return Right(Exception(e));
        }
      }
   }
}