// import 'dart:convert';

// GetUserSelectedCropModel getUserSelectedCropModelFromJson(String str) => GetUserSelectedCropModel.fromJson(json.decode(str));

// String getUserSelectedCropModelToJson(GetUserSelectedCropModel data) => json.encode(data.toJson());

// class GetUserSelectedCropModel {
//     int? statusCode;
//     dynamic status;
//     dynamic message;
//     List<Result>? result;

//     GetUserSelectedCropModel({
//         this.statusCode,
//         this.status,
//         this.message,
//         this.result,
//     });

//     factory GetUserSelectedCropModel.fromJson(Map<String, dynamic> json) => GetUserSelectedCropModel(
//         statusCode: json["statusCode"],
//         status: json["status"],
//         message: json["message"],
//         result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
//     );

//     Map<String, dynamic> toJson() => {
//         "statusCode": statusCode,
//         "status": status,
//         "message": message,
//         "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
//     };
// }

// class Result {
//     int? week;
//     List<Crop>? crops;

//     Result({
//         this.week,
//         this.crops,
//     });

//     factory Result.fromJson(Map<String, dynamic> json) => Result(
//         week: json["week"],
//         crops: json["crops"] == null ? [] : List<Crop>.from(json["crops"]!.map((x) => Crop.fromJson(x))),
//     );

//     Map<String, dynamic> toJson() => {
//         "week": week,
//         "crops": crops == null ? [] : List<dynamic>.from(crops!.map((x) => x.toJson())),
//     };
// }

// class Crop {
//     int? id;
//     int? userid;
//     int? landid;
//     int? cropId;
//     int? weeks;
//     bool? weekenable;
//     int? steps;
//     bool? stepsflag;
//     dynamiccreatedDate;
//     dynamic description;
//     dynamic alert;

//     Crop({
//         this.id,
//         this.userid,
//         this.landid,
//         this.cropId,
//         this.weeks,
//         this.weekenable,
//         this.steps,
//         this.stepsflag,
//         this.createdDate,
//         this.description,
//         this.alert,
//     });

//     factory Crop.fromJson(Map<String, dynamic> json) => Crop(
//         id: json["id"],
//         userid: json["userid"],
//         landid: json["landid"],
//         cropId: json["cropId"],
//         weeks: json["weeks"],
//         weekenable: json["weekenable"],
//         steps: json["steps"],
//         stepsflag: json["stepsflag"],
//         createdDate: json["created_date"] == null ? null : DateTime.parse(json["created_date"]),
//         description: json["description"],
//         alert: json["alert"],
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "userid": userid,
//         "landid": landid,
//         "cropId": cropId,
//         "weeks": weeks,
//         "weekenable": weekenable,
//         "steps": steps,
//         "stepsflag": stepsflag,
//         "created_date": "${createdDate!.year.toString().padLeft(4, '0')}-${createdDate!.month.toString().padLeft(2, '0')}-${createdDate!.day.toString().padLeft(2, '0')}",
//         "description": description,
//         "alert": alert,
//     };
// }

// To parse this JSON data, do
//
//     final getUserSelectedCropModel = getUserSelectedCropModelFromJson(jsonString);

import 'dart:convert';

GetUserSelectedCropModel getUserSelectedCropModelFromJson(String str) => GetUserSelectedCropModel.fromJson(json.decode(str));

String getUserSelectedCropModelToJson(GetUserSelectedCropModel data) => json.encode(data.toJson());

class GetUserSelectedCropModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetUserSelectedCropModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetUserSelectedCropModel.fromJson(Map<String, dynamic> json) => GetUserSelectedCropModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<GroupedCrop>? groupedCrops;
    List<MasterCrop>? masterCrops;
    List<AgriYieldDatum>? agriYieldData;

    Result({
        this.groupedCrops,
        this.masterCrops,
        this.agriYieldData,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        groupedCrops: json["groupedCrops"] == null ? [] : List<GroupedCrop>.from(json["groupedCrops"]!.map((x) => GroupedCrop.fromJson(x))),
        masterCrops: json["masterCrops"] == null ? [] : List<MasterCrop>.from(json["masterCrops"]!.map((x) => MasterCrop.fromJson(x))),
        agriYieldData: json["agriYieldData"] == null ? [] : List<AgriYieldDatum>.from(json["agriYieldData"]!.map((x) => AgriYieldDatum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "groupedCrops": groupedCrops == null ? [] : List<dynamic>.from(groupedCrops!.map((x) => x.toJson())),
        "masterCrops": masterCrops == null ? [] : List<dynamic>.from(masterCrops!.map((x) => x.toJson())),
        "agriYieldData": agriYieldData == null ? [] : List<dynamic>.from(agriYieldData!.map((x) => x.toJson())),
    };
}

class AgriYieldDatum {
    int? id;
    int? userid;
    int? landid;
    int? cropId;
    dynamic outedDate;
    dynamic hectors;
    dynamic createdAt;
    dynamic updatedAt;

    AgriYieldDatum({
        this.id,
        this.userid,
        this.landid,
        this.cropId,
        this.outedDate,
        this.hectors,
        this.createdAt,
        this.updatedAt,
    });

    factory AgriYieldDatum.fromJson(Map<String, dynamic> json) => AgriYieldDatum(
        id: json["id"],
        userid: json["userid"],
        landid: json["landid"],
        cropId: json["cropId"],
        outedDate: json["outed_date"] == null ? null : DateTime.parse(json["outed_date"]),
        hectors: json["hectors"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userid": userid,
        "landid": landid,
        "cropId": cropId,
        "outed_date": outedDate?.toIso8601String(),
        "hectors": hectors,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}

class GroupedCrop {
    int? week;
    List<Crop>? crops;

    GroupedCrop({
        this.week,
        this.crops,
    });

    factory GroupedCrop.fromJson(Map<String, dynamic> json) => GroupedCrop(
        week: json["week"],
        crops: json["crops"] == null ? [] : List<Crop>.from(json["crops"]!.map((x) => Crop.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "week": week,
        "crops": crops == null ? [] : List<dynamic>.from(crops!.map((x) => x.toJson())),
    };
}

class Crop {
    int? id;
    int? userid;
    int? landid;
    int? cropId;
    dynamic weeks;
    bool? weekenable;
    dynamic steps;
    bool? stepsflag;
    dynamic createdDate;
    dynamic description;
    dynamic alert;

    Crop({
        this.id,
        this.userid,
        this.landid,
        this.cropId,
        this.weeks,
        this.weekenable,
        this.steps,
        this.stepsflag,
        this.createdDate,
        this.description,
        this.alert,
    });

    factory Crop.fromJson(Map<String, dynamic> json) => Crop(
        id: json["id"],
        userid: json["userid"],
        landid: json["landid"],
        cropId: json["cropId"],
        weeks: json["weeks"],
        weekenable: json["weekenable"],
        steps: json["steps"],
        stepsflag: json["stepsflag"],
        createdDate: json["created_date"] == null ? null : DateTime.parse(json["created_date"]),
        description: json["description"],
        alert: json["alert"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userid": userid,
        "landid": landid,
        "cropId": cropId,
        "weeks": weeks,
        "weekenable": weekenable,
        "steps": steps,
        "stepsflag": stepsflag,
        "created_date": "${createdDate!.year.toString().padLeft(4, '0')}-${createdDate!.month.toString().padLeft(2, '0')}-${createdDate!.day.toString().padLeft(2, '0')}",
        "description": description,
        "alert": alert,
    };
}

class MasterCrop {
    int? id;
    dynamic name;
    dynamic createdAt;
    dynamic updatedAt;
    dynamic totaldays;
    dynamic imageUrl;

    MasterCrop({
        this.id,
        this.name,
        this.createdAt,
        this.updatedAt,
        this.totaldays,
        this.imageUrl,
    });

    factory MasterCrop.fromJson(Map<String, dynamic> json) => MasterCrop(
        id: json["id"],
        name: json["name"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        totaldays: json["totaldays"],
        imageUrl: json["image_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
        "totaldays": totaldays,
        "image_url": imageUrl,
    };
}
