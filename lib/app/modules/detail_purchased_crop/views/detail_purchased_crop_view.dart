import 'package:agritech/core/utils/widget_utils/date_format.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../../../routes/app_pages.dart';
import '../controllers/detail_purchased_crop_controller.dart';

class DetailPurchasedCropView extends GetView<DetailPurchasedCropController> {
  const DetailPurchasedCropView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'detail_crop'.tr,
            style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
          ),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: colorBlack,
            ),
            onPressed: () async {
              Get.back();
            },
          ),
        ),
        body: Obx(
          () => controller.apiDetailsStatus.value == ApiStatus.LOADING
              ? Shimmers().getBannerShimmer()
              : Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(11),
                        child: Image.network(
                          controller.getdetailsOfPurchasedCropModel.value.result
                                  ?.cropImage ??
                              '',
                          height: Get.height * 0.3,
                          width: Get.width,
                          fit: BoxFit.cover,
                          errorBuilder: (context, error, stackTrace) {
                            return Image.asset(
                              defaultImageUrl,
                              height: Get.height * 0.2,
                              width: Get.width,
                              fit: BoxFit.cover,
                            );
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        controller.getdetailsOfPurchasedCropModel.value.result?.cropName ??
                            '',
                        style: TextStyles.kTSFS24W600.copyWith(
                            color: colorbackground,
                            fontWeight: FontWeight.w700),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        '${'location'.tr}${controller.getdetailsOfPurchasedCropModel.value.result?.landDetails?.village ?? ''}',
                        style: TextStyles.kTSDS12W600
                            .copyWith(color: colorbackground),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${'purchased_quantity'.tr}${controller.getdetailsOfPurchasedCropModel.value.result?.yieldDetails?.quantity ?? ''} ${'Tons'.tr}',
                        style: TextStyles.kTSDS12W600
                            .copyWith(color: colorbackground),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${'owner'.tr}${controller.getdetailsOfPurchasedCropModel.value.result?.ownerName ?? ''}',
                        style: TextStyles.kTSDS12W600
                            .copyWith(color: colorbackground),
                      ),
                       const SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${'total_days'.tr}${controller.getdetailsOfPurchasedCropModel.value.result?.totaldays ?? ''}',
                        style: TextStyles.kTSDS12W600
                            .copyWith(color: colorbackground),
                      ),
                       const SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${'outed_date'.tr}${formatDate(controller.getdetailsOfPurchasedCropModel.value.result!.yieldDetails!.outedDate.toString())}',
                        style: TextStyles.kTSDS12W600
                            .copyWith(color: colorbackground),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      controller.getdetailsOfPurchasedCropModel.value.result!.croppaymentstatus!.first.paymentStatus=="Partially Paid"?
                      InkWell(
                        onTap: () async{
                          Get.toNamed(Routes.PURCHASED_CHECKOUT_DETAILS,arguments:{"landId":controller.getdetailsOfPurchasedCropModel.value.result!.landId!,"cropId":controller.getdetailsOfPurchasedCropModel.value.result!.cropId!,"quantity":controller.getdetailsOfPurchasedCropModel.value.result!.croppaymentstatus!.first.quantity} );
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              color: primary),
                          child: Text(
                            'balance_payment'.tr,
                            style: TextStyles.kTSDS12W500
                                .copyWith(color: colorWhite),
                          ),
                        ),
                      ):Text('payment_completed'.tr,style: TextStyles.kTSFS16W600.copyWith(color: primary),),
                    ],
                  ),
                ),
        ));
  }
}
