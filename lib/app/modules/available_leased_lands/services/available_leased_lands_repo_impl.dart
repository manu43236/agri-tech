import 'package:agritech/app/modules/available_leased_lands/services/available_leased_lands_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../../home/controllers/home_controller.dart';
import '../../home/data/get_leased_lands_details_model.dart';
import '../controllers/available_leased_lands_controller.dart';

class AvailableLeasedLandsRepoImpl extends AvailableLeasedLandsRepo with NetworkCheckService{
  final DioClient _dioClient;
  AvailableLeasedLandsRepoImpl(this._dioClient);

  @override
  Future<Either<GetLeasedLandsDetailsModel,Exception>>  leasedSearchLands()async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Leasetaker/getLeaseTakerDetails?type=${Get.find<HomeController>().role.value}&id=${Get.find<HomeController>().userId.value}&search=${Get.find<AvailableLeasedLandsController>().farmerLeasedLandsSearch.value}', Method.get,);
        return result.fold((l) {
          GetLeasedLandsDetailsModel model = GetLeasedLandsDetailsModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}