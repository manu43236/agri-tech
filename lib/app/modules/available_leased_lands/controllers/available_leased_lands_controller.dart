import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../home/data/get_leased_lands_details_model.dart';
import '../../home/services/home_repo_impl.dart';
import '../services/available_leased_lands_repo_impl.dart';

class AvailableLeasedLandsController extends BaseController {
  Rx<GetLeasedLandsDetailsModel> getLeasedSearchLandsDetailsModel =
      Rx(GetLeasedLandsDetailsModel());

  var name = "".obs;
  var farmerLeasedLandsSearch = ''.obs;
  var apiFarLeaseLandsStatus = ApiStatus.LOADING.obs;

  @override
  void onInit() {
    super.onInit();
    leasedSearchLands();
  }

  // Home Page leased lands  API
  Future<void> leasedSearchLands() async {
    apiFarLeaseLandsStatus.value = ApiStatus.LOADING;
    try {
      var result = await AvailableLeasedLandsRepoImpl(dioClient).leasedSearchLands();
      print('============hey=============');
      print(result);
      result.fold((model) {
        if (model.statusCode == 200) {
          getLeasedSearchLandsDetailsModel.value = model;
          print(getLeasedSearchLandsDetailsModel.value.result!.lands!.length);
        } else if (model.statusCode == 400) {
          getLeasedSearchLandsDetailsModel.value.result!.lands!.clear();
          getLeasedSearchLandsDetailsModel.value.result!.lands == [];
        }
        apiFarLeaseLandsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiFarLeaseLandsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
