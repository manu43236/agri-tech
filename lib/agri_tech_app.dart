import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';

import 'app/routes/app_pages.dart';
import 'core/base/base_binding.dart';
import 'core/config/env.dart';
import 'core/conts/color_consts.dart';
import 'localisation/languages.dart';

class AgriTechApp extends StatefulWidget {
  final EnvVars env;

  const AgriTechApp({super.key, required this.env});

  @override
  State<AgriTechApp> createState() => _AppState();
}

class _AppState extends State<AgriTechApp> {

  @override
  void initState() {
    Env().loadVars(widget.env);
    

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: Env().title,
        translations: LocalizationService(),
        locale: LocalizationService.defaultLocale,
        supportedLocales: LocalizationService.supportedLocales,
        fallbackLocale: LocalizationService.defaultLocale,
        localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate, // This handles Material widgets
        GlobalWidgetsLocalizations.delegate, // This handles widget localization
        GlobalCupertinoLocalizations.delegate, // For Cupertino widgets
        // Add your custom delegates here for the 'te_IN' locale if needed
      ],
        //locale: const Locale('en', 'US'),
       //locale: const Locale('tel_IN'),
        theme: ThemeData(
            useMaterial3: false,
            colorScheme: const ColorScheme.light(
                primary: primary, onSecondary: Colors.white),
            fontFamily: 'Inter',
            appBarTheme:
                const AppBarTheme(color: Colors.transparent, elevation: 0.0),
            floatingActionButtonTheme:
                const FloatingActionButtonThemeData(backgroundColor: primary)),
        debugShowCheckedModeBanner: false,
        initialRoute: Routes.SPLASH_SCREEN,
        getPages: AppPages.routes,
        initialBinding: BaseBinding());
  }
}

// import 'package:flutter/material.dart';
// import 'package:flutter_localizations/flutter_localizations.dart';
// import 'package:get/get.dart';

// import 'app/routes/app_pages.dart';
// import 'core/base/base_binding.dart';
// import 'core/config/env.dart';
// import 'core/conts/color_consts.dart';
// import 'localisation/languages.dart';

// class AgriTechApp extends StatefulWidget {
//   final EnvVars env;

//   const AgriTechApp({super.key, required this.env});

//   @override
//   State<AgriTechApp> createState() => _AppState();
// }

// class _AppState extends State<AgriTechApp> {
//   Locale _locale = LocalizationService.defaultLocale;

//   @override
//   void initState() {
//     super.initState();
//     Env().loadVars(widget.env);
//     _loadLocale();
//   }

//   Future<void> _loadLocale() async {
//     final savedLocale = await LocalizationService.getSavedLocale();
//     setState(() {
//       _locale = savedLocale;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return GetMaterialApp(
//       title: Env().title,
//       translations: LocalizationService(),
//       locale: _locale,
//       supportedLocales: LocalizationService.supportedLocales,
//       fallbackLocale: LocalizationService.defaultLocale,
//       localizationsDelegates: const [
//         GlobalMaterialLocalizations.delegate,
//         GlobalWidgetsLocalizations.delegate,
//         GlobalCupertinoLocalizations.delegate,
//       ],
//       theme: ThemeData(
//         useMaterial3: false,
//         colorScheme: const ColorScheme.light(
//           primary: primary,
//           onSecondary: Colors.white,
//         ),
//         fontFamily: 'Inter',
//         appBarTheme: const AppBarTheme(color: Colors.transparent, elevation: 0.0),
//         floatingActionButtonTheme: const FloatingActionButtonThemeData(
//           backgroundColor: primary,
//         ),
//       ),
//       debugShowCheckedModeBanner: false,
//       initialRoute: Routes.SPLASH_SCREEN,
//       getPages: AppPages.routes,
//       initialBinding: BaseBinding(),
//     );
//   }
// }
