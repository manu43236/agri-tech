import 'package:agritech/app/modules/notifications/controllers/notifications_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/details_of_farmer_controller.dart';
import '../views/lands_dropdown.dart';

class DetailsOfFarmerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailsOfFarmerController>(
      () => DetailsOfFarmerController(),
    );
    Get.lazyPut<NotificationsController>(
      () => NotificationsController(),
    );
    //  Get.lazyPut<LandsDropdown>(
    //   () => LandsDropdown(textEditingController: TextEditingController(),),
    // );
  }
}
