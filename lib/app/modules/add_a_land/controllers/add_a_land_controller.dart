import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get_rx/get_rx.dart';

class AddALandController extends BaseController {
  var showAddLand = false.obs;
  var showAddJob = false.obs;
  var selectedIndex = 0.obs;


  // Add controllers for each field
  TextEditingController stateController = TextEditingController();
  TextEditingController districtController = TextEditingController();
  TextEditingController villageController = TextEditingController();
  TextEditingController mandalController = TextEditingController();
  var image = Rxn<String>();  // For holding image data or path

  @override
  void onInit() {
    super.onInit();
    // Initialize or reset the fields here when the controller is created
  }

  void resetFormFields() {
    stateController.clear();
    districtController.clear();
    villageController.clear();
    mandalController.clear();
    image.value = null; // Clear image data
  }

  void toggleAddLand() {
    resetFormFields(); // Reset fields when switching views or showing form
    showAddLand.value = !showAddLand.value;
    showAddJob.value = false;
  }

  void toggleAddJob() {
    showAddJob.value = !showAddJob.value;
    showAddLand.value = false;
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
    // Clean up controllers when not needed anymore
    stateController.dispose();
    districtController.dispose();
    villageController.dispose();
    mandalController.dispose();
  }
}