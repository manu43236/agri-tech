import 'package:agritech/app/modules/vendor/controllers/vendor_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../../core/base/base_controller.dart';
import '../../../../../core/conts/color_consts.dart';
import '../../../../../core/conts/img_const.dart';
import '../../../../../core/themes/text_styles.dart';
import '../../../../../core/utils/widget_utils/shimmers.dart';
import '../../../../routes/app_pages.dart';

class RecentCropsWidgets extends GetView<VendorController> {
  const RecentCropsWidgets({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final PageController _pageController = PageController();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Obx(
        () => Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 10,
              ),
              Text(
                'available_crops'.tr,
                style: TextStyles.kTSFS14W600
                    .copyWith(color: colorHello, fontWeight: FontWeight.w500),
              ),
              const SizedBox(
                height: 10,
              ),
              controller.apiStatus.value == ApiStatus.LOADING
                  ? Shimmers().getShimmerItem()
                  : controller.getrecentCropAvailabilityModel.value.result !=
                              null &&
                          controller.getrecentCropAvailabilityModel.value
                              .result!.isNotEmpty
                      ? Column(
                          children: [
                            SizedBox(
                              height: Get.height / 3,
                              child: PageView.builder(
                                  controller: _pageController,
                                  onPageChanged: (index) {
                                    controller.currentPage.value = index;
                                  },
                                  scrollDirection: Axis.horizontal,
                                  itemCount: controller
                                              .getrecentCropAvailabilityModel
                                              .value
                                              .result !=
                                          null
                                      ? controller
                                          .getrecentCropAvailabilityModel
                                          .value
                                          .result!
                                          .length
                                          .clamp(0, 5)
                                      : 0,
                                  itemBuilder: (context, index) {
                                    final land = controller
                                        .getrecentCropAvailabilityModel
                                        .value
                                        .result![index];
                                    final imageUrl =
                                        land.cropDetails!.imageUrl ?? agri;
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 5),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "${controller.getrecentCropAvailabilityModel.value.result![index].cropDetails!.name} ${'crop_at'.tr} ${controller.getrecentCropAvailabilityModel.value.result![index].landDetails!.village}",
                                            //textAlign: TextAlign.start,
                                            style: TextStyles.kTSDS14W500
                                                .copyWith(color: colorHello),
                                          ),
                                          const SizedBox(
                                            height: 5,
                                          ),
                                          InkWell(
                                            onTap: () async {
                                              Get.toNamed(Routes.DETAIL_CROP,
                                                  arguments: {
                                                    "yieldId": controller
                                                        .getrecentCropAvailabilityModel
                                                        .value
                                                        .result![index]
                                                        .yieldDetails!
                                                        .id
                                                  });
                                            },
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                              child: Image.network(
                                                imageUrl,
                                                height: Get.height * 0.3,
                                                width: Get.width,
                                                fit: BoxFit.cover,
                                                errorBuilder: (context, error,
                                                    stackTrace) {
                                                  return Image.asset(
                                                    agri,
                                                    height: Get.height * 0.3,
                                                    width: Get.width,
                                                    fit: BoxFit.cover,
                                                  );
                                                },
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  }),
                            ),
                            Obx(() => Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: List.generate(
                                    controller.getrecentCropAvailabilityModel
                                                .value.result !=
                                            null
                                        ? controller
                                            .getrecentCropAvailabilityModel
                                            .value
                                            .result!
                                            .length
                                            .clamp(0, 5)
                                        : 0,
                                    (index) => buildDot(
                                        index, controller.currentPage.value),
                                  ),
                                )),
                            Align(
                              alignment: Alignment.topRight,
                              child: InkWell(
                                onTap: () {
                                  Get.toNamed(Routes.AVAILABLE_CROPS);
                                },
                                child: Text(
                                  'view_all'.tr,
                                  style: TextStyles.kTSCF12W500.copyWith(
                                      fontSize: 10,
                                      decoration: TextDecoration.underline),
                                ),
                              ),
                            ),
                          ],
                        )
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const SizedBox(height: 60),
                            Center(
                              child: Text(
                                'no_crops_found'.tr,
                                style: TextStyles.kTSFS18W500
                                    .copyWith(color: colorHello),
                              ),
                            ),
                            const SizedBox(height: 60),
                          ],
                        ),
            ]),
      ),
    );
  }

  Widget buildDot(int index, int currentIndex) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 2),
      width: currentIndex == index ? 10 : 8,
      height: currentIndex == index ? 12 : 8,
      decoration: BoxDecoration(
        color: currentIndex == index ? Colors.blue : Colors.grey,
        shape: BoxShape.circle,
      ),
    );
  }
}
