// To parse this JSON data, do
//
//     final mandalModel = mandalModelFromJson(jsonString);

import 'dart:convert';

MandalModel mandalModelFromJson(String str) => MandalModel.fromJson(json.decode(str));

String mandalModelToJson(MandalModel data) => json.encode(data.toJson());

class MandalModel {
    int? statusCode;
    String? status;
    String? message;
    List<MandalResult>? result;

    MandalModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory MandalModel.fromJson(Map<String, dynamic> json) => MandalModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<MandalResult>.from(json["result"]!.map((x) => MandalResult.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class MandalResult {
    int? id;
    String? name;

    MandalResult({
        this.id,
        this.name,
    });

    factory MandalResult.fromJson(Map<String, dynamic> json) => MandalResult(
        id: json["id"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
    };
}
