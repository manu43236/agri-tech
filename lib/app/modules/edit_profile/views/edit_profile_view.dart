import 'package:agritech/app/modules/edit_profile/controllers/edit_profile_controller.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/conts/img_const.dart';
import 'package:agritech/core/utils/widget_utils/buttons/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import '../../../../core/themes/text_styles.dart';
import '../../home/controllers/home_controller.dart';

class EditProfileView extends GetView<EditProfileController> {
  const EditProfileView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

    void showPicker(BuildContext context) {
      showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  leading: const Icon(Icons.photo_library),
                  title: Text('gallery'.tr),
                  onTap: () {
                    controller.pickImage(ImageSource.gallery);
                    Navigator.of(context).pop();
                  },
                ),
                ListTile(
                  leading: const Icon(Icons.photo_camera),
                  title: Text('camera'.tr),
                  onTap: () {
                    controller.pickImage(ImageSource.camera);
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 150, // Adjust the height to fit your content
        flexibleSpace: Stack(
          children: [
            SizedBox(
              width: Get.width,
              child: Image.asset(
                profileLogo,
                fit: BoxFit.cover,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 80),
              child: Align(
                alignment: Alignment.center,
                child: Obx(() {
                  final String profilePath = controller.profilePath.value;
                  final String profileImage =
                      Get.find<HomeController>().profileImage.value;

                  ImageProvider? imageProvider;

                  if (profilePath.isNotEmpty &&
                      File(profilePath).existsSync()) {
                    // Valid local file
                    imageProvider = FileImage(File(profilePath));
                  } else if (profileImage.isNotEmpty &&
                      Uri.tryParse(profileImage)?.hasAbsolutePath == true) {
                    // Valid network image
                    imageProvider = NetworkImage(profileImage);
                  } else {
                    // Fallback to default asset image
                    imageProvider = const AssetImage(farmer);
                  }

                  return CircleAvatar(
                    backgroundColor: inActiveDotColor,
                    radius: 60,
                    backgroundImage: imageProvider,
                    child: Stack(
                      children: [
                        Positioned(
                          bottom: 1,
                          right: 10,
                          child: CircleAvatar(
                            radius: 15,
                            backgroundColor: primary,
                            child: IconButton(
                              onPressed: () {
                                showPicker(context);
                              },
                              icon: const Icon(
                                Icons.edit_outlined,
                                color: colorWhite,
                                size: 15,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }),
              ),
            ),
          ],
        ),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: Get.height * 0.1,
            ),
            Expanded(
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(31),
                      topRight: Radius.circular(31)),
                  color: colorGhostWhite,
                  boxShadow: [
                    BoxShadow(
                      color: colorBlack.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: const Offset(0, -3),
                    ),
                  ],
                ),
                child: SingleChildScrollView(
                  child: Obx(()=>
                     Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(height: 20),
                        Align(
                                  alignment: Alignment.topRight,
                                  child: Obx(() => GestureDetector(
                      onTap: controller.toggleEditing,
                      child: Text(
                        controller.isEditing.value ? "" : "edit".tr,
                        style: const TextStyle(
                          color: primary,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                    )),
                                ),
                        Text('name'.tr,
                            style: TextStyles.kTSFS16W600
                                .copyWith(color: colorBlack)),
                        TextFormField(
                          controller: controller.nameController.value,
                           enabled: controller.isEditing.value,
                          decoration: InputDecoration(
                            prefixIconConstraints:
                                const BoxConstraints(minWidth: 48, maxHeight: 50),
                            prefixIcon: SizedBox(
                              width: 1,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Image.asset(user),
                                  const SizedBox(
                                      height: 20,
                                      child: VerticalDivider(
                                        color: colorBlack,
                                        thickness: 1,
                                        width: 18,
                                      )),
                                ],
                              ),
                            ),
                            hintText: 'enter_your_name'.tr,
                            hintStyle: TextStyles.kTSFS14WNORM
                                .copyWith(color: Colors.grey),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'required_name'.tr;
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text('age'.tr,
                            style: TextStyles.kTSFS16W600
                                .copyWith(color: colorBlack)),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          controller: controller.ageController.value,
                          enabled: controller.isEditing.value,
                          decoration: InputDecoration(
                            prefixIconConstraints:
                                const BoxConstraints(minWidth: 48, maxHeight: 50),
                            prefixIcon: SizedBox(
                              width: 1,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Image.asset(age),
                                  const SizedBox(
                                      height: 20,
                                      child: VerticalDivider(
                                        color: colorBlack,
                                        thickness: 1,
                                        width: 18,
                                      )),
                                ],
                              ),
                            ),
                            hintText: 'age'.tr,
                            hintStyle: TextStyles.kTSFS14WNORM
                                .copyWith(color: Colors.grey),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'required_age'.tr;
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text('mobile_number'.tr,
                            style: TextStyles.kTSFS16W600
                                .copyWith(color: colorBlack)),
                        TextFormField(
                          readOnly: true,
                          inputFormatters: [LengthLimitingTextInputFormatter(10)],
                          keyboardType: TextInputType.phone,
                          controller: controller.mobileController.value,
                          enabled: controller.isEditing.value,
                          decoration: InputDecoration(
                            prefixIconConstraints:
                                const BoxConstraints(minWidth: 48, maxHeight: 50),
                            prefixIcon: SizedBox(
                              width: 1,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Image.asset(phones),
                                  const SizedBox(
                                      height: 20,
                                      child: VerticalDivider(
                                        color: colorBlack,
                                        thickness: 1,
                                        width: 18,
                                      )),
                                ],
                              ),
                            ),
                            hintText: 'enter_your_mobile_number'.tr,
                            hintStyle: TextStyles.kTSFS14WNORM
                                .copyWith(color: Colors.grey),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'required_mobile_number'.tr;
                            } else if (value.length < 10) {
                              return 'enter_valid_mobile_number'.tr;
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text('address'.tr,
                            style: TextStyles.kTSFS16W600
                                .copyWith(color: colorBlack)),
                        TextFormField(
                          controller: controller.addressController.value,
                          enabled: controller.isEditing.value,
                          decoration: InputDecoration(
                            prefixIconConstraints:
                                const BoxConstraints(minWidth: 48, maxHeight: 50),
                            prefixIcon: SizedBox(
                              width: 1,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Image.asset(locationPin),
                                  const SizedBox(
                                      height: 20,
                                      child: VerticalDivider(
                                        color: colorBlack,
                                        thickness: 1,
                                        width: 18,
                                      )),
                                ],
                              ),
                            ),
                            hintText: 'enter_location'.tr,
                            hintStyle: TextStyles.kTSFS14WNORM
                                .copyWith(color: Colors.grey),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'required_address'.tr;
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text('select_role'.tr,
                            style: TextStyles.kTSFS16W600
                                .copyWith(color: colorBlack)),
                        TextFormField(
                          readOnly: true,
                          controller: controller.roleController.value,
                          enabled: controller.isEditing.value,
                          decoration: InputDecoration(
                            prefixIconConstraints:
                                const BoxConstraints(minWidth: 48, maxHeight: 50),
                            prefixIcon: SizedBox(
                              width: 1,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Image.asset(businessman),
                                  const SizedBox(
                                      height: 20,
                                      child: VerticalDivider(
                                        color: colorBlack,
                                        thickness: 1,
                                        width: 18,
                                      )),
                                ],
                              ),
                            ),
                            hintText: 'enter_role'.tr,
                            hintStyle: TextStyles.kTSFS14WNORM
                                .copyWith(color: Colors.grey),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'required_role'.tr;
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text('description'.tr,
                            style: TextStyles.kTSFS16W600
                                .copyWith(color: colorBlack)),
                        TextFormField(
                          controller: controller.descriptionController.value,
                          enabled: controller.isEditing.value,
                          maxLines: null,
                          decoration: InputDecoration(
                            prefixIconConstraints:
                                const BoxConstraints(minWidth: 48, maxHeight: 50),
                            prefixIcon: SizedBox(
                              width: 1,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.description_outlined,
                                    color: colorGryeW600,
                                  ),
                                  const SizedBox(
                                      height: 20,
                                      child: VerticalDivider(
                                        color: colorBlack,
                                        thickness: 1,
                                        width: 18,
                                      )),
                                ],
                              ),
                            ),
                            hintText: 'enter_your_details'.tr,
                            hintStyle: TextStyles.kTSFS14WNORM
                                .copyWith(color: Colors.grey),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'required_description'.tr;
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 40,
                        ),
                        CustomButton(
                          action: () {
                            if (_formKey.currentState!.validate()) {
                              //   if (controller.profileFile.value.isEmpty) {
                              //   Get.snackbar(
                              //     'Error',
                              //     '* required to add profile image',
                              //     snackPosition: SnackPosition.TOP,
                              //     backgroundColor: colorRed,
                              //     colorText: colorWhite,
                              //   );
                              // } else {
                              //   controller.updateEditProfile();
                              // }
                              controller.updateEditProfile();
                            }
                          },
                          name: 'save'.tr,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
