import 'package:agritech/core/conts/img_const.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../controllers/vendor_payment_successful_controller.dart';

class VendorPaymentSuccessfulView
    extends GetView<VendorPaymentSuccessfulController> {
  const VendorPaymentSuccessfulView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Image.asset(
                vendorPaymentSucess,
                height: Get.height*0.25,
              ),
            ),
            Text('payment_successful'.tr,style: TextStyles.kTSFS24W600.copyWith(color: primary,fontWeight: FontWeight.w700),)
          ],
        ),
      ),
    );
  }
}
