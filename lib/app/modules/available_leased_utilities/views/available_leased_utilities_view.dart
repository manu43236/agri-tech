import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/text_fields/custom_search_widget.dart';
import '../../../routes/app_pages.dart';
import '../controllers/available_leased_utilities_controller.dart';

class AvailableLeasedUtilitiesView
    extends GetView<AvailableLeasedUtilitiesController> {
  const AvailableLeasedUtilitiesView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'available_leased_utilities'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomSearchField(
                  onChanged: (val) {
                    controller.leasedUtilitiesSearch.value = val;
                    if (val.length >= 3) {
                      controller.leasedUtility();
                    } else if (val.length == 0) {
                      controller.leasedUtility();
                    }
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                controller.apiLeaseUtilitiesStatus.value == ApiStatus.LOADING
                    ? const SizedBox()
                    : controller.getLeasedUtilitiesDetailsModel.value.result!
                            .utilities!.isEmpty
                        ?  Align(
                            alignment: Alignment.center,
                            child: Text(
                              "no_utilities_found".tr,
                              style:
                                  TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                          )
                        : Column(
                            children: _leaseUtilities(),
                          )
                // controller.name.value.isEmpty
                //     ? const SizedBox()
                //     : Text(controller.name.value ?? ""),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _leaseUtilities() {
    List<Widget> items = [];
    for (var index = 0;
        index <
            controller
                .getLeasedUtilitiesDetailsModel.value.result!.utilities!.length;
        index++) {
      items.add(InkWell(
        onTap: () async {
          controller.utilitiId.value = controller.getLeasedUtilitiesDetailsModel
              .value.result!.utilities![index].id!;
          Get.toNamed(Routes.DETAIL_LEASED_UTILITIES,arguments: {"leaseUtilId":controller.utilitiId.value});
             // id: controller.utilitiId.value);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //Image.asset(land,),
            ClipRRect(
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(11), topRight: Radius.circular(11)),
              child: Image.network(
                controller.getLeasedUtilitiesDetailsModel.value.result!
                        .utilities?[index].image ??
                    '',
                height: Get.height * 0.2,
                width: Get.width,
                fit: BoxFit.cover,
                errorBuilder: (context, error, stackTrace) {
                  return Image.asset(
                    defaultImageUrl,
                    height: Get.height * 0.2,
                    width: Get.width,
                    fit: BoxFit.cover,
                  );
                },
              ),
            ),
            Container(
              width: Get.width,
              decoration: const BoxDecoration(
                  color: colorAsh,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(11),
                      bottomRight: Radius.circular(11)),
                  shape: BoxShape.rectangle),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${controller.getLeasedUtilitiesDetailsModel.value.result!.utilities![index].name} ${'utility_at'.tr} ${controller.getLeasedUtilitiesDetailsModel.value.result!.utilities![index].location}",
                      style: TextStyles.kTSDS14W500.copyWith(color: colorHello),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ));
    }
    return items;
  }
}
