import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:get/get.dart';
import '../../available_farmworker_jobs/controllers/available_farmworker_jobs_controller.dart';
import '../../saved_jobs/controllers/saved_jobs_controller.dart';
import '../controllers/farm_worker_controller.dart';

class FarmWorkerHomePageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FarmWorkerHomePageController>(
      () => FarmWorkerHomePageController(),
    );
    Get.lazyPut<SavedJobsController>(
      ()=>SavedJobsController()
    );
    Get.lazyPut<AvailableFarmworkerJobsController>(
      () => AvailableFarmworkerJobsController(),
    );
  }
}
