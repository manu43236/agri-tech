import 'package:agritech/core/conts/img_const.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/base/base_controller.dart';
import '../../../../../core/conts/color_consts.dart';
import '../../../../../core/themes/text_styles.dart';
import '../../../../../core/utils/widget_utils/shimmers.dart';
import '../../../../routes/app_pages.dart';
import '../../controllers/farm_worker_controller.dart';

class MySkillsWidget extends GetView<FarmWorkerHomePageController> {
  const MySkillsWidget({super.key});

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Obx(
        () => Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'my_skills'.tr,
              style: TextStyles.kTSDS14W500.copyWith(color: colorHello),
            ),
            const SizedBox(
              height: 5,
            ),
            controller.apiStatus.value == ApiStatus.LOADING
                ? Shimmers().getShimmerItem()
                : controller.getSkillsByUserIdModel.value.result != null &&
                        controller
                            .getSkillsByUserIdModel.value.result!.isNotEmpty
                    ? Column(
                        children: [
                          SizedBox(
                            height: Get.height * 0.13,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: controller.getSkillsByUserIdModel
                                    .value.result!.length,
                                itemBuilder: (context, index) {
                                  var land = controller
                                      .getSkillsByUserIdModel.value.result!;
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: InkWell(
                                      onTap: () {
                                        Get.toNamed(Routes.DETAIL_MY_SKILLS,arguments: {"id":land[index].id,"names":land[index].skill});
                                      },
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Flexible(
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.only(topLeft: Radius.circular(12),topRight: Radius.circular(12)),
                                              child: Image.network(
                                                defaultImageUrl ?? '',
                                                width: Get.width * 0.42,
                                                height: Get.height*0.15,
                                                fit: BoxFit.cover,
                                                errorBuilder:
                                                    (context, error, stackTrace) {
                                                  // Display a local asset if the network image fails
                                                  return Image.asset(
                                                    defaultImageUrl,
                                                    width: Get.width * 0.42,
                                                    height: Get.height*0.15,
                                                    fit: BoxFit.cover,
                                                  );
                                                },
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width: Get.width*0.42,
                                            height: Get.height*0.02,
                                            padding: EdgeInsets.symmetric(horizontal: 7),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(12),bottomRight: Radius.circular(12)),
                                              color: colorGrey.shade300
                                            ),
                                            child: Text(
                                            land[index].skill.toString(),
                                            style: TextStyles.kTSCF12W500,
                                          ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }),
                          ),
                          const SizedBox(height: 5),
                          Align(
                            alignment: Alignment.topRight,
                            child: InkWell(
                              onTap: () async {
                                Get.toNamed(Routes.AVAILABLE_MY_SKILLS);
                              },
                              child: Text(
                                'view_all'.tr,
                                style: TextStyles.kTSCF12W500.copyWith(
                                  fontSize: 10,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: Get.height * 0.03,
                          ),
                          Center(
                            child: Text(
                              'no_leased_jobs'.tr,
                              style: TextStyles.kTSFS18W500.copyWith(
                                color: colorGrey,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Get.height * 0.03,
                          ),
                        ],
                      ),
          ],
        ),
      ),
    );
  }
}
