import 'package:get/get.dart';

import '../controllers/success_payment_checkout_page_controller.dart';

class SuccessPaymentCheckoutPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SuccessPaymentCheckoutPageController>(
      () => SuccessPaymentCheckoutPageController(),
    );
  }
}
