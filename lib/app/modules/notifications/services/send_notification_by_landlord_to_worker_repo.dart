import 'package:agritech/models/send_notification_by_landlord_to_worker_model.dart';
import 'package:dartz/dartz.dart';

abstract class SendNotificationByLandlordToWorkerRepo{
  Future<Either<SendNotificationByLandlordToWorkerModel,Exception>> landlordToWorkerNotification({params1});
}