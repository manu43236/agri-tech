import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/suitable_crops/services/suitable_crop_repo_impl.dart';
import 'package:get/get.dart';
import '../../../../app/modules/suitable_crops/data/create_selected_crop_week_model.dart';
import '../../../../app/modules/suitable_crops/data/get_crop_by_id_model.dart';
import '../../../../app/modules/suitable_crops/data/get_crops_by_mandal_model.dart';
import '../../../../app/modules/suitable_crops/data/get_user_selected_crop_model.dart';
import '../../base_controller.dart';

class CropsDropdownController extends BaseController {

  var selectedValue = ''.obs;
  Rx<GetCropsByMandalModel> getCropsByMandalModel = Rx(GetCropsByMandalModel());
  Rx<GetCropByIdModel> getCropByIdModel =
      Rx(GetCropByIdModel());
  Rx<CreateSelectedCropWeekModel> createSelectedCropWeekModel =
      Rx(CreateSelectedCropWeekModel());
  Rx<GetUserSelectedCropModel> getUserSelectedCropModel =
      Rx(GetUserSelectedCropModel());
  var cropId = ''.obs;
  var landId = ''.obs;
  var userId=''.obs;

  @override
  void onInit() {
    getAllCrops();
    landId.value=Get.find<HomeController>().landId.value;
    userId.value=Get.find<HomeController>().userId.value;
    super.onInit();
  }

  Future<void> getAllCrops() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SuitableCropRepoImpl(dioClient).getCrops();
      result.fold((model) {
        getCropsByMandalModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      apiStatus.value = ApiStatus.FAIL;
    }
  }

  //get crop by id 
  Future<void> getCropById(String value) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await SuitableCropRepoImpl(dioClient).getCropById(params1:  value);
      print("======================== get crop by id ====================");
      print(result);
      result.fold((model) {
        getCropByIdModel.value = model;
        cropId.value=getCropByIdModel.value.result!.id.toString();
        print('cropId${cropId.value}');
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //create selected crop week 
  Future<void> CreateSelectedCropWeek() async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await SuitableCropRepoImpl(dioClient).createCropWeek(params1:{
            "userid":userId.value ,
            "landid": landId.value,
            "cropId": cropId.value,
          });
      print("======================== create selected crop week ====================");
      print('userId:${userId.value}');
      print("landid:${landId.value}");
      print('cropId:${cropId.value}');
      print(result);
      result.fold((model) {
        createSelectedCropWeekModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //get user selected crop
  Future<void> getUserSelectedCrop({landId,cropId,lang}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await SuitableCropRepoImpl(dioClient).getUserCrop(landId: landId,cropId: cropId,lang: lang);
      print("======================== get user selected crop ====================");
      print(result);
      result.fold((model) {
        getUserSelectedCropModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }
}
