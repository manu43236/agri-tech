import 'package:dartz/dartz.dart';

import '../../utility/data/get_utility_by_user_id_model.dart';

abstract class AvailableMyUtilitiesRepo{
  Future<Either<GetUtilityByUserIdModel,Exception>>  availableSearchUtility();
}