import 'package:agritech/app/modules/otp/views/custom_otp.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/conts/img_const.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:agritech/core/utils/widget_utils/buttons/custom_button.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/otp_controller.dart';

class OtpView extends GetView<OtpController> {
  OtpView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () => Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Stack(
              children: [
                SizedBox(width: Get.width, child: Image.asset(appBarLogo,fit: BoxFit.cover,)),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 60),
                  child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        'otp'.tr,
                        style: TextStyles.kTSFS26W400
                            .copyWith(color: Colors.white),
                      )),
                ),
              ],
            ),
            Expanded(child: Image.asset(farmer)),
            Expanded(
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: colorBlack.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: const Offset(0, -3),
                    ),
                  ],
                  color: colorGhostWhite,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(27),
                      topRight: Radius.circular(27)),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'otp_verification'.tr,
                        style: TextStyles.kTSFS20W700
                            .copyWith(color: const Color(0xff1E232C)),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "enter_the_verification_code_we_just_sent_on_your_mobile".tr,
                        style: TextStyles.kTSCF12W500
                            .copyWith(color: const Color(0xff838BA1)),
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      CustomOTP(
                        controller: controller.OtpControllers.value,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      CustomButton(
                        name: 'authenticate'.tr,
                        action: () {
                          controller.otpVerification();
                        },
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      InkWell(
                        onTap: () {
                          //Get.find<LoginController>().doLoginGetOtp();
                          controller.resendOtp();
                        },
                        child: Center(
                            child: Text(
                          'resend_otp'.tr,
                          style: TextStyles.kTSFS15W600
                              .copyWith(color: const Color(0xff4D7881)),
                        )),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
