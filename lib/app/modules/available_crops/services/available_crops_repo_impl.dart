import 'package:agritech/app/modules/available_crops/services/available_crops_repo.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../data/filter_with_crop_and_quantity_model.dart';
import '../data/get_all_crops_model.dart';

class AvailableCropsRepoImpl extends AvailableCropsRepo with NetworkCheckService{
  final DioClient _dioClient;
  AvailableCropsRepoImpl(this._dioClient);

  @override
  Future<Either<GetAllCropsModel,Exception>> getCrops()async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Crop/getCrops', Method.get);
        return result.fold((l){
          GetAllCropsModel model= GetAllCropsModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<FilterWithCropAndQuantityModel,Exception>> filterWithCropQuantity({crop,minQuan,maxQuan})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Vender/filterWithCropNameOrQuantityOrBoth?cropName=$crop&latitude=${Get.find<HomeController>().latitude.value}&longitude=${Get.find<HomeController>().longitude.value}&minQuantity=$minQuan&maxQuantity=$maxQuan', Method.get);
        return result.fold((l){
          FilterWithCropAndQuantityModel model= FilterWithCropAndQuantityModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}