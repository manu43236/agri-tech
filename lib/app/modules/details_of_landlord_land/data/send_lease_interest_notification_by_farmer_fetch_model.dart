// To parse this JSON data, do
//
//     final sendLeaseInterestNotificationByFarmerFetchModel = sendLeaseInterestNotificationByFarmerFetchModelFromJson(jsonString);

import 'dart:convert';

SendLeaseInterestNotificationByFarmerFetchModel sendLeaseInterestNotificationByFarmerFetchModelFromJson(String str) => SendLeaseInterestNotificationByFarmerFetchModel.fromJson(json.decode(str));

String sendLeaseInterestNotificationByFarmerFetchModelToJson(SendLeaseInterestNotificationByFarmerFetchModel data) => json.encode(data.toJson());

class SendLeaseInterestNotificationByFarmerFetchModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<Result>? result;

    SendLeaseInterestNotificationByFarmerFetchModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SendLeaseInterestNotificationByFarmerFetchModel.fromJson(Map<String, dynamic> json) => SendLeaseInterestNotificationByFarmerFetchModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    dynamic statusForLandlord;
    LandDetails? landDetails;
    LandlordDetails? landlordDetails;
    dynamic message;

    Result({
        this.statusForLandlord,
        this.landDetails,
        this.landlordDetails,
        this.message,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        statusForLandlord: json["status_for_landlord"],
        landDetails: json["landDetails"] == null ? null : LandDetails.fromJson(json["landDetails"]),
        landlordDetails: json["landlordDetails"] == null ? null : LandlordDetails.fromJson(json["landlordDetails"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status_for_landlord": statusForLandlord,
        "landDetails": landDetails?.toJson(),
        "landlordDetails": landlordDetails?.toJson(),
        "message": message,
    };
}

class LandDetails {
    int? id;
    dynamic surveyNum;
    dynamic village;
    dynamic state;
    dynamic district;
    dynamic mandal;
    dynamic imageUrl;

    LandDetails({
        this.id,
        this.surveyNum,
        this.village,
        this.state,
        this.district,
        this.mandal,
        this.imageUrl,
    });

    factory LandDetails.fromJson(Map<String, dynamic> json) => LandDetails(
        id: json["id"],
        surveyNum: json["surveyNum"],
        village: json["village"],
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        imageUrl: json["image_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "surveyNum": surveyNum,
        "village": village,
        "state": state,
        "district": district,
        "mandal": mandal,
        "image_url": imageUrl,
    };
}

class LandlordDetails {
    int? id;
    dynamic name;
    dynamic address;
    dynamic phone;
    dynamic imageUrl;

    LandlordDetails({
        this.id,
        this.name,
        this.address,
        this.phone,
        this.imageUrl,
    });

    factory LandlordDetails.fromJson(Map<String, dynamic> json) => LandlordDetails(
        id: json["id"],
        name: json["name"],
        address: json["address"],
        phone: json["phone"],
        imageUrl: json["image_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "address": address,
        "phone": phone,
        "image_url": imageUrl,
    };
}
