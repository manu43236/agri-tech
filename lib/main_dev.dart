import 'package:agritech/core/utils/widget_utils/location/location.dart';
import 'package:flutter/material.dart';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'agri_tech_app.dart';
import 'core/config/envs/dev_env.dart';
import 'core/utils/storage_utils/secure_storage.dart';

void main()async {
  WidgetsFlutterBinding.ensureInitialized();
  SecureStorage().loadSecureStorage(const FlutterSecureStorage());
  // print('===========location==========');
  // Location location = Location();
  // await location.determinePosition();
  Get.put(Location());
  runApp(
    AgriTechApp(env: DevEnv()),
  );
}
