
import 'package:agritech/app/modules/add_job/services/add_job_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:agritech/models/add_job_model.dart';
import 'package:dartz/dartz.dart';

class AddJobRepoImpl extends AddJobRepo with NetworkCheckService{
  final DioClient _dioClient;
  AddJobRepoImpl(this._dioClient);
  @override
  Future<Either<AddJobModel,Exception>> doAddJob({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Jobs/addJobbylandlord', Method.post,params: params1);
       return result.fold((l) {
          AddJobModel model = AddJobModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}