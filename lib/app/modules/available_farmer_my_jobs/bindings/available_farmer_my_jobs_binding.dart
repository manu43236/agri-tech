import 'package:agritech/app/modules/select_region/controllers/select_region_controller.dart';
import 'package:get/get.dart';

import '../controllers/available_farmer_my_jobs_controller.dart';

class AvailableFarmerMyJobsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailableFarmerMyJobsController>(
      () => AvailableFarmerMyJobsController(),
    );
    Get.lazyPut<SelectRegionController>(
      () => SelectRegionController(),fenix: true
    );
  }
}
