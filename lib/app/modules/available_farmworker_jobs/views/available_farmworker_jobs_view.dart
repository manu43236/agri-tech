import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/text_fields/custom_search_widget.dart';
import '../../../routes/app_pages.dart';
import '../../home/controllers/home_controller.dart';
import '../../saved_jobs/controllers/saved_jobs_controller.dart';
import '../../select_region/views/select_region_view.dart';
import '../controllers/available_farmworker_jobs_controller.dart';

class AvailableFarmworkerJobsView
    extends GetView<AvailableFarmworkerJobsController> {
  const AvailableFarmworkerJobsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'available_farmworker_jobs'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello,fontSize: 18),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
         actions: [
            InkWell(
              onTap: () {
                 Get.bottomSheet(
                isScrollControlled: true,
                isDismissible: true,
                SelectRegionView(type: 'worker_jobs'.tr,)
              );
              },
              child: Image.asset(
                cone,
                height: 20,
                width: 20,
              ),
            ),
            const SizedBox(
              width: 20,
            )
          ]
      ),
      body: Obx(
        () => Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 10,
                ),
                CustomSearchField(
                  onChanged: (val) {
                    controller.workerJobSearch.value = val;
                    print("Dataaaa");
                    print(controller.filterJobsSearchModel.value.result);
                    if (val.length >= 3) {
                      controller.filterJobsSearch(state: '',district:'' ,mandal:'' ,village:'' ,search:controller.workerJobSearch.value );
                    } else if (val.length == 0) {
                      controller.filterJobsSearch(state: '',district:'' ,mandal:'' ,village:'' ,search:controller.workerJobSearch.value );
                    }
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                controller.dataSelectedItem.value.isEmpty
                    ? const SizedBox()
                    : Text('Your selected region is ${controller.dataSelectedItem.value}' ?? "", style: TextStyles.kTSDS14W500
                                          .copyWith(color: colorHello),),
                const SizedBox(
                  height: 10,
                ),
                controller.apiWorkerJobsStatus.value == ApiStatus.LOADING
                    ? const SizedBox()
                    : controller.filterJobsSearchModel.value.result!.isEmpty
                        ?  Align(
                            alignment: Alignment.center,
                            child: Text(
                              "no_jobs_found".tr,
                              style:
                                  const TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                          )
                        : Column(
                            children: _workJobs(),
                          )
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _workJobs() {
    List<Widget> items = [];
    for (var index = 0;
        index < controller.filterJobsSearchModel.value.result!.length;
        index++) {
      items.add(InkWell(
        onTap: () async {
          // Get.find<HomeController>().jobId.value =
          //                         controller.filterJobsSearchModel.value.result![index].job!.id.toString();
          //                     await Get.find<SavedJobsController>()
          //                         .getAllSavedJobs();
          //                     await Get.find<HomeController>().getIdJobs();
             Get.toNamed(Routes.DETAIL_FARMWORKER_JOBS,arguments: {"jobId":controller.filterJobsSearchModel.value.result![index].job!.id!});
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(11), topRight: Radius.circular(11)),
              child: Image.network(
                controller.filterJobsSearchModel.value.result![index].job!
                        .image ??
                    '',
                height: Get.height * 0.2,
                width: Get.width,
                fit: BoxFit.cover,
                errorBuilder: (context, error, stackTrace) {
                  return Image.asset(
                    defaultImageUrl,
                    height: Get.height * 0.2,
                    width: Get.width,
                    fit: BoxFit.cover,
                  );
                },
              ),
            ),
            Container(
              width: Get.width,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(11),
                      bottomRight: Radius.circular(11)),
                  color: colorAsh,
                  shape: BoxShape.rectangle),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${controller.filterJobsSearchModel.value.result![index].job!.fullName}",
                      style: TextStyles.kTSDS14W500.copyWith(color: colorHello),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                        '${'location'.tr}${controller.filterJobsSearchModel.value.result![index].job!.village}'),
                    const SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ));
    }
    return items;
  }
}
