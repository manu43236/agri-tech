// To parse this JSON data, do
//
//     final filterWithCropAndQuantityModel = filterWithCropAndQuantityModelFromJson(jsonString);

import 'dart:convert';

FilterWithCropAndQuantityModel filterWithCropAndQuantityModelFromJson(String str) => FilterWithCropAndQuantityModel.fromJson(json.decode(str));

String filterWithCropAndQuantityModelToJson(FilterWithCropAndQuantityModel data) => json.encode(data.toJson());

class FilterWithCropAndQuantityModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<Result>? result;

    FilterWithCropAndQuantityModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory FilterWithCropAndQuantityModel.fromJson(Map<String, dynamic> json) => FilterWithCropAndQuantityModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    LandDetails? landDetails;
    CropDetails? cropDetails;
    YieldDetails? yieldDetails;
    dynamic distance;

    Result({
        this.landDetails,
        this.cropDetails,
        this.yieldDetails,
        this.distance,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        landDetails: json["landDetails"] == null ? null : LandDetails.fromJson(json["landDetails"]),
        cropDetails: json["cropDetails"] == null ? null : CropDetails.fromJson(json["cropDetails"]),
        yieldDetails: json["yieldDetails"] == null ? null : YieldDetails.fromJson(json["yieldDetails"]),
        distance: json["distance"]?.toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "landDetails": landDetails?.toJson(),
        "cropDetails": cropDetails?.toJson(),
        "yieldDetails": yieldDetails?.toJson(),
        "distance": distance,
    };
}

class CropDetails {
    int? id;
    dynamic name;
    dynamic imageUrl;
    dynamic costPerHector;

    CropDetails({
        this.id,
        this.name,
        this.imageUrl,
        this.costPerHector,
    });

    factory CropDetails.fromJson(Map<String, dynamic> json) => CropDetails(
        id: json["id"],
        name: json["name"],
        imageUrl: json["image_url"],
        costPerHector: json["costPerHector"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image_url": imageUrl,
        "costPerHector": costPerHector,
    };
}

class LandDetails {
    int? id;
    dynamic latitude;
    dynamic longitude;
    dynamic village;
    dynamic mandal;
    dynamic district;
    dynamic state;
    dynamic description;
    dynamic landInAcres;
    dynamic water;

    LandDetails({
        this.id,
        this.latitude,
        this.longitude,
        this.village,
        this.mandal,
        this.district,
        this.state,
        this.description,
        this.landInAcres,
        this.water,
    });

    factory LandDetails.fromJson(Map<String, dynamic> json) => LandDetails(
        id: json["id"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        village: json["village"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        description: json["description"],
        landInAcres: json["landInAcres"],
        water: json["water"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "latitude": latitude,
        "longitude": longitude,
        "village": village,
        "mandal": mandal,
        "district": district,
        "state": state,
        "description": description,
        "landInAcres": landInAcres,
        "water": water,
    };
}

class YieldDetails {
    int? id;
    dynamic outedDate;
    dynamic quantity;
    bool? status;

    YieldDetails({
        this.id,
        this.outedDate,
        this.quantity,
        this.status,
    });

    factory YieldDetails.fromJson(Map<String, dynamic> json) => YieldDetails(
        id: json["id"],
        outedDate: json["outedDate"] == null ? null : DateTime.parse(json["outedDate"]),
        quantity: json["quantity"],
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "outedDate": outedDate?.toIso8601String(),
        "quantity": quantity,
        "status": status,
    };
}
