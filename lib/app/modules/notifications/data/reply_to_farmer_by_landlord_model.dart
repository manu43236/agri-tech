// To parse this JSON data, do
//
//     final replyToFarmerByLandlordModel = replyToFarmerByLandlordModelFromJson(jsonString);

import 'dart:convert';

ReplyToFarmerByLandlordModel replyToFarmerByLandlordModelFromJson(String str) => ReplyToFarmerByLandlordModel.fromJson(json.decode(str));

String replyToFarmerByLandlordModelToJson(ReplyToFarmerByLandlordModel data) => json.encode(data.toJson());

class ReplyToFarmerByLandlordModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    ReplyToFarmerByLandlordModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory ReplyToFarmerByLandlordModel.fromJson(Map<String, dynamic> json) => ReplyToFarmerByLandlordModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    Notification? notification;
    Lease? lease;

    Result({
        this.notification,
        this.lease,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        notification: json["notification"] == null ? null : Notification.fromJson(json["notification"]),
        lease: json["lease"] == null ? null : Lease.fromJson(json["lease"]),
    );

    Map<String, dynamic> toJson() => {
        "notification": notification?.toJson(),
        "lease": lease?.toJson(),
    };
}

class Lease {
    int? id;
    int? landId;
    int? landlord;
    int? farmer;
    dynamic status;
    DateTime? startingDate;
    dynamic endingDate;
    DateTime? updatedAt;
    DateTime? createdAt;
    dynamic farmWorker;
    dynamic utiliser;
    dynamic utilityId;
    dynamic jobId;

    Lease({
        this.id,
        this.landId,
        this.landlord,
        this.farmer,
        this.status,
        this.startingDate,
        this.endingDate,
        this.updatedAt,
        this.createdAt,
        this.farmWorker,
        this.utiliser,
        this.utilityId,
        this.jobId,
    });

    factory Lease.fromJson(Map<String, dynamic> json) => Lease(
        id: json["id"],
        landId: json["landId"],
        landlord: json["landlord"],
        farmer: json["farmer"],
        status: json["status"],
        startingDate: json["starting_date"] == null ? null : DateTime.parse(json["starting_date"]),
        endingDate: json["ending_date"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        farmWorker: json["farmWorker"],
        utiliser: json["utiliser"],
        utilityId: json["utilityId"],
        jobId: json["jobId"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landId": landId,
        "landlord": landlord,
        "farmer": farmer,
        "status": status,
        "starting_date": startingDate?.toIso8601String(),
        "ending_date": endingDate,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "farmWorker": farmWorker,
        "utiliser": utiliser,
        "utilityId": utilityId,
        "jobId": jobId,
    };
}

class Notification {
    bool? isRead;
    int? id;
    dynamic name;
    dynamic message;
    dynamic from;
    dynamic to;
    int? landId;
    dynamic statusForLandlord;
    dynamic imageUrl;
    bool? flag;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic statusForFarmer;
    dynamic jobId;
    dynamic statusForFarmworker;

    Notification({
        this.isRead,
        this.id,
        this.name,
        this.message,
        this.from,
        this.to,
        this.landId,
        this.statusForLandlord,
        this.imageUrl,
        this.flag,
        this.updatedAt,
        this.createdAt,
        this.statusForFarmer,
        this.jobId,
        this.statusForFarmworker,
    });

    factory Notification.fromJson(Map<String, dynamic> json) => Notification(
        isRead: json["is_read"],
        id: json["id"],
        name: json["name"],
        message: json["message"],
        from: json["from"],
        to: json["to"],
        landId: json["landId"],
        statusForLandlord: json["status_for_landlord"],
        imageUrl: json["image_url"],
        flag: json["flag"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        statusForFarmer: json["status_for_farmer"],
        jobId: json["jobId"],
        statusForFarmworker: json["status_for_farmworker"],
    );

    Map<String, dynamic> toJson() => {
        "is_read": isRead,
        "id": id,
        "name": name,
        "message": message,
        "from": from,
        "to": to,
        "landId": landId,
        "status_for_landlord": statusForLandlord,
        "image_url": imageUrl,
        "flag": flag,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "status_for_farmer": statusForFarmer,
        "jobId": jobId,
        "status_for_farmworker": statusForFarmworker,
    };
}
