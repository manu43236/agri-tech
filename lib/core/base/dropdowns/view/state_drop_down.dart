// import 'package:agritech/core/base/base_controller.dart';
// import 'package:agritech/core/base/dropdowns/controller/state_dropdown_controller.dart';
// import 'package:agritech/core/utils/widget_utils/selector_text_field.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// import '../../../themes/text_styles.dart';
// import '../../../utils/widget_utils/search_dialog.dart';
// import '../../../utils/widget_utils/search_view.dart';
// import '../controller/district_dropdown_controller.dart';

// // ignore: must_be_immutable
// class StateDropDown extends SearchViews<StateDropdownController> {
//   TextEditingController? textEditingController;
//   int? id;
//   StateDropDown({super.key,this.textEditingController,this.id});

//   @override
//   Widget cBuild(BuildContext context,StateDropdownController cController) {
//     return Obx(

//       ()=> cController.apiStatus == ApiStatus.LOADING ?
//       const SizedBox():
//       InkWell(
//         onTap: () async {
//           var item = await Get.dialog(SearchSelectionDialogView(
//                 items: cController.makes,
//                 searchHint: 'Select State'.tr,
//                 title: 'Select State'.tr,
//               ));
//               textEditingController!.text = item;
//               print("dialog open ===========");
//               print(item);
//               print(cController.stateResult.value.where((val)=>val.name == item).toList());
//               var result = cController.stateResult.value.where((val)=>val.name == item).toList();
//               print(result.first.id);
//               id =result.first.id; 
//               Get.find<DistrictDropdownController>().getAllDistricts(id: id);
//         },
//         child: SelectorCustomTextField(
//               leftPadding: 5,
//               rightPadding: 0,
//               cMargin: const EdgeInsets.symmetric(horizontal: 4),
//               lableStyle: TextStyles.kTSDS12W500,
//               hintStyle: TextStyles.kTSDS14W500,
//               lable: 'State'.tr,
//               controller: textEditingController!,
//               hintText: 'Select State'.tr,
//               showTextFieldLable: true,
//               showSufixIcon:const SizedBox(),
//               isEnabled: false,
//             ),
//       ),
//     );
//   }
// }

import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/base/dropdowns/controller/state_dropdown_controller.dart';
import 'package:agritech/core/utils/widget_utils/selector_text_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../themes/text_styles.dart';
import '../../../utils/widget_utils/search_dialog.dart';
import '../../../utils/widget_utils/search_view.dart';
import '../controller/district_dropdown_controller.dart';

class StateDropDown extends SearchViews<StateDropdownController> {
  TextEditingController? textEditingController;
  int? id;
  final String? Function(String?)? validator;

  StateDropDown({
    super.key,
    this.textEditingController,
    this.id,
    this.validator,
  });

  @override
  Widget cBuild(BuildContext context, StateDropdownController cController) {
    return Obx(
      () => cController.apiStatus == ApiStatus.LOADING
          ? const SizedBox()
          : FormField<String>(
              validator: validator,
              builder: (FormFieldState<String> field) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () async {
                        var item = await Get.dialog(SearchSelectionDialogView(
                          items: cController.makes,
                          searchHint: 'select_state'.tr,
                          title: 'select_state'.tr,
                        ));

                        if (item != null) {
                          textEditingController!.text = item;
                          var result = cController.stateResult.value
                              .where((val) => val.name == item)
                              .toList();
                          id = result.isNotEmpty ? result.first.id : null;
                        //   print("object");
                        //  print(Get.find<DistrictDropdownController>().districtController.value.text);
                        //   print("object");

                          // Get.find<DistrictDropdownController>().districtController.value.text=='';
                          // Get.find<MandalDropdownController>().mandalController.value.text=='';
                          // Get.find<VillageDropdownController>().villageController.value.text=='';

                          // Trigger district fetching
                          Get.find<DistrictDropdownController>()
                              .getAllDistricts(id: id,lang: cController.selectedLang.value);
                          field.didChange(item); // Notify the field of the change
                        }
                      },
                      child: SelectorCustomTextField(
                        leftPadding: 5,
                        rightPadding: 0,
                        cMargin: const EdgeInsets.symmetric(horizontal:0),
                        lableStyle: TextStyles.kTSCF12W500,
                        hintStyle: TextStyles.kTSDS14W500,
                        lable: 'state'.tr,
                        controller: textEditingController!,
                        hintText: 'select_state'.tr,
                        showTextFieldLable: true,
                        showSufixIcon: const SizedBox(),
                        isEnabled: false,
                      ),
                    ),
                    if (field.hasError)
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Text(
                          field.errorText!,
                          style: TextStyle(color: Colors.red, fontSize: 12),
                        ),
                      ),
                  ],
                );
              },
            ),
    );
  }
  
}
