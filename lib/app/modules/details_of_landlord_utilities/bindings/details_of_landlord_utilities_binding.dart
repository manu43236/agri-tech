import 'package:get/get.dart';

import '../../instrument_details/controllers/instrument_details_controller.dart';
import '../../saved_utilities/controllers/saved_utilities_controller.dart';
import '../../utility/controllers/utility_controller.dart';
import '../controllers/details_of_landlord_utilities_controller.dart';

class DetailsOfLandlordUtilitiesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailsOfLandlordUtilitiesController>(
      () => DetailsOfLandlordUtilitiesController(),
    );
    Get.lazyPut<UtilityController>(
      () => UtilityController(),
    );
    Get.lazyPut<SavedUtilitiesController>(
      () => SavedUtilitiesController(),
    );
    Get.lazyPut<InstrumentDetailsController>(
      () => InstrumentDetailsController(),
    );
  }
}
