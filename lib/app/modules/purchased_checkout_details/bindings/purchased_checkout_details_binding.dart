import 'package:get/get.dart';

import '../../vendor_checkout_details/controllers/vendor_checkout_details_controller.dart';
import '../controllers/purchased_checkout_details_controller.dart';

class PurchasedCheckoutDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PurchasedCheckoutDetailsController>(
      () => PurchasedCheckoutDetailsController(),
    );
    Get.lazyPut<VendorCheckoutDetailsController>(
      () => VendorCheckoutDetailsController(),
    );
  }
}
