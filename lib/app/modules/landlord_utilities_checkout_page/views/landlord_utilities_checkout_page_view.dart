import 'package:agritech/app/modules/details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/landlord_utilities_checkout_page_controller.dart';

class LandlordUtilitiesCheckoutPageView
    extends GetView<LandlordUtilitiesCheckoutPageController> {
  const LandlordUtilitiesCheckoutPageView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'checkout'.tr,
          style: TextStyles.kTSFS24W600
              .copyWith(color: colorSuitable, fontWeight: FontWeight.w800),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () =>controller.apiUtilityPaymentStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): 
         Padding(
          padding: const EdgeInsets.only(left: 25, right: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Image.network(
                  controller
                          .getUtilityPaymentByIdModel
                          .value
                          .result
                          ?.utilityDetails
                          ?.image
                          .toString() ??
                      '',
                  height: Get.height * 0.18,
                  width: Get.width,
                  fit: BoxFit.cover,
                  errorBuilder: (context, error, stackTrace) {
                    // Display a local asset if the network image fails
                    return Image.asset(
                      defaultImageUrl,
                      height: Get.height * 0.18,
                      width: Get.width,
                      fit: BoxFit.cover,
                    );
                  },
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          controller
                                  .getUtilityPaymentByIdModel
                                  .value
                                  .result
                                  ?.utilityDetails
                                  ?.name ??
                              '',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyles.kTSFS20W700
                              .copyWith(color: colorSuitable),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                '${controller.getUtilityPaymentByIdModel.value.result?.utilityDetails?.address?.mandal ?? ''} , ${controller.getUtilityPaymentByIdModel.value.result?.utilityDetails?.address?.village ?? ''}',
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                style: TextStyles.kTSDS14W500
                                    .copyWith(color: colorSuitable),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                 // Get.find<DetailsOfLandlordUtilitiesController>().getUtilityPaymentByIdModel.value.result!.paymentStatus!='Partially Paid'?
                  Flexible(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () async {
                          if (controller
                                  .getUtilityPaymentByIdModel
                                  .value
                                  .result!
                                  .quantity >
                              1) {
                            controller.updateQuantityNumber.value =
                                controller
                                        .getUtilityPaymentByIdModel
                                        .value
                                        .result!
                                        .quantity -
                                    1;
                            await controller.updateUtilityPayment();
                            await controller
                                .getUtilityPayment(createdUtillityId: controller.createdUtilityId.value);
                            controller.amount.value =
                                controller
                                        .getUtilityPaymentByIdModel
                                        .value
                                        .result!
                                        .totalPayment *
                                    100;
                          }
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 1),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: colorQuantity,
                          ),
                          child: const Icon(
                            Icons.remove,
                            size: 20,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(
                        controller
                                .getUtilityPaymentByIdModel
                                .value
                                .result
                                ?.quantity
                                .toString() ??
                            '',
                        style: TextStyles.kTSFS20W700
                            .copyWith(color: colorSuitable, fontSize: 20),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      InkWell(
                        onTap: () async {
                          controller.updateQuantityNumber.value =
                              controller
                                      .getUtilityPaymentByIdModel
                                      .value
                                      .result!
                                      .quantity +
                                  1;
                          await controller.updateUtilityPayment();
                          await controller
                              .getUtilityPayment(createdUtillityId: controller.createdUtilityId.value);
                          controller.amount.value =
                              controller
                                      .getUtilityPaymentByIdModel
                                      .value
                                      .result!
                                      .totalPayment *
                                  100;
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 5, vertical: 1),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: colorQuantity,
                          ),
                          child: const Icon(
                            Icons.add,
                            size: 20,
                          ),
                        ),
                      ),
                    ],
                  ))
                  // :Padding(
                  //   padding: const EdgeInsets.only(right: 10),
                  //   child: Text(
                  //         Get.find<DetailsOfLandlordUtilitiesController>()
                  //                 .getUtilityPaymentByIdModel
                  //                 .value
                  //                 .result
                  //                 ?.quantity
                  //                 .toString() ??
                  //             '',
                  //         style: TextStyles.kTSFS20W700
                  //             .copyWith(color: colorSuitable, fontSize: 20),
                  //       ),
                  // ),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              //Get.find<DetailsOfLandlordUtilitiesController>().getUtilityPaymentByIdModel.value.result!.paymentStatus!='Partially Paid'?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('total_cost_of_selected_item'.tr,
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorSuitable)),
                      Text(
                        '${'rs'.tr} ${controller.getUtilityPaymentByIdModel.value.result?.costOfSelectedItem ?? "-"}',
                        style: TextStyles.kTSDS14W500
                            .copyWith(color: colorSuitable),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('advance_payment'.tr,
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorSuitable)),
                      Text(
                          '${'rs'.tr} ${controller.getUtilityPaymentByIdModel.value.result?.tenPercentPrice ?? "-"}',
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorSuitable))
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('balance_payment'.tr,
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorSuitable)),
                      Text(
                          '${'rs'.tr} ${controller.getUtilityPaymentByIdModel.value.result?.ninetyPercentPrice}',
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorSuitable))
                    ],
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    'to_made_later_after_receiving_the_prod'.tr,
                    style:
                        TextStyles.kTSFS10W500.copyWith(color: colorSuitable),
                  ),
                  const SizedBox(height: 20),
                  const Divider(
                    thickness: 1,
                    color: colorBlack,
                  ),
                  const SizedBox(height: 20),
                  // Price and Total
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('amount_to_be_paid'.tr,
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorSuitable)),
                      Text(
                        '${'rs'.tr} ${controller.getUtilityPaymentByIdModel.value.result?.tenPercentPrice ?? "-"}',
                        style: TextStyles.kTSDS14W500
                            .copyWith(color: colorSuitable),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('gst'.tr,
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorSuitable)),
                      Text(
                          '${'rs'.tr} ${controller.getUtilityPaymentByIdModel.value.result?.tenPercentGst ?? "-"}',
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorSuitable))
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('total'.tr,
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorSuitable)),
                      Text(
                          '${'rs'.tr} ${controller.getUtilityPaymentByIdModel.value.result?.tenPercentPayment}',
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorSuitable))
                    ],
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  Center(
                    child: InkWell(
                      onTap: () async {
                        await controller.utilityPayment(
                            id: controller
                                .getUtilityPaymentByIdModel
                                .value
                                .result!
                                .id);
                        controller.openCheckout(controller.amount.value);
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(13),
                            color: primary),
                        child: Text(
                          'make_a_payment'.tr,
                          style: TextStyles.kTSFS16W700
                              .copyWith(color: colorWhite),
                        ),
                      ),
                    ),
                  ),
                ],
              )
              // :Center(
              //   child: Column(
              //     children: [
              //       const SizedBox(height: 50,),
              //       Image.asset(
              //         payment,
              //         height: Get.height*0.2,
              //         width: Get.width*0.5,
              //       ),
              //       const SizedBox(height: 10,),
              //       Text('Advance payment is successful',style: TextStyles.kTSFS14W600.copyWith(color: colorHello),)
              //     ],
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
