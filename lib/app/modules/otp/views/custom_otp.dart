import 'package:agritech/app/modules/otp/controllers/otp_controller.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';

class CustomOTP extends StatefulWidget {
   final TextEditingController controller;
   CustomOTP({super.key,required this.controller});
  
  @override
  State<CustomOTP> createState() => _CustomOTPState();
}

class _CustomOTPState extends State<CustomOTP> {
  @override
  Widget build(BuildContext context) {
    final defaultPinTheme = PinTheme(
      width: 56,
      height: 56,
      textStyle: const TextStyle(
          fontSize: 20,
          color: Color.fromRGBO(30, 60, 87, 1),
          fontWeight: FontWeight.w600),
      decoration: BoxDecoration(
        border: Border.all(color: Color.fromARGB(255, 209, 214, 219)),
        borderRadius: BorderRadius.circular(20),
      ),
    );
    final onSubmit = defaultPinTheme.copyDecorationWith(
      border: Border.all(color: const Color(0xff35C2C1)),
      borderRadius: BorderRadius.circular(8),
    );

    final onFocus = defaultPinTheme.copyWith(
      decoration: defaultPinTheme.decoration!.copyWith(
         border: Border.all(color: Color.fromARGB(255, 221, 232, 149)),
        borderRadius: BorderRadius.circular(13),
      ),
    );

    final onError=defaultPinTheme.copyBorderWith(
                border: Border.all(color: Colors.redAccent),
              );
    return  Pinput(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      length: 4,
      controller: widget.controller,
      defaultPinTheme: defaultPinTheme,
      focusedPinTheme:onFocus ,
      submittedPinTheme: onSubmit,
      errorPinTheme: onError,
      validator: (pin) {
        if (pin == null || pin.length < 4) {
          return 'pin is incorrect';
        }
        return null;
      },
    );
  }
}
