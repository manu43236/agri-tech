import 'package:agritech/app/modules/notifications/controllers/notifications_controller.dart';
import 'package:get/get.dart';

import '../../details_of_farmer/controllers/details_of_farmer_controller.dart';
import '../controllers/detail_saved_farmers_controller.dart';

class DetailSavedFarmersBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailSavedFarmersController>(
      () => DetailSavedFarmersController(),
    );
     Get.lazyPut<DetailsOfFarmerController>(
      () => DetailsOfFarmerController(),
    );
    Get.lazyPut<NotificationsController>(
      () => NotificationsController(),
    );
  }
}
