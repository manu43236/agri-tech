// To parse this JSON data, do
//
//     final acceptByFarmworkerModel = acceptByFarmworkerModelFromJson(jsonString);

import 'dart:convert';

AcceptByFarmworkerModel acceptByFarmworkerModelFromJson(String str) => AcceptByFarmworkerModel.fromJson(json.decode(str));

String acceptByFarmworkerModelToJson(AcceptByFarmworkerModel data) => json.encode(data.toJson());

class AcceptByFarmworkerModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    AcceptByFarmworkerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory AcceptByFarmworkerModel.fromJson(Map<String, dynamic> json) => AcceptByFarmworkerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    NewNotification? newNotification;
    String? senderName;

    Result({
        this.newNotification,
        this.senderName,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        newNotification: json["newNotification"] == null ? null : NewNotification.fromJson(json["newNotification"]),
        senderName: json["senderName"],
    );

    Map<String, dynamic> toJson() => {
        "newNotification": newNotification?.toJson(),
        "senderName": senderName,
    };
}

class NewNotification {
    bool? isRead;
    int? id;
    String? name;
    String? message;
    int? from;
    int? to;
    int? jobId;
    String? statusForLandlord;
    dynamic statusForFarmer;
    String? imageUrl;
    DateTime? updatedAt;
    DateTime? createdAt;
    dynamic landId;
    dynamic statusForFarmworker;

    NewNotification({
        this.isRead,
        this.id,
        this.name,
        this.message,
        this.from,
        this.to,
        this.jobId,
        this.statusForLandlord,
        this.statusForFarmer,
        this.imageUrl,
        this.updatedAt,
        this.createdAt,
        this.landId,
        this.statusForFarmworker,
    });

    factory NewNotification.fromJson(Map<String, dynamic> json) => NewNotification(
        isRead: json["is_read"],
        id: json["id"],
        name: json["name"],
        message: json["message"],
        from: json["from"],
        to: json["to"],
        jobId: json["jobId"],
        statusForLandlord: json["status_for_landlord"],
        statusForFarmer: json["status_for_farmer"],
        imageUrl: json["image_url"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        landId: json["landId"],
        statusForFarmworker: json["status_for_farmworker"],
    );

    Map<String, dynamic> toJson() => {
        "is_read": isRead,
        "id": id,
        "name": name,
        "message": message,
        "from": from,
        "to": to,
        "jobId": jobId,
        "status_for_landlord": statusForLandlord,
        "status_for_farmer": statusForFarmer,
        "image_url": imageUrl,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "landId": landId,
        "status_for_farmworker": statusForFarmworker,
    };
}
