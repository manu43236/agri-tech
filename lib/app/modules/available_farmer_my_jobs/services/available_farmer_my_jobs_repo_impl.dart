import 'package:agritech/app/modules/available_farmer_my_jobs/controllers/available_farmer_my_jobs_controller.dart';
import 'package:agritech/app/modules/available_farmer_my_jobs/services/available_farmer_my_jobs_repo.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../../home/data/get_all_jobs_by_farmer_model.dart';

class AvailableFarmerMyJobsRepoImpl extends AvailableFarmerMyJobsRepo with NetworkCheckService{
  final DioClient _dioClient;
  AvailableFarmerMyJobsRepoImpl(this._dioClient);

   @override
  Future<Either<GetAllJobsByFarmerModel,Exception>> getFarmerSearchJobs()async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Jobs/getAllJobsByFarmerId?farmerId=${Get.find<HomeController>().userId.value}&search=${Get.find<AvailableFarmerMyJobsController>().farmerMyJobsSearch.value}', Method.get);
       return result.fold((l) {
          GetAllJobsByFarmerModel model = GetAllJobsByFarmerModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}