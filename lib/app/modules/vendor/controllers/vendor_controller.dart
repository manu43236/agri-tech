import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/vendor/services/vendor_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../data/get_filtered_crop_availability_model.dart';
import '../data/get_vendor_crops_model.dart';

class VendorController extends BaseController {

  Rx<GetFilteredCropAvailabilityModel> getrecentCropAvailabilityModel =
      Rx(GetFilteredCropAvailabilityModel());

  Rx<GetVendorCropsModel> getVendorCropsModel =
      Rx(GetVendorCropsModel());

  RxInt currentPage = 0.obs;
  var apiPurchasedStatus=ApiStatus.LOADING.obs;
  var id=''.obs;

  @override
  void onInit() async{
    id.value = await SecureStorage().readData(key: "id") ?? "";
    super.onInit();
    recentCrops(userId: id.value,latitude: Get.find<HomeController>().latitude.value,longitude: Get.find<HomeController>().longitude.value);
    purchasedCrops();
  }

  //recent crops API
  Future<void> recentCrops({userId,latitude,longitude}) async {
    apiStatus.value = ApiStatus.LOADING;
    currentPage.value = 0;
    try {
      var result = await VendorRepoImpl(dioClient).getFilteredCrops(userId: userId,latitude: latitude,longitude: longitude);
      print('=============== get filtered crops =========');
      print(result);
      result.fold((model) {
        getrecentCropAvailabilityModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //purchased crops API
  Future<void> purchasedCrops() async {
    apiPurchasedStatus.value = ApiStatus.LOADING;
    currentPage.value = 0;
    try {
      var result = await VendorRepoImpl(dioClient).purchasedCrops();
      print('=============== get purchased crops =========');
      print(result);
      result.fold((model) {
        getVendorCropsModel.value = model;
        apiPurchasedStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiPurchasedStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
