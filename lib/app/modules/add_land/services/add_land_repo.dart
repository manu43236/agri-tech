import 'package:dartz/dartz.dart';
import '../../../../models/add_land_model.dart';

abstract class AddLandRepo{
  Future<Either<AddLandModel,Exception>> doAddLand({params1});
}