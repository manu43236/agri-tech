import 'package:agritech/app/modules/home/services/mandal_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/app/modules/select_region/data/mandal_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/storage_utils/secure_storage.dart';


class MandalDropdownController extends BaseController {


 // var model = StatelistModel().obs;
  var selectMake = 'Select Mandal'.obs;
  var mandal= <String>[].obs;
  Rx<List<MandalResult>> mandalResult = Rx([]);
  var items = <String>[].obs;
  var selectedLang=''.obs;

  TextEditingController mandalController = TextEditingController();
  @override
  void onInit() async{
    selectedLang.value = await SecureStorage().readData(key: "lang");
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  getAllMandals({id,lang}) async {
    apiStatus.value = ApiStatus.LOADING;
    print(id);
    mandal.clear();
    items.clear();
    if(id != 0){
    var result = await MandalRepoImpl(dioClient).getMandal(id: id,lang: lang);
    print("result");
    print(result);
    result.fold((l) {
      if (l.result!.isNotEmpty) {
        mandalResult.value = l.result!;
        for (var element in l.result!) {
          items.add(element.name!);
        }
        mandal.addAll(items);
        
        print(mandal);
      }
      apiStatus.value = ApiStatus.SUCCESS;
    
    }, (r) {
     
      apiStatus.value = ApiStatus.FAIL;
    });
  }
  }
}