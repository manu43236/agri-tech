import 'package:get/get.dart';

import '../../select_region/controllers/select_region_controller.dart';
import '../controllers/available_farmworker_jobs_controller.dart';

class AvailableFarmworkerJobsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailableFarmworkerJobsController>(
      () => AvailableFarmworkerJobsController(),
    );
     Get.lazyPut<SelectRegionController>(
      () => SelectRegionController(),fenix: true
    );
  }
}
