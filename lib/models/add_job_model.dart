class AddJobModel {
    AddJobModel({
        required this.statusCode,
        required this.status,
        required this.message,
        required this.result,
    });

    final int? statusCode;
    final String? status;
    final String? message;
    final Result? result;

    factory AddJobModel.fromJson(Map<String, dynamic> json){ 
        return AddJobModel(
            statusCode: json["statusCode"],
            status: json["status"],
            message: json["message"],
            result: json["result"] == null ? null : Result.fromJson(json["result"]),
        );
    }

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };

}

class Result {
    Result({
        required this.id,
        required this.landLordId,
        required this.fullName,
        required this.description,
        required this.startDate,
        required this.endDate,
        required this.state,
        required this.district,
        required this.village,
        required this.mandal,
        required this.status,
        required this.updatedAt,
        required this.createdAt,
        required this.farmerId,
        required this.imageId,
        required this.image,
    });

    final int? id;
    final int? landLordId;
    final String? fullName;
    final String? description;
    final DateTime? startDate;
    final DateTime? endDate;
    final String? state;
    final String? district;
    final String? village;
    final String? mandal;
    final String? status;
    final DateTime? updatedAt;
    final DateTime? createdAt;
    final dynamic farmerId;
    final int? imageId;
    final String? image;

    factory Result.fromJson(Map<String, dynamic> json){ 
        return Result(
            id: json["id"],
            landLordId: json["landLordId"],
            fullName: json["fullName"],
            description: json["description"],
            startDate: DateTime.tryParse(json["startDate"] ?? ""),
            endDate: DateTime.tryParse(json["endDate"] ?? ""),
            state: json["state"],
            district: json["district"],
            village: json["village"],
            mandal: json["mandal"],
            status: json["status"],
            updatedAt: DateTime.tryParse(json["updatedAt"] ?? ""),
            createdAt: DateTime.tryParse(json["createdAt"] ?? ""),
            farmerId: json["farmerId"],
            imageId: json["image_id"],
            image: json["image"],
        );
    }

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "fullName": fullName,
        "description": description,
        "startDate": startDate?.toIso8601String(),
        "endDate": endDate?.toIso8601String(),
        "state": state,
        "district": district,
        "village": village,
        "mandal": mandal,
        "status": status,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "farmerId": farmerId,
        "image_id": imageId,
        "image": image,
    };

}
