


import 'package:agritech/app/modules/Landlord_utilities/controllers/landlord_utilities_controller.dart';
import 'package:agritech/app/modules/Landlord_utilities/services/landlord_utility_repo.dart';
import 'package:agritech/app/modules/details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import 'package:agritech/app/modules/utility/controllers/utility_controller.dart';

import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:agritech/models/create_utility_model.dart';
import 'package:agritech/models/create_utility_payment_model.dart';
import 'package:agritech/models/get_recent_four_utilities_model.dart';
import 'package:agritech/models/get_utility_by_id_model.dart';
import 'package:agritech/models/get_utility_by_location_model.dart';
import 'package:agritech/models/get_utility_payment_by_id_model.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../data/filter_utilities_by_location_model.dart';

class LandlordUtilityRepoImpl extends LandlordUtilityRepo with NetworkCheckService {
  final DioClient _dioClient;

  LandlordUtilityRepoImpl(this._dioClient);

  @override
  Future<Either<GetRecentFourUtilitiesModel, Exception>> recentUtilities() async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    }
    try {
      var result = await _dioClient.requestForAuth('utility/getRecentFourUtilities?search=${Get.find<LandlordUtilitiesController>().utilitySearch.value}', Method.get);
      return result.fold(
        (l) {
          GetRecentFourUtilitiesModel model = GetRecentFourUtilitiesModel.fromJson(l.data);
          return Left(model);
        },
        (r) => Right(Exception("API Error: $r")),
      );
    } catch (e) {
      return Right(Exception("Unexpected Error in recentUtilities: ${e.toString()}"));
    }
  }

  @override
  Future<Either<CreateUtilityModel, Exception>> addInstrument({params1}) async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    }
    try {
      var result = await _dioClient.requestForAuth('utility/createUtility', Method.post, params: params1);
      return result.fold(
        (l) {
          CreateUtilityModel model = CreateUtilityModel.fromJson(l.data);
          return Left(model);
        },
        (r) => Right(Exception("API Error: $r")),
      );
    } catch (e) {
      return Right(Exception("Unexpected Error in addInstrument: ${e.toString()}"));
    }
  }

  @override
  Future<Either<GetUtilityByIdModel, Exception>> getUtilityById({id}) async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    }
    try {
      var utilityId = Get.find<UtilityController>().utilityId.value ?? id;
      var result = await _dioClient.requestForAuth('utility/getUtilityById?id=$utilityId', Method.get);
      return result.fold(
        (l) {
          GetUtilityByIdModel model = GetUtilityByIdModel.fromJson(l.data);
          return Left(model);
        },
        (r) => Right(Exception("API Error: $r")),
      );
    } catch (e) {
      return Right(Exception("Unexpected Error in getUtilityById: ${e.toString()}"));
    }
  }

    @override
  Future<Either<GetUtilityPaymentByIdModel, Exception>> getUtilityPaymentById({id}) async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    }
    try {

      var result = await _dioClient.requestForAuth('utility/getUtilityById?id=$id', Method.get);
      return result.fold(
        (l) {
          GetUtilityPaymentByIdModel model = GetUtilityPaymentByIdModel.fromJson(l.data);
          return Left(model);
        },
        (r) => Right(Exception("API Error: $r")),
      );
    } catch (e) {
      return Right(Exception("Unexpected Error in getUtilityById: ${e.toString()}"));
    }
  }

  @override
  Future<Either<GetUtilityByLocationModel, Exception>> getUtilitesByLocation() async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    }
    try {
      var result = await _dioClient.requestForAuth('utility/getUtilityByLocation?Location=${Get.find<DetailsOfLandlordUtilitiesController>().utilityVillage}', Method.get);
      return result.fold(
        (l) {
          GetUtilityByLocationModel model = GetUtilityByLocationModel.fromJson(l.data);
          return Left(model);
        },
        (r) => Right(Exception(r)),
      );
    } catch (e) {
      return Right(Exception("Unexpected Error in getUtilitesByLocation: ${e.toString()}"));
    }
  }

  @override
  Future<Either<GetRecentFourUtilitiesModel, Exception>> filterUtilities({params1}) async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    }
    try {
      var result = await _dioClient.requestForAuth('utility/filterUtilitiesByLocation', Method.post, params: params1);
      return result.fold(
        (l) {
          GetRecentFourUtilitiesModel model = GetRecentFourUtilitiesModel.fromJson(l.data);
          return Left(model);
        },
        (r) => Right(Exception("API Error: $r")),
      );
    } catch (e) {
      return Right(Exception("Unexpected Error in filterUtilities: ${e.toString()}"));
    }
  }
   @override
  Future<Either<CreateUtilityPaymentModel, Exception>> utilityPayment({params1}) async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    }
    try {
      var result = await _dioClient.requestForAuth('utilityPayment/createUtilityPayment', Method.post, params: params1);
      return result.fold(
        (l) {
          CreateUtilityPaymentModel model = CreateUtilityPaymentModel.fromJson(l.data);
          return Left(model);
        },
        (r) => Right(Exception("API Error: $r")),
      );
    } catch (e) {
      return Right(Exception("Unexpected Error in utilityPayment: ${e.toString()}"));
    }
  }
   @override
  Future<Either<GetUtilityPaymentByIdModel, Exception>> getUtilityPayment() async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    }
    try {
      var result = await _dioClient.requestForAuth('utilityPayment/getUtilityPaymentById?id=${Get.find<DetailsOfLandlordUtilitiesController>().createdUtilityId}', Method.get);
      return result.fold(
        (l) {
          GetUtilityPaymentByIdModel model = GetUtilityPaymentByIdModel.fromJson(l.data);
          return Left(model);
        },
        (r) => Right(Exception(r)),
      );
    } catch (e) {
      return Right(Exception("Unexpected Error in getUtilityPayment: ${e.toString()}"));
    }
  }

  @override
  Future<Either<FilterUtilitiesByLocationModel,Exception>> filterUtilityLocation({params1}) async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    }
    try {
      var result = await _dioClient.requestForAuth('utility/filterUtilitiesByLocation', Method.post, params: params1);
      return result.fold(
        (l) {
          FilterUtilitiesByLocationModel model = FilterUtilitiesByLocationModel.fromJson(l.data);
          return Left(model);
        },
        (r) => Right(Exception("API Error: $r")),
      );
    } catch (e) {
      return Right(Exception("Unexpected Error in addInstrument: ${e.toString()}"));
    }
  }
}
