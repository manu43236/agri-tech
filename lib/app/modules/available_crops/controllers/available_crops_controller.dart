import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:get/get.dart';

import '../../vendor/data/get_filtered_crop_availability_model.dart';
import '../../vendor/services/vendor_repo_impl.dart';
import '../data/filter_with_crop_and_quantity_model.dart';
import '../data/get_all_crops_model.dart';
import '../services/available_crops_repo_impl.dart';

class AvailableCropsController extends BaseController {

  Rx<GetFilteredCropAvailabilityModel> getfilteredCropAvailabilityModel =
      Rx(GetFilteredCropAvailabilityModel());

  Rx<GetAllCropsModel> getAllCropsModel =
      Rx(GetAllCropsModel());

  Rx<FilterWithCropAndQuantityModel> filterWithCropAndQuantityModel =
      Rx(FilterWithCropAndQuantityModel());

  var apiFilteredCrops=ApiStatus.LOADING.obs;
  var apiFiltercropquanStatus=ApiStatus.LOADING.obs;

  RxString selectedCrop = ''.obs;
  RxInt minQuantity = 0.obs;
  RxInt maxQuantity = 100.obs;
  RxInt selectedQuantity = 0.obs;
  var selectedMinQuan=0.obs;
  var selectedMaxQuan=0.obs;
  var userId=''.obs;
 

  @override
  void onInit() async{
    super.onInit();
    userId.value=await SecureStorage().readData(key: "id"??'');
    await filteredCrops(userId: userId.value,latitude: Get.find<HomeController>().latitude.value,longitude: Get.find<HomeController>().longitude.value );
    getCrops();
  }

  @override
  void onReady() {
    super.onReady();
  }

   //filtered crops API
  Future<void> filteredCrops({userId,latitude,longitude}) async {
    apiFilteredCrops.value = ApiStatus.LOADING;
    try {
      var result = await VendorRepoImpl(dioClient).getFilteredCrops(userId: userId,latitude: latitude,longitude: longitude);
      print('=============== get filtered crops =========');
      print(result);
      result.fold((model) {
        getfilteredCropAvailabilityModel.value = model;
        apiFilteredCrops.value = ApiStatus.SUCCESS;
      }, (r) {
        apiFilteredCrops.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }


  // get crops API
  Future<void> getCrops() async {
    apiStatus.value= ApiStatus.LOADING;
    try {
      var result = await AvailableCropsRepoImpl(dioClient).getCrops();
      print('=============== get crops =========');
      print(result);
      result.fold((model) {
        getAllCropsModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  // Filter with crop & quantity API
  Future<void> filterWithCropQuantity({crop,minQuan,maxQuan}) async {
    apiFiltercropquanStatus.value= ApiStatus.LOADING;
    try {
      var result = await AvailableCropsRepoImpl(dioClient).filterWithCropQuantity(crop: crop,minQuan:minQuan,maxQuan: maxQuan);
      print('=============== filter with crops and quantity =========');
      print(result);
      result.fold((model) {
        filterWithCropAndQuantityModel.value = model;
        apiFiltercropquanStatus.value = ApiStatus.SUCCESS;
        update();
      }, (r) {
        apiFiltercropquanStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onClose() {
    super.onClose();
  }

}
