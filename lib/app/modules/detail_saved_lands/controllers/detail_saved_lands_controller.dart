import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/saved_lands/controllers/saved_lands_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../detail_recent_lands/data/land_by_id_model.dart';
import '../../home/services/land_by_id_repo_impl.dart';
import '../../saved/data/get_all_saved_lands_model.dart';
import '../../saved/services/saved_repo_impl.dart';

class DetailSavedLandsController extends BaseController {
  
  final HomeController homeController=Get.find<HomeController>();

  Rx<AllSavedLandsModel> allSavedLandsModel = Rx(AllSavedLandsModel());
  Rx<LandByIdModel> landByIdModel = Rx(LandByIdModel());

  var userId=''.obs;
  var apiLandIdStatus = ApiStatus.LOADING.obs;

  @override
  void onInit() async {
    super.onInit();
    userId.value = await SecureStorage().readData(key: "id") ?? "";
    await getIdLands(id: Get.arguments["landId"]);
    allLandsSaved(id: userId.value);
  }

  // Lands BY id API
  Future<void> getIdLands({id}) async {
    apiLandIdStatus.value = ApiStatus.LOADING;
    try {
      var result = await LandByIdRepoImpl(dioClient).getLand(params1: {
        "id": id,
      });

      result.fold((model) {
        landByIdModel.value = model;
        apiLandIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiLandIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

      //get all saved lands API
  Future<void> allLandsSaved({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient)
          .allSavedLands(userId: id);
      print("===================== get all saved lands ==============");
      print(result);
      result.fold((model) {
        allSavedLandsModel.value = model;
        print(allSavedLandsModel.value.result!.landCount!);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
