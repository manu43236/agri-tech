// import 'package:flutter/material.dart';

// import '../../../../core/conts/color_consts.dart';

// class CustomExperienceField extends StatelessWidget {
//   final TextEditingController controller;
//   final String? Function(String?)? validator; // Add a validator parameter
  
//   const CustomExperienceField({
//     super.key,
//     required this.controller,
//     this.validator, // Initialize the validator parameter
//   });

//   @override
//   Widget build(BuildContext context) {
//     return TextFormField(
//       controller: controller,
//       decoration: InputDecoration(
//         contentPadding: const EdgeInsets.symmetric(vertical: 0,horizontal: 10),
//         enabledBorder: OutlineInputBorder(
//           borderRadius: BorderRadius.circular(12),
//           borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
//         ),
//         focusedBorder: OutlineInputBorder(
//           borderRadius: BorderRadius.circular(12),
//           borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
//         ),
//         errorBorder: OutlineInputBorder(
//           borderRadius: BorderRadius.circular(12),
//           borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
//         ),
//         focusedErrorBorder: OutlineInputBorder(
//           borderRadius: BorderRadius.circular(12),
//           borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
//         ),
//         errorStyle: const TextStyle(
//           color: Colors.red, // This will set the error text color to red
//         ),
//         suffix: Container(

//         ),
//       ),
//       validator: validator, // Assign the validator to the TextFormField
//     );
//   }
// }

// import 'package:agritech/app/modules/add_skill/controllers/add_skill_controller.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// import '../../../../core/conts/color_consts.dart';

// class CustomExperienceField extends StatelessWidget {
//   final String? Function(String?)? validator;
//   final TextEditingController controller;

//   const CustomExperienceField({
//     super.key,
//     required this.controller,
//     this.validator,
//   });

//   @override
//   Widget build(BuildContext context) {
//     final Controller = Get.find<AddSkillController>(); // GetX controller

//     return Obx(() => Row(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             Expanded(
//               flex: 2,
//               child: TextFormField(
//                 controller: controller,
//                 decoration: InputDecoration(
//                   contentPadding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                   enabledBorder: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(12),
//                     borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
//                   ),
//                   focusedBorder: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(12),
//                     borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
//                   ),
//                   errorBorder: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(12),
//                     borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
//                   ),
//                   focusedErrorBorder: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(12),
//                     borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
//                   ),
//                   errorStyle: const TextStyle(
//                     color: Colors.red, // Error text color
//                   ),
//                 ),
//                 validator: validator,
//               ),
//             ),
//             const SizedBox(width: 10), // Space between the field and dropdown
//             Expanded(
//               flex: 1,
//               child: DropdownButtonFormField<String>(
//                 value: Controller.selectedSuffix.value,
//                 decoration: InputDecoration(
//                   contentPadding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
//                   enabledBorder: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(12),
//                     borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
//                   ),
//                   focusedBorder: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(12),
//                     borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
//                   ),
//                 ),
//                 items: const [
//                   DropdownMenuItem(value: "Year", child: Text("Year")),
//                   DropdownMenuItem(value: "Month", child: Text("Month")),
//                 ],
//                 onChanged: (value) {
//                   if (value != null) {
//                     Controller.updateSuffix(value); // Update the controller
//                   }
//                 },
//               ),
//             ),
//           ],
//         ));
//   }
// }

import 'package:agritech/app/modules/add_skill/controllers/add_skill_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';

class CustomExperienceField extends StatelessWidget {
  final String? Function(String?)? validator;
  final TextEditingController controller;

  const CustomExperienceField({
    super.key,
    required this.controller,
    this.validator,
  });

  @override
  Widget build(BuildContext context) {
    final Controller = Get.find<AddSkillController>(); // GetX controller

    return Obx(() => TextFormField(
          controller: controller,
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12),
              borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12),
              borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12),
              borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12),
              borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
            ),
            errorStyle: const TextStyle(
              color: Colors.red, // Error text color
            ),
            suffixIcon: Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Container(
                width: 100, // Set a fixed width for the dropdown
                padding: const EdgeInsets.only(right: 10),
                
                child: DropdownButton<String>(
                  isExpanded: true, // Ensure the dropdown expands to fill the container
                  value: Controller.selectedSuffix.value,
                  underline: const SizedBox(), // Removes the underline
                   style: const TextStyle(fontSize: 14, color: Colors.black), 
                  items: const [
                    DropdownMenuItem(value: "Years", child: Text("Years")),
                    DropdownMenuItem(value: "Months", child: Text("Months")),
                  ],
                  onChanged: (value) {
                    if (value != null) {
                      Controller.updateSuffix(value); // Update the controller
                    }
                  },
                ),
              ),
            ),
          ),
          validator: validator,
        ));
  }
}
