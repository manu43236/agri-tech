import 'package:agritech/app/modules/hired_farmworkers/controllers/hired_farmworkers_controller.dart';
import 'package:get/get.dart';

import '../controllers/hired_farmers_controller.dart';

class HiredFarmersBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HiredFarmersController>(
      () => HiredFarmersController(),
    );
     Get.lazyPut<HiredFarmworkersController>(
      () => HiredFarmworkersController(),
    );
  }
}
