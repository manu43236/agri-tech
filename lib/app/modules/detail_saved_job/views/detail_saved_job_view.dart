import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/detail_saved_job_controller.dart';

class DetailSavedJobView extends GetView<DetailSavedJobController> {
  const DetailSavedJobView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'detail'.tr,
          style: TextStyles.kTSFS20W500
              .copyWith(color: colorBlack, fontWeight: FontWeight.w400),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(right: 20, left: 20),
        child: Obx(() => controller.apiJobIdStatus.value == ApiStatus.LOADING
            ? Shimmers().getListShimmer()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: Get.height * 0.35,
                      width: Get.width,
                      decoration: BoxDecoration(
                          color: colorAsh,
                          borderRadius: BorderRadius.circular(13)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: controller.jobByIdModel.value.result?.image !=
                                null
                            ? Image.network(
                                controller.jobByIdModel.value.result!.image!,
                                height: Get.height * 0.35,
                                width: Get.width,
                                fit: BoxFit.cover,
                              )
                            : Image.asset(
                                'assets/images/agri.png',
                                height: Get.height * 0.35,
                                width: Get.width,
                                fit: BoxFit.cover,
                              ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${'Job_at'.tr} ${controller.jobByIdModel.value.result?.village ?? 'Unknown Village'}',
                          style: TextStyles.kTSFS20W700
                              .copyWith(color: colorDetails),
                        ),
                        Text(
                          '${controller.jobByIdModel.value.result?.mandal ?? 'Unknown Mandal'}, ${controller.jobByIdModel.value.result?.district ?? 'Unknown District'}',
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorDetails),
                        ),
                      ],
                    ),
                    const SizedBox(height: 15),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          radius: 26,
                          backgroundColor: colorAsh,
                          child: ClipOval(
                            child: Image.network(
                              controller.homeController.getUserModel.value
                                      .result?.imageUrl ??
                                  defaultImageUrl,
                              height: Get.height * 0.1,
                              width: Get.width * 0.2,
                              fit: BoxFit.cover,
                              errorBuilder: (context, error, stackTrace) {
                                // Display a local asset if the network image fails
                                return Image.asset(
                                  defaultImageUrl,
                                  height: Get.height * 0.1,
                                  width: Get.width * 0.2,
                                  fit: BoxFit.cover,
                                );
                              },
                            ),
                          ),
                        ),
                        const SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              controller.homeController.getUserModel.value
                                      .result?.firstName ??
                                  'Unknown User',
                              style: TextStyles.kTSDS14W700
                                  .copyWith(color: colorPic),
                            ),
                            const SizedBox(height: 5),
                            Text(
                              controller.homeController.getUserModel.value
                                      .result?.address ??
                                  'Unknown Address',
                              style: TextStyles.kTSWFS10W700
                                  .copyWith(color: colorDetails),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Text(
                      controller.jobByIdModel.value.result?.description ??
                          'No no_description_available'.tr,
                      style: TextStyles.kTSCF12W500,
                    ),
                    const SizedBox(height: 20),
                    Text(
                      'saved_jobs'.tr,
                      style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                    ),
                    const SizedBox(height: 10),
                    SizedBox(
                      height: Get.height * 0.08,
                      width: Get.width,
                      child: Obx(() {
                        final jobs = controller
                            .getAllSavedJobsModel.value.result?.savedJobs;

                        if (jobs != null) {
                          // Exclude the selected job from the list
                          final filteredJobs = jobs
                              .where((job) =>
                                  job.jobid.toString() !=
                                  controller.homeController.jobId.value)
                              .toList();

                          if (filteredJobs.isEmpty) {
                            return Center(
                              child: Text(
                                "no_jobs_available".tr,
                                style: TextStyles.kTSFS16W600
                                    .copyWith(color: colorHello),
                              ),
                            );
                          }

                          return ListView.separated(
                            itemCount: filteredJobs.length,
                            scrollDirection: Axis.horizontal,
                            separatorBuilder: (context, index) =>
                                const SizedBox(width: 10),
                            itemBuilder: (context, index) {
                              final job = filteredJobs[index];
                              final jobImage = job.imageUrl;

                              return InkWell(
                                onTap: () async {
                                  controller.homeController.jobId.value =
                                      job.jobid?.toString() ?? '';
                                  await controller.getIdJobs(
                                      id: controller
                                          .homeController.jobId.value);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: colorGrey,
                                    borderRadius: BorderRadius.circular(11),
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(11),
                                    child: jobImage != null
                                        ? Image.network(
                                            jobImage,
                                            height: Get.height * 0.09,
                                            width: Get.width * 0.2,
                                            fit: BoxFit.cover,
                                          )
                                        : Image.asset(
                                            'assets/images/agri.png',
                                            height: Get.height * 0.09,
                                            width: Get.width * 0.2,
                                            fit: BoxFit.cover,
                                          ),
                                  ),
                                ),
                              );
                            },
                          );
                        }

                        return Center(
                          child: Text(
                            "no_jobs_available".tr,
                            style: TextStyles.kTSFS16W600
                                .copyWith(color: colorHello),
                          ),
                        );
                      }),
                    ),
                  ],
                ),
              )),
      ),
    );
  }
}
