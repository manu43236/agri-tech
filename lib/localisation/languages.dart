// import 'package:agritech/localisation/lang_jsons/tel_lang.dart';
// import 'package:get/get_navigation/src/root/internacionalization.dart';

// import 'lang_jsons/eng_lang.dart';


// class Languages extends Translations {
//   @override
//   Map<String, Map<String, String>> get keys => {
//     'en_US': engLang,
//     'tel_IN':telLang
//     };
// }

import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'lang_jsons/eng_lang.dart';
import 'lang_jsons/tel_lang.dart';

class LocalizationService extends Translations {
  static final Locale defaultLocale = Locale('en', 'US');
  static final List<Locale> supportedLocales = [
    Locale('en', 'US'),
    Locale('te', 'IN'),
  ];


  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': engLang,
        'te_IN': telLang,
      };

  // static Future<void> saveLanguage(String languageCode) async {
  //   await SecureStorage().writeStringData("langCode", languageCode);
  // }

  //  static Future<Locale> getSavedLocale() async {
  //   final languageCode = await SecureStorage().readData(key:"langCode");
  //   if (languageCode != null) {
  //     final localeParts = languageCode.split('_');
  //     return Locale(localeParts[0], localeParts[1]);
  //   }
  //   return defaultLocale;
  // }

  // void changeLocale(String langCode, String countryCode) {
  //   final locale = Locale(langCode, countryCode);
  //   Get.updateLocale(locale);
  // }
}
