import 'package:agritech/app/modules/available_leased_utilities/services.dart/available_leased_utilities_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../../home/controllers/home_controller.dart';
import '../../home/data/get_lease_utilities_details_model.dart';
import '../controllers/available_leased_utilities_controller.dart';

class AvailableLeasedUtilitiesRepoImpl extends AvailableLeasedUtilitiesRepo with NetworkCheckService{
  final DioClient _dioClient;
  AvailableLeasedUtilitiesRepoImpl(this._dioClient);

  @override
  Future<Either<GetLeaseUtilitiesDetailsModel,Exception>>  leasedUtilities()async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Leasetaker/getLeaseUtilityDetails?id=${Get.find<HomeController>().userId.value}&search=${Get.find<AvailableLeasedUtilitiesController>().leasedUtilitiesSearch.value}', Method.get);
        return result.fold((l) {
          GetLeaseUtilitiesDetailsModel model = GetLeaseUtilitiesDetailsModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}