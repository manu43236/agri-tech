import 'package:get/get.dart';

import '../controllers/filter_vendor_controller.dart';

class FilterVendorBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FilterVendorController>(
      () => FilterVendorController(),
    );
  }
}
