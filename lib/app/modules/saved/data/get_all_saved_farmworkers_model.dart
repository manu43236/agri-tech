// To parse this JSON data, do
//
//     final getAllSavedFarmworkerModel = getAllSavedFarmworkerModelFromJson(jsonString);

import 'dart:convert';

GetAllSavedFarmworkerModel getAllSavedFarmworkerModelFromJson(String str) => GetAllSavedFarmworkerModel.fromJson(json.decode(str));

String getAllSavedFarmworkerModelToJson(GetAllSavedFarmworkerModel data) => json.encode(data.toJson());

class GetAllSavedFarmworkerModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    GetAllSavedFarmworkerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetAllSavedFarmworkerModel.fromJson(Map<String, dynamic> json) => GetAllSavedFarmworkerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<SavedFarmer>? savedFarmers;
    int? farmerCount;

    Result({
        this.savedFarmers,
        this.farmerCount,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        savedFarmers: json["savedFarmers"] == null ? [] : List<SavedFarmer>.from(json["savedFarmers"]!.map((x) => SavedFarmer.fromJson(x))),
        farmerCount: json["farmerCount"],
    );

    Map<String, dynamic> toJson() => {
        "savedFarmers": savedFarmers == null ? [] : List<dynamic>.from(savedFarmers!.map((x) => x.toJson())),
        "farmerCount": farmerCount,
    };
}

class SavedFarmer {
    int? savedFarmworkerId;
    int? farmworkerid;
    String? firstName;
    dynamic locationDetails;
    String? mobileNumber;
    String? imageUrl;
    String? address;
    int? age;
    bool? flag;

    SavedFarmer({
        this.savedFarmworkerId,
        this.farmworkerid,
        this.firstName,
        this.locationDetails,
        this.mobileNumber,
        this.imageUrl,
        this.address,
        this.age,
        this.flag,
    });

    factory SavedFarmer.fromJson(Map<String, dynamic> json) => SavedFarmer(
        savedFarmworkerId: json["savedFarmworkerId"],
        farmworkerid: json["farmworkerid"],
        firstName: json["firstName"],
        locationDetails: json["location_details"],
        mobileNumber: json["mobileNumber"],
        imageUrl: json["image_url"],
        address: json["address"],
        age: json["age"],
        flag: json["flag"],
    );

    Map<String, dynamic> toJson() => {
        "savedFarmworkerId": savedFarmworkerId,
        "farmworkerid": farmworkerid,
        "firstName": firstName,
        "location_details": locationDetails,
        "mobileNumber": mobileNumber,
        "image_url": imageUrl,
        "address": address,
        "age": age,
        "flag": flag,
    };
}
