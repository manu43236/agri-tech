import 'package:agritech/app/modules/home/services/district_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:agritech/app/modules/select_region/data/district_model.dart';
import 'package:dartz/dartz.dart';


class DistrictRepoImpl extends DistrictRepo with NetworkCheckService{
  final DioClient _dioClient;
  DistrictRepoImpl( this._dioClient);
  @override
  Future<Either<DistrictModel,Exception>> getDistrict({id,lang})async{
    var data=await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('District/getDistrictsByStateId?state_id=$id&language=$lang',Method.get);
        return result.fold((l){
          DistrictModel model=DistrictModel.fromJson(l.data);
          return left(model);
        },(r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}