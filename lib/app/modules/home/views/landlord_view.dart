import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/home/views/widgets/leased_utilities_widget.dart';
import 'package:agritech/app/modules/home/views/widgets/my_jobs_widget.dart';
import 'package:agritech/app/modules/home/views/widgets/my_lands_widget.dart';
import 'package:agritech/app/modules/home/views/widgets/recent_lands_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/landlord_controller.dart';



class LandlordView extends GetView<LandlordController> {
  const LandlordView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return  SafeArea(
      child: RefreshIndicator(
        onRefresh: ()async {
          controller.recentLands();
          controller.addAllLands(userId:Get.find<HomeController>().userId.value);
          controller.addAlljobs(userId:Get.find<HomeController>().userId.value);
          controller.leasedUtilities(userId:Get.find<HomeController>().userId.value);
        },
        child: const SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RecentLandsWidget(),
              MyLandsWidget(),
              MyJobsWidget(),
              LeasedUtilitiesWidget()
            ],
          ),
        ),
      ),
    ); 
  }
}
