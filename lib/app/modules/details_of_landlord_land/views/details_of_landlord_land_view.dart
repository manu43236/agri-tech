import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../controllers/details_of_landlord_land_controller.dart';

class DetailsOfLandlordLandView
    extends GetView<DetailsOfLandlordLandController> {
  const DetailsOfLandlordLandView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'details_of_land'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () =>controller.apiLandIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer():
         Padding(
          padding: const EdgeInsets.only(right: 20, left: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: colorAsh,
                    borderRadius: BorderRadius.circular(13),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.network(
                      (controller.landByIdModel.value.result
                                  ?.landDetails?.imageUrl?.isNotEmpty ??
                              false)
                          ? controller.landByIdModel.value.result
                                  ?.landDetails?.imageUrl ??
                              defaultImageUrl
                          : '',
                      height: Get.height * 0.3,
                      width: Get.width,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.3,
                          width: Get.width,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${controller.landByIdModel.value.result?.landDetails?.fullName ?? 'Unknown'} ${'at'.tr}',
                          style: TextStyles.kTSFS20W700
                              .copyWith(color: colorDetails),
                        ),
                        Text(
                          '${controller.landByIdModel.value.result?.landDetails?.village ?? ''}, ${controller.landByIdModel.value.result?.landDetails?.district ?? ''}',
                          style: TextStyles.kTSDS14W500
                              .copyWith(color: colorDetails),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 15),
                Row(
                  children: [
                    Image.asset(waterCycle, height: 15),
                    const SizedBox(width: 10),
                    Text(
                      controller.landByIdModel.value.result
                                  ?.landDetails?.water ==
                              'No'
                          ? 'not_available'.tr
                          : 'available'.tr,
                      style:
                          TextStyles.kTSFS12W400.copyWith(color: colorDetails),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Text(
                  controller.landByIdModel.value.result
                          ?.landDetails?.description ??
                      '',
                  style: TextStyles.kTSCF12W500,
                ),
                const SizedBox(height: 50),
                Align(
                  alignment: Alignment.center,
                  child: 
                  
                controller.statusForLandlord.value=="" && controller.apiStatusUpdate.value==ApiStatus.LOADING?InkWell(
                    onTap: () async {      
                     await controller
                          .sendLeaseInterestNotificationByFarmer();
                      controller.sendLeaseInterestNotificationByFarmerFetch();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 25),
                      decoration: BoxDecoration(
                        color: colorPic,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text(
                        'show_interest'.tr,
                        style:
                            TextStyles.kTSFS16W700.copyWith(color: colorWhite),
                      ),
                    ),
                  ):Text(controller.statusForLandlord.value,style: TextStyles.kTSFS18W600.copyWith(color:controller.statusForLandlord.value=='pending'?Color.fromARGB(255, 227, 154, 44): primary),),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
