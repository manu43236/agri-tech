import 'package:get/get.dart';

import '../../notifications/controllers/notifications_controller.dart';
import '../../saved_lands/controllers/saved_lands_controller.dart';
import '../controllers/details_of_landlord_land_controller.dart';

class DetailsOfLandlordLandBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailsOfLandlordLandController>(
      () => DetailsOfLandlordLandController(),
    );
    Get.lazyPut<NotificationsController>(
      ()=>NotificationsController()
    );
     Get.lazyPut<SavedLandsController>(
      ()=>SavedLandsController()
    );
  }
}
