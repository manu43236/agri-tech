// import 'package:flutter/material.dart';

// class CustomSearchField extends StatelessWidget {
//   final String hintText;
//   final IconData suffixIcon;
//   final TextEditingController? controller;
//   final ValueChanged<String>? onChanged;
//   final EdgeInsetsGeometry? padding;

//   const CustomSearchField({
//     Key? key,
//     this.hintText = 'Search',
//     this.suffixIcon = Icons.search,
//     this.controller,
//     this.onChanged,
//     this.padding = const EdgeInsets.symmetric(horizontal: 20),
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: padding!,
//       child: ConstrainedBox(
//         constraints: const BoxConstraints(
//           minHeight: 40, 
//           maxHeight: 40, 
//         ),
//         child: TextField(
//           controller: controller,
//           onChanged: onChanged,
//           style: const TextStyle(fontSize: 14), 
//           decoration: InputDecoration(
//             isDense: true, 
//             contentPadding: const EdgeInsets.symmetric(vertical: 4, horizontal: 10),
//             hintText: hintText,
//             suffixIcon: Icon(suffixIcon, size: 20), 
//             border: OutlineInputBorder(
//               borderRadius: BorderRadius.circular(16),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }


import 'package:flutter/material.dart';

class CustomSearchField extends StatelessWidget {

  final Function(String) onChanged;

  const CustomSearchField({super.key,required this.onChanged,});

  @override
  Widget build(BuildContext context) {
    final FocusNode focusNode = FocusNode();
    return Row(
    mainAxisSize: MainAxisSize.max,
    children: [
      Expanded(
        flex: 8,
        child: TextField(
          focusNode: focusNode, // Attach FocusNode to the TextField.
          onChanged: onChanged,
        //   (val) {
        //    // controller.myCustomerSearchText.value = val;
        //     if(val.length>3){
        //       //controller.getAllLandlordLands();
        //     }
        // else {
        //   //controller.getAllLandlordLands(); 
        // }
          //},
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey.shade400,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey.shade600,
              ),
            ),
            prefixIcon: Icon(
              Icons.search,
              color: Colors.grey.shade600,
            ),
            constraints: const BoxConstraints(maxHeight: 40.0),
            contentPadding: const EdgeInsets.symmetric(vertical: 2.0),
          ),
        ),
      ),
    ],
  );
  }
}
