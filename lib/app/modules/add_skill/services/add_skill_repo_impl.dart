import 'package:agritech/app/modules/add_skill/services/add_skill_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../data/add_skill_model.dart';

class AddSkillRepoImpl extends AddSkillRepo with NetworkCheckService{
  final DioClient _dioClient;
  AddSkillRepoImpl(this._dioClient);

  @override
  Future<Either<AddSkillModel,Exception>> addSkill({params1})async{
    bool data=await checkInternet();
    if(!data){
      return Right(Exception("No Internet found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('FarmerWorkerSkills/addSkill', Method.post,params: params1);
        return result.fold((l) {
          AddSkillModel model = AddSkillModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}