import 'package:agritech/app/modules/login/services/login_repo_impl.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/models/login_get_otp_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';

class LoginController extends BaseController {
  Rx<TextEditingController> mobileController = Rx(TextEditingController());
  var profileName = "".obs;
  var role = "".obs;
  var otpValue = "".obs;
  @override
  void onInit() async {
    super.onInit();
    profileName.value = await SecureStorage().readData(key: "firstName") ?? "";
    role.value = await SecureStorage().readData(key: "role") ?? "";
  }

  @override
  void onReady() {
    super.onReady();
  }

  void doLoginGetOtp() async {
    try {
      var result = await LoginRepoImpl(dioClient).loginOtp(params1: {
        "mobileNumber": mobileController.value.text,
      });
      result.fold((left) {
        _handleOtpResponse(left);
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void _handleOtpResponse(LoginGetOtpModel model) async {
    print('OTP response: ${model.toString()}');
    if (model.statusCode == 200) {
      await SecureStorage()
          .writeStringData("id", model.result!.user!.id.toString());
      otpValue.value = model.result!.user!.otp.toString();
      Get.toNamed(Routes.OTP, arguments: {
        "isLogin": true,
        "otp": otpValue.value,
        'mobileNumber': mobileController.value.text
      });

      print('OTP sent successfully');
    } else if (model.statusCode == 400) {
      Get.snackbar(
        'FAILED',
        'User does not exist',
        snackPosition: SnackPosition.TOP,
      );
    } else {
      apiStatus.value = ApiStatus.FAIL;
      print('Failed to send OTP: ${model.message}');
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
