import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/date_format.dart';
import '../controllers/jobs_posted_by_landlord_controller.dart';

class JobsPostedByLandlordView extends GetView<JobsPostedByLandlordController> {
  const JobsPostedByLandlordView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false, // Prevents default back button
        flexibleSpace: Stack(
          children: [
            SizedBox(
              width: Get.width,
              child: Image.asset(
                profileLogo,
                fit: BoxFit.cover,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30, right: 10, left: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: const Icon(Icons.arrow_back),
                    onPressed: () {
                      Get.back();
                    },
                  ),
                   Text(
                    'jobs'.tr,
                    style: TextStyles.kTSFS26W400,
                  ),
                  const SizedBox(
                    width: 30,
                  ),
                ],
              ),
            ),
          ],
        ),
        toolbarHeight: 80, // Adjust height as needed
        backgroundColor:
            Colors.transparent, // Make AppBar background transparent
        elevation: 0, // Remove shadow
      ),
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 20,right: 20,top: 20),
              child: Obx(() {
                final ongoingLands = controller
                    .allJobsOfLandlordModel.value.result?.ongoingJobs;

                if (ongoingLands == null || ongoingLands.isEmpty) {
                  return  Center(
                    child: Text(
                      "no_jobs_available".tr,
                      style: TextStyles.kTSDS14W500,
                    ),
                  );
                }

                return ListView.separated(
                  padding: EdgeInsets.zero,
                  itemCount: ongoingLands.length,
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: 10),
                  itemBuilder: (context, index) {
                    final land = ongoingLands[index];
                    return Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 10),
                      width: Get.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: colorFilter,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                land.fullName ?? " ",
                                style: TextStyles.kTSDS14W500
                                    .copyWith(color: colorHello),
                              ),
                              const SizedBox(height: 5),
                              Text(
                                '${'loc'.tr}  ${land.village ?? ""}',
                                style: TextStyles.kTSCF12W500
                                    .copyWith(color: colorGrey.shade500),
                              ),
                              const SizedBox(height: 5),
                              Text(
                                '${'posted_on'.tr} ${formatDateToIST(land.createdAt.toString() ?? "")}',
                                style: TextStyles.kTSCF12W500
                                    .copyWith(color: colorGrey.shade500),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ],
                          ),
                          Flexible(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(15),
                              child:
                                  land.image != null && land.image!.isNotEmpty
                                      ? Image.network(
                                          land.image!,
                                          height: Get.height * 0.09,
                                          width: Get.width * 0.3,
                                          fit: BoxFit.cover,
                                        )
                                      : Image.asset(
                                          defaultImageUrl,
                                          height: Get.height * 0.09,
                                          width: Get.width * 0.3,
                                          fit: BoxFit.cover,
                                        ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                );
              }),
            ),
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}
