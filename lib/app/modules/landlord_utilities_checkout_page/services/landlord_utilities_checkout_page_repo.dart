import 'package:agritech/app/modules/landlord_utilities_checkout_page/data/pay_payment_model.dart';
import 'package:dartz/dartz.dart';

import '../../details_of_landlord_utilities/data/create_utility_payment_model.dart';
import '../data/verify_utility_payment_model.dart';

abstract class LandlordUtilitiesCheckoutPageRepo{
  Future<Either<CreateUtilityPaymentModel,Exception>> updateQuantity({params1});
  Future<Either<PayPaymentModel,Exception>> payPayment({paymentId});
  Future<Either<PayPaymentModel,Exception>> payBalancePayment({balancePaymentId});
  Future<Either<VerifyUtilityPaymentModel,Exception>> verifyPayment({params1});
}