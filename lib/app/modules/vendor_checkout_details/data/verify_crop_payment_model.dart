// To parse this JSON data, do
//
//     final verifyCropPaymentModel = verifyCropPaymentModelFromJson(jsonString);

import 'dart:convert';

VerifyCropPaymentModel verifyCropPaymentModelFromJson(String str) => VerifyCropPaymentModel.fromJson(json.decode(str));

String verifyCropPaymentModelToJson(VerifyCropPaymentModel data) => json.encode(data.toJson());

class VerifyCropPaymentModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    VerifyCropPaymentModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory VerifyCropPaymentModel.fromJson(Map<String, dynamic> json) => VerifyCropPaymentModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    PaymentRecord? paymentRecord;
    AgriYieldRecord? agriYieldRecord;

    Result({
        this.paymentRecord,
        this.agriYieldRecord,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        paymentRecord: json["paymentRecord"] == null ? null : PaymentRecord.fromJson(json["paymentRecord"]),
        agriYieldRecord: json["agriYieldRecord"] == null ? null : AgriYieldRecord.fromJson(json["agriYieldRecord"]),
    );

    Map<String, dynamic> toJson() => {
        "paymentRecord": paymentRecord?.toJson(),
        "agriYieldRecord": agriYieldRecord?.toJson(),
    };
}

class AgriYieldRecord {
    int? id;
    int? userid;
    int? landid;
    int? cropId;
    dynamic outedDate;
    dynamic hectors;
    dynamic remainingQuantity;
    bool? status;
    dynamic createdAt;
    dynamic updatedAt;

    AgriYieldRecord({
        this.id,
        this.userid,
        this.landid,
        this.cropId,
        this.outedDate,
        this.hectors,
        this.remainingQuantity,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory AgriYieldRecord.fromJson(Map<String, dynamic> json) => AgriYieldRecord(
        id: json["id"],
        userid: json["userid"],
        landid: json["landid"],
        cropId: json["cropId"],
        outedDate: json["outed_date"] == null ? null : DateTime.parse(json["outed_date"]),
        hectors: json["hectors"],
        remainingQuantity: json["remainingQuantity"],
        status: json["status"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userid": userid,
        "landid": landid,
        "cropId": cropId,
        "outed_date": outedDate?.toIso8601String(),
        "hectors": hectors,
        "remainingQuantity": remainingQuantity,
        "status": status,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}

class PaymentRecord {
    int? id;
    int? userId;
    int? cropId;
    int? landId;
    dynamic utilityId;
    dynamic quantity;
    dynamic hectors;
    dynamic remainingQuantity;
    dynamic perHectorcost;
    dynamic costOfSelectedItem;
    dynamic tenPercentGst;
    dynamic nintyPercentGst;
    dynamic tenPercentPayment;
    dynamic nintyPercentpayment;
    dynamic totalPayment;
    dynamic paymentStatus;
    dynamic amountPaid;
    dynamic tenPercentPrice;
    dynamic ninetyPercentPrice;
    dynamic razorpayOrderId;
    dynamic razorpayPaymentId;
    dynamic razorpaySignature;
    dynamic createdAt;
    dynamic updatedAt;

    PaymentRecord({
        this.id,
        this.userId,
        this.cropId,
        this.landId,
        this.utilityId,
        this.quantity,
        this.hectors,
        this.remainingQuantity,
        this.perHectorcost,
        this.costOfSelectedItem,
        this.tenPercentGst,
        this.nintyPercentGst,
        this.tenPercentPayment,
        this.nintyPercentpayment,
        this.totalPayment,
        this.paymentStatus,
        this.amountPaid,
        this.tenPercentPrice,
        this.ninetyPercentPrice,
        this.razorpayOrderId,
        this.razorpayPaymentId,
        this.razorpaySignature,
        this.createdAt,
        this.updatedAt,
    });

    factory PaymentRecord.fromJson(Map<String, dynamic> json) => PaymentRecord(
        id: json["id"],
        userId: json["userId"],
        cropId: json["cropId"],
        landId: json["landId"],
        utilityId: json["utilityId"],
        quantity: json["quantity"],
        hectors: json["hectors"],
        remainingQuantity: json["remainingQuantity"],
        perHectorcost: json["perHectorcost"],
        costOfSelectedItem: json["costOfSelectedItem"],
        tenPercentGst: json["tenPercentGST"],
        nintyPercentGst: json["nintyPercentGST"],
        tenPercentPayment: json["tenPercentPayment"],
        nintyPercentpayment: json["nintyPercentpayment"],
        totalPayment: json["totalPayment"],
        paymentStatus: json["paymentStatus"],
        amountPaid: json["amountPaid"],
        tenPercentPrice: json["tenPercentPrice"],
        ninetyPercentPrice: json["ninetyPercentPrice"],
        razorpayOrderId: json["razorpay_order_id"],
        razorpayPaymentId: json["razorpay_payment_id"],
        razorpaySignature: json["razorpay_signature"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "cropId": cropId,
        "landId": landId,
        "utilityId": utilityId,
        "quantity": quantity,
        "hectors": hectors,
        "remainingQuantity": remainingQuantity,
        "perHectorcost": perHectorcost,
        "costOfSelectedItem": costOfSelectedItem,
        "tenPercentGST": tenPercentGst,
        "nintyPercentGST": nintyPercentGst,
        "tenPercentPayment": tenPercentPayment,
        "nintyPercentpayment": nintyPercentpayment,
        "totalPayment": totalPayment,
        "paymentStatus": paymentStatus,
        "amountPaid": amountPaid,
        "tenPercentPrice": tenPercentPrice,
        "ninetyPercentPrice": ninetyPercentPrice,
        "razorpay_order_id": razorpayOrderId,
        "razorpay_payment_id": razorpayPaymentId,
        "razorpay_signature": razorpaySignature,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
