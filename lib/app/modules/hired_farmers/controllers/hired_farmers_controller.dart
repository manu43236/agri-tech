import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../hired_farmworkers/data/hired_farmers_farmworkers_model.dart';
import '../../hired_farmworkers/services/hired_farmworkers_repo_impl.dart';

class HiredFarmersController extends BaseController {

  Rx<HiredFarmersFarmworkersModel> hiredFarmersFarmworkersModel = Rx(HiredFarmersFarmworkersModel());

  @override
  void onInit() {
    super.onInit();
    hiredFarmersFarmworkers();
  }

  //get hired farmworkers and farmers API
   Future<void> hiredFarmersFarmworkers() async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result = await HiredFarmworkersRepoImpl(dioClient).hired();
      print('=============== get hired farmers and farmworkers =========');
      print(result);
      result.fold((model) {
        hiredFarmersFarmworkersModel.value=model;
        print(hiredFarmersFarmworkersModel.value.result!.farmers![0].firstName);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
