import 'package:agritech/app/modules/home/services/save_farmer_as_favourite_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../../../../models/save_farmer_as_favourite_model.dart';
import '../../../../models/save_landlord_as_favourite_model.dart';

class SaveFarmerAsFavouriteRepoImpl extends SaveFarmerAsFavouriteRepo with NetworkCheckService{
  final DioClient _dioClient;
  SaveFarmerAsFavouriteRepoImpl(this._dioClient);
  @override
  Future<Either<SaveFarmerAsFavouriteModel,Exception>> saveFarmer({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('save/saveFarmersAsFavorite', Method.post,params: params1);
        return result.fold((l){
          SaveFarmerAsFavouriteModel model=SaveFarmerAsFavouriteModel.fromJson(l.data);
          return Left(model);
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<SaveLandlordAsFavouriteModel,Exception>> saveLandlord({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('save/saveLandlordAsFavorite', Method.post,params: params1);
        return result.fold((l){
          SaveLandlordAsFavouriteModel model=SaveLandlordAsFavouriteModel.fromJson(l.data);
          return Left(model);
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}