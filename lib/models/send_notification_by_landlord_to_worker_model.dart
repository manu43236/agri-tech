// To parse this JSON data, do
//
//     final sendNotificationByLandlordToWorkerModel = sendNotificationByLandlordToWorkerModelFromJson(jsonString);

import 'dart:convert';

SendNotificationByLandlordToWorkerModel sendNotificationByLandlordToWorkerModelFromJson(String str) => SendNotificationByLandlordToWorkerModel.fromJson(json.decode(str));

String sendNotificationByLandlordToWorkerModelToJson(SendNotificationByLandlordToWorkerModel data) => json.encode(data.toJson());

class SendNotificationByLandlordToWorkerModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    SendNotificationByLandlordToWorkerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SendNotificationByLandlordToWorkerModel.fromJson(Map<String, dynamic> json) => SendNotificationByLandlordToWorkerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    Notification? notification;
    String? farmworkerName;

    Result({
        this.notification,
        this.farmworkerName,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        notification: json["notification"] == null ? null : Notification.fromJson(json["notification"]),
        farmworkerName: json["farmworkerName"],
    );

    Map<String, dynamic> toJson() => {
        "notification": notification?.toJson(),
        "farmworkerName": farmworkerName,
    };
}

class Notification {
    bool? isRead;
    int? id;
    dynamic name;
    dynamic message;
    int? from;
    int? to;
    int? jobId;
    dynamic statusForFarmworker;
    dynamic imageUrl;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic statusForLandlord;
    dynamic statusForFarmer;
    dynamic landId;

    Notification({
        this.isRead,
        this.id,
        this.name,
        this.message,
        this.from,
        this.to,
        this.jobId,
        this.statusForFarmworker,
        this.imageUrl,
        this.updatedAt,
        this.createdAt,
        this.statusForLandlord,
        this.statusForFarmer,
        this.landId,
    });

    factory Notification.fromJson(Map<String, dynamic> json) => Notification(
        isRead: json["is_read"],
        id: json["id"],
        name: json["name"],
        message: json["message"],
        from: json["from"],
        to: json["to"],
        jobId: json["jobId"],
        statusForFarmworker: json["status_for_farmworker"],
        imageUrl: json["image_url"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        statusForLandlord: json["status_for_landlord"],
        statusForFarmer: json["status_for_farmer"],
        landId: json["landId"],
    );

    Map<String, dynamic> toJson() => {
        "is_read": isRead,
        "id": id,
        "name": name,
        "message": message,
        "from": from,
        "to": to,
        "jobId": jobId,
        "status_for_farmworker": statusForFarmworker,
        "image_url": imageUrl,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "status_for_landlord": statusForLandlord,
        "status_for_farmer": statusForFarmer,
        "landId": landId,
    };
}
