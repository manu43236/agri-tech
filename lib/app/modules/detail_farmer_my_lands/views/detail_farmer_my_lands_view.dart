import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../../../routes/app_pages.dart';
import '../../home/controllers/home_controller.dart';
import '../../home/controllers/landlord_controller.dart';
import '../controllers/detail_farmer_my_lands_controller.dart';

class DetailFarmerMyLandsView extends GetView<DetailFarmerMyLandsController> {
  const DetailFarmerMyLandsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'detail_my_lands'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () =>controller.apiLandIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): Padding(
          padding: const EdgeInsets.only(right: 20, left: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: colorAsh,
                    borderRadius: BorderRadius.circular(13),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.network(
                      controller.landByIdModel.value.result
                              ?.landDetails?.imageUrl
                              .toString() ??
                          defaultImageUrl,
                      height: Get.height * 0.3,
                      width: Get.width,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        // Display a local asset if the network image fails
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.3,
                          width: Get.width,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${controller.landByIdModel.value.result?.landDetails?.fullName ?? 'Unknown'} ${'at'.tr}',
                            style: TextStyles.kTSFS20W700
                                .copyWith(color: colorDetails),
                          ),
                          Text(
                            '${controller.landByIdModel.value.result?.landDetails?.village ?? ''}, ${controller.landByIdModel.value.result?.landDetails?.district ?? ''}',
                            style: TextStyles.kTSDS14W500
                                .copyWith(color: colorDetails),
                          ),
                        ],
                      ),
                    ),
                    Obx(
                      () => InkWell(
                        onTap: () async {
                          if (controller.isFavourite.value) {
                            await Get.find<HomeController>().updateSavedUser(
                                false,
                                id: controller.deleteId.value,
                                errorMessage: "land_unsaved_successfully".tr);
                            await controller
                                .allLandsSaved(id: controller.id.value);
                            controller.isFavourite.value =
                                false;
                          } else {
                            await Get.find<HomeController>()
                                .saveLandAsFavourite(landId: controller.landId.value);
                            await controller
                                .allLandsSaved(id: controller.id.value);
                            controller.isFavourite.value = true;
                          }
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(11),
                            color: controller.isFavourite.value
                                ? colorBlue.withOpacity(0.5)
                                : colorSave,
                          ),
                          child: Icon(
                            controller.isFavourite.value
                                ? Icons.bookmark
                                : Icons.bookmark_border_outlined,
                            color: colorBlack.withOpacity(0.5),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 15),
                Row(
                  children: [
                    Image.asset(waterCycle, height: 15),
                    const SizedBox(width: 10),
                    Text(
                      controller.landByIdModel.value.result
                                  ?.landDetails?.water ==
                              'No'
                          ? 'not_available'.tr
                          : 'available'.tr,
                      style:
                          TextStyles.kTSFS12W400.copyWith(color: colorDetails),
                    ),
                    const SizedBox(width: 50,),
                    Row(
                      children: [
                        controller.isLoan.value==true?
                        controller.landByIdModel.value.result!.loanAgentStatus!="Request sent to Loan Agent"?
                    InkWell(
                      onTap: () async {
                        await controller.sendLoanNotificationByFarmer(
                            landId: controller.landByIdModel
                                .value.result?.landDetails?.id);
                        controller.getIdLands(id:controller.landByIdModel
                                .value.result?.landDetails?.id);
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 10),
                        decoration: BoxDecoration(
                          color: colorPic,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Text(
                          'avail_loan'.tr,
                          style: TextStyles.kTSFS12W700
                              .copyWith(color: colorWhite),
                        ),
                      ),
                    ):Row(
                      children: [
                        Image.asset(
                          loanSent,
                          height: Get.height*0.04,
                          width: Get.width*0.09,
                        ),
                        SizedBox(width: 5,),
                        Text('applied'.tr,style: TextStyles.kTSDS12W500.copyWith(color: primary),)
                      ],
                    ):controller.isInsurance.value==true?
                    controller.landByIdModel.value.result!.insuranceAgentStatus!="Request sent to Insurance Agent"?
                    InkWell(
                      onTap: () async {
                        await controller.sendFarmerInsuraneNotification(
                            landId: controller.landByIdModel
                                .value.result?.landDetails?.id);
                        controller.getIdLands(id:  controller.landByIdModel
                                .value.result?.landDetails?.id);
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 10),
                        decoration: BoxDecoration(
                          color: colorPic,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Text(
                          'avail_insurance'.tr,
                          style: TextStyles.kTSFS12W700
                              .copyWith(color: colorWhite),
                        ),
                      ),
                    ):Row(
                      children: [
                        Image.asset(
                          loanSent,
                          height: Get.height*0.04,
                          width: Get.width*0.09,
                        ),
                        SizedBox(width: 5,),
                        Text('applied'.tr,style: TextStyles.kTSDS12W500.copyWith(color: primary),)
                      ],
                    ):SizedBox(),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Align(
                  alignment: Alignment.center,
                  child: InkWell(
                    onTap: () async {
                      //await Get.find<SuitableCropsController>().getAllCrops();
                      Get.toNamed(Routes.SUITABLE_CROPS,arguments: {"mandalId":controller.landByIdModel.value.result
                                  ?.landDetails?.mandal,
                                  "landId":controller.landByIdModel
                            .value.result?.landDetails?.id.toString(),
                        "cropId":controller.landByIdModel
                            .value.result?.landDetails?.cropId.toString()
                                  });
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 25),
                      decoration: BoxDecoration(
                        color: colorPic,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text(
                        'suitable_crop'.tr,
                        style:
                            TextStyles.kTSFS16W700.copyWith(color: colorWhite),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        CircleAvatar(
                          radius: 26,
                          backgroundColor: colorAsh,
                          child: controller.homeController.getUserModel.value
                                      .result?.imageUrl !=
                                  null
                              ? ClipOval(
                                  child: Image.network(
                                    Get.find<HomeController>()
                                        .getUserModel
                                        .value
                                        .result!
                                        .imageUrl
                                        .toString(),
                                    fit: BoxFit.cover,
                                    height: Get.height * 0.1,
                                    width: Get.width * 0.2,
                                  ),
                                )
                              : const Icon(Icons.person,
                                  size: 40, color: Colors.grey),
                        ),
                        const SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              controller.homeController.getUserModel.value
                                        .result?.firstName ?? '',
                              style: TextStyles.kTSDS14W700
                                  .copyWith(color: colorPic),
                            ),
                            const SizedBox(height: 5),
                            Text(
                              controller.homeController.getUserModel.value
                                        .result?.address ??'',
                              style: TextStyles.kTSWFS10W700
                                  .copyWith(color: colorDetails),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Text(
                  controller.landByIdModel.value.result
                          ?.landDetails?.description ??
                      '',
                  style: TextStyles.kTSCF12W500,
                ),
                const SizedBox(height: 20),
                controller.landByIdModel.value.result!
                        .farmerDetails!.isNotEmpty
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'hired_farmers'.tr,
                            style: TextStyles.kTSFS16W600
                                .copyWith(color: colorPic),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            height: Get.height * 0.07,
                            width: Get.width,
                            child: ListView.separated(
                              scrollDirection: Axis.horizontal,
                              itemCount: controller.landByIdModel
                                  .value.result!.farmerDetails!.length,
                              separatorBuilder: (context, index) =>
                                  const SizedBox(
                                width: 10,
                              ),
                              itemBuilder: (context, index) {
                                return Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    CircleAvatar(
                                      radius: 26,
                                      backgroundColor: colorAsh,
                                      child: controller
                                                  .landByIdModel
                                                  .value
                                                  .result
                                                  ?.farmerDetails![index]
                                                  .imageUrl !=
                                              null
                                          ? ClipOval(
                                              child: Image.network(
                                                controller
                                                    .landByIdModel
                                                    .value
                                                    .result
                                                    ?.farmerDetails![index]
                                                    .imageUrl!,
                                                fit: BoxFit.cover,
                                                height: Get.height * 0.1,
                                                width: Get.width * 0.2,
                                              ),
                                            )
                                          : const Icon(Icons.person,
                                              size: 40, color: Colors.grey),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          controller
                                                      .landByIdModel
                                                      .value
                                                      .result!
                                                      .farmerDetails !=
                                                  null
                                              ? controller
                                                  .landByIdModel
                                                  .value
                                                  .result!
                                                  .farmerDetails![index]
                                                  .name
                                                  .toString()
                                              : '',
                                          style: TextStyles.kTSDS14W700
                                              .copyWith(color: colorPic),
                                        ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          controller
                                                      .landByIdModel
                                                      .value
                                                      .result!
                                                      .farmerDetails !=
                                                  null
                                              ? controller
                                                  .landByIdModel
                                                  .value
                                                  .result!
                                                  .farmerDetails![index]
                                                  .address
                                                  .toString()
                                              : '',
                                          style: TextStyles.kTSWFS10W700
                                              .copyWith(color: colorDetails),
                                        )
                                      ],
                                    ),
                                  ],
                                );
                              },
                            ),
                          ),
                        ],
                      )
                    : SizedBox(),
                Text(
                  'my_lands'.tr,
                  style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                ),
                const SizedBox(height: 10),
                SizedBox(
                  height: Get.height * 0.08,
                  width: Get.width,
                  child: Obx(() {
                    // Filter the list to exclude the selected land ID
                    final filteredLands = Get.find<LandlordController>()
                        .getAllLandsByFarmerModel
                        .value
                        .result
                        ?.ongoingLands
                        ?.where((land) =>
                            land.id.toString() !=
                            controller.landId.value)
                        .toList();

                    if (filteredLands == null || filteredLands.isEmpty) {
                      return Center(
                        child: Text(
                          'no_lands_available'.tr,
                          style:
                              TextStyles.kTSFS16W600.copyWith(color: colorGrey),
                        ),
                      );
                    }

                    return ListView.separated(
                      itemCount: filteredLands.length,
                      scrollDirection: Axis.horizontal,
                      separatorBuilder: (context, index) =>
                          const SizedBox(width: 10),
                      itemBuilder: (context, index) {
                        final land = filteredLands[index];
                        return InkWell(
                          onTap: () async {
                            // Update landId when tapping on a new land;
                            controller.getIdLands(id:filteredLands[index].id.toString());
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: colorGrey,
                              borderRadius: BorderRadius.circular(11),
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(11),
                              child: Image.network(
                                land.imageUrl.toString(),
                                height: Get.height * 0.09,
                                width: Get.width * 0.2,
                                fit: BoxFit.cover,
                                errorBuilder: (context, error, stackTrace) {
                                  return Image.asset(
                                    defaultImageUrl,
                                    height: Get.height * 0.09,
                                    width: Get.width * 0.2,
                                    fit: BoxFit.cover,
                                  );
                                },
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
