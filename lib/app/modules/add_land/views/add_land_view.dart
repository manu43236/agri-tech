import 'package:agritech/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../core/base/dropdowns/view/district_drop_down.dart';
import '../../../../core/base/dropdowns/view/mandal_drop_down.dart';
import '../../../../core/base/dropdowns/view/state_drop_down.dart';
import '../../../../core/base/dropdowns/view/village_drop_down.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/themes/text_styles.dart';
import '../../home/views/custom_text.dart';
import 'custom_text_form_field.dart';
import '../controllers/add_land_controller.dart';

class AddLandView extends GetView<AddLandController> {
  AddLandView({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  void showPicker(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.photo_library),
                title:  Text('gallery'.tr),
                onTap: () {
                  controller.pickImage(ImageSource.gallery);
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: const Icon(Icons.photo_camera),
                title:  Text('camera'.tr),
                onTap: () {
                  controller.pickImage(ImageSource.camera);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'add_a_land'.tr,
          style: TextStyles.kTSFS24W600
              .copyWith(color: colorDetails, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () {
            Get.offNamed(Routes.DASHBOARD);
          },
        ),
      ),
      // bottomNavigationBar: Get.find<DashboardController>().bottomNav(),
      body: Obx(
        () => SingleChildScrollView(
          padding:
              const EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 0),
          child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CustomText(name: 'land_name'.tr),
                const SizedBox(height: 5),
                CustomTextFormField(
                  controller: controller.nameController.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_name'.tr;
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(name: 'land_in_acres'.tr),
                          const SizedBox(height: 5),
                          CustomTextFormField(
                            controller: controller.landInAcresController.value,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'required_acres'.tr;
                              }
                              return null;
                            },
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 5),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(name: 'land_survey_number'.tr),
                          const SizedBox(height: 5),
                          CustomTextFormField(
                            controller: controller.landSurveyController.value,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'required_survey_number'.tr;
                              }
                              return null;
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                StateDropDown(
                  textEditingController: controller.stateController.value,
                  id: controller.selectedStateIds.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_state'.tr;
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                DistrictDropDown(
                  textEditingController: controller.districtController.value,
                  id: controller.selectedDistrictIds.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_district'.tr;
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                MandalDropDown(
                  textEditingController: controller.mandalController.value,
                  id: controller.selectedMandalIds.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_mandal'.tr;
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                VillageDropDown(
                  textEditingController: controller.villageController.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_village'.tr;
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                CustomText(name: 'description'.tr),
                const SizedBox(height: 5),
                CustomTextFormField(
                  controller: controller.descriptionController.value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'required_description'.tr;
                    }
                    return null;
                  },
                ),
                Obx(
                  () => Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      CustomText(name: 'water_availability'.tr),
                      const SizedBox(width: 10),
                      Radio<String>(
                        value: 'yes'.tr,
                        groupValue: controller.selectedOption.value,
                        onChanged: (value) {
                          controller.setSelectedOption(value!);
                        },
                      ),
                      Text('yes'.tr),
                      const SizedBox(width: 10),
                      Radio<String>(
                        value: 'no'.tr,
                        groupValue: controller.selectedOption.value,
                        onChanged: (value) {
                          controller.setSelectedOption(value!);
                        },
                      ),
                      Text('no'.tr),
                    ],
                  ),
                ),
                CustomText(name: 'image'.tr),
                const SizedBox(height: 5),
                GestureDetector(
                  onTap: () => showPicker(context),
                  child: Container(
                    height: 150,
                    decoration: BoxDecoration(
                      border: Border.all(color: colorTextFieldBorder, width: 2),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Obx(
                      () {
                        final landImage = controller.landImage.value;
                        if (landImage == null) {
                          return Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Icon(Icons.image,
                                    size: 50, color: Colors.grey),
                                Text('upload_image_here'.tr,
                                    style: TextStyles.kTSDS12W600),
                              ],
                            ),
                          );
                        } else {
                          return Stack(
                            children: [
                              Positioned.fill(
                                child: Image.file(landImage, fit: BoxFit.cover),
                              ),
                              Positioned(
                                top: 8,
                                right: 8,
                                child: GestureDetector(
                                  onTap: () => controller.removeImage(),
                                  child: const CircleAvatar(
                                    radius: 15,
                                    backgroundColor: Colors.red,
                                    child: Icon(Icons.close,
                                        size: 20, color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          );
                        }
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Center(
                  child: InkWell(
                    onTap: () {
                      //isFormValid.value = true;
                      print('on tap');
                      if (_formKey.currentState!.validate()) {
                        // If the form is valid, proceed with submission
                        if (controller.landPath.value.isEmpty) {
                          Get.snackbar('error', 'required_to_add_land_image'.tr,
                              snackPosition: SnackPosition.TOP,
                              backgroundColor: colorRed,
                              colorText: colorWhite);
                        } else {
                          print('on tapping');
                          if (controller.role.value == 'Landlord') {
                            print('add land');
                            controller.addLands();
                          } else if (controller.role.value == 'Farmer') {
                            print('add farmer land');
                            controller.addFarmerLands();
                          }
                        }
                      }
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 7),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(11),
                        color: primary,
                      ),
                      child: Text(
                        'add_land'.tr,
                        style:
                            TextStyles.kTSFS18W500.copyWith(color: colorWhite),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
