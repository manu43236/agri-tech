import 'package:get/get.dart';

import '../../available_recent_lands/controllers/available_recent_lands_controller.dart';
import '../../translator_icon/controllers/translator_icon_controller.dart';
import '../controllers/select_region_controller.dart';

class SelectRegionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SelectRegionController>(
      () => SelectRegionController(),
    );
    Get.lazyPut<AvailableRecentLandsController>(
      ()=>AvailableRecentLandsController()
    );
    Get.lazyPut<TranslatorIconController>(
      ()=>TranslatorIconController()
    );
  }
}
