import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../models/filter_jobs_model.dart';
import '../../farm_worker_home_page/services/filter_jobs_repo_impl.dart';
import '../../home/controllers/home_controller.dart';

class AvailableFarmworkerJobsController extends BaseController {

  Rx<FilterJobsModel> filterJobsSearchModel = Rx(FilterJobsModel());

  final HomeController homeController = Get.find<HomeController>();

  var workerJobSearch=''.obs;
  var dataSelectedItem=''.obs;
  var apiWorkerJobsStatus=ApiStatus.LOADING.obs;

  @override
  void onInit() {
    super.onInit();
    filterJobsSearch(state: '',district:'' ,mandal:'' ,village:'' ,search:workerJobSearch.value );
    // homeController.getIdJobs();
  }

  //filter jobs  API
  Future<void> filterJobsSearch({state,district,mandal,village,search}) async {
    apiWorkerJobsStatus.value = ApiStatus.LOADING;
    try {
      var result = await FilterJobsRepoImpl(dioClient).filteredJobs(params1: {
        "longitude": Get.find<HomeController>().longitude.value,
        "latitude": Get.find<HomeController>().latitude.value,
        "village": village,
        "mandal": mandal,
        "district": district,
        "state": state,
        "search":search
      });
      print('longitude:${Get.find<HomeController>().longitude.value}');
      print('latitude:${Get.find<HomeController>().latitude.value}');
      print(
          '===================filter  jobs by location======================');
      print(result);
      result.fold((left) {
        filterJobsLocationHandleResponse(left);
        apiWorkerJobsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiWorkerJobsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print("error$e");
    }
  }

  void filterJobsLocationHandleResponse(FilterJobsModel model) async {
    print('hyyuio');
    if (model.statusCode == 200) {
      print('============model response=====');
      filterJobsSearchModel.value=model;
    }else if(model.statusCode==400){
      print('===============status=============');
      filterJobsSearchModel.value.result!.clear();
      filterJobsSearchModel.value.result ==[];
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
