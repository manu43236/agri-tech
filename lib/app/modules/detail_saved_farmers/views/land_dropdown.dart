import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../controllers/detail_saved_farmers_controller.dart';

class LandDropdown extends GetView<DetailSavedFarmersController> {
  const LandDropdown({super.key});

  void show(BuildContext context) {
    Get.bottomSheet(
      Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.center,
            child: InkWell(
              onTap: () {
                Get.back();
              },
              child: Container(
                  padding: const EdgeInsets.all(5),
                  decoration: const BoxDecoration(
                      color: primary, shape: BoxShape.circle),
                  child: const Icon(
                    Icons.close,
                    size: 25,
                    color: colorWhite,
                  )),
            ),
          ),
          Container(
            height: Get.height * 0.75,
            padding: const EdgeInsets.all(16),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(16),
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Obx(() {
                    final lands = controller
                            .getAllAvailableLandsOfLandlordModel.value.result ??
                        [];
                    if (lands.isEmpty) {
                      return Center(child: Text("no_lands_available".tr));
                    }
                    return GridView.builder(
                      shrinkWrap: true,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        childAspectRatio: 1,
                      ),
                      itemCount: lands.length,
                      itemBuilder: (context, index) {
                        final land = lands[index];
                        return GestureDetector(
                          onTap: () async {
                            // Update selected land details in the controller
                            controller.selectedLand.value = {
                              'name': land.fullName,
                              'image': land.imageUrl ??
                                  agri, 
                            };
                            controller.selectedLandId.value =
                                land.id.toString();
                            print(
                                'selected land id:${controller.selectedLandId.value}');
                            controller.statusForLandlord.value='';
                            await controller.FetchedLandsDetails(userId: controller.id.value,farmerId: controller.farmerId.value);
                            print('status kkk:${controller.statusForLandlord.value}');
                            Get.back(); 
                          },
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              ClipOval(
                                child: Image.network(
                                  land.imageUrl ??
                                      agri, // Replace with your image URL
                                  width: 70,
                                  height: 70,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              const SizedBox(height: 8),
                              Text(
                                land.fullName, // Replace with dynamic title
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  }),
                ),
              ],
            ),
          ),
        ],
      ),
      isScrollControlled: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        await controller
            .addAllLands(userId: controller.id.value); // Fetch lands when the dropdown is tapped
        show(context);
      },
      child: Obx(() {
        final selectedLand = controller.selectedLand.value;
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: primary,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              if (selectedLand != null)
                Expanded(
                  child: Row(
                    children: [
                      ClipOval(
                        child: Image.network(
                          selectedLand['image'] ?? '',
                          width: 25,
                          height: 25,
                          fit: BoxFit.cover,
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: Text(
                          selectedLand['name'] ?? '',
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              else
                Expanded(
                  child:  Text(
                    'select_land'.tr,
                    style:const TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
              SizedBox(
                width: 50,
              ),
              const Icon(Icons.keyboard_arrow_down_rounded,
                  color: Colors.white, size: 20),
            ],
          ),
        );
      }),
    );
  }
}
