// import 'package:agritech/app/modules/notifications/controllers/notifications_controller.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// import '../../../../../core/conts/color_consts.dart';
// import '../../../../../core/themes/text_styles.dart';
// import '../../../../routes/app_pages.dart';

// class NotificationIconView extends GetView<NotificationsController> {
//   const NotificationIconView({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Stack(
//       children: [
//         IconButton(
//           padding: const EdgeInsets.only(right: 30, top: 10),
//           onPressed: () {
//             Get.toNamed(Routes.NOTIFICATIONS);
//           },
//           icon: const Icon(
//             CupertinoIcons.bell,
//             size: 30,
//           ),
//         ),
//         Positioned(
//           top: 10,
//           left: 12,
//           child: Obx(() => 
//                 controller.notificationsCountModel.value.result!.unreadCount>0?
//                 CircleAvatar(
//                 backgroundColor: colorRed,
//                 radius: 8,
//                 child: Text(
//                   controller.notificationsCountModel.value.result!.unreadCount > 9
//                       ? '9+'
//                       : controller.notificationsCountModel.value.result!.unreadCount.toString(),
//                   style: TextStyles.kTSCF12W500,
//                 ),
//               ):const SizedBox()),
//         ),
//       ],
//     );
//   }
// }

import 'package:agritech/app/modules/notifications/controllers/notifications_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/conts/color_consts.dart';
import '../../../../../core/themes/text_styles.dart';
import '../../../../routes/app_pages.dart';

class NotificationIconView extends GetView<NotificationsController> {
  const NotificationIconView({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        IconButton(
          padding: const EdgeInsets.only(right: 30, top: 10),
          onPressed: () {
            print('role selction: ${controller.role.value}');
            controller.getNotifications(userId: controller.id.value);
            Get.toNamed(Routes.NOTIFICATIONS);
          },
          icon: const Icon(
            CupertinoIcons.bell,
            size: 30,
          ),
        ),
        Positioned(
          top: 10,
          left: 12,
          child: Obx(() {
            final unreadCount = controller.notificationsCountModel.value.result?.unreadCount ?? 0;
            return unreadCount > 0
                ? CircleAvatar(
                    backgroundColor: colorRed,
                    radius: 8,
                    child: Text(
                      unreadCount > 9 ? '9+' : unreadCount.toString(),
                      style: TextStyles.kTSCF12W500,
                    ),
                  )
                : const SizedBox();
          }),
        ),
      ],
    );
  }
}
