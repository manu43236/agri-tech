import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../models/get_user_model.dart';
import '../../home/services/get_user_repo_impl.dart';

class HiredFarmersDetailsController extends BaseController {

  Rx<GetUserModel> getUserProfileModel = Rx(GetUserModel());

  var apiUserByProfileIdStatus=ApiStatus.LOADING.obs;

  @override
  void onInit() {
    super.onInit();
    if(Get.arguments!=null){
      getUserByProfileId(id: Get.arguments["id"]);
    }
  }

  // Get user profile by id API
  Future<void> getUserByProfileId({id}) async {
    apiUserByProfileIdStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await GetUserRepoImpl(dioClient).getUser(id: id);
      print("======================== user by id ====================");
      print('Result $result');
      result.fold((model) {
        getUserProfileModel.value = model;

        apiUserByProfileIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiUserByProfileIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
