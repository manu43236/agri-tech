// To parse this JSON data, do
//
//     final getYieldsInHectorModel = getYieldsInHectorModelFromJson(jsonString);

import 'dart:convert';

GetYieldsInHectorModel getYieldsInHectorModelFromJson(String str) => GetYieldsInHectorModel.fromJson(json.decode(str));

String getYieldsInHectorModelToJson(GetYieldsInHectorModel data) => json.encode(data.toJson());

class GetYieldsInHectorModel {
    int? statusCode;
    String? status;
    String? message;
    List<Result>? result;

    GetYieldsInHectorModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetYieldsInHectorModel.fromJson(Map<String, dynamic> json) => GetYieldsInHectorModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    int? userid;
    int? landid;
    int? cropId;
    dynamic outedDate;
    dynamic hectors;
    dynamic createdAt;
    dynamic updatedAt;

    Result({
        this.id,
        this.userid,
        this.landid,
        this.cropId,
        this.outedDate,
        this.hectors,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        userid: json["userid"],
        landid: json["landid"],
        cropId: json["cropId"],
        outedDate: json["outed_date"] == null ? null : DateTime.parse(json["outed_date"]),
        hectors: json["hectors"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userid": userid,
        "landid": landid,
        "cropId": cropId,
        "outed_date": outedDate?.toIso8601String(),
        "hectors": hectors,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
