// To parse this JSON data, do
//
//     final getUtilityByIdModel = getUtilityByIdModelFromJson(jsonString);

import 'dart:convert';

GetUtilityByIdModel getUtilityByIdModelFromJson(String str) => GetUtilityByIdModel.fromJson(json.decode(str));

String getUtilityByIdModelToJson(GetUtilityByIdModel data) => json.encode(data.toJson());

class GetUtilityByIdModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetUtilityByIdModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetUtilityByIdModel.fromJson(Map<String, dynamic> json) => GetUtilityByIdModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    int? utililityId;
    dynamic nameOfInstrument;
    dynamic pricePerDay;
    dynamic brandName;
    dynamic state;
    dynamic district;
    dynamic mandal;
    dynamic village;
    dynamic description;
    int? imageId;
    dynamic image;
    bool? status;
    dynamic createdAt;
    dynamic updatedAt;

    Result({
        this.id,
        this.utililityId,
        this.nameOfInstrument,
        this.pricePerDay,
        this.brandName,
        this.state,
        this.district,
        this.mandal,
        this.village,
        this.description,
        this.imageId,
        this.image,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        utililityId: json["utililityId"],
        nameOfInstrument: json["nameOfInstrument"],
        pricePerDay: json["pricePerDay"],
        brandName: json["brandName"],
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
        description: json["description"],
        imageId: json["image_id"],
        image: json["image"],
        status: json["status"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "utililityId": utililityId,
        "nameOfInstrument": nameOfInstrument,
        "pricePerDay": pricePerDay,
        "brandName": brandName,
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
        "description": description,
        "image_id": imageId,
        "image": image,
        "status": status,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}

