import 'package:agritech/app/modules/utility/data/get_recent_four_utilities_model.dart';
import 'package:agritech/app/modules/utility/services/utility_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:get/get.dart';

import '../data/get_utility_by_user_id_model.dart';

class UtilityController extends BaseController {
  Rx<GetUtilityByUserIdModel> getUtilityByUserIdModel =
      Rx(GetUtilityByUserIdModel());
  Rx<GetRecentFourUtilitiesModel> getRecentFourUtilitiesModel =
      Rx(GetRecentFourUtilitiesModel());

  var utilityId = "".obs;
  var mainUtilSearch = ''.obs;
  var isUtility = false.obs;

  var id=''.obs;

  var apiUserUtilityStatus=ApiStatus.LOADING.obs;

  RxInt currentPage = 0.obs;

  @override
  void onInit() async{
    id.value= await SecureStorage().readData(key: "id");
    super.onInit();
   // userUtilities(userId:id.value , search: '',state: '',district: '',mandal: '',village: '');
    recentUtilities(search: mainUtilSearch.value);
  }

  //user utilities API
  Future<void> userUtilities({userId,search,state,district,mandal,village}) async {
    apiUserUtilityStatus.value = ApiStatus.LOADING;

    try {
      var result = await UtilityRepoImpl(dioClient).getUtilities(userId: userId, search: search,state: state,district: district,mandal: mandal,village: village);
      print('=============== get user utilities =========');
      print(result);
      result.fold((model) {
        if (model.statusCode == 200) {
          getUtilityByUserIdModel.value = model;
          print(getUtilityByUserIdModel.value.result!.first.brandName);
        } 
        apiUserUtilityStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiUserUtilityStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //recent four utilities API
  Future<void> recentUtilities({search}) async {
    apiStatus.value = ApiStatus.LOADING;
    currentPage.value = 0;
    try {
      var result = await UtilityRepoImpl(dioClient).recentUtilities(search: search);
      print('=============== get recent utilities =========');
      print(result);
      result.fold((model) {
        getRecentFourUtilitiesModel.value = model;
        print(getRecentFourUtilitiesModel.value.result!.first.brandName);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
