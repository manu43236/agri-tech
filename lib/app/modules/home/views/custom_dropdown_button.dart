
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/select_region/data/district_model.dart';
import 'package:agritech/app/modules/select_region/data/mandal_model.dart';
import 'package:agritech/app/modules/select_region/data/village_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../select_region/data/state_model.dart';

class StateDropdownButton extends GetView<HomeController> {
  final List<StateResult> items;
  final StateResult? selectedValue;
  final ValueChanged<StateResult?> onChanged;
  final TextEditingController ccontroller;

  const StateDropdownButton({
    Key? key,
    required this.items,
    required this.selectedValue,
    required this.onChanged,
    required this.ccontroller
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("State Selection");
    print(selectedValue);
    return 
    Container(
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
      decoration: BoxDecoration(
        border: Border.all(color: colorTextFieldBorder, width: 2),
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<StateResult>(
          isExpanded: true,
          value:  items.contains(selectedValue) ? selectedValue : null,
          elevation: 16,
          style: const TextStyle(color: colorBlack),
           onChanged: (newValue) {
            onChanged(newValue);
            ccontroller.text = newValue?.name ??'' ; // Update the controller with the selected value
          },
          items: items.map<DropdownMenuItem<StateResult>>((value) {
            return  DropdownMenuItem(
              value: value,
              child: Text( value.name!),
            );
          }).toList()
         
        ),
      ),
    );
    
  }
}

class DistrictDropdownButton extends GetView<HomeController> {
  final List<DistrictResult> items;
  final DistrictResult? selectedValue;
  final ValueChanged<DistrictResult?> onChanged;
  final TextEditingController ccontroller;

  const DistrictDropdownButton({
    Key? key,
    required this.items,
    required this.selectedValue,
    required this.onChanged,
    required this.ccontroller
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("District Selection");
    print(selectedValue);
    return 
    Container(
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
      decoration: BoxDecoration(
        border: Border.all(color: colorTextFieldBorder, width: 2),
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<DistrictResult>(
          isExpanded: true,
          value: items.contains(selectedValue) ? selectedValue : null,
          elevation: 16,
          style: const TextStyle(color: colorBlack),
           onChanged: (newValue) {
            onChanged(newValue);
             if (newValue != null) {
              ccontroller.text = newValue.name!;
            }
            //ccontroller.text = newValue!.districtName!;
          },
          items: items.map<DropdownMenuItem<DistrictResult>>((value) {
            return  DropdownMenuItem(
              value: value,
              child: Text( value.name!),
            );
          }).toList()
         
        ),
      ),
    );
    
  }
}

class MandalDropdownButton extends GetView<HomeController> {
  final List<MandalResult> items;
  final String? selectedValue;
  final ValueChanged<MandalResult?> onChanged;
  final TextEditingController ccontroller;

  const MandalDropdownButton({
    Key? key,
    required this.items,
    required this.selectedValue,
    required this.onChanged,
    required this.ccontroller
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("State Selection");
    print(selectedValue);
    return 
    Container(
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
      decoration: BoxDecoration(
        border: Border.all(color: colorTextFieldBorder, width: 2),
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<MandalResult>(
          isExpanded: true,
          //value: selectedValue!.isEmpty ?  null : selectedValue!,
          elevation: 16,
          style: const TextStyle(color: colorBlack),
           onChanged: (newValue) {
            onChanged(newValue);
            ccontroller.text = newValue!.name! ; // Update the controller with the selected value
          },
          //onChanged: onChanged,
          items: items.map<DropdownMenuItem<MandalResult>>((value) {
            return  DropdownMenuItem(
              value: value,
              child: Text( value.name!),
            );
          }).toList()
         
        ),
      ),
    );
    
  }
}



class VillageDropdownButton extends GetView<HomeController> {
  final List<VillageResult> items;
  final String? selectedValue;
  final ValueChanged<VillageResult?> onChanged;
  final TextEditingController ccontroller;

  const VillageDropdownButton({
    Key? key,
    required this.items,
    required this.selectedValue,
    required this.onChanged,
    required this.ccontroller
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("State Selection");
    print(selectedValue);
    return 
    Container(
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
      decoration: BoxDecoration(
        border: Border.all(color: colorTextFieldBorder, width: 2),
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<VillageResult>(
          isExpanded: true,
          //value: selectedValue!.isEmpty ?  null : selectedValue!,
          elevation: 16,
          style: const TextStyle(color: colorBlack),
           onChanged: (newValue) {
            onChanged(newValue);
            ccontroller.text = newValue!.name! ; // Update the controller with the selected value
          },
          //onChanged: onChanged,
          items: items.map<DropdownMenuItem<VillageResult>>((value) {
            return  DropdownMenuItem(
              value: value,
              child: Text( value.name!),
            );
          }).toList()
         
        ),
      ),
    );
    
  }
}

