// To parse this JSON data, do
//
//     final districtModel = districtModelFromJson(jsonString);

import 'dart:convert';

DistrictModel districtModelFromJson(String str) => DistrictModel.fromJson(json.decode(str));

String districtModelToJson(DistrictModel data) => json.encode(data.toJson());

class DistrictModel {
    int? statusCode;
    String? status;
    String? message;
    List<DistrictResult>? result;

    DistrictModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory DistrictModel.fromJson(Map<String, dynamic> json) => DistrictModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<DistrictResult>.from(json["result"]!.map((x) => DistrictResult.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class DistrictResult {
    int? id;
    String? name;

    DistrictResult({
        this.id,
        this.name,
    });

    factory DistrictResult.fromJson(Map<String, dynamic> json) => DistrictResult(
        id: json["id"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
    };
}
