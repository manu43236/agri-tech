// To parse this JSON data, do
//
//     final getAllJobsByFarmerModel = getAllJobsByFarmerModelFromJson(jsonString);

import 'dart:convert';

GetAllJobsByFarmerModel getAllJobsByFarmerModelFromJson(String str) => GetAllJobsByFarmerModel.fromJson(json.decode(str));

String getAllJobsByFarmerModelToJson(GetAllJobsByFarmerModel data) => json.encode(data.toJson());

class GetAllJobsByFarmerModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetAllJobsByFarmerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetAllJobsByFarmerModel.fromJson(Map<String, dynamic> json) => GetAllJobsByFarmerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<dynamic>? completedJobs;
    List<OngoingJob>? ongoingJobs;
    List<dynamic>? notStartedJobs;

    Result({
        this.completedJobs,
        this.ongoingJobs,
        this.notStartedJobs,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        completedJobs: json["completedJobs"] == null ? [] : List<dynamic>.from(json["completedJobs"]!.map((x) => x)),
        ongoingJobs: json["ongoingJobs"] == null ? [] : List<OngoingJob>.from(json["ongoingJobs"]!.map((x) => OngoingJob.fromJson(x))),
        notStartedJobs: json["notStartedJobs"] == null ? [] : List<dynamic>.from(json["notStartedJobs"]!.map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "completedJobs": completedJobs == null ? [] : List<dynamic>.from(completedJobs!.map((x) => x)),
        "ongoingJobs": ongoingJobs == null ? [] : List<dynamic>.from(ongoingJobs!.map((x) => x.toJson())),
        "notStartedJobs": notStartedJobs == null ? [] : List<dynamic>.from(notStartedJobs!.map((x) => x)),
    };
}

class OngoingJob {
    int? id;
    dynamic landLordId;
    int? farmerId;
    dynamic fullName;
    dynamic startDate;
    dynamic endDate;
    dynamic latitude;
    dynamic longitude;
    dynamic state;
    dynamic district;
    dynamic village;
    dynamic mandal;
    dynamic description;
    int? imageId;
    dynamic image;
    dynamic status;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;

    OngoingJob({
        this.id,
        this.landLordId,
        this.farmerId,
        this.fullName,
        this.startDate,
        this.endDate,
        this.latitude,
        this.longitude,
        this.state,
        this.district,
        this.village,
        this.mandal,
        this.description,
        this.imageId,
        this.image,
        this.status,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory OngoingJob.fromJson(Map<String, dynamic> json) => OngoingJob(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        startDate: json["startDate"] == null ? null : DateTime.parse(json["startDate"]),
        endDate: json["endDate"] == null ? null : DateTime.parse(json["endDate"]),
        latitude: json["latitude"]?.toDouble(),
        longitude: json["longitude"]?.toDouble(),
        state: json["state"],
        district: json["district"],
        village: json["village"],
        mandal: json["mandal"],
        description: json["description"],
        imageId: json["image_id"],
        image: json["image"],
        status: json["status"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "fullName": fullName,
        "startDate": startDate?.toIso8601String(),
        "endDate": endDate?.toIso8601String(),
        "latitude": latitude,
        "longitude": longitude,
        "state": state,
        "district": district,
        "village": village,
        "mandal": mandal,
        "description": description,
        "image_id": imageId,
        "image": image,
        "status": status,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
