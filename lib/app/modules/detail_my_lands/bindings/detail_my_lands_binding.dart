import 'package:get/get.dart';

import '../../saved_lands/controllers/saved_lands_controller.dart';
import '../controllers/detail_my_lands_controller.dart';

class DetailMyLandsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailMyLandsController>(
      () => DetailMyLandsController(),
    );
    Get.lazyPut<SavedLandsController>(
      () => SavedLandsController(),
    );
  }
}
