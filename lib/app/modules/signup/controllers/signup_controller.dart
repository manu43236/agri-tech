// import 'package:agritech/app/modules/signup/services/get_otp_repo_impl.dart';
// import 'package:agritech/app/routes/app_pages.dart';
// import 'package:agritech/core/base/base_controller.dart';
// import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
// import 'package:agritech/models/get_otp_model.dart';
// import 'package:agritech/models/register_model.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:dio/dio.dart' as dio;
// import '../services/signup_repo_impl.dart';

// class SignupController extends BaseController {
//   //TODO: Implement SignupController

//     Rx<TextEditingController> mobileController = Rx(TextEditingController());

//   @override
//   void onInit() {
//     super.onInit();
//   }

//   @override
//   void onReady() {
//     super.onReady();
//   }

//     void doRegister() async {
//     if (mobileController.value.text.isNotEmpty) {
//       try {

//         dio.FormData formData = dio.FormData.fromMap({
//           "mobileNumber": mobileController.value.text,
//         });
//         await SecureStorage().writeStringData("mobileNumber",mobileController.value.text);
//         var result = await SignUpRepoImpl(dioClient).doSignup(params1: formData);
//         result.fold((left) {
//         _hanldeResponse(left);
//         }, (r) {
//           apiStatus.value = ApiStatus.FAIL;
//         });
//       } catch (e) {
//         print('Error');
//       }
//     }
//   }

// _hanldeResponse(RegisterModel model) async{
//   if(model.statusCode.toString() == '200'){
//      await SecureStorage().writeStringData("id",model.result!.id.toString());
//      doGetOtp();
//      Get.toNamed(Routes.OTP);
//   } else if(model.message == 'Phone Number already exists'){
//     doGetOtp();
//     Get.toNamed(Routes.OTP);
//   }

//   }

//   void doGetOtp() async {
//       try {
//         await SecureStorage().writeStringData("mobileNumber",mobileController.value.text);
//         var result = await GetOtpRepoImpl(dioClient).getOtp(params1:{
//           "mobileNumber": mobileController.value.text,
//         });
//         result.fold((left) {
//         hanldeResponse(left);
//         }, (r) {
//           apiStatus.value = ApiStatus.FAIL;
//         });
//       } catch (e) {
//         print('Error');
//       }
//   }

//   hanldeResponse(GetOtpModel model) async{
//   if(model.statusCode == 200){

//   }
// }

//   @override
//   void onClose() {
//     super.onClose();
//   }

// }

import 'package:agritech/app/modules/signup/services/get_otp_repo_impl.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:agritech/models/get_otp_model.dart';
import 'package:agritech/models/register_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart' as dio;
import '../services/signup_repo_impl.dart';

class SignupController extends BaseController {
  Rx<TextEditingController> mobileController = Rx(TextEditingController());
  var otpValue = "".obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void doRegister() async {
    if (mobileController.value.text.isNotEmpty) {
      try {
        dio.FormData formData = dio.FormData.fromMap({
          "mobileNumber": mobileController.value.text,
        });

        var result =
            await SignUpRepoImpl(dioClient).doSignup(params1: formData);
        result.fold((left) {
          _handleRegisterResponse(left);
        }, (r) {
          apiStatus.value = ApiStatus.FAIL;
        });
      } catch (e) {
        print('Error: $e');
      }
    }
  }

  void _handleRegisterResponse(RegisterModel model) async {
    if (model.statusCode == 200) {
      await SecureStorage().writeStringData("id", model.result!.id.toString());

      doGetOtp();
    } else if (model.statusCode == 400) {
      Get.snackbar('Failed', 'Phone number already exists',
          backgroundColor: colorRed, colorText: colorWhite);
    } else {
      apiStatus.value = ApiStatus.FAIL;
      print('Registration failed: ${model.message}');
      // doGetOtp();
    }
  }

  void doGetOtp() async {
    try {
      var result = await GetOtpRepoImpl(dioClient).getOtp(params1: {
        "mobileNumber": mobileController.value.text,
      });
      result.fold((left) {
        _handleOtpResponse(left);
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void _handleOtpResponse(GetOtpModel model) async {
    if (model.statusCode == 200) {
      otpValue.value = model.result!.user!.otp.toString();
      Get.offNamed(Routes.OTP, arguments: {
        'isLogin': false,
        'otp': otpValue.value,
        'mobileNumber': mobileController.value.text
      });
      print('OTP sent successfully');
    } else {
      apiStatus.value = ApiStatus.FAIL;
      print('Failed to send OTP: ${model.message}');
    }
  }

  @override
  void onClose() {
    super.onClose();
    mobileController.value.dispose();
  }
}
