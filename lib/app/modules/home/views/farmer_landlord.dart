import 'package:agritech/app/modules/home/controllers/farmer_landlord_controller.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../../../../core/utils/widget_utils/text_fields/custom_search_widget.dart';

class FarmerLandlord extends GetView<FarmerLandlordController> {
  const FarmerLandlord({super.key});

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    final imageSize = Get.width * 0.15;
    final innerCircleSize = Get.width * 0.14;

    return Obx(
      () => RefreshIndicator(
        onRefresh: () async{
          controller.farmerLandlordSearch.value='';
          controller.filterLandlords(latitude: Get.find<HomeController>().latitude.value,longitude: Get.find<HomeController>().longitude.value,search: controller.farmerLandlordSearch.value);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Text(
                'available_landlords'.tr,
                style: TextStyles.kTSFS14W600
                    .copyWith(color: colorHello, fontWeight: FontWeight.w500),
              ),
            ),
            const SizedBox(height: 10,),
            Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: CustomSearchField(
                    onChanged: (val) {
                      controller.farmerLandlordSearch.value=val;
                      if(val.length>=3){
                        controller.filterLandlords(latitude: Get.find<HomeController>().latitude.value,longitude: Get.find<HomeController>().longitude.value,search: controller.farmerLandlordSearch.value);
                      }else if(val.length==0){
                        controller.filterLandlords(latitude: Get.find<HomeController>().latitude.value,longitude: Get.find<HomeController>().longitude.value,search: controller.farmerLandlordSearch.value);
                      }
                    },
                  ),
          ),
            Expanded(
                child:  controller.apiStatus.value == ApiStatus.LOADING
                  ? Shimmers().getListShimmer()
                  :controller.filterNearLandlordModel.value.result?.landlords
                            ?.isNotEmpty ??
                        false
                    ? ListView.builder(
                        itemCount: controller.filterNearLandlordModel.value
                            .result!.landlords!.length,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 20),
                        itemBuilder: (context, index) {
                          final farmer = controller.filterNearLandlordModel.value
                              .result!.landlords![index];
                          return InkWell(
                            onTap: () async {
                              // final homeController = Get.find<HomeController>();
                              // homeController.landlordId.value = farmer.id.toString();
                              // await homeController.getUserByProfileId(homeController.landlordId.value);
                              // homeController.selectedLandlordId.value = homeController.getUserProfileModel.value.result?.id.toString() ?? '';
                              // homeController.landlordId.value = farmer.id.toString();
                              // await Get.find<LandlordController>().addAllLands();
                              // Get.toNamed(Routes.DETAILS_OF_LANDLORD);
                              Get.find<HomeController>().landlordId.value =
                                  controller.filterNearLandlordModel.value.result!
                                      .landlords![index].id
                                      .toString();
        
                              Get.toNamed(Routes.DETAILS_OF_LANDLORD, arguments: {
                                "userId":
                                    Get.find<HomeController>().landlordId.value
                              });
                            },
                            child: Container(
                              margin: const EdgeInsets.symmetric(vertical: 10),
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(19),
                                boxShadow: [
                                  BoxShadow(
                                    color: colorBlack.withOpacity(0.2),
                                    blurRadius: 5,
                                    spreadRadius: 1,
                                    offset: const Offset(0, 3),
                                    blurStyle: BlurStyle.inner,
                                  ),
                                ],
                                color: colorWhite,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        Container(
                                          height: imageSize,
                                          width: imageSize,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: colorCircleFarmer,
                                          ),
                                        ),
                                        Positioned(
                                          left: 2,
                                          child: Container(
                                            height: innerCircleSize,
                                            width: innerCircleSize,
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: colorWhite,
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          child: Container(
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: colorCircleFarmer,
                                            ),
                                            child: ClipOval(
                                              child: Image.network(
                                                farmer.imageUrl ??
                                                    defaultImageUrl,
                                                height: imageSize - 5,
                                                width: imageSize - 5,
                                                fit: BoxFit.cover,
                                                errorBuilder:
                                                    (context, error, stackTrace) {
                                                  return Image.asset(
                                                    defaultImageUrl,
                                                    height: imageSize - 5,
                                                    width: imageSize - 5,
                                                    fit: BoxFit.cover,
                                                  );
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(width: 30),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          farmer.firstName ?? '-',
                                          style: TextStyles.kTSCF12W500,
                                        ),
                                        Text(
                                          farmer.age?.toString() ?? '-',
                                          style: TextStyles.kTSCF12W500,
                                        ),
                                        Text(
                                          farmer.address ?? '-',
                                          style: TextStyles.kTSCF12W500,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      )
                    : Center(
                        child: Text('no_landlords_found'.tr,
                            style: TextStyles.kTSDS12W500
                                .copyWith(color: colorGrey)))),
          ],
        ),
      ),
    );
  }
}
