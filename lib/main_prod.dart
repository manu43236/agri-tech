import 'package:agritech/core/config/envs/prod_env.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'agri_tech_app.dart';
import 'core/utils/storage_utils/secure_storage.dart';

void main() {
  SecureStorage().loadSecureStorage(const FlutterSecureStorage());
  runApp(
    AgriTechApp(env: ProdEnv()),
  );
}
