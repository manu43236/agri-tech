import 'package:get/get.dart';

import '../../home/controllers/landlord_controller.dart';
import '../../select_region/controllers/select_region_controller.dart';
import '../controllers/available_recent_lands_controller.dart';

class AvailableRecentLandsBinding extends Bindings {
  @override
  void dependencies() {
         Get.lazyPut<SelectRegionController>(
      ()=>SelectRegionController(),fenix: true
    );
    Get.lazyPut<AvailableRecentLandsController>(
      () => AvailableRecentLandsController(),
    );
    Get.lazyPut<LandlordController>(
      ()=>LandlordController()
    );

  }
}
