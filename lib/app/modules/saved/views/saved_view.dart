import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/saved_controller.dart';

class SavedView extends GetView<SavedController> {
  const SavedView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              SizedBox(width: Get.width, child: Image.asset(profileLogo,fit: BoxFit.cover,)),
              Positioned(
                top: 45, // Adjust the top position as needed
                left: 15, // Adjust the left position as needed
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                      icon: const Icon(Icons.arrow_back),
                      onPressed: () {
                        Get.back(); // Back navigation logic
                      },
                    ),
                    const SizedBox(width: 80), // Space between the icon and text
                    Text(
                      'saved'.tr,
                      style: TextStyles.kTSFS26W400,
                    ),
                  ],
                ),
              ),
            ],
          ),
          Get.find<HomeController>().role.value == 'Landlord'
              ? const SizedBox(height: 30)
              : const SizedBox(height: 0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (Get.find<HomeController>().role.value == 'Landlord') ...[
                  InkWell(
                    onTap: () {
                      //controller.allFarmersSaved();
                      Get.toNamed(Routes.SAVED_FARMERS);
                    },
                    child: Text(
                      'farmers'.tr,
                      style: TextStyles.kTSNFS17W400.copyWith(color: colorSaved),
                    ),
                  ),
                  const SizedBox(height: 20),
                  InkWell(
                    onTap: () {
                      //controller.allFarmworkersSaved();
                      Get.toNamed(Routes.SAVED_FARMWORKERS);
                    },
                    child: Text(
                      'farmworkers'.tr,
                      style: TextStyles.kTSNFS17W400.copyWith(color: colorSaved),
                    ),
                  ),
                  const SizedBox(height: 20),
                  InkWell(
                    onTap: () {
                      //controller.allLandsSaved();
                      Get.toNamed(Routes.SAVED_LANDS);
                    },
                    child: Text(
                      'my_lands'.tr,
                      style: TextStyles.kTSNFS17W400.copyWith(color: colorSaved),
                    ),
                  ),
                  const SizedBox(height: 20),
                  InkWell(
                    onTap: () {
                      Get.toNamed(Routes.SAVED_UTILITIES);
                    },
                    child: Text(
                      'my_utilities'.tr,
                      style: TextStyles.kTSNFS17W400.copyWith(color: colorSaved),
                    ),
                  ),
                ] else if (Get.find<HomeController>().role.value == 'Farmer') ...[
                  InkWell(
                    onTap: () {
                      //controller.getAllLandlordsSaved();
                      Get.toNamed(Routes.SAVED_LANDLORDS);
                    },
                    child: Text(
                      'landlords'.tr,
                      style: TextStyles.kTSNFS17W400.copyWith(color: colorSaved),
                    ),
                  ),
                  const SizedBox(height: 20),
                  InkWell(
                    onTap: () {
                      //controller.allFarmworkersSaved();
                      Get.toNamed(Routes.SAVED_FARMWORKERS);
                    },
                    child: Text(
                      'farmworkers'.tr,
                      style: TextStyles.kTSNFS17W400.copyWith(color: colorSaved),
                    ),
                  ),
                  const SizedBox(height: 20),
                  InkWell(
                    onTap: () {
                      //controller.allLandsSaved();
                      Get.toNamed(Routes.SAVED_LANDS);
                    },
                    child: Text(
                      'my_lands'.tr,
                      style: TextStyles.kTSNFS17W400.copyWith(color: colorSaved),
                    ),
                  ),
                  const SizedBox(height: 20),
                  InkWell(
                    onTap: () {
                      Get.toNamed(Routes.SAVED_UTILITIES);
                    },
                    child: Text(
                      'my_utilities'.tr,
                      style: TextStyles.kTSNFS17W400.copyWith(color: colorSaved),
                    ),
                  ),
                ] else if (Get.find<HomeController>().role.value == 'Farm Worker') ...[
                  InkWell(
                    onTap: () {
                      //controller.getAllSavedJobs();
                      Get.toNamed(Routes.SAVED_JOBS);
                    },
                    child: Text(
                      'my_jobs'.tr,
                      style: TextStyles.kTSNFS17W400.copyWith(color: colorSaved),
                    ),
                  ),
                  const SizedBox(height: 20),
                  InkWell(
                    onTap: () {
                      Get.toNamed(Routes.SAVED_UTILITIES);
                    },
                    child: Text(
                      'my_utilities'.tr,
                      style: TextStyles.kTSNFS17W400.copyWith(color: colorSaved),
                    ),
                  ),
                ],
              ],
            ),
          ),
        ],
      ),
    );
  }
}
