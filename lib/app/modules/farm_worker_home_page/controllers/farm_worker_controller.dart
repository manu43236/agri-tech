import 'package:agritech/app/modules/farm_worker_home_page/services/filter_jobs_repo_impl.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:get/get.dart';
import '../../../../core/base/base_controller.dart';
import '../../../../models/filter_jobs_model.dart';
import '../data/get_skills_by_user_id_model.dart';

class FarmWorkerHomePageController extends BaseController {
  Rx<FilterJobsModel> filterJobsByModel = Rx(FilterJobsModel());
  Rx<GetSkillsByUserIdModel> getSkillsByUserIdModel =
      Rx(GetSkillsByUserIdModel());

  var isJobs = false.obs;
  var workerJobSearch=''.obs;
  final HomeController homeController = Get.find<HomeController>();
  var jobId = "".obs;
  var statusForLandlord = "".obs;

  var apiStatusLoad = ApiStatus.LOADING.obs;

  @override
  void onInit() {
    super.onInit();
    print('filter home page');
  }

  //filter jobs  API
  Future<void> filterJobs({state,district,mandal,village,search}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await FilterJobsRepoImpl(dioClient).filteredJobs(params1: {
        "longitude": Get.find<HomeController>().longitude.value,
        "latitude": Get.find<HomeController>().latitude.value,
        "village": village,
        "mandal": mandal,
        "district": district,
        "state": state,
        "search": search
      });
      print('longitude:${Get.find<HomeController>().longitude.value}');
      print('latitude:${Get.find<HomeController>().latitude.value}');
      print(
          '===================filter  jobs by location======================');
      print(result);
      result.fold((left) {
        filterJobsLocationHandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print("error$e");
    }
  }

  void filterJobsLocationHandleResponse(FilterJobsModel model) async {
    print('hyyuio');
    if (model.statusCode == 200) {
      print('============model response=====');

      // Initialize `filterJobsByModel.value.result` if it is null
      filterJobsByModel.value.result ??= [];
      filterJobsByModel.value.result!.clear();

      // Add the result from the model to `filterJobsByModel`
      for (var element in model.result!) {
        filterJobsByModel.value.result!.add(element);
      }

      // Update the value so observers can react to the change
      filterJobsByModel.refresh();
      print('========done==========');
    }
  }

  // get user by id skills API
  Future<void> getUserByIdSkills({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await FilterJobsRepoImpl(dioClient).getUserSkills(userId: userId);
      print("===================== get user skills ==============");
      print(result);
      result.fold((model) {
        getSkillsByUserIdModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
