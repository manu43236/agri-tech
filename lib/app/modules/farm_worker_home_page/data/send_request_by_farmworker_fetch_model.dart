// To parse this JSON data, do
//
//     final sendRequestByFarmworkerFetchModel = sendRequestByFarmworkerFetchModelFromJson(jsonString);

import 'dart:convert';

SendRequestByFarmworkerFetchModel sendRequestByFarmworkerFetchModelFromJson(String str) => SendRequestByFarmworkerFetchModel.fromJson(json.decode(str));

String sendRequestByFarmworkerFetchModelToJson(SendRequestByFarmworkerFetchModel data) => json.encode(data.toJson());

class SendRequestByFarmworkerFetchModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    SendRequestByFarmworkerFetchModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SendRequestByFarmworkerFetchModel.fromJson(Map<String, dynamic> json) => SendRequestByFarmworkerFetchModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    RecipientDetails? recipientDetails;
    List<JobDetail>? jobDetails;

    Result({
        this.recipientDetails,
        this.jobDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        recipientDetails: json["recipientDetails"] == null ? null : RecipientDetails.fromJson(json["recipientDetails"]),
        jobDetails: json["jobDetails"] == null ? [] : List<JobDetail>.from(json["jobDetails"]!.map((x) => JobDetail.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "recipientDetails": recipientDetails?.toJson(),
        "jobDetails": jobDetails == null ? [] : List<dynamic>.from(jobDetails!.map((x) => x.toJson())),
    };
}

class JobDetail {
    int? jobId;
    dynamic description;
    dynamic village;
    dynamic district;
    dynamic state;
    dynamic createdAt;
    dynamic statusForLandlord;
    dynamic statusForFarmer;

    JobDetail({
        this.jobId,
        this.description,
        this.village,
        this.district,
        this.state,
        this.createdAt,
        this.statusForLandlord,
        this.statusForFarmer,
    });

    factory JobDetail.fromJson(Map<String, dynamic> json) => JobDetail(
        jobId: json["jobId"],
        description: json["description"],
        village: json["village"],
        district: json["district"],
        state: json["state"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        statusForLandlord: json["status_for_landlord"],
        statusForFarmer: json["status_for_farmer"],
    );

    Map<String, dynamic> toJson() => {
        "jobId": jobId,
        "description": description,
        "village": village,
        "district": district,
        "state": state,
        "createdAt": createdAt?.toIso8601String(),
        "status_for_landlord": statusForLandlord,
        "status_for_farmer": statusForFarmer,
    };
}

class RecipientDetails {
    int? id;
    dynamic name;
    dynamic role;
    dynamic imageUrl;

    RecipientDetails({
        this.id,
        this.name,
        this.role,
        this.imageUrl,
    });

    factory RecipientDetails.fromJson(Map<String, dynamic> json) => RecipientDetails(
        id: json["id"],
        name: json["name"],
        role: json["role"],
        imageUrl: json["imageUrl"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "role": role,
        "imageUrl": imageUrl,
    };
}
