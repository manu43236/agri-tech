import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../detail_recent_lands/data/land_by_id_model.dart';
import '../../home/controllers/home_controller.dart';
import '../../home/services/land_by_id_repo_impl.dart';
import '../../saved/data/get_all_saved_lands_model.dart';
import '../../saved/services/saved_repo_impl.dart';
import '../data/send_notification_for_loan_model.dart';
import '../services/detail_my_lands_repo_impl.dart';

class DetailMyLandsController extends BaseController {

  Rx<SendNotificationForLoanModel>
      sendNotificationForLoanModel =
      Rx(SendNotificationForLoanModel());
  Rx<AllSavedLandsModel> allSavedLandsModel = Rx(AllSavedLandsModel());
  Rx<LandByIdModel> landByIdModel = Rx(LandByIdModel());

  final HomeController homeController = Get.find<HomeController>();
  var isLoan=false.obs;
  var isInsurance=false.obs;
  var id=''.obs;

  var apiLandIdStatus = ApiStatus.LOADING.obs;
  var isFavourite = false.obs;
  var deleteId=0.obs;
  var landId = "".obs;

  @override
  void onInit() async{
    super.onInit();
    id.value= await SecureStorage().readData(key: "id"??'');
    if(Get.arguments!=null){
      isLoan.value=Get.arguments["isLoan"];
      isInsurance.value=Get.arguments["isInsurance"];
      await allLandsSaved(id: id.value);
      await getIdLands(id: Get.arguments["landId"]);
    }
  }

   //get all saved lands API
  Future<void> allLandsSaved({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient)
          .allSavedLands(userId: id);
      print("===================== get all saved lands ==============");
      print(result);
      result.fold((model) {
        allSavedLandsModel.value = model;
        print(allSavedLandsModel.value.result!.landCount!);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  // Lands BY id API
  Future<void> getIdLands({id}) async {
    apiLandIdStatus.value = ApiStatus.LOADING;
    try {
      var result = await LandByIdRepoImpl(dioClient).getLand(params1: {
        "id": id,
      });

      result.fold((model) {
        landByIdModel.value = model;
        landId.value = model.result!.landDetails!.id.toString() ?? "";
        if (model.statusCode == 200) {
          isFavourite.value = false;
          if (allSavedLandsModel
                      .value
                      .result !=
                  null &&
              allSavedLandsModel
                      .value
                      .result!
                      .savedLands !=
                  null) {
            for (var element in allSavedLandsModel
                .value
                .result!
                .savedLands!) {
              if (element.landid == model.result!.landDetails!.id) {
                deleteId.value = element.id!;
                isFavourite.value = true;
              }
            }
          }
        }
        apiLandIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiLandIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

   //send Loan notification API
  Future<void> sendLoanNotification({landId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailMyLandsRepoImpl(dioClient)
          .sendLoanNotification(params1: {
        "id": Get.find<HomeController>().userId.value,
        "LoanAgentId": 48,
        "landId": landId
      });
      print('land id is:${landId}');
      print('user id is:${Get.find<HomeController>().userId.value}');
      print(
          "======================== sent loan notification ====================");
      print(result);
      result.fold((model) {
        sendNotificationForLoanModel.value = model;
        if (sendNotificationForLoanModel.value.statusCode == 200) {
          Get.defaultDialog(
          title: "Success",
          middleText: "Your request has been sent to the loan agent. Please wait for further processing.",
          middleTextStyle:TextStyle(fontSize: 12,color: colorBlack),
          textConfirm: "OK",
          buttonColor: primary,
          confirmTextColor: colorWhite,
          onConfirm: () {
            Get.back(); // Close the dialog
          },
          titleStyle: TextStyle(color: colorGreen),
        );
        } else if (sendNotificationForLoanModel.value.statusCode == 400) {
         Get.defaultDialog(
          title: "Alert",
          middleText: "Your request has already been sent to the loan agent. Please wait for further processing.",
          middleTextStyle:TextStyle(fontSize: 12,color: colorBlack),
          textConfirm: "OK",
          buttonColor: colorRed,
          confirmTextColor: colorWhite,
          onConfirm: () {
            Get.back(); // Close the dialog
          },
          titleStyle: TextStyle(color: colorRed),
        );
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  
   //send Insurance notification API
  Future<void> sendInsuraneNotification({landId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailMyLandsRepoImpl(dioClient)
          .sendLoanNotification(params1: {
        "id": Get.find<HomeController>().userId.value,
        "insurance_agent_id":6,
        "landId": landId
      });
      print('land id is:${landId}');
      print('user id is:${Get.find<HomeController>().userId.value}');
      print(
          "======================== sent insurance notification ====================");
      print(result);
      result.fold((model) {
        sendNotificationForLoanModel.value = model;
        if (sendNotificationForLoanModel.value.statusCode == 200) {
          Get.defaultDialog(
          title: "Success",
          middleText: "Your request has been sent to the Insurance agent. Please wait for further processing.",
          middleTextStyle:TextStyle(fontSize: 12,color: colorBlack),
          textConfirm: "OK",
          buttonColor: primary,
          confirmTextColor: colorWhite,
          onConfirm: () {
            Get.back(); // Close the dialog
          },
          titleStyle: TextStyle(color: colorGreen),
        );
        } else if (sendNotificationForLoanModel.value.statusCode == 400) {
          Get.defaultDialog(
          title: "Alert",
          middleText: "Your request has already been sent to the Insurance agent. Please wait for further processing.",
          middleTextStyle:TextStyle(fontSize: 12,color: colorBlack),
          textConfirm: "OK",
          buttonColor: colorRed,
          confirmTextColor: colorWhite,
          onConfirm: () {
            Get.back(); // Close the dialog
          },
          titleStyle: TextStyle(color: colorRed),
        );
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }


  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
