import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:get/get.dart';

import '../../saved/data/get_all_saved_landlord_model.dart';
import '../../saved/services/saved_repo_impl.dart';

class SavedLandlordsController extends BaseController {

  Rx<GetAllSavedLandlordModel> getAllSavedLandlordModel =
      Rx(GetAllSavedLandlordModel());

  var id=''.obs;

  @override
  void onInit() async{
    super.onInit();
    id.value= await SecureStorage().readData(key: "id"??'');
    await getAllLandlordsSaved(userId: id.value);
  }

    //get all saved landlords API
  Future<void> getAllLandlordsSaved({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient).savedLandlord(userId: userId);
      print('=================get saved landlords=============');
      print(result);
      result.fold(
        (model) {
          getAllSavedLandlordModel.value = model;
          apiStatus.value = ApiStatus.SUCCESS;
        },(r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
