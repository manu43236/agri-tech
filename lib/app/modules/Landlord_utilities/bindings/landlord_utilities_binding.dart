import 'package:agritech/app/modules/Landlord_utilities/controllers/landlord_utilities_controller.dart';
import 'package:agritech/app/modules/saved_utilities/controllers/saved_utilities_controller.dart';
import 'package:get/get.dart';

import '../../instrument_details/controllers/instrument_details_controller.dart';

class LandlordUtilitiesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LandlordUtilitiesController>(
      () => LandlordUtilitiesController(),
    );
    Get.lazyPut<InstrumentDetailsController>(
      () => InstrumentDetailsController(),
    );
    Get.lazyPut<SavedUtilitiesController>(
      () => SavedUtilitiesController(),
    );
  }
}
