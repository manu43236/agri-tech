import 'package:agritech/app/modules/farm_worker_home_page/services/filter_jobs_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../../../../models/filter_jobs_model.dart';
import '../../../../models/save_job_as_favourite_model.dart';
import '../data/get_skills_by_user_id_model.dart';
import '../data/send_request_by_farmworker_fetch_model.dart';


class FilterJobsRepoImpl extends FilterJobsRepo with NetworkCheckService {
  final DioClient _dioClient;
  FilterJobsRepoImpl(this._dioClient);
  @override
  Future<Either<FilterJobsModel, Exception>> filteredJobs({params1}) async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    } else {
      try {
        var result = await _dioClient.requestForAuth(
            'Jobs/filterjobsByLocation',
            Method.post,
            params: params1);
            print('params1:$params1');
        return result.fold((l) {
          FilterJobsModel model = FilterJobsModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      } catch (e) {
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<SaveJobAsFavouriteModel,Exception>> jobAsFavourite({params1})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('save/saveJobAsFavorite', Method.post,params: params1);
        return result.fold((l){
          SaveJobAsFavouriteModel model=SaveJobAsFavouriteModel.fromJson(l.data);
          return Left(model);
        }, (r)=>Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<SendRequestByFarmworkerFetchModel,Exception>> farmworkersFetch({params1}) async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    } else {
      try {
        var result = await _dioClient.requestForAuth(
            'notification/sendRequestByFarmworker',
            Method.post,
            params: params1);
            print('params1:$params1');
        return result.fold((l) {
          SendRequestByFarmworkerFetchModel model = SendRequestByFarmworkerFetchModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      } catch (e) {
        return Right(Exception(e));
      }
    }
  }
  
  @override
  Future<Either<GetSkillsByUserIdModel,Exception>> getUserSkills({userId}) async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    } else {
      try {
        var result = await _dioClient.requestForAuth(
            'FarmerWorkerSkills/getSkillsByUserId?userId=$userId',
            Method.get);
        return result.fold((l) {
          GetSkillsByUserIdModel model = GetSkillsByUserIdModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      } catch (e) {
        return Right(Exception(e));
      }
    }
  }
}
