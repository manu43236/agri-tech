import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/home/data/get_lease_utilities_details_model.dart';
import 'package:agritech/app/modules/home/data/get_leased_lands_details_model.dart';
import 'package:agritech/app/modules/home/services/home_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';


import '../data/all_jobs_of_landlord_model.dart';
import '../data/all_lands_of_landlord.dart';
import '../data/get_all_jobs_by_farmer_model.dart';
import '../data/get_all_lands_by_farmer_model.dart';
import '../data/get_leased_jobs_details_model.dart';
import '../data/recent_lands_model.dart';

class LandlordController extends BaseController{

 
  Rx<RecentLandsModel> recentLandsModel = Rx(RecentLandsModel());
  Rx<AddLandsOfLandlordModel> addLandsOfLandlordModel =
      Rx(AddLandsOfLandlordModel());
  Rx<AllJobsOfLandlordModel> allJobsOfLandlordModel =
      Rx(AllJobsOfLandlordModel());
  Rx<GetAllLandsByFarmerModel> getAllLandsByFarmerModel =
      Rx(GetAllLandsByFarmerModel());
  Rx<GetAllJobsByFarmerModel> getAllJobsByFarmerModel =
      Rx(GetAllJobsByFarmerModel());
  Rx<GetLeaseUtilitiesDetailsModel> getLeaseUtilitiesDetailsModel =
      Rx(GetLeaseUtilitiesDetailsModel());
  Rx<GetLeasedJobDetailsModel> getLeasedJobDetailsModel =
      Rx(GetLeasedJobDetailsModel());
  Rx<GetLeasedLandsDetailsModel> getLeasedLandsDetailsModel =
      Rx(GetLeasedLandsDetailsModel());

  RxInt currentPage = 0.obs;
  var ongoingLandDescriptions = <Map<String, String>>[].obs;
  var ongoingJobDescriptions = <Map<String, String>>[].obs;
  var selectedCrop = ''.obs;

  var userId = "".obs;
  var myCustomerSearchText=''.obs;
  var apiRecentLandsStatus=ApiStatus.LOADING.obs;

  @override
  void onInit() {
    super.onInit();
  }

  void setSelectedCrop(String crop) {
    selectedCrop.value = crop;
  }

  // recent lands for top
   recentLands() async {
    apiRecentLandsStatus.value = ApiStatus.LOADING;

    currentPage.value=0;

    try {
      var result = await HomeRepoImpl(dioClient).doRecentLands();
      print('=============== get all recent lands =========');
      print(result);
      result.fold((left) {
       // recentLandshandleResponse(left);
       if(left.statusCode == 200) {
        recentLandsModel.value = left;
       }
        apiRecentLandsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiRecentLandsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }


  // my Lands View API
  Future<void> addAllLands({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      //landlordId.value = Get.arguments as String;
      var result =
          await HomeRepoImpl(dioClient).doAddAllLands(id:userId);
      print('llllllllllllllllllllllllllllllllllllllllll');
      print('results:$result');
      result.fold((left) {
        allLandshandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void allLandshandleResponse(AddLandsOfLandlordModel model) async {
    print('kkkkkkkkkkkkkkkkkkkkk');
    print(model);
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
      addLandsOfLandlordModel.value.result?.ongoingLands?.clear();
      addLandsOfLandlordModel.value = model;
      // ongoingLandDescriptions.value =
      //     addLandsOfLandlordModel.value.result!.ongoingLands!.map((land) {
      //   return {
      //     land.id.toString():
      //         "${land.fullName} land at ${land.village} with survey number - ${land.surveyNum}"
      //   };
      // }).toList();
      print('77777777777777777777777');
      print(addLandsOfLandlordModel.value.result!.ongoingLands![0].description);
    }
  }


  //Home Page ALL JOBS View
  Future<void> addAlljobs({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await HomeRepoImpl(dioClient)
          .addJobsOfLandlord(userId: userId);
      print("========================All jobs of landlord====================");
      print("id:${Get.find<HomeController>().userId.value}");
      print(result);
      result.fold((left) {
        allJobshandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void allJobshandleResponse(AllJobsOfLandlordModel model) async {
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
      allJobsOfLandlordModel.value = model;
      print('77777777777777777777777');
       ongoingJobDescriptions.value =
          allJobsOfLandlordModel.value.result!.ongoingJobs!.map((job) {
        return {
          job.id.toString():
              "${job.fullName} job at ${job.village}"
        };
      }).toList();
      print(allJobsOfLandlordModel.value.result!.ongoingJobs?[0].description);
    }
  }

// Home Page ALL Lands of farmer flow API
  Future<void> addAllFarmersLands({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await HomeRepoImpl(dioClient).allFarmerLands(params1: {
        "farmerId": userId,
      });
      print('============hey=============');
      print(result);
      result.fold((left) {
        allFarmerLandshandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void allFarmerLandshandleResponse(GetAllLandsByFarmerModel model) async {
    if (model.statusCode == 200) {
      print('======== all lands of farmer flow =========');
      print(model);
      getAllLandsByFarmerModel.value = model;
      print(
          getAllLandsByFarmerModel.value.result!.ongoingLands![0].description);
    }
  }

  //Home Page ALL JOBS of farmer flow
  Future<void> addAllFarmerjobs({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      print('hiiiiiiiii');
      var result = await HomeRepoImpl(dioClient)
          .getAllFarmerJobs(params1: {
        "farmerId": userId,
      });
      print("========================All jobs of farmer====================");
      print("id:${Get.find<HomeController>().userId.value}");
      print(result);
      result.fold((left) {
        allFarmerJobshandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void allFarmerJobshandleResponse(GetAllJobsByFarmerModel model) async {
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
      getAllJobsByFarmerModel.value = model;
      ongoingJobDescriptions.value =
          getAllJobsByFarmerModel.value.result!.ongoingJobs!.map((job) {
        return {job.id.toString(): "${job.fullName} job at ${job.village}"};
      }).toList();
      print(getAllJobsByFarmerModel.value.result!.ongoingJobs?[0].description);
    }
  }

  // Home Page leased utilities  API
  Future<void> leasedUtilities({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await HomeRepoImpl(dioClient).getLeaseDetails(userId: userId);
      print('============hey leased utilities=============');
      print(result);
      result.fold((model) {
        if(model.statusCode==200){
          getLeaseUtilitiesDetailsModel.value=model;
          print(getLeaseUtilitiesDetailsModel.value.result!.utilities!.length);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  // Home Page leased jobs  API
  Future<void> leasedJobs({userId,role}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await HomeRepoImpl(dioClient).getLeasedJobDetails(userId: userId,role: role);
      print('============hey=============');
      print(result);
      result.fold((model) {
        if(model.statusCode==200){
          getLeasedJobDetailsModel.value=model;
          print(getLeasedJobDetailsModel.value.result!.jobs!.length);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  // Home Page leased lands  API
  Future<void> leasedLands({userId,role}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await HomeRepoImpl(dioClient).getLeasedLandDetails(userId: userId,role: role);
      print('============hey=============');
      print(result);
      result.fold((model) {
        if(model.statusCode==200){
          getLeasedLandsDetailsModel.value=model;
          print(getLeasedLandsDetailsModel.value.result!.lands!.length);
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }
  

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}