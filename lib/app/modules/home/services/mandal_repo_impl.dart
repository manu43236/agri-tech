import 'package:agritech/app/modules/home/services/mandal_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import '../../select_region/data/mandal_model.dart';

class MandalRepoImpl extends MandalRepo with NetworkCheckService{
  final DioClient _dioClient;
  MandalRepoImpl( this._dioClient);
  @override
  Future<Either<MandalModel,Exception>> getMandal({id,lang})async{
    var data=await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Mandal/getMandalsByDistrictId?district_id=$id&language=$lang',Method.get);
        return result.fold((l){
          MandalModel model=MandalModel.fromJson(l.data);
          return left(model);
        },(r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}