import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../app/modules/home/services/village_repo_impl.dart';
import '../../../../app/modules/select_region/data/village_model.dart';


class VillageDropdownController extends BaseController {


 // var model = StatelistModel().obs;
  var selectMake = 'Select Village'.obs;
  var village= <String>[].obs;
  Rx<List<VillageResult>> villageResult = Rx([]);
   var items = <String>[].obs;

  TextEditingController villageController = TextEditingController();
  @override
  void onInit() {
  
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  getAllVillages({id,lang}) async {
    apiStatus.value = ApiStatus.LOADING;
    print(id);
    village.clear();
    items.clear();
    if(id != 0){
    var result = await VillageRepoImpl(dioClient).getVillage(id: id,lang: lang);
    print("result");
    print(result);
    result.fold((l) {
      if (l.result!.isNotEmpty) {
        villageResult.value = l.result!;
        for (var element in l.result!) {
          items.add(element.name!);
        }
        village.addAll(items);
        
        print(village);
      }
      apiStatus.value = ApiStatus.SUCCESS;
    
    }, (r) {
     
      apiStatus.value = ApiStatus.FAIL;
    });
  }
  }
}