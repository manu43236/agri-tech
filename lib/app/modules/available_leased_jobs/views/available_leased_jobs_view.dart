import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/text_fields/custom_search_widget.dart';
import '../../../routes/app_pages.dart';
import '../../home/controllers/home_controller.dart';
import '../controllers/available_leased_jobs_controller.dart';

class AvailableLeasedJobsView extends GetView<AvailableLeasedJobsController> {
  const AvailableLeasedJobsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Available Leased Jobs',
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10,
                ),
                CustomSearchField(
                  onChanged: (val) {
                    controller.workerLeasedJobSearch.value = val;
                    print("Dataaaa");
                    print(controller
                        .getLeasedSearchJobsDetailsModel.value.result);
                    if (val.length >= 3) {
                      controller.leasedJobs();
                    } else if (val.length == 0) {
                      controller.leasedJobs();
                    }
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                controller.apiWorkerLeasedJobsStatus.value == ApiStatus.LOADING
                    ? const SizedBox()
                    : controller.getLeasedSearchJobsDetailsModel.value.result!
                            .jobs!.isEmpty
                        ? const Align(
                            alignment: Alignment.center,
                            child: Text(
                              "No Jobs Found",
                              style:
                                  TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                          )
                        : Column(
                            children: _workSearchJobs(),
                          )
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _workSearchJobs() {
    List<Widget> items = [];
    for (var index = 0;
        index <
            controller
                .getLeasedSearchJobsDetailsModel.value.result!.jobs!.length;
        index++) {
      items.add(InkWell(
        onTap: () async {
          Get.find<HomeController>().jobId.value =controller
                .getLeasedSearchJobsDetailsModel.value.result!.jobs![index].id
              .toString();
          Get.toNamed(Routes.DETAIL_LEASED_JOBS,
              arguments: {"jobId": Get.find<HomeController>().jobId.value});
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(11), topRight: Radius.circular(11)),
              child: Image.network(
                controller.getLeasedSearchJobsDetailsModel.value.result!
                        .jobs![index].image ??
                    '',
                height: Get.height * 0.2,
                width: Get.width,
                fit: BoxFit.cover,
                errorBuilder: (context, error, stackTrace) {
                  return Image.asset(
                    defaultImageUrl,
                    height: Get.height * 0.2,
                    width: Get.width,
                    fit: BoxFit.cover,
                  );
                },
              ),
            ),
            Container(
              width: Get.width,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(11),
                      bottomRight: Radius.circular(11)),
                  color: colorAsh,
                  shape: BoxShape.rectangle),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${controller.getLeasedSearchJobsDetailsModel.value.result!.jobs![index].description}",
                      style: TextStyles.kTSDS14W500.copyWith(color: colorHello),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                        'Location-${controller.getLeasedSearchJobsDetailsModel.value.result!.jobs![index].address}'),
                    const SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ));
    }
    return items;
  }
}
