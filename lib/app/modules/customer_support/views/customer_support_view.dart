import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/customer_support_controller.dart';

class CustomerSupportView extends GetView<CustomerSupportController> {
  const CustomerSupportView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text('customer_support'.tr,style:TextStyles.kTSFS24W600.copyWith(color: colorBlack) ,),
        centerTitle: true,
        leading: IconButton(
          onPressed: (){
            Get.back();
        }, 
        icon: Icon(Icons.arrow_back,color: colorBlack,)
        ),
      ),
      body: const Center(
        child: Text(
          'Coming soon......',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
