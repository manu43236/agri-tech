import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/saved_jobs_controller.dart';

class SavedJobsView extends GetView<SavedJobsController> {
  const SavedJobsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'saved_jobs'.tr,
          style: TextStyles.kTSFS20W500
              .copyWith(color: colorBlack, fontWeight: FontWeight.w400),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: Obx(
          () => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: controller.getAllSavedJobsModel.value.result != null &&
                        controller
                                .getAllSavedJobsModel.value.result!.savedJobs !=
                            null &&
                        controller.getAllSavedJobsModel.value.result!.savedJobs!
                            .isNotEmpty
                    ? ListView.separated(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        itemCount: controller.getAllSavedJobsModel.value.result!
                            .savedJobs!.length,
                        separatorBuilder: (context, index) =>
                            const SizedBox(height: 10),
                        itemBuilder: (context, index) {
                          final savedJob = controller.getAllSavedJobsModel.value
                              .result!.savedJobs![index];
                          // final ongoingLand =
                          //     Get.find<FarmWorkerHomePageController>()
                          //         .filterJobsByModel
                          //         .value
                          //         .result?[index]
                          //         .job;

                          return InkWell(
                            onTap: () async {
                              Get.find<HomeController>().jobId.value =
                                  savedJob.jobid.toString();
                              Get.toNamed(Routes.DETAIL_SAVED_JOB,arguments: {"jobId":Get.find<HomeController>().jobId.value });
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(11),
                                      topRight: Radius.circular(11)),
                                  child: Image.network(
                                    savedJob.imageUrl.toString(),
                                    height: Get.height * 0.2,
                                    width: Get.width,
                                    fit: BoxFit.cover,
                                    errorBuilder: (context, error, stackTrace) {
                                      // Display a local asset if the network image fails
                                      return Image.asset(
                                        defaultImageUrl,
                                        height: Get.height * 0.2,
                                        width: Get.width,
                                        fit: BoxFit.cover,
                                      );
                                    },
                                  ),
                                ),
                                Container(
                                  width: Get.width,
                                  decoration: const BoxDecoration(
                                    color: colorAsh,
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(11),
                                        bottomRight: Radius.circular(11)),
                                    shape: BoxShape.rectangle,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    child: Column(
                                       crossAxisAlignment: CrossAxisAlignment.start,
                                       mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${savedJob.name} ${'job_at'.tr} ${savedJob.location ?? ''}",
                                          style: TextStyles.kTSDS14W500
                                              .copyWith(color: colorHello),
                                        ),
                                        const SizedBox(height: 5),
                                        Text(
                                          '${'location'.tr} ${savedJob.location ?? 'NA'}      ${'number_of_days_to_work'.tr}',
                                          //${formatDateToIST(ongoingLand!.endDate.toString())}
                                          style: TextStyles.kTSFS10W500
                                              .copyWith(color: colorHello),
                                        ),
                                        const SizedBox(height: 5),
                                        // InkWell(
                                        //   onTap: () {},
                                        //   child: Container(
                                        //     padding: const EdgeInsets.symmetric(
                                        //         horizontal: 10, vertical: 5),
                                        //     decoration: const BoxDecoration(
                                        //         color: colorCost),
                                        //     child: const Text(
                                        //       'Cost Enquiry',
                                        //       style: TextStyle(
                                        //         color: colorWhite,
                                        //         fontSize: 6,
                                        //       ),
                                        //     ),
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      )
                    :  Center(child: Text('no_jobs_saved'.tr)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
