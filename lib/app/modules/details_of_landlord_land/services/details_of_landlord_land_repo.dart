import 'package:agritech/app/modules/details_of_landlord_land/data/send_lease_interest_notification_by_farmer_fetch_model.dart';
import 'package:dartz/dartz.dart';

abstract class DetailsOfLandlordLandRepo{
  Future<Either<SendLeaseInterestNotificationByFarmerFetchModel,Exception>> farmerFetch({params1});
}