import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/profile/controllers/profile_controller.dart';
import 'package:get/get.dart';

import '../controllers/edit_profile_controller.dart';

class EditProfileBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EditProfileController>(
      () => EditProfileController(),
    );
     Get.lazyPut<HomeController>(
      () => HomeController(),
    );
     Get.lazyPut<ProfileController>(
      () => ProfileController(),
    );
  }
}
