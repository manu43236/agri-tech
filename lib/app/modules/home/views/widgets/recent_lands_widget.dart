import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../../core/base/base_controller.dart';
import '../../../../../core/conts/color_consts.dart';
import '../../../../../core/conts/img_const.dart';
import '../../../../../core/themes/text_styles.dart';
import '../../../../../core/utils/widget_utils/shimmers.dart';
import '../../../../routes/app_pages.dart';

class RecentLandsWidget extends GetView<LandlordController> {
  const RecentLandsWidget({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;
  @override
  Widget build(BuildContext context) {
    final PageController _pageController = PageController();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Obx(
        () => Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'available_lands'.tr,
                style: TextStyles.kTSFS14W600
                    .copyWith(color: colorHello, fontWeight: FontWeight.w500),
              ),
              // IconButton(
              //   onPressed: () {
              //     Get.find<SelectRegionController>().isRecent.value = true;
              //     Get.toNamed(Routes.SELECT_REGION);
              //   },
              //   icon: const Icon(
              //     Icons.more_horiz,
              //     color: colorDots,
              //   ),
              // ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          controller.apiRecentLandsStatus.value == ApiStatus.LOADING
              ? Shimmers().getShimmerItem()
              : controller.recentLandsModel.value.result != null &&
                      controller.recentLandsModel.value.result!.isNotEmpty
                  ? Column(
                      children: [
                        SizedBox(
                          height: Get.height / 3,
                          child: PageView.builder(
                              controller: _pageController,
                              onPageChanged: (index) {
                                controller.currentPage.value = index;
                              },
                              scrollDirection: Axis.horizontal,
                              itemCount: controller
                                  .recentLandsModel.value.result!.length,
                              itemBuilder: (context, index) {
                                final land = controller
                                    .recentLandsModel.value.result![index];
                                final imageUrl =
                                    land.imageUrl ?? defaultImageUrl;
                                return Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "${controller.recentLandsModel.value.result![index].fullName} ${'at'.tr} ${controller.recentLandsModel.value.result![index].village}",
                                        //textAlign: TextAlign.start,
                                        style: TextStyles.kTSDS14W500
                                            .copyWith(color: colorHello),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      InkWell(
                                        onTap: () async {
                                          final selectedLand = controller
                                              .recentLandsModel
                                              .value
                                              .result![index];

                                          Get.toNamed(
                                              Routes.DETAIL_RECENT_LANDS,
                                              arguments: {
                                                "landId":
                                                    selectedLand.id.toString()
                                              });
                                        },
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(12),
                                          child: Image.network(
                                            imageUrl ?? defaultImageUrl,
                                            height: Get.height / 3.5,
                                            width: Get.width,
                                            fit: BoxFit.cover,
                                            errorBuilder:
                                                (context, error, stackTrace) {
                                              return Image.asset(
                                                defaultImageUrl,
                                                height: Get.height / 3.5,
                                                width: Get.width,
                                                fit: BoxFit.cover,
                                              );
                                            },
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              }),
                        ),
                        Obx(() => Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: List.generate(
                                controller
                                    .recentLandsModel.value.result!.length,
                                (index) => buildDot(
                                    index, controller.currentPage.value),
                              ),
                            )),
                        Align(
                          alignment: Alignment.topRight,
                          child: InkWell(
                            onTap: () {
                              // print('tapping on recent land view all');
                              // Get.find<SelectRegionController>().isRecent.value = false;
                              // print(
                              //     'is recent ${controller.isRecent.value}');
                              //Get.find<LandlordController>().recentLands();
                              Get.toNamed(Routes.AVAILABLE_RECENT_LANDS);
                              //arguments: {"heading": ""
                              //  'Your selected region is ${Get.find<HomeController>().getSelectedRegionName()}'
                            },
                            child: Text(
                              'view_all'.tr,
                              style: TextStyles.kTSCF12W500.copyWith(
                                  fontSize: 10,
                                  decoration: TextDecoration.underline),
                            ),
                          ),
                        ),
                      ],
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: 60),
                        Text(
                          'no_lands'.tr,
                          style: TextStyles.kTSFS18W500
                              .copyWith(color: colorHello),
                        ),
                        const SizedBox(height: 20),
                        InkWell(
                          onTap: () {
                            Get.toNamed(Routes.ADD_LAND);
                          },
                          child: Text(
                            'add_your_land_here'.tr,
                            style: TextStyles.kTSFS18W500.copyWith(
                                color: colorBlue,
                                decoration: TextDecoration.underline),
                          ),
                        ),
                        const SizedBox(height: 60),
                      ],
                    ),
        ]),
      ),
    );
  }

  Widget buildDot(int index, int currentIndex) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 2),
      width: currentIndex == index ? 10 : 8,
      height: currentIndex == index ? 12 : 8,
      decoration: BoxDecoration(
        color: currentIndex == index ? Colors.blue : Colors.grey,
        shape: BoxShape.circle,
      ),
    );
  }
}
