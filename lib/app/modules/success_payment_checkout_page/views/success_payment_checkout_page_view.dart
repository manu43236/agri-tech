import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/success_payment_checkout_page_controller.dart';

class SuccessPaymentCheckoutPageView
    extends GetView<SuccessPaymentCheckoutPageController> {
  const SuccessPaymentCheckoutPageView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     appBar: AppBar(
        title: Text(
          'payment_checkout'.tr,
          style: TextStyles.kTSFS24W600
              .copyWith(color: colorSuitable, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Obx(()=>
        Padding(
          padding: const EdgeInsets.only(left: 20,right: 20,),
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Image.network(
                  controller
                          .getUtilityPaymentByIdModel
                          .value
                          .result
                          ?.utilityDetails
                          ?.image
                          .toString() ??
                      '',
                  height: Get.height * 0.18,
                  width: Get.width,
                  fit: BoxFit.cover,
                  errorBuilder: (context, error, stackTrace) {
                    // Display a local asset if the network image fails
                    return Image.asset(
                      defaultImageUrl,
                      height: Get.height * 0.18,
                      width: Get.width,
                      fit: BoxFit.cover,
                    );
                  },
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          controller
                                  .getUtilityPaymentByIdModel
                                  .value
                                  .result
                                  ?.utilityDetails
                                  ?.name ??
                              '',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style:
                              TextStyles.kTSFS20W700.copyWith(color: colorSuitable),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                '${controller.getUtilityPaymentByIdModel.value.result?.utilityDetails?.address?.mandal ?? ''} , ${controller.getUtilityPaymentByIdModel.value.result?.utilityDetails?.address?.village ?? ''}',
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                style: TextStyles.kTSDS14W500
                                    .copyWith(color: colorSuitable),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Text(
                        controller
                                .getUtilityPaymentByIdModel
                                .value
                                .result
                                ?.quantity
                                .toString() ??
                            '',
                        style: TextStyles.kTSFS20W700
                            .copyWith(color: colorSuitable, fontSize: 20),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 50,),
              Center(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 50,
                    ),
                    Image.asset(
                      payment,
                      height: Get.height * 0.2,
                      width: Get.width * 0.5,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      'payment_is_successful'.tr,
                      style: TextStyles.kTSFS14W600.copyWith(color: colorHello),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
