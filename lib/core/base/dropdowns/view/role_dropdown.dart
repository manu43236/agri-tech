import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../app/modules/profile/controllers/profile_controller.dart';
import '../../../../app/modules/profile/data/get_user_roles_model.dart';
import '../../../conts/color_consts.dart';
import '../../../conts/img_const.dart';
import '../../../themes/text_styles.dart';
import '../../../utils/widget_utils/search_dialog.dart';

class RoleDropdown extends GetView<ProfileController> {
  final TextEditingController textEditingController;
  final Function(String?)? onSelected;

  RoleDropdown({
    super.key,
    required this.textEditingController,
    this.onSelected,
  });

  Future<void> _showRoleSelectionDialog(BuildContext context) async {
    // Fetch the list of roles
    List<UserResult> roleList = controller.getUserRolesModel.value.result ?? [];
    if (roleList.isEmpty) {
      Get.snackbar("no_roles_available".tr, "please_add_roles_first".tr);
      return;
    }

    // Prepare role names for the dialog
    List<String> items =
        roleList.map((role) => role.name.toString() ?? '').toList();

    // Display the search dialog
    var selectedItem = await Get.dialog(
      SearchSelectionDialogView(
        items: items,
        searchHint: 'search_role'.tr,
        title: 'select_role'.tr,
      ),
    );

    // Handle role selection
    if (selectedItem != null) {
      textEditingController.text = selectedItem;
      var result = roleList.firstWhere((val) => val.name == selectedItem);

      // Update the selected role in the homeController
      controller.selectedValue.value = result.name ?? '';
      onSelected?.call(controller.selectedValue.value);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _showRoleSelectionDialog(context),
      child: Obx(() {
        return InputDecorator(
          decoration: InputDecoration(
            prefixIconConstraints:
                const BoxConstraints(minWidth: 48, maxHeight: 50),
            prefixIcon: SizedBox(
              width: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(businessman),
                  const SizedBox(
                    height: 20,
                    child: VerticalDivider(
                      color: colorBlack,
                      thickness: 1,
                      width: 18,
                    ),
                  ),
                ],
              ),
            ),
            suffixIcon: const Icon(Icons.arrow_drop_down),
            contentPadding: const EdgeInsets.symmetric(vertical: 20),
            hintText: controller.selectedValue.value.isNotEmpty
                ? controller.selectedValue.value
                : 'select_role'.tr,
            hintStyle: TextStyles.kTSFS14WNORM.copyWith(color: Colors.grey),
          ),
          child: Text(
            controller.selectedValue.value.isNotEmpty
                ? controller.selectedValue.value
                : 'select_role'.tr,
            style: controller.selectedValue.value.isNotEmpty
                ? TextStyles.kTSFS16W700.copyWith(fontWeight: FontWeight.w400)
                : TextStyles.kTSFS14WNORM.copyWith(color: Colors.grey),
          ),
        );
      }),
    );
  }
}
