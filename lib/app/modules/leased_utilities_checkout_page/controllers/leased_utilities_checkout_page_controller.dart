import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../data/get_utility_ninty_percent_details_model.dart';
import '../services/leased_utilities_checkout_repo_impl.dart';

class LeasedUtilitiesCheckoutPageController extends BaseController {

  Rx<GetUtilityNintyPercentDetailsModel> getUtilityNintyPercentDetailsModel = Rx(GetUtilityNintyPercentDetailsModel());

  @override
  void onInit() async {
    super.onInit();
    print('objectssssssssss');
    if(Get.arguments!=null){
      await getNintyPercentUtilityDetails(id: Get.arguments['id']);
    }
  }

  //get ninty percent utility details API
  Future<void> getNintyPercentUtilityDetails({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await LeasedUtilitiesCheckoutRepoImpl(dioClient).getNintyDetails(id: id);
      print("===================== get 90 percent utility details ==============");
      print(result);
      result.fold((model) {
        getUtilityNintyPercentDetailsModel.value = model;
        //print(getUtilityNintyPercentDetailsModel.value.result!.amountPaid);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
