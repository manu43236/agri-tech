import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/detail_leased_jobs_controller.dart';

class DetailLeasedJobsView extends GetView<DetailLeasedJobsController> {
  const DetailLeasedJobsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Detail Leased Jobs',
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(right: 20,left: 20),
        child: Obx(() =>controller.apiJobIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): 
        SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: Get.height * 0.35,
                    width: Get.width,
                    decoration: BoxDecoration(
                        color: colorAsh,
                        borderRadius: BorderRadius.circular(13)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: controller.jobByIdModel.value.result?.image != null
                          ? Image.network(
                              controller.jobByIdModel.value.result!.image!,
                              height: Get.height * 0.35,
                              width: Get.width,
                              fit: BoxFit.cover,
                            )
                          : Image.asset(
                              'assets/images/agri.png',
                              height: Get.height * 0.35,
                              width: Get.width,
                              fit: BoxFit.cover,
                            ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'JOB AT ${controller.jobByIdModel.value.result?.village ?? 'Unknown Village'}',
                            style: TextStyles.kTSFS20W700
                                .copyWith(color: colorDetails),
                          ),
                          Text(
                            '${controller.jobByIdModel.value.result?.mandal ?? 'Unknown Mandal'}, ${controller.jobByIdModel.value.result?.district ?? 'Unknown District'}',
                            style: TextStyles.kTSDS14W500
                                .copyWith(color: colorDetails),
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          CircleAvatar(
                            radius: 26,
                            backgroundColor: colorAsh,
                            child: ClipOval(
                              child: 
                              Image.network(
                      controller.jobByIdModel.value.result?.jobOwnerDetails?.imageUrl.toString() ??
                          defaultImageUrl,
                      height: Get.height * 0.1,
                      width: Get.width*0.2,
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        return Image.asset(
                          defaultImageUrl,
                          height: Get.height * 0.1,
                          width: Get.width*0.2,
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                            ),
                          ),
                          const SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                 controller.jobByIdModel.value.result?.jobOwnerDetails?.firstName
                                        ?.toString() ??
                                    'Unknown User',
                                style: TextStyles.kTSDS14W700
                                    .copyWith(color: colorPic),
                              ),
                              const SizedBox(height: 5),
                              Text(
                                 controller.jobByIdModel.value.result?.jobOwnerDetails?.address
                                        ?.toString() ??
                                    'Unknown Address',
                                style: TextStyles.kTSWFS10W700
                                    .copyWith(color: colorDetails),
                              )
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Text(
                    controller.jobByIdModel.value.result?.description ??
                        'No description available',
                    style: TextStyles.kTSCF12W500,
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
