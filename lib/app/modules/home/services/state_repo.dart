import 'package:agritech/app/modules/select_region/data/state_model.dart';
import 'package:dartz/dartz.dart';

abstract class StateRepo{
  Future<Either<StateModel,Exception>> getState();
}