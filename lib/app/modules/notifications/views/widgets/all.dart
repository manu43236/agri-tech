import 'package:agritech/app/modules/notifications/controllers/notifications_controller.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../core/base/base_controller.dart';
import '../../../../../core/utils/widget_utils/date_format.dart';
import '../../../../../core/utils/widget_utils/shimmers.dart';

class All extends GetView<NotificationsController> {
  const All({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'notifications'.tr,
          style: TextStyles.kTSFS24W600
              .copyWith(color: colorHello, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            controller.getNotificationsCount();
            Get.back();
          },
        ),
      ),
      body: Obx(
        () {
          final notifications =
              controller.getNotificationsModel.value.result ?? [];

          return controller.apiStatus.value == ApiStatus.LOADING
              ? Shimmers().getListShimmer()
              : RefreshIndicator(
                  onRefresh: () async {
                    controller.getNotifications(userId: controller.id.value);
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Divider(thickness: 2, color: colorAsh),
                      Expanded(
                        child: ListView.separated(
                          itemCount: notifications.length,
                          separatorBuilder: (context, index) => const Divider(
                            thickness: 1,
                            color: colorAsh,
                          ),
                          itemBuilder: (context, index) {
                            final notification = notifications[index];
                            final imageUrl = notification.imageUrl ?? '';
                            final name = notification.name ?? 'Unknown';
                            final message = notification.message ?? '';
                            final createdAt =
                                notification.createdAt?.toString() ?? '';

                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  InkWell(
                                    onTap: () async {
                                      print('on tap');
                                      controller.updateState(index);
                                      print('on tappping item');
                                      controller.notificationId.value =
                                          notification.id.toString();
                                      await controller.markAsViewed();
                                      controller.getNotificationsCount();
                                      controller.getNotifications(userId: controller.id.value);
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        // Notification dot
                                        notification.isRead == false
                                            ? const Padding(
                                                padding:
                                                    EdgeInsets.only(bottom: 45),
                                                child: CircleAvatar(
                                                  radius: 4,
                                                  backgroundColor:
                                                      Color.fromARGB(
                                                          255, 65, 126, 231),
                                                  child: CircleAvatar(
                                                    radius: 3,
                                                    backgroundColor:
                                                        Color.fromARGB(
                                                            255, 141, 193, 236),
                                                  ),
                                                ),
                                              )
                                            : const SizedBox(),
                                        const SizedBox(width: 10),
                                        // Profile image
                                        CircleAvatar(
                                          radius: 20,
                                          backgroundColor: colorAsh,
                                          child: ClipOval(
                                            child: imageUrl.isNotEmpty
                                                ? Image.network(
                                                    imageUrl,
                                                    height: Get.height * 0.05,
                                                    width: Get.width * 0.2,
                                                    fit: BoxFit.cover,
                                                    errorBuilder: (context,
                                                            error,
                                                            stackTrace) =>
                                                        const Icon(Icons.error,
                                                            color: colorBlack),
                                                  )
                                                : const Icon(Icons.person,
                                                    color: colorBlack),
                                          ),
                                        ),
                                        const SizedBox(width: 10),
                                        // Notification details
                                        Flexible(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 5),
                                            child: Text(
                                              message,
                                              style: TextStyles.kTSCF12W500
                                                  .copyWith(color: colorBlack),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  notification.statusForLandlord ==
                                              "Accepted" &&
                                          notification.statusForFarmer ==
                                              "Interested"
                                      ? const SizedBox()
                                      : notification.statusForLandlord ==
                                                  "Accepted" &&
                                              notification
                                                      .statusForFarmworker ==
                                                  "interested"
                                          ? const SizedBox()
                                          : notification.statusForLandlord ==
                                                      "Request Sent" &&
                                                  notification
                                                          .statusForFarmworker ==
                                                      "Accepted"
                                              ? const SizedBox()
                                              : notification.statusForLandlord ==
                                                          "approved" &&
                                                      notification
                                                              .statusForFarmer ==
                                                          "interested"
                                                  ? const SizedBox()
                                                  : notification.statusForLandlord ==
                                                              null &&
                                                          notification
                                                                  .statusForFarmworker ==
                                                              "Accepted"
                                                      ? SizedBox()
                                                      :notification.statusForLandlord ==
                                                              null &&
                                                            notification.statusForFarmer ==
                                                              null &&  
                                                          notification
                                                                  .statusForFarmworker ==
                                                              null
                                                      ? SizedBox()
                                                      : Obx(() {
                                                          print(
                                                              "value ${controller.isButton.value && controller.selectedNotificationIndex.value == index}");
                                                          // return notification.statusForLandlord.toString().toLowerCase()=="accepted"&&notification.statusForFarmer.toString().toLowerCase()=="interested"?SizedBox():
                                                          return controller
                                                                      .isButton
                                                                      .value &&
                                                                  controller
                                                                          .selectedNotificationIndex
                                                                          .value ==
                                                                      index
                                                              ? Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    InkWell(
                                                                      onTap:
                                                                          () async {
                                                                        print(
                                                                            'landlord approve tapping');
                                                                        print(
                                                                            'statusForLandlord: ${notification.statusForLandlord}');
                                                                        print(
                                                                            'statusForFarmer: ${notification.statusForFarmer}');
                                                                        print(
                                                                            'statusForFarmworker: ${notification.statusForFarmworker}');
                                                                        if (notification.statusForLandlord ==
                                                                                null &&
                                                                            notification.statusForFarmer ==
                                                                                'Interested') {
                                                                          print(
                                                                              'landlord approve tapped');
                                                                          controller
                                                                              .notificationId
                                                                              .value = notification.id
                                                                                  ?.toString() ??
                                                                              '';
                                                                          await controller
                                                                              .approved(index); // Mark as approved
                                                                          controller
                                                                              .getNotifications(userId: controller.id.value);
                                                                        } else if (notification.statusForLandlord ==
                                                                                'Pending' &&
                                                                            notification.statusForFarmworker ==
                                                                                'interested') {
                                                                          controller
                                                                              .notificationId
                                                                              .value = notification.id
                                                                                  ?.toString() ??
                                                                              '';
                                                                          await controller
                                                                              .acceptByLandlordOrFarmerForJobTaken();
                                                                          controller
                                                                              .getNotifications(userId: controller.id.value);
                                                                        } else if (notification.statusForLandlord ==
                                                                                'pending' &&
                                                                            notification.statusForFarmworker ==
                                                                                null) {
                                                                          await controller
                                                                              .acceptRequestByFarmerOrLandlordOfWorker(
                                                                            id: notification.from,
                                                                            jobId:
                                                                                notification.jobId,
                                                                          );
                                                                          controller
                                                                              .getNotifications(userId: controller.id.value);
                                                                        } else if (notification.statusForLandlord ==
                                                                                null &&
                                                                            notification.statusForFarmer ==
                                                                                'interested') {
                                                                          await controller.replyToFarmerByLandlord(
                                                                              farmerId: notification.from,
                                                                              landId: notification.landId);
                                                                          controller
                                                                              .getNotifications(userId: controller.id.value);
                                                                        }
                                                                      },
                                                                      child:
                                                                          Container(
                                                                        padding: const EdgeInsets
                                                                            .symmetric(
                                                                            horizontal:
                                                                                16,
                                                                            vertical:
                                                                                3),
                                                                        decoration:
                                                                            BoxDecoration(
                                                                          borderRadius:
                                                                              BorderRadius.circular(6),
                                                                          color:
                                                                              colorKnockBrandPrimary,
                                                                        ),
                                                                        child:
                                                                            Text(
                                                                          'approve'.tr,
                                                                          style:
                                                                              TextStyles.kTSNFS16W500,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    const SizedBox(
                                                                        width:
                                                                            20),
                                                                    InkWell(
                                                                      onTap:
                                                                          () {
                                                                        controller
                                                                            .notificationId
                                                                            .value = notification.id
                                                                                ?.toString() ??
                                                                            '';
                                                                        controller
                                                                            .deleteNotificationById();
                                                                        controller
                                                                            .getNotifications(userId: controller.id.value);
                                                                      },
                                                                      child:
                                                                          Container(
                                                                        padding: const EdgeInsets
                                                                            .symmetric(
                                                                            horizontal:
                                                                                16,
                                                                            vertical:
                                                                                3),
                                                                        decoration:
                                                                            BoxDecoration(
                                                                          borderRadius:
                                                                              BorderRadius.circular(6),
                                                                          border: Border.all(
                                                                              width: 1,
                                                                              color: colorDeclineBorder),
                                                                        ),
                                                                        child:
                                                                            Text(
                                                                          'decline'.tr,
                                                                          style: TextStyles
                                                                              .kTSNFS16W500
                                                                              .copyWith(color: colorBlack),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                )
                                                              : const SizedBox(); // Empty space when buttons are hidden
                                                        }),
                                  const SizedBox(height: 10),
                                  Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      formatDateToIST(createdAt),
                                      style: TextStyles.kTSDS14W500
                                          .copyWith(color: colorDayTime),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                );
        },
      ),
    );
  }
}
