// To parse this JSON data, do
//
//     final getLeasedJobDetailsModel = getLeasedJobDetailsModelFromJson(jsonString);

import 'dart:convert';

GetLeasedJobDetailsModel getLeasedJobDetailsModelFromJson(String str) => GetLeasedJobDetailsModel.fromJson(json.decode(str));

String getLeasedJobDetailsModelToJson(GetLeasedJobDetailsModel data) => json.encode(data.toJson());

class GetLeasedJobDetailsModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetLeasedJobDetailsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetLeasedJobDetailsModel.fromJson(Map<String, dynamic> json) => GetLeasedJobDetailsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    Farmworker? farmworker;
    List<Job>? jobs;

    Result({
        this.farmworker,
        this.jobs,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        farmworker: json["farmworker"] == null ? null : Farmworker.fromJson(json["farmworker"]),
        jobs: json["jobs"] == null ? [] : List<Job>.from(json["jobs"]!.map((x) => Job.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "farmworker": farmworker?.toJson(),
        "jobs": jobs == null ? [] : List<dynamic>.from(jobs!.map((x) => x.toJson())),
    };
}

class Farmworker {
    int? id;
    dynamic name;
    dynamic mobileNumber;
    dynamic image;

    Farmworker({
        this.id,
        this.name,
        this.mobileNumber,
        this.image,
    });

    factory Farmworker.fromJson(Map<String, dynamic> json) => Farmworker(
        id: json["id"],
        name: json["name"],
        mobileNumber: json["mobileNumber"],
        image: json["image"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "mobileNumber": mobileNumber,
        "image": image,
    };
}

class Job {
    int? id;
    dynamic title;
    dynamic description;
    dynamic image;
    dynamic address;

    Job({
        this.id,
        this.title,
        this.description,
        this.image,
        this.address,
    });

    factory Job.fromJson(Map<String, dynamic> json) => Job(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        image: json["image"],
        address: json["address"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "image": image,
        "address": address,
    };
}
