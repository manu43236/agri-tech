import 'package:get/get.dart';

class VendorPaymentSuccessfulController extends GetxController {
  //TODO: Implement VendorPaymentSuccessfulController

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void increment() => count.value++;
}
