import 'package:agritech/app/modules/otp/services/otp_repo_impl.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/models/otp_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/app_consts.dart';
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../models/login_get_otp_model.dart';
import '../../login/controllers/login_controller.dart';
import '../../login/services/login_repo_impl.dart';

class OtpController extends BaseController {
  Rx<TextEditingController> OtpControllers = Rx(TextEditingController());

  var isLogin = false.obs;
  var otp = "".obs;
  var mobileNumber = "";
  @override
  void onInit() {
    isLogin.value = Get.arguments?['isLogin'];
    otp.value = Get.arguments?['otp'];
    mobileNumber = Get.arguments['mobileNumber'];
    OtpControllers.value.text = otp.value;
    
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  /// for login and register otp verification

  void otpVerification() async {
    if (OtpControllers.value.text.isNotEmpty) {
      try {
        var result = await OTPRepoImpl(dioClient).doOTP(params1: {
          "mobileNumber": mobileNumber,
          "otp": OtpControllers.value.text
        });
        print('===========result=============');
        print(result);
        result.fold((left) {
          _hanldeResponse(left);
        }, (r) {
          apiStatus.value = ApiStatus.FAIL;
        });
      } catch (e) {
        print('Error');
      }
    }
  }

  void _hanldeResponse(OTPModel model)async {
    if (model.statusCode == 200) {
      print('======================================================================');
      print(model.result!.user!.accesstoken);
      //print('user   : ${SecureStorage().writeStringData(USERIDS, model.result!.user!.id)}');
      if (isLogin.value == true) {
        await SecureStorage().writeStringData(TOKEN, model.result!.user!.accesstoken);
        await SecureStorage().writeStringData(USERIDS, model.result!.user!.id.toString());
        await SecureStorage().writeStringData(ROLE, model.result!.user!.role.toString());
        print('rolesssss:${model.result!.user!.role.toString()}');
        Get.offAllNamed(Routes.DASHBOARD);
      }
       else {
        Get.offNamed(Routes.PROFILE);
      }
    } else {
      Get.snackbar('Error', model.message ?? 'Unknown error');
    }
  }

  //resend otp
  void resendOtp() async {
    try {
      var result = await LoginRepoImpl(dioClient).loginOtp(params1: {
        "mobileNumber": Get.find<LoginController>().mobileController.value.text,
      });
      result.fold((left) {
        _handleOtpResponse(left);
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void _handleOtpResponse(LoginGetOtpModel model) async {
    print('OTP response: ${model.toString()}');
    if (model.statusCode == 200) {
      await SecureStorage()
          .writeStringData("id", model.result!.user!.id.toString());
      OtpControllers.value.text = model.result!.user!.otp.toString();

      print('OTP sent successfully');
    } else if (model.statusCode == 400) {
      Get.snackbar(
        'FAILED',
        'User does not exist',
        snackPosition: SnackPosition.TOP,
      );
    } else {
      apiStatus.value = ApiStatus.FAIL;
      print('Failed to send OTP: ${model.message}');
    }
  }


  @override
  void onClose() {
    super.onClose();
  }
}
