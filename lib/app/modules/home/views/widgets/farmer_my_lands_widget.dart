import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/base/base_controller.dart';
import '../../../../../core/conts/color_consts.dart';
import '../../../../../core/conts/img_const.dart';
import '../../../../../core/themes/text_styles.dart';
import '../../../../../core/utils/widget_utils/shimmers.dart';
import '../../../../routes/app_pages.dart';
import '../../../select_region/controllers/select_region_controller.dart';

class FarmerMyLandsWidget extends GetView<LandlordController> {
  const FarmerMyLandsWidget({super.key});

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Obx(
        () => Column(
          children: [
            Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'my_lands'.tr,
                  style: TextStyles.kTSFS12W500.copyWith(color: colorHello),
                )),
            const SizedBox(
              height: 5,
            ),
            controller.apiStatus.value == ApiStatus.LOADING
                ? Shimmers().getShimmerItem()
                : controller.getAllLandsByFarmerModel.value.result != null &&
                        controller.getAllLandsByFarmerModel.value.result!
                            .ongoingLands!.isNotEmpty
                    ? Column(
                        children: [
                          SizedBox(
                            height: Get.height * 0.14,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: controller.getAllLandsByFarmerModel
                                    .value.result!.ongoingLands!.length,
                                itemBuilder: (context, index) {
                                  final land = controller
                                      .getAllLandsByFarmerModel
                                      .value
                                      .result!
                                      .ongoingLands![index];
                                  final imageUrl = land.imageUrl ??
                                      defaultImageUrl;
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: InkWell(
                                      onTap: () async {
                                        Get.find<HomeController>()
                                                .landId
                                                .value =
                                            controller
                                                .getAllLandsByFarmerModel
                                                .value
                                                .result!
                                                .ongoingLands![index]
                                                .id
                                                .toString();
                                        // await controller.addAllFarmersLands();
                                        // await Get.find<HomeController>()
                                        //     .getIdLands();
                                        // print('heyyyyyy');
                                        Get.toNamed(
                                            Routes.DETAIL_FARMER_MY_LANDS,arguments: {"landId":controller
                                                .getAllLandsByFarmerModel
                                                .value
                                                .result!
                                                .ongoingLands![index]
                                                .id
                                                .toString(),"isLoan":false,"isInsurance":false});
                                      },
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(12),
                                        child: Image.network(
                                          imageUrl ?? '',
                                          height: Get.height / 2.43,
                                          width: Get.width / 2.43,
                                          fit: BoxFit.cover,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            // Display a local asset if the network image fails
                                            return Image.asset(
                                              defaultImageUrl,
                                              height: Get.height / 2.43,
                                              width: Get.width / 2.43,
                                              fit: BoxFit.cover,
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                          ),
                          const SizedBox(height: 5),
                          Align(
                            alignment: Alignment.topRight,
                            child: InkWell(
                              onTap: () async {
                                // Get.find<SelectRegionController>()
                                //     .isFarmerLands
                                //     .value = false;
                                // await Get.find<LandlordController>()
                                //     .addAllFarmersLands();
                                Get.toNamed(Routes.AVAILABLE_FARMER_MY_LANDS);
                              },
                              child: Text(
                                'view_all'.tr,
                                style: TextStyles.kTSCF12W500.copyWith(
                                    fontSize: 10,
                                    decoration: TextDecoration.underline),
                              ),
                            ),
                          ),
                        ],
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: Get.height * 0.02,
                          ),
                          InkWell(
                              onTap: () {
                                Get.toNamed(Routes.ADD_LAND);
                              },
                              child: Text(
                                'no_lands_add_it_here'.tr,
                                style: TextStyles.kTSFS18W500.copyWith(
                                    color: colorBlue,
                                    decoration: TextDecoration.underline),
                              )),
                          SizedBox(
                            height: Get.height * 0.02,
                          ),
                        ],
                      ),
          ],
        ),
      ),
    );
  }
}
