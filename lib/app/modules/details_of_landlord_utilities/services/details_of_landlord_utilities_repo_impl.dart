import 'package:agritech/app/modules/details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import 'package:agritech/app/modules/details_of_landlord_utilities/services/details_of_landlord_utilities_repo.dart';
import 'package:agritech/app/modules/instrument_details/controllers/instrument_details_controller.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../../home/controllers/home_controller.dart';
import '../../utility/data/get_recent_four_utilities_model.dart';
import '../data/create_utility_payment_model.dart';
import '../data/saved_utility_model.dart';

class DetailsOfLandlordUtilitiesRepoImpl extends DetailsOfLandlordUtilitiesRepo with NetworkCheckService{
  final DioClient _dioClient;
  DetailsOfLandlordUtilitiesRepoImpl(this._dioClient);

  @override
  Future<Either<GetRecentFourUtilitiesModel,Exception>> getUtilitiesByLocation({village})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('utility/getUtilityByLocation?Location=$village', Method.get);
        return result.fold((l){
          GetRecentFourUtilitiesModel model=GetRecentFourUtilitiesModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<CreateUtilityPaymentModel,Exception>>  createUtilityPayment({params1})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('utilityPayment/createUtilityPayment', Method.post,params: params1);
        return result.fold((l){
          CreateUtilityPaymentModel model=CreateUtilityPaymentModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

   @override
   Future<Either<CreateUtilityPaymentModel,Exception>> getUtilityPayment({id})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('utilityPayment/getUtilityPaymentById?id=$id', Method.get);
        return result.fold((l){
          CreateUtilityPaymentModel model=CreateUtilityPaymentModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
   }

   @override
  Future<Either<SavedUtilityModel,Exception>> savedUtility({id})async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("NO Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('save/savedUtility?userid=${Get.find<HomeController>().userId.value}&utilityid=$id', Method.get);
        return result.fold((l){
          SavedUtilityModel model=SavedUtilityModel.fromJson(l.data);
          return Left(model);
        },(r)=>Right(Exception(r)));
      }catch(e){
        return right(Exception(e));
      }
    }
  }

}