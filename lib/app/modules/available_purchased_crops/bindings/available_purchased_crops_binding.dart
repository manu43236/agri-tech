import 'package:get/get.dart';

import '../controllers/available_purchased_crops_controller.dart';

class AvailablePurchasedCropsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailablePurchasedCropsController>(
      () => AvailablePurchasedCropsController(),
    );
  }
}
