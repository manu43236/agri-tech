import 'package:agritech/app/modules/farm_worker_home_page/controllers/farm_worker_controller.dart';
import 'package:agritech/app/modules/utility/controllers/utility_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../models/get_user_model.dart';
import '../../home/controllers/home_controller.dart';
import '../../home/controllers/landlord_controller.dart';
import '../../home/services/get_user_repo_impl.dart';
import '../../vendor/controllers/vendor_controller.dart';

class DashboardController extends BaseController {
  RxInt currentIndex = 0.obs;
  var role = "".obs;
  var id = "".obs;

  RxBool isTopNavigation = false.obs;

  Rx<GetUserModel> getUserModel = Rx(GetUserModel());

  @override
  void onInit() async{
    super.onInit();
    print('dashboard on in it');
    id.value = await SecureStorage().readData(key: "id");
    role.value= await SecureStorage().readData(key: "role");
    print(role.value);
    //await getUserById(id:id.value);
    // .then((value) {
    //   print(id.value);
    //   Location().determinePosition().then((val) {
    //     updateIndex(currentIndex.value);
    //   });
      
    // });
    print(currentIndex.value);
  }

  @override
  void onReady() {
    super.onReady();
  }



  Future getUserById({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    // id.value = await SecureStorage().readData(key: "id");
    try {
      var result =
          await GetUserRepoImpl(dioClient).getUser(id: id);
      print("======================== user by id ====================");
      print(result);
      result.fold((model) async {
        getUserModel.value = model;
        await SecureStorage().writeStringData(
            "role",
            getUserModel.value.result != null
                ? getUserModel.value.result!.role.toString()
                : '');
        role.value = await SecureStorage().readData(key: "role");
        print('role dashboard:${role.value}');
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void updateIndex(int index) async {
    currentIndex.value = index;
    role.value = await SecureStorage().readData(key: "role");
    print("dashboard after initi");
    print('role:${role.value}');
    if (currentIndex.value == 0) {
      //userId.value = await SecureStorage().readData(key: "id");
      if (role.value == 'Landlord') {
        Get.find<HomeController>()
            .getUserById(id:id.value );
        Get.find<LandlordController>().recentLands();
        Get.find<LandlordController>().addAllLands(userId:id.value  );
        Get.find<LandlordController>().addAlljobs(userId: id.value);
        Get.find<LandlordController>().leasedUtilities(userId: id.value);

      } else if (role.value == 'Farmer') {
        Get.find<HomeController>()
            .getUserById(id: id.value );
        Get.find<LandlordController>().recentLands();
        Get.find<LandlordController>().addAllFarmersLands(userId: id.value);
        Get.find<LandlordController>().addAllFarmerjobs(userId: id.value);
        Get.find<LandlordController>().leasedUtilities(userId: id.value);
        Get.find<LandlordController>().leasedLands(userId: id.value,role: role.value);
      } else if (role.value == 'Farm Worker') {
        Get.find<HomeController>()
            .getUserById(id:id.value );
        Get.find<FarmWorkerHomePageController>().filterJobs(state: '',district: '',mandal: '',village: '',search: '');
        Get.find<LandlordController>().leasedJobs(userId: id.value,role: role.value);
        Get.find<LandlordController>().leasedUtilities(userId: id.value);
        Get.find<FarmWorkerHomePageController>().getUserByIdSkills(userId: id.value);
      } else if (role.value == 'Utiliser') {
        Get.find<HomeController>()
            .getUserById(id:id.value );
        Get.find<UtilityController>().userUtilities(userId: id.value,search: '',state: '',district: '',mandal: '',village: '');
      } else if(role.value=='Vendor'){
        Get.find<HomeController>()
            .getUserById(id:id.value );
        Get.find<VendorController>().recentCrops();
    }
    } 
  }

  @override
  void onClose() {
    super.onClose();
  }
}
