import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/conts/img_const.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../routes/app_pages.dart';
import '../controllers/loans_view_controller.dart';

class LoansView extends GetView<LoansViewController> {
  const LoansView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 30),
        child: Column(
          children: [
            InkWell(
              onTap: () {
                if (Get.find<HomeController>().role.value == "Landlord") {
                  Get.toNamed(Routes.AVAILABLE_MY_LANDS,arguments: {"isLoan":true,"isInsurance":false,"heading":''});
                } else if (Get.find<HomeController>().role.value == "Farmer") {
                  Get.toNamed(Routes.AVAILABLE_FARMER_MY_LANDS,arguments: {"isLoan":true,"isInsurance":false,"heading":''});
                } else if (Get.find<HomeController>().role.value ==
                    "Utiliser") {
                  Get.toNamed(Routes.AVAILABLE_MY_UTILITIES,arguments: {"isLoan":true,"isInsurance":false,"heading":''});
                }
              },
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15), color: colorLoans),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Image.asset(
                      loans,
                      height: Get.height * 0.15,
                      width: Get.width * 0.2,
                    ),
                    Text(
                      'loans'.tr,
                      style: TextStyles.kTSFS18W500.copyWith(color: colorHello),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            InkWell(
              onTap: () {
                if (Get.find<HomeController>().role.value == "Landlord") {
                  Get.toNamed(Routes.AVAILABLE_MY_LANDS,arguments: {"isInsurance":true,"isLoan":false,"heading":''});
                } else if (Get.find<HomeController>().role.value == "Farmer") {
                  Get.toNamed(Routes.AVAILABLE_FARMER_MY_LANDS,arguments: {"isInsurance":true,"isLoan":false,"heading":''});
                } else if (Get.find<HomeController>().role.value ==
                    "Utiliser") {
                  Get.toNamed(Routes.AVAILABLE_MY_UTILITIES,arguments: {"isInsurance":true,"isLoan":false,"heading":''});
                }
              },
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15), color: colorLoans),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Image.asset(
                      insurance,
                      height: Get.height * 0.15,
                      width: Get.width * 0.2,
                    ),
                    Text(
                      'insurance'.tr,
                      style: TextStyles.kTSFS18W500.copyWith(color: colorHello),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
