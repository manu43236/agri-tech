// To parse this JSON data, do
//
//     final filterLandsByLandlordIdModel = filterLandsByLandlordIdModelFromJson(jsonString);

import 'dart:convert';

FilterLandsByLandlordIdModel filterLandsByLandlordIdModelFromJson(String str) => FilterLandsByLandlordIdModel.fromJson(json.decode(str));

String filterLandsByLandlordIdModelToJson(FilterLandsByLandlordIdModel data) => json.encode(data.toJson());

class FilterLandsByLandlordIdModel {
    int? statusCode;
    String? status;
    String? message;
    List<Result>? result;

    FilterLandsByLandlordIdModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory FilterLandsByLandlordIdModel.fromJson(Map<String, dynamic> json) => FilterLandsByLandlordIdModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    int? landLordId;
    dynamic farmerId;
    String? fullName;
    int? landInAcres;
    dynamic surveyNum;
    String? water;
    String? village;
    String? description;
    String? mandal;
    String? district;
    String? state;
    dynamic latitude;
    dynamic longitude;
    dynamic radius;
    int? imageId;
    String? imageUrl;
    String? status;
    bool? flag;
    DateTime? createdAt;
    DateTime? updatedAt;

    Result({
        this.id,
        this.landLordId,
        this.farmerId,
        this.fullName,
        this.landInAcres,
        this.surveyNum,
        this.water,
        this.village,
        this.description,
        this.mandal,
        this.district,
        this.state,
        this.latitude,
        this.longitude,
        this.radius,
        this.imageId,
        this.imageUrl,
        this.status,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        landInAcres: json["landInAcres"],
        surveyNum: json["surveyNum"],
        water: json["water"],
        village: json["village"],
        description: json["description"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        radius: json["radius"],
        imageId: json["image_id"],
        imageUrl: json["image_url"],
        status: json["status"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "fullName": fullName,
        "landInAcres": landInAcres,
        "surveyNum": surveyNum,
        "water": water,
        "village": village,
        "description": description,
        "mandal": mandal,
        "district": district,
        "state": state,
        "latitude": latitude,
        "longitude": longitude,
        "radius": radius,
        "image_id": imageId,
        "image_url": imageUrl,
        "status": status,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
