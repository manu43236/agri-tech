import 'package:dartz/dartz.dart';
import '../data/get_all_saved_utility_model.dart';

abstract class SavedUtilitiesRepo{
  Future<Either<GetAllSavedUtilityModel,Exception>> getSavedUtilities();
}