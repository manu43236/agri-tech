import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/conts/app_consts.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../routes/app_pages.dart';

class SplashScreenController extends BaseController {
  
  String logo = 'assets/images/agriLogo.png';

  var token = ''.obs;
  var roles = ''.obs;
  var selectedLang=''.obs;

  @override
  void onInit() async{
    super.onInit();
    // Location location = Location();
    // await location.determinePosition();
    sharedpref();
  }

  void sharedpref() async {
    token.value = await SecureStorage().readData(key: TOKEN) ?? '';
    await SecureStorage().writeStringData("lang",'en',);
    // selectedLang.value= await SecureStorage().readData(key:"lang");
    // print(selectedLang.value);
    print('======================token data================');
    print(token.value);
    if (token.value.isNotEmpty) {
      Future.delayed(const Duration(seconds: 2), () {
        Get.toNamed(Routes.DASHBOARD);
      });
    } else if (token.value.isEmpty) {
      Future.delayed(const Duration(seconds: 2), () {
        Get.toNamed(Routes.WELCOME_PAGE);
      });
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
