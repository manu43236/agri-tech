import 'package:get/get.dart';

import '../controllers/vendor_checkout_details_controller.dart';

class VendorCheckoutDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VendorCheckoutDetailsController>(
      () => VendorCheckoutDetailsController(),
    );
  }
}
