// To parse this JSON data, do
//
//     final createUtilityPaymentModel = createUtilityPaymentModelFromJson(jsonString);

import 'dart:convert';

CreateUtilityPaymentModel createUtilityPaymentModelFromJson(String str) => CreateUtilityPaymentModel.fromJson(json.decode(str));

String createUtilityPaymentModelToJson(CreateUtilityPaymentModel data) => json.encode(data.toJson());

class CreateUtilityPaymentModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    CreateUtilityPaymentModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory CreateUtilityPaymentModel.fromJson(Map<String, dynamic> json) => CreateUtilityPaymentModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? id;
    int? userId;
    int? utilityId;
    dynamic quantity;
    dynamic costOfSelectedItem;
    dynamic tenPercentGst;
    dynamic nintyPercentGst;
    dynamic totalPayment;
    dynamic tenPercentPrice;
    dynamic ninetyPercentPrice;
    dynamic tenPercentPayment;
    dynamic nintyPercentpayment;
    dynamic paymentStatus;
    dynamic amountPaid;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic razorpayOrderId;
    dynamic razorpayPaymentId;
    dynamic razorpaySignature;
    UtilityDetails? utilityDetails;

    Result({
        this.id,
        this.userId,
        this.utilityId,
        this.quantity,
        this.costOfSelectedItem,
        this.tenPercentGst,
        this.nintyPercentGst,
        this.totalPayment,
        this.tenPercentPrice,
        this.ninetyPercentPrice,
        this.tenPercentPayment,
        this.nintyPercentpayment,
        this.paymentStatus,
        this.amountPaid,
        this.updatedAt,
        this.createdAt,
        this.razorpayOrderId,
        this.razorpayPaymentId,
        this.razorpaySignature,
        this.utilityDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        userId: json["userId"],
        utilityId: json["utilityId"],
        quantity: json["quantity"],
        costOfSelectedItem: json["costOfSelectedItem"],
        tenPercentGst: json["tenPercentGST"]?.toDouble(),
        nintyPercentGst: json["nintyPercentGST"]?.toDouble(),
        totalPayment: json["totalPayment"],
        tenPercentPrice: json["tenPercentPrice"]?.toDouble(),
        ninetyPercentPrice: json["ninetyPercentPrice"]?.toDouble(),
        tenPercentPayment: json["tenPercentPayment"]?.toDouble(),
        nintyPercentpayment: json["nintyPercentpayment"]?.toDouble(),
        paymentStatus: json["paymentStatus"],
        amountPaid: json["amountPaid"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        razorpayOrderId: json["razorpay_order_id"],
        razorpayPaymentId: json["razorpay_payment_id"],
        razorpaySignature: json["razorpay_signature"],
        utilityDetails: json["utilityDetails"] == null ? null : UtilityDetails.fromJson(json["utilityDetails"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "utilityId": utilityId,
        "quantity": quantity,
        "costOfSelectedItem": costOfSelectedItem,
        "tenPercentGST": tenPercentGst,
        "nintyPercentGST": nintyPercentGst,
        "totalPayment": totalPayment,
        "tenPercentPrice": tenPercentPrice,
        "ninetyPercentPrice": ninetyPercentPrice,
        "tenPercentPayment": tenPercentPayment,
        "nintyPercentpayment": nintyPercentpayment,
        "paymentStatus": paymentStatus,
        "amountPaid": amountPaid,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "razorpay_order_id": razorpayOrderId,
        "razorpay_payment_id": razorpayPaymentId,
        "razorpay_signature": razorpaySignature,
        "utilityDetails": utilityDetails?.toJson(),
    };
}

class UtilityDetails {
    int? id;
    dynamic name;
    dynamic description;
    dynamic pricePerDay;
    dynamic image;
    Address? address;

    UtilityDetails({
        this.id,
        this.name,
        this.description,
        this.pricePerDay,
        this.image,
        this.address,
    });

    factory UtilityDetails.fromJson(Map<String, dynamic> json) => UtilityDetails(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        pricePerDay: json["pricePerDay"],
        image: json["image"],
        address: json["address"] == null ? null : Address.fromJson(json["address"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "pricePerDay": pricePerDay,
        "image": image,
        "address": address?.toJson(),
    };
}

class Address {
    dynamic state;
    dynamic district;
    dynamic mandal;
    dynamic village;

    Address({
        this.state,
        this.district,
        this.mandal,
        this.village,
    });

    factory Address.fromJson(Map<String, dynamic> json) => Address(
        state: json["state"],
        district: json["district"],
        mandal: json["mandal"],
        village: json["village"],
    );

    Map<String, dynamic> toJson() => {
        "state": state,
        "district": district,
        "mandal": mandal,
        "village": village,
    };
}
