import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../core/conts/color_consts.dart';

class CustomDatePicker extends StatelessWidget {
  final TextEditingController controller;

  CustomDatePicker({required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
      decoration: BoxDecoration(
        border: Border.all(color: colorTextFieldBorder, width: 2),
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: TextFormField(
        controller: controller, // editing controller of this TextField
        decoration: InputDecoration(
          border: InputBorder.none,
          icon: Icon(Icons.calendar_today), // icon of text field
        ),
        readOnly: true, // set it true, so that user will not able to edit text
        onTap: () async {
          DateTime? pickedDate = await showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2000), // DateTime.now() - not to allow to choose before today.
            lastDate: DateTime(2101),
          );
      
          if (pickedDate != null) {
            String formattedDate = DateFormat('yyyy-MM-dd').format(pickedDate);
            controller.text = formattedDate; // set output date to TextField value.
          } else {
            print('Date is not selected');
          }
        },
      ),
    );
  }
}
