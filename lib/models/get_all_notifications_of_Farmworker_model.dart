// To parse this JSON data, do
//
//     final getAllNotificationsByFarmworkerModel = getAllNotificationsByFarmworkerModelFromJson(jsonString);

import 'dart:convert';

GetAllNotificationsByFarmworkerModel getAllNotificationsByFarmworkerModelFromJson(String str) => GetAllNotificationsByFarmworkerModel.fromJson(json.decode(str));

String getAllNotificationsByFarmworkerModelToJson(GetAllNotificationsByFarmworkerModel data) => json.encode(data.toJson());

class GetAllNotificationsByFarmworkerModel {
    int? statusCode;
    String? status;
    String? message;
    List<Result>? result;

    GetAllNotificationsByFarmworkerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetAllNotificationsByFarmworkerModel.fromJson(Map<String, dynamic> json) => GetAllNotificationsByFarmworkerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    int? from;
    String? message;
    int? jobId;
    String? name;
    String? imageUrl;
    bool? isRead;
    String? statusForFarmworker;
    DateTime? createdAt;
    DateTime? updatedAt;

    Result({
        this.id,
        this.from,
        this.message,
        this.jobId,
        this.name,
        this.imageUrl,
        this.isRead,
        this.statusForFarmworker,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        from: json["from"],
        message: json["message"],
        jobId: json["jobId"],
        name: json["name"],
        imageUrl: json["image_url"],
        isRead: json["is_read"],
        statusForFarmworker: json["status_for_farmworker"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "from": from,
        "message": message,
        "jobId": jobId,
        "name": name,
        "image_url": imageUrl,
        "is_read": isRead,
        "status_for_farmworker": statusForFarmworker,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
