import 'package:agritech/app/modules/details_of_farmer/services/details_of_farmers_repo.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../data/get_all_available_lands_of_landlord_model.dart';
import '../data/near_by_farmers_model.dart';
import '../data/send_notification_by_landlord_fetch_model.dart';

class DetailsOfFarmersRepoImpl extends DetailsOfFarmersRepo with NetworkCheckService{
  final DioClient _dioClient;
  DetailsOfFarmersRepoImpl(this._dioClient);

  Future<Either<NearByFarmersModel,Exception>> nearByFarmers({latitude,longitude})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Land/filterNearFarmers?latitude=$latitude&longitude=$longitude', Method.get);
        return result.fold((l){
          NearByFarmersModel model= NearByFarmersModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  Future<Either<GetAllAvailableLandsOfLandlordModel,Exception>> availableLands({String? id})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('Land/getAllAvailableLandsByLandLord?landLordId=${id}', Method.get);
        return result.fold((l){
          GetAllAvailableLandsOfLandlordModel model= GetAllAvailableLandsOfLandlordModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<SendNotificationByLandLordFetchModel,Exception>> fetchdetails({params1})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result = await _dioClient.requestForAuth('notification/sendNotificationByLandLord', Method.post,params: params1);
        return result.fold((l){
          SendNotificationByLandLordFetchModel model= SendNotificationByLandLordFetchModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}