import 'package:agritech/app/modules/details_of_farmer/data/get_all_available_lands_of_landlord_model.dart';
import 'package:agritech/app/modules/details_of_farmer/data/near_by_farmers_model.dart';
import 'package:agritech/app/modules/details_of_farmer/services/details_of_farmers_repo_impl.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../models/get_user_model.dart';
import '../../../../models/send_notification_by_landlord_model.dart';
import '../../home/data/all_lands_of_landlord.dart';
import '../../home/services/get_user_repo_impl.dart';
import '../../home/services/home_repo_impl.dart';
import '../../home/services/send_notification_by_landlord_repo_impl.dart';
import '../../saved/data/get_all_saved_farmer_model.dart';
import '../../saved/services/saved_repo_impl.dart';
import '../data/send_notification_by_landlord_fetch_model.dart';

class DetailsOfFarmerController extends BaseController {
  Rx<NearByFarmersModel> nearByFarmersModel = Rx(NearByFarmersModel());
  Rx<GetAllAvailableLandsOfLandlordModel> getAllAvailableLandsOfLandlordModel =
      Rx(GetAllAvailableLandsOfLandlordModel());
  Rx<SendNotificationByLandlordModel> sendNotificationByLandlordModel =
      Rx(SendNotificationByLandlordModel());
  Rx<SendNotificationByLandLordFetchModel>
      sendNotificationByLandLordFetchModel =
      Rx(SendNotificationByLandLordFetchModel());
  Rx<GetUserModel> getUserProfileModel = Rx(GetUserModel());
  Rx<GetAllSavedFarmerModel> getAllSavedFarmerModel =
      Rx(GetAllSavedFarmerModel());
  Rx<AddLandsOfLandlordModel> addLandsOfLandlordModel =
      Rx(AddLandsOfLandlordModel());

  final HomeController homeController=Get.find<HomeController>();

  var ongoingLandDescriptions = <Map<String, String>>[].obs;
  var selectedLandId = ''.obs;
  var selectedCrop = ''.obs;
  var selectedId = false.obs;

  var apiStatusFarmer = false.obs;
  Rx<List<SavedFarmer>> savedFarmers = Rx([]);
  var apiUserByProfileIdStatus=ApiStatus.LOADING.obs;
  var farmerFavourite = false.obs;
  var deleteId = 0.obs;
  var farmerId=0.obs;
  

  var selectedLand = Rxn<Map<String, String>>();
  

  var statusForLandlord = ''.obs;
  var fetchedLandId=''.obs;
  var isSelectedLandPresent = ''.obs;

  var id=''.obs;
  var role=''.obs;

  @override
  void onInit() async {
    super.onInit();
    id.value= await SecureStorage().readData(key: "id"??'');
    role.value=await SecureStorage().readData(key:"role"??'');
    await allFarmersSaved(id: id.value);
    if(Get.arguments!=null){
       await getUserByProfileId(id: Get.arguments["farmerId"]);
    }
    ongoingLandDescriptions = ongoingLandDescriptions;
    addAllLands(userId:  id.value);
    availableLandsList(id( id.value));
    FetchedLandsDetails(userId:  id.value,farmerId: farmerId.value);
    nearByFarmers(latitude: homeController.latitude.value,longitude: homeController.longitude.value);
  }

  void setSelectedCrop(String crop) {
    selectedCrop.value = crop;
  }

  void selectLand(String landName) {
    // Handle land selection logic
    print("Selected land: $landName");
    Get.back(); // Close the bottom sheet
  }

  //near by farmers API
  Future<void> nearByFarmers({latitude,longitude}) async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result = await DetailsOfFarmersRepoImpl(dioClient).nearByFarmers(latitude: latitude,longitude: longitude);
      print('=============== near by farmers =========');
      print(result);
      result.fold((model) {
        nearByFarmersModel.value = model;
        print(nearByFarmersModel.value.result!.farmers![0].firstName);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //available lands list API
  Future<void> availableLandsList(String id) async {
    apiStatus.value = ApiStatus.LOADING;

    try {
      var result =
          await DetailsOfFarmersRepoImpl(dioClient).availableLands(id: id);
      print('=============== available lands list =========');
      print(result);
      result.fold((model) {
        getAllAvailableLandsOfLandlordModel.value = model;
        print(getAllAvailableLandsOfLandlordModel.value.result![0].status);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //send notification by landlord API
  Future<void> sentNotificationByLandlordId({landlordId,farmerId,landId}) async {
    apiStatusFarmer.value = false;
    try {
      var result = await SendNotificationByLandlordRepoImpl(dioClient)
          .sentNotification(params1: {
        "landLordId":landlordId, //Get.find<HomeController>().userId.value,
        "farmerId": farmerId,//Get.find<HomeController>().farmerId.value,
        "landId": landId//selectedLandId.value
      });
      print('land id is:${selectedLandId.value}');
      print('landlord id is:${Get.find<HomeController>().userId.value}');
      print('farmer id is:${Get.find<HomeController>().farmerId.value}');
      print(
          "======================== sent notification by landlord id ====================");
      print(result);
      result.fold((model) {
        sendNotificationByLandlordModel.value = model;
        print(sendNotificationByLandlordModel.value.result != null
            ? sendNotificationByLandlordModel
                .value.result!.notification!.message
            : '');
        if (sendNotificationByLandlordModel.value.statusCode == 200) {
          Get.snackbar('Success', 'Notification sent successfully',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorGreen,
              colorText: colorBlack);
        } else if (sendNotificationByLandlordModel.value.statusCode == 400) {
          Get.snackbar('Failure', 'Notification already sent to the farmer',
              snackPosition: SnackPosition.TOP,
              backgroundColor: colorRed,
              colorText: colorWhite);
        }
        apiStatusFarmer.value = true;
      }, (r) {
        apiStatusFarmer.value = false;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //Fetched Lands Details API
 Future<void> FetchedLandsDetails({userId,farmerId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await DetailsOfFarmersRepoImpl(dioClient).fetchdetails(params1: {
        "landLordId":userId ,//Get.find<HomeController>().userId.value,
        "farmerId":farmerId //Get.find<HomeController>().farmerId.value,
      });
      print('landlord id is:${Get.find<HomeController>().userId.value}');
      print('farmer id is:${Get.find<HomeController>().farmerId.value}');
      print(
          "======================== land details fetched while selected a land ====================");
      print(result);
      result.fold((model) {
        if (model.statusCode == 200) {
          sendNotificationByLandLordFetchModel.value = model;
              for (var element in sendNotificationByLandLordFetchModel.value.result!) {
          print('for loop');
          print(element.landDetails!.id);
          print(selectedLandId.value);
          statusForLandlord.value == "";
          if (element.landDetails!.id.toString() == selectedLandId.value) {
            print('elwmmmmmmmmmm');
            statusForLandlord.value = element.statusForLandlord!;
          }
        } 
        } 
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

    // Get user profile by id API
  Future<void> getUserByProfileId({id}) async {
    apiUserByProfileIdStatus.value = ApiStatus.LOADING;
    try {
      var result =
          await GetUserRepoImpl(dioClient).getUser(id: id);
      print("======================== user by id ====================");
      print('Result $result');
      result.fold((model) {
        getUserProfileModel.value = model;
        farmerId.value=getUserProfileModel.value.result!.id!;
          farmerFavourite.value = false;
          print('Farmer favourite: ${farmerFavourite.value}');
          final results = getAllSavedFarmerModel
                  .value
                  .result
                  ?.savedFarmers ??
              [];
          print('Saved farmers: $results');

          for (var element in results) {
            if (element.farmerid == getUserProfileModel.value.result?.id) {
              deleteId.value = element.savedFarmerId ?? 0;
              print('Delete ID for farmer: ${deleteId.value}');
              farmerFavourite.value = true;
            }
          }

        apiUserByProfileIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiUserByProfileIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

   Future<void> allFarmersSaved({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient)
          .savedFarmersList(userId: id);
      result.fold(
        (model) {
          getAllSavedFarmerModel.value = model;
          apiStatus.value = ApiStatus.SUCCESS;
        },(r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  // my Lands View API
  Future<void> addAllLands({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      //landlordId.value = Get.arguments as String;
      var result =
          await HomeRepoImpl(dioClient).doAddAllLands(id:userId);
      print('llllllllllllllllllllllllllllllllllllllllll');
      print('results:$result');
      result.fold((left) {
        allLandshandleResponse(left);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void allLandshandleResponse(AddLandsOfLandlordModel model) async {
    print('kkkkkkkkkkkkkkkkkkkkk');
    print(model);
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
      addLandsOfLandlordModel.value.result?.ongoingLands?.clear();
      addLandsOfLandlordModel.value = model;
      ongoingLandDescriptions.value =
          addLandsOfLandlordModel.value.result!.ongoingLands!.map((land) {
        return {
          land.id.toString():
              "${land.fullName} job at ${land.village}"
        };
      }).toList();
    }
  }


  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
