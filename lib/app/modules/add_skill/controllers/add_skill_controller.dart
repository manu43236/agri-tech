import 'package:agritech/app/modules/farm_worker_home_page/controllers/farm_worker_controller.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../routes/app_pages.dart';
import '../../dashboard/controllers/dashboard_controller.dart';
import '../data/add_skill_model.dart';
import '../services/add_skill_repo_impl.dart';

class AddSkillController extends BaseController {
  var nameController = TextEditingController().obs;
  var expirenceController = TextEditingController().obs;
  var descriptionController = TextEditingController().obs;

  var selectedSuffix = "Years".obs;
  var userId=''.obs;

  @override
  void onInit() async{
    userId.value=await SecureStorage().readData(key: "id"??'');
    super.onInit();
  }

  void updateSuffix(String value) {
    selectedSuffix.value = value; // Update the dropdown value
  }

  //Add Skill by Farmworker API
  void addSkill() async {
    apiStatus.value = ApiStatus.LOADING;
    if (nameController.value.text.isNotEmpty &&
        descriptionController.value.text.isNotEmpty &&
        expirenceController.value.text.isNotEmpty) {
      try {
        print('000000000000000000000000000000000000');
        print(Get.find<HomeController>().userId.value);
        print(nameController.value.text);
        print(expirenceController.value.text);
        print(descriptionController.value.text);
        var result = await AddSkillRepoImpl(dioClient).addSkill(params1: {
          "userId": int.parse(Get.find<HomeController>().userId.value),
          "skill": nameController.value.text,
          "experience": expirenceController.value.text,
          "description": descriptionController.value.text
        });
        print("result:$result");
        result.fold((left) {
          _handleResponse(left);
          apiStatus.value = ApiStatus.SUCCESS;
        }, (right) {
          apiStatus.value = ApiStatus.FAIL;
        });
      } catch (e) {
        print('Error: $e');
      }
    } else {
      print('One or more fields are empty.');
    }
  }

  void _handleResponse(AddSkillModel model) async {
    if (model.statusCode == 200) {
      Get.find<DashboardController>().currentIndex.value = 0;
      Get.find<HomeController>().selectedIndex.value = 0;
      Get.find<FarmWorkerHomePageController>().getUserByIdSkills(userId: userId.value);
      Get.offNamed(Routes.DASHBOARD);
      nameController.value.clear();
      expirenceController.value.clear();
      descriptionController.value.clear();
      Get.snackbar('Success', model.message,
          backgroundColor: colorGreen, colorText: colorBlack);
    } else if (model.statusCode == 400) {
      Get.snackbar('Failed', 'missing fields',
          backgroundColor: colorRed,
          colorText: colorWhite,
          snackPosition: SnackPosition.TOP);
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
