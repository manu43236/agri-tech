// To parse this JSON data, do
//
//     final sendNotificationByFarmerModel = sendNotificationByFarmerModelFromJson(jsonString);

import 'dart:convert';

SendNotificationByFarmerModel sendNotificationByFarmerModelFromJson(String str) => SendNotificationByFarmerModel.fromJson(json.decode(str));

String sendNotificationByFarmerModelToJson(SendNotificationByFarmerModel data) => json.encode(data.toJson());

class SendNotificationByFarmerModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    SendNotificationByFarmerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SendNotificationByFarmerModel.fromJson(Map<String, dynamic> json) => SendNotificationByFarmerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    bool? isRead;
    int? id;
    dynamic message;
    dynamic to;
    dynamic name;
    dynamic from;
    int? landId;
    dynamic imageUrl;
    dynamic statusForFarmer;
    bool? flag;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic statusForLandlord;
    dynamic jobId;
    dynamic statusForFarmworker;

    Result({
        this.isRead,
        this.id,
        this.message,
        this.to,
        this.name,
        this.from,
        this.landId,
        this.imageUrl,
        this.statusForFarmer,
        this.flag,
        this.updatedAt,
        this.createdAt,
        this.statusForLandlord,
        this.jobId,
        this.statusForFarmworker,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        isRead: json["is_read"],
        id: json["id"],
        message: json["message"],
        to: json["to"],
        name: json["name"],
        from: json["from"],
        landId: json["landId"],
        imageUrl: json["image_url"],
        statusForFarmer: json["status_for_farmer"],
        flag: json["flag"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        statusForLandlord: json["status_for_landlord"],
        jobId: json["jobId"],
        statusForFarmworker: json["status_for_farmworker"],
    );

    Map<String, dynamic> toJson() => {
        "is_read": isRead,
        "id": id,
        "message": message,
        "to": to,
        "name": name,
        "from": from,
        "landId": landId,
        "image_url": imageUrl,
        "status_for_farmer": statusForFarmer,
        "flag": flag,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "status_for_landlord": statusForLandlord,
        "jobId": jobId,
        "status_for_farmworker": statusForFarmworker,
    };
}
