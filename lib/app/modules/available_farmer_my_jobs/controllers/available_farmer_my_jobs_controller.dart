import 'package:agritech/app/modules/available_farmer_my_jobs/services/available_farmer_my_jobs_repo_impl.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../available_my_jobs/data/jobs_filter_model.dart';
import '../../available_my_jobs/services/available_my_jobs_repo_impl.dart';
import '../../home/data/get_all_jobs_by_farmer_model.dart';

class AvailableFarmerMyJobsController extends BaseController {

Rx<GetAllJobsByFarmerModel> getJobsByFarmerModel =
      Rx(GetAllJobsByFarmerModel());
Rx<JobsFilterModel> farmerMyJobsFilterModel = Rx(JobsFilterModel());

 var name = "".obs;
 var farmerMyJobsSearch=''.obs;
 var apiFarJobsStatus=ApiStatus.LOADING.obs;
 var apiFilterFarmerJobsStatus=ApiStatus.LOADING.obs;
 var dataSelectedItem=''.obs;

  @override
  void onInit() async{
    if (Get.arguments != null) {
      name.value = Get.arguments["heading"];
    }
    super.onInit();
    //await getFarmerjobs();
    farmerMyJobsFilterJobsList(landlordId: '',farmerId: Get.find<HomeController>().userId.value,
    search: farmerMyJobsSearch.value,state: '',district: '',mandal: '',village: '' );
  }

  //available farmer search API
  Future<void> getFarmerjobs() async {
    apiFarJobsStatus.value = ApiStatus.LOADING;
    try {
      print('hiiiiiiiii');
      var result = await AvailableFarmerMyJobsRepoImpl(dioClient)
          .getFarmerSearchJobs();
      print("========================All jobs of farmer====================");
      print(result);
      result.fold((left) {
        allFarmerJobshandleResponse(left);
        apiFarJobsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiFarJobsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void allFarmerJobshandleResponse(GetAllJobsByFarmerModel model) async {
    if (model.statusCode == 200) {
      print('============model response=====');
      print(model);
      getJobsByFarmerModel.value = model;
      print(getJobsByFarmerModel.value.result!.ongoingJobs?[0].description);
    }else if(model.statusCode==400){
      print('===============status=============');
      getJobsByFarmerModel.value.result!.ongoingJobs!.clear();
      getJobsByFarmerModel.value.result!.ongoingJobs ==[];
    }
  }

  void farmerMyJobsFilterJobsList({landlordId,farmerId,search,state,district,mandal,village}) async {
    apiFilterFarmerJobsStatus.value = ApiStatus.LOADING;

    try {
      var result = await AvailableMyJobsRepoImpl(dioClient).filterOwnJobs(landlordId: landlordId,farmerId: farmerId,search: search, state: state,district: district,mandal: mandal,village: village);
      print('=============== get all filter my jobs list =========');
      print(result);
      result.fold((left) {
        filterFarmerJobshandleResponse(left);
        apiFilterFarmerJobsStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiFilterFarmerJobsStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void filterFarmerJobshandleResponse(JobsFilterModel model) async {
    if (model.statusCode == 200) {
      farmerMyJobsFilterModel.value.result?.clear();
      farmerMyJobsFilterModel.value = model;
    }else if(model.statusCode==400){
      print('===============status=============');
      farmerMyJobsFilterModel.value.result?.clear();
      farmerMyJobsFilterModel.value.result! ==[];
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
