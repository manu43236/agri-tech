import 'package:agritech/models/profile_model.dart';
import 'package:dartz/dartz.dart';

abstract class EditProfileRepo{
  Future<Either<ProfileModel,Exception>> doEditProfile({params1});
}