import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../data/get_ninty_percent_crop_payment_details_model.dart';
import '../services/purchased_checkout_details_repo_impl.dart';

class PurchasedCheckoutDetailsController extends BaseController {

  Rx<GetNintyPercentCropPaymentModel> getNintyPercentCropPaymentModel =
      Rx(GetNintyPercentCropPaymentModel());

  var apiNintyPercentDetails=ApiStatus.LOADING.obs;

  @override
  void onInit() {
    super.onInit();
    if(Get.arguments!=null){
      purchasedCrops(landId: Get.arguments["landId"],cropId: Get.arguments["cropId"],quantity: Get.arguments["quantity"]);
    }
  }

  //purchased crops API
  Future<void> purchasedCrops({landId,cropId,quantity}) async {
    apiNintyPercentDetails.value = ApiStatus.LOADING;
    try {
      var result = await PurchasedCheckoutDetailsRepoImpl(dioClient).getDetails(landId: landId,cropId: cropId,quantity: quantity);
      print('=============== get ninty percent crop details =========');
      print(result);
      result.fold((model) {
        getNintyPercentCropPaymentModel.value = model;
        apiNintyPercentDetails.value = ApiStatus.SUCCESS;
      }, (r) {
        apiNintyPercentDetails.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
