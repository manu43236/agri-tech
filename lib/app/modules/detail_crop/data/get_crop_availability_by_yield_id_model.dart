// To parse this JSON data, do
//
//     final getCropAvailabilityByYieldIdModel = getCropAvailabilityByYieldIdModelFromJson(jsonString);

import 'dart:convert';

GetCropAvailabilityByYieldIdModel getCropAvailabilityByYieldIdModelFromJson(String str) => GetCropAvailabilityByYieldIdModel.fromJson(json.decode(str));

String getCropAvailabilityByYieldIdModelToJson(GetCropAvailabilityByYieldIdModel data) => json.encode(data.toJson());

class GetCropAvailabilityByYieldIdModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetCropAvailabilityByYieldIdModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetCropAvailabilityByYieldIdModel.fromJson(Map<String, dynamic> json) => GetCropAvailabilityByYieldIdModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    int? yieldId;
    int? cropId;
    dynamic cropName;
    dynamic cropImage;
    dynamic totaldays;
    dynamic costPerHector;
    int? landId;
    dynamic ownerName;
    LandDetails? landDetails;
    YieldDetails? yieldDetails;

    Result({
        this.yieldId,
        this.cropId,
        this.cropName,
        this.cropImage,
        this.totaldays,
        this.costPerHector,
        this.landId,
        this.ownerName,
        this.landDetails,
        this.yieldDetails,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        yieldId: json["yieldId"],
        cropId: json["cropId"],
        cropName: json["cropName"],
        cropImage: json["cropImage"],
        totaldays: json["totaldays"],
        costPerHector: json["costPerHector"],
        landId: json["landId"],
        ownerName: json["ownerName"],
        landDetails: json["landDetails"] == null ? null : LandDetails.fromJson(json["landDetails"]),
        yieldDetails: json["yieldDetails"] == null ? null : YieldDetails.fromJson(json["yieldDetails"]),
    );

    Map<String, dynamic> toJson() => {
        "yieldId": yieldId,
        "cropId": cropId,
        "cropName": cropName,
        "cropImage": cropImage,
        "totaldays": totaldays,
        "costPerHector": costPerHector,
        "landId": landId,
        "ownerName": ownerName,
        "landDetails": landDetails?.toJson(),
        "yieldDetails": yieldDetails?.toJson(),
    };
}

class LandDetails {
    int? id;
    int? landLordId;
    dynamic farmerId;
    dynamic landInAcres;
    dynamic village;
    dynamic district;
    dynamic state;
    dynamic description;
    dynamic latitude;
    dynamic longitude;
    dynamic surveyNum;
    dynamic mandal;
    dynamic fullName;
    dynamic water;

    LandDetails({
        this.id,
        this.landLordId,
        this.farmerId,
        this.landInAcres,
        this.village,
        this.district,
        this.state,
        this.description,
        this.latitude,
        this.longitude,
        this.surveyNum,
        this.mandal,
        this.fullName,
        this.water,
    });

    factory LandDetails.fromJson(Map<String, dynamic> json) => LandDetails(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        landInAcres: json["landInAcres"],
        village: json["village"],
        district: json["district"],
        state: json["state"],
        description: json["description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        surveyNum: json["surveyNum"],
        mandal: json["mandal"],
        fullName: json["fullName"],
        water: json["water"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "landInAcres": landInAcres,
        "village": village,
        "district": district,
        "state": state,
        "description": description,
        "latitude": latitude,
        "longitude": longitude,
        "surveyNum": surveyNum,
        "mandal": mandal,
        "fullName": fullName,
        "water": water,
    };
}

class YieldDetails {
    dynamic outedDate;
    dynamic hectors;
    bool? status;
    dynamic remainingQuantity;
    dynamic quantity;

    YieldDetails({
        this.outedDate,
        this.hectors,
        this.status,
        this.remainingQuantity,
        this.quantity,
    });

    factory YieldDetails.fromJson(Map<String, dynamic> json) => YieldDetails(
        outedDate: json["outedDate"] == null ? null : DateTime.parse(json["outedDate"]),
        hectors: json["hectors"],
        status: json["status"],
        remainingQuantity: json["remainingQuantity"],
        quantity: json["quantity"],
    );

    Map<String, dynamic> toJson() => {
        "outedDate": outedDate?.toIso8601String(),
        "hectors": hectors,
        "status": status,
        "remainingQuantity": remainingQuantity,
        "quantity": quantity,
    };
}
