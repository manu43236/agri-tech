import 'package:agritech/app/modules/notifications/data/accept_by_landlord_or_farmer_for_job_taken_farmworkermodel.dart';
import 'package:agritech/app/modules/notifications/data/accept_request_by_farmer_or_landlord_model.dart';
import 'package:agritech/app/modules/notifications/data/get_notifications_model.dart';
import 'package:agritech/app/modules/notifications/data/notifications_count_model.dart';
import 'package:agritech/app/modules/notifications/data/reply_to_farmer_by_landlord_model.dart';
import 'package:agritech/app/modules/notifications/data/respond_by_farmworker_to_landlord_or_farmer_model.dart';
import 'package:agritech/app/modules/notifications/data/send_lease_interest_farmer_notification_model.dart';
import 'package:agritech/app/modules/notifications/data/update_flag_model.dart';
import 'package:agritech/models/accept_by_farmworker_model.dart';
import 'package:agritech/models/get_all_notifications_of_Farmworker_model.dart';
import 'package:agritech/models/send_request_by_farmworker_model.dart';
import 'package:dartz/dartz.dart';

abstract class NotificationsRepo{
  Future<Either<GetAllNotificationsByFarmworkerModel,Exception>> getAllFarmworkerNotifications({params1});
  Future<Either<SendRequestByFarmworkerModel,Exception>> sendRequestByFarmworkerModel({params1});
  Future<Either<dynamic,Exception>>  viewedFarmworkerNotifications({params1});
  Future<Either<AcceptByFarmworkerModel,Exception>> acceptFarmworkerNotification({params1});
  Future<Either<UpdateFlagModel,Exception>> updateFlag();
  Future<Either<SendLeaseInterestNotificationByFarmerModel,Exception>> sendLeaseInterest({params1});
  Future<Either<NotificationsCountModel,Exception>> getCount();
  Future<Either<GetNotificationsModel,Exception>> getAllNotifications();
  Future<Either<SendRequestByFarmworkerModel,Exception>> sendRequestByFarmworker({params1});
  Future<Either<RespondByFarmworkerToLandlordOrFarmerModel,Exception>> respondByFarmworker();
  Future<Either<AcceptbylandlordOrFarmerForJobTakenFarmworkerModel,Exception>> acceptLandlordOrFarmer();
  Future<Either<AcceptRequestByFarmerOrLandlordModel,Exception>> acceptRequestSentByWorker({params1});
  Future<Either<ReplyToFarmerByLandlordModel,Exception>> replyToFarmer({params1});
}