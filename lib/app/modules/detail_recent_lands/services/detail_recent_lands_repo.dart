import 'package:agritech/app/modules/detail_recent_lands/data/land_by_id_model.dart';
import 'package:dartz/dartz.dart';

import '../../home/data/recent_lands_model.dart';

abstract class DetailRecentLandsRepo{
  Future<Either<LandByIdModel,Exception>> getLand({params1});
  Future<Either<RecentLandsModel,Exception>>  filterLocations({params1});
}