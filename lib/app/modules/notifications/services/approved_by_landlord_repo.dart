import 'package:dartz/dartz.dart';

abstract class ApprovedByLandlordRepo{
  Future<Either<dynamic,Exception>>  approvedNotifications({params1});
}