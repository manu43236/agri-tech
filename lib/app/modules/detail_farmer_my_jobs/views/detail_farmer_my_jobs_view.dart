import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../home/controllers/landlord_controller.dart';
import '../controllers/detail_farmer_my_jobs_controller.dart';

class DetailFarmerMyJobsView extends GetView<DetailFarmerMyJobsController> {
  const DetailFarmerMyJobsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'detail_my_jobs'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(right: 20, left: 20),
        child: Obx(
          () {
            final jobDetails =
                controller.jobByIdModel.value.result;
            final myJobs = Get.find<LandlordController>()
                .getAllJobsByFarmerModel
                .value
                .result
                ?.ongoingJobs
                ?.where((job) =>
                    job.id.toString() != controller.homeController.jobId.value)
                .toList();

            return controller.apiJobIdStatus.value==ApiStatus.LOADING?Shimmers().getListShimmer(): 
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: Get.height * 0.35,
                    width: Get.width,
                    decoration: BoxDecoration(
                      color: colorAsh,
                      borderRadius: BorderRadius.circular(13),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: Image.network(
                        jobDetails?.image ?? '',
                        height: Get.height * 0.35,
                        width: Get.width,
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) {
                          return Image.asset(
                            defaultImageUrl,
                            height: Get.height * 0.35,
                            width: Get.width,
                            fit: BoxFit.cover,
                          );
                        },
                      ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'available_job_at'.tr,
                            style: TextStyles.kTSFS20W700
                                .copyWith(color: colorDetails),
                          ),
                          Text(
                            '${jobDetails?.village ?? ''}, ${jobDetails?.district ?? ''}',
                            style: TextStyles.kTSDS14W500
                                .copyWith(color: colorDetails),
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      CircleAvatar(
                        radius: 26,
                        backgroundColor: colorAsh,
                        child: Get.find<HomeController>()
                                    .getUserModel
                                    .value
                                    .result
                                    ?.imageUrl !=
                                null
                            ? ClipOval(
                                child: Image.network(
                                  Get.find<HomeController>()
                                      .getUserModel
                                      .value
                                      .result!
                                      .imageUrl!,
                                  fit: BoxFit.cover,
                                  height: Get.height * 0.1,
                                  width: Get.width * 0.2,
                                ),
                              )
                            : const Icon(Icons.person,
                                size: 40, color: Colors.grey),
                      ),
                      const SizedBox(width: 10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.homeController.getUserModel.value
                                        .result?.firstName ?? '',
                            style: TextStyles.kTSDS14W700
                                .copyWith(color: colorPic),
                          ),
                          const SizedBox(height: 5),
                          Text(
                            controller.homeController.getUserModel.value
                                        .result?.address ?? '',
                            style: TextStyles.kTSWFS10W700
                                .copyWith(color: colorDetails),
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),
                  Text(
                    jobDetails?.description ?? '',
                    style: TextStyles.kTSCF12W500,
                  ),
                  const SizedBox(height: 20),
                  controller.jobByIdModel.value.result!
                          .farmworkerDetails!.isNotEmpty
                      ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'hired_farmworkers'.tr,
                              style: TextStyles.kTSFS16W600
                                  .copyWith(color: colorPic),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            SizedBox(
                              height: Get.height * 0.07,
                              width: Get.width ,
                              child: ListView.separated(
                                itemCount: controller
                                    .jobByIdModel
                                    .value
                                    .result!
                                    .farmworkerDetails!
                                    .length,
                                separatorBuilder: (context, index) =>
                                    const SizedBox(
                                  width: 10,
                                ),
                                itemBuilder: (context, index) {
                                  return Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      CircleAvatar(
                                        radius: 26,
                                        backgroundColor: colorAsh,
                                        child: controller
                                                    .jobByIdModel
                                                    .value
                                                    .result
                                                    ?.farmworkerDetails![index]
                                                    .imageUrl !=
                                                null
                                            ? ClipOval(
                                                child: Image.network(
                                                  controller
                                                      .jobByIdModel
                                                      .value
                                                      .result
                                                      ?.farmworkerDetails![
                                                          index]
                                                      .imageUrl!,
                                                  fit: BoxFit.cover,
                                                  height: Get.height * 0.1,
                                                  width: Get.width * 0.2,
                                                ),
                                              )
                                            : const Icon(Icons.person,
                                                size: 40, color: Colors.grey),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            controller
                                                        .jobByIdModel
                                                        .value
                                                        .result!
                                                        .farmworkerDetails !=
                                                    null
                                                ? controller
                                                    .jobByIdModel
                                                    .value
                                                    .result!
                                                    .farmworkerDetails![index]
                                                    .firstName
                                                    .toString()
                                                : '',
                                            style: TextStyles.kTSDS14W700
                                                .copyWith(color: colorPic),
                                          ),
                                          const SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            controller
                                                        .jobByIdModel
                                                        .value
                                                        .result!
                                                        .farmworkerDetails !=
                                                    null
                                                ? controller
                                                    .jobByIdModel
                                                    .value
                                                    .result!
                                                    .farmworkerDetails![index]
                                                    .address
                                                    .toString()
                                                : '',
                                            style: TextStyles.kTSWFS10W700
                                                .copyWith(color: colorDetails),
                                          )
                                        ],
                                      ),
                                    ],
                                  );
                                },
                              ),
                            ),
                          ],
                        )
                      : SizedBox(),
                  const SizedBox(height: 20),
                  Text(
                    'my_jobs'.tr,
                    style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                  ),
                  const SizedBox(height: 10),
                  myJobs != null && myJobs.isNotEmpty
                      ? SizedBox(
                          height: Get.height * 0.08,
                          child: ListView.separated(
                            itemCount: myJobs.length,
                            scrollDirection: Axis.horizontal,
                            separatorBuilder: (context, index) =>
                                const SizedBox(width: 10),
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () async {
                                  // controller.homeController.jobId.value =
                                  //     myJobs[index].id.toString();
                                  await controller.getIdJobs(id:myJobs[index].id.toString() );
                                 // Get.toNamed(Routes.DETAIL_FARMER_MY_JOBS);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: colorGrey,
                                    borderRadius: BorderRadius.circular(11),
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(11),
                                    child: Image.network(
                                      myJobs[index].image ?? '',
                                      height: Get.height * 0.09,
                                      width: Get.width * 0.2,
                                      fit: BoxFit.cover,
                                      errorBuilder:
                                          (context, error, stackTrace) {
                                        return Image.asset(
                                          defaultImageUrl,
                                          height: Get.height * 0.09,
                                          width: Get.width * 0.2,
                                          fit: BoxFit.cover,
                                        );
                                      },
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        )
                      : Center(
                          child: Text(
                            'no_jobs_available'.tr,
                            style: TextStyles.kTSDS14W500
                                .copyWith(color: colorDetails),
                          ),
                        ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
