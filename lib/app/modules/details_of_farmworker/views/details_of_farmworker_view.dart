import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../controllers/details_of_farmworker_controller.dart';
import 'jobs_dropdown.dart';

class DetailsOfFarmworkerView extends GetView<DetailsOfFarmworkerController> {
  const DetailsOfFarmworkerView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'details_of_farmworker'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: Obx(
          () => controller.apiUserByProfileIdStatus.value == ApiStatus.LOADING
              ? Shimmers().getListShimmer()
              : SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ClipOval(
                        child: Image.network(
                          controller.getUserProfileModel.value.result!.imageUrl
                                  .toString() ??'',
                          height: Get.height * 0.4,
                          width: Get.height * 0.4,
                          fit: BoxFit.cover,
                          errorBuilder: (context, error, stackTrace) {
                            return Image.asset(
                              defaultImageUrl,
                              height: Get.height * 0.4,
                              width: Get.height * 0.4,
                              fit: BoxFit.cover,
                            );
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                controller.getUserProfileModel.value.result
                                        ?.firstName ??
                                    'N/A',
                                style: TextStyles.kTSDS14W700
                                    .copyWith(color: colorPic),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                controller.getUserProfileModel.value.result?.age
                                        .toString() ??
                                    '0',
                                style: TextStyles.kTSDS14W700
                                    .copyWith(color: colorPic),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                controller.getUserProfileModel.value.result
                                        ?.address ??
                                    'no_address_provided'.tr,
                                style: TextStyles.kTSDS14W700
                                    .copyWith(color: colorPic),
                              ),
                            ],
                          ),
                          InkWell(
                            onTap: () async {
                              if (controller.isFarmworkerSaved.value == true) {
                                // Perform delete action if the farmer is already saved
                                await controller.homeController.updateSavedUser(
                                    false,
                                    id: controller.deleteId.value,
                                    errorMessage:
                                        "farmworker_unsaved_successfully".tr);
                                await controller.allFarmworkersSaved(
                                    id: controller.id.value);
                                controller.isFarmworkerSaved.value = false;
                              } else {
                                // Perform save action if the farmer is not saved
                                await controller.homeController
                                    .saveFarmworkerAsFavourite();
                                await controller.allFarmworkersSaved(
                                    id: controller.id.value);
                                controller.isFarmworkerSaved.value = true;
                              }
                            },
                            child: Obx(
                              () => Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(11),
                                  color: controller.isFarmworkerSaved.value
                                      ? colorBlue.withOpacity(0.5)
                                      : colorSave,
                                ),
                                child: Icon(
                                  controller.isFarmworkerSaved.value
                                      ? Icons.bookmark
                                      : Icons.bookmark_border_outlined,
                                  color: colorBlack.withOpacity(0.5),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      Text(
                        'skills'.tr,
                        style: TextStyles.kTSDS12W700
                            .copyWith(color: colorDetails),
                      ),
                      const SizedBox(height: 5),
                      Obx(() {
                        //final skills = controller.skills;
                        final skills = controller.skills.value;
                        final displayedSkills = controller.showAllSkills.value
                            ? skills
                            : skills.take(3).toList();

                        if (skills.isNotEmpty) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              SizedBox(
                                height:
                                    (displayedSkills.length / 3).ceil() * 40.0,
                                child: GridView.builder(
                                  gridDelegate:
                                      const SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 3,
                                    crossAxisSpacing: 10.0,
                                    mainAxisSpacing: 10.0,
                                    mainAxisExtent:
                                        33, // Adjust height of each grid item
                                  ),
                                  itemCount: displayedSkills.length,
                                  itemBuilder: (context, index) {
                                    return Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 3, vertical: 1),
                                      width: Get.width,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        color: colorGrey.shade300,
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                      ),
                                      child: Text(
                                        displayedSkills[index].skill ?? '',
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        style: const TextStyle(
                                          color: primary,
                                          fontSize: 14,
                                        ),
                                      ),
                                    );
                                  },
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                ),
                              ),
                              const SizedBox(height: 3),
                              controller.skills.value.length <= 3
                                  ? SizedBox()
                                  :
                                  //controller.skills.length<=3?SizedBox():
                                  GestureDetector(
                                      onTap: controller.toggleShowAllSkills,
                                      child: Text(
                                        controller.showAllSkills.value
                                            ? 'view_less'.tr
                                            : 'view_more'.tr,
                                        style: TextStyles.kTSFS10W500.copyWith(
                                            color: primary,
                                            decoration:
                                                TextDecoration.underline),
                                      ),
                                    ),
                            ],
                          );
                        } else {
                          return Center(
                            child: Text(
                              'no_skills_found'.tr,
                              style: TextStyles.kTSCF12W500
                                  .copyWith(color: colorGrey, fontSize: 14),
                            ),
                          );
                        }
                      }),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: JobsDropdown()),
                          const SizedBox(width: 50),
                          controller.statusForLandlord.value == ""
                              ? InkWell(
                                  onTap: () async {
                                    if (controller
                                        .selectedJobId.value.isEmpty) {
                                      Get.snackbar('msg'.tr,
                                          'please_select_job_first'.tr,
                                          backgroundColor: colorGreen,
                                          colorText: colorBlack);
                                    } else {
                                      await controller
                                          .sentNotificationByLandlordToWorker(
                                              senderId: controller.id.value,
                                              workerId:
                                                  controller.workerId.value,
                                              senderRole: controller.role.value,
                                              jobId: controller
                                                  .selectedJobId.value);
                                      await controller.fetchedJobDetails(
                                          senderId: controller.id.value,
                                          workerId: controller.workerId.value,
                                          senderRole: controller.role.value);
                                    }
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(7),
                                      color: colorPic,
                                    ),
                                    child: Text(
                                      'hire'.tr,
                                      style: TextStyles.kTSNFS16W600
                                          .copyWith(color: colorWhite),
                                    ),
                                  ),
                                )
                              : Text(
                                  controller.statusForLandlord.value,
                                  style: TextStyles.kTSFS18W600.copyWith(
                                      color:
                                          controller.statusForLandlord.value ==
                                                  'pending'
                                              ? const Color.fromARGB(
                                                  255, 227, 154, 44)
                                              : primary),
                                )
                        ],
                      ),
                      const SizedBox(height: 20),
                      Text(
                        'description_about_him'.tr,
                        style: TextStyles.kTSDS12W700
                            .copyWith(color: colorDetails),
                      ),
                      const SizedBox(height: 10),
                      Text(
                        controller.getUserProfileModel.value.result
                                ?.description ??
                            '',
                        style: TextStyles.kTSCF12W500,
                      ),
                      const SizedBox(height: 20),
                      Text(
                        'suggested_farmworkers'.tr,
                        style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                      ),
                      const SizedBox(height: 10),
                      SizedBox(
                        height: Get.height * 0.08,
                        child: Obx(() {
                          final farmers = controller
                              .nearByFarmworkersModel.value.result?.farmworkers;
                          if (farmers == null || farmers.isEmpty) {
                            return Text('no_farmworkers_available'.tr);
                          }

                          final selectedFarmerId =
                              controller.farmWorkerId.value;

                          final filteredFarmers = farmers
                              .where((farmer) =>
                                  farmer.id != int.tryParse(selectedFarmerId))
                              .toList();

                          if (filteredFarmers.isEmpty) {
                            return Center(
                              child: Text(
                                'no_near_by_farmworkers_available'.tr,
                                style: TextStyles.kTSDS14W500
                                    .copyWith(color: colorGrey),
                              ),
                            );
                          }

                          return ListView.separated(
                            itemCount: filteredFarmers.length,
                            scrollDirection: Axis.horizontal,
                            separatorBuilder: (context, index) =>
                                const SizedBox(width: 10),
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () async {
                                  controller.selectedJob.value = null;
                                  controller.statusForLandlord.value = "";
                                  controller.workerId.value =
                                      filteredFarmers[index].id!;
                                  controller.farmWorkerId.value =
                                      controller.workerId.value.toString();

                                  await controller.getUserByProfileId(
                                    id: controller.workerId.value,
                                  );
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: colorGrey,
                                    borderRadius: BorderRadius.circular(11),
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(11),
                                    child: Image.network(
                                      filteredFarmers[index]
                                          .imageUrl
                                          .toString(),
                                      height: Get.height * 0.09,
                                      width: Get.width * 0.2,
                                      fit: BoxFit.cover,
                                      errorBuilder:
                                          (context, error, stackTrace) {
                                        return Image.asset(
                                          defaultImageUrl,
                                          height: Get.height * 0.09,
                                          width: Get.width * 0.2,
                                          fit: BoxFit.cover,
                                        );
                                      },
                                    ),
                                  ),
                                ),
                              );
                            },
                          );
                        }),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
