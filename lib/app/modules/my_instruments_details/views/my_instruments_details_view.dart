import 'package:agritech/app/modules/my_instruments_details/controllers/my_instruments_details_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/widget_utils/shimmers.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';

class MyInstrumentsDetailsView extends GetView<MyInstrumentsDetailsController> {
  const MyInstrumentsDetailsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'my_instrument_details'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          right: 20,
          left: 20,
        ),
        child: SingleChildScrollView(
          child: Obx(
            () => controller.apigetUtilityStatus.value == ApiStatus.LOADING
                ? Shimmers().getListShimmer()
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: colorAsh,
                            borderRadius: BorderRadius.circular(15)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: Image.network(
                            controller.getUtilityByIdModel.value.result?.utility
                                    ?.image ??
                                defaultImageUrl,
                            height: Get.height * 0.3,
                            width: Get.width,
                            fit: BoxFit.cover,
                            errorBuilder: (context, error, stackTrace) {
                              // Display a local asset if the network image fails
                              return Image.asset(
                                defaultImageUrl,
                                height: Get.height * 0.3,
                                width: Get.width,
                                fit: BoxFit.cover,
                              );
                            },
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '${controller.getUtilityByIdModel.value.result?.utility?.nameOfInstrument ?? ''}',
                                style: TextStyles.kTSFS20W700
                                    .copyWith(color: colorDetails),
                              ),
                              Text(
                                '${controller.getUtilityByIdModel.value.result?.utility?.village ?? ''}, ${controller.getUtilityByIdModel.value.result?.utility?.district ?? ''}',
                                style: TextStyles.kTSDS14W500
                                    .copyWith(color: colorDetails),
                              ),
                            ],
                          ),
                          controller.isLoan.value == true
                              ? controller.getUtilityByIdModel.value.result
                                          ?.controllingData?.statusForLoan !=
                                      "Request sent to Loan Agent"
                                  ? InkWell(
                                      onTap: () async {
                                        await controller
                                            .sendUtilityLoanNotification(
                                                utilId: controller
                                                    .getUtilityByIdModel
                                                    .value
                                                    .result
                                                    ?.utility
                                                    ?.id);
                                        controller.getUtilityById(
                                            id: controller.getUtilityByIdModel
                                                .value.result?.utility?.id);
                                      },
                                      child: Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        decoration: BoxDecoration(
                                          color: colorPic,
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        child: Text(
                                          'avail_loan'.tr,
                                          style: TextStyles.kTSFS12W700
                                              .copyWith(color: colorWhite),
                                        ),
                                      ),
                                    )
                                  : Row(
                                      children: [
                                        Image.asset(
                                          loanSent,
                                          height: Get.height * 0.04,
                                          width: Get.width * 0.09,
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          'applied'.tr,
                                          style: TextStyles.kTSDS12W500
                                              .copyWith(color: primary),
                                        )
                                      ],
                                    )
                              : controller.isInsurance.value == true
                                  ? controller
                                              .getUtilityByIdModel
                                              .value
                                              .result
                                              ?.controllingData
                                              ?.statusForInsuranceAgent !=
                                          "Request sent to Insurance Agent"
                                      ? InkWell(
                                          onTap: () async {
                                            await controller
                                                .sendUtilityInsuranceNotification(
                                                    utilId: controller
                                                        .getUtilityByIdModel
                                                        .value
                                                        .result
                                                        ?.utility
                                                        ?.id);
                                            controller.getUtilityById(
                                                id: controller
                                                    .getUtilityByIdModel
                                                    .value
                                                    .result
                                                    ?.utility
                                                    ?.id);
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 5, horizontal: 10),
                                            decoration: BoxDecoration(
                                              color: colorPic,
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            child: Text(
                                              'avail_insurance'.tr,
                                              style: TextStyles.kTSFS12W700
                                                  .copyWith(color: colorWhite),
                                            ),
                                          ),
                                        )
                                      : Row(
                                          children: [
                                            Image.asset(
                                              loanSent,
                                              height: Get.height * 0.04,
                                              width: Get.width * 0.09,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              'applied'.tr,
                                              style: TextStyles.kTSDS12W500
                                                  .copyWith(color: primary),
                                            )
                                          ],
                                        )
                                  : SizedBox(),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CircleAvatar(
                              radius: 26,
                              backgroundColor: colorAsh,
                              child: ClipOval(
                                child: Image.network(
                                  controller.getUtilityByIdModel.value.result
                                          ?.utiliser?.imageUrl ??
                                      defaultImageUrl,
                                  height: Get.height * 0.1,
                                  width: Get.width * 0.2,
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, error, stackTrace) {
                                    return Image.asset(
                                      defaultImageUrl,
                                      height: Get.height * 0.1,
                                      width: Get.width * 0.2,
                                      fit: BoxFit.cover,
                                    );
                                  },
                                ),
                              )),
                          const SizedBox(
                            width: 20,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                controller.getUtilityByIdModel.value.result
                                        ?.utiliser?.firstName ??
                                    '',
                                style: TextStyles.kTSDS14W700
                                    .copyWith(color: colorPic),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                controller.getUtilityByIdModel.value.result
                                        ?.utiliser?.address ??
                                    '',
                                style: TextStyles.kTSWFS10W700
                                    .copyWith(color: colorDetails),
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        controller.getUtilityByIdModel.value.result?.utility
                                ?.description ??
                            "",
                        style: TextStyles.kTSCF12W500,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      controller.getUtilityByIdModel.value.result?.leasetaker !=
                              null
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'lessee'.tr,
                                  style: TextStyles.kTSFS16W600
                                      .copyWith(color: primary),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        CircleAvatar(
                                            radius: 26,
                                            backgroundColor: colorAsh,
                                            child: ClipOval(
                                              child: Image.network(
                                                controller
                                                    .getUtilityByIdModel
                                                    .value
                                                    .result!
                                                    .leasetaker!
                                                    .imageUrl
                                                    .toString(),
                                                height: Get.height * 0.1,
                                                width: Get.width * 0.2,
                                                fit: BoxFit.cover,
                                                errorBuilder: (context, error,
                                                    stackTrace) {
                                                  return Image.asset(
                                                    defaultImageUrl,
                                                    height: Get.height * 0.1,
                                                    width: Get.width * 0.2,
                                                    fit: BoxFit.cover,
                                                  );
                                                },
                                              ),
                                            )),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              controller
                                                      .getUtilityByIdModel
                                                      .value
                                                      .result
                                                      ?.leasetaker
                                                      ?.firstName ??
                                                  '',
                                              style: TextStyles.kTSDS14W700
                                                  .copyWith(color: colorPic),
                                            ),
                                            const SizedBox(
                                              height: 5,
                                            ),
                                            Text(
                                              controller
                                                      .getUtilityByIdModel
                                                      .value
                                                      .result
                                                      ?.leasetaker
                                                      ?.address ??
                                                  '',
                                              style: TextStyles.kTSWFS10W700
                                                  .copyWith(
                                                      color: colorDetails),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            )
                          : SizedBox(),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'my_utilities'.tr,
                        style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: Get.height * 0.08,
                        width: Get.width,
                        child: (controller.utilityController
                                    .getUtilityByUserIdModel.value.result
                                    ?.where((utility) =>
                                        utility.id.toString() !=
                                        controller.getUtilityByIdModel.value
                                            .result?.utility?.id
                                            .toString())
                                    .isEmpty ??
                                true)
                            ? Center(
                                child: Text(
                                  'no_lands_are_available_near_your_location'
                                      .tr,
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.grey,
                                  ),
                                ),
                              )
                            : ListView.separated(
                                itemCount: controller.utilityController
                                        .getUtilityByUserIdModel.value.result
                                        ?.where((utility) =>
                                            utility.id.toString() !=
                                            controller.getUtilityByIdModel.value
                                                .result?.utility?.id
                                                .toString())
                                        .length ??
                                    0,
                                scrollDirection: Axis.horizontal,
                                separatorBuilder: (context, index) =>
                                    const SizedBox(width: 10),
                                itemBuilder: (context, index) {
                                  final filteredLands = controller
                                      .utilityController
                                      .getUtilityByUserIdModel
                                      .value
                                      .result
                                      ?.where((utility) =>
                                          utility.id.toString() !=
                                          controller.getUtilityByIdModel.value
                                              .result?.utility?.id
                                              .toString())
                                      .toList();
                                  final utility = filteredLands;
                                  return InkWell(
                                    onTap: () async {
                                      controller.getUtilityById(
                                          id: utility[index].id);
                                      //Get.toNamed(Routes.MY_INSTRUMENTS_DETAILS);
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: colorGrey,
                                        borderRadius: BorderRadius.circular(11),
                                      ),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(11),
                                        child: Image.network(
                                          utility![index].image,
                                          height: Get.height * 0.09,
                                          width: Get.width * 0.2,
                                          fit: BoxFit.cover,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            return Image.asset(
                                              defaultImageUrl,
                                              height: Get.height * 0.09,
                                              width: Get.width * 0.2,
                                              fit: BoxFit.cover,
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                      )
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
