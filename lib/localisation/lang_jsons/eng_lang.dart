const engLang = {
  //=========================landlord_module=======================================
  //welcome_page_view
  'already_have_an_account?': 'Already have an account? ',
  'create_an_account': 'Create an account',
  'here': ' here',
  'login': 'Login',
  //Sign_up_view
  'sign_up': 'Sign up',
  'phone_no': 'Phone No',
  '+91 000-00-000': '+91 000-00-000',
  'required_mob_num': '* required mobile number',
  'enter_valid_mob_num': '* enter valid mobile number',
  'get_otp': 'Get Otp',
  //Otp_view
  'otp': 'OTP',
  'otp_verification': 'OTP Verification',
  'enter_the_verification_code_we_just_sent_on_your_mobile':
      'Enter the verification code we just sent on your mobile',
  'authenticate': 'Authenticate',
  'resend_otp': 'Resend Otp',
  //Profile_view
  'gallery': 'Gallery',
  'camera': 'Camera',
  'enter_details': 'Enter Details',
  'name': 'Name',
  'enter_your_name': 'Enter your Name',
  'required_name': '* required name',
  'age': 'Age',
  'required_age': '* required age',
  'mobile_number': 'Mobile Number',
  'enter_your_mobile_number': 'Enter your mobile number',
  'address': 'Address',
  'enter_location': 'Enter Location',
  'required_address': '* required address',
  'select_role': 'Select Role',
  'description': 'Description',
  'enter_your_details': 'Enter your details',
  'required_description': '* required description',
  'save': 'Save',
  'search_role':'Search Role',
  'no_roles_available':'No Roles Available',
  'please_add_roles_first':'Please add roles first',
  //Dashboard_view
  'add': 'Add',
  'add_instrument': 'Add Instrument',
  'add_skill': 'Add Skill',
  'add_land': 'Add Land',
  'add_job': 'Add Job',
  //home_view
  'landlord': 'Landlord',
  'farmer': 'Farmer',
  'farm_worker': 'Farm Worker',
  'utilities': 'Utilities',
  'loans_insurance': 'Loans&Insurance',
  'hello': 'Hello',
  'you_are_now_a': 'You are now a ',
  'you': 'You',
  'others': 'Others',
  'vendor': 'Vendor',
  'something_went_wrong': 'Something Went Wrong',
  //my_lands_widget
  'available_lands': 'Available Lands',
  'at': 'at',
  'view_all': 'View All',
  'no_lands': 'No Lands',
  'add_your_land_here': 'Add your Land here',
  'my_lands': 'My Lands',
  'no_lands_add_it_here': 'No Lands add it here',
  //my_jobs_widget
  'my_jobs': 'My Jobs',
  'no_jobs_add_it_here': 'No Jobs add it here',
  //leased_utilities_widget
  'leased_utilities': 'Leased Utilities',
  'no_leased_utilities': 'No Leased Utilities',
  //leased_jobs_widget
  'leased_jobs': 'Leased Jobs',
  'no_leased_jobs': 'No Leased Jobs',
  //leased_lands_widget
  'leased_lands': 'Leased Lands',
  'no_leased_lands': 'No Leased Lands',
  //available_recent_lands_view
  'available_recent_lands': 'Available Recent Lands',
  'extent': 'Extent- ',
  'acres': 'Acres',
  'water_facility': 'Water Facility- ',
  //detail_recent_lands_view
  'detail_recent_lands': 'Detail Recent Lands',
  'land_unsaved_successfully': 'Land unsaved successfully',
  'not_available': 'Not Available',
  'available': 'Available',
  'hired_farmers': 'Hired Farmers',
  'suggested_lands': 'Suggested Lands',
  'no_lands_are_available_near_your_location':
      'No lands are available near your location',
  //available_my_lands_view
  'available_my_lands': 'Available My Lands',
  'no_lands_found': 'No Lands Found',
  //detail_my_lands_view
  'detail_my_lands': 'Detail My Lands',
  'avail_loan': 'Avail Loan',
  'applied': 'Applied',
  'avail_insurance': 'Avail Insurance',
  'suitable_crop': 'Suitable Crop',
  'no_description_available': 'No description available.',
  'no_lands_available': 'No lands available.',
  //suitable_crop_view
  'selected_crop_for': 'Selected crop for',
  'days': 'days',
  'yield': 'Yield',
  'tons': ' /tons',
  'enable_editing_to_modify_the_field': 'Enable editing to modify the field.',
  'submit': 'Submit',
  'no_dataa':
      'No data available for the selected crop. Please try a different crop.',
  //grid_view_widget
  'week': 'Week',
  'done': 'Done',
  //crops_dropdown_view
  'no_crops_available_please_add_crops_first':
      'No crops available. Please add crops first',
  'select_crop': 'Select Crop',
  'select_other_crop': 'Select Other Crop',
  'working_days_info': 'Working Days Info',
  'no_of_working_days_for': 'No. of working days for',
  'no_working_days_data_available': 'No working days data available.',
  //available_my_jobs_view
  'available_my_jobs': 'Available My Jobs',
  'no_jobs_found': 'No Jobs Found',
  'location': 'Location- ',
  //detail_my_jobs_view
  'detail_my_jobs': 'Detail My Jobs',
  'available_job_at': 'Available job at',
  'hired_farmworkers': 'Hired Farmworkers',
  'no_jobs_available': 'No jobs available',
  //available_leased_utilities
  'available_leased_utilities': 'Available Leased Utilities',
  'no_utilities_found': 'No Utilities Found',
  'utility_at': 'Utility at',
  //detail_leased_utilities_view
  'utility_details': 'Utility Details',
  'utility_owner': 'Utility Owner',
  'balance_payment': 'Balance Payment',
  //landlord_farmer_view
  'available_farmers': 'Available Farmers',
  'no_farmers_found': 'No Farmers found',
  //farmer_landlord_view
  'available_landlords': 'Available Landlords',
  'no_landlords_found': 'No Landlords found',
  //landlord_worker
  'available_farmWorkers': 'Available FarmWorkers',
  'no_farm_workers_found': 'No Farm Workers found',
  //landlord_utilities_view
  'instruments': 'Instruments',
  'no_instruments_available': 'No instruments available',
  'status': 'Status : ',
  'leased': 'Leased',
  //loans_view
  'loans': 'Loans',
  'insurance': 'Insurance',
  //add_a_land_view
  'add_a_land': 'Add a Land',
  'no_job': 'No Job',
  'add_job_here': 'Add Job here',
  //add_land_view
  'land_name': 'Land Name',
  'land_in_acres': 'Land in Acres',
  'required_acres': '* required acres',
  'land_survey_number': 'Land Survey Number',
  'required_survey_number': '* required survey number',
  'select_state': 'Select State',
  'state': 'State',
  'required_state': '* required state',
  'select_district': 'Select District',
  'district': 'District',
  'required_district': '* required district',
  'select_mandal': 'Select Mandal',
  'mandal': 'Mandal',
  'required_mandal': '* required mandal',
  'select_village': 'Select Village',
  'village': 'Village',
  'required_village': '* required village',
  'water_availability': 'Water Availability',
  'yes': 'Yes',
  'no': 'No',
  'image': 'Image',
  'upload_image_here': 'Upload Image Here',
  'error': 'Error',
  'required_to_add_land_image': '* Required to add land image',
  //add_job_view
  'add_a_job': 'Add a Job',
  'job_name': 'Job Name',
  'start_date': 'Start Date',
  'end_date': 'End Date',
  'required_to_add_job_image': '* Required to add job image',
  //add_a_skill
  'add_a_skill': 'Add a Skill',
  'skill_name': 'Skill Name',
  'experience': 'Experience',
  'please_enter_your_experience': 'Please enter your experience',
  //add_instrument
  'name_of_the_instrument': 'Name of the Instrument',
  'required_name_of_the_instrument': '* required name of the instrument',
  'price_per_day': 'Price per day',
  'required_price': '* required price',
  'brand_name': 'Brand Name',
  'required_brand_name': '* required brand name',
  'required_to_add_image': 'Required to add Image',
  //details_of_farmer_view
  'details_of_farmer': 'Details of Farmer',
  'no_address_provided': 'No address provided',
  'farmer_unsaved_successfully': 'Farmer unsaved successfully',
  'msg': 'message',
  'please_select_land_first': 'please select land first',
  'hire': 'Hire',
  'description_about_him': 'Description About him',
  'suggested_farmers': 'Suggested farmers',
  'no_nearby_farmers_found': 'No nearby farmers found',
  'select_land': 'Select Land',
  //details_of_farmworker_view
  'details_of_farmworker': 'Details of Farmworker',
  'farmworker_unsaved_successfully': 'Farmworker unsaved successfully',
  'skills': 'Skills',
  'no_skills_found': 'No skills found',
  'please_select_job_first': 'please select job first',
  'suggested_farmworkers': 'Suggested Farmworkers',
  'no_farmworkers_available': 'No farmworkers available',
  'no_near_by_farmworkers_available': 'No near by  farmworkers available',
  'select_job': 'Select Job',
  'view_less': 'View Less',
  'view_more': 'View More',
  //details_of_landlord_utilities_view
  'utility_unsaved_successfully': 'Utility unsaved successfully',
  'day': '/ day',
  'more': 'More',
  'no_utilities_found_near_your_location':
      'No utilities found near your location',
  'call': 'Call',
  'rent_now': 'Rent Now',
  //landlord_utilities_checkout_page_view
  'total_cost_of_selected_item': 'Total cost of selected item',
  'rs': 'Rs',
  'advance_payment': 'Advance Payment',
  'to_made_later_after_receiving_the_prod':
      'To made later, after receiving the prod',
  'amount_to_be_paid': 'Amount to be paid',
  'gst': 'GST',
  'total': 'Total',
  'make_a_payment': 'Make a payment',
  //notifications_view
  'notifications': 'Notifications',
  'approve': 'Approve',
  'decline': 'Decline',
  'interested': 'Interested',
  //profile_page_view
  'profile': 'Profile',
  'my_details': 'My Details',
  'saved': 'Saved',
  'settings': 'Settings',
  'customer_support': 'Customer Support',
  'my_instruments': 'My Instruments',
  'purchased': 'Purchased',
  'update_kyc': 'Update KYC',
  'e_kyc_verified': 'e-KYC Verified',
  'farm_workers': 'Farm Workers',
  'jobs_posted': 'Jobs Posted',
  'logout': 'Logout',
  //edit_profile_view
  'edit': 'Edit',
  'required_mobile_number': '* required mobile number',
  'enter_valid_mobile_number': 'Enter valid mobile number',
  'enter_role': 'Enter role',
  'required_role': '* required role',
  //select_region_page
  'clear_all': 'Clear All',
  'select_your_region': 'Select your Region',
  'close': 'Close',
  'sta': 'state',
  'dis': 'district',
  'man': 'mandal',
  'vill': 'village',
  'recent_Lands': 'recentLands',
  'my_Lands': 'myLands',
  'my_Jobs':'myJobs',
  'farmer_My_Lands': 'farmerMyLands',
  'farmer_My_Jobs':'farmerMyJobs',
  'worker_jobs':'workerJobs',
  'my_Utilities':'myUtilities',
  'recent_utilities':'recentUtilities',
  //saved_view
  'farmers': 'Farmers',
  'farmworkers': 'Farmworkers',
  'my_utilities': 'My Utilities',
  'landlords': 'Landlords',
  //saved_farmers_view
  'saved_farmers': 'Saved Farmers',
  'no_farmers_saved': 'No Farmers saved',
  //saved_farmworkers_view
  'saved_farmworkers': 'Saved Farmworkers',
  'no_farm_workers_saved': 'No Farm Workers saved',
  //saved_lands_view
  'saved_lands': 'Saved Lands',
  'no_lands_saved': 'No Lands saved',
  //saved_utilities_view
  'saved_utilities': 'Saved Utilities',
  'ava': 'Available: ',
  'rented_out': 'Rented Out',
  'no_utilities_saved': 'No Utilities saved',
  //saved_landlords_view
  'saved_landlords': 'Saved Landlords',
  'no_landlords_saved': 'No Landlords saved',
  //saved_jobs_view
  'saved_jobs': 'Saved Jobs',
  'job_at': 'Job at',
  'number_of_days_to_work': 'Number of days to work : 5',
  'no_jobs_saved': 'No Jobs saved',
  //hired_farmers_details_view
  'details': 'Details',
  'nam': 'Name : ',
  'loc': 'Location : ',
  //jobs_posted_by_landlord_view
  'jobs': 'Jobs',
  'posted_on': 'Posted on :',
  //=================farmer_module=====================
  //detail_leased_lands_view
  'detail_leased_lands': 'Detail Leased Lands',
  //details_of_landlord
  'details_of_landlord': 'Details of Landlord',
  'landlord_unsaved_successfully': 'Landlord unsaved successfully',
  //details_of_landlord_land_view
  'details_of_land': 'Details of Land',
  'show_interest': 'Show Interest',
  //detail_saved_lands_view
  'no_saved_lands_available': 'No saved lands available',
  //============================farmworker_module====================
  //farmworker_home_view_page
  'jobs_available': 'JOBS AVAILABLE',

  //available_farmworker_jobs
  'available_farmworker_jobs': 'Available Farmworker Jobs',

  //detail_farmworker_job
  'detail_jobs': 'Detail Jobs',
  'job_unsaved_successfully': 'Job unsaved successfully',
  'interest': 'Interest',
  'suggested_jobs': 'Suggested Jobs',
  'your_selected_region_is': 'Your selected region is ',
  //my_skills_widget
  'my_skills': 'My Skills',

  //detail_my_skills
  'detail_my_skill': 'Detail My Skill',
  'years': 'years',
  'experiences': 'Experience:',
  'relevent_jobs': 'Relevant Jobs',
  'no_relevant_jobs_available.': 'No relevant jobs available.',
  'no_jobs_found_matching_the_criteria': 'No jobs found matching the criteria',
  //detail_relevant_jobs
  'details_of_relevent_jobs': 'Details of Relevent jobs',
  //detail_saved_jobs_view
  'detail':'Detail',
  'Job_at':'JOB AT',
  //=========================utiliser_module======================
  //recent_utilities_widget
  'available_utilities':'Available Utilities',
  'no_utilities':'No Utilities',
  //detail_recent_utilities
  'detail_recent_utility':'Detail Recent Utility',
  'lessee':'Lessee',
  'suggested_utilities':'Suggested Utilities',
  //my_instrument_details_view
  'my_instrument_details':'My Instrument Details',
  //available_recent_utilities_view
  'available_recent_utilities':'Available Recent Utilities',
  'no_utilities_found_matching_the_criteria':'No utilities found matching the criteria',
  //available_my_utilities_view
  'available_my_utilities':'Available My Utilities',
  'no_instruments_added':'No instruments added',
  //success_payment_checkout_page_view
  'payment_checkout':'Payment Checkout',
  'payment_is_successful':'payment is successful',
  //=======================vendor_module=================
  //recent_crops_widgets
  'available_crops':'Available Crops',
  'crop_at':'crop at',
  'no_crops_found':'No Crops Found',
  //purchased_crops_widget
  'purchased_crops':'Purchased Crops',
  'no_pcurchased_crops':'No Purchased Crops',
  //available_crops_view
  'quantity':'Quantity:',
  'Tons':'Tons',
  'outdate':'OutDate - ',
  'select_quantity':'Select Quantity',
  //detail_crops_view
  'detail_crop':'Detail Crop',
  'quantity_available':'Quantity Available - ',
  'owner':'Owner - ',
  'total_days':'Total Days - ',
  'avail_this':'Avail this',
  'purchased_quantity':'Purchased Quantity - ',
  'outed_date':'Outed Date - ',
  'payment_completed':'Payment Completed',
  //vendor_payment_successful_view
  'payment_successful':'Payment Successful!',
  //vendor_checkout_details_view
  'checkout':'Checkout',
  //purchased_checkout_details_view
  'payment_made':'Payment made',
  //kyc_welcome_page_view
  'kyc_details':'KYC Details',
  'verify_your_identity':'Verify Your Identity',
  'kyc_text':'Hey there! We just need to confirm it is really you. It is a quick and easy process, and once you are verified, you will have full access to all features.',
  'proceed':'Proceed',
  'skip':'Skip',
  //kyc_details_page_view
  'proof_of_identity':'Proof Of Identity',
  'in_order':'In order to complete your registration, please upload a copy of your identity.',
  'choose_your_identity_type':'Choose your identity type:',
  'aadhaar':'Aadhaar',
  'pan_card':'Pan Card',
  'voter_id':'Voter Id',
  'upload_identity':'Upload Identity',
  'front_image':'frontImage',
  'upload_required':'Upload Required',
  'please_upload_the_front_image_first':'Please upload the Front Image first.',
  'back_image':'backImage',
  'upload_selfie_picture':'Upload Selfie Picture',
  'selfie_image':'Selfie Image',
  'incomplete':'Incomplete',
  'please_upload_the_required_document':'Please upload the required document.',
  'continue':'Continue',
  'price':'Price-',
};
