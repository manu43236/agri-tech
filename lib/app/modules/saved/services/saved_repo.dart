import 'package:dartz/dartz.dart';

import '../data/get_all_saved_Jobs_model.dart';
import '../data/get_all_saved_farmer_model.dart';
import '../data/get_all_saved_farmworkers_model.dart';
import '../data/get_all_saved_landlord_model.dart';
import '../data/get_all_saved_lands_model.dart';

abstract class SavedRepo{
  Future<Either<AllSavedLandsModel,Exception>> allSavedLands();
  Future<Either<GetAllSavedFarmerModel,Exception>> savedFarmersList();
  Future<Either<GetAllSavedLandlordModel,Exception>> savedLandlord();
  Future<Either<GetAllSavedFarmworkerModel,Exception>> allFarmworkers();
  Future<Either<GetAllSavedJobsModel,Exception>> getAllJobs();
}