import 'package:agritech/app/modules/profile/controllers/profile_controller.dart';
import 'package:agritech/app/modules/signup/controllers/signup_controller.dart';
import 'package:get/get.dart';

import '../controllers/welcome_page_controller.dart';

class WelcomePageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WelcomePageController>(
      () => WelcomePageController(),
    );
    
  }
}
