import 'package:agritech/app/modules/detail_my_skills/data/get_skills_by_id_model.dart';
import 'package:agritech/app/modules/detail_my_skills/data/relevant_skill_jobs_model.dart';
import 'package:dartz/dartz.dart';

abstract class DetailMySkillsRepo{
  Future<Either<GetSkillsByIdModel,Exception>> getSkillById();
  Future<Either<RelavantSkillJobsModel,Exception>> getSkillJobs();
}