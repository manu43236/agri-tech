import 'package:get/get.dart';

import '../../details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import '../../instrument_details/controllers/instrument_details_controller.dart';
import '../../saved_utilities/controllers/saved_utilities_controller.dart';
import '../controllers/detail_recent_utilities_controller.dart';

class DetailRecentUtilitiesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailRecentUtilitiesController>(
      () => DetailRecentUtilitiesController(),
    );
    Get.lazyPut<InstrumentDetailsController>(
      () => InstrumentDetailsController(),
    );
     Get.lazyPut<DetailsOfLandlordUtilitiesController>(
      () => DetailsOfLandlordUtilitiesController(),
    ); 
     Get.lazyPut<SavedUtilitiesController>(
      () => SavedUtilitiesController(),
    );
  }
}
