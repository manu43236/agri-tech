import 'package:get/get.dart';

import '../controllers/kyc_details_page_controller.dart';

class KycDetailsPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<KycDetailsPageController>(
      () => KycDetailsPageController(),
    );
  }
}
