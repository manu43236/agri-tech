import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/my_instruments_controller.dart';

class MyInstrumentsView extends GetView<MyInstrumentsController> {
  const MyInstrumentsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'my_instruments'.tr,
          style: TextStyles.kTSFS24W600.copyWith(color: colorDetails),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: colorBlack),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Obx(
        () => controller.getUtilityUserIdModel.value.result != null &&
                controller.getUtilityUserIdModel.value.result!.isNotEmpty
            ? Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: ListView(
                  children: [
                    GridView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 10,
                        childAspectRatio: 0.9,
                        mainAxisSpacing: 10,
                      ),
                      itemCount: controller.getUtilityUserIdModel.value.result?.length ?? 0,
                      itemBuilder: (context, index) {
                        final instrument =
                            controller.getUtilityUserIdModel.value.result![index];

                        return InkWell(
                          onTap: () {
                            Get.toNamed(
                              Routes.MY_INSTRUMENTS_DETAILS,
                              arguments: {
                                "instrumentId": instrument.id!,
                                "isLoan": false,
                                "isInsurance": false,
                              },
                            );
                          },
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              ClipRRect(
                                borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(11),
                                  topRight: Radius.circular(11),
                                ),
                                child: Image.network(
                                  instrument.image ?? defaultImageUrl,
                                  height: Get.height * 0.12,
                                  width: Get.width * 0.5,
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, error, stackTrace) {
                                    return Image.asset(
                                      defaultImageUrl,
                                      height: Get.height * 0.12,
                                      width: Get.width * 0.5,
                                      fit: BoxFit.cover,
                                    );
                                  },
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.only(
                                      bottomLeft: Radius.circular(11),
                                      bottomRight: Radius.circular(11),
                                    ),
                                    color: colorGrey.shade100,
                                  ),
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        instrument.nameOfInstrument ?? 'Unknown Instrument',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyles.kTSCF12W500.copyWith(color: colorHello),
                                      ),
                                      Text(
                                        instrument.village ?? 'Unknown Location',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyles.kTSCF12W500.copyWith(color: colorHello),
                                      ),
                                      Text(
                                        '${'available'.tr} ${instrument.status ?? 'N/A'}',
                                        style: TextStyles.kTSCF12W500.copyWith(color: colorHello),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ],
                ),
              )
            : Center(
                child: Text(
                  'no_instruments_added'.tr,
                  style: TextStyles.kTSDS12W500,
                ),
              ),
      ),
    );
  }
}