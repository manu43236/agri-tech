import 'package:agritech/app/modules/utility/controllers/utility_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../home/controllers/home_controller.dart';
import '../../utility/data/get_utility_by_id_model.dart';
import '../../utility/services/utility_repo_impl.dart';
import '../data/send_notification_for_loan_utility_model.dart';
import '../services/my_instrument_details_repo_impl.dart';



class MyInstrumentsDetailsController extends BaseController {

  Rx<SendNotificationForLoanUtilityModel>
      sendNotificationForLoanUtilityModel =
      Rx(SendNotificationForLoanUtilityModel());
  Rx<GetUtilityByIdModel> getUtilityByIdModel = Rx(GetUtilityByIdModel());

  RxBool isSwitched = false.obs;
  // final InstrumentDetailsController instrumentDetailsController=Get.find<InstrumentDetailsController>();
  final UtilityController utilityController=Get.find<UtilityController>();


  var utilityVillage="".obs;
  var apigetUtilityStatus=ApiStatus.LOADING.obs;

  var isLoan=false.obs;
  var isInsurance=false.obs;

  @override
  void onInit() async {
    super.onInit();
    if(Get.arguments!=null){
      await getUtilityById(id: Get.arguments["instrumentId"]);
      isLoan.value=Get.arguments["isLoan"];
      isInsurance.value=Get.arguments["isInsurance"];
    }
  }

  //get utility by id API
  Future<void> getUtilityById({id}) async {
    apigetUtilityStatus.value = ApiStatus.LOADING;
    try {
      var result = await UtilityRepoImpl(dioClient).getUtilityById(id: id);
      print("===================== get utility by id ==============");
      print(result);
      result.fold((model) {
        if(model.statusCode==200){
          getUtilityByIdModel.value = model;
      }
        apigetUtilityStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apigetUtilityStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

   //send Loan notification API
  Future<void> sendUtilityLoanNotification({utilId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await MyInstrumentDetailsRepoImpl(dioClient)
          .sendUtilityLoan(params1: {
        "id": Get.find<HomeController>().userId.value,
        "LoanAgentId": 48,
        "utilityId": utilId
      });
      print('util id is:${utilId}');
      print('user id is:${Get.find<HomeController>().userId.value}');
      print(
          "======================== sent utility loan notification ====================");
      print(result);
      result.fold((model) {
        sendNotificationForLoanUtilityModel.value = model;
        if (sendNotificationForLoanUtilityModel.value.statusCode == 200) {
          Get.defaultDialog(
          title: "Success",
          middleText: "Your request has been sent to the loan agent. Please wait for further processing.",
          middleTextStyle:TextStyle(fontSize: 12,color: colorBlack),
          textConfirm: "OK",
          buttonColor: primary,
          confirmTextColor: colorWhite,
          onConfirm: () {
            Get.back(); // Close the dialog
          },
          titleStyle: TextStyle(color: colorGreen),
        );
        } else if (sendNotificationForLoanUtilityModel.value.statusCode == 400) {
          Get.defaultDialog(
          title: "Alert",
          middleText: "Your request has already been sent to the loan agent. Please wait for further processing.",
          middleTextStyle:TextStyle(fontSize: 12,color: colorBlack),
          textConfirm: "OK",
          buttonColor: colorRed,
          confirmTextColor: colorWhite,
          onConfirm: () {
            Get.back(); // Close the dialog
          },
          titleStyle: TextStyle(color: colorRed),
        );
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

   //send Insurance notification API
  Future<void> sendUtilityInsuranceNotification({utilId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await MyInstrumentDetailsRepoImpl(dioClient)
          .sendUtilityLoan(params1: {
        "id": Get.find<HomeController>().userId.value,
        "insurance_agent_id":6,
        "utilityId": utilId
      });
      print('util id is:${utilId}');
      print('user id is:${Get.find<HomeController>().userId.value}');
      print(
          "======================== sent utility Insurance notification ====================");
      print(result);
      result.fold((model) {
        sendNotificationForLoanUtilityModel.value = model;
        if (sendNotificationForLoanUtilityModel.value.statusCode == 200) {
           Get.defaultDialog(
          title: "Success",
          middleText: "Your request has been sent to the Insurance agent. Please wait for further processing.",
          middleTextStyle:TextStyle(fontSize: 12,color: colorBlack),
          textConfirm: "OK",
          buttonColor: primary,
          confirmTextColor: colorWhite,
          onConfirm: () {
            Get.back(); // Close the dialog
          },
          titleStyle: TextStyle(color: colorGreen),
        );
        } else if (sendNotificationForLoanUtilityModel.value.statusCode == 400) {
          Get.defaultDialog(
          title: "Alert",
          middleText: "Your request has already been sent to the Insurance agent. Please wait for further processing.",
          middleTextStyle:TextStyle(fontSize: 12,color: colorBlack),
          textConfirm: "OK",
          buttonColor: colorRed,
          confirmTextColor: colorWhite,
          onConfirm: () {
            Get.back(); // Close the dialog
          },
          titleStyle: TextStyle(color: colorRed),
        );
        }
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
