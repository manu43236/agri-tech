
import 'package:agritech/models/otp_model.dart';
import 'package:dartz/dartz.dart';

abstract class LoginOTPRepo{
   Future<Either<OTPModel,Exception>> doLoginOTP({params1});
}

