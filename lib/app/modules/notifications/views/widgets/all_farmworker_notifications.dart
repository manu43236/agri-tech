import 'package:agritech/app/modules/notifications/controllers/notifications_controller.dart';
import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/base/base_controller.dart';
import '../../../../../core/utils/widget_utils/date_format.dart';
import '../../../../../core/utils/widget_utils/shimmers.dart';

class AllFarmworkerNotifications extends GetView<NotificationsController> {
  const AllFarmworkerNotifications({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'notifications'.tr,
          style: TextStyles.kTSFS24W600
              .copyWith(color: colorHello, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            controller.getNotificationsCount();
            Get.back();
          },
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          controller.getNotifications(userId: controller.id.value);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Divider(thickness: 1, color: colorAsh),
            Expanded(
              child: Obx(() {
                final notifications =
                    controller.getNotificationsModel.value.result ?? [];
                //  final notifications = controller.notifications;
                // Check if notifications are empty
                // if (notifications.isEmpty) {
                //   return Center(
                //     child: Text(
                //       'No notifications found for the specified ${controller.role.value} Id.',
                //       style: TextStyles.kTSCF12W500.copyWith(color: colorBlack),
                //     ),
                //   );
                // }

                // Display notifications list
                return controller.apiStatus.value == ApiStatus.LOADING
                      ? Shimmers().getListShimmer()
                      :ListView.separated(
                  itemCount: notifications.length,
                  separatorBuilder: (context, index) => const Divider(
                    thickness: 1,
                    color: colorAsh,
                  ),
                  itemBuilder: (context, index) {
                    final notification = notifications[index];
                    final imageUrl = notification.imageUrl ?? '';
                    final name = notification.name ?? 'Unknown';
                    final message = notification.message ?? '';

                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InkWell(
                            onTap: () async {
                              controller.updateState(index);
                              print('on tappping item');
                              controller.notificationId.value =
                                  notification.id.toString();
                              await controller.markAsViewed();
                              controller.getNotificationsCount();
                              controller.getNotifications(userId: controller.id.value);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                // Notification dot
                                notification.isRead == false
                                    ? const Padding(
                                        padding: EdgeInsets.only(bottom: 45),
                                        child: CircleAvatar(
                                          radius: 4,
                                          backgroundColor: Color.fromARGB(
                                              255, 65, 126, 231),
                                          child: CircleAvatar(
                                            radius: 3,
                                            backgroundColor: Color.fromARGB(
                                                255, 141, 193, 236),
                                          ),
                                        ),
                                      )
                                    : const SizedBox(),
                                const SizedBox(width: 10),
                                // Profile image
                                CircleAvatar(
                                  radius: 20,
                                  backgroundColor: colorAsh,
                                  child: ClipOval(
                                    child: imageUrl.isNotEmpty
                                        ? Image.network(
                                            imageUrl,
                                            height: Get.height * 0.05,
                                            width: Get.width * 0.2,
                                            fit: BoxFit.cover,
                                            errorBuilder: (context, error,
                                                    stackTrace) =>
                                                const Icon(Icons.error,
                                                    color: colorBlack),
                                          )
                                        : const Icon(Icons.person,
                                            color: colorBlack),
                                  ),
                                ),
                                const SizedBox(width: 10),
                                // Notification details
                                Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 5),
                                    child: 
                                     Text(message,style: TextStyles
                                                            .kTSCF12W500
                                                            .copyWith(
                                                                color:
                                                                    colorBlack),
                                                      ),
                                    // Text.rich(
                                    //   TextSpan(
                                    //     children: [
                                    //       TextSpan(
                                    //         text: '$name: ',
                                    //         style: TextStyles.kTSCF12W500
                                    //             .copyWith(
                                    //           color: colorKnockGray,
                                    //           fontWeight: FontWeight.w900,
                                    //         ),
                                    //       ),
                                    //       TextSpan(
                                    //         text: message,
                                    //         style: TextStyles.kTSCF12W500
                                    //             .copyWith(color: colorBlack),
                                    //       ),
                                    //     ],
                                    //   ),
                                    //),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          notification.statusForFarmworker == "Interested"
                              ? SizedBox()
                              :
                              notification.statusForFarmworker == "Accepted"
                              ? SizedBox()
                              :
                              //notification.statusForLandlord.toString().toLowerCase()=="accepted"&&notification.statusForFarmer.toString().toLowerCase()=="interested"?SizedBox():
                              Obx(() {
                                  // return notification.statusForLandlord.toString().toLowerCase()=="accepted"&&notification.statusForFarmer.toString().toLowerCase()=="interested"?SizedBox():
                                  return controller.isButton.value &&
                                          controller.selectedNotificationIndex
                                                  .value ==
                                              index
                                      ? Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            InkWell(
                                              onTap: () async {
                                                // controller.notificationId
                                                //     .value = notification.id
                                                //         ?.toString() ??
                                                //     '';
                    
                                                // await controller
                                                //     .approvedFarmworker(
                                                //         id: notification
                                                //             .from);
                                                controller.farmworkerIds.value=notification.to.toString();
                                                controller.toSenderId.value=notification.from.toString();
                                                controller.jobIds.value=notification.jobId.toString();
                                                await controller.respondtoLandlordOrFarmerByWorker();
                                                controller.getNotifications(userId: controller.id.value);
                                              },
                                              child: Container(
                                                padding: const EdgeInsets
                                                    .symmetric(
                                                    horizontal: 16,
                                                    vertical: 3),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          6),
                                                  color:
                                                      colorKnockBrandPrimary,
                                                ),
                                                child: Text(
                                                  'intrested'.tr,
                                                  style:
                                                      TextStyles.kTSNFS16W500,
                                                ),
                                              ),
                                            ),
                                            const SizedBox(width: 20),
                                            InkWell(
                                              onTap: () {
                                                controller
                                                        .landlordNotificationId
                                                        .value =
                                                    controller
                                                        .getNotificationsModel
                                                        .value
                                                        .result![index]
                                                        .id
                                                        .toString();
                                                controller
                                                    .deleteNotificationById();
                                                // controller
                                                //     .getAllFarmworkerNotifications();
                                                controller.getNotifications(userId: controller.id.value);
                                              },
                                              child: Container(
                                                padding: const EdgeInsets
                                                    .symmetric(
                                                    horizontal: 16,
                                                    vertical: 3),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          6),
                                                  border: Border.all(
                                                      width: 1,
                                                      color:
                                                          colorDeclineBorder),
                                                ),
                                                child: Text(
                                                  'decline'.tr,
                                                  style: TextStyles
                                                      .kTSNFS16W500
                                                      .copyWith(
                                                          color: colorBlack),
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      : const SizedBox(); // Empty space when buttons are hidden
                                }),
                          const SizedBox(height: 10),
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              formatDateToIST(
                                  notification.createdAt.toString()),
                              style: TextStyles.kTSDS14W500.copyWith(
                                color: colorDayTime,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                );
              }),
            ),
          ],
        ),
      ),
    );
  }
}
