// To parse this JSON data, do
//
//     final landlordFarmWorkerModel = landlordFarmWorkerModelFromJson(jsonString);

import 'dart:convert';

LandlordFarmWorkerModel landlordFarmWorkerModelFromJson(String str) => LandlordFarmWorkerModel.fromJson(json.decode(str));

String landlordFarmWorkerModelToJson(LandlordFarmWorkerModel data) => json.encode(data.toJson());

class LandlordFarmWorkerModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    LandlordFarmWorkerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory LandlordFarmWorkerModel.fromJson(Map<String, dynamic> json) => LandlordFarmWorkerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<Farmer>? farmers;

    Result({
        this.farmers,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        farmers: json["farmers"] == null ? [] : List<Farmer>.from(json["farmers"]!.map((x) => Farmer.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "farmers": farmers == null ? [] : List<dynamic>.from(farmers!.map((x) => x.toJson())),
    };
}

class Farmer {
    int? id;
    String? firstName;
    String? mobileNumber;
    String? role;
    int? age;
    dynamic gender;
    String? address;
    dynamic village;
    dynamic mandal;
    dynamic district;
    dynamic state;
    int? otp;
    double? longitude;
    double? latitude;
    String? accesstoken;
    int? profileId;
    String? imageUrl;
    String? description;
    String? locationDetails;
    bool? flag;
    DateTime? createdAt;
    DateTime? updatedAt;

    Farmer({
        this.id,
        this.firstName,
        this.mobileNumber,
        this.role,
        this.age,
        this.gender,
        this.address,
        this.village,
        this.mandal,
        this.district,
        this.state,
        this.otp,
        this.longitude,
        this.latitude,
        this.accesstoken,
        this.profileId,
        this.imageUrl,
        this.description,
        this.locationDetails,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory Farmer.fromJson(Map<String, dynamic> json) => Farmer(
        id: json["id"],
        firstName: json["firstName"],
        mobileNumber: json["mobileNumber"],
        role: json["role"],
        age: json["age"],
        gender: json["gender"],
        address: json["address"],
        village: json["village"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        otp: json["otp"],
        longitude: json["longitude"]?.toDouble(),
        latitude: json["latitude"]?.toDouble(),
        accesstoken: json["accesstoken"],
        profileId: json["profileId"],
        imageUrl: json["image_url"],
        description: json["Description"],
        locationDetails: json["location_details"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "mobileNumber": mobileNumber,
        "role": role,
        "age": age,
        "gender": gender,
        "address": address,
        "village": village,
        "mandal": mandal,
        "district": district,
        "state": state,
        "otp": otp,
        "longitude": longitude,
        "latitude": latitude,
        "accesstoken": accesstoken,
        "profileId": profileId,
        "image_url": imageUrl,
        "Description": description,
        "location_details": locationDetails,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
