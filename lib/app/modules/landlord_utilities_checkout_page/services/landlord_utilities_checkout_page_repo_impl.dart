import 'package:agritech/app/modules/landlord_utilities_checkout_page/services/landlord_utilities_checkout_page_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../../details_of_landlord_utilities/data/create_utility_payment_model.dart';
import '../data/pay_payment_model.dart';
import '../data/verify_utility_payment_model.dart';

class LandlordUtilitiesCheckoutPageRepoImpl extends LandlordUtilitiesCheckoutPageRepo with NetworkCheckService{
  final DioClient _dioClient;
  LandlordUtilitiesCheckoutPageRepoImpl(this._dioClient);
  Future<Either<CreateUtilityPaymentModel,Exception>> updateQuantity({params1})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('utilityPayment/updateUtilityPayment', Method.post,params: params1);
        return result.fold((l){
          CreateUtilityPaymentModel model=CreateUtilityPaymentModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  Future<Either<PayPaymentModel,Exception>> payPayment({paymentId})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('utilityPayment/payPayment?paymentId=$paymentId', Method.get);
        return result.fold((l){
          PayPaymentModel model=PayPaymentModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  Future<Either<PayPaymentModel,Exception>> payBalancePayment({balancePaymentId})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('utilityPayment/payPayment?utilityId=$balancePaymentId', Method.get);
        return result.fold((l){
          PayPaymentModel model=PayPaymentModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  Future<Either<VerifyUtilityPaymentModel,Exception>> verifyPayment({params1})async{
    var data = await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('utilityPayment/verifyUtilityPayment', Method.post,params: params1);
        return result.fold((l){
          VerifyUtilityPaymentModel model=VerifyUtilityPaymentModel.fromJson(l.data);
          return Left(model);
        }, (r)=> Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

}