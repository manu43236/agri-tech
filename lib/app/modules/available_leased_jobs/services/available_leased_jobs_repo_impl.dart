import 'package:agritech/app/modules/available_leased_jobs/services/available_leased_jobs_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../../home/controllers/home_controller.dart';
import '../../home/data/get_leased_jobs_details_model.dart';
import '../controllers/available_leased_jobs_controller.dart';

class AvailableLeasedJobsRepoImpl extends AvailableLeasedJobsRepo with NetworkCheckService{
  final DioClient _dioClient;
  AvailableLeasedJobsRepoImpl(this._dioClient);

  @override
  Future<Either<GetLeasedJobDetailsModel,Exception>>  leasedSearchJobs()async{
    var data= await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Leasetaker/getLeaseTakerDetails?type=${Get.find<HomeController>().role.value}&id=${Get.find<HomeController>().userId.value}&search=${Get.find<AvailableLeasedJobsController>().workerLeasedJobSearch.value}', Method.get,);
        return result.fold((l) {
          GetLeasedJobDetailsModel model = GetLeasedJobDetailsModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}