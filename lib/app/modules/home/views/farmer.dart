import 'package:agritech/app/modules/home/views/widgets/farmer_my_jobs_widget.dart';
import 'package:agritech/app/modules/home/views/widgets/farmer_my_lands_widget.dart';
import 'package:agritech/app/modules/home/views/widgets/leased_lands_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/home_controller.dart';
import '../controllers/landlord_controller.dart';
import 'widgets/leased_utilities_widget.dart';
import 'widgets/recent_lands_widget.dart';


class Farmerr extends GetView<LandlordController> {
  const Farmerr({super.key});

  @override
  Widget build(BuildContext context) {
    return  SafeArea(
      child: RefreshIndicator(
        onRefresh: () async{
           controller.recentLands();
           controller.addAllFarmersLands(userId: Get.find<HomeController>().userId.value);
           controller.addAllFarmerjobs(userId: Get.find<HomeController>().userId.value);
           controller.leasedLands(userId: Get.find<HomeController>().userId.value,role: Get.find<HomeController>().role.value);
           controller.leasedUtilities(userId: Get.find<HomeController>().userId.value);
        },
        child: const SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RecentLandsWidget(),
              //RecentJobsWidget(),
              FarmerMyLandsWidget(),
              FarmerMyJobsWidget(),
              LeasedLandsWidget(),
              LeasedUtilitiesWidget()
            ],
          ),
        ),
      ),
    );
  }
}