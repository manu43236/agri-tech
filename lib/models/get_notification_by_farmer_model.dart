// To parse this JSON data, do
//
//     final getNotificationByFarmerModel = getNotificationByFarmerModelFromJson(jsonString);

import 'dart:convert';

GetNotificationByFarmerModel getNotificationByFarmerModelFromJson(String str) => GetNotificationByFarmerModel.fromJson(json.decode(str));

String getNotificationByFarmerModelToJson(GetNotificationByFarmerModel data) => json.encode(data.toJson());

class GetNotificationByFarmerModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<NfeResult>? result;

    GetNotificationByFarmerModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetNotificationByFarmerModel.fromJson(Map<String, dynamic> json) => GetNotificationByFarmerModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<NfeResult>.from(json["result"]!.map((x) => NfeResult.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class NfeResult {
    int? id;
    dynamic message;
    dynamic name;
    dynamic from;
    dynamic to;
    bool? isRead;
    dynamic statusForLandlord;
    dynamic statusForFarmer;
    dynamic imageUrl;
    int? landId;
    int? jobId;
    dynamic statusForFarmworker;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;

    NfeResult({
        this.id,
        this.message,
        this.name,
        this.from,
        this.to,
        this.isRead,
        this.statusForLandlord,
        this.statusForFarmer,
        this.imageUrl,
        this.landId,
        this.jobId,
        this.statusForFarmworker,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory NfeResult.fromJson(Map<String, dynamic> json) => NfeResult(
        id: json["id"],
        message: json["message"],
        name: json["name"],
        from: json["from"],
        to: json["to"],
        isRead: json["is_read"],
        statusForLandlord: json["status_for_landlord"],
        statusForFarmer: json["status_for_farmer"],
        imageUrl: json["image_url"],
        landId: json["landId"],
        jobId: json["jobId"],
        statusForFarmworker: json["status_for_farmworker"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "message": message,
        "name": name,
        "from": from,
        "to": to,
        "is_read": isRead,
        "status_for_landlord": statusForLandlord,
        "status_for_farmer": statusForFarmer,
        "image_url": imageUrl,
        "landId": landId,
        "jobId": jobId,
        "status_for_farmworker": statusForFarmworker,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
