import 'package:agritech/app/modules/edit_profile/services/edit_profile_repo_impl.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart' as dio;
import 'package:image_picker/image_picker.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../core/utils/widget_utils/location/location.dart';
import '../../../../models/profile_model.dart';

class EditProfileController extends BaseController {
  Rx<TextEditingController> mobileController = Rx(TextEditingController());
  Rx<TextEditingController> nameController = Rx(TextEditingController());
  Rx<TextEditingController> addressController = Rx(TextEditingController());
  Rx<TextEditingController> ageController = Rx(TextEditingController());
 // Rx<SingleValueDropDownController> roleController =Rx(SingleValueDropDownController());
  Rx<TextEditingController> roleController = Rx(TextEditingController());
  Rx<TextEditingController> descriptionController = Rx(TextEditingController());

  var profileImage = Rx<XFile?>(null);
  var profilePath = ''.obs;
  var profileFile = ''.obs;
  var selectedValue = ''.obs;
  var longitude = 0.0.obs;
  var latitude = 0.0.obs;
  var address = "".obs;
  var image = "".obs;
  var profileName = "".obs;
  var role = "".obs;
  var mobNum = ''.obs;
  var id = "".obs;
  var userId="".obs;
  var isEditing = false.obs;
  List<String> dropdownItems = ["Landlord", "Farmer", "Farm Worker"];

  Rx<ProfileModel> profileModel = Rx(ProfileModel());

  @override
  void onInit() async {
    super.onInit();
    userId.value = await SecureStorage().readData(key: "id") ?? "";
    final cController = Get.find<HomeController>().getUserModel();
    nameController.value.text = cController.result!.firstName.toString() ?? '';
    ageController.value.text = cController.result!.age.toString() ?? '';
    addressController.value.text = cController.result!.address.toString() ?? '';
    roleController.value.text=cController.result!.role.toString() ??'';
    descriptionController.value.text=cController.result!.description.toString()??'';
    profileFile.value=cController.result!.imageUrl.toString() ?? '';
    mobNum.value=cController.result!.mobileNumber.toString()??'';
    //mobNum.value = await SecureStorage().readData(key: "mobileNumber");
  // mobNum.value=Get.arguments['mobileNumber'];
    id.value = await SecureStorage().readData(key: "id");
    mobileController.value.text = mobNum.value;
    Location location = Location();
    await location.determinePosition();
    if (location.address.isNotEmpty) {
      latitude.value = location.latitude.toDouble();
      longitude.value = location.longitude.toDouble();
      address.value = location.address.toString();
      print('Address: ${address.value}');
    } else {
      print('Address could not be retrieved.');
    }
  }

  void setSelectedValue(String value) {
    selectedValue.value = value;
  }

  void toggleEditing() {
    isEditing.value = !isEditing.value;
  }

  Future<void> pickImage(ImageSource source) async {
    final XFile? image = await ImagePicker().pickImage(source: source);

    if (image != null) {
      // selectedImagePath.value = pickedFile.path;
      profileImage.value = image;
      profileFile.value = profileImage.value!.name;
      profilePath.value = profileImage.value!.path;
      // Upload the image using Dio or any other method you prefer.
    } else {
      Get.snackbar('Error', 'No image selected');
    }
  }

  Future<void> updateEditProfile() async {
    if (mobileController.value.text.isNotEmpty &&
        nameController.value.text.isNotEmpty &&
        addressController.value.text.isNotEmpty &&
        roleController.value.text.isNotEmpty &&
        descriptionController.value.text.isNotEmpty
        ) {
      try {
        dio.FormData formData = dio.FormData.fromMap({
          "id": id.value,
          "mobileNumber": mobNum.value,
          // "file": await dio.MultipartFile.fromFile(profilePath.value,
          //     filename: profileFile.value),
          "address": addressController.value.text,
          "age": ageController.value.text,
          "role": roleController.value.text,
          "firstName": nameController.value.text,
          "longitude": longitude.value,
          "latitude": latitude.value,
          "Description": descriptionController.value.text,
          "location_details": address.value
        });

        if (profilePath.value.isNotEmpty) {
        formData.files.add(
          MapEntry(
            "file",
            await dio.MultipartFile.fromFile(profilePath.value, filename: profileFile.value),
          ),
        );
      }

        var result = await EditProfileRepoImpl(dioClient)
            .doEditProfile(params1: formData);
        print('mobile number:${mobNum.value}');
        print('===============result=============');
        print(result);
        result.fold((left) {
          _handleResponse(left);
        }, (r) {
          apiStatus.value = ApiStatus.FAIL;
        });
      } catch (e) {
        print('Error: $e');
      }
    }
  }

  void _handleResponse(ProfileModel model) async {
    if (model.statusCode == 200) {
      print('==================edit profile==============');
      profileModel.value = model;
      profileName.value = profileModel.value.result!.firstName.toString();
      role.value = profileModel.value.result!.role.toString();
      image.value = profileModel.value.result!.imageUrl.toString();
      print(image.value);
      Get.back();
      Get.snackbar('Success', 'User updated successfully',
          snackPosition: SnackPosition.TOP,
          backgroundColor: colorGreen,
          colorText: colorBlack);
      Get.find<HomeController>().getUserById(id:userId.value);
    }
  }


  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
