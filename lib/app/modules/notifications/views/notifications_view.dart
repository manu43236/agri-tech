import 'package:agritech/app/modules/notifications/controllers/notifications_controller.dart';
import 'package:agritech/app/modules/notifications/views/widgets/all.dart';
import 'package:agritech/app/modules/notifications/views/widgets/all_farmer.dart';
import 'package:agritech/app/modules/notifications/views/widgets/all_farmworker_notifications.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NotificationsView extends GetView<NotificationsController> {
  const NotificationsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // PageView that contains all three pages
          Expanded(
            child: Obx(() {
              // Return the appropriate widget based on the role
              if (controller.role.value == 'Landlord') {
                return const All();
              } else if (controller.role.value == 'Farmer') {
                print('============farmer notifications=========');
                return const AllFarmer();
              } else {
                return const AllFarmworkerNotifications();
              }
            }),
          ),
        ],
      ),
    );
  }
}
