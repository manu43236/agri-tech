import 'package:agritech/app/modules/signup/services/get_otp_repo.dart';
import 'package:agritech/models/get_otp_model.dart';
import 'package:dartz/dartz.dart';
import '../../../../core/base/dio_client.dart';
import '../../../../core/network/network_check_service.dart';

class GetOtpRepoImpl extends GetOtpRepo with NetworkCheckService {
  final DioClient _dioClient;

  GetOtpRepoImpl(this._dioClient);

  @override
  Future<Either<GetOtpModel, Exception>> getOtp({params1}) async {
    bool data = await checkInternet();

    if (!data) {
      return Right(Exception('No Network found'));
    } else {
      try {
        print(params1);
        var result = await _dioClient.requestForAuth('User/getOtp', Method.post,
            params: params1);
        return result.fold((l) {
          GetOtpModel model = GetOtpModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      } catch (e) {
        return Right(Exception(e));
      }
    }
  }
}
