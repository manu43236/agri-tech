
import 'package:intl/intl.dart';

String formatDate(String dateStr) {
  DateTime dateTime = DateTime.parse(dateStr);
  return DateFormat('dd MMM yyyy').format(dateTime);
}

String formatDateToIST(String dateStr) {
  // Parse the date string
  DateTime utcDate = DateTime.parse(dateStr);

  // Convert to IST by adding 5 hours 30 minutes (IST is UTC+5:30)
  DateTime istDate = utcDate.add(const Duration(hours: 5, minutes: 30));

  // Format the date to 12-hour format with AM/PM
  final DateFormat formatter = DateFormat('dd-MM-yyyy  hh:mm a');
  return formatter.format(istDate);
}

