import 'package:agritech/core/conts/color_consts.dart';
import 'package:agritech/core/themes/text_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CircleDesign extends StatelessWidget {
  final Color color;
  final String imagePath;
  final String name;
  const CircleDesign(
      {super.key,
      required this.color,
      required this.imagePath,
      required this.name});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(6.0),
      child: Column(
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: 64,
                width: 64,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: color,
                ),
              ),
              Positioned(
                left: 2,
                child: Container(
                  height: 60,
                  width: 60,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: colorWhite,
                  ),
                ),
              ),
              Positioned(
                child: Container(
                    height: 55,
                    width: 55,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: color,
                      //   image: DecorationImage(
                      //   image: AssetImage(imagePath),
                      //   fit: BoxFit.fitHeight
                      // ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Image.asset(imagePath),
                    )),
              ),
            ],
          ),
          SizedBox(
            height: 15,
            child: Text(
              name,
              style: TextStyles.kTSCF12W500,
            ),
          )
        ],
      ),
    );
  }
}
