import 'package:agritech/app/modules/available_crops/data/filter_with_crop_and_quantity_model.dart';
import 'package:agritech/app/modules/available_crops/data/get_all_crops_model.dart';
import 'package:dartz/dartz.dart';

abstract class AvailableCropsRepo{
  Future<Either<GetAllCropsModel,Exception>> getCrops();
  Future<Either<FilterWithCropAndQuantityModel,Exception>> filterWithCropQuantity();
}