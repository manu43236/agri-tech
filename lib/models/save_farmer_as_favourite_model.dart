// To parse this JSON data, do
//
//     final saveFarmerAsFavouriteModel = saveFarmerAsFavouriteModelFromJson(jsonString);

import 'dart:convert';

SaveFarmerAsFavouriteModel saveFarmerAsFavouriteModelFromJson(String str) => SaveFarmerAsFavouriteModel.fromJson(json.decode(str));

String saveFarmerAsFavouriteModelToJson(SaveFarmerAsFavouriteModel data) => json.encode(data.toJson());

class SaveFarmerAsFavouriteModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    SaveFarmerAsFavouriteModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SaveFarmerAsFavouriteModel.fromJson(Map<String, dynamic> json) => SaveFarmerAsFavouriteModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    bool? flag;
    int? id;
    int? userid;
    int? farmerid;
    String? role;
    DateTime? updatedAt;
    DateTime? createdAt;
    dynamic landlordid;
    dynamic farmworkerid;
    dynamic landid;
    dynamic jobid;

    Result({
        this.flag,
        this.id,
        this.userid,
        this.farmerid,
        this.role,
        this.updatedAt,
        this.createdAt,
        this.landlordid,
        this.farmworkerid,
        this.landid,
        this.jobid,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        flag: json["flag"],
        id: json["id"],
        userid: json["userid"],
        farmerid: json["farmerid"],
        role: json["role"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        landlordid: json["landlordid"],
        farmworkerid: json["farmworkerid"],
        landid: json["landid"],
        jobid: json["jobid"],
    );

    Map<String, dynamic> toJson() => {
        "flag": flag,
        "id": id,
        "userid": userid,
        "farmerid": farmerid,
        "role": role,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "landlordid": landlordid,
        "farmworkerid": farmworkerid,
        "landid": landid,
        "jobid": jobid,
    };
}
