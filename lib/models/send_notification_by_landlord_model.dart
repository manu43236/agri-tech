// To parse this JSON data, do
//
//     final sendNotificationByLandlordModel = sendNotificationByLandlordModelFromJson(jsonString);

import 'dart:convert';

SendNotificationByLandlordModel sendNotificationByLandlordModelFromJson(String str) => SendNotificationByLandlordModel.fromJson(json.decode(str));

String sendNotificationByLandlordModelToJson(SendNotificationByLandlordModel data) => json.encode(data.toJson());

class SendNotificationByLandlordModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    SendNotificationByLandlordModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SendNotificationByLandlordModel.fromJson(Map<String, dynamic> json) => SendNotificationByLandlordModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    Notification? notification;
    dynamic name;

    Result({
        this.notification,
        this.name,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        notification: json["notification"] == null ? null : Notification.fromJson(json["notification"]),
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "notification": notification?.toJson(),
        "name": name,
    };
}

class Notification {
    bool? isRead;
    int? id;
    dynamic message;
    dynamic from;
    dynamic to;
    int? landId;
    dynamic imageUrl;
    dynamic name;
    dynamic statusForLandlord;
    bool? flag;
    dynamic updatedAt;
    dynamic createdAt;
    dynamic statusForFarmer;
    dynamic jobId;
    dynamic statusForFarmworker;

    Notification({
        this.isRead,
        this.id,
        this.message,
        this.from,
        this.to,
        this.landId,
        this.imageUrl,
        this.name,
        this.statusForLandlord,
        this.flag,
        this.updatedAt,
        this.createdAt,
        this.statusForFarmer,
        this.jobId,
        this.statusForFarmworker,
    });

    factory Notification.fromJson(Map<String, dynamic> json) => Notification(
        isRead: json["is_read"],
        id: json["id"],
        message: json["message"],
        from: json["from"],
        to: json["to"],
        landId: json["landId"],
        imageUrl: json["image_url"],
        name: json["name"],
        statusForLandlord: json["status_for_landlord"],
        flag: json["flag"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        statusForFarmer: json["status_for_farmer"],
        jobId: json["jobId"],
        statusForFarmworker: json["status_for_farmworker"],
    );

    Map<String, dynamic> toJson() => {
        "is_read": isRead,
        "id": id,
        "message": message,
        "from": from,
        "to": to,
        "landId": landId,
        "image_url": imageUrl,
        "name": name,
        "status_for_landlord": statusForLandlord,
        "flag": flag,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "status_for_farmer": statusForFarmer,
        "jobId": jobId,
        "status_for_farmworker": statusForFarmworker,
    };
}
