import 'package:agritech/app/modules/vendor/controllers/vendor_controller.dart';
import 'package:agritech/core/conts/img_const.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/base/base_controller.dart';
import '../../../../../core/conts/color_consts.dart';
import '../../../../../core/themes/text_styles.dart';
import '../../../../../core/utils/widget_utils/shimmers.dart';
import '../../../../routes/app_pages.dart';

class PurchasedCropsWidgets extends GetView<VendorController> {
  const PurchasedCropsWidgets({super.key});

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Obx(
        () => Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'purchased_crops'.tr,
              style: TextStyles.kTSDS14W500.copyWith(color: colorHello),
            ),
            const SizedBox(
              height: 5,
            ),
            controller.apiPurchasedStatus.value == ApiStatus.LOADING
                ? Shimmers().getShimmerItem()
                : controller.getVendorCropsModel.value.result !=
                            null &&
                        controller.getVendorCropsModel.value.result!.isNotEmpty
                    ? Column(
                        children: [
                          SizedBox(
                            height: Get.height * 0.14,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: controller
                                    .getVendorCropsModel
                                    .value
                                    .result!
                                    .length,
                                itemBuilder: (context, index) {
                                  var land = controller
                                      .getVendorCropsModel
                                      .value
                                      .result!
                                      [index].cropDetails!;
                                  final imageUrl =
                                      land.imageUrl ?? defaultImageUrl;
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: InkWell(
                                      onTap: () async {
                                        print(controller
                                      .getVendorCropsModel
                                      .value
                                      .result![index].agriYields!.first.id);
                                        Get.toNamed(Routes.DETAIL_PURCHASED_CROP,arguments: {"yieldId":controller
                                      .getVendorCropsModel
                                      .value
                                      .result![index].agriYields!.first.id,"id":controller
                                      .getVendorCropsModel
                                      .value
                                      .result![index].id});
                                      },
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(12),
                                        child: Image.network(
                                          imageUrl ?? '',
                                          width: Get.width * 0.42,
                                          fit: BoxFit.cover,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            // Display a local asset if the network image fails
                                            return Image.asset(
                                              defaultImageUrl,
                                              width: Get.width * 0.42,
                                              fit: BoxFit.cover,
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                          ),
                          const SizedBox(height: 5),
                          Align(
                            alignment: Alignment.topRight,
                            child: InkWell(
                              onTap: () async {
                                Get.toNamed(Routes.AVAILABLE_PURCHASED_CROPS);
                              },
                              child: Text(
                                'view_all'.tr,
                                style: TextStyles.kTSCF12W500.copyWith(
                                  fontSize: 10,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: Get.height * 0.03,
                          ),
                          Center(
                            child: Text(
                              'no_pcurchased_crops'.tr,
                              style: TextStyles.kTSFS18W500.copyWith(
                                color: colorGrey,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Get.height * 0.03,
                          ),
                        ],
                      ),
          ],
        ),
      ),
    );
  }
}
