// To parse this JSON data, do
//
//     final getUserRolesModel = getUserRolesModelFromJson(jsonString);

import 'dart:convert';

GetUserRolesModel getUserRolesModelFromJson(String str) => GetUserRolesModel.fromJson(json.decode(str));

String getUserRolesModelToJson(GetUserRolesModel data) => json.encode(data.toJson());

class GetUserRolesModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<UserResult>? result;

    GetUserRolesModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetUserRolesModel.fromJson(Map<String, dynamic> json) => GetUserRolesModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<UserResult>.from(json["result"]!.map((x) => UserResult.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class UserResult {
    int? id;
    dynamic name;

    UserResult({
        this.id,
        this.name,
    });

    factory UserResult.fromJson(Map<String, dynamic> json) => UserResult(
        id: json["id"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
    };
}
