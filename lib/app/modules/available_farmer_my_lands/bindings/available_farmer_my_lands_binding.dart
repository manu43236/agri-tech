import 'package:get/get.dart';

import '../../select_region/controllers/select_region_controller.dart';
import '../controllers/available_farmer_my_lands_controller.dart';

class AvailableFarmerMyLandsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AvailableFarmerMyLandsController>(
      () => AvailableFarmerMyLandsController(),
    );
    Get.lazyPut<SelectRegionController>(
      () => SelectRegionController(),fenix: true,
    );
  }
}
