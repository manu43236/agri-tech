import 'package:agritech/app/modules/details_of_farmworker/data/get_all_available_jobs_model.dart';
import 'package:agritech/app/modules/details_of_farmworker/data/send_work_notification_by_landlord_or_farmer_fetch_model.dart';
import 'package:dartz/dartz.dart';
import '../data/near_by_farmworkers_model.dart';

abstract class DetailsOfFarmworkerRepo{
  Future<Either<NearByFarmworkersModel,Exception>> nearByFarmworkers();
  Future<Either<GetAvailableJobsModel,Exception>> availableJobs();
  Future<Either<SendWorkNotificationByFarmerOrLandlordToFarmworkerFetchModel,Exception>>  fetchFarmworkers({params1});
}