import 'package:agritech/app/modules/add_land/services/add_land_by_farmer_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';

import '../../../../models/add_land_by_farmer_model.dart';

class AddLandByFarmerRepoImpl extends AddLandByFarmerRepo with NetworkCheckService{

  final DioClient _dioClient;
  AddLandByFarmerRepoImpl(this._dioClient);

  @override
  Future<Either<AddLandByFarmerModel,Exception>> doAddLandFarmer({params1})async{
    bool data=await checkInternet();
    if(!data){
      return Right(Exception("No Internet found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Land/addLandByFarmer', Method.post,params: params1);
        return result.fold((l) {
          AddLandByFarmerModel model = AddLandByFarmerModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}