import 'package:dartz/dartz.dart';

import '../data/district_model.dart';
import '../data/mandal_model.dart';
import '../data/state_model.dart';
import '../data/village_model.dart';

abstract class SelectRegionRepo{
   Future<Either<StateModel,Exception>> getState();
   Future<Either<DistrictModel,Exception>>  getDistrict();
   Future<Either<MandalModel,Exception>> getMandal();
   Future<Either<VillageModel,Exception>> getVillage();
}