import 'package:dartz/dartz.dart';
import '../../../../models/add_job_by_farmer_model.dart';

abstract class AddJobByFarmerRepo{
  Future<Either<AddJobByFarmerModel,Exception>>  doAddFarmerJob({params1});
}