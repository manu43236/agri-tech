import 'package:agritech/app/modules/available_farmer_my_lands/controllers/available_farmer_my_lands_controller.dart';
import 'package:agritech/app/modules/available_farmer_my_lands/services/available_farmer_my_lands_repo.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../../home/data/get_all_lands_by_farmer_model.dart';

class AvailableFarmerMyLandsRepoImpl extends AvailableFarmerMyLandsRepo with NetworkCheckService{
  final DioClient _dioClient;
  AvailableFarmerMyLandsRepoImpl(this._dioClient);

  @override
  Future<Either<GetAllLandsByFarmerModel,Exception>> getFarmerLandsSearch()async{
    var data=await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result=await _dioClient.requestForAuth('Land/getAllLandsByFarmer?farmerId=${Get.find<HomeController>().userId.value}&search=${Get.find<AvailableFarmerMyLandsController>().farmerLandsSearch.value}', Method.get,);
         return result.fold((l) {
          GetAllLandsByFarmerModel model = GetAllLandsByFarmerModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }
}