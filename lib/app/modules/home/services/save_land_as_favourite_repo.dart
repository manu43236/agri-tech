import 'package:agritech/models/save_land_as_favourite_model.dart';
import 'package:dartz/dartz.dart';

abstract class SaveLandAsFavouriteRepo{
  Future<Either<SaveLandAsFavouriteModel,Exception>>  saveLand({params1});
}