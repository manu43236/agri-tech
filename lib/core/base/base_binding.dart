import 'package:agritech/core/base/session_controller.dart';
import 'package:get/get.dart';


class BaseBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SessionController());
  }
}
