import 'package:agritech/core/base/base_controller.dart';
import 'package:agritech/core/utils/storage_utils/secure_storage.dart';
import 'package:get/get.dart';

import '../../details_of_landlord_utilities/data/create_utility_payment_model.dart';
import '../../details_of_landlord_utilities/services/details_of_landlord_utilities_repo_impl.dart';
import '../../saved_utilities/data/get_all_saved_utility_model.dart';
import '../../saved_utilities/services/saved_utilities_repo_impl.dart';
import '../../utility/data/get_recent_four_utilities_model.dart';
import '../../utility/data/get_utility_by_id_model.dart';
import '../../utility/services/utility_repo_impl.dart';


class DetailSavedUtilitiesController extends BaseController {

  Rx<GetUtilityByIdModel> getUtilityByIdModel = Rx(GetUtilityByIdModel());
  Rx<GetRecentFourUtilitiesModel> getUtilityByLocationModel = Rx(GetRecentFourUtilitiesModel());
  Rx<GetAllSavedUtilityModel> getAllSavedUtilityModel =
      Rx(GetAllSavedUtilityModel());
  Rx<CreateUtilityPaymentModel> createUtilityPaymentModel = Rx(CreateUtilityPaymentModel());
  Rx<CreateUtilityPaymentModel> getUtilityPaymentByIdModel = Rx(CreateUtilityPaymentModel());

  var apigetUtilityStatus=ApiStatus.LOADING.obs;
  var utilityVillage=''.obs;
  var userId=''.obs;
  var createdUtilityId="".obs;
  var paymentUtilityId="".obs;

  @override
  void onInit() async{
    super.onInit();
    userId.value=await SecureStorage().readData(key: "id");
    if(Get.arguments!=null){
      await getSavedUtilities(userId:userId.value );
      await getUtilityById(id: Get.arguments["utilIds"]);
      await getUtilityByLocation(village: utilityVillage.value);
    }
  }

  //get utility by id API
  Future<void> getUtilityById({id}) async {
    apigetUtilityStatus.value = ApiStatus.LOADING;
    try {
      var result = await UtilityRepoImpl(dioClient).getUtilityById(id: id);
      print("===================== get utility by id ==============");
      print(result);
      result.fold((model) {
        if(model.statusCode==200){
        getUtilityByIdModel.value = model;
        utilityVillage.value=getUtilityByIdModel.value.result!.utility!.village.toString();      
      }
        apigetUtilityStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apigetUtilityStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //get utility by location API
  Future<void> getUtilityByLocation({village}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailsOfLandlordUtilitiesRepoImpl(dioClient).getUtilitiesByLocation(village: village);
      print("===================== get utility by location ==============");
      print(result);
      result.fold((model) {
        getUtilityByLocationModel.value = model;
        print(getUtilityByLocationModel.value.result![0].nameOfInstrument);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //get saved utilities API
  Future<void> getSavedUtilities({userId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedUtilitiesRepoImpl(dioClient).getSavedUtilities(userId: userId);
      print(
          "======================== get saved utilities ====================");
      print(result);
      result.fold((model) async {
        getAllSavedUtilityModel.value = model;
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  //create utility payment API
  Future<void> createUtilityPayment({utilityId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailsOfLandlordUtilitiesRepoImpl(dioClient).createUtilityPayment(params1:{
        "userId": userId.value, //Get.find<HomeController>().selectedLandlordId.value,
        "utilityId": utilityId //Get.find<UtilityController>().utilityId.value
      });
      print("===================== create utility payment ==============");
      print(result);
      result.fold((model) {
        createUtilityPaymentModel.value = model;
        createdUtilityId.value = createUtilityPaymentModel.value.result!.id.toString();
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  //get Utility Payment By Id  API
  Future<void> getUtilityPayment({createdUtillityId}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await DetailsOfLandlordUtilitiesRepoImpl(dioClient).getUtilityPayment(id: createdUtillityId);
      print("===================== get utility payment by id ==============");
      print(result);
      result.fold((model) {
        getUtilityPaymentByIdModel.value = model;
        paymentUtilityId.value = getUtilityPaymentByIdModel.value.result!.id.toString();
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
