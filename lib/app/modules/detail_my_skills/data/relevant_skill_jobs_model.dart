// To parse this JSON data, do
//
//     final relavantSkillJobsModel = relavantSkillJobsModelFromJson(jsonString);

import 'dart:convert';

RelavantSkillJobsModel relavantSkillJobsModelFromJson(String str) => RelavantSkillJobsModel.fromJson(json.decode(str));

String relavantSkillJobsModelToJson(RelavantSkillJobsModel data) => json.encode(data.toJson());

class RelavantSkillJobsModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    List<Result>? result;

    RelavantSkillJobsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory RelavantSkillJobsModel.fromJson(Map<String, dynamic> json) => RelavantSkillJobsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    int? landLordId;
    int? farmerId;
    dynamic fullName;
    dynamic startDate;
    dynamic endDate;
    dynamic latitude;
    dynamic longitude;
    dynamic state;
    dynamic district;
    dynamic village;
    dynamic mandal;
    dynamic description;
    int? imageId;
    dynamic image;
    dynamic status;
    bool? flag;
    dynamic createdAt;
    dynamic updatedAt;

    Result({
        this.id,
        this.landLordId,
        this.farmerId,
        this.fullName,
        this.startDate,
        this.endDate,
        this.latitude,
        this.longitude,
        this.state,
        this.district,
        this.village,
        this.mandal,
        this.description,
        this.imageId,
        this.image,
        this.status,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        startDate: json["startDate"] == null ? null : DateTime.parse(json["startDate"]),
        endDate: json["endDate"] == null ? null : DateTime.parse(json["endDate"]),
        latitude: json["latitude"],
        longitude: json["longitude"],
        state: json["state"],
        district: json["district"],
        village: json["village"],
        mandal: json["mandal"],
        description: json["description"],
        imageId: json["image_id"],
        image: json["image"],
        status: json["status"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "fullName": fullName,
        "startDate": startDate?.toIso8601String(),
        "endDate": endDate?.toIso8601String(),
        "latitude": latitude,
        "longitude": longitude,
        "state": state,
        "district": district,
        "village": village,
        "mandal": mandal,
        "description": description,
        "image_id": imageId,
        "image": image,
        "status": status,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
