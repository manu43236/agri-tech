import 'package:agritech/app/modules/profile/controllers/profile_controller.dart';
import 'package:agritech/app/modules/signup/controllers/signup_controller.dart';
import 'package:get/get.dart';

import '../controllers/otp_controller.dart';

class OtpBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OtpController>(
      () => OtpController(),
    );
  }
}
