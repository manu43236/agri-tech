import 'package:agritech/core/base/base_controller.dart';
import 'package:get/get.dart';

import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../saved/data/get_all_saved_farmworkers_model.dart';
import '../../saved/services/saved_repo_impl.dart';

class SavedFarmworkersController extends BaseController {

  Rx<GetAllSavedFarmworkerModel> getAllSavedFarmworkerModel =
      Rx(GetAllSavedFarmworkerModel());

  var userId=''.obs;

  @override
  void onInit() async{
    super.onInit();
    userId.value = await SecureStorage().readData(key: "id") ?? "";
    allFarmworkersSaved(id: userId.value);
  }

   Future<void> allFarmworkersSaved({id}) async {
    apiStatus.value = ApiStatus.LOADING;
    try {
      var result = await SavedRepoImpl(dioClient)
          .allFarmworkers(userId: id);
      print("===================== get all saved farmworkers ==============");
      print(result);
      result.fold((model) {
        getAllSavedFarmworkerModel.value = model;
        print(getAllSavedFarmworkerModel.value.result!.farmerCount);
        apiStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('error ${e}');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
