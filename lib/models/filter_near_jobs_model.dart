// To parse this JSON data, do
//
//     final filterNearJobsModel = filterNearJobsModelFromJson(jsonString);

import 'dart:convert';

FilterNearJobsModel filterNearJobsModelFromJson(String str) => FilterNearJobsModel.fromJson(json.decode(str));

String filterNearJobsModelToJson(FilterNearJobsModel data) => json.encode(data.toJson());

class FilterNearJobsModel {
    int? statusCode;
    String? status;
    String? message;
    List<Result>? result;

    FilterNearJobsModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory FilterNearJobsModel.fromJson(Map<String, dynamic> json) => FilterNearJobsModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    Job? job;
    User? user;

    Result({
        this.job,
        this.user,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        job: json["job"] == null ? null : Job.fromJson(json["job"]),
        user: json["user"] == null ? null : User.fromJson(json["user"]),
    );

    Map<String, dynamic> toJson() => {
        "job": job?.toJson(),
        "user": user?.toJson(),
    };
}

class Job {
    int? id;
    int? landLordId;
    dynamic farmerId;
    String? fullName;
    DateTime? startDate;
    DateTime? endDate;
    String? state;
    String? district;
    String? village;
    String? mandal;
    String? description;
    int? imageId;
    String? image;
    String? status;
    bool? flag;
    DateTime? createdAt;
    DateTime? updatedAt;

    Job({
        this.id,
        this.landLordId,
        this.farmerId,
        this.fullName,
        this.startDate,
        this.endDate,
        this.state,
        this.district,
        this.village,
        this.mandal,
        this.description,
        this.imageId,
        this.image,
        this.status,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory Job.fromJson(Map<String, dynamic> json) => Job(
        id: json["id"],
        landLordId: json["landLordId"],
        farmerId: json["farmerId"],
        fullName: json["fullName"],
        startDate: json["startDate"] == null ? null : DateTime.parse(json["startDate"]),
        endDate: json["endDate"] == null ? null : DateTime.parse(json["endDate"]),
        state: json["state"],
        district: json["district"],
        village: json["village"],
        mandal: json["mandal"],
        description: json["description"],
        imageId: json["image_id"],
        image: json["image"],
        status: json["status"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "landLordId": landLordId,
        "farmerId": farmerId,
        "fullName": fullName,
        "startDate": startDate?.toIso8601String(),
        "endDate": endDate?.toIso8601String(),
        "state": state,
        "district": district,
        "village": village,
        "mandal": mandal,
        "description": description,
        "image_id": imageId,
        "image": image,
        "status": status,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}

class User {
    int? id;
    String? firstName;
    String? mobileNumber;
    String? role;
    int? age;
    dynamic gender;
    String? address;
    dynamic village;
    dynamic mandal;
    dynamic district;
    dynamic state;
    int? otp;
    double? longitude;
    double? latitude;
    String? accesstoken;
    int? profileId;
    String? imageUrl;
    String? description;
    String? locationDetails;
    bool? flag;
    DateTime? createdAt;
    DateTime? updatedAt;

    User({
        this.id,
        this.firstName,
        this.mobileNumber,
        this.role,
        this.age,
        this.gender,
        this.address,
        this.village,
        this.mandal,
        this.district,
        this.state,
        this.otp,
        this.longitude,
        this.latitude,
        this.accesstoken,
        this.profileId,
        this.imageUrl,
        this.description,
        this.locationDetails,
        this.flag,
        this.createdAt,
        this.updatedAt,
    });

    factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        firstName: json["firstName"],
        mobileNumber: json["mobileNumber"],
        role: json["role"],
        age: json["age"],
        gender: json["gender"],
        address: json["address"],
        village: json["village"],
        mandal: json["mandal"],
        district: json["district"],
        state: json["state"],
        otp: json["otp"],
        longitude: json["longitude"]?.toDouble(),
        latitude: json["latitude"]?.toDouble(),
        accesstoken: json["accesstoken"],
        profileId: json["profileId"],
        imageUrl: json["image_url"],
        description: json["Description"],
        locationDetails: json["location_details"],
        flag: json["flag"],
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "mobileNumber": mobileNumber,
        "role": role,
        "age": age,
        "gender": gender,
        "address": address,
        "village": village,
        "mandal": mandal,
        "district": district,
        "state": state,
        "otp": otp,
        "longitude": longitude,
        "latitude": latitude,
        "accesstoken": accesstoken,
        "profileId": profileId,
        "image_url": imageUrl,
        "Description": description,
        "location_details": locationDetails,
        "flag": flag,
        "createdAt": createdAt?.toIso8601String(),
        "updatedAt": updatedAt?.toIso8601String(),
    };
}
