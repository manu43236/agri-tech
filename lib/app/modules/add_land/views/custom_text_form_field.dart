import 'package:flutter/material.dart';

import '../../../../core/conts/color_consts.dart';

class CustomTextFormField extends StatelessWidget {
  final TextEditingController controller;
  final String? Function(String?)? validator; // Add a validator parameter
  
  const CustomTextFormField({
    super.key,
    required this.controller,
    this.validator, // Initialize the validator parameter
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(vertical: 0,horizontal: 10),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(color: colorTextFieldBorder, width: 2),
        ),
        errorStyle: const TextStyle(
          color: Colors.red, // This will set the error text color to red
        ),
      ),
      validator: validator, // Assign the validator to the TextFormField
    );
  }
}
