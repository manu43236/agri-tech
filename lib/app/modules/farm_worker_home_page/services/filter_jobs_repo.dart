import 'package:agritech/app/modules/farm_worker_home_page/data/get_skills_by_user_id_model.dart';
import 'package:agritech/models/filter_jobs_model.dart';
import 'package:dartz/dartz.dart';

import '../../../../models/save_job_as_favourite_model.dart';
import '../data/send_request_by_farmworker_fetch_model.dart';

abstract class FilterJobsRepo{
  Future<Either<FilterJobsModel,Exception>> filteredJobs({params1});
  Future<Either<SaveJobAsFavouriteModel,Exception>> jobAsFavourite({params1});
  Future<Either<SendRequestByFarmworkerFetchModel,Exception>> farmworkersFetch({params1});
  Future<Either<GetSkillsByUserIdModel,Exception>> getUserSkills();
}