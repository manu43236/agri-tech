// To parse this JSON data, do
//
//     final saveLandAsFavouriteModel = saveLandAsFavouriteModelFromJson(jsonString);

import 'dart:convert';

SaveLandAsFavouriteModel saveLandAsFavouriteModelFromJson(String str) => SaveLandAsFavouriteModel.fromJson(json.decode(str));

String saveLandAsFavouriteModelToJson(SaveLandAsFavouriteModel data) => json.encode(data.toJson());

class SaveLandAsFavouriteModel {
    int? statusCode;
    String? status;
    String? message;
    Result? result;

    SaveLandAsFavouriteModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory SaveLandAsFavouriteModel.fromJson(Map<String, dynamic> json) => SaveLandAsFavouriteModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    bool? flag;
    int? id;
    int? userid;
    int? landlordid;
    int? landid;
    DateTime? updatedAt;
    DateTime? createdAt;
    dynamic farmerid;
    dynamic farmworkerid;
    dynamic role;
    dynamic jobid;

    Result({
        this.flag,
        this.id,
        this.userid,
        this.landlordid,
        this.landid,
        this.updatedAt,
        this.createdAt,
        this.farmerid,
        this.farmworkerid,
        this.role,
        this.jobid,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        flag: json["flag"],
        id: json["id"],
        userid: json["userid"],
        landlordid: json["landlordid"],
        landid: json["landid"],
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
        farmerid: json["farmerid"],
        farmworkerid: json["farmworkerid"],
        role: json["role"],
        jobid: json["jobid"],
    );

    Map<String, dynamic> toJson() => {
        "flag": flag,
        "id": id,
        "userid": userid,
        "landlordid": landlordid,
        "landid": landid,
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
        "farmerid": farmerid,
        "farmworkerid": farmworkerid,
        "role": role,
        "jobid": jobid,
    };
}
