import 'package:flutter/material.dart';

import '../themes/custom_material_color.dart';

const primary = Color(0xFF4D7881);
const secondary = Color(0xFF0D4A61);
final primaryMaterial = CustomMaterialColor.createMaterialColor(
  const Color((0xFFFFC518)),
);
final cartBorders = Colors.grey.shade300;
const appBarColor = Color(0xFF051B23);
const tabBarBgColor = Color(0xFFEBEEEB);
const hinttextColor = Color(0xff7A8B99);
const inActiveBtnColor = Color(0xFFBCC8CC);
const inActiveDotColor = Color(0xFFD9D9D9);

final appBgColor = const Color(0xFF051B23).withOpacity(0.5);
final unSelectedColor1 = const Color(0xffC0CDD1).withOpacity(0.9);
const unSelectedColor = Color(0xFFC0CDD1);

const colorWhite = Color(0xFFFFFFFF);
const colorLightGreen = Color(0xffE1E8EB);
const colorLightGreenText = Color(0xffC0CDD1);
const colorLightGreenLine = Color(0xff657C85);
const colorUeBg = Color(0xff051B23);
const colorUserGuide = Color(0xffE6EEF0);
const colorLighBlueBtnBg = Color(0xff1D88AF);
const addServiceTextColor = Color(0xff1D4452);
const guideTextColor = Color(0xff6F909C);
const addHMRTextColor = Color(0xff76858A);
const addServiceHintTextColor = Color(0xff657C85);
const addServiceDropDownBtnColor = Color(0xff156380);
const colorBlueDarkBtn = Color(0xff104B61);
const colorMachineWork = Color(0xffD4E6E9);
const colorGhostWhite=Color(0xffF8F8FF);
const colorHello=Color(0xff1D1A20);

const colorRed = Colors.red;
const colorGrey = Colors.grey;
const colorGreen = Colors.green;
const colorBrown = Colors.brown;
const colorBlueDarkText = Color(0xff32738A);
const colorBlueText = Color(0xff104B61);

// add service form colors
const colorAddMachine = Color(0xffFFC629);
const colorAddHmr = Color(0xff1E43BD);
const colorAddCustomer = Color(0xff1E43BD);
const colorAddTeam = Color(0xffA84E22);
const colorAddFuel = Color(0xff98BD1E);
const colorAddContract = Color(0xffB73C39);
const colorAddProject = Color(0xffD38538);
const colorAccecntYellow = Color(0xffFFDE85);
const colorAddRateCard = Color(0xff032AB2);
const colorUpdateService = Color(0xff1EBDAD);
const colorOrange = Colors.orange;
const colorPink = Colors.pink;
const colorPurple = Colors.purple;
const colorBlue = Colors.blue;
const colorBlack = Colors.black;
Color colorGryeW50 = Colors.grey.shade50;
Color colorGryeW200 = Colors.grey.shade200;
Color colorGryeW300 = Colors.grey.shade300;
Color colorGryeW600 = Colors.grey.shade600;
Color colorGryeW900 = Colors.grey.shade900;
Color colorRedW800 = Colors.red.shade800;
Color colorGreenW900 = Colors.green.shade900;

const colorGreenBg = Color(0xff363C24);

const utilizationCardOne = Color(0xff1D88AF);
const utilizationCardTwo = Color(0xffFFDE85);

const hmrCardOne = Color(0xff1E43BD);

const earningsCardOne = Color(0xff23A530);

const expenseCardOne = Color(0xffCC413D);
const fmServiceBgColor = Color(0xffF5F9FA);

const machineBgColor1 = Color(0xff229FCC);
const machineBgColor2 = Color(0xffFFDE85);
const machineBgColor3 = Color(0xffFFDE85);

const drawerHeaderColor = Color(0xff051B23);
const drawerProfileHeaderColor = Color(0xffE1E8EB);
const drawerBgColor = Color(0xffF5F9FA);
const drawertextColor = Color(0xffF5F9FA);

const basicInfoCardBgColor = Color(0xff9619C2);
const colorCircleLandlord=Color(0xffF9EFCB);
const colorCircleFarmer=Color(0xffD5EEF6);
const colorCircleWorker=Color(0xffF8E3FB);
const colorCircleUtilities=Color(0xffE3EDFB);
const colorCircleOthers= Color(0xffFBE3E3);
const colorTextFieldBorder=Color(0xffF0F6FF);
const colorHire=Color(0xffF1F1F1);
const colorDots=Color(0xffAEAEAE);
const colorFilter=Color(0xffF6F6F6);
const colorAsh=Color(0xffF5F5F5);
const colorCost=Color(0xff4D7881);
const colorDetails=Color(0xff0F2F44);
const colorPic=Color(0xff4D7881);
const colorSave=Color(0xffEAF1FF);
const colorAll=Color(0xffE4321B);
const colorUnread=Color(0xff697386);
const colorKnockGray=Color(0xff1A1F36);
const colordivider=Color(0xffE4E8EE);
const colorDayTime=Color(0xffA5ACB8);
const colorKnockBrandPrimary=Color(0xffE95744);
const colorDeclineBorder=Color(0xffDDDEE1);
const colorFinalDivider=Color(0xff5469D4);
const colorSaved=Color(0xff1C1C1C);
const colorSuitable=Color(0xff0F2F44);
const colorSuitableCrop=Color(0xff588089);
const colorbackground=Color(0xff333333);
const colorQuantity=Color(0xffD9D9D9);
const colorCircleLoans=Color(0xffFBE3E3);
const colorLoans=Color(0xffF0F0F0);
