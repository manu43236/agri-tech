// To parse this JSON data, do
//
//     final createSelectedCropWeekModel = createSelectedCropWeekModelFromJson(jsonString);

import 'dart:convert';

CreateSelectedCropWeekModel createSelectedCropWeekModelFromJson(String str) => CreateSelectedCropWeekModel.fromJson(json.decode(str));

String createSelectedCropWeekModelToJson(CreateSelectedCropWeekModel data) => json.encode(data.toJson());

class CreateSelectedCropWeekModel {
    int? statusCode;
    String? status;
    String? message;
    List<Result>? result;

    CreateSelectedCropWeekModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory CreateSelectedCropWeekModel.fromJson(Map<String, dynamic> json) => CreateSelectedCropWeekModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    int? id;
    int? userid;
    int? landid;
    int? cropId;
    int? weeks;
    int? steps;
    bool? weekenable;
    bool? stepsflag;
    DateTime? createdDate;
    DateTime? updatedAt;
    DateTime? createdAt;

    Result({
        this.id,
        this.userid,
        this.landid,
        this.cropId,
        this.weeks,
        this.steps,
        this.weekenable,
        this.stepsflag,
        this.createdDate,
        this.updatedAt,
        this.createdAt,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        userid: json["userid"],
        landid: json["landid"],
        cropId: json["cropId"],
        weeks: json["weeks"],
        steps: json["steps"],
        weekenable: json["weekenable"],
        stepsflag: json["stepsflag"],
        createdDate: json["created_date"] == null ? null : DateTime.parse(json["created_date"]),
        updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
        createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "userid": userid,
        "landid": landid,
        "cropId": cropId,
        "weeks": weeks,
        "steps": steps,
        "weekenable": weekenable,
        "stepsflag": stepsflag,
        "created_date": "${createdDate!.year.toString().padLeft(4, '0')}-${createdDate!.month.toString().padLeft(2, '0')}-${createdDate!.day.toString().padLeft(2, '0')}",
        "updatedAt": updatedAt?.toIso8601String(),
        "createdAt": createdAt?.toIso8601String(),
    };
}
