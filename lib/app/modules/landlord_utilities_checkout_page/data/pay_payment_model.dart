// To parse this JSON data, do
//
//     final payPaymentModel = payPaymentModelFromJson(jsonString);

import 'dart:convert';

PayPaymentModel payPaymentModelFromJson(String str) => PayPaymentModel.fromJson(json.decode(str));

String payPaymentModelToJson(PayPaymentModel data) => json.encode(data.toJson());

class PayPaymentModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    PayPaymentModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory PayPaymentModel.fromJson(Map<String, dynamic> json) => PayPaymentModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    dynamic orderId;
    dynamic amount;
    dynamic paymentId;
    dynamic paymentStatus;

    Result({
        this.orderId,
        this.amount,
        this.paymentId,
        this.paymentStatus,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        orderId: json["orderId"],
        amount: json["amount"],
        paymentId: json["paymentId"],
        paymentStatus: json["paymentStatus"],
    );

    Map<String, dynamic> toJson() => {
        "orderId": orderId,
        "amount": amount,
        "paymentId": paymentId,
        "paymentStatus": paymentStatus,
    };
}
