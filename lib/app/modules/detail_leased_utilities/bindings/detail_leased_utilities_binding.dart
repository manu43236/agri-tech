import 'package:get/get.dart';

import '../../detail_saved_utilities/controllers/detail_saved_utilities_controller.dart';
import '../../instrument_details/controllers/instrument_details_controller.dart';
import '../../leased_utilities_checkout_page/controllers/leased_utilities_checkout_page_controller.dart';
import '../controllers/detail_leased_utilities_controller.dart';

class DetailLeasedUtilitiesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailLeasedUtilitiesController>(
      () => DetailLeasedUtilitiesController(),
    );
     Get.lazyPut<InstrumentDetailsController>(
      () => InstrumentDetailsController(),
    );
     Get.lazyPut<LeasedUtilitiesCheckoutPageController>(
      () => LeasedUtilitiesCheckoutPageController(),
    );
     Get.lazyPut<DetailSavedUtilitiesController>(
      () => DetailSavedUtilitiesController(),
    );
  }
}
