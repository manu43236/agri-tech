import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../detail_recent_lands/data/land_by_id_model.dart';
import '../../home/services/land_by_id_repo_impl.dart';

class DetailLeasedLandsController extends BaseController {

  Rx<LandByIdModel> landByIdModel = Rx(LandByIdModel());

  final HomeController homeController=Get.find<HomeController>();

  var apiLandIdStatus = ApiStatus.LOADING.obs;
  var landId=''.obs;

  @override
  void onInit() {
    super.onInit();
    getIdLands(id: Get.arguments['landId']);
  }

  // Lands BY id API
  Future<void> getIdLands({id}) async {
    apiLandIdStatus.value = ApiStatus.LOADING;
    try {
      var result = await LandByIdRepoImpl(dioClient).getLand(params1: {
        "id": id,
      });

      result.fold((model) {
        landByIdModel.value = model;
        landId.value = model.result!.landDetails!.id.toString() ?? "";
        
        apiLandIdStatus.value = ApiStatus.SUCCESS;
      }, (r) {
        apiLandIdStatus.value = ApiStatus.FAIL;
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
