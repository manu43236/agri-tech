// To parse this JSON data, do
//
//     final stateModelResponse = stateModelResponseFromJson(jsonString);

import 'dart:convert';

StateModel stateModelResponseFromJson(String str) => StateModel.fromJson(json.decode(str));

String stateModelResponseToJson(StateModel data) => json.encode(data.toJson());

class StateModel {
    int? statusCode;
    String? status;
    String? message;
    List<StateResult>? result;

    StateModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory StateModel.fromJson(Map<String, dynamic> json) => StateModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? [] : List<StateResult>.from(json["result"]!.map((x) => StateResult.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class StateResult {
    int? id;
    dynamic name;

    StateResult({
        this.id,
        this.name,
    });

    factory StateResult.fromJson(Map<String, dynamic> json) => StateResult(
        id: json["id"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
    };
}
