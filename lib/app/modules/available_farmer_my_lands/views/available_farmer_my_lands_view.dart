import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/text_fields/custom_search_widget.dart';
import '../../../routes/app_pages.dart';
import '../../home/controllers/home_controller.dart';
import '../../select_region/views/select_region_view.dart';
import '../controllers/available_farmer_my_lands_controller.dart';

class AvailableFarmerMyLandsView
    extends GetView<AvailableFarmerMyLandsController> {
  const AvailableFarmerMyLandsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            'Available My Lands',
            style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
          ),
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: colorBlack,
            ),
            onPressed: () async {
              Get.back();
            },
          ),
          actions: [
            InkWell(
              onTap: () {
                 Get.bottomSheet(
                isScrollControlled: true,
                isDismissible: true,
                SelectRegionView(type: 'farmer_My_Lands'.tr,)
              );
              },
              child: Image.asset(
                cone,
                height: 20,
                width: 20,
              ),
            ),
            const SizedBox(
              width: 20,
            )
          ]),
      body: Obx(
        () => Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10,
                ),
                CustomSearchField(
                  onChanged: (val) {
                    controller.farmerLandsSearch.value = val;
                    if (val.length >= 3) {
                      print("dataaaa");
                      controller. farmerLandsFilterLandsList(farmerId:Get.find<HomeController>().userId.value,
    landlordId: '',search:controller.farmerLandsSearch.value,state: '',district: '',mandal: '',village: '' );
                      //controller.getFarmersLands();
                    } else if (val.isEmpty) {
                      controller. farmerLandsFilterLandsList(farmerId:Get.find<HomeController>().userId.value,
    landlordId: '',search:controller. farmerLandsSearch.value,state: '',district: '',mandal: '',village: '' );
                      //controller.getFarmersLands();
                    }
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                controller.dataSelectedItem.value.isEmpty
                    ? const SizedBox()
                    : Text('Your selected region is ${controller.dataSelectedItem.value}' ?? "", style: TextStyles.kTSDS14W500
                                          .copyWith(color: colorHello),),
                SizedBox(
                  height: 10,
                ),
                controller.apiStatus.value == ApiStatus.LOADING
                    ? const SizedBox()
                    : controller.farmerMyLandsFilterModel.value.result!.isEmpty
                        ? const Align(
                            alignment: Alignment.center,
                            child: Text(
                              "No Lands Found",
                              style:
                                  TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                          )
                        : Column(
                            children: _avaliLand(),
                          )
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _avaliLand() {
    List<Widget> items = [];
    for (var index = 0;
        index <
            controller.farmerMyLandsFilterModel.value.result!.length;
        index++) {
      items.add(SizedBox(
        child: InkWell(
          onTap: () async {
            Get.find<HomeController>().landId.value = controller
                .farmerMyLandsFilterModel.value.result![index].id
                .toString();
            Get.toNamed(Routes.DETAIL_FARMER_MY_LANDS,arguments:{"landId": Get.find<HomeController>().landId.value ,"isLoan":controller.isLoan.value,"isInsurance":controller.isInsurance.value});
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(11),
                    topRight: Radius.circular(11)),
                child: Image.network(
                  controller.farmerMyLandsFilterModel.value.result!
                          [index].imageUrl ??
                      '',
                  height: Get.height * 0.2,
                  width: Get.width,
                  fit: BoxFit.cover,
                  errorBuilder: (context, error, stackTrace) {
                    return Image.asset(
                      defaultImageUrl,
                      height: Get.height * 0.2,
                      width: Get.width,
                      fit: BoxFit.cover,
                    );
                  },
                ),
              ),
              Container(
                width: Get.width,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(11),
                        bottomRight: Radius.circular(11)),
                    color: colorAsh,
                    shape: BoxShape.rectangle),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${controller.farmerMyLandsFilterModel.value.result![index].fullName} Land at ${controller.farmerMyLandsFilterModel.value.result![index].village}",
                        style:
                            TextStyles.kTSDS14W500.copyWith(color: colorHello),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Extent-${controller.farmerMyLandsFilterModel.value.result![index].landInAcres} Acres      Water Facility-${controller.farmerMyLandsFilterModel.value.result![index].water != null ? controller.farmerMyLandsFilterModel.value.result![index].water : 'No'}',
                        style:
                            TextStyles.kTSFS10W500.copyWith(color: colorHello),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      // InkWell(
                      //   onTap: () {},
                      //   child: Container(
                      //     padding: const EdgeInsets.symmetric(
                      //         horizontal: 10, vertical: 5),
                      //     decoration: const BoxDecoration(
                      //       color: colorCost,
                      //     ),
                      //     child: const Text(
                      //       'Cost Enquiry',
                      //       style: TextStyle(
                      //         color: colorWhite,
                      //         fontSize: 12,
                      //       ),
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ));
    }
    return items;
  }
}
