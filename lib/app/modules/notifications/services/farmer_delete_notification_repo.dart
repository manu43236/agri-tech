import 'package:dartz/dartz.dart';

abstract class FarmerDeleteNotificationRepo{
  Future<Either<dynamic,Exception>>  deleteFarmerNotification({params1});
}