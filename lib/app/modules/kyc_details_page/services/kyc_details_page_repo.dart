import 'package:agritech/app/modules/kyc_details_page/data/upload_id_document_model.dart';
import 'package:dartz/dartz.dart';

abstract class KycDetailsPageRepo {
  Future<Either<UploadIdDocumentModel,Exception>> uploadId({params1});
}