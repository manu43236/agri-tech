import 'package:agritech/core/utils/widget_utils/date_format.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../routes/app_pages.dart';
import '../controllers/available_purchased_crops_controller.dart';

class AvailablePurchasedCropsView
    extends GetView<AvailablePurchasedCropsController> {
  const AvailablePurchasedCropsView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text(
          'available_crops'.tr,
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
              Get.back();
          },
        ),
        // actions: [
        //   InkWell(
        //     onTap: () {
        //       Get.toNamed(Routes.FILTER_VENDOR);
        //     },
        //     child: Image.asset(
        //       cone,
        //       height: 20,
        //       width: 20,
        //     ),
        //   ),
        //   const SizedBox(
        //     width: 20,
        //   )
        // ],
      ),
      body: Obx(()=>
         Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child:  SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  controller.apiPurchasedCrops.value == ApiStatus.LOADING
                      ? const SizedBox()
                      : controller.getPurchasedAvailableCropsModel.value.result!.isEmpty
                          ?  Align(
                              alignment: Alignment.center,
                              child: Text(
                                "no_crops_found".tr,
                                style: TextStyle(fontSize: 16, color: Colors.grey),
                              ),
                            )
                          : Column(
                              children: _lordLands(),
                            )
                ],
              ),
            ),
        ),
      ),
    );
  }

  List<Widget> _lordLands() {
    List<Widget> items = [];
    for (var index = 0;
        index <
            controller
                .getPurchasedAvailableCropsModel.value.result!.length;
        index++) {
      items.add(Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: InkWell(
          onTap: () async {
            Get.toNamed(Routes.DETAIL_PURCHASED_CROP,arguments:{"yieldId":controller.getPurchasedAvailableCropsModel.value.result![index].agriYields!.first.id,"id":controller.getPurchasedAvailableCropsModel.value.result![index].id});
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(11), topRight: Radius.circular(11)),
                child: Image.network(
                  controller.getPurchasedAvailableCropsModel.value.result?[index].cropDetails?.imageUrl ??
                      '',
                  height: Get.height * 0.2,
                  width: Get.width,
                  fit: BoxFit.cover,
                  errorBuilder: (context, error, stackTrace) {
                    return Image.asset(
                      defaultImageUrl,
                      height: Get.height * 0.2,
                      width: Get.width,
                      fit: BoxFit.cover,
                    );
                  },
                ),
              ),
              Container(
                width: Get.width,
                decoration: const BoxDecoration(
                    color: colorAsh,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(11),
                        bottomRight: Radius.circular(11)),
                    shape: BoxShape.rectangle),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${controller.getPurchasedAvailableCropsModel.value.result![index].cropDetails!.name}",
                        style: TextStyles.kTSDS14W500.copyWith(color: colorHello),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        "${'total_days'.tr} ${controller.getPurchasedAvailableCropsModel.value.result![index].cropDetails!.totaldays}",
                        style: TextStyles.kTSDS12W500.copyWith(color: colorHello),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        '${'outed_date'.tr} ${formatDate(controller.getPurchasedAvailableCropsModel.value.result![index].agriYields!.first.outedDate.toString())}',
                        style: TextStyles.kTSFS12W500.copyWith(color: colorHello),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ));
    }
    return items;
  }
}
