//STG Urls
const String devAuthUri = "http://3.110.104.209/api/";
const String devBaseUri = "http://3.110.104.209/";
const String devBearerToken =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKemRXSWlPaUp1WVhabFpXNTJhWGw1WVhCMU56Z3dRR2R0WVdsc0xtTnZiU0lzSW1saGRDSTZNVFk1T0RJek5EVXlNeXdpWlhod0lqb3hOams0TWpVeU5USXpmUS51RDJHVkxnZDhrSlFSdjFEV0U5aGZlVUktYlFLS1ZkMllUWWpOcGQzTU9VIiwiaWF0IjoxNjk4MjM0NTIzLCJleHAiOjE2OTgyNTI1MjN9._y9OxYBdsSSUoRW1PZeOgpH98btszOSH7mSH-vRUwcg";

//PROD Urls
const String prodAuthUri = "http://3.110.104.209/api/";
const String prodBaseUri = "http://3.110.104.209/";

const String prodBearerToken =
    "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJib3RAeWFudHJhbGl2ZS5jb20iLCJpc0FkbWluIjp0cnVlLCJleHAiOjE2NDMzNTAyNTQsImlhdCI6MTY0MzI2Mzg1NH0.Tsw0t5KPCglyYfiuGM9sDGTg6MFTNRkv3qI-Xmdj7hg";
