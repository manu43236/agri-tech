import 'dart:io';

import 'package:agritech/app/modules/dashboard/controllers/dashboard_controller.dart';
import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart' as dio;
import 'package:image_picker/image_picker.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/utils/storage_utils/secure_storage.dart';
import '../../../../models/add_job_by_farmer_model.dart';
import '../../../../models/add_job_model.dart';
import '../../../routes/app_pages.dart';
import '../../home/controllers/home_controller.dart';
import '../services/add_job_by_farmer_repo_impl.dart';
import '../services/add_job_repo_impl.dart';

class AddJobController extends BaseController {

  var nameJobController = TextEditingController().obs;
  var stateJobController = TextEditingController().obs;
  var districtJobController = TextEditingController().obs;
  var mandalJobController = TextEditingController().obs;
  var villageJobController = TextEditingController().obs;
  var descriptionJobController = TextEditingController().obs;
  var imageJobController = TextEditingController().obs;
  var startDateController = TextEditingController().obs;
  var endDateController = TextEditingController().obs;
  

  var landImage = Rx<File?>(null);
  RxString landPath = ''.obs;
  RxString landFile = ''.obs;
  var landValue = ''.obs;
  var userId = "".obs;
  var landId = "".obs;
  var role = "".obs;
  var selectedStateIds = 0.obs;
  var selectedDistrictIds = 0.obs;
  var selectedMandalIds = 0.obs;
  var selectedOption = ''.obs;

  var isFormSubmitted = false.obs;

  DateTime? selectedStartDate;
  var startDate = "".obs;
  var endDate = "".obs;

  @override
  void onInit() async{
    super.onInit();
    userId.value = await SecureStorage().readData(key: "id") ?? "";
    role.value=Get.find<HomeController>().role.value;
  }


  void setImagePath(String path) {
    imageJobController.value.text = path;
  }

  DateTime convertStringToDate({date}) {
    return DateTime.parse(date).toLocal();
  }

  Future<void> pickImage(ImageSource source) async {
    final XFile? image = await ImagePicker().pickImage(source: source);

    if (image != null) {
      landImage.value = File(image.path);
      landFile.value = image.name;
      landPath.value = image.path;
      setImagePath(image.path);
    } else {
      Get.snackbar('Error', 'No image selected');
    }
  }


  void removeImage() {
    landImage.value = null;
  }

  
//Add Job By Landlord API
  void addJobs() async {
    apiStatus.value = ApiStatus.LOADING;
    if (nameJobController.value.text.isNotEmpty &&
        startDateController.value.text.isNotEmpty &&
        endDateController.value.text.isNotEmpty &&
        stateJobController.value.text.isNotEmpty &&
        districtJobController.value.text.isNotEmpty &&
        mandalJobController.value.text.isNotEmpty &&
        villageJobController.value.text.isNotEmpty &&
        descriptionJobController.value.text.isNotEmpty &&
        imageJobController.value.text.isNotEmpty) {
      try {
        dio.FormData formData = dio.FormData.fromMap({
          "landLordId": userId.value,
          "fullName": nameJobController.value.text,
          "startDate": startDateController.value.text,
          "endDate": endDateController.value.text,
          "state": stateJobController.value.text,
          "district": districtJobController.value.text,
          "mandal": mandalJobController.value.text,
          "village": villageJobController.value.text,
          "longitude":Get.find<HomeController>().longitude.value,
          "latitude":Get.find<HomeController>().latitude.value,
          "description": descriptionJobController.value.text,
          "file": await dio.MultipartFile.fromFile(
            landPath.value,
            filename: landFile.value,
          ),
        });

        var result =
            await AddJobRepoImpl(dioClient).doAddJob(params1: formData);
        result.fold((left) {
          jobHandleResponse(left);
          apiStatus.value = ApiStatus.SUCCESS;
        }, (right) {
          apiStatus.value = ApiStatus.FAIL;
        });
      } catch (e) {
        print('Error: $e');
      }
    } else {
      print('One or more fields are empty.');
    }
  }

  void jobHandleResponse(AddJobModel model) async {
    if (model.statusCode == 200) {
      await Get.find<LandlordController>().addAlljobs(userId: userId.value);
      //Get.back();
      Get.find<DashboardController>().currentIndex.value = 0;
      Get.find<HomeController>().selectedIndex.value=0;
      Get.toNamed(Routes.DASHBOARD);
      // Get.find<DashboardController>().pageController.jumpToPage(0);
      nameJobController.value.clear();
      startDateController.value.clear();
      endDateController.value.clear();
      stateJobController.value.clear();
      districtJobController.value.clear();
      mandalJobController.value.clear();
      villageJobController.value.clear();
      descriptionJobController.value.clear();
      startDate.value='';
      endDate.value='';
      removeImage();
       Get.snackbar(
        'Success', 
        'Job added successfully',
        backgroundColor: colorGreen,
        colorText: colorBlack
      );
    }else if(model.statusCode==400){
      Get.snackbar(
        'Failed', 
        'A job with the same details already exists',
        backgroundColor: colorRed,
        colorText: colorWhite
      );
    }
  }

//Add Job by farmer API
  void addJobByFarmer() async {
    apiStatus.value = ApiStatus.LOADING;
    if (nameJobController.value.text.isNotEmpty &&
        startDateController.value.text.isNotEmpty &&
        endDateController.value.text.isNotEmpty &&
        stateJobController.value.text.isNotEmpty &&
        districtJobController.value.text.isNotEmpty &&
        mandalJobController.value.text.isNotEmpty &&
        villageJobController.value.text.isNotEmpty &&
        descriptionJobController.value.text.isNotEmpty &&
        imageJobController.value.text.isNotEmpty) {
      try {
        dio.FormData formData = dio.FormData.fromMap({
          "farmerId": userId.value,
          "fullName": nameJobController.value.text,
          "startDate": startDateController.value.text,
          "endDate": endDateController.value.text,
          "state": stateJobController.value.text,
          "district": districtJobController.value.text,
          "mandal": mandalJobController.value.text,
          "village": villageJobController.value.text,
          "longitude":Get.find<HomeController>().longitude.value,
          "latitude":Get.find<HomeController>().latitude.value,
          "description": descriptionJobController.value.text,
          "file": await dio.MultipartFile.fromFile(
            landPath.value,
            filename: landFile.value,
          ),
        });

        var result = await AddJobByFarmerRepoImpl(dioClient)
            .doAddFarmerJob(params1: formData);
        result.fold((left) {
          print(left);
          jobFarmerHandleResponse(left);
          apiStatus.value = ApiStatus.SUCCESS;
        }, (right) {
          apiStatus.value = ApiStatus.FAIL;
        });
      } catch (e) {
        print('Error: $e');
      }
    } else {
      print('One or more fields are empty.');
    }
  }

  void jobFarmerHandleResponse(AddJobByFarmerModel model) async {
  print('Handling response with statusCode: ${model.statusCode}');
  if (model.statusCode == 200) {
    print('Successfully added farmer job.');
    Get.find<DashboardController>().currentIndex.value = 0;
    Get.find<HomeController>().selectedIndex.value=0;
    await Get.find<LandlordController>().addAllFarmerjobs(userId: userId.value);
    Get.toNamed(Routes.DASHBOARD);
    nameJobController.value.clear();
      startDateController.value.clear();
      endDateController.value.clear();
      stateJobController.value.clear();
      districtJobController.value.clear();
      mandalJobController.value.clear();
      villageJobController.value.clear();
      descriptionJobController.value.clear();
      startDate.value='';
      endDate.value='';
      removeImage();
    Get.snackbar(
      'Success', 
      'Job added successfully',
      backgroundColor: colorGreen,
      colorText: colorBlack
    );
  } else if (model.statusCode == 400) {
    print('Failed to add job, statusCode: 400');
    Get.snackbar(
      'Failed', 
      'A job with the same details already exists',
      backgroundColor: colorRed,
      colorText: colorWhite
    );
  } else {
    print('Unexpected statusCode: ${model.statusCode}');
  }
}



  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
