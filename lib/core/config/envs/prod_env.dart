


import '../../conts/api_const.dart';
import '../env.dart';

class ProdEnv extends EnvVars {
  @override
  String get apiToken => '';

  @override
  String get baseURL => prodBaseUri;

  @override
  String get title => 'AGRITECH';

  @override
  String get bearerToken =>
      prodBearerToken;

  @override
  String get authBaseURL => prodAuthUri;
 
}
