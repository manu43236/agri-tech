import 'package:agritech/app/modules/select_region/services/select_region_repo.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';


import '../data/district_model.dart';
import '../data/mandal_model.dart';
import '../data/state_model.dart';
import '../data/village_model.dart';

class SelectRegionRepoImpl extends SelectRegionRepo with NetworkCheckService{
  final DioClient _dioClient;
  SelectRegionRepoImpl(this._dioClient);

  @override
  Future<Either<StateModel,Exception>> getState({lang})async{
    var data=await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result=await _dioClient.requestForAuth('State/getAllStates?language=$lang', Method.get);
        return  result.fold((l) {
          StateModel model = StateModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      }catch(e){
         return Right(Exception(e));
      }
    }
  }

   @override
  Future<Either<DistrictModel,Exception>> getDistrict({id,lang})async{
    var data=await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('District/getDistrictsByStateId?state_id=$id&language=$lang',Method.get);
        return result.fold((l){
          DistrictModel model=DistrictModel.fromJson(l.data);
          return left(model);
        },(r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<MandalModel,Exception>> getMandal({id,lang})async{
    var data=await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Mandal/getMandalsByDistrictId?district_id=$id&language=$lang',Method.get,);
        return result.fold((l){
          MandalModel model=MandalModel.fromJson(l.data);
          return left(model);
        },(r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

  @override
  Future<Either<VillageModel,Exception>> getVillage({id,lang})async{
    var data=await checkInternet();
    if(!data){
      return Right(Exception("No Network found"));
    }else{
      try{
        var result= await _dioClient.requestForAuth('Village/getVillagesByMandalId?mandal_id=$id&language=$lang',Method.get);
        return result.fold((l){
          VillageModel model=VillageModel.fromJson(l.data);
          return left(model);
        },(r) => Right(Exception(r)));
      }catch(e){
        return Right(Exception(e));
      }
    }
  }

}