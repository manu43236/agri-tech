import 'package:agritech/app/modules/home/controllers/landlord_controller.dart';
import 'package:agritech/core/conts/img_const.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/base/base_controller.dart';
import '../../../../../core/conts/color_consts.dart';
import '../../../../../core/themes/text_styles.dart';
import '../../../../../core/utils/widget_utils/shimmers.dart';
import '../../../../routes/app_pages.dart';
import '../../controllers/home_controller.dart';

class MyLandsWidget extends GetView<LandlordController> {
  const MyLandsWidget({super.key});

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Obx(
        () => Column(
          children: [
            Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'my_lands'.tr,
                  style: TextStyles.kTSFS12W500.copyWith(color: colorHello),
                )),
            const SizedBox(
              height: 5,
            ),
            controller.apiStatus.value == ApiStatus.LOADING
                ? Shimmers().getShimmerItem()
                : 
                controller.addLandsOfLandlordModel.value.result != null &&
                        controller.addLandsOfLandlordModel.value.result!
                            .ongoingLands!.isNotEmpty
                    ? Column(
                        children: [
                          SizedBox(
                            height: Get.height * 0.14,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: controller.addLandsOfLandlordModel
                                    .value.result!.ongoingLands!.length,
                                itemBuilder: (context, index) {
                                  final land = controller
                                      .addLandsOfLandlordModel
                                      .value
                                      .result!
                                      .ongoingLands![index];
                                  final imageUrl =
                                      land.imageUrl ?? defaultImageUrl;
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: InkWell(
                                      onTap: () async {
                                        Get.find<HomeController>().landId.value = controller
                                            .addLandsOfLandlordModel
                                            .value
                                            .result!
                                            .ongoingLands![index]
                                            .id
                                            .toString();
                                        Get.toNamed(Routes.DETAIL_MY_LANDS,arguments: {"landId":controller
                                            .addLandsOfLandlordModel
                                            .value
                                            .result!
                                            .ongoingLands![index]
                                            .id
                                            .toString(),"isLoan":false,"isInsurance":false});
                                      },
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(12),
                                        child: Image.network(
                                          imageUrl ?? '',
                                          width: Get.width * 0.42,
                                          fit: BoxFit.cover,
                                          errorBuilder:
                                              (context, error, stackTrace) {
                                            // Display a local asset if the network image fails
                                            return Image.asset(
                                              defaultImageUrl,
                                              width: Get.width * 0.42,
                                              fit: BoxFit.cover,
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                          ),
                          const SizedBox(height: 5),
                          Align(
                            alignment: Alignment.topRight,
                            child: InkWell(
                              onTap: () async{
                                Get.toNamed(Routes.AVAILABLE_MY_LANDS);
                              },
                              child: Text(
                                'view_all'.tr,
                                style: TextStyles.kTSCF12W500.copyWith(
                                  fontSize: 10,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    : Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height:Get.height*0.03,),
                        InkWell(
                            onTap: () {
                              Get.toNamed(Routes.ADD_LAND);
                            },
                            child: Text(
                              'no_lands_add_it_here'.tr,
                              style: TextStyles.kTSFS18W500.copyWith(
                                  color: colorBlue,
                                  decoration: TextDecoration.underline),
                            )),
                         SizedBox(height:Get.height*0.03,),
                      ],
                    ),
          ],
        ),
      ),
    );
  }
}
