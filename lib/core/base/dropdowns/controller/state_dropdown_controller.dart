import 'package:agritech/app/modules/home/services/state_repo_impl.dart';
import 'package:agritech/core/base/base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../app/modules/select_region/data/state_model.dart';
import '../../../utils/storage_utils/secure_storage.dart';

class StateDropdownController extends BaseController {


 // var model = StatelistModel().obs;
  var selectMake = 'Select State'.obs;
  var selectedLang=''.obs;
  var makes = <String>[].obs;
  Rx<List<StateResult>> stateResult = Rx([]);

  TextEditingController stateController = TextEditingController();
  @override
  void onInit() async{
    selectedLang.value = await SecureStorage().readData(key: "lang");
    getAllState(lang: selectedLang.value);
    super.onInit();
  }


  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  getAllState({lang}) async {
    apiStatus.value = ApiStatus.LOADING;
    var result = await StateRepoImpl(dioClient).getState(lang: lang);
    print("result");
    print(result);
    result.fold((l) {
      if (l.result!.isNotEmpty) {
        var items = <String>[];
        stateResult.value = l.result!;
        for (var element in l.result!) {
          items.add(element.name.toString());
        }
        makes.addAll(items);
        
        print(makes);
      }
      apiStatus.value = ApiStatus.SUCCESS;
    }, (r) {
     
      apiStatus.value = ApiStatus.FAIL;
    });
  }
}