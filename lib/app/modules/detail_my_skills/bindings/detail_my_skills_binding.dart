import 'package:get/get.dart';

import '../controllers/detail_my_skills_controller.dart';

class DetailMySkillsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailMySkillsController>(
      () => DetailMySkillsController(),
    );
  }
}
