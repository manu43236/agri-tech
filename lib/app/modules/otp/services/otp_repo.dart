
import 'package:agritech/models/otp_model.dart';
import 'package:dartz/dartz.dart';

abstract class OTPRepo{
   Future<Either<OTPModel,Exception>> doOTP({params1});
}

