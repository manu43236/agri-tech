import 'package:agritech/app/modules/detail_saved_farmworkers/views/job_dropdown.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../../details_of_farmworker/views/jobs_dropdown.dart';
import '../../home/controllers/home_controller.dart';
import '../controllers/detail_saved_farmworkers_controller.dart';

class DetailSavedFarmworkersView
    extends GetView<DetailSavedFarmworkersController> {
  const DetailSavedFarmworkersView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Details of Farmworker',
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: Obx(
          () =>controller.apiUserByProfileIdStatus.value == ApiStatus.LOADING
              ? Shimmers().getListShimmer()
              : SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipOval(
                        child: Image.network(
                          controller.getUserProfileModel.value.result!.imageUrl
                                  .toString() ??
                              defaultImageUrl,
                          height: Get.height * 0.4,
                          width: Get.height * 0.4,
                          fit: BoxFit.cover,
                          errorBuilder: (context, error, stackTrace) {
                            return Image.asset(
                              defaultImageUrl,
                              height: Get.height * 0.4,
                              width: Get.height * 0.4,
                              fit: BoxFit.cover,
                            );
                          },
                        ),
                      ),
                const SizedBox(
                  height: 20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          'Name : ',
                          style: TextStyles.kTSDS12W700
                              .copyWith(color: colorHello),
                        ),
                        Text(
                          controller
                                  .getUserProfileModel
                                  .value
                                  .result
                                  ?.firstName ??
                              'N/A',
                          style:
                              TextStyles.kTSDS14W700.copyWith(color: colorPic),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Text(
                          'Location : ',
                          style: TextStyles.kTSDS12W700
                              .copyWith(color: colorHello),
                        ),
                        Text(
                          controller
                                  .getUserProfileModel
                                  .value
                                  .result
                                  ?.address ??
                              'No address provided',
                          style:
                              TextStyles.kTSDS14W700.copyWith(color: colorPic),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Expanded(child: JobDropdown()),
                          const SizedBox(width: 50),
                          controller.statusForLandlord.value == ""
                              ? InkWell(
                                  onTap: () async {
                                    if (controller
                                        .selectedJobId.value.isEmpty) {
                                      Get.snackbar('msg'.tr,
                                          'please_select_job_first'.tr,
                                          backgroundColor: colorGreen,
                                          colorText: colorBlack);
                                    } else {
                                      await controller
                                          .sentNotificationByLandlordToWorker(
                                            senderId: controller.userId.value,
                                            workerId: controller.workerId.value,
                                            senderRole: controller.role.value,
                                            jobId: controller.selectedJobId.value
                                          );
                                      await controller.fetchedJobDetails(
                                          senderId: controller.userId.value,
                                          workerId: controller.workerId.value,
                                          senderRole: controller.role.value);
                                    }
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(7),
                                      color: colorPic,
                                    ),
                                    child: Text(
                                      'hire'.tr,
                                      style: TextStyles.kTSNFS16W600
                                          .copyWith(color: colorWhite),
                                    ),
                                  ),
                                )
                              : Text(
                                  controller.statusForLandlord.value,
                                  style: TextStyles.kTSFS18W600.copyWith(
                                      color:
                                          controller.statusForLandlord.value ==
                                                  'pending'
                                              ? const Color.fromARGB(
                                                  255, 227, 154, 44)
                                              : primary),
                                )
                        ],
                      ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   children: [
                //     const Expanded(child: JobsDropdown()),
                //     const SizedBox(width: 10),
                //     controller.apiUserByProfileIdStatus.value == ApiStatus.LOADING
                //         ? Shimmers().getShimmerForText()
                //         : Get.find<DetailsOfFarmworkerController>()
                //                     .statusForLandlord
                //                     .value ==
                //                 ""
                //             ? InkWell(
                //                 onTap: () {
                //                   if (Get.find<HomeController>().role.value ==
                //                       'Landlord') {
                //                     if (Get.find<
                //                             DetailsOfFarmworkerController>()
                //                         .selectedJobId
                //                         .value
                //                         .isEmpty) {
                //                       Get.snackbar(
                //                           'message', 'please select job first',
                //                           backgroundColor: colorGreen,
                //                           colorText: colorBlack);
                //                       //controller.selectedJobId.value = '';
                //                     }
                //                     Get.find<DetailsOfFarmworkerController>()
                //                         .sentNotificationByLandlordToWorker();
                //                     Get.find<DetailsOfFarmworkerController>()
                //                         .fetchedJobDetails();
                //                     //controller.selectedJobId.value = '';
                //                   }
                //                 },
                //                 child: Container(
                //                   padding: const EdgeInsets.symmetric(
                //                       horizontal: 20, vertical: 3),
                //                   decoration: BoxDecoration(
                //                     borderRadius: BorderRadius.circular(7),
                //                     color: colorPic,
                //                   ),
                //                   child: Text(
                //                     'Hire',
                //                     style: TextStyles.kTSNFS16W600
                //                         .copyWith(color: colorWhite),
                //                   ),
                //                 ),
                //               )
                //             : Text(
                //                 Get.find<DetailsOfFarmworkerController>()
                //                     .statusForLandlord
                //                     .value,
                //                 style: TextStyles.kTSFS18W600.copyWith(
                //                     color:
                //                         Get.find<DetailsOfFarmworkerController>()
                //                                     .statusForLandlord
                //                                     .value ==
                //                                 'pending'
                //                             ? const Color.fromARGB(255, 227, 154, 44)
                //                             : primary),
                //               )
                //   ],
                // ),
                const SizedBox(height: 20),
                Text(
                  'Description About him',
                  style: TextStyles.kTSDS12W700.copyWith(color: colorDetails),
                ),
                const SizedBox(height: 10),
                Text(
                  controller
                          .getUserProfileModel
                          .value
                          .result
                          ?.description ??
                      '',
                  style: TextStyles.kTSCF12W500,
                ),
                const SizedBox(height: 20),
                const SizedBox(height: 20),
                Text(
                  'Saved farmworkers',
                  style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
                ),
                const SizedBox(height: 10),
                SizedBox(
                  height: Get.height * 0.08,
                  child: controller
                                  .getAllSavedFarmworkerModel
                                  .value
                                  .result !=
                              null &&
                          controller
                              .getAllSavedFarmworkerModel
                              .value
                              .result!
                              .savedFarmers!
                              .isNotEmpty
                      ? ListView.separated(
                          itemCount: controller
                              .getAllSavedFarmworkerModel
                              .value
                              .result!
                              .savedFarmers!
                              .length,
                          scrollDirection: Axis.horizontal,
                          separatorBuilder: (context, index) =>
                              const SizedBox(width: 10),
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () async {
                                controller.selectedJob.value=null;
                                controller.statusForLandlord.value = "";
                                controller.workerId.value =
                                    controller
                                        .getAllSavedFarmworkerModel
                                        .value
                                        .result!
                                        .savedFarmers![index]
                                        .farmworkerid!;
                                await controller
                                    .getUserByProfileId(id: controller.workerId.value);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: colorGrey,
                                  borderRadius: BorderRadius.circular(11),
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(11),
                                  child: Image.network(
                                    controller
                                        .getAllSavedFarmworkerModel
                                        .value
                                        .result!
                                        .savedFarmers![index]
                                        .imageUrl
                                        .toString(),
                                    height: Get.height * 0.09,
                                    width: Get.width * 0.2,
                                    fit: BoxFit.cover,
                                    errorBuilder: (context, error, stackTrace) {
                                      return Image.asset(
                                        defaultImageUrl,
                                        height: Get.height * 0.09,
                                        width: Get.width * 0.2,
                                        fit: BoxFit.cover,
                                      );
                                    },
                                  ),
                                ),
                              ),
                            );
                          },
                        )
                      : const Text('No saved farmworkers found'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
