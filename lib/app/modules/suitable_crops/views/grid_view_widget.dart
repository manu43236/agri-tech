import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/modules/suitable_crops/controllers/suitable_crops_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/conts/color_consts.dart';
import '../../../../core/themes/text_styles.dart';

class GridViewWidget extends GetView<SuitableCropsController> {
  const GridViewWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return GridView.builder(
        physics: BouncingScrollPhysics(),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3, // 3 items per row
          crossAxisSpacing: 10, // Space between grid items
          mainAxisSpacing: 10,
        ),
        itemCount: controller.getUserSelectedCropModel.value.result!.groupedCrops!.length,
        itemBuilder: (context, index) {
          if (controller.getUserSelectedCropModel.value.result!.groupedCrops![index].crops!.isNotEmpty) {
            bool hasEnabledWeek = false;
            for (var crop in controller.getUserSelectedCropModel.value.result!.groupedCrops![index].crops!) {
              if (crop.weekenable == true) {
                hasEnabledWeek = true;
                break;
              }
            }
            if (hasEnabledWeek) {
              return GridTile(
                child: InkWell(
                  onTap: () {
                    Get.bottomSheet(
                      SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                Get.back();
                              },
                              child: Container(
                                  padding: const EdgeInsets.all(5),
                                  decoration: const BoxDecoration(
                                      color: colorGrey, shape: BoxShape.circle),
                                  child: const Icon(
                                    Icons.close,
                                    size: 25,
                                  )),
                            ),
                            Container(
                              decoration: const BoxDecoration(
                                color: colorWhite,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(21),
                                    topRight: Radius.circular(21)),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      height: 35,
                                      width: Get.width,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(6),
                                        color: colorWhite,
                                        border: Border.all(color: colorSuitableCrop),
                                      ),
                                      child: Center(
                                          child: Text(
                                        '${'week'.tr} ${index + 1}',
                                        style: TextStyles.kTSFS12W400,
                                      )),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: colorWhite,
                                        borderRadius: BorderRadius.circular(6),
                                        border: Border.all(color: colorSuitableCrop),
                                      ),
                                      child: ListView.builder(
                                        itemCount: controller
                                            .getUserSelectedCropModel
                                            .value
                                            .result!.groupedCrops![index]
                                            .crops!
                                            .length,
                                        shrinkWrap: true,
                                        physics: const BouncingScrollPhysics(),
                                        itemBuilder: (context, cropIndex) {
                                          return Row(
                                            children: [
                                              Obx(
                                                () => Checkbox(
                                                  value: controller
                                                      .getUserSelectedCropModel
                                                      .value
                                                      .result!.groupedCrops![index]
                                                      .crops![cropIndex]
                                                      .stepsflag,
                                                  checkColor: colorBlack,
                                                  onChanged:
                                                      (bool? newValue) async {
                                                    if (newValue != null &&
                                                        newValue == true) {
                                                      // if (cropIndex > 0) {
                                                      //   if (controller
                                                      //           .getUserSelectedCropModel
                                                      //           .value
                                                      //           .result!.groupedCrops![index]
                                                      //           .crops![cropIndex]
                                                      //           .stepsflag ==
                                                      //       false) {
                                                      //     Get.snackbar(
                                                      //       'Failed',
                                                      //       'Crop cannot be updated as the created date is greater than today.',
                                                      //       backgroundColor: colorRed,
                                                      //       colorText: colorWhite,
                                                      //     );
                                                      //     return;
                                                      //   }
                                                      // }
                                                      controller.dateCreated.value=controller
                                                                .getUserSelectedCropModel
                                                                .value
                                                                .result!.groupedCrops![index]
                                                                .crops![cropIndex]
                                                                .createdDate.toString();
                                                      final parsedDate = DateTime.parse(controller.dateCreated.value);
                                                      if(parsedDate.isBefore(DateTime.now()) || parsedDate.isAtSameMomentAs(DateTime.now())){
                                                                  controller.descriptionId.value =
                                                          controller
                                                              .getUserSelectedCropModel
                                                              .value
                                                              .result!.groupedCrops![index]
                                                              .crops![cropIndex]
                                                              .id!;
                              
                                                      await controller.updateUserSelectedCrop(true);
                              
                                                      await controller.getUserSelectedCrop(
                                                       landId: Get.find<HomeController>().landId.value,
                                                       cropId: controller.cropIdss.value.toString(),
                                                       lang: controller.selectedLang.value
                                                      );
                              
                                                      controller
                                                          .getUserSelectedCropModel
                                                          .value
                                                          .result!.groupedCrops![index]
                                                          .crops![cropIndex]
                                                          .stepsflag = true;
                              
                                                      controller.update();
                                                      }
                                                      
                                                    }
                                                  },
                                                ),
                                              ),
                                              Flexible(
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      controller
                                                          .getUserSelectedCropModel
                                                          .value
                                                          .result!.groupedCrops![index]
                                                          .crops![cropIndex]
                                                          .description!,
                                                    ),
                                                    if (controller
                                                            .getUserSelectedCropModel
                                                            .value
                                                            .result!.groupedCrops![index]
                                                            .crops![cropIndex]
                                                            .stepsflag ==
                                                        false)
                                                      Text(
                                                        controller
                                                                .getUserSelectedCropModel
                                                                .value
                                                                .result!.groupedCrops![index]
                                                                .crops![cropIndex]
                                                                .alert ??
                                                            '',
                                                        style: const TextStyle(
                                                            color: colorRed), // Optional styling
                                                      ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          );
                                        },
                                      ),
                                    ),
                                    const SizedBox(height: 10),
                                    InkWell(
                                      onTap: () {
                                        Get.back(); // Close the bottom sheet
                                      },
                                      child: Container(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 15, vertical: 5),
                                        decoration: BoxDecoration(
                                          color: colorSuitableCrop,
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        child: Text(
                                          'done'.tr,
                                          style: TextStyles.kTSDS14W500.copyWith(color: colorWhite),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  child: Card(
                    color: colorGrey,
                    child: SizedBox(
                      width: 100, // Limit the size of the grid item
                      child: Center(
                        child:
                        // Text('${'week'.tr} ${controller.getUserSelectedCropModel.value.result!.groupedCrops![index].crops![index].weeks}'),
                         Text('${'week'.tr} ${controller.getUserSelectedCropModel.value.result!.groupedCrops![index].week}'),
                      ),
                    ),
                  ),
                ),
              );
            } else {
              return GridTile(
                child: Card(
                  color: colorGrey.shade200, // Lighter color for other indices
                  child: SizedBox(
                    width: 100, // Limit the size of the grid item
                    child: Center(
                      child: Text(
                        '${'week'.tr} ${controller.getUserSelectedCropModel.value.result!.groupedCrops![index].week}',
                        style: TextStyle(color: colorGrey.shade600), // Light text color
                      ),
                    ),
                  ),
                ),
              );
            }
          } else {
            return const SizedBox(); // Return an empty widget if no crops are available
          }
        },
      );
    });
  }
 }

 

// import 'package:agritech/app/modules/home/controllers/home_controller.dart';
// import 'package:agritech/app/modules/suitable_crops/controllers/suitable_crops_controller.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// import '../../../../core/conts/color_consts.dart';
// import '../../../../core/themes/text_styles.dart';

// class GridViewWidget extends GetView<SuitableCropsController> {
//   const GridViewWidget({super.key});
//   @override
//   Widget build(BuildContext) {
//     return GridView.builder(
//         gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
//           crossAxisCount: 3, // 3 items per row
//           crossAxisSpacing: 10, // Space between grid items
//           mainAxisSpacing: 10,
//         ),
//         itemCount: controller.getUserSelectedCropModel.value.result!.length,
//         itemBuilder: (context, index) {
//           if (controller.getUserSelectedCropModel.value.result![index]
//                   .crops![index].weekenable ==
//               true) {
//             return GridTile(
//               child: InkWell(
//                 onTap: () {
//                   Get.bottomSheet(
//                     SingleChildScrollView(
//                       physics: const BouncingScrollPhysics(),
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.center,
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           InkWell(
//                             onTap: () {
//                               Get.back();
//                             },
//                             child: Container(
//                                 padding: const EdgeInsets.all(5),
//                                 decoration: const BoxDecoration(
//                                     color: colorGrey, shape: BoxShape.circle),
//                                 child: const Icon(
//                                   Icons.close,
//                                   size: 25,
//                                 )),
//                           ),
//                           Container(
//                             decoration: const BoxDecoration(
//                               color: colorWhite,
//                               borderRadius: BorderRadius.only(
//                                   topLeft: Radius.circular(21),
//                                   topRight: Radius.circular(21)),
//                             ),
//                             child: Padding(
//                               padding: const EdgeInsets.symmetric(
//                                   horizontal: 20, vertical: 20),
//                               child: Column(
//                                 crossAxisAlignment: CrossAxisAlignment.center,
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   Container(
//                                     height: 35,
//                                     width: Get.width,
//                                     decoration: BoxDecoration(
//                                       borderRadius: BorderRadius.circular(6),
//                                       color: colorWhite,
//                                       border:
//                                           Border.all(color: colorSuitableCrop),
//                                     ),
//                                     child: Center(
//                                         child: Text(
//                                       'Week ${index + 1}',
//                                       style: TextStyles.kTSFS12W400,
//                                     )),
//                                   ),
//                                   const SizedBox(
//                                     height: 10,
//                                   ),
//                                   Container(
//                                     decoration: BoxDecoration(
//                                       color: colorWhite,
//                                       borderRadius: BorderRadius.circular(6),
//                                       border:
//                                           Border.all(color: colorSuitableCrop),
//                                     ),
//                                     child: ListView.builder(
//                                       itemCount: controller
//                                           .getUserSelectedCropModel
//                                           .value
//                                           .result![index]
//                                           .crops!
//                                           .length,
//                                       shrinkWrap: true,
//                                       physics: const BouncingScrollPhysics(),
//                                       itemBuilder: (context, cropIndex) {
//                                         return Row(
//                                           children: [
//                                             Obx(
//                                               () => Checkbox(
//                                                 value: controller
//                                                     .getUserSelectedCropModel
//                                                     .value
//                                                     .result![index]
//                                                     .crops![cropIndex]
//                                                     .stepsflag,
//                                                 checkColor: colorBlack,
//                                                 onChanged:
//                                                     (bool? newValue) async {
//                                                   if (newValue != null &&
//                                                       newValue == true) {
//                                                     // Check if the cropIndex is greater than or equal to 0
//                                                     if (cropIndex > 0) {
//                                                       // Check the current crop's stepsflag
//                                                       if (controller
//                                                               .getUserSelectedCropModel
//                                                               .value
//                                                               .result![index]
//                                                               .crops![cropIndex]
//                                                               .stepsflag ==
//                                                           false) {
//                                                         // Show snackbar message
//                                                         // controller.errorMessage.value=true;
//                                                         // print('error: ${controller.errorMessage.value}');
//                                                         Get.snackbar(
//                                                           'Failed',
//                                                           'Crop cannot be updated as the created date is greater than today.',
//                                                           backgroundColor:
//                                                               colorRed,
//                                                           colorText: colorWhite,
//                                                         );
//                                                         // Do not update the checkbox or proceed further
//                                                         return;
//                                                       }
//                                                     }
//                                                     // Continue with the selection logic if stepsflag is valid
//                                                     controller.descriptionId
//                                                             .value =
//                                                         controller
//                                                             .getUserSelectedCropModel
//                                                             .value
//                                                             .result![index]
//                                                             .crops![cropIndex]
//                                                             .id!;

//                                                     // Call the API to update the selected crop
//                                                     await controller
//                                                         .updateUserSelectedCrop(
//                                                             true);

//                                                     // Fetch the updated crop data
//                                                     await controller
//                                                         .getUserSelectedCrop(
//                                                       Get.find<HomeController>().landId.value,
//                                                       controller.cropId.value,
//                                                     );

//                                                     // Update the stepsflag to true after the successful API call
//                                                     controller
//                                                         .getUserSelectedCropModel
//                                                         .value
//                                                         .result![index]
//                                                         .crops![cropIndex]
//                                                         .stepsflag = true;

//                                                     // Notify the UI to update
//                                                     controller.update();
//                                                   }
//                                                 },
//                                               ),
//                                             ),
//                                             Flexible(
//                                               child: Column(
//                                                 mainAxisAlignment:
//                                                     MainAxisAlignment.start,
//                                                 crossAxisAlignment:
//                                                     CrossAxisAlignment.start,
//                                                 children: [
//                                                   Text(
//                                                     controller
//                                                         .getUserSelectedCropModel
//                                                         .value
//                                                         .result![index]
//                                                         .crops![cropIndex]
//                                                         .description!,
//                                                   ),
//                                                   if (controller
//                                                           .getUserSelectedCropModel
//                                                           .value
//                                                           .result![index]
//                                                           .crops![cropIndex]
//                                                           .stepsflag ==
//                                                       false)
//                                                     Text(
//                                                       controller
//                                                               .getUserSelectedCropModel
//                                                               .value
//                                                               .result![index]
//                                                               .crops![cropIndex]
//                                                               .alert ??
//                                                           '',
//                                                       style: const TextStyle(
//                                                           color:
//                                                               colorRed), // Optional styling
//                                                     ),
//                                                 ],
//                                               ),
//                                             )
//                                           ],
//                                         );
//                                       },
//                                     ),
//                                   ),
//                                   const SizedBox(height: 10),
//                                   InkWell(
//                                     onTap: () {
//                                       Get.back(); // Close the bottom sheet
//                                     },
//                                     child: Container(
//                                       padding: const EdgeInsets.symmetric(
//                                           horizontal: 15, vertical: 5),
//                                       decoration: BoxDecoration(
//                                         color: colorSuitableCrop,
//                                         borderRadius: BorderRadius.circular(10),
//                                       ),
//                                       child: Text(
//                                         'Done',
//                                         style: TextStyles.kTSDS14W500
//                                             .copyWith(color: colorWhite),
//                                       ),
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                   );
//                 },
//                 child: Card(
//                   color: colorGrey,
//                   child: SizedBox(
//                     width: 100, // Limit the size of the grid item
//                     child: Center(
//                       child: Text('Week ${index + 1}'),
//                     ),
//                   ),
//                 ),
//               ),
//             );
//           } else {
//             return GridTile(
//               child: Card(
//                 color: colorGrey.shade200, // Lighter color for other indices
//                 child: SizedBox(
//                   width: 100, // Limit the size of the grid item
//                   child: Center(
//                     child: Text(
//                       'Week ${index + 1}',
//                       style: TextStyle(
//                           color: colorGrey.shade600), // Light text color
//                     ),
//                   ),
//                 ),
//               ),
//             );
//           }
//         });
//   }
// }
