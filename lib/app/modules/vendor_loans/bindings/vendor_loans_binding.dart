import 'package:get/get.dart';

import '../controllers/vendor_loans_controller.dart';

class VendorLoansBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VendorLoansController>(
      () => VendorLoansController(),
    );
  }
}
