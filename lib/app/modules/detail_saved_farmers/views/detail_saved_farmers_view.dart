import 'package:agritech/app/routes/app_pages.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../controllers/detail_saved_farmers_controller.dart';
import 'land_dropdown.dart';

class DetailSavedFarmersView extends GetView<DetailSavedFarmersController> {
  const DetailSavedFarmersView({Key? key}) : super(key: key);

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Details of Farmer',
          style: TextStyles.kTSFS20W500.copyWith(color: colorHello),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: colorBlack,
          ),
          onPressed: () async {
            Get.back();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: Obx(
          () => SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: controller
                              .getUserProfileModel
                              .value
                              .result
                              ?.imageUrl !=
                          null
                      ? ClipRRect(
                          borderRadius: BorderRadius.circular(11),
                          child: Image.network(
                            controller
                                .getUserProfileModel
                                .value
                                .result!
                                .imageUrl
                                .toString(),
                            fit: BoxFit.cover,
                            height: Get.height * 0.35,
                            width: Get.width,
                          ),
                        )
                      : const Icon(Icons.person, size: 40, color: Colors.grey),
                ),
                const SizedBox(height: 20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          'Name : ',
                          style: TextStyles.kTSDS12W700
                              .copyWith(color: colorHello),
                        ),
                        Text(
                          controller
                                  .getUserProfileModel
                                  .value
                                  .result
                                  ?.firstName ??
                              'N/A',
                          style:
                              TextStyles.kTSDS14W700.copyWith(color: colorPic),
                        ),
                      ],
                    ),
                    const SizedBox(height: 5),
                    Row(
                      children: [
                        Text(
                          'Location : ',
                          style: TextStyles.kTSDS12W700
                              .copyWith(color: colorHello),
                        ),
                        Text(
                          controller
                                  .getUserProfileModel
                                  .value
                                  .result
                                  ?.address ??
                              'No address provided',
                          style:
                              TextStyles.kTSDS14W700.copyWith(color: colorPic),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Expanded(child: LandDropdown()),
                      const SizedBox(
                          width:
                              50), // Spacing between Dropdown and Hire button
                      controller.apiStatus.value == ApiStatus.LOADING
                          ? Shimmers().getShimmerForText()
                          : controller.statusForLandlord.value == ""
                              ? InkWell(
                                  onTap: () async{
                                    if (controller.selectedLand.value == null) {
                                      Get.snackbar(
                                          'message', 'please select land first',
                                          backgroundColor: colorGreen,
                                          colorText: colorBlack);
                                    } else {
                                      controller.sentNotificationByLandlordId(
                                        landlordId: controller.id.value,
                                        farmerId: controller.farmerId.value,
                                        landId: controller.selectedLandId.value
                                      );
                                      await controller.availableLandsList(id(controller.id.value));
                                      await controller.FetchedLandsDetails(userId: controller.id.value,farmerId: controller.farmerId.value);
                                    }
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 11),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(7),
                                      color: colorPic,
                                    ),
                                    child: Text(
                                      'Hire',
                                      style: TextStyles.kTSNFS16W600
                                          .copyWith(color: colorWhite),
                                    ),
                                  ),
                                )
                              : Text(
                                  controller.statusForLandlord.value,
                                  style: TextStyles.kTSFS18W600.copyWith(
                                      color: controller
                                                  .statusForLandlord.value ==
                                              'pending'
                                          ? const Color.fromARGB(255, 227, 154, 44)
                                          : primary),
                                )
                    ],
                  ),
                const SizedBox(height: 20),
                Text(
                  'Description About him',
                  style: TextStyles.kTSDS12W700.copyWith(color: colorDetails),
                ),
                const SizedBox(height: 10),
                Text(
                  controller
                          .getUserProfileModel
                          .value
                          .result
                          ?.description
                          .toString() ??
                      '',
                  style: TextStyles.kTSCF12W500,
                ),
                const SizedBox(height: 20),
                Text(
  'Saved farmers',
  style: TextStyles.kTSFS16W600.copyWith(color: colorPic),
),
SizedBox(
  height: Get.height * 0.08,
  child: controller.getAllSavedFarmerModel.value.result != null &&
          controller.getAllSavedFarmerModel.value.result!.savedFarmers!.isNotEmpty
      ? ListView.separated(
          itemCount: controller.getAllSavedFarmerModel.value.result!.savedFarmers!
              .where((farmer) => farmer.farmerid != controller.farmerId.value)
              .length,
          scrollDirection: Axis.horizontal,
          separatorBuilder: (context, index) => const SizedBox(width: 10),
          itemBuilder: (context, index) {
            // Filter the list to exclude the selected farmer
            final filteredFarmers = controller.getAllSavedFarmerModel.value.result!
                .savedFarmers!
                .where((farmer) => farmer.farmerid != controller.farmerId.value)
                .toList();
            final farmer = filteredFarmers[index];

            return InkWell(
              onTap: () async {
                controller.selectedLand.value = null;
                controller.statusForLandlord.value ='';;
                await controller.getUserByProfileId(id: farmer.farmerid!);
              },
              child: Container(
                decoration: BoxDecoration(
                  color: colorGrey,
                  borderRadius: BorderRadius.circular(11),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(11),
                  child: Image.network(
                    farmer.imageUrl.toString(),
                    height: Get.height * 0.09,
                    width: Get.width * 0.2,
                    fit: BoxFit.cover,
                    errorBuilder: (context, error, stackTrace) {
                      return Image.asset(
                        defaultImageUrl,
                        height: Get.height * 0.09,
                        width: Get.width * 0.2,
                        fit: BoxFit.cover,
                      );
                    },
                  ),
                ),
              ),
            );
          },
        )
      : const Text('No saved farmers found'),
),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
