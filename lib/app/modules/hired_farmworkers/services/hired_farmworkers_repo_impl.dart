import 'package:agritech/app/modules/hired_farmworkers/services/hired_farmworkers_repo.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/core/base/dio_client.dart';
import 'package:agritech/core/network/network_check_service.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../data/hired_farmers_farmworkers_model.dart';

class HiredFarmworkersRepoImpl extends HiredFarmworkersRepo
    with NetworkCheckService {
  final DioClient _dioClient;
  HiredFarmworkersRepoImpl(this._dioClient);

  @override
  Future<Either<HiredFarmersFarmworkersModel, Exception>> hired() async {
    var data = await checkInternet();
    if (!data) {
      return Right(Exception("No Network found"));
    } else {
      try {
        var result = await _dioClient.requestForAuth(
            'notification/getFarmersAndFarmworkersByLandlordOrFarmer?${Get.find<HomeController>().role.value == 'Landlord' ? 'landlordId=${Get.find<HomeController>().selectedLandlordId.value}' : 'farmerId=${Get.find<HomeController>().selectedLandlordId.value}'}',
            Method.get);
        return result.fold((l) {
          HiredFarmersFarmworkersModel model =
              HiredFarmersFarmworkersModel.fromJson(l.data);
          return Left(model);
        }, (r) => Right(Exception(r)));
      } catch (e) {
        return Right(Exception(e));
      }
    }
  }
}
