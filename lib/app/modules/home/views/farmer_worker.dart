import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:agritech/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/base/base_controller.dart';
import '../../../../core/conts/color_consts.dart';
import '../../../../core/conts/img_const.dart';
import '../../../../core/themes/text_styles.dart';
import '../../../../core/utils/widget_utils/shimmers.dart';
import '../../../../core/utils/widget_utils/text_fields/custom_search_widget.dart';
import '../controllers/landlord_worker_controller.dart';

class FarmerWorker extends GetView<LandlordWorkerController> {
  const FarmerWorker({super.key});

  static const String defaultImageUrl = agri;

  @override
  Widget build(BuildContext context) {
    final imageSize = Get.width * 0.15;
    final innerCircleSize = Get.width * 0.14;

    return Obx(
      () => RefreshIndicator(
        onRefresh: () async {
          Get.find<LandlordWorkerController>().landlordWorkerSearch.value = '';
          Get.find<LandlordWorkerController>().filterFarmworkers();
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Text(
                'Available FarmWorkers',
                style: TextStyles.kTSFS14W600.copyWith(
                  color: colorHello,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: CustomSearchField(
                onChanged: (val) {
                  controller.landlordWorkerSearch.value = val;
                  if (val.length >= 3) {
                    controller.filterFarmworkers();
                  } else if (val.length == 0) {
                    controller.filterFarmworkers();
                  }
                },
              ),
            ),
            Expanded(
              child: controller.apiStatus.value == ApiStatus.LOADING
                  ? Shimmers().getListShimmer()
                  : (controller.landlordFarmWorkerModel.value.result
                                  ?.farmworkers ??
                              [])
                          .isNotEmpty
                      ? ListView.builder(
                          itemCount: controller.landlordFarmWorkerModel.value
                              .result!.farmworkers!.length,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          itemBuilder: (context, index) {
                            final farmer = controller.landlordFarmWorkerModel
                                .value.result!.farmworkers![index];

                            return InkWell(
                              onTap: () async {
                                // final homeController =
                                //     Get.find<HomeController>();
                                // homeController.farmWorkerId.value =
                                //     farmer.id?.toString() ?? '';
                                // await homeController.getUserByProfileId(id: 
                                //     homeController.farmWorkerId.value);
                                Get.toNamed(Routes.DETAILS_OF_FARMWORKER,arguments: {"workerId":farmer.id!});
                              },
                              child: Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 10),
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(19),
                                  boxShadow: [
                                    BoxShadow(
                                      color: colorBlack.withOpacity(0.2),
                                      blurRadius: 5,
                                      spreadRadius: 1,
                                      offset: const Offset(0, 3),
                                      blurStyle: BlurStyle.inner,
                                    ),
                                  ],
                                  color: colorWhite,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 20,right: 20),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          Container(
                                            height: imageSize,
                                            width: imageSize,
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: colorCircleWorker,
                                            ),
                                          ),
                                          Positioned(
                                            left: 2,
                                            child: Container(
                                              height: innerCircleSize,
                                              width: innerCircleSize,
                                              decoration: const BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: colorWhite,
                                              ),
                                            ),
                                          ),
                                          ClipOval(
                                            child: Image.network(
                                              farmer.imageUrl ??
                                                  defaultImageUrl,
                                              height: imageSize - 5,
                                              width: imageSize - 5,
                                              fit: BoxFit.cover,
                                              errorBuilder:
                                                  (context, error, stackTrace) {
                                                return Image.asset(
                                                  defaultImageUrl,
                                                  height: imageSize - 5,
                                                  width: imageSize - 5,
                                                  fit: BoxFit.cover,
                                                );
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(width: 20),
                                      Expanded(
                                        flex: 2,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              farmer.firstName ?? 'N/A',
                                              style: TextStyles.kTSCF12W500,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            Text(
                                              farmer.age?.toString() ?? 'N/A',
                                              style: TextStyles.kTSCF12W500,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            Text(
                                              farmer.address ?? '-',
                                              style: TextStyles.kTSCF12W500,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 20,
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              farmer.skills != null &&
                                                      farmer.skills!.isNotEmpty
                                                  ? farmer.skills!.first.skill ??
                                                      'N/A'
                                                  : '',
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyles.kTSCF12W500,
                                            ),
                                            const SizedBox(
                                              height: 2,
                                            ),
                                            Text(
                                              farmer.skills != null &&
                                                      farmer.skills!.length > 1
                                                  ? (farmer.skills![1].skill ??
                                                      'N/A')
                                                  : '',
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyles.kTSCF12W500,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        )
                      : const Center(
                          child: Text(
                            'No Farm Workers found',
                            style: TextStyles.kTSDS12W500,
                          ),
                        ),
            ),
          ],
        ),
      ),
    );
  }
}
