// To parse this JSON data, do
//
//     final getAllSavedLandlordModel = getAllSavedLandlordModelFromJson(jsonString);

import 'dart:convert';

GetAllSavedLandlordModel getAllSavedLandlordModelFromJson(String str) => GetAllSavedLandlordModel.fromJson(json.decode(str));

String getAllSavedLandlordModelToJson(GetAllSavedLandlordModel data) => json.encode(data.toJson());

class GetAllSavedLandlordModel {
    int? statusCode;
    dynamic status;
    dynamic message;
    Result? result;

    GetAllSavedLandlordModel({
        this.statusCode,
        this.status,
        this.message,
        this.result,
    });

    factory GetAllSavedLandlordModel.fromJson(Map<String, dynamic> json) => GetAllSavedLandlordModel(
        statusCode: json["statusCode"],
        status: json["status"],
        message: json["message"],
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "status": status,
        "message": message,
        "result": result?.toJson(),
    };
}

class Result {
    List<SavedFarmer>? savedFarmers;
    int? farmerCount;

    Result({
        this.savedFarmers,
        this.farmerCount,
    });

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        savedFarmers: json["savedFarmers"] == null ? [] : List<SavedFarmer>.from(json["savedFarmers"]!.map((x) => SavedFarmer.fromJson(x))),
        farmerCount: json["farmerCount"],
    );

    Map<String, dynamic> toJson() => {
        "savedFarmers": savedFarmers == null ? [] : List<dynamic>.from(savedFarmers!.map((x) => x.toJson())),
        "farmerCount": farmerCount,
    };
}

class SavedFarmer {
    int? savedId;
    int? landlordid;
    dynamic firstName;
    dynamic locationDetails;
    dynamic mobileNumber;
    dynamic imageUrl;
    dynamic address;
    dynamic age;
    bool? flag;

    SavedFarmer({
        this.savedId,
        this.landlordid,
        this.firstName,
        this.locationDetails,
        this.mobileNumber,
        this.imageUrl,
        this.address,
        this.age,
        this.flag,
    });

    factory SavedFarmer.fromJson(Map<String, dynamic> json) => SavedFarmer(
        savedId: json["savedId"],
        landlordid: json["landlordid"],
        firstName: json["firstName"],
        locationDetails: json["location_details"],
        mobileNumber: json["mobileNumber"],
        imageUrl: json["image_url"],
        address: json["address"],
        age: json["age"],
        flag: json["flag"],
    );

    Map<String, dynamic> toJson() => {
        "savedId": savedId,
        "landlordid": landlordid,
        "firstName": firstName,
        "location_details": locationDetails,
        "mobileNumber": mobileNumber,
        "image_url": imageUrl,
        "address": address,
        "age": age,
        "flag": flag,
    };
}
