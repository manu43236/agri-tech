import 'package:agritech/app/modules/details_of_farmer/controllers/details_of_farmer_controller.dart';
import 'package:agritech/app/modules/home/controllers/home_controller.dart';
import 'package:get/get.dart';

import '../../details_of_landlord_utilities/controllers/details_of_landlord_utilities_controller.dart';
import '../../notifications/controllers/notifications_controller.dart';
import '../../saved_farmworkers/controllers/saved_farmworkers_controller.dart';
import '../../saved_landlords/controllers/saved_landlords_controller.dart';
import '../controllers/details_of_landlord_controller.dart';

class DetailsOfLandlordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DetailsOfLandlordUtilitiesController());
    Get.lazyPut<DetailsOfLandlordController>(
        () => DetailsOfLandlordController());
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<SavedLandlordsController>(() => SavedLandlordsController());
    Get.lazyPut<SavedFarmworkersController>(() => SavedFarmworkersController());
     Get.lazyPut<NotificationsController>(
      ()=>NotificationsController()
    );
    Get.lazyPut<DetailsOfFarmerController>(
      ()=>DetailsOfFarmerController()
    );
  }
}
