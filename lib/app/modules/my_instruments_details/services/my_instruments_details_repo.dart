import 'package:agritech/app/modules/my_instruments_details/data/send_notification_for_loan_utility_model.dart';
import 'package:dartz/dartz.dart';

abstract class MyInstrumentsDetailsRepo{
  Future<Either<SendNotificationForLoanUtilityModel,Exception>> sendUtilityLoan({params1});
}